<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * MY Controller extend CI_Controller
 *
 * @author      appleboy
 * @copyright   2012 appleboy
 * @link        http://blog.wu-boy.com
 * @package     CodeIgniter
 * @subpackage  CI_Controller
 */
class MY_Controller extends CI_Controller
{
    /**
     * login user data
     *
     * @var string
     **/
    public $loginer;

    /**
     * login user facebook data
     *
     * @var string
     **/
    public $user_fb_data;

    /**
     * timestamp
     *
     * @var int
     **/
    protected $_time;

    /**
     * __construct
     *
     * @return void
     **/
    public function __construct()
    {
        parent::__construct();
        // load native session
        $this->load->library(array('session', 'ion_auth', 'lib_category', 'zacl', 'system'));
        // load template library
        $this->load->spark('codeigniter-template/1.0.0');
        // facebook model
        $this->load->model('facebook_model');
        // get facebook data from session
        $this->user_fb_data = $this->session->userdata('fb_data');

        // permission
        $this->_auth();

        // load some data
        $this->_load();

        // define variable
        $this->_time = time();
        

        // set the variable for javascript to calculate 
        $this->load->model('System_configuration_model', 'sys_config');
        $this->template->set('free_shipping', $this->sys_config->get_free_shipping());        
        
    }
    /**
     *
     * User Authentication
     *
     */
    private function _auth()
    {
        $ctrl = $this->router->class;
        $action = $this->router->method;

        // check facebook login
        if (!$this->ion_auth->logged_in() && !is_null($this->user_fb_data['me']) && $this->session->userdata('login_type') === 'facebook') {
            
            /*
             * Patch for facebook API email field bug.
             */
            // If the facebook didn't give us email... we create one.
            // Notice that $identity is the same as in the $user_data
            
            if (!$this->user_fb_data['me']['email']){
                if ($this->user_fb_data['me']['username']){
                    $identity = $this->user_fb_data['me']['username'] . '@facebook.com';
                }
                else{
                    $identity = $this->user_fb_data['me']['id'] . '@facebook.com';              
                }
                $this->user_fb_data['me']['email'] = $identity;
            }
            
            // facebook auto login or insert new user account.
            $this->ion_auth->other_login($this->user_fb_data['me']['id'], $this->user_fb_data);
        }

        if ($this->ion_auth->logged_in()) {
            // user profile
            $this->loginer = $this->ion_auth->profile();
        } else {
            // generate guest user login data
            $this->ion_auth->guest_login();
            // set guest user data
            $this->loginer = $this->ion_auth->profile();
        }

        // Initial User ACL
        $this->zacl->initial();

        // if no permission, redirect to home page
        if ($this->uri->segment(1) != 'API' and !$this->zacl->check_acl($ctrl, $action)) {
            if (!$this->ion_auth->logged_in()) {
                $this->system->redirect();
            }
            redirect('/', 'refresh');
        }

        $this->template->set('loginer', $this->loginer);
        $this->template->set('logined', $this->ion_auth->logged_in());
        $this->template->set('ctrl', $ctrl);
        $this->template->set('action', $action);
    }

    /*
     *  load css or javascript
     */
    private function _load()
    {
        // load controller css
        $css_file_path = 'assets/css/'.$this->router->class.'.css';
        if (file_exists($css_file_path)) {
            $this->template->add_css($css_file_path);
        }

        // get category list
        $product_category = $activity_category = array();
        $rows = $this->lib_category->order_by('sort_order', 'asc')->categories()->result_array();

        foreach ($rows as $row) {
            switch ($row['type']) {
                case 'product':
                    array_push($product_category, $row);
                    break;
                case 'activity':
                    array_push($activity_category, $row);
                    break;
            }
        }

        $this->template->set('product_category', $product_category);
        $this->template->set('activity_category', $activity_category);
    }
    
    /*
     * this is kind of AOP(Aspect-Oriented Programming) technique.
     * force user to login before excute the 
     * 
     * @author howtomakeaturn
     */
    function redirect_if_not_login(){
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->system->redirect();
            return;
        }
    }

}

/* 
 * unit testing controller
 * @author howtomakeaturn
 */
class Test_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('unit_test');
        $this->load->database();                

        $whitelist = array('127.0.0.1');

        if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
            exit('not allowed');
        }                
    }

    protected function mocking_order(){
        $order = new stdClass();
        $order->buyer = new stdClass();
        $order->buyer->location= new stdClass(); 
        $order->buyer->name = 'mocking_name';
        $order->buyer->gender = '1';
        $order->buyer->contact_phone_number = '3345678';        
        $order->buyer->location->zipcode = '888';
        $order->buyer->location->county = 'mocking_taipei';
        $order->buyer->location->district = 'mocking_street';
        $order->buyer->location->address = 'mocking_somewhere';
        
        $order->receiver = new stdClass();
        $order->receiver->location= new stdClass(); 
        $order->receiver->name = 'mocking_name';
        $order->receiver->gender = '1';
        $order->receiver->contact_phone_number = '3345678';
        $order->receiver->location->zipcode = '888';
        $order->receiver->location->county = 'mocking_taipei';
        $order->receiver->location->district = 'mocking_street';
        $order->receiver->location->address = 'mocking_somewhere';
    
        $order->delivery_time = 'any';
        $order->receipt_to = '1';

        $order->uniform_invoice_type = 'two';
        $order->uniform_invoice_title = 'mocking title';
        $order->uniform_invoice_number = 'mocking number';
        
        return $order;
    }    

    
    
    /**
     * copied from sellorder controller
     */
    protected function generate_order_number()
    {
        $this->load->helper('date');

        // $datestring = "Year: %Y Month: %m Day: %d - %h:%i %a";
        $datestring = "%Y%m%d";
        $time = time();
        // $time = $this->_time;

        // date('Ymd') == date('Ymd', strtotime($timestamp));
        // update order status
        // $this->db->set($data)->where('orders.order_number', $ordernumber)->update('orders');
        // query order_number

        $order_num = "S";
        $time_str = mdate($datestring, $time);
        $this->load->library('lib_sellorder');
        // select count(*), DATE_FORMAT(date_purchased,"%Y-%m-%d") as created_day FROM sellorders GROUP BY created_day
        $result = $this->lib_sellorder->select('count(*), DATE_FORMAT(date_purchased,"%Y%m%d") as created_day')
             ->group_by('created_day')
             ->orders()
             ->result_array();

        foreach ($result as $row) {
            if($time_str == $row['created_day']){
                $count_today = $row['count(*)'];
		if(!$count_today || $count_today==0)
                    $count_today = 1;
		else	
                    $count_today++;
            }else{
                $count_today = 1;
            }
        }

        $sn_today = $count_today;
        $sn_today = str_pad($sn_today,4,'0',STR_PAD_LEFT);

        $time_str = substr($time_str, 2);
        $order_num .= $time_str.$sn_today;

        // return strtoupper($this->system->generate_code('1', 'word')) . $this->system->generate_code('10', 'digit');
        return $order_num;
    }        

}


 /* admin controller
 * @author howtomakeaturn
 */
class Admin_Controller extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->spark('codeigniter-template/1.0.0');        
        $this->load->library('ion_auth');
        $this->load->library('system');
        if ( !$this->ion_auth->is_admin()){
            $this->system->redirect();
        }
        $this->load->database();
    }

}


/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
