<?php
/*
 * Sending emails related actions!
 * 
 * Apply the 'service object' concept from Rails community
 * to separate interactions from domain class.
 * 
 * @author howtomakeaturn
 */
class MailSender{
    protected $CI;
  
    function __construct(){
        $this->CI =& get_instance();      
        $this->CI->load->database();
        $this->CI->load->spark('codeigniter-template/1.0.0');
        $this->CI->template->set_layout('template/email');
        $this->CI->load->library('notification');
        $this->CI->load->model('User_model');
    }
    
    /*
     * @params:
     *    sender: how their friends know the sender
     *    receiver: the receiver emails
     *    title: the email title
     *    content: how the sender introduce itself
     *    user_id: sender id
     */    
    function advertise_to_friends($data){
        $html = $this->CI->template->render(
            'email/advertise_to_friends', $data, true);      

        $receiver = explode(',', $data['receiver']);
        if (count($receiver)===1){
            $addr = trim($receiver[0]);
            $this->CI->notification->send($data['title'], $html, $addr);          
        }
        else{
            foreach($receiver as $value){
                $addr = trim($value);
                $this->CI->notification->send($data['title'], $html, $addr);
            }
        }      
    }
    
    function send_return_item_reminder_mail($item_number){
        $this->CI->load->model('Product_clothing_model', 'clothing');        
        
        $item = $this->CI->clothing->get_one_by_product_item_number($item_number);
        $order = $this->CI->db->where('id', $item['buy_order_id'])
                                                    ->from('buy_order')->get()->row_array();
        $html = $this->CI->template->render(
            'email/send_return_item_reminder_mail', [ 'item' => $item, 'order' => $order], true);
        $email = $this->CI->User_model->get_effective_email($order['user_id']);
        $this->CI->notification->send('退貨處理通知', $html, $email);
        return true;
    }
    
    function send_is_picking_order_reminder_mail($order_id){
        $this->CI->load->model('Sellorder_model');
        $order = $this->CI->Sellorder_model->get_by_id($order_id);
        $html = $this->CI->template->render(
            'email/send_is_picking_order_reminder_mail', [ 'order' => $order], true);            
        $email = $this->CI->User_model->get_effective_email($order->user_id);
        
        $this->CI->notification->send('收貨處理通知', $html, $email);      
        return true;
    }
    
    function send_is_checking_order_reminder_mail($order_id){
        $this->CI->load->model('Sellorder_model');
        $order = $this->CI->Sellorder_model->get_by_id($order_id);
        $html = $this->CI->template->render(
            'email/send_is_checking_order_reminder_mail', [ 'order' => $order], true);            
        $email = $this->CI->User_model->get_effective_email($order->user_id);
        
        $this->CI->notification->send('驗收處理通知', $html, $email);      
        return true;
    }
    
    function send_is_in_shelf_reminder_mail($order_id){
        $this->CI->load->model('Sellorder_model');
        $order = $this->CI->Sellorder_model->get_by_id($order_id);
        $html = $this->CI->template->render(
            'email/send_is_in_shelf_reminder_mail', [ 'order' => $order], true);            
        $email = $this->CI->User_model->get_effective_email($order->user_id);
        
        $this->CI->notification->send('衣服上架通知', $html, $email);      
        return true;      
    }
    
    function shout(){
        return 'shout';
    }
  
}

