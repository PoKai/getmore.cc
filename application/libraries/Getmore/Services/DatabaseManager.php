<?php
/*
 * Database management related actions!
 * 
 * Apply the 'service object' concept from Rails community
 * to separate interactions from domain class.
 * 
 * @author howtomakeaturn
 */
class DatabaseManager{
    protected $CI;
  
    function __construct(){
        $this->CI =& get_instance();      
        $this->CI->load->database();
        $this->CI->load->library('notification');
    }
    
    function send_backup_to_email(){
        // Load the DB utility class
        $this->CI->load->dbutil();
        $filename = 'getmore-' . date('c');
        $prefs = array(
            #'tables'      => array('product_clothing', 'articles'),  // Array of tables to backup.
            'ignore'      => array('logs'),           // List of tables to omit from the backup
            'format'      => 'zip',             // gzip, zip, txt
            'filename'    => $filename . '.sql',    // File name - NEEDED ONLY WITH ZIP FILES
            'add_drop'    => TRUE,              // Whether to add DROP TABLE statements to backup file
            'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
            'newline'     => "\n"               // Newline character used in backup file
        );

        // Backup your entire database and assign it to a variable
        $backup =& $this->CI->dbutil->backup($prefs); 

        // Load the download helper and send the file to your desktop
//        $this->CI->load->helper('download');
  //      force_download($filename .'.zip', $backup);

        $this->CI->load->helper('file');
        write_file(APPPATH . '/backup/' . $filename .'.zip', $backup); 

        $this->CI->load->library('Notification');
        $mailer = $this->CI->notification->get_setted_phpmailer_instance();
        $mailer->Subject = $filename . '資料庫備份';                     // 設定郵件標題                      
        $mailer->AddAddress('pokai@getmore.cc', 'Getmore管理員');  // 收件者郵件及名稱 
        $mailer->AddAddress('howtomakeaturn@getmore.cc', 'Getmore管理員');  // 收件者郵件及名稱 
        $mailer->Body = $filename . '資料庫備份';
        $mailer->AddAttachment( APPPATH . 'backup/' . $filename .'.zip',
                         $filename . '.zip');
        $mailer->Send();        
        unlink(APPPATH . 'backup/' . $filename .'.zip');
    }    

}

