<?php
/*
 * This class mainly focus on transform data schema in database
 * with domain class structure.
 * 
 * @author howtomakeaturn
 */
require_once('Article.php');

class ArticleDataMapper{
    protected $CI;
  
    function __construct(){
        $this->CI =& get_instance();      
        $this->CI->load->database();
    }
  
    function save(Article $article){
        $this->CI->db->insert('articles', array(
            'title' => $article->title,
            'content' => $article->content,
            'feature_pic_id' => $article->feature_pic_id,
            'created_at' => $article->created_at,
            'author' => $article->author,
            'about' => $article->about,
            'published' => $article->published,
        ));
        
        return $this->CI->db->insert_id();
    }
    
    function fetchFromDataToArticle($data){
        return $this->fromDataToArticle($data);
    }
    
    function fromArticleToData(Article $article){
        $data = array(
            'title' => $article->title,
            'content' => $article->content,
            'feature_pic_id' => $article->feature_pic_id,
            'created_at' => $article->created_at,
            'author' => $article->author ?: '',
            'about' => $article->about ?: '',
            'published' => $article->published,
            'category_id' => $article->category_id,
            'intro' => $article->intro
        );
        
        return $data;
    }
    
    function fromDataToArticle($data){
        $article = new Article($data['id'], $data['title'], $data['content'], 
            $data['feature_pic_id'], $data['created_at']);
        
        $article->feature_pic_path = image_url($data['file_name']);
        $article->author = $data['author'];
        $article->about = $data['about'];
        $article->published = ($data['published']=='1')? TRUE : FALSE;
        $article->intro = $data['intro'];
        $article->category = new ArticleCategory();

        $category = $this->CI->db->from('article_categories')
                                             ->where('id', $data['category_id'])
                                             ->get()->row();
                                             
        $article->category->id = (!$category) ? '0' : $category->id;
        $article->category->name = (!$category) ? '未分類' : $category->name;        
        return $article;      
    }
    
    function get($id){
        $data = $this->CI->db->from('articles')
                                            ->where('id', $id)
                                            ->join('files', 'files.file_id = articles.feature_pic_id', 'left')
                                            ->get()->row_array();
                                            
        $article = $this->fetchFromDataToArticle($data);
        return $article;
    }
    
    function getAll(){
        $rows = $this->CI->db->from('articles')
                                            ->order_by('created_at', 'desc')
                                            ->get()->result_array();
                                            
        $articles = array();
        foreach($rows as $row){
            $article = $this->fetchFromDataToArticle($row);
            array_push($articles, $article);
        }
        
        return $articles;
      
    }

    function remove(Article $article){
        $this->CI->db->where('id', $article->id)
                              ->delete('articles');
                
        return true;
    }
  
}

