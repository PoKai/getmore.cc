<?php
/*
 * The domain class for blogging system.
 * 
 * Implement the blogging system with repository pattern, 
 * data mapper pattern in domain driven design methodology.
 * Because Codeigniter didn't provide anything in model, 
 * it's difficult to implement re-usable code. 
 * DDD is much better!
 * 
 * @author howtomakeaturn
 */
class Article{
    
    /*
     * Declare member attributes as public is kind of tradeoff. 
     * It avoids making all setter and getter method for all attributes.
     * Why do so? Because you need to implement setter for primary key(id) in data mapper,
     * which is pretty weird.
     * Since we need to exploid database schema into domain object anyway, 
     * declare them as public is much more convinient.
     */
    
    public $id;
    public $title;
    public $content;
    public $feature_pic_id;
    public $feature_pic_path;
    public $created_at;
    public $about;
    public $author;
    public $published;
    public $category;
    public $intro;
    
    function __construct($id='', $title='', $content='', $feature_pic_id='', $created_at=''){
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->feature_pic_id = $feature_pic_id;
        $this->created_at = ($created_at) ? $created_at : date('c');
    }
    
    function getIntro(){
        return ($this->intro)? : strip_tags($this->content);
    }
  
}
