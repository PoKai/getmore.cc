<?php
/*
 * The domain class for category of article
 * @author howtomakeaturn
 */
class ArticleCategory{
    public $id;
    public $name;
}
