<?php
/*
 * Provide several ways to fetch articles.
 * 
 * @author howtomakeaturn
 */
require_once('Article.php');
require_once('ArticleDataMapper.php');
require_once('ArticleCategory.php');

class ArticleRepository{
    protected $CI;
    protected $dm;
    
    const PER_PAGE_NUM = 9;
    
    function __construct(){
        $this->CI =& get_instance();      
        $this->CI->load->database();
        $this->dm = new ArticleDataMapper();
    }
    
    // Create or Update an article.
    function save(Article $article){
        // insert into database
        if (!$article->id){
            $data = $this->dm->fromArticleToData($article);        
            $this->CI->db->insert('articles', $data);        
            return $this->CI->db->insert_id();        
        }
        // update from database
        else{
            $data = $this->dm->fromArticleToData($article);        
            $this->CI->db->where('id', $article->id);
            $this->CI->db->update('articles', $data); 
            return $article->id;
        }
    }
    
    function get($id){
        $article = $this->dm->get($id);
        return $article;
    }
    
    function getAllPublishedPaginated($page){
        $this->CI->db->limit( self::PER_PAGE_NUM, ($page - 1)*self::PER_PAGE_NUM );
        return $this->getAllPublished();
    }
    
    function getAllPublishedByCategoryIdPaginated($category_id, $page){
        $this->CI->db->limit( self::PER_PAGE_NUM, ($page - 1)*self::PER_PAGE_NUM );
        return $this->getAllPublishedByCategoryId($category_id);      
    }
    
    function getAllPublished(){
        $this->CI->db->where('published', TRUE);
        return $this->getAll();
    }

    function getAllPublishedByCategoryId($category_id){
        $this->CI->db->where('published', TRUE);
        $this->CI->db->where('category_id', $category_id);
        return $this->getAll();
    }

    
    function getAll(){
        $rows = $this->CI->db->from('articles')
                                            ->join('files', 'files.file_id = articles.feature_pic_id', 'left')        
                                            ->order_by('created_at', 'desc')
                                            ->get()->result_array();
                                            
        $articles = array();
        foreach($rows as $row){
            $article = $this->dm->fetchFromDataToArticle($row);
            array_push($articles, $article);
        }
        
        return $articles;      
    }
    
    function getRecentArticles($number = 5){
        $rows = $this->CI->db->from('articles')
                                            ->where('published', TRUE)
                                            ->order_by('created_at', 'desc')
                                            ->limit($number)
                                            ->get()->result_array();
                                            
        $articles = array();
        foreach($rows as $row){
            $article = $this->dm->fetchFromDataToArticle($row);
            array_push($articles, $article);
        }
        
        return $articles;
    }
    
    function countPublished(){
        return $this->CI->db->where('published', TRUE)->from('articles')->get()->num_rows();
    }
    
    function countPublishedByCategoryId($category_id){
        return $this->CI->db->where('published', TRUE)
                                               ->where('category_id', $category_id)
                                               ->from('articles')
                                               ->get()->num_rows();
    }    
  
}

