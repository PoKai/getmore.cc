<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name: Url Library
*
* Author: appleboy
*
*/

class Lib_static_url
{
    /**
     * CodeIgniter global
     *
     * @var string
     **/
    protected $ci;

    /**
     * extra where
     *
     * @var array
     **/
    public $_extra_where = array();

    /**
     * extra set
     *
     * @var array
     **/
    public $_extra_set = array();

    /**
     * Min price
     *
     * @var int
     **/
    public $_min_price = null;

    /**
     * Transportation Charge
     *
     * @var int
     **/
    public $_transportation_charge = null;

    /**
     * __construct
     *
     * @return void
     * @author appleboy
     **/
    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->model('url_model');
    }

    /**
     * __call
     *
     * Acts as a simple way to call model methods without loads of stupid alias'
     *
     **/
    public function __call($method, $arguments)
    {
        if (!method_exists( $this->ci->url_model, $method) ) {
            throw new Exception('Undefined method url::' . $method . '() called');
        }

        return call_user_func_array( array($this->ci->url_model, $method), $arguments);
    }
}
