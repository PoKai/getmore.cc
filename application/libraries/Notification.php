<?php
/*
 * Sending email library.
 * It provides two clients to send emails: Mandril API and Google SMTP.
 * 
 * @author howtomakeaturn
 */

class Notification {
    /**
    * CodeIgniter global
    */
    protected $ci;
    protected $mandrill_instance;
    protected $gmail;
    
    // this could be 'google' or 'mandrill'
    protected $send_media = 'google';

    public function __construct(){
        $this->ci =& get_instance();
        $this->ci->load->config('mandrill');
        $api_key = $this->ci->config->item('api_key');

        require(APPPATH. 'libraries/mandrill/src/Mandrill.php');
        $this->mandrill_instance = new Mandrill($api_key);
        require(APPPATH. 'libraries/PHPMailer-5.1.0/class.phpmailer.php');
    }
    
    function send($subject, $html, $address){
        if ($this->send_media=='mandrill'){
            $this->mandrill_send($subject, $html, $address);
        }
        else if ($this->send_media=='google'){
            $this->google_send($subject, $html, $address);
        }
    }

    function get_setted_phpmailer_instance(){
        $mailer = new PHPMailer();                        // 建立新物件        
        $mailer->IsSMTP();                                // 設定使用SMTP方式寄信        
        $mailer->SMTPAuth = true;                         // 設定SMTP需要驗證
        $mailer->SMTPSecure = "ssl";                      // Gmail的SMTP主機需要使用SSL連線   
        $mailer->Host = "smtp.gmail.com";                 // Gmail的SMTP主機        
        $mailer->Port = 465;                              // Gmail的SMTP主機的port為465      
        $mailer->CharSet = "utf-8";                       // 設定郵件編碼   
        $mailer->Encoding = "base64";
        $mailer->WordWrap = 50;                           // 每50個字元自動斷行          
        $mailer->Username = "no-reply@getmore.cc";     // 設定驗證帳號        
        $mailer->Password = "Get7023%^&*";              // 設定驗證密碼                  
        $mailer->From = "no-reply@getmore.cc";         // 設定寄件者信箱        
        $mailer->FromName = "Getmore二次時尚";                 // 設定寄件者姓名                        
        $mailer->IsHTML(true);                            // 設定郵件內容為HTML        
        return $mailer;
    }

    private function google_send($subject, $html, $address){
        $this->gmail = new PHPMailer();                        // 建立新物件        

        $this->gmail->IsSMTP();                                // 設定使用SMTP方式寄信        
        $this->gmail->SMTPAuth = true;                         // 設定SMTP需要驗證

        $this->gmail->SMTPSecure = "ssl";                      // Gmail的SMTP主機需要使用SSL連線   
        $this->gmail->Host = "smtp.gmail.com";                 // Gmail的SMTP主機        
        $this->gmail->Port = 465;                              // Gmail的SMTP主機的port為465      
        $this->gmail->CharSet = "utf-8";                       // 設定郵件編碼   
        $this->gmail->Encoding = "base64";
        $this->gmail->WordWrap = 50;                           // 每50個字元自動斷行
          
        $this->gmail->Username = "no-reply@getmore.cc";     // 設定驗證帳號        
        $this->gmail->Password = "ajawutfrczwliiup";              // 設定驗證密碼        
          
        $this->gmail->From = "no-reply@getmore.cc";         // 設定寄件者信箱        
        $this->gmail->FromName = "Getmore二次時尚";                 // 設定寄件者姓名        
          
        $this->gmail->Subject = $subject;                     // 設定郵件標題        
      
        $this->gmail->IsHTML(true);                            // 設定郵件內容為HTML        
        
        $this->gmail->AddAddress($address, 'Getmore會員');  // 收件者郵件及名稱 
        $this->gmail->Body = $html;

        if($this->gmail->Send()) {                             // 郵件寄出
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    private function mandrill_send($subject, $html, $address){
        $message = array(
            'html' => $html,
            'subject' => $subject,
            'from_email' => 'no-reply@getmore.cc',
            'from_name' => 'Getmore二次時尚',
            'to' => array(
                array(
                    'email' => $address,
                    'name' => 'Getmore會員',
                    'type' => 'to'
                )
            )
        );
        $this->mandrill_instance->messages->send($message);                    
    }
}


/* End of file Notification.php */
