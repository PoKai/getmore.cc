<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name: Config Library
*
* Author: appleboy
*
*/

class Lib_config
{
    /**
     * CodeIgniter global
     *
     * @var string
     **/
    protected $ci;

    /**
     * extra where
     *
     * @var array
     **/
    public $_extra_where = array();

    /**
     * extra set
     *
     * @var array
     **/
    public $_extra_set = array();

    /**
     * config
     *
     * @var array
     **/
    public $_config = array();

    /**
     * __construct
     *
     * @return void
     * @author Ben
     **/
    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->library(array('session', 'files_lib'));
        $this->ci->load->model('config_model');

        $result = $this->ci->config_model->select('name, value')->configs()->result_array();
        foreach ($result as $row)
        {
            $this->_config[$row['name']] = $row['value'];
        }
    }

    /**
     * __call
     *
     * Acts as a simple way to call model methods without loads of stupid alias'
     *
     **/
    public function __call($method, $arguments)
    {
        if (!method_exists( $this->ci->config_model, $method) )
        {
            throw new Exception('Undefined method config::' . $method . '() called');
        }

        return call_user_func_array( array($this->ci->config_model, $method), $arguments);
    }

    /**
     * top image
     *
     * @return  array
     * @author  appleboy
     **/
    public function top_image()
    {
        $files = array();

        if(isset($this->_config['banner_list']) and !empty($this->_config['banner_list']))
        {
            $files = $this->ci->files_lib->select('file_id, file_name')->where('file_id', explode(',', $this->_config['banner_list']))->order_by_field('file_id', $this->_config['banner_list'])->files()->result_array();
        }

        return $files;
    }

    /**
     * top link
     *
     * @return  array
     * @author  appleboy
     **/
    public function top_link()
    {
        $link = array();

        if($this->_config['link_list'] != '')
        {
            $link_array = explode(',', $this->_config['link_list']);
            foreach($link_array as $item)
            {
                if(stripos($item, '::') !== false)
                {
                    $item = explode('::', $item);
                    $data = array(
                        "link" => $item[0],
                        "target" => ($item[1] == "1") ? true : false,
                        "target_value" => $item[1]
                    );
                    $link[] = $data;
                }
            }
        }

        return $link;
    }

    /**
     * config item
     *
     * @param   string
     * @return  string
     * @author  appleboy
     **/
    public function item($key = "")
    {
        return ($key != '' and isset($this->_config[$key])) ? $this->_config[$key] : '';
    }

    /**
     * config keys
     *
     * @param   int
     * @return  array
     * @author  appleboy
     **/
    public function keys()
    {
        return (!empty($this->_config)) ? array_keys($this->_config) : array();
    }
}
