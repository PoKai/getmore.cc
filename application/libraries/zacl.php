<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Zend ACL Library
 * Author: Bo-Yi Wu <appleboy.tw@gmail.com>
 * Date: 2011-10-18
 */

class Zacl
{
    // Set the instance variable
    protected $ci;
    protected $acl;
    protected $acl_group = array();

    public $all_group;

    public function __construct()
    {
        // Get the instance
        $this->ci =& get_instance();
        $this->ci->load->library("session");
        $this->ci->load->library("ion_auth");
        $this->ci->load->model("acl_model");
        // Set the include path and require the needed files
        set_include_path(get_include_path() . PATH_SEPARATOR . APPPATH . 'libraries/');

        require_once(APPPATH . 'libraries/Zend/Acl.php');
        require_once(APPPATH . 'libraries/Zend/Acl/Role.php');
        require_once(APPPATH . 'libraries/Zend/Acl/Resource.php');

        $this->acl = new Zend_Acl();
    }

    /**
     * __call
     *
     * Acts as a simple way to call model methods without loads of stupid alias'
     *
     **/
    public function __call($method, $arguments)
    {
        if (!method_exists( $this->ci->acl_model, $method) ) {
            throw new Exception('Undefined method Zacl::' . $method . '() called');
        }

        return call_user_func_array( array($this->ci->acl_model, $method), $arguments);
    }

    /*
     * ACL Initial
     *
     * @appleboy
     * @return void
     *
     **/
    public function initial()
    {
        $user_groups = $this->ci->session->userdata('user_groups');
        $this->all_group = $this->ci->ion_auth_model->get_groups();

        // add user role
        $this->acl->addRole(new Zend_Acl_Role($this->ci->session->userdata('user_id')));

        foreach ($this->all_group as $group) {
            // add group to Role and Resource
            $this->acl->addRole(new Zend_Acl_Role($group->name));
            $this->acl->add(new Zend_Acl_Resource($group->name));
            // check if $user_groups is array or false;
            if (is_array($user_groups) AND in_array($group->name, $user_groups)) {
                $this->acl->allow($this->ci->session->userdata('user_id'), $group->name);
            }

        }

        $data = $this->ci->acl_model->get_acl();
        $group = array();
        if (!empty($data)) {
            foreach ($data as $row) {
                $class_name = ucfirst($row['class_name']);
                $method_name = ucfirst($row['method_name']);

                if (!isset($group[$class_name])) {
                    if (!$this->acl->has($class_name)) {
                        $this->acl->add(new Zend_Acl_Resource($class_name));
                    }

                    $group[$class_name] = true;
                }

                if ($row['action'] == 'allow') {
                    $this->acl->allow($row['group_name'], $class_name, $method_name);
                } else {
                    $this->acl->deny($row['group_name'], $class_name, $method_name);
                }

                $this->acl_group[$class_name][$method_name] = true;
            }
        }
    }

    public function check_acl($resource = null, $privage = null)
    {
        if (isset($resource)) {
            $resource = ucfirst($resource);
        }

        if (isset($privage)) {
            $privage = ucfirst($privage);
        }

        // check $resource and $privage exist;
        if (isset($resource) AND isset($privage)) {
            if (!isset($this->acl_group[$resource][$privage])) {
                return true;
            }
        }

        if (!$this->acl->has($resource)) {
            return true;
        }

        // check user in group permission
        if ($privage === null) {
            $role = $this->ci->session->userdata('user_id');
            return $this->acl->isAllowed($role, $resource);
        }

        // check group in resource permission
        $role = $this->ci->session->userdata('user_groups');

        if (!is_array($role) and !$this->acl->hasRole($role)) {
            return true;
        } else {
            if (is_array($role)) {
                foreach ($role as $group) {
                    if (!$this->acl->hasRole($group)) {
                        return true;
                    }
                }
            }
        }

        if (isset($privage) and !empty($privage)) {
            if (is_array($role)) {
                foreach ($role as $group) {
                    if ($this->acl->isAllowed($group, $resource, $privage)) {
                        return true;
                    }
                }
            } else

                return $this->acl->isAllowed($role, $resource, $privage);
        }

        return FALSE;
    }

}
/* End of file Zacl.php */
/* Location: ./application/libraries/Zacl.php */
