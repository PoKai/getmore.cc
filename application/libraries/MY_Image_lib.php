<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Image_lib extends CI_Image_lib {

    public function __construct()
    {
        parent::__construct();
        $this->_ci = & get_instance();
        $this->_ci->load->config('images');
        $this->_ci->load->helper('html');
        $this->_upload_path = $this->_ci->config->item('upload_path');
    }

    public function image_resize($file, $type = '', $force = false, $show = false)
    {
        if(empty($type))
            return;

        $filepath = $this->_upload_path . $file;

        $size = $this->_ci->config->item('image_size');

        if (isset($size[$type]))
        {
            foreach($size[$type] as $row)
            {
                $image_array = array();
                $image_array = explode('x', $row);
                $w = $image_array[0];
                $h = $image_array[1];
                $thumbpath = $this->_upload_path . 'thumbs/' . implode('_', $image_array) . '_' . $file;

                if ((!file_exists($thumbpath) or $force == true) and file_exists($filepath))
                {
                    $thumb_config = array(
                        'thumb_marker' => '',
                        'create_thumb' => TRUE,
                        'source_image' => $filepath,
                        'width' => $image_array[0],
                        'height' => $image_array[1],
                        'master_dim' => 'auto',
                        'new_image' => $thumbpath
                    );
                    $thumb_config = array_merge($this->_ci->config->item('gd2'), $thumb_config);

                    $this->initialize($thumb_config);
                    $this->resize();
                    $this->clear();

                    if($show)
                    {
                        echo $type . " 完成縮圖 " . $thumbpath . "\n";
                    }
                }
            }
        }
    }
}

// END MY_Image_lib Class

/* End of file Image_lib.php */
/* Location: ./application/libraries/MY_Image_lib.php */
