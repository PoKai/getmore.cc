<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name: Order Library
*
* Author: appleboy
*
*/

class Lib_sellorder
{
    /**
     * CodeIgniter global
     *
     * @var string
     **/
    protected $ci;

    /**
     * extra where
     *
     * @var array
     **/
    public $_extra_where = array();

    /**
     * extra set
     *
     * @var array
     **/
    public $_extra_set = array();

    /**
     * Min price for less items
     *
     * @var int
     **/
    public $_min_price_less = null;

    /**
     * Min price for more items
     *
     * @var int
     **/
    public $_min_price_more = null;

    /**
     * Transportation charge for less items
     *
     * @var int
     **/
    public $_transportation_charge_less = null;

    /**
     * Transportation charge for more items
     *
     * @var int
     **/
    public $_transportation_charge_more = null;

    /**
     * Sell Charge for small item
     *
     * @var int
     **/
    public $_sell_charge_small = null;

    /**
     * Sell Charge for medium item
     *
     * @var int
     **/
    public $_sell_charge_medium = null;

    /**
     * Sell Charge for big(feather sweater) item
     *
     * @var int
     **/
    public $_sell_charge_big = null;

    /**
     * __construct
     *
     * @return void
     * @author popchieh
     **/
    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->library(array('session', 'ion_auth', 'lib_sellcart', 'lib_event', 'lib_product', 'lib_config'));
        $this->ci->load->model('sellorder_model');
        $this->time_diff = CART_TIME_DIFF;

        $this->_transportation_charge_less = (int) $this->ci->lib_config->item('transportation_charge_less');
        $this->_transportation_charge_more = (int) $this->ci->lib_config->item('transportation_charge_more');
        
        $this->_min_price_less = (int) $this->ci->lib_config->item('min_price_less');
        $this->_min_price_more = (int) $this->ci->lib_config->item('min_price_more');

        $this->_sell_charge_big = (int) $this->ci->lib_config->item('sell_charge_big');
        $this->_sell_charge_medium = (int) $this->ci->lib_config->item('sell_charge_medium');
        $this->_sell_charge_small = (int) $this->ci->lib_config->item('sell_charge_small');
    }

    /**
     * __call
     *
     * Acts as a simple way to call model methods without loads of stupid alias'
     *
     **/
    public function __call($method, $arguments)
    {
        if (!method_exists( $this->ci->sellorder_model, $method) ) {
            throw new Exception('Undefined method order::' . $method . '() called');
        }

        return call_user_func_array( array($this->ci->sellorder_model, $method), $arguments);
    }

    /**
     * User Order list
     *
     * @return array
     * @author appleboy
     **/
    public function user_order($steps = 1, $order_id = NULL, $order_number = NULL)
    {
        //initial value
        $is_transportation_charge = false;
        $is_status_change = false;

        $data = $counts = $events = $products = $events_id = $products_id = array();
        $total_price = $total_event_price = $total_original_product_price = $total_product_price = 0;
        $shipping_price = $transportation_charge = $total_shipping_price = $product_id = 0;
        
        //add by howtomakeaturn. product_item_number start with 1.
        $product_id = 1;
        
        $product_item_number = "AA";

        // get user carts
        $carts = $this->ci->lib_sellcart->contents();

        // if carts is empty, redirect to index
        if (empty($carts)) {
            return array();
        }

        // foreach ($carts as $row) {
            // if (isset($row->type)) {
            //     if ($row->type == 'product') {
            //         array_push($products_id, $row->id);
            //     } elseif ($row->type == 'event') {
            //         array_push($events_id, $row->id);
            //     }
            //     $counts[$row->type][$row->id] = $row->count;
            // }
        // }

        // if (!empty($events_id)) {
        //     $result = $this->ci->lib_event->select("files.file_name, events.*")->where('events.id', $events_id)->where('`options`&' . (_IS_INDEX) . ' >= 1')->where('`options`&' . (_IS_ENABLED) . ' >= 1')->events()->result_array();
        //     foreach ($result as $row) {
        //         $show_update_btn = true;
        //         $status_message = '';
        //         $people_left = $row['peoples'] - $row['participants'];

        //         if (!empty($row['deadline_time']) and strtotime($row['deadline_time']) < time()) {
        //             $status_message = '<span style="color:red">報名時間截止</span>';
        //             $unit_menu = $status_message;
        //             $unit = $status_message;
        //             $is_status_change = true;
        //             $show_update_btn = false;
        //             $counts['event'][$row['id']] = 0;
        //         } else if ($people_left <= 0) {
        //             $status_message = '<span style="color:red">課程已額滿</span>';
        //             $unit_menu = $status_message;
        //             $unit = $status_message;
        //             $is_status_change = true;
        //             $show_update_btn = false;
        //             $counts['event'][$row['id']] = 0;
        //         } else if ($people_left < $counts['event'][$row['id']]) {
        //             $status_message = '<span style="color:red">報名人數不足</span>';
        //             $unit_menu = $this->ci->lib_sellcart->format_dropdown($people_left, 'event', $people_left);
        //             $unit = $people_left;
        //             $is_status_change = true;
        //             $counts['event'][$row['id']] = $people_left;
        //         } else {
        //             $unit_menu = $this->ci->lib_sellcart->format_dropdown($people_left, 'event', $counts['event'][$row['id']]);
        //             $unit = $counts['event'][$row['id']];
        //         }

        //         $data = array(
        //             "id"                => $row['id'],
        //             "image_url"         => image_url($row['file_name'], 72, 72),
        //             "title"             => $row['title'],
        //             "event_time"        => date("Y-m-d H:i", strtotime($row['event_time'])),
        //             "price"             => $row['price'],
        //             "unit_menu"         => $unit_menu,
        //             "unit"              => $unit,
        //             "total_price"       => $counts['event'][$row['id']] * $row['price'],
        //             "show_update_btn"   => $show_update_btn,
        //             "status_message"    => $status_message
        //         );

        //         $total_event_price += $counts['event'][$row['id']] * $row['price'];
        //         array_push($events, $data);

        //         if ($steps == '3' and isset($order_id) and $counts['event'][$row['id']] > 0) {
        //             $data = array(
        //                 "order_id"          => $order_id,
        //                 "event_id"          => $row['id'],
        //                 "event_title"       => $row['title'],
        //                 "event_price"       => $row['price'],
        //                 "event_time"        => $row['event_time'],
        //                 "event_quantity"    => $counts['event'][$row['id']]
        //             );
        //             $order_event_id = $this->ci->sellorder_model->add_order($data, 'event');

        //             // add event register
        //             for ($i=0; $i<$counts['event'][$row['id']]; $i++) {
        //                 // field format : user_name_[event id]_[number start at 1]
        //                 $user_name_field = 'user_name_' . $row['id'] . '_' . ($i+1);
        //                 $user_phone_field = 'user_phone_' . $row['id'] . '_' . ($i+1);
        //                 $data = array(
        //                     "order_id"      => $order_id,
        //                     "event_id"      => $row['id'],
        //                     "user_name"     => $this->ci->input->get_post($user_name_field),
        //                     "user_phone"    => $this->ci->input->get_post($user_phone_field)
        //                 );

        //                 // insert register to database
        //                 $this->ci->lib_event->add_event_user($data);
        //             }

        //             // update event table
        //             $this->ci->db->set('participants', 'participants + ' . $counts['event'][$row['id']], false)->update('events', NULL, array('id' => $row['id']));
        //         }
        //     }
        // }

        // $carts_array = get_object_vars($carts);

        // if (!empty($products_id)) {
            // $result = $this->ci->lib_product->select("files.file_name, products.*")->where('products.id', $products_id)->products()->result_array();
            foreach ($carts as $row) {
                $show_update_btn = true;
                $status_message = '';
                // if ($counts['product'][$row['id']] <= 0) {
                //     continue;
                // }

                // if ($row['inventory_amount'] <= 0) {
                //     $status_message = '<span style="color:red">商品已賣完</span>';
                //     $unit_menu = $status_message;
                //     $unit = $status_message;
                //     $is_status_change = true;
                //     $show_update_btn = false;
                //     $counts['product'][$row['id']] = 0;
                // } else if ($row['inventory_amount'] < $counts['product'][$row['id']]) {
                //     $status_message = '<span style="color:red">商品庫存不足</span>';
                //     $unit_menu = $this->ci->lib_sellcart->format_dropdown($row['inventory_amount'], 'product', $row['inventory_amount']);
                //     $unit = $row['inventory_amount'];
                //     $is_status_change = true;
                //     $counts['product'][$row['id']] = $row['inventory_amount'];
                // } else {
                //     $unit_menu = $this->ci->lib_sellcart->format_dropdown($row['inventory_amount'], 'product', $counts['product'][$row['id']]);
                //     $unit = $counts['product'][$row['id']];
                // }

                // if($row->shipping_size === "small"){
                //     $shipping_price = $this->_sell_charge_small;
                // }elseif($row->shipping_size === "medium"){
                //     $shipping_price = $this->_sell_charge_medium;
                // }elseif($row->shipping_size === "big"){
                //     $shipping_price = $this->_sell_charge_big;
                // }
                
                if ($steps == '4') {
                    $product_id_str = str_pad($product_id,3,'0',STR_PAD_LEFT);
                    // $product_item_number = $formdata['order_number'].$product_id_str;
                    $product_item_number = $order_number.$product_id_str;
                    // $products['product_item_number'] = $product_item_number;
                    // $this->lib_sellorder->update_order($order_id, array('products' => $row));
                    // $data->products = array_merge($data->products, array('product_item_number', $product_item_number));
                    $product_id++;
                }

                $data = array(
                    // "id"                => $row['id'],
                    // "image_url"         => image_url($row['file_name'], 72, 72),
                    // "title"             => $row['title'],
                    // "product_type"      => $row['product_type'],
                    // "product_brand"     => $row['product_brand'],
                    // "original_price"    => $row['original_price'],
                    // "sell_price"        => $row['sell_price'],
                    // "shipping_size"     => $row['shipping_size'],

                    "id"                => $row->id,
                    "title"             => $row->title,
                    "product_type"      => $row->product_type,
                    "product_brand"     => $row->product_brand,
                    "original_price"    => $row->original_price,
                    "sell_price"        => $row->sell_price,
                    // "shipping_size"     => $row->shipping_size,
                    "shipping_price"    => $row->shipping_price,
                    "product_note"      => $row->product_note,
                    "product_item_number" => $product_item_number,

                    // "unit_menu"         => $unit_menu,
                    // "unit"              => $unit,
                    // "total_price"       => $counts['product'][$row['id']] * $row['sell_price'],
                    // "show_update_btn"   => $show_update_btn,
                    "status_message"    => $status_message
                );

                // $total_original_product_price += $counts['product'][$row['id']] * $row['original_price'];
                // $total_product_price += $counts['product'][$row['id']] * $row['sell_price'];
                $total_shipping_price += $row->shipping_price;
                array_push($products, $data);
                // if ($steps == '4' and isset($order_id) and $counts['product'][$row['id']] > 0) {
                if ($steps == '4' and isset($order_id)) {
                    $data = array(
                        "order_id" => $order_id,
                        "product_id" => $row->id,
                        "product_title" => $row->title,
                        "product_type" => $row->product_type,
                        "product_brand" => $row->product_brand,
                        "product_item_number" => $product_item_number,
                        "product_original_price" => $row->original_price,
                        "product_sell_price" => $row->sell_price,
                        // "product_quantity" => $counts['product'][$row['id']]
                        "shipping_price" => $row->shipping_price,
                        "comments" => $row->product_note,
                        "donative" => $row->donative
                    );
                    $order_product_id = $this->ci->sellorder_model->add_order($data, 'product');
                    // update product inventory_amount
                    // $this->ci->db->set('inventory_amount', 'inventory_amount - ' . $counts['product'][$row['id']], false)->update('products', NULL, array('id' => $row['id']));
                }
            }
        // }

        // $total_price = $total_product_price + $total_event_price;
        // $total_price = 123018;
        $total_price += $total_shipping_price;

        // no transportation charge if register event or total product price > min price
        if ($total_price != 0 and ($total_price < $this->_min_price_less)) {
            $is_transportation_charge = true;
            $transportation_charge = $this->_transportation_charge_less;
            $total_price += $transportation_charge;
        } elseif ($total_price != 0 and ($total_price < $this->_min_price_more)) {
            $is_transportation_charge = true;
            $transportation_charge = $this->_transportation_charge_more;
            $total_price += $transportation_charge;
        }


        /*
        //special event, special price!
        $total_shipping_price = 299;
        $transportation_charge = 0;
        if (count($carts) > 3){
          $total_shipping_price = 299 + (count($carts) - 3) * 88;
        }
        */

        $data = array(
            "is_status_change"              => $is_status_change,
            "products"                      => $products,
            // "events"                        => $events,
            // "counts"                        => $counts,
            // "total_event_price"             => (int) $total_event_price,
            // "total_original_product_price"  => (int) $total_original_product_price,
            // "total_product_price"           => (int) $total_product_price,
            "total_shipping_price"          => (int) $total_shipping_price,
            "total_price"                   => (int) $total_shipping_price,//$total_price,
            "transportation_charge"         => ($is_transportation_charge) ? $transportation_charge : 0,
            "shipping_price_small"          => $this->_sell_charge_small,
            "shipping_price_medium"         => $this->_sell_charge_medium,
            "shipping_price_big"            => $this->_sell_charge_big
        );

        return $data;
    }

    /**
     * limit order time
     *
     * @return array
     * @author  appleboy
     **/
    public function limit_payment_time()
    {
        // get data from config table
        $credit_card_limit_day = (int) $this->ci->lib_config->item('credit_card_limit_day');
        $credit_card_limit_hour = (int) $this->ci->lib_config->item('credit_card_limit_hour');
        $atm_limit_day = (int) $this->ci->lib_config->item('atm_limit_day');
        $atm_limit_hour = (int) $this->ci->lib_config->item('atm_limit_hour');
        $web_atm_limit_day = (int) $this->ci->lib_config->item('web_atm_limit_day');
        $web_atm_limit_hour = (int) $this->ci->lib_config->item('web_atm_limit_hour');

        $limit = array(
            PAYMENT_CREDIT_CARD => array(
                'day' => $credit_card_limit_day,
                'hour' => $credit_card_limit_hour,
                'time' => (86400 * $credit_card_limit_day + 3600 * $credit_card_limit_hour)
            ),
            PAYMENT_ATM => array(
                'day' => $atm_limit_day,
                'hour' => $atm_limit_hour,
                'time' => (86400 * $atm_limit_day + 3600 * $atm_limit_hour)
            ),
            PAYMENT_WEB_ATM => array(
                'day' => $web_atm_limit_day,
                'hour' => $web_atm_limit_hour,
                'time' => (86400 * $web_atm_limit_day + 3600 * $web_atm_limit_hour)
            )
        );

        return $limit;
    }

    /**
     * Payment method
     *
     * @param   string
     * @return string
     * @author  appleboy
     **/
    public function payment_method($payment_method = 'credit-cart')
    {
        switch ($payment_method) {
            case 'credit-cart':
                $payment_method = '信用卡';
            break;
            case 'web-atm':
                $payment_method = 'WEB ATM';
            break;
            case 'atm':
                $payment_method = 'ATM 轉帳';
            break;
            default:
                $payment_method = '信用卡';
        }

        return $payment_method;
    }

    /**
     * Uniform invoice
     *
     * @param   array
     * @return string
     * @author  appleboy
     **/
    public function uniform_invoice($uniform_invoice = array())
    {
        $information = '';
        $uniform_invoice['uniform_invoice_type'] = $uniform_invoice['uniform_invoice_type'] ? $uniform_invoice['uniform_invoice_type'] : 0;

        if (!empty($uniform_invoice['uniform_invoice_title']) and !empty($uniform_invoice['uniform_invoice_number'])) {
            $information = '，' . $uniform_invoice['uniform_invoice_title'] . '，' . $uniform_invoice['uniform_invoice_number'];
        }

        switch ($uniform_invoice['uniform_invoice_type']) {
            case '0':
                $uniform_invoice_title = '捐贈發票(捐贈單位：創世基金會)';
            break;
            case '1':
                $uniform_invoice_title = '二聯式發票' . $information;
            break;
            case '2':
                $uniform_invoice_title = '三聯式發票' . $information;
            break;
        }

        return $uniform_invoice_title;
    }
}
