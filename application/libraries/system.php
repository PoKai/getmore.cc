<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class System
{
    /**
     * CodeIgniter global
     *
     * @var string
     **/
    protected $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->helper('url');
        $this->ci->load->library('session');
        $this->ci->load->spark('curl/1.2.1');
        $this->ci->load->config('sms');
    }

    /**
     *
     * Generate an activation code
     *
     * @param int
     * @param string
     * @return string
     */
    public function generate_code($length = 11, $type = 'auto')
    {
        $code = "";
        switch ($type) {
            case 'digit':
                $chars = '1234567890';
            break;
            case 'word':
                $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            break;
            default:
                $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            break;
        }

        srand((double) microtime()*1000000);
        for ($i=0; $i<$length; $i++) {
            $code .= substr ($chars, rand() % strlen($chars), 1);
        }

        return $code;
    }

    /**
     *
     * Parase url
     *
     * @param string
     * @param array
     * @return string
     */
    public function parse_url($url = NULL, $params_replace = array())
    {
        if (!isset($url))
            return NULL;

        $url_array = parse_url($url);

        if (!empty($url_array['query'])) {
            $params = explode('&', $url_array['query']);
            $retained_params = array();
            foreach ($params as $param) {
                foreach ($params_replace as $key => $value) {
                    if (strpos($param, $key . '=') !== false) {
                        $param = $key . '=' . $value;
                        break;
                    }
                }
                $retained_params[] = $param;
            }
        }
        if (!empty($retained_params)) {
            $query = '?'.implode($retained_params, '&');
        }

        return $url_array['scheme'] . '://' . $url_array['host'] . $url_array['path'] . $query;
    }

    /**
     *
     * output message
     *
     * @param string
     * @param string type: error, notice, info, success
     * @return string
     */
    public function message($message, $type = 'info', $set_flash = true)
    {
        $message = '<div class="alert alert-' . $type . '"><a class="close" data-dismiss="alert" type="button">×</a>' . $message . '</div>';

        if ($set_flash) {
            $this->ci->session->set_flashdata('message', $message);
        }

        return $message;
    }

    public function bite_str($string, $start, $len, $byte=3)
    {
        $str     = "";
        $count   = 0;
        $str_len = strlen($string);
        for ($i=0; $i<$str_len; $i++) {
            if (($count+1-$start)>$len) {
                $str  .= "...";
                break;
            } elseif ((ord(substr($string,$i,1)) <= 128) && ($count < $start)) {
                $count++;
            } elseif ((ord(substr($string,$i,1)) > 128) && ($count < $start)) {
                $count = $count+2;
                $i = $i+$byte-1;
            } elseif ((ord(substr($string,$i,1)) <= 128) && ($count >= $start)) {
                $str .= substr($string,$i,1);
                $count++;
            } elseif ((ord(substr($string,$i,1)) > 128) && ($count >= $start)) {
                $str  .= substr($string,$i,$byte);
                $count = $count+2;
                $i = $i+$byte-1;
            }
        }

        return $str;
    }

    /**
     *
     * set redirect url when user is not login
     *
     * @param string
     * @return void
     */
    public function redirect($redirect_url = NULL, $message = NULL)
    {
        if (isset($message)) {
            $this->ci->session->set_flashdata('message', $this->message($message, 'error'));
        }

        $redirect_url = (isset($redirect_url) AND !empty($redirect_url)) ? $redirect_url : current_url();

        redirect('/auth/login?redirect_url=' . urlencode($redirect_url), 'refresh');
    }

    /**
     * Diff time
     *
     * @time int
     * @return string
     * @author  appleboy
     **/
    public function get_time_out($time = '')
    {
        if (empty($time) or $time < time()) return '';

        $format = '';
        $time = $time - time();
        $day = floor($time/86400);
        $hour = floor(($time - 86400*$day)/3600);
        $min = floor(($time - 86400*$day - 3600*$hour)/60);

        $data = array(
            'day'   => $day,
            'hour'  => $hour,
            'min'   => $min
        );

        $format .= ($day > 0) ? '<span>' . $day . '</span>天' : '';
        $format .= ($hour > 0) ? '<span>' . $hour . '</span>小時' : '';
        $format .= ($day == 0 and $min > 0) ? '<span>' . $min . '</span>分' : '';

        return $format;
    }

    /**
     * Send SMS message to user
     *
     * @msgbody string
     * @return true/false
     * @author  danny
     **/
    public function send_sms_msg($phone_num='', $msgbody = '')
    {
        //$this->ci->config->item('sms_send_url');
        $data = array(
                        'username' => $this->ci->config->item('sms_send_username'),
                        'password' => $this->ci->config->item('sms_send_password'),
                        'encoding' => $this->ci->config->item('sms_send_encoding'),
                        'vldtime' => '3600',
                        'dstaddr' => $phone_num,
                        'smbody' => $msgbody    //urlencode($msgbody)
                    );

        return $this->ci->curl->simple_get($this->ci->config->item('sms_send_url'), $data, array('PORT' => 9600));
    }
}

/* End of file System.php */
/* Location: ./application/libraries/System.php */
