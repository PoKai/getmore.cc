<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name: Subject Library
*
* Author: appleboy
*
*/

class Lib_subject
{
    /**
     * CodeIgniter global
     *
     * @var string
     **/
    protected $ci;

    /**
     * extra where
     *
     * @var array
     **/
    public $_extra_where = array();

    /**
     * extra set
     *
     * @var array
     **/
    public $_extra_set = array();

    /**
     * default limit
     *
     * @var int
     **/
    public $_limit = 6;

    /**
     * __construct
     *
     * @return void
     * @author Ben
     **/
    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->library('system');
        $this->ci->load->model('subject_model');
    }

    /**
     * __call
     *
     * Acts as a simple way to call model methods without loads of stupid alias'
     *
     **/
    public function __call($method, $arguments)
    {
        if (!method_exists( $this->ci->subject_model, $method) )
        {
            throw new Exception('Undefined method subject::' . $method . '() called');
        }

        return call_user_func_array( array($this->ci->subject_model, $method), $arguments);
    }

    /**
     * get_subject_list
     *
     **/
    public function get_list($data)
    {
        if (isset($data['id']) and $data['id'] != 'all') {
            $data['id'] = intval($data['id']);
            ($data['id'] != 0) and $this->where('product_subjects.category_id', $data['id']);
        }

        $data['offset'] = (isset($data['offset'])) ? (int) $data['offset'] : 0;
        $data['limit'] = (isset($data['limit'])) ? (int) $data['limit'] : $this->_limit;

        // get product subject
        $this->offset($data['offset'])->limit($data['limit']);
        $rows = $this->select("files.file_name, category.tw_name as category_name, product_subjects.*, DATE_FORMAT(`up_time`, '%Y-%m-%d %H:%i') as up_time, DATE_FORMAT(`down_time`, '%Y-%m-%d %H:%i') as down_time")->where('`options`&' . (_IS_ENABLED) . ' >= 1')->order_by('up_time', 'DESC')->subjects()->result_array();

        $output = array();
        foreach ($rows as $row) {
            $down_time = ($row['down_time'] != '') ? strtotime($row['down_time']) : $row['edit_time'];
            $row['time'] = $this->ci->system->get_time_out($down_time);
            $row['image_path'] = image_url($row['file_name'], 258, 224);
            $row['message'] = (!empty($row['time'])) ? '剩 ' . $row['time'] . ' 將結束特價' : '優惠尚未開始或已經結束';
            array_push($output, $row);
        }

        $return = array(
            'count' => count($output),
            'output' => $output
        );

        return $return;
    }
}
