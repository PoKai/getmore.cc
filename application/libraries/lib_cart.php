<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name: Cart Library
*
* Author: appleboy
*
*/

class Lib_cart
{
    /**
     * CodeIgniter global
     *
     * @var string
     **/
    protected $ci;

    /**
     * extra where
     *
     * @var array
     **/
    public $_extra_where = array();

    /**
     * extra set
     *
     * @var array
     **/
    public $_extra_set = array();

    /**
     * __construct
     *
     * @return void
     * @author appleboy
     **/
    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->library(array('session', 'ion_auth'));
        $this->ci->load->model('cart_model');
        $this->time_diff = CART_TIME_DIFF;
    }

    /**
     * __call
     *
     * Acts as a simple way to call model methods without loads of stupid alias'
     *
     **/
    public function __call($method, $arguments)
    {
        if (!method_exists( $this->ci->cart_model, $method) )
        {
            throw new Exception('Undefined method cart::' . $method . '() called');
        }

        return call_user_func_array( array($this->ci->cart_model, $method), $arguments);
    }

    /**
     * User carts
     *
     * Returns the entire cart array
     *
     * @return array()
     * @author appleboy
     **/
    public function contents()
    {
        $carts = array();
        $time = time();
        $user_id = $this->ci->session->userdata('user_id');
        $session_carts = array(
            "time"  => (int) $this->ci->session->userdata('carts_time'),
            "item"  => ($this->ci->session->userdata('carts')) ? $this->ci->session->userdata('carts') : array()
        );

        $row = $this->ci->cart_model->where('user_id', $user_id)->carts()->row_array();

        // row not found, insert database
        if(empty($row))
        {
            if(empty($session_carts['item']) or !is_array($session_carts['item']))
            {
                return array();
            }
            $data = array(
                "carts"     => json_encode($session_carts['item']),
                "user_id"   => $user_id
            );
            $this->ci->cart_model->add_cart($data);
            $this->ci->session->set_userdata("carts", $session_carts['item']);
            $this->ci->session->set_userdata("carts_time", $time);

            return $session_carts['item'];
        }
        else
        {
            $item_time = $row['edit_time'];
            $update = false;
            $new_data = $session_carts['item'];
            $old_data = json_decode($row['carts']);

            if(($session_carts['time'] - $item_time > $this->time_diff) or ($item_time - $session_carts['time'] > $this->time_diff))
            {
                $update = true;
                foreach($new_data as $new)
                {
                    $insert = true;
                    if(!empty($old_data))
                    {
                        $i = 0;
                        foreach($old_data as $old)
                        {
                            if(isset($old->type) and $old->type == $new->type and $old->id == $new->id)
                            {
                                $old_data[$i] = $new;
                                $insert = false;
                                break 1; // end foreach
                            }
                            $i++;
                        }
                    }
                    if($insert) $old_data[] = $new;
                }
            }

            if($update)
            {
                // update carts table
                $this->ci->session->set_userdata("carts_time", $time);
                $this->ci->lib_cart->update_cart($row['id'], array("carts" => json_encode($old_data)));
            }
            // update carts session
            $this->ci->session->set_userdata("carts", $old_data);

            return $old_data;
        }
    }

    /**
     * cart dropdown
     *
     * Returns dropdown menu of product count.
     *
     * @access  public
     * @return  string
     */
    public function format_dropdown($value = 0, $type = 'product', $default = 1, $js = "")
    {
        if ($value <= 0) {
            return;
        }

        $max = ($type == 'product') ? PRODUCT_MAX_COUNT : EVENT_MAX_COUNT;

        $count = ($value < $max and !empty($value)) ? $value : $max;
        for($i=1;$i<=$count;$i++)
        {
            $options[$i] = $i;
        }

        return form_dropdown('order_count', $options, $default, $js);
    }

    /**
     * Format Number
     *
     * Returns the supplied number with commas and a decimal point.
     *
     * @access  public
     * @return  string
     */
    public function format_number($n = '', $decimals = 0)
    {
        if ($n == '')
        {
            return '';
        }

        // Remove anything that isn't a number or decimal point.
        $n = (float) $n;

        return number_format($n, $decimals, '.', ',');
    }
}
