<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * OpenID Library
 * Author: Bo-Yi Wu <appleboy.tw@gmail.com>
 * Date: 2011-10-21
 */

class Openid {

    // Set the instance variable
    protected $ci;
    protected $openid;

    function __construct()
    {
        // Get the instance
        $this->ci =& get_instance();
        $this->ci->load->library("session");
        $this->ci->load->library("ion_auth");
        $this->ci->load->library("system");

        // Set the include path and require the needed files
        set_include_path(get_include_path() . PATH_SEPARATOR . APPPATH . 'libraries/');
        require_once(APPPATH . 'libraries/OpenID/OpenID.php');

        $this->openid = new LightOpenID($this->ci->config->item('base_url'));
    }

    public function login_google()
    {
        if(!$this->openid->mode)
        {
            $this->openid->identity = 'https://www.google.com/accounts/o8/id';
            $this->openid->required = array(
                'namePerson/first',
                'namePerson/last',
                'contact/email',
                'pref/language',
                'contact/country/home'
            );
            header('Location: ' . $this->openid->authUrl());
        }

        if($this->openid->validate())
        {

            $identity = $this->openid->identity;
            $attributes = $this->openid->getAttributes();
            $email = $attributes['contact/email'];
            $first_name = $attributes['namePerson/first'];
            $last_name = $attributes['namePerson/last'];

            $data = array(
                'email' => $email,
                'first_name' => $first_name,
                'last_name' => $last_name
            );
            $this->ci->ion_auth->other_login($email, $data);
        }
        else
        {
            // clean login type
            $this->ci->session->unset_userdata('login_type');
        }
        $redirect_url = urldecode($this->ci->session->userdata('redirect_url'));
        redirect($redirect_url, 'refresh');
    }
}
/* End of file openid.php */
/* Location: ./application/libraries/openid.php */
