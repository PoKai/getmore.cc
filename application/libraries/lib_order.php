<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name: Order Library
*
* Author: appleboy
*
*/

class Lib_order
{
    /**
     * CodeIgniter global
     *
     * @var string
     **/
    protected $ci;

    /**
     * extra where
     *
     * @var array
     **/
    public $_extra_where = array();

    /**
     * extra set
     *
     * @var array
     **/
    public $_extra_set = array();

    /**
     * Min price
     *
     * @var int
     **/
    public $_min_price = null;

    /**
     * Transportation Charge
     *
     * @var int
     **/
    public $_transportation_charge = null;

    /**
     * __construct
     *
     * @return void
     * @author appleboy
     **/
    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->library(array('session', 'ion_auth', 'lib_cart', 'lib_event', 'lib_product', 'lib_config'));
        $this->ci->load->model('order_model');
        $this->time_diff = CART_TIME_DIFF;

        $this->_transportation_charge = (int) $this->ci->lib_config->item('transportation_charge');
        $this->_min_price = (int) $this->ci->lib_config->item('min_price');
    }

    /**
     * __call
     *
     * Acts as a simple way to call model methods without loads of stupid alias'
     *
     **/
    public function __call($method, $arguments)
    {
        if (!method_exists( $this->ci->order_model, $method) ) {
            throw new Exception('Undefined method order::' . $method . '() called');
        }

        return call_user_func_array( array($this->ci->order_model, $method), $arguments);
    }

    /**
     * User Order list
     *
     * @return array
     * @author appleboy
     **/
    public function user_order($steps = 1, $order_id = NULL)
    {
        //initial value
        $is_transportation_charge = false;
        $is_status_change = false;

        $data = $counts = $events = $products = $events_id = $products_id = array();
        $total_price = $total_event_price = $total_original_product_price = $total_product_price = 0;

        // get user carts
        $carts = $this->ci->lib_cart->contents();

        // if carts is empty, redirect to index
        if (empty($carts)) {
            return array();
        }

        foreach ($carts as $row) {
            if (isset($row->type)) {
                if ($row->type == 'product') {
                    array_push($products_id, $row->id);
                } elseif ($row->type == 'event') {
                    array_push($events_id, $row->id);
                }
                $counts[$row->type][$row->id] = $row->count;
            }
        }

        if (!empty($events_id)) {
            $result = $this->ci->lib_event->select("files.file_name, events.*")->where('events.id', $events_id)->where('`options`&' . (_IS_INDEX) . ' >= 1')->where('`options`&' . (_IS_ENABLED) . ' >= 1')->events()->result_array();
            foreach ($result as $row) {
                $show_update_btn = true;
                $status_message = '';
                $people_left = $row['peoples'] - $row['participants'];

                if (!empty($row['deadline_time']) and strtotime($row['deadline_time']) < time()) {
                    $status_message = '<span style="color:red">報名時間截止</span>';
                    $unit_menu = $status_message;
                    $unit = $status_message;
                    $is_status_change = true;
                    $show_update_btn = false;
                    $counts['event'][$row['id']] = 0;
                } else if ($people_left <= 0) {
                    $status_message = '<span style="color:red">課程已額滿</span>';
                    $unit_menu = $status_message;
                    $unit = $status_message;
                    $is_status_change = true;
                    $show_update_btn = false;
                    $counts['event'][$row['id']] = 0;
                } else if ($people_left < $counts['event'][$row['id']]) {
                    $status_message = '<span style="color:red">報名人數不足</span>';
                    $unit_menu = $this->ci->lib_cart->format_dropdown($people_left, 'event', $people_left);
                    $unit = $people_left;
                    $is_status_change = true;
                    $counts['event'][$row['id']] = $people_left;
                } else {
                    $unit_menu = $this->ci->lib_cart->format_dropdown($people_left, 'event', $counts['event'][$row['id']]);
                    $unit = $counts['event'][$row['id']];
                }

                $data = array(
                    "id"                => $row['id'],
                    "image_url"         => image_url($row['file_name'], 72, 72),
                    "title"             => $row['title'],
                    "event_time"        => date("Y-m-d H:i", strtotime($row['event_time'])),
                    "price"             => $row['price'],
                    "unit_menu"         => $unit_menu,
                    "unit"              => $unit,
                    "total_price"       => $counts['event'][$row['id']] * $row['price'],
                    "show_update_btn"   => $show_update_btn,
                    "status_message"    => $status_message
                );

                $total_event_price += $counts['event'][$row['id']] * $row['price'];
                array_push($events, $data);

                if ($steps == '3' and isset($order_id) and $counts['event'][$row['id']] > 0) {
                    $data = array(
                        "order_id"          => $order_id,
                        "event_id"          => $row['id'],
                        "event_title"       => $row['title'],
                        "event_price"       => $row['price'],
                        "event_time"        => $row['event_time'],
                        "event_quantity"    => $counts['event'][$row['id']]
                    );
                    $order_event_id = $this->ci->order_model->add_order($data, 'event');

                    // add event register
                    for ($i=0; $i<$counts['event'][$row['id']]; $i++) {
                        // field format : user_name_[event id]_[number start at 1]
                        $user_name_field = 'user_name_' . $row['id'] . '_' . ($i+1);
                        $user_phone_field = 'user_phone_' . $row['id'] . '_' . ($i+1);
                        $data = array(
                            "order_id"      => $order_id,
                            "event_id"      => $row['id'],
                            "user_name"     => $this->ci->input->get_post($user_name_field),
                            "user_phone"    => $this->ci->input->get_post($user_phone_field)
                        );

                        // insert register to database
                        $this->ci->lib_event->add_event_user($data);
                    }

                    // update event table
                    $this->ci->db->set('participants', 'participants + ' . $counts['event'][$row['id']], false)->update('events', NULL, array('id' => $row['id']));
                }
            }
        }

        if (!empty($products_id)) {
            $result = $this->ci->lib_product->select("files.file_name, products.*")->where('products.id', $products_id)->products()->result_array();
            foreach ($result as $row) {
                $show_update_btn = true;
                $status_message = '';
                if ($counts['product'][$row['id']] <= 0) {
                    continue;
                }

                if ($row['inventory_amount'] <= 0) {
                    $status_message = '<span style="color:red">商品已賣完</span>';
                    $unit_menu = $status_message;
                    $unit = $status_message;
                    $is_status_change = true;
                    $show_update_btn = false;
                    $counts['product'][$row['id']] = 0;
                } else if ($row['inventory_amount'] < $counts['product'][$row['id']]) {
                    $status_message = '<span style="color:red">商品庫存不足</span>';
                    $unit_menu = $this->ci->lib_cart->format_dropdown($row['inventory_amount'], 'product', $row['inventory_amount']);
                    $unit = $row['inventory_amount'];
                    $is_status_change = true;
                    $counts['product'][$row['id']] = $row['inventory_amount'];
                } else {
                    $unit_menu = $this->ci->lib_cart->format_dropdown($row['inventory_amount'], 'product', $counts['product'][$row['id']]);
                    $unit = $counts['product'][$row['id']];
                }

                $data = array(
                    "id"                => $row['id'],
                    "image_url"         => image_url($row['file_name'], 72, 72),
                    "title"             => $row['title'],
                    "original_price"    => $row['original_price'],
                    "sell_price"        => $row['sell_price'],
                    "unit_menu"         => $unit_menu,
                    "unit"              => $unit,
                    "total_price"       => $counts['product'][$row['id']] * $row['sell_price'],
                    "show_update_btn"   => $show_update_btn,
                    "status_message"    => $status_message
                );

                $total_original_product_price += $counts['product'][$row['id']] * $row['original_price'];
                $total_product_price += $counts['product'][$row['id']] * $row['sell_price'];
                array_push($products, $data);

                if ($steps == '3' and isset($order_id) and $counts['product'][$row['id']] > 0) {
                    $data = array(
                        "order_id" => $order_id,
                        "product_id" => $row['id'],
                        "product_title" => $row['title'],
                        "product_item_number" => $row['item_number'],
                        "product_original_price" => $row['original_price'],
                        "product_sell_price" => $row['sell_price'],
                        "product_quantity" => $counts['product'][$row['id']]
                    );
                    $order_product_id = $this->ci->order_model->add_order($data, 'product');
                    // update product inventory_amount
                    $this->ci->db->set('inventory_amount', 'inventory_amount - ' . $counts['product'][$row['id']], false)->update('products', NULL, array('id' => $row['id']));
                }
            }
        }

        $total_price = $total_product_price + $total_event_price;

        // no transportation charge if register event or total product price > min price
        if ($total_product_price != 0 and ($total_price < $this->_min_price)) {
            $is_transportation_charge = true;
            $total_price += $this->_transportation_charge;
        }

        $data = array(
            "is_status_change"              => $is_status_change,
            "products"                      => $products,
            "events"                        => $events,
            "counts"                        => $counts,
            "total_event_price"             => (int) $total_event_price,
            "total_original_product_price"  => (int) $total_original_product_price,
            "total_product_price"           => (int) $total_product_price,
            "total_price"                   => (int) $total_price,
            "transportation_charge"         => ($is_transportation_charge) ? $this->_transportation_charge : 0
        );

        return $data;
    }

    /**
     * limit order time
     *
     * @return array
     * @author  appleboy
     **/
    public function limit_payment_time()
    {
        // get data from config table
        $credit_card_limit_day = (int) $this->ci->lib_config->item('credit_card_limit_day');
        $credit_card_limit_hour = (int) $this->ci->lib_config->item('credit_card_limit_hour');
        $atm_limit_day = (int) $this->ci->lib_config->item('atm_limit_day');
        $atm_limit_hour = (int) $this->ci->lib_config->item('atm_limit_hour');
        $web_atm_limit_day = (int) $this->ci->lib_config->item('web_atm_limit_day');
        $web_atm_limit_hour = (int) $this->ci->lib_config->item('web_atm_limit_hour');

        $limit = array(
            PAYMENT_CREDIT_CARD => array(
                'day' => $credit_card_limit_day,
                'hour' => $credit_card_limit_hour,
                'time' => (86400 * $credit_card_limit_day + 3600 * $credit_card_limit_hour)
            ),
            PAYMENT_ATM => array(
                'day' => $atm_limit_day,
                'hour' => $atm_limit_hour,
                'time' => (86400 * $atm_limit_day + 3600 * $atm_limit_hour)
            ),
            PAYMENT_WEB_ATM => array(
                'day' => $web_atm_limit_day,
                'hour' => $web_atm_limit_hour,
                'time' => (86400 * $web_atm_limit_day + 3600 * $web_atm_limit_hour)
            )
        );

        return $limit;
    }

    /**
     * Payment method
     *
     * @param   string
     * @return string
     * @author  appleboy
     **/
    public function payment_method($payment_method = 'credit-cart')
    {
        switch ($payment_method) {
            case 'credit-cart':
                $payment_method = '信用卡';
            break;
            case 'web-atm':
                $payment_method = 'WEB ATM';
            break;
            case 'atm':
                $payment_method = 'ATM 轉帳';
            break;
            default:
                $payment_method = '信用卡';
        }

        return $payment_method;
    }

    /**
     * Uniform invoice
     *
     * @param   array
     * @return string
     * @author  appleboy
     **/
    public function uniform_invoice($uniform_invoice = array())
    {
        $information = '';
        $uniform_invoice['uniform_invoice_type'] = $uniform_invoice['uniform_invoice_type'] ? $uniform_invoice['uniform_invoice_type'] : 0;

        if (!empty($uniform_invoice['uniform_invoice_title']) and !empty($uniform_invoice['uniform_invoice_number'])) {
            $information = '，' . $uniform_invoice['uniform_invoice_title'] . '，' . $uniform_invoice['uniform_invoice_number'];
        }

        switch ($uniform_invoice['uniform_invoice_type']) {
            case '0':
                $uniform_invoice_title = '捐贈發票(捐贈單位：創世基金會)';
            break;
            case '1':
                $uniform_invoice_title = '二聯式發票' . $information;
            break;
            case '2':
                $uniform_invoice_title = '三聯式發票' . $information;
            break;
        }

        return $uniform_invoice_title;
    }
}
