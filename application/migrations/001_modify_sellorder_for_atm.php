<?php

class Migration_Modify_sellorder_for_atm extends CI_Migration {
    public function up(){
        $fields = array(
            'last_five_number' => array(
                'type' => 'VARCHAR',
                'constraint' => 5, 
            ),
            'estimated_remit_datetime' => array(
                'type' => 'datetime'
            )
        );
        $this->dbforge->add_column('sellorders', $fields);
        
        $fields2 = array(
            'status_id' => array(
                'type' => 'FLOAT',
            ),
        );
        $this->dbforge->modify_column('sellorders', $fields2);        
    }
 
    public function down(){
        $this->dbforge->drop_column('sellorders', 'last_five_number');
        $this->dbforge->drop_column('sellorders', 'estimated_remit_datetime');
    }
}
