<?php

class Migration_Modify_buyorder_for_coupon_amount extends CI_Migration {
    public function up(){
        $fields = array(
            'coupon_amount' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8
            )
        );
        $this->dbforge->add_column('buy_order', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('buy_order', 'coupon_amount');
    }
}
