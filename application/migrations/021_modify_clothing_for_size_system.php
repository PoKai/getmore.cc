<?php

class Migration_Modify_clothing_for_size_system extends CI_Migration {
    public function up(){
        $fields = array(
            'size_type_major' => array(
                'type' => 'VARCHAR',
                'constraint' => 16
            ),
            'size_standard' => array(
                'type' => 'VARCHAR',
                'constraint' => 4
            ),
            'size_type_minor' => array(
                'type' => 'VARCHAR',
                'constraint' => 16
            ),
            'size_number_minor' => array(
                'type' => 'VARCHAR',
                'constraint' => 16
            ),
        );
        $this->dbforge->add_column('product_clothing', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('product_clothing', 'size_type_major');
        $this->dbforge->drop_column('product_clothing', 'size_standard');
        $this->dbforge->drop_column('product_clothing', 'size_type_minor');
        $this->dbforge->drop_column('product_clothing', 'size_number_minor');
    }
}
