<?php

class Migration_Modify_sellorders_for_final_price extends CI_Migration {
    public function up(){
        $fields = array(
            'final_price' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8
            )
        );
        $this->dbforge->add_column('sellorders', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('sellorders', 'final_price');
    }
}
