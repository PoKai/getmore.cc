<?php

class Migration_Create_article extends CI_Migration {
    public function up(){
        $fields = array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8,
                'auto_increment' => TRUE
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => 32
            ),
            'content' => array(
                'type' => 'TEXT'
            ),
            'feature_pic_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8
            ),
            'created_at' => array(
                'type' => 'datetime'
            )            
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);        
        $this->dbforge->create_table('articles');
    }
 
    public function down(){
        $this->dbforge->drop_table('articles');
    }
}
