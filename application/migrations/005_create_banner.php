<?php

class Migration_Create_banner extends CI_Migration {
    public function up(){
        $fields = array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8,
                'auto_increment' => TRUE
            ),
            'file_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'href' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'order' => array(
                'type' => 'INT',
                'constraint' => 3
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);        
        $this->dbforge->create_table('banners');
    }
 
    public function down(){
        $this->dbforge->drop_table('banners');
    }
}
