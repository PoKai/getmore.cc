<?php

class Migration_modify_coupon_for_name_and_time extends CI_Migration {
    public function up(){
        $fields = array(
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 32
            ),
            'created_at' => array(
                'type' => 'datetime'
            )
        );
        $this->dbforge->add_column('coupons', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('coupons', 'name');
        $this->dbforge->drop_column('coupons', 'created_at');
    }
}
