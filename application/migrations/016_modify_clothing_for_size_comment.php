<?php

class Migration_Modify_clothing_for_size_comment extends CI_Migration {
    public function up(){
        $fields = array(
            'size_comment' => array(
                'type' => 'VARCHAR',
                'constraint' => 10
            )
        );
        $this->dbforge->add_column('product_clothing', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('product_clothing', 'size_comment');
    }
}
