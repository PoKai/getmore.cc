<?php

class Migration_Create_clothing_like extends CI_Migration {
    public function up(){
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 10,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 10
            ),
            'clothing_id' => array(
                'type' => 'INT',
                'constraint' => 10
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('clothing_likes');
    }
 
    public function down(){
        $this->dbforge->drop_table('clothing_likes');
    }
}
