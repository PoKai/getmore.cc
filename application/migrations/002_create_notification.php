<?php

class Migration_Create_notification extends CI_Migration {
    public function up(){
        $fields = array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8,
                'auto_increment' => TRUE
            ),
            'clothing_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8
            ),
            'owner_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8
            ),
            'notified' => array(
                'type' => 'BOOLEAN'
            ),            
            'created_at' => array(
                'type' => 'datetime'
            )            
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('notifications');
    }
 
    public function down(){
        $this->dbforge->drop_table('notifications');
    }
}
