<?php

class Migration_Create_sellcoupon extends CI_Migration {
    public function up(){
        $fields = array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8,
                'auto_increment' => TRUE
            ),
            'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 32
            ),
            'amount' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'expired_at' => array(
                'type' => 'DATETIME'
            ),
            'times_available' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'times_used' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'creator_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8
            ),
            'minimum_charge' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 32
            ),
            'created_at' => array(
                'type' => 'datetime'
            )            
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);        
        $this->dbforge->create_table('sellcoupons');
    }
 
    public function down(){
        $this->dbforge->drop_table('sellcoupons');
    }
}
