<?php

class Migration_support_article_category extends CI_Migration {
    public function up(){
        $fields = array(
            'category_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8
            )
        );
        $this->dbforge->add_column('articles', $fields);

        $fields = array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 32
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);        
        $this->dbforge->create_table('article_categories');
    }
 
    public function down(){
        $this->dbforge->drop_column('articles', 'category_id');
        $this->dbforge->drop_table('article_categories');        
    }
}
