<?php

class Migration_Create_system_configuration extends CI_Migration {
    public function up(){
        $fields = array(
            'free_shipping' => array(
                'type' => 'BOOLEAN'
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->create_table('system_configuration');
    }
 
    public function down(){
        $this->dbforge->drop_table('system_configuration');
    }
}
