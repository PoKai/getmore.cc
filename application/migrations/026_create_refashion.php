<?php

class Migration_Create_refashion extends CI_Migration {
    public function up(){
        $fields = array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8,
                'auto_increment' => TRUE
            ),
            'q1' => array(
                'type' => 'TEXT'
            ),
            'q2' => array(
                'type' => 'TEXT'
            ),
            'q3' => array(
                'type' => 'TEXT'
            ),
            'story' => array(
                'type' => 'TEXT'
            ),
            'file_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8
            ),
            'description' => array(
                'type' => 'TEXT'
            ),
            'user_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8
            ),
            'created_at' => array(
                'type' => 'datetime'
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);        
        $this->dbforge->create_table('refashions');
    }
 
    public function down(){
        $this->dbforge->drop_table('refashions');
    }
}
