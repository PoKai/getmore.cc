<?php

class Migration_Modify_article_for_intro extends CI_Migration {
    public function up(){
        $fields = array(
            'intro' => array(
                'type' => 'TEXT'
            )
        );
        $this->dbforge->add_column('articles', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('articles', 'intro');
    }
}
