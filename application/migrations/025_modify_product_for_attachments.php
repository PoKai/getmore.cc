<?php

class Migration_Modify_product_for_attachments extends CI_Migration {
    public function up(){
        $fields = array(
            'has_tag' => array(
                'type' => 'BOOLEAN'
            ),
            'has_accessory' => array(
                'type' => 'VARCHAR',
                'constraint' => 32
            )
        );
        $this->dbforge->add_column('sellorders_products', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('sellorders_products', 'has_tag');
        $this->dbforge->drop_column('sellorders_products', 'has_accessory');
    }
}
