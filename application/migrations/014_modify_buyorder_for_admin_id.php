<?php

class Migration_Modify_buyorder_for_admin_id extends CI_Migration {
    public function up(){
        $fields = array(
            'admin_id' => array(
                'type' => 'INT',
                'constraint' => 10
            )
        );
        $this->dbforge->add_column('buy_order', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('buy_order', 'admin_id');
    }
}
