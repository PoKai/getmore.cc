<?php

class Migration_Modify_sellorder_for_coupon_amount_and_code extends CI_Migration {
    public function up(){
        $fields = array(
            'coupon_amount' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8
            ),
            'coupon_code' => array(
                'type' => 'VARCHAR',
                'constraint' => 32
            )
        );
        $this->dbforge->add_column('sellorders', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('sellorders', 'coupon_amount');
        $this->dbforge->drop_column('sellorders', 'coupon_code');
    }
}
