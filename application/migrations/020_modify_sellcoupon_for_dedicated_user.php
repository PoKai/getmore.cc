<?php

class Migration_Modify_sellcoupon_for_dedicated_user extends CI_Migration {
    public function up(){
        $fields = array(
            'dedicated_user_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8
            )
        );
        $this->dbforge->add_column('sellcoupons', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('sellcoupons', 'dedicated_user_id');
    }
}
