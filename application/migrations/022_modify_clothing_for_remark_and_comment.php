<?php

class Migration_Modify_clothing_for_remark_and_comment extends CI_Migration {
    public function up(){
        $fields = array(
            'admin_comment' => array(
                'type' => 'VARCHAR',
                'constraint' => 64
            ),
            'admin_remark' => array(
                'type' => 'VARCHAR',
                'constraint' => 64
            )
        );
        $this->dbforge->add_column('product_clothing', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('product_clothing', 'admin_comment');
        $this->dbforge->drop_column('product_clothing', 'admin_remark');
    }
}
