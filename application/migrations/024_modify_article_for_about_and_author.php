<?php

class Migration_Modify_article_for_about_and_author extends CI_Migration {
    public function up(){
        $fields = array(
            'author' => array(
                'type' => 'VARCHAR',
                'constraint' => 32
            ),
            'about' => array(
                'type' => 'TEXT'
            )
        );
        $this->dbforge->add_column('articles', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('articles', 'author');
        $this->dbforge->drop_column('articles', 'about');
    }
}
