<?php

class Migration_Modify_article_for_preview extends CI_Migration {
    public function up(){
        $fields = array(
            'published' => array(
                'type' => 'BOOLEAN'
            )
        );
        $this->dbforge->add_column('articles', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('articles', 'published');
    }
}
