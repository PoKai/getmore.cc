<?php

class Migration_Modify_clothing_for_level extends CI_Migration {
    public function up(){
        $fields = array(
            'level' => array(
                'type' => 'TINYINT',
                'constraint' => 4
            )
        );
        $this->dbforge->add_column('product_clothing', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('product_clothing', 'level');
    }
}
