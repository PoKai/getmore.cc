<?php

class Migration_Add_publication_time extends CI_Migration {
    public function up(){
        $fields = array(
            'publication_time' => array(
                'type' => 'DATETIME'
            )
        );

        $this->dbforge->add_column('product_clothing', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('product_clothing', 'publication_time');
    }
}
