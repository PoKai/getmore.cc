<?php

class Migration_Modify_banner_for_new_tab extends CI_Migration {
    public function up(){
        $fields = array(
            'open_in_new_tab' => array(
                'type' => 'BOOLEAN'
            )
        );
        $this->dbforge->add_column('banners', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('banners', 'open_in_new_tab');
    }
}
