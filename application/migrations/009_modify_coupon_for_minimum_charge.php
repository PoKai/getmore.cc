<?php

class Migration_modify_coupon_for_minimum_charge extends CI_Migration {
    public function up(){
        $fields = array(
            'minimum_charge' => array(
                'type' => 'INT',
                'constraint' => 11
            )
        );
        $this->dbforge->add_column('coupons', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('coupons', 'minimum_charge');
    }
}
