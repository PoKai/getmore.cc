<?php

class Migration_Create_brand extends CI_Migration {
    public function up(){
        $fields = array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => 8,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 32
            ),
            'approved' => array(
                'type' => 'BOOLEAN'
            ),
            'created_at' => array(
                'type' => 'datetime'
            )
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);        
        $this->dbforge->create_table('brands');
    }
 
    public function down(){
        $this->dbforge->drop_table('brands');
    }
}
