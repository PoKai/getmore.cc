<?php

class Migration_Modify_clothing_for_on_shelf extends CI_Migration {
    public function up(){
        $fields = array(
            'on_shelf' => array(
                'type' => 'BOOLEAN',
                'default' => 1
            )
        );
        $this->dbforge->add_column('product_clothing', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('product_clothing', 'on_shelf');
    }
}
