<?php

class Migration_Modify_clothing_for_multiple_level extends CI_Migration {
    public function up(){
        $this->dbforge->drop_column('product_clothing', 'level');

        $fields = array(
            'level' => array(
                'type' => 'VARCHAR',
                'constraint' => 32
            )
        );
        $this->dbforge->add_column('product_clothing', $fields);
    }
 
    public function down(){
        //lazy
    }
}
