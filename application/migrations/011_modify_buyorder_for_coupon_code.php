<?php

class Migration_Modify_buyorder_for_coupon_code extends CI_Migration {
    public function up(){
        $fields = array(
            'coupon_code' => array(
                'type' => 'VARCHAR',
                'constraint' => 32
            )
        );
        $this->dbforge->add_column('buy_order', $fields);
    }
 
    public function down(){
        $this->dbforge->drop_column('buy_order', 'coupon_code');
    }
}
