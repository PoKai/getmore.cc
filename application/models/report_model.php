<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_model extends JR_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function buy_order($begin_time, $end_time, $payment, $user_id){
        $fields = array('o.id', 'o.order_number', 'o.created_at', 'o.buyer_name', 'o.user_id', 'o.items_count', 'o.items_price', 'o.shipping_price', 'o.coupon_amount', 'o.deduction_amount', 'o.payment_method');

        $query = $this->db->from('buy_order o')->select($fields)->join('users u', 'u.id = o.user_id', 'left')->where('o.status', 5);

        if ($begin_time !== '') {
            $query->where('o.created_at >=', $begin_time);
        }

        if($end_time !== '') {
            $query->where('o.created_at <=', $end_time);
        }

        if ($payment !== 'all') {
            $query->where('o.payment_method', $payment);
        }

        if ($user_id !== '') {
            $query->where('o.user_id', $user_id);
        }
        $query->order_by('o.created_at');

        return $query->get()->result_array();
    }

    public function product_clothing($begin_time, $end_time, $product_type){
        $fields = array('c.id', 's.shipping_wash_id', 'c.product_item_number', 'c.product_title', 'c.product_brand', 'c.product_type');

        $query = $this->db->from('product_clothing c')->select($fields)->join('sellorders_products s', 'c.product_id = s.id')->where('c.on_shelf', 1)->where_in('c.lock_status', array(0, 1));

        if ($begin_time !== '') {
            $query->where('c.publication_time >=', $begin_time);
        }

        if($end_time !== '') {
            $query->where('c.publication_time <=', $end_time);
        }

        if ($product_type !== '') {
            $query->where('c.product_type', $product_type);
        }

        return $query->get()->result_array();
    }

    public function return_request($begin_time, $end_time, $user_id, $status){
        $fields = array('r.id', 'r.order_number', 'r.created_at', 'b.user_id', 'b.buyer_name', 'COUNT(r.item_number) as count', 'SUM(r.price) as total_price', 'b.status = 6 as total_return');

        $query = $this->db->from('return_requests r')->select($fields)->join('buy_order b', 'r.order_number = b.order_number')->group_by('r.order_number');

        if ($begin_time !== '') {
            $query->where('r.created_at >=', $begin_time);
        }

        if($end_time !== '') {
            $query->where('r.created_at <=', $end_time);
        }

        if ($user_id !== '') {
            $query->where('b.user_id', $user_id);
        }

        switch ($status) {
            case 'all':
                $query->where('b.status', 6);
                break;
            case 'part':
                $query->where('b.status !=', 6);
                break;
        }

        return $query->get()->result_array();
    }

    public function selled_products($begin_time, $end_time, $product_type){
        $fields = array('c.id', 's.shipping_wash_id', 'c.product_item_number', 'c.product_title', 'c.product_brand', 'c.product_sell_price', 'o.user_id as sell_user', 'b.user_id as buy_user', 'c.product_type');

        $query = $this->db->from('product_clothing c')->select($fields)->join('sellorders_products s', 'c.product_id = s.id')->join('sellorders o', 's.order_id = o.id')->join('buy_order b', 'b.id = c.buy_order_id')->where('c.on_shelf', 0)->where('c.lock_status', 2);

        if ($begin_time !== '') {
            $query->where('c.last_locked_time >=', $begin_time);
        }

        if($end_time !== '') {
            $query->where('c.last_locked_time <=', $end_time);
        }

        if ($product_type !== '') {
            $query->where('c.product_type', $product_type);
        }

        return $query->get()->result_array();
    }

    public function sellorders($begin_time, $end_time, $user_id){
        $fields = array('o.id', 'o.date_purchased', 'o.order_number', 'o.user_id', 'o.user_name', 'COUNT(1) as count', 'o.total_price', 'o.transportation_charge', 'o.coupon_amount', 'o.deduction_amount', 'u.sell_count');

        $query = $this->db->from('sellorders o')->select($fields)->join('sellorders_products p', 'o.id = p.order_id')->join('users u', 'u.id = o.user_id', 'left')->group_by('o.order_number')->where('o.status_id', 6);

        if ($begin_time !== '') {
            $query->where('o.date_purchased >=', $begin_time);
        }

        if($end_time !== '') {
            $query->where('o.date_purchased <=', $end_time);
        }

        if ($user_id !== '') {
            $query->where('o.user_id', $user_id);
        }

        return $query->get()->result_array();
    }
}
