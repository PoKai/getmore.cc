<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Config_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tables['config'] = 'config';
    }

    /**
     * configs
     *
     * @return object
     * @author appleboy
     **/
    public function configs()
    {
        // define in MY_Model
        $this->handle_process();

        $this->response = $this->db->get($this->tables['config']);

        return $this;
    }

    /**
     * config
     *
     * @return object
     * @author appleboy
     **/
    public function config($id = NULL)
    {
        $this->limit(1);
        $this->where($this->tables['config'].'.id', $id);

        $this->configs();

        return $this;
    }

    /**
     * Add config
     *
     * @return bool
     * @author appleboy
     **/
    public function add_config($data = array())
    {
        if(empty($data))

            return false;

        // Config table.
        $data = array(
            'type'       => $data['type'],
            'tw_name'    => $data['tw_name'],
            'en_name'    => $data['en_name'],
            'sort_order' => $data['sort_order'],
            'add_time'   => time(),
            'edit_time'  => time()
        );

        $this->db->insert($this->tables['config'], $data);
        $id = $this->db->insert_id();

        return $id;
    }

    /**
     * update config
     *
     * @return bool
     * @author appleboy
     **/
    public function update_config($name, $data = array())
    {
        if(empty($name) or empty($data))

            return false;

        $data = array_merge($data, array('edit_time' => time()));
        $this->db->where('name', $name)->update($this->tables['config'], $data);

        return true;
    }

    /**
    * delete config
    *
    * @return bool
    * @author appleboy
    **/
    public function delete_config($id)
    {
        $this->db->trans_begin();

        if(is_array($id))
           $this->db->where_in('id', $id);
        else
           $this->db->where('id', $id);

        $this->db->delete($this->tables['config']);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            return FALSE;
        }

        $this->db->trans_commit();

        return TRUE;
    }
}
