<?php

class Edm_vip_model extends JR_Model {
    
    function __construct(){
        parent::__construct();
        $this->load->database();
        
    }
    
    public function get_all_count(){
        return $this->db->count_all('edm_vip');
    }

    public function insert_data($data){
        $this->db->insert('edm_vip', $data);
    }

    public function has_register($user_id){
        return $this->db->where('user_id', $user_id)->count_all_results('edm_vip');
    }
}
