<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tables['event'] = 'events';
        $this->tables['event_user'] = 'events_users';
        $this->tables['user'] = 'users';
        $this->tables['order'] = 'orders';
        $this->tables['category'] = 'category';
        $this->tables['files'] = 'files';
    }

    /**
     * events
     *
     * @return object Categories
     * @author appleboy
     **/
    public function events()
    {
        // define in MY_Model
        $this->handle_process();

        // left join category
        $this->db->join($this->tables['category'], $this->tables['category'] . '.id = ' . $this->tables['event'] . '.category_id', 'left');
        $this->db->join($this->tables['files'], $this->tables['files'] . '.file_id = SUBSTRING_INDEX(' . $this->tables['event'] . '.file_list, ",", 1)', 'left');
        $this->response = $this->db->get($this->tables['event']);

        return $this;
    }

    /**
     * event
     *
     * @return object
     * @author appleboy
     **/
    public function event($id = NULL)
    {
        $this->limit(1);
        $this->where($this->tables['event'].'.id', $id);

        $this->events();

        return $this;
    }

    /**
     * Add event
     *
     * @return bool
     * @author appleboy
     **/
    public function add_event($data = array())
    {
        if(empty($data))

            return false;

        $options = 0x00;
        if ($data['is_index'] == '1')
            $options = ($options | _IS_INDEX);
        if ($data['is_enabled'] == '1')
            $options = ($options | _IS_ENABLED);
        unset($data['is_enabled']);
        unset($data['is_index']);

        $another = array(
            'add_time'   => time(),
            'edit_time'  => time(),
            'options'    => $options
        );

        $data = array_merge($data, $another);

        $this->db->insert($this->tables['event'], $data);
        $id = $this->db->insert_id();

        return $id;
    }

    /**
     * registers
     *
     * @return object Categories
     * @author appleboy
     **/
    public function registers()
    {
        // define in MY_Model
        $this->handle_process();

        // left join category
        $this->db->join($this->tables['order'], $this->tables['event_user'] . '.order_id = ' . $this->tables['order'] . '.id', 'left');
        $this->db->join($this->tables['user'], $this->tables['order'] . '.user_id = ' . $this->tables['user'] . '.id', 'left');
        $this->response = $this->db->get($this->tables['event_user']);

        return $this;
    }

    /**
     * Add event register
     *
     * @return bool
     * @author appleboy
     **/
    public function add_event_user($data = array())
    {
        if (empty($data)) {
            return false;
        }

        $another = array(
            'add_time'   => time(),
            'edit_time'  => time()
        );

        $data = array_merge($data, $another);

        $this->db->insert($this->tables['event_user'], $data);
        $id = $this->db->insert_id();

        return $id;
    }

    /**
     * update event
     *
     * @return bool
     * @author appleboy
     **/
    public function update_event($id, $data = array(), $escape = true)
    {
        if(empty($id) or empty($data))

            return false;

        if (isset($data['is_index']) && isset($data['is_enabled'])) {
            $options = 0x00;
            if ($data['is_index'] == '1')
                $options = ($options | _IS_INDEX);
            if ($data['is_enabled'] == '1')
                $options = ($options | _IS_ENABLED);
            $data['options'] = $options;
        } else {
            if (isset($data['is_index'])) {
                if ($data['is_index'] == '1')
                    $this->db->set('options', '(options | ' . _IS_INDEX . ')', false);
                else
                    $this->db->set('options', '(options & ' . ~_IS_INDEX . ')', false);
            }
            if (isset($data['is_enabled'])) {
                if ($data['is_enabled'] == '1')
                    $this->db->set('options', '(options | ' . _IS_ENABLED . ')', false);
                else
                    $this->db->set('options', '(options & ' . ~_IS_ENABLED . ')', false);
            }
        }
        unset($data['is_enabled']);
        unset($data['is_index']);

        // escape == false
        if ($escape === false) {
            foreach ($data as $key => $value) {
                $this->db->set($key, $value, false);
            }
            $data = array();
        }

        if(is_array($id))
            $this->db->where_in('id', $id);
        else
            $this->db->where('id', $id);

        unset($data['id']);
        $data = array_merge($data, array('edit_time' => time()));
        $this->db->update($this->tables['event'], $data);

        return true;
    }

    /**
    * delete event
    *
    * @return bool
    * @author appleboy
    **/
    public function delete_event($id)
    {
        $this->db->trans_begin();

        if(is_array($id))
           $this->db->where_in('id', $id);
        else
           $this->db->where('id', $id);

        $this->db->delete($this->tables['event']);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            return FALSE;
        }

        $this->db->trans_commit();

        return TRUE;
    }
}
