<?php
    /*
     * Coupon for buy order.
     * 
     * @author howtomakeaturn
     */
class Coupon_model extends JR_Model {
    
    /*
     * @params mixed (amount, code, times_available, expired_at, creator_id)
     * @author howtomakeaturn
     */
    function create($data){
        
        if ( empty($data['amount']) || empty($data['code']) || 
              empty($data['times_available']) )
        {
            throw new Exception('invalid parameters');
        }        
        
        if (!empty($data['dedicated_user_id'])){
            $query = $this->db->from('users')->where('id', $data['dedicated_user_id'])->get()->result();
            
            if (empty($query)){
                throw new Exception('dedicated user id not exist');          
            }
        }
                
        $insert_data = $data;
        $insert_data['times_used'] = 0;
        $insert_data['created_at'] = date("Y-m-d H:i:s");
        $id = $this->insert($insert_data);
        return $id;
    }
    
    /*
     * Validate the coupon is valid or not.
     */
    function validate_coupon_code($code){
        $coupon = $this->get_by('code', $code);
        if (empty($coupon)){
            throw new Exception('invalid coupon code', 1);
        }
        if ( date("Y-m-d H:i:s") > $coupon->expired_at ){
            throw new Exception('this coupon is expired', 2);
        }
        if ( $coupon->times_available <= $coupon->times_used ){
            throw new Exception('this coupon has run out', 3);
        }        
        return TRUE;
    }    
    
    /*
     * user consume one coupon, means the times_uses should plus 1
     * 
     * @author howtomakeaturn
     */
    function consume($code){
        $coupon = $this->get_by('code', $code);
        if (empty($coupon)){
            throw new Exception('invalid coupon code');
        }
        if ( date("Y-m-d H:i:s") > $coupon->expired_at ){
            throw new Exception('this coupon is expired');
        }
        if ( $coupon->times_available == $coupon->times_used ){
            throw new Exception('this coupon has run out');
        }
        
        $data = array('times_used' => $coupon->times_used + 1);
        $this->update($coupon->id, $data);
        return TRUE;
    }
    
    function remove($id){
        $this->delete($id);      
        return TRUE;
    }
    
    /*
     * Parse data from the uploaded file.
     * Admin users can upload coupons via CSV file.
     * 
     * @params String (usually the temporary file path in the computer)
     * @return Array
     */
    function parse_from_file_to_array($file_path){
        if ( !file_exists($file_path) ){
            throw new Exception('file not found');
        }
        
        $csv_data = array();
        
        $file = fopen($file_path, 'r');

        while(! feof($file)){
            $data = fgetcsv($file);
            if ( !empty($data) ){
                array_push($csv_data, $data);
            }
        }

        fclose($file);        
        return $csv_data;
    }
    
    function create_from_csv_array($csv_data, $creator_id){
        $ids = array();
        foreach($csv_data as $csv){
            $data = array(
                'name' => $csv[0],
                'code' => $csv[1],
                'amount' => $csv[2],
                'times_available' => $csv[3],
                'minimum_charge' => $csv[4],
                'expired_at' => $csv[5],
                'dedicated_user_id' => $csv[6],        
                'creator_id' => $creator_id
            );
            
            array_push($ids, $this->create($data));
        }
      
        return $ids;
    }
    
    // send emails
    function notify_coupon_owner($coupon_id, $mail_content){
        $coupon = $this->get($coupon_id);
        
        if ( empty($coupon->dedicated_user_id) ){
            return false;
        }
        
        $user_id = $coupon->dedicated_user_id;
        
        if (empty($this->template)){
            $this->load->spark('codeigniter-template/1.0.0');
            $this->template->set_layout('template/email');          
        }
        $html = $this->template->render(
            'email/notify_coupon_owner', ['coupon' => $coupon, 'mail_content' => $mail_content], true);
        
        // $email = $this->db->get_where('users', array('id' => $user_id))->result()[0]->email;
        $this->load->model('User_model');
        $email = $this->User_model->get_effective_email($user_id);
                
        $this->load->library('notification');
        $this->notification->send('您有Getmore禮券！', $html, $email);        

        return true;
    }
        
}
