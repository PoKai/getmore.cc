<?php
    /*
     * How much money do users have in getmore.
     * 
     * @author howtomakeaturn
     */
class Account_model extends JR_Model {
    public $primary_key = 'user_id';
    //public $has_many = array( 'transactions');
    public $has_many = array( 'transactions' => array( 'primary_key' => 'user_id', 'model' => 'Transaction_model' ) );

    function __construct()
    {
        // 呼叫模型(Model)的建構函數
        parent::__construct();
        $this->load->database();        
    }
    
    /*
     * I know these functions are weird.
     * It's for compatibility. Forgive me :D
     * 
     * @author howtomakeaturn
     */
    function get_by_user_id($user_id){
        return $this->get_by_user_id_with_clothing_id($user_id);
    }
    
    function get_by_user_id_with_clothing_id($user_id){
        return $this->get_by_user_id_internal($user_id);      
    }
    function get_by_user_id_without_clothing_id($user_id){
        return $this->get_by_user_id_internal($user_id, FALSE);
    }
    private function get_by_user_id_internal($user_id, $with_clothing_id=TRUE){
        // lot redundant code here, Im in busy.
        // fix it if you have time :D
        $result = $this->db->select('*')
											 ->from('users ')
											 ->where('id', $user_id)
											 ->get()->result();
                       
        $no_user_in_user_table = false;
        if ( count($result) == 0 ){
            $no_user_in_user_table = true;
            throw new Exception("no this user in database: $user_id");
        }
        $result = $this->db->select('*')
											 ->from('accounts a ')
											 ->where('a.user_id', $user_id)
											 ->get()->result();
        
        if ( !$no_user_in_user_table && (count($result) == 0) ){
            $this->create_if_not_exist($user_id);
        }
        
        $result = $this->db->select('*')
											 ->from('accounts a ')
											 ->where('a.user_id', $user_id)
											 ->get()->result();
        $account = $result[0];
        /*
        $account->transactions = $this->db->select('tr.*, pc.id as clothing_id')
                           ->from('transactions tr')
                           ->join('product_clothing pc', 'tr.related_number = pc.product_item_number', 'left')
                           ->where('user_id', $user_id)
                           ->order_by('created_at', 'desc')
                           ->get()->result();
        */
       
        $account->transactions = $this->db->select('*')
                           ->from('transactions')
                           ->where('user_id', $user_id)
                           ->order_by('created_at', 'desc')
                           ->get()->result();
        if ($with_clothing_id){
            $this->load->model('Product_clothing_model', 'clothing');
            foreach($account->transactions as &$trans){         
              $clothing = $this->db->select('id')
                                                     ->from('product_clothing')
                                                     ->where('product_item_number', $trans->related_number)
                                                     ->get()->result()[0];
              $trans->clothing_id = $clothing->id;
            }
        }
        return $account;          
    }

    function create_if_not_exist($user_id){
        $result = $this->db->select('*')
											 ->from('accounts a')
											 ->where('a.user_id', $user_id)
											 ->get()->result();
        
        if ( count($result) == 0 ){
            $this->insert(
                array(
                    'user_id'=>$user_id,
                    'balance'=> '0',
                    'updated_at'=>date("Y-m-d H:i:s")
                )
            );
            return true;
        }
        return false;
    }
        
}
