<?php
    /*
     * 'Like' feature for closet show room.
     * 
     * @author howtomakeaturn
     */
class Demo_like_model extends JR_Model {
    function Demo_like()
    {
        // Call the Model constructor
        parent::Model();
        /* load 資料庫 */
        $this->load->database();
    }
    
    function create($demo_id, $user_id){
        
        if ( empty($demo_id) || empty($user_id)){
            throw new Exception('some variables are empty');
        }
        
        $this->db->from('demo_likes');
        $this->db->where('user_id', $user_id);
        $this->db->where('demo_id', $demo_id);

        $query = $this->db->get();
        $rows = $query->result_array();        
        
        // if user like it before, deny he/she from like it again
        if (count($rows) != 0){
            return false;
        }
        
        // create the like tuple
        $data = array(
            'demo_id'      => $demo_id,
            'user_id'    => $user_id
        );
        $this->db->insert('demo_likes', $data);        
        
        $this->db->from('closet_demos')->where('id', $demo_id);
        $query = $this->db->get();
        $row = $query->result_array();
        $demo = $row[0];
        $num_of_likes = $demo['num_of_likes'];
        $num_of_likes += 1;
        
        $data = array('num_of_likes' => $num_of_likes);
        
        $this->db->where('id', $demo_id);
        $this->db->update('closet_demos', $data);        
        
    }

}
?>
