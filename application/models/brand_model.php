<?php
    /*
     * Brands for autocomplete in sell page and clothing searching page.
     * 
     * @author howtomakeaturn
     */
class Brand_model extends JR_Model {

    protected $return_type = 'array';

    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    /*
     * Insert one row if needed.
     * 
     * @params mixed: 
     *     name: brand name
     * @return Boolean: False if already exist.
     */
    function insertIfNotExist($data){
        $query = $this->get_many_by('name', $data['name']);
        if ($query){
            return false;
        }
        $this->Brand_model->insert(
            array(
                'name' => $data['name'],
                'approved' => false,
                'created_at' => date('c')
            )
        );      
        return true;
    }
    
    public function get_level_by_brand_name($brand_name){
        $brand_info = $this->get_by('name', $brand_name);
        if(!$brand_info) return '';
        return $brand_info['category_id'];
    }

    public function get_categories(){
        return $this->db->from('brand_categories')->order_by('id asc, created_at asc')->get()->result_array();
    }

    public function new_category($name){
        $this->db->insert('brand_categories', array(
            'name' => $name
        )); 
        return $this->db->insert_id();
    }
}
