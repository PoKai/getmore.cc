<?php 
class Bank_model extends CI_Model {

    public function __construct(){
        parent::__construct();
        $this->load->spark('curl');
    }
    
    public function bank_activate($data){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, base_url("/restful/bank"));
        curl_setopt($ch, CURLOPT_COOKIE, http_build_query($_COOKIE,'',';'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        return curl_exec($ch);
    }

    public function get_bank_info(){
        return json_decode($this->curl->simple_get('/restful/bank', array(), array(
            CURLOPT_COOKIE => http_build_query($_COOKIE,'',';')
        )));
    }
}
?>