<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * Name: Url Model
 * @author: appleboy
 */

class Url_model extends MY_Model
{
    /**
     * __construct
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tables['master'] = URL_TABLE;
    }
}
