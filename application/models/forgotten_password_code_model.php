<?php
    /*
     * @author howtomakeaturn
     */
class Forgotten_password_code_model extends JR_Model {

    function __construct()
    {
        // 呼叫模型(Model)的建構函數
        parent::__construct();
        $this->load->database();        
    }
    
    function check_code($code){
        if (empty($code)){
            throw new Exception('請輸入重置碼');
        }
        $tuple = $this->get_by('code', $code);
        if (empty($tuple)){
            throw new Exception('此重置碼不存在');
        }
        if ($tuple->done == true){
            throw new Exception('此重置碼已無效');            
        }
        
        $now_time = strtotime(date("Y-m-d H:i:s"));
        $created_time = strtotime($tuple->created_at);
        $delta = $now_time - $created_time;

        // it's 12 hours
        if ($delta > 12 * 60 * 60){
            throw new Exception('此重置碼已過期');                      
        }
 
        return true;
    }
    
    function handle($code, $new_pw, $confirm_pw){
        
        // actually, just excute check_code is fine, since all exceptions
        // should be threw there.
        // I just like two check and throw again ;)
        if ( !$this->check_code($code) ){
            throw new Exception('error happened');
        }        
        if ($new_pw != $confirm_pw){
            throw new Exception('請輸入相同新密碼兩次'); 
        }
        $this->load->library('form_validation');
        if (!$this->form_validation->min_length($new_pw, 8)){
            throw new Exception('密碼長度至少要 8 個字');          
        }
        if ( preg_match('/\s/',$new_pw) ){
            throw new Exception('密碼請勿包含空白');
        }
        
        // start transaction mode
        $this->db->trans_start();
                
        // expire the tuple
        $this->update_by(array('code'=>$code), array('done'=>true));

        // this is the same as procedure in ion_auth library
        // I keep them the same because maybe you can sync them in the future
        // have a look if you feel confused
        // @authro howtomakeaturn
        $forgotten_password_code = $this->get_by('code', $code);        
        $query = $this->db->select('id, password, salt')
                          ->where('id', $forgotten_password_code->user_id)
                          ->limit(1)
                          ->get('users');

        $result = $query->row();

        $this->load->library('ion_auth');
        $new = $this->ion_auth->hash_password($new_pw, $result->salt);

        //store the new password and reset the remember code so all remembered instances have to re-login
        $data = array(
            'password' => $new,
            'remember_code' => NULL,
         );
        $this->db->update('users', $data, array('id' => $forgotten_password_code->user_id));

        // finish transaction mode
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            throw new Exception('errors happened in transaction.');
        }                
        return true;
    }
    
    function create_for_user_id($user_id){
        $query = $this->db->from('users')
                                            ->where('id', $user_id)
                                            ->get();
        if ($query->num_rows() == 0){
            throw new Exception("cannot find user with id: $user_id");
        }
        
        $user = $query->result()[0];
        
        if (empty($user->password)){
            throw new Exception("您是以facebook註冊，請直接登入，不需設定密碼。");          
        }
        
        // start transaction mode
        $this->db->trans_start();
        
        // only allow the newest code to be alive
        // kill all other codes here
        $this->update_by(array('user_id'=>$user_id), array('done'=>true));
        
        // this is the same as procedure in ion_auth library
        // I keep them the same because maybe you can sync them in the future
        // have a look if you feel confused
        // @authro howtomakeaturn
        $this->load->library('ion_auth');
        $code = $this->ion_auth->hash_password(microtime().$user->email);
        
        $data = array(
            'user_id' => $user_id,
            'code' => $code,
            'created_at' => date("Y-m-d H:i:s"),
            'done' => false
        );
        $this->insert($data);
        
        // finish transaction mode
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            throw new Exception('errors happened in transaction.');
        }                
        return $code;
    }
   
}
