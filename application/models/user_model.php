<?php
/*
 * Member system mechanism.
 * 
 * @author howtomakeaturn
 */

class User_model extends JR_Model {
    
    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->spark('curl');
    }
    
    public function get_user(){
        return json_decode($this->curl->simple_get('/restful/user/me', array(), array(
            CURLOPT_COOKIE => http_build_query($_COOKIE,'',';')
        )));
    }

    public function increase_sell_count($user_ids){
        $this->db->where_in('id', $user_ids)->set('sell_count', '`sell_count`+1', FALSE)->update('users');

        return true;
    }

    public function increase_buy_count($user_id){
        $this->db->where('id', $user_id)->set('buy_count', '`buy_count`+1', FALSE)->update('users');

        return true;
    }

    public function get_users_by_emails($emails){
        return $this->db->where_in('email', $emails)->from('users')->get()->result_array();
    }

    /*
     * Return the active_email if existed.
     * Otherwise, return the registration email.
     * 
     * @author howtomakeaturn
     */
    
    function get_effective_email($user_id){
        $user = $this->as_array()->get($user_id);
        $email = $user['email'];
        if ($user['active_email']){
            $email = $user['active_email'];
        }
        return $email;
    }

    /*
     * Check if the user is verified with phone number.
     */
    function is_verified($user_id){
        $user = $this->as_array()->get($user_id);
        if (empty($user['verify'])){
            return false;
        }
        else{
            return true;
        }
    }
    
    /*
     * Update data into 'users' table.
     * $user_data fields are the same as the table fields.
     * 
     * @author howtomakeaturn
     */
    function save_personal_information($user_id, $user_data){
        
        $protected_fields = array(
            'user_name', 'mobile_phone', 'birthday', 
            'telephone', 'identification_card_number'
        );
        
        foreach($protected_fields as $field){
            if (empty($user_data[$field])){
                throw new Exception('請填入完整資訊');
            }
        }

        if (empty(json_decode($user_data['p_address'], true)['address'])){
            throw new Exception('請填入地址。');                
        }
        if (empty(json_decode($user_data['s_address'], true)['address'])){
            throw new Exception('請填入地址。');                
        }

        if (!$this->validate_mobile_phone_number($user_data['mobile_phone'])){
            throw new Exception('手機號碼格式錯誤。');
        }
        
        if (!$this->validate_id_number($user_data['identification_card_number'])){
            throw new Exception('身份證驗證失敗。');
        }
        
        $this->db->where('id', $user_id)->update('users', $user_data);
            
        return true;
    }
    
    // Did the user fill in all the necessary information?
    function is_filled_information($user_id){
        $user = $this->as_array()->get($user_id);
        
        $check_fields = array(
            'user_name', 'mobile_phone', 'birthday', 
            'telephone', 'identification_card_number'
        );
        
        foreach($check_fields as $field){
            if (!($user[$field])){
                return false;
            }
        }
        
        return true;
    }
    
    // Is the sms verification code the same as the one in the session?
    function check_sms_code($sms_code){
        if ($sms_code == $this->session->userdata('sms_code')){
            $this->update($this->session->userdata('user_id'), array('verify'=>1));
            return true;
        }
        else{
            return false;
        }
    }

    // Check if it's valid phone number.
    public function validate_mobile_phone_number($number){
        if (strlen($number) !== 10){
            return false;          
        }
        if (substr($number, 0, 1)!=='0')
        {
            return false;
        }
        if (substr($number, 1, 1)!=='9')
        {
            return false;
        }


        return true;
    }

    //**************
    // 身份證檢查
    // http://doublekai.org/blog/?p=25
    //**************
    public function validate_id_number($id){
        $id = ucfirst($id);
        //建立字母分數陣列
        $head = array('A'=>1,'I'=>39,'O'=>48,'B'=>10,'C'=>19,'D'=>28,
                      'E'=>37,'F'=>46,'G'=>55,'H'=>64,'J'=>73,'K'=>82,
                      'L'=>2,'M'=>11,'N'=>20,'P'=>29,'Q'=>38,'R'=>47,
                      'S'=>56,'T'=>65,'U'=>74,'V'=>83,'W'=>21,'X'=>3,
                      'Y'=>12,'Z'=>30);
        //建立加權基數陣列
        $multiply = array(8,7,6,5,4,3,2,1);
        //檢查身份字格式是否正確
        if (ereg("^[a-zA-Z][1-2][0-9]+$",$id) && strlen($id) == 10){
            //切開字串
            $len = strlen($id);
            for($i=0; $i<$len; $i++){
                $stringArray[$i] = substr($id,$i,1);
            }
            //取得字母分數
            $total = $head[array_shift($stringArray)];
            //取得比對碼
            $point = array_pop($stringArray);
            //取得數字分數
            $len = count($stringArray);
            for($j=0; $j<$len; $j++){
                $total += $stringArray[$j]*$multiply[$j];
            }
            //檢查比對碼
            if (($total%10 == 0 )?0:10-$total%10 != $point) {
                return false;
            } else {
                return true;
            }
        }  else {
           return false;
        }
    }

    // the sms verification code
    public function generate_sms_code(){
        $chars = "0123456789"; 
        srand((double)microtime()*1000000); 
        $i = 0; 
        $pass = '' ; 

        while ($i < 7) { 
            $num = rand() % 10; 
            $tmp = substr($chars, $num, 1); 
            $pass = $pass . $tmp; 
            $i++; 
        } 

        return $pass; 
    }
    
    public function user_update($data){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, base_url("/restful/user"));
        curl_setopt($ch, CURLOPT_COOKIE, http_build_query($_COOKIE,'',';'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        return curl_exec($ch);
    }

    public function update($data){
        return $this->curl->simple_put('/restful/user', $data, array(
            CURLOPT_COOKIE => http_build_query($_COOKIE,'',';')
        ));
    }

}
