<?php
/**
 * Model for buy orders.
 *
 * @author grassshrimp
 **/
class Sub_order_model extends JR_Model {
	public $_table = 'sub_order';

	function Sub_order() {
		// Call the Model constructor
		parent::Model();
		/* load 資料庫 */
		$this->load->database();

		$this->load->spark('curl');
	}

	/**
	 * @author popchieh, shipped/edited by howtomakeaturn
	 */
	public function generate_sub_order_number($buy_order_number) {

    $count = $this->db->like('sub_order_number', $buy_order_number)->from($this->_table)->count_all_results();

		$sub_order_number = $buy_order_number . str_pad($count+1, 3, '0', STR_PAD_LEFT);

		return $sub_order_number;
	}

}
?>
