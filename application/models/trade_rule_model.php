<?php 
class Trade_rule_model extends CI_Model {

    function __construct(){
        parent::__construct();
        $this->load->spark('curl');
    }
    
    function create_default($data){
        $this->curl->simple_post('/restful/trade', $data, array(
            CURLOPT_COOKIE => http_build_query($_COOKIE,'',';')
        ));
    }
}
?>