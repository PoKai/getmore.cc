<?php
class Facebook_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->config->load('facebook');
        $this->load->library('session');

        $config = array(
            'appId'  => $this->config->item("facebook_app_id"),
            'secret' => $this->config->item("facebook_api_secret")
        );
        $this->load->library('facebook', $config);
        $user = $this->facebook->getUser();

        // We may or may not have this data based on whether the user is logged in.
        //
        // If we have a $user id here, it means we know the user is logged into
        // Facebook, but we don't know if the access token is valid. An access
        // token is invalid if the user logged out of Facebook.
        $profile = NULL;
        if ($user) {
            try {
                // Proceed knowing you have a logged in user who's authenticated.
                //$profile = $this->facebook->api('/me');
                $profile = $this->facebook->api('/me');
            } catch (FacebookApiException $e) {
                error_log($e);
                $user = NULL;
            }
        }

        $fb_data = array(
            'me' => $profile,
            'uid' => $user,
            'loginUrl' => $this->facebook->getLoginUrl(array('scope' => $this->config->item("facebook_default_scope"))),
            'logoutUrl' => $this->facebook->getLogoutUrl(),
        );
        $this->session->set_userdata('fb_data', $fb_data);
    }
}

/* End of file facebook_model.php */
/* Location: ./application/models/facebook_model.php */
