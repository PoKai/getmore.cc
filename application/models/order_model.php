<?php

/*
 * Actions related to 'sellorders' table
 * 
 * @author howtomakeaturn
 */

class Order_model extends JR_Model {
    
    public $_table = 'sellorders';
    protected $return_type = 'array';
    
    // When inserting data, there are necessary.
    // Let validation library check them for you.
    public $validate = array(
        array( 'field' => 'user_id',
               'rules' => 'required' ),
        array( 'field' => 'user_name',
               'rules' => 'required' ),
        array( 'field' => 'user_mobile_phone',
               'rules' => 'required' ),
        array( 'field' => 'user_address',
               'rules' => 'required' ),
        array( 'field' => 'uniform_invoice_type',
               'rules' => 'required' ),
        array( 'field' => 'deduction_amount',
               'rules' => 'less_than_equal_to[0]' ),
        array( 'field' => 'order_number',
               'rules' => 'required' ),
        array( 'field' => 'status_id',
               'rules' => 'required' ),
        array( 'field' => 'payment_method',
               'rules' => 'required' )
    );
    
    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->model('Product_model');
        $this->load->model('Sellcoupon_model');
    }
    
    function create($order_data, $items_data, $user_id){
        $this->db->trans_start();
                              
        $id = $this->add_empty_order($order_data);
        foreach($items_data as $item_data){
            $this->Product_model->add_to_order($id, $item_data);
        }
        $this->fill_in_total_price($id);
        $this->fill_in_transportation_charge($id);
        $this->activate_coupon($id, $user_id);
        $this->handle_deduction($id);
        $this->fill_in_final_price($id);
        $this->protect_payment_method($id);
        
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            throw new Exception('transaction failed. (Order_model: create)');
        }
                        
        return $id;
    }

    function activate_coupon($id, $user_id){
        $order = $this->get($id);
        if (!$order['coupon_code']){
            // user didn't provide coupon code, we are done.
            return $id;
        }
        
        $this->Sellcoupon_model->validate_coupon_code($order['coupon_code']);
        $this->Sellcoupon_model->validate_coupon_owner($order['coupon_code'], $user_id);
        
        $coupon = $this->Sellcoupon_model->as_array()->get_by('code', $order['coupon_code']);
        if ( (int)$order['total_price'] < (int)$coupon['minimum_charge']){
            throw new Exception('not satisfy the minimum charge.');
        }
        
        // fill in the coupon amount field
        $this->update($id, array('coupon_amount' => -1 * (int) $coupon['amount']));
        
        $this->Sellcoupon_model->consume($order['coupon_code']);
        return $id;        
    }
    
    // don't over-design. don't be too defensive.
    function add_empty_order($data){
        $insert_data = $data;
        $insert_data['add_time'] = time();
        $id = $this->insert($insert_data);
        if (!$id){
            throw new Exception('fail to pass the model validation');
        }
        return $id;
    }

    // don't over-design. don't be too defensive.
    function fill_in_total_price($id){
        $items = $this->Product_model->as_array()->
            not_rejected()->get_many_by('order_id', $id);

        $washing_fee = 0;
        foreach($items as $item){
            $washing_fee += $item['shipping_price'];
        }
        
        $this->update($id, array('total_price' => $washing_fee), TRUE);
        return  $id;
    }
    
    function fill_in_transportation_charge($id){
        $order = $this->as_array()->get($id);
        $transportation_charge = $this->get_price_transportation_charge($order['total_price']);
        
        $this->update($id, array('transportation_charge' => $transportation_charge), TRUE);
        return  $id;
    }
    
    function handle_deduction($id){
        $order = $this->as_array()->get($id);
        if ($order['deduction_amount'] != 0){
            $deduction_amount = (int) $order['deduction_amount'];
            
            $this->load->model('Account_model');
            $account = $this->Account_model->as_array()->get($order['user_id']);
            
            if ($account['balance'] + $deduction_amount < 0){
                throw new Exception('not enough money in account.');
            }
            
            $this->load->model('Transaction_model', 'transaction');
            $this->transaction->insert_to_user($order['user_id'], 
                "洗衣費抵扣", $order['order_number'], $deduction_amount, '');
        }
        return $id;
    }

    function fill_in_final_price($id){
        $order = $this->as_array()->get($id);
        $final_price = $order['total_price'] + 
            $order['transportation_charge'] + 
            $order['deduction_amount'] +
            $order['coupon_amount'];
            
        if ( $order['deduction_amount'] + $order['total_price'] + 
            $order['transportation_charge'] < 0 )
        {
            throw new Exception('deduction amount is too big');
        }

        if ( $final_price < 0 ){
            throw new Exception('coupon amount is too big');
        }

        $this->update($id, array('final_price' => $final_price), TRUE);
        return  $id;
    }
    
    function protect_payment_method($id){
        // Because the front-end js code should provide the 
        // proper payment method for us, we don't really
        // update the order's payment method.
            
        $order = $this->as_array()->get($id);
        
        if ( ( $order['payment_method'] == 'deduction' ) && 
              ( $order['final_price'] != 0 ) )
        {
            throw new Exception('payment method is deduction. but final price is not zero.');
        }
        
        return  $id;      
    }

    function complete($id){
        $data = array( 'status_id' => ORDER_COMPLETE, 
            'date_finished' => date("Y-m-d H:i:s"));

        $this->update($id, $data, TRUE);
        return  $id;
    }

    function get_price_transportation_charge($number){
        return 120;
    }


    public function generate_order_number()
    {
        $this->load->helper('date');
        $this->load->library('lib_sellorder');

        $datestring = "%Y%m%d";
        $time = time();

        $order_num = "S";
        /*if($_SERVER['REMOTE_ADDR'] == '127.0.0.1' ){
            $order_num = "T";
        }
        if(site_url() == 'http://beta.getmore.cc/' ){
            $order_num = "T";
        }*/
        
        $time_str = mdate($datestring, $time);

        $result = $this->lib_sellorder->select('count(*), DATE_FORMAT(date_purchased,"%Y%m%d") as created_day')
             ->group_by('created_day')
             ->orders()
             ->result_array();

        foreach ($result as $row) {
            if($time_str == $row['created_day']){
                $count_today = $row['count(*)'];
                if(!$count_today || $count_today==0)
                    $count_today = 1;
                else	
                    $count_today++;
            }
            else{
                $count_today = 1;
            }
        }

        $sn_today = $count_today;
        $sn_today = str_pad($sn_today,4,'0',STR_PAD_LEFT);

        $time_str = substr($time_str, 2);
        $order_num .= $time_str.$sn_today;

        return $order_num;
    }

    public function notify_the_atm_order_created($order_id){
        $order = $this->get($order_id);

        $this->load->spark('codeigniter-template/1.0.0');
        $this->template->set_layout('template/email');
        $html = $this->template->render('email/notify_the_atm_order_created', 
            ['order' => $order], true );
        
        $this->load->model('User_model');
        $email = $this->User_model->get_effective_email($order['user_id']);
        
        $this->load->library('notification');
        $this->notification->send('GetMore二次時尚貨單成立'  . 
            '（貨單編號： ' . $order['order_number'] . '）', $html, $email);
    }
      
    /*
     * Used when admin rejected items and need to recalculate.
     */
    function recalculate($id){
        $this->db->trans_start();
        
        $order = $this->get($id);

        $msg = "[ 重新計算洗衣費 : 原洗衣費記錄 " . $order['total_price'] . " (洗衣費) + " .
            $order['transportation_charge'] . " (運費) + " . 
            $order['deduction_amount'] . " (抵扣額) + " . 
            $order['coupon_amount'] . " (" . $order['coupon_code'] . ") = " .
            $order['final_price'] . " ]";
                              
        $this->fill_in_total_price($id);
        $this->fill_in_transportation_charge($id);
        $this->fill_in_final_price($id);
        
        $this->write_log($id, $msg);
        
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            throw new Exception('transaction failed. (Order_model: recalculate)');
        }
                        
        return $id;
    }


    
    function write_log($id, $message){
        $order = $this->get($id);      
        $log = $order['logs'] . $message;        
        $this->update($id, ['logs' => $log], TRUE);
        return TRUE;
    }
    

}
