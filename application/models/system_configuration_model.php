<?php
/*
 * Stuff related to system configuration.
 * 
 * @author howtomakeaturn
 */
class System_configuration_model extends CI_Model {
    /**
     * Retrieves current free_shipping value
     *
     * @return	Boolean
     */
    function get_free_shipping(){
        $row = $this->db->select('free_shipping')
                                     ->get('system_configuration')->row();
        
        if (empty($row)){
            show_error('system configuration setting error.');
        }
        else{
            return $row->free_shipping;
        }
    }
    
    /*
     * Setting up the status for whether charging for 
     * shipping fee or not.
     * 
     * @param Boolean $free_shipping
     * @author howtomakeaturn
     * */
    function set_free_shipping($free_shipping){
        return $this->db->update('system_configuration', 
            array( 'free_shipping' => $free_shipping));
    }
    
    /*
     * Banners-related functions. (Photos in the index.)
     */    
    function add_banner($file_id, $href, $open_in_new_tab){
        $data = array(
            'file_id' => $file_id,
            'href'           => $href,
            'order'         => 0,
            'open_in_new_tab' => $open_in_new_tab
        );
        $this->db->insert('banners', $data);
        return $this->db->insert_id();
    }
    
    function set_banner_order($ordered_banner_ids){
        foreach($ordered_banner_ids as $index => $value){
            $this->db->where('id', $value)->update('banners', array('order'=>$index));
        }
        return TRUE;      
    }
    
    function delete_banner($id){
        $this->db->where('id', $id)->delete('banners');
        return TRUE;      
    }
    
    function get_banners_with_image_url_and_href(){
        $banners = $this->db->order_by('order')->get('banners')->result();
        
        foreach($banners as $banner){
            $banner->image_url = image_url($banner->file_id, null, 'banner');
        }
        
        return $banners;      
    }

}
