<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * a service-level model
 * 
 * @author howtomakeaturn
 */

class Manage_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function order_detail($order_number){
        $this->load->model('Buy_order_model');
        $order = $this->Buy_order_model->get_complete_order_by_order_number($order_number);
        return $order;
    }

}
