<?php
class Refashion_model extends JR_Model {

    protected $return_type = 'array';
    public $protected_attributes = array( 'id');
    
    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->model('Files_model');
    }
    
    function insert($data){
        $data['created_at'] = date('c');
        return parent::insert($data);        
    }
    
    function get_all(){
        $posts = parent::get_all();
        foreach($posts as &$post){
            $file = $this->db->from('files')->where('file_id', $post['file_id'])
                                         ->get()->row_array();
            $post['img_url'] = image_url($file['file_name']);
        }
        return $posts;
    }

}
