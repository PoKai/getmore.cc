<?php
    /*
     * @author howtomakeaturn
     */
class Sellcoupon_model extends JR_Model {
    
    /*
     * @params mixed (amount, code, times_available, expired_at, creator_id)
     * @author howtomakeaturn
     */
    function create($data){
        
        if ( empty($data['amount']) || empty($data['code']) || 
              empty($data['times_available']) )
        {
            throw new Exception('invalid parameters');
        }        
                
        $insert_data = $data;
        $insert_data['times_used'] = 0;
        $insert_data['created_at'] = date("Y-m-d H:i:s");
        $id = $this->insert($insert_data);
        return $id;
    }
    
    function validate_coupon_code($code){
        $coupon = $this->get_by('code', $code);
        if (empty($coupon)){
            throw new Exception('invalid coupon code', 1);
        }
        if ( date("Y-m-d H:i:s") > $coupon->expired_at ){
            throw new Exception('this coupon is expired', 2);
        }
        if ( $coupon->times_available <= $coupon->times_used ){
            throw new Exception('this coupon has run out', 3);
        }        
        return TRUE;
    }    

    function validate_coupon_owner($code, $user_id){
        $coupon = $this->get_by('code', $code);

        if (!$coupon->dedicated_user_id){
            return TRUE;
        }

        if ( $coupon->dedicated_user_id !== $user_id ){
            throw new Exception('not the dedicated user', 5);
        }
        return TRUE;
    }    
    /*
     * user consume one coupon, means the times_uses plus 1
     * 
     * @author howtomakeaturn
     */
    function consume($code){
        $coupon = $this->get_by('code', $code);
        if (empty($coupon)){
            throw new Exception('invalid coupon code');
        }
        if ( date("Y-m-d H:i:s") > $coupon->expired_at ){
            throw new Exception('this coupon is expired');
        }
        if ( $coupon->times_available == $coupon->times_used ){
            throw new Exception('this coupon has run out');
        }
        
        $data = array('times_used' => $coupon->times_used + 1);
        $this->update($coupon->id, $data);
        return TRUE;
    }
    
    function remove($id){
        $this->delete($id);      
        return TRUE;
    }
        
}
