<?php
    /*
     * Transactions.
     * 
     * @author howtomakeaturn
     */
class Transaction_model extends JR_Model {
    // public $belongs_to = array( 'account' );
    public $belongs_to = array( 'Account' => array( 'primary_key' => 'user_id', 'model' => 'Account_model' ) );
    
    function __construct()
    {
        // 呼叫模型(Model)的建構函數
        parent::__construct();
        $this->load->database();
        // these two lines cause memory problem. WTF?
        $this->load->model('Account_model', 'account');
        // $this->load->model('Transaction_model', 'transaction');
    }
    
    /*
     * create one transaction for user and its account.
     * 
     * @author howtomakeaturn
     * @return Boolean
     */
    function insert_to_user( $user_id = '', $trans_name = '', $related_number='', $trans_amount= '', $note=''){
        if (count(func_get_args())!=5){
//        if ( empty($user_id) || empty($trans_name) || empty($trans_amount) ){
//            throw new Exception('one of the parameters in empty');
            throw new Exception('number of parameters is not correct');
        }
//        if ( empty($user_id) || empty($trans_name) || empty($trans_amount) ){
//            throw new Exception('one of the parameters in empty');
   //         throw new Exception('one of the parameters is not passed');
    //    }
        $account = $this->account->get($user_id);
        if (empty($account)){
            throw new Exception("invalid 'user_id': $user_id. can't find account data with it.");          
        }
        if ( !is_numeric($trans_amount) || !is_integer( $trans_amount) ){
            throw new Exception("invalid 'trans_amount': $trans_amount. it's not integer.");                    
        }

        if ( $trans_amount == 0 ){
            throw new Exception("transaction amount is zero. no need to create tuple.");                    
        }
        
        // it MUST to be processed in transaction mode.
        $this->db->trans_start();            
        $new_account_balance = $account->balance + $trans_amount;

        /*
        if ( $new_account_balance < 0 ){
            throw new Exception("'new_account_balance' will be negative: $new_account_balance. this deduction is forbidden");                    
        }
        */
        
        $this->account->update($user_id,
            array( 'balance' => $new_account_balance, 'updated_at' => date("Y-m-d H:i:s" )));
        
        $this->transaction->insert(array(
            'user_id' => $user_id,
            'name' => $trans_name,
            'related_number' => $related_number,
            'amount' => $trans_amount,
            'note' => $note,
            'created_at' => date("Y-m-d H:i:s")
        ));
        
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            throw new Exception('errors happened in transaction.
                in insert_to_user');
        }
        return TRUE;
    }
    
    // Retrieve all transactions.
    public function latest_transactions(){
        $this->db->order_by('created_at', 'desc');
        return $this;
    }
    
}
