<?php

/*
 * Zend ACL Model
 * Author: Bo-Yi Wu <appleboy.tw@gmail.com>
 * Date: 2011-10-18
 */

class Acl_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_acl()
    {
        $this->db->select('acl_id, t1.resource_id as resource_id, t3.name as group_name, t2.resource_name as class_name, t1.resource_name as method_name, t1.resource_description as resource_description, action');
        $this->db->join("acl_resource t1", "t1.resource_id = t4.resource_id", "left");
        $this->db->join("acl_resource t2", "t2.resource_id = t1.parent_id", "left");
        $this->db->join("groups t3", "t4.group_id = t3.id", "left");
        $this->db->order_by("t4.resource_id, t3.group_order", "ASC");
        $query = $this->db->get('acl t4');

        if ($query->num_rows() > 0) {
           $row = $query->result_array();

           return $row;
        }

        return FALSE;
    }

    public function edit_acl($id, $action = 'allow')
    {
        $id = intval($id);

        $data = array(
           'action' => $action
        );

        $this->db->where('acl_id', $id);
        $this->db->update('acl', $data);

        return TRUE;
    }

    public function del_acl($id = array(), $field = "resource_id")
    {
        switch ($field) {
            case 'acl_id':
                if(is_array($id))
                    $this->db->where_in('acl_id', $id);
                else
                    $this->db->where('acl_id', $id);
                $this->db->delete('acl');
            break;
            case 'resource_id':
                $table = array("acl_resource", "acl");
                if(is_array($id))
                    $this->db->where_in('resource_id', $id);
                else
                    $this->db->where('resource_id', $id);
                $this->db->delete($table);
            break;
        }

        return TRUE;
    }

    public function get_top_resource()
    {
        $query = $this->db->select('resource_id, resource_name, resource_description')
                 ->where('parent_id', 0)
                 ->order_by('resource_order', 'ASC')
                 ->get('acl_resource');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return FALSE;
    }

    public function check_resource($resource = NULL, $permission = NULL)
    {
        if (!isset($resource) or empty($resource)) {
            return FALSE;
        }

        if (!isset($permission)) {
            $query = $this->db->select('resource_id')
                     ->where('resource_name', $resource)
                     ->get('acl_resource');
            if ($query->num_rows() > 0) {
                $id = $query->row()->resource_id;
            }
        } else {
            $query = $this->db->select('resource_id')
                     ->where('parent_id', $resource)
                     ->where('resource_name', $permission)
                     ->get('acl_resource');
            if ($query->num_rows() > 0) {
                $id = $query->row()->resource_id;
            }
        }

        return (isset($id)) ? $id : FALSE;
    }

    public function add_resource($data = array())
    {
        if (!isset($data) or empty($data)) {
            return FALSE;
        }

        if (isset($data['parent_id'])) {
            //check if permission exist();
            $id = $this->check_resource($data['parent_id'], $data['resource_name']);
            if($id) return $id;
        }

        if (isset($data['action'])) {
            $action = $data['action'];
            unset($data['action']);
        }
        if (isset($data['group_id'])) {
            $group = $data['group_id'];
            unset($data['group_id']);
        }

        $this->db->insert('acl_resource', $data);
        $id = $this->db->insert_id();
        $count = count($group);
        for ($i=0;$i<$count;$i++) {
            $this->zacl->add_acl($group[$i], $id, $action[$i]);
        }

        return (isset($id)) ? $id : FALSE;
    }

    public function add_acl($group_id = NULL, $resource_id = NULL, $action = NULL)
    {
        $data = array(
            "group_id" => $group_id,
            "resource_id" => $resource_id,
            "action" => $action
        );

        $this->db->insert('acl', $data);
        $id = $this->db->insert_id();

        return (isset($id)) ? $id : FALSE;
    }
}
/* End of file acl_model.php */
/* Location: ./application/models/acl_model.php */
