<?php
/**
 * not smart model which fetches data from database
 * it doesn't follow the skeleton made by appleboy
 * but it make the website work in short time!
 *
 * @author howtomakeaturn
 **/
class Product_clothing_model extends JR_Model {
	public $_table = 'product_clothing';

	private $s3_path = 'https://s3-ap-southeast-1.amazonaws.com/getmore/clothing/';

	function Product_clothing() {
		// Call the Model constructor
		parent::Model();
		/* load 資料庫 */
		$this->load->database();
		$this->load->helper('Product_clothing_helper');

	}

	/*
	 * if customer want to cancel their purchasing
	 * @return Boolean
	 */

	function return_item($item_number) {
		if (empty($item_number)) {
			throw new Exception('empty item_number');
		}
		$clothing = $this->get_by('product_item_number', $item_number);
		if (empty($clothing)) {
			throw new Exception('can not find clothing');
		}

		$this->load->model('Buy_order_model', 'buyorder');
		$order = $this->buyorder->get($clothing->buy_order_id);

		if (empty($order)) {
			throw new Exception('can not find order');
		}

		$old_log = $order->logs;
		if (empty($old_log)) {
			$log = [];
		} else {
			$log = json_decode($old_log);
		}
		$message = "退貨：" .
		"商品名稱（" . $clothing->product_title .
		"），商品編號（" . $clothing->product_item_number .
		"），商品金額（" . $clothing->product_sell_price .
		"）";
		array_push($log, $message);

		$this->db->trans_start();

		$this->buyorder->update($order->id, ['logs' => json_encode($log)]);

		$this->load->model('Return_request_model', 'rr');
		$this->rr->insert([
			'order_number' => $order->order_number,
			'item_number' => $clothing->product_item_number,
			'price' => $clothing->product_sell_price,
			'created_at' => date("Y-m-d H:i:s"),
		]);

		$this->update($clothing->id, [
			'buy_order_id' => '',
			'lock_status' => '',
			'last_locked_time' => '',
		]);

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			throw new Exception('errors happened');
		}

		return true;
	}

	/*
	 * update the price of clothing.
	 * this method check the ownership before excuting.
	 *
	 * @author chuan-hao, you
	 * @return Boolean
	 */
	function update_price($id, $new_price, $owner_id) {
		if (count(func_get_args()) != 3) {
			throw new Exception('number of parameters is not correct');
		}
		if (empty($id) || empty($new_price) || empty($owner_id)) {
			throw new Exception('one of the parameters is empty');
		}
		if ($new_price < 100) {
			throw new Exception("invalid new_price: $new_price");
		}
		$query = $this->db
			->select('so.user_id, pc.lock_status, pc.log, pc.product_sell_price, sp.product_sell_price as first_sell_price')
			->from('product_clothing pc')
			->join('sellorders_products sp', 'pc.product_id = sp.id')
			->join('sellorders so', 'sp.order_id = so.id')
			->join('buy_order bo', 'pc.buy_order_id = bo.id', 'left')
			->where('pc.id', $id)
			->get()->result();
		if (empty($query)) {
			throw new Exception('cannot find clothing by this id: ' . $id);
		}
		if (count($query) > 1) {
			throw new Exception('get more than one clothing by this id: ' . $id);
		}
		$clothing = $query[0];
		if ($clothing->user_id != $owner_id) {
			throw new Exception("the user_id $owner_id isn't the owner of clothing_id $id");
		}
		if ($clothing->lock_status != 0) {
			throw new Exception("the lock status is not 0");
		}

		$previous_log = $clothing->log;
		$previous_price = $clothing->product_sell_price;
		$date_now = date("Y-m-d H:i:s");

		$log = $previous_log . "$date_now: $previous_price=>$new_price ";

		$reduction_again = 0;
		if ($new_price < $clothing->first_sell_price) {
			$reduction_again = 1;
		}

		$this->db->where('id', $id)
			->update('product_clothing', array(
				'product_sell_price' => $new_price,
				'log' => $log,
				'reduction_again' => $reduction_again));

		if ($reduction_again) {
			@$this->notify_the_clothing_is_reduction_again($id);
		}

		return true;
	}

	function get_clothing_by_user_id($user_id) {
		$result = new StdClass();
		$result->receiving = [];
		$result->selling = [];
		$result->sent = [];
		$result->finished = [];

		$products = $this->db
			->select('sp.product_title, sp.product_item_number,
                         sp.product_original_price, sp.product_sell_price,
                         sp.shelf, sp.remove')
			->from('sellorders_products sp')
			->join('sellorders so', 'sp.order_id = so.id')
			->where('so.user_id', $user_id)
			->where('sp.shelf', '0')
		// this may need to change to 2,3,4,5 in the future
			->where_in('status_id', [1.5, 2, 3, 4, 5, 6])
			->get()->result();

		foreach ($products as $product) {
			// these are receiving
			if ($product->shelf == 0 && $product->remove == 0) {
				array_push($result->receiving, $product);
			}
		}

		$clothings = $this->db
			->select('pc.product_title, pc.add_time,
                          pc.product_original_price, pc.product_sell_price,
                          pc.product_item_number, pc.id, bo.status as buyorder_status,
                          pc.lock_status, pc.suggested_price')
			->from('product_clothing pc')
			->join('sellorders_products sp', 'pc.product_id = sp.id')
			->join('sellorders so', 'sp.order_id = so.id')
			->join('buy_order bo', 'pc.buy_order_id = bo.id', 'left')
			->where('so.user_id', $user_id)
			->get()->result();

		foreach ($clothings as $clothing) {
			// these are selling
			if (in_array($clothing->lock_status, [0, 1])) {
				array_push($result->selling, $clothing);
			}
			// these are basically soldout, but maybe customers
			// would like to return what they received. notice the different
			if (in_array($clothing->buyorder_status, [1.5, 2, 3, 4])) {
				array_push($result->sent, $clothing);
			}

			// these are finished items
			if (in_array($clothing->buyorder_status, [5])) {
				array_push($result->finished, $clothing);
			}

		}
		return $result;
	}

	/*
	 * system need to know who's the owner of clothing
	 * for adding amount to account
	 * @author howtomakeaturn
	 */
	function get_user_id_by_product_item_number($product_item_number) {
		if (empty($product_item_number)) {
			throw new Exception('product_item_number is empty');
		}
		$this->db->select('so.user_id');
		$this->db->from('product_clothing clothing');
		$this->db->join('sellorders_products item', 'clothing.product_id = item.id');
		$this->db->join('sellorders so', 'item.order_id = so.id');
		$this->db->where('clothing.product_item_number', $product_item_number);

		$query = $this->db->get();
		$row = $query->result();
		if (count($row) != 1) {
			throw new Exception('cannot get exactly one tuples with this product_item_number: ' . $product_item_number);
		}
		return $row[0]->user_id;
	}

	/*
	 *
	 * @params $preparation: whether to checkout the clothing
	 *    immediately or just add it to the order
	 */

	function put_one_into_physical_buyorder($product_item_number,
		$order_number, $preparation = false) {
		if (empty($product_item_number)) {
			throw new Exception("empty product_item_number");
		}

		$clothing = $this->get_by('product_item_number', $product_item_number);

		if (empty($clothing)) {
			throw new Exception("can't find clothing in the database");
		}

		if ($clothing->lock_status != '0') {
			throw new Exception("the clothing is not in lock_status 0, which means it's not available");
		}

		$this->load->model('Buy_order_model', 'buyorder');
		$this->load->model('Sub_order_model');
		// maybe the result is the same as get_by
		$order = $this->buyorder->get_by('order_number', $order_number);
		$sub_order = $this->Sub_order_model->get_by('buy_order_id', $order->id);
		if (count($order) != '1') {
			throw new Exception("the number of buyorder fetched by order number is not exactly one. it is " . count($order) . '. order number: ' . $order_number);
		}

		$data = array(
			'lock_status' => '2',
			'buy_order_id' => $order->id,
			'last_locked_time' => date("Y-m-d H:i:s"),
		);

		if ($preparation) {
			$data['lock_status'] = 1;
		}

		$this->db->trans_start();

		$this->db->where('product_item_number', $product_item_number);
		$this->db->update('product_clothing', $data);

		$items_count = $order->items_count + 1;
		$items_price = $order->items_price + $clothing->product_sell_price;
		$total_price = $order->total_price + $clothing->product_sell_price;

		$order_data = array(
			'items_count' => $items_count,
			'items_price' => $items_price,
			'total_price' => $total_price,
		);

		$sub_order_products = json_decode($sub_order->products, true);

		$sellorders_product = $this->db->where('id', $clothing->product_id)->from('sellorders_products')->get()->row();

		array_push($sub_order_products, [
			id => $clothing->id,
			product_item_number => $clothing->product_item_number,
			product_sell_pric => $clothing->product_sell_price,
			product_brand => $clothing->product_brand,
			product_title => $clothing->product_title,
			files => explode(',', $clothing->files),
			returns => "false",
			reason => "",
			color => $clothing->color,
			size => $clothing->size,
			size_type_major => "",
			size_number_minor => "",
			number => 1,
			has_tag => $sellorders_product->has_tag,
			has_accessory => $sellorders_product->has_accessory,
			shipping_wash_id => $sellorders_product->shipping_wash_id
		]);

		$this->buyorder->update($order->id, $order_data);
		$this->Sub_order_model->update($sub_order->id, array_merge($order_data, [
			'products' => json_encode($sub_order_products)
		]));

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			show_error('errors happened in transaction. in put_one_into_physical_buyorder');
		}
	}

	function get_similar_clothing($id) {
		$clothing = $this->get_single($id);
		$clothings = array();

		$styles = explode(",", $clothing['style']);

		foreach ($styles as $style) {
			$where = "FIND_IN_SET('" . $style . "', style)";

			$query = $this->db->order_by("publication_time", "desc")
				->where('on_shelf', '1')
				->where_in('lock_status', [0])
				->where('publication_time <', date('Y-m-d H:i:s'))
				->where('product_type', $clothing['product_type'])
				->where('pattern', $clothing['pattern'])
				->where($where)
				->get('product_clothing');
			$rows = $query->result_array();
			$clothings = array_unique(array_merge($clothings, $rows), SORT_REGULAR);

			if (count($clothings) >= 8) {
				break;
			}
		}

		return array_slice($clothings, 0, 9);
	}

	// this function is deprecated
	function get_similar($style, $product_type) {
		$result_array = array();

		$styles = explode(",", $style);

		// get similar clothing from the same style
		foreach ($styles as $style) {
			$where = "FIND_IN_SET('" . $styles[0] . "', style)";
			$query = $this->db->order_by("add_time", "desc")
				->where($where)
				->where_in('lock_status', [0])
				->where('publication_time <', date('Y-m-d H:i:s'))
				->limit(10)
				->get('product_clothing');
			$row = $query->result_array();
			$result_array = array_unique(array_merge($result_array, $row), SORT_REGULAR);

			// just provide about 10 similar clothing to users
			if (count($result_array) > 10) {
				break;
			}
		}

		// if we didnt get enough clothing from style,
		// then get clothing from same product_type
		if (count($result_array) < 10) {
			$query = $this->db->order_by("add_time", "desc")
				->where('product_type', $product_type)
				->where('publication_time <', date('Y-m-d H:i:s'))
				->where_in('lock_status', [0])
				->limit(10)
				->get('product_clothing');
			$row = $query->result_array();
			$result_array = array_unique(array_merge($result_array, $row), SORT_REGULAR);
		}

		// if we still didnt get enough clothing from style and product_type,
		// then get clothing from whatever
		if (count($result_array) < 21) {
			$query = $this->db->order_by("add_time", "desc")
				->where_in('lock_status', [0])
				->where('publication_time <', date('Y-m-d H:i:s'))
				->limit(21)
				->get('product_clothing');
			$row = $query->result_array();
			$result_array = array_unique(array_merge($result_array, $row), SORT_REGULAR);
		}

		return $result_array;
	}

	function reduction_again_clothing() {
		$query = $this->db// ->order_by("files", "desc")
			->order_by("publication_time", "desc")
			->where_in('lock_status', [0, 1, 2])
			->where('reduction_again', true)
			->get('product_clothing');

		$row = $query->result_array();

		// check if it's suggested item
		$this->load->model('Suggested_items_model');
		$suggested_clothing_ids = $this->Suggested_items_model->get_all_clothing_ids();
		foreach ($row as &$clothing) {
			$clothing['is_recommended'] = false;
			if (in_array($clothing['id'], $suggested_clothing_ids)) {
				$clothing['is_recommended'] = true;
			}
		}
		return $row;
	}

	function get_many_by_pattern($pattern) {
		$query = $this->db// ->order_by("files", "desc")
			->order_by("add_time", "desc")
			->where_in('lock_status', [0, 1, 2])
			->where('pattern', $pattern)
			->get('product_clothing');

		$row = $query->result_array();

		// check if it's suggested item
		$this->load->model('Suggested_items_model');
		$suggested_clothing_ids = $this->Suggested_items_model->get_all_clothing_ids();
		foreach ($row as &$clothing) {
			$clothing['is_recommended'] = false;
			if (in_array($clothing['id'], $suggested_clothing_ids)) {
				$clothing['is_recommended'] = true;
			}
		}
		return $row;
	}

	function get_branded_clothings($limit = 20, $offset = 0, $type = 'new') {
		$query = $this->db// ->order_by("files", "desc")
			->order_by("add_time", "desc")
			->where_in('lock_status', [0, 1, 2])
			->where('branded', '1')
			->limit($limit, $offset);

		if ($type === 'new') {
			// do nothing
		} else if ($type === 'men') {
			$query = $query->like('product_type', '2', 'after');
		} else if ($type === 'women') {
			$query = $query->like('product_type', '1', 'after');
		} else if ($type === 'kids') {
			$query = $query->like('product_type', '3', 'after');
		}

		$query = $query->get('product_clothing');

		$row = $query->result_array();

		// check if it's suggested item
		$this->load->model('Suggested_items_model');
		$suggested_clothing_ids = $this->Suggested_items_model->get_all_clothing_ids();
		foreach ($row as &$clothing) {
			$clothing['is_recommended'] = false;
			if (in_array($clothing['id'], $suggested_clothing_ids)) {
				$clothing['is_recommended'] = true;
			}
		}
		return $row;

	}

	function get_entries($limit = 20, $offset = 0, $type = 'new') {
		$query = $this->db// ->order_by("files", "desc")
			->where('on_shelf', '1')
			->order_by("publication_time", "desc")
			->where_in('lock_status', [0, 1, 2])
			->where('publication_time <', date('Y-m-d H:i:s'))
			->limit($limit, $offset);

		if ($type === 'new') {
			// do nothing
		} else if ($type === 'men') {
			$query = $query->like('product_type', '2', 'after');
		} else if ($type === 'women') {
			$query = $query->like('product_type', '1', 'after');
		} else if ($type === 'kids') {
			$query = $query->like('product_type', '3', 'after');
		}

		$query = $query->get('product_clothing');

		$row = $query->result_array();

		// check if it's suggested item
		$this->load->model('Suggested_items_model');
		$suggested_clothing_ids = $this->Suggested_items_model->get_all_clothing_ids();
		foreach ($row as &$clothing) {
			$clothing['is_recommended'] = false;
			if (in_array($clothing['id'], $suggested_clothing_ids)) {
				$clothing['is_recommended'] = true;
			}
		}

		return $row;

	}

	function get_number_by_factors(
		$type = 'new', $season = '', $color = '', $style, $subtype = '',
		$pattern = '', $keyword = '', $item_long, $size, $status, $price_to, $price_from) {
		$query = $this->construct_query_with_factors(
			$type, $season, $color, $style, $subtype, $pattern, $keyword, $item_long, $size, $status, $price_to, $price_from);
		$query = $query->select('id');
		$query = $query->where('on_shelf', '1');
		$query = $query->get('product_clothing');
		$row = $query->result_array();
		return count($row);
	}

	private function construct_query_with_factors(
		$type = 'new', $season = '', $color = '', $style = '', $subtype = '', $pattern = '', $keyword = '', $item_long, $size = '', $status = '', $price_to = '', $price_from = '') {
		$query = $this->db// ->order_by("files", "desc")
			->order_by("publication_time", "desc")
			->where('publication_time <', date('Y-m-d H:i:s'))
			->where_in('lock_status', [0, 1, 2]);

		if ($type === 'new') {
			// do nothing
		} else if ($type === 'men') {
			$query = $query->like('product_type', '2', 'after');
		} else if ($type === 'women') {
			$query = $query->like('product_type', '1', 'after');
		} else if ($type === 'kids') {
			$query = $query->like('product_type', '3', 'after');
		}

		if (!empty($season)) {
			$season_array = explode(',', $season);
			$query = $query->where_in('season', $season_array);
		}
		if (!empty($color)) {
			$color_array = explode(',', $color);
			$query = $query->where_in('color', $color_array);
		}
		if ($size) {
			$size_array = explode(',', $size);
			$query = $query->where_in('size_standard', $size_array);
		}
		if ($status) {
			$status_array = explode(',', $status);
			$query = $query->where_in('item_status', $status_array);
		}

		if ($price_to) {
			$query = $query->where('product_sell_price <= ', $price_to);
		}

		if ($price_from) {
			$query = $query->where('product_sell_price >= ', $price_from);
		}

		// this is buggy, result is not corret
		/*
		if (!empty($style)){
		$style_array = explode(',', $style);
		$query = $query->where_in('style', $style_array);
		}
		 * */

		// and this is fucking awesome
		// create the SQL statement manually
		if (!empty($style)) {
			$style_array = explode(',', $style);
			$where = '';
			foreach ($style_array as $style) {
				$where .= " FIND_IN_SET('" . $style . "', `style`) OR ";
				// $query = $query->or_where($where);
			}
			$where = substr($where, 0, -3);
			$where = '(' . $where . ')';
			$query = $query->where($where, NULL, false);
		}

		if (!empty($subtype)) {
			$query = $query->like('product_type', $subtype, 'after');
		}
		if (!empty($pattern)) {
			$pattern_array = explode(',', $pattern);
			$query = $query->where_in('pattern', $pattern_array);
		}
		if (!empty($keyword)) {
			$query = $query->like('product_brand', $keyword);
		}
		if (!empty($item_long)) {
			$item_long_array = explode(',', $item_long);
			$query = $query->where_in('item_long', $item_long_array);
		}

		return $query;
	}

	function get_many_by_factors(
		$limit = 20, $offset = 0, $type = 'new', $season = '',
		$color = '', $style = '', $subtype = '', $pattern = '',
		$keyword = '', $item_long, $size, $status, $price_to, $price_from) {
		$query = $this->construct_query_with_factors(
			$type, $season, $color, $style, $subtype, $pattern,
			$keyword, $item_long, $size, $status, $price_to, $price_from);

		$query = $query->where('on_shelf', '1');
		$query = $query->limit($limit, $offset);
		$query = $query->get('product_clothing');
		$row = $query->result_array();

		// check if it's suggested item
		$this->load->model('Suggested_items_model');
		$suggested_clothing_ids = $this->Suggested_items_model->get_all_clothing_ids();
		foreach ($row as &$clothing) {
			$clothing['is_recommended'] = false;
			if (in_array($clothing['id'], $suggested_clothing_ids)) {
				$clothing['is_recommended'] = true;
			}
		}
		return $row;
	}

	function get_one_by_product_item_number($product_item_number = "") {
		$this->db->from('product_clothing')->where('product_item_number', $product_item_number);
		$query = $this->db->get();
		$row = $query->result_array();
		$item = $row[0];

		/* $item['files'] is not usable
		 * so transform it into an usable array
		 */

		if (count($row) != 1) {
			show_error("according to product_item_number, get not one product_clothing but" . count($row) . ", it means you shelf the same clothing for more than once");
		} else {
			return $item;
		}

	}

	/* update one item by product item number
	 * this is made for admin site calling update API.
	 *
	 * @author howtomakeaturn
	 */
	function update_one_by_product_item_number($product_item_number = "", $data = []) {
		$this->db->from('product_clothing')->where('product_item_number', $product_item_number);
		$query = $this->db->get();
		$row = $query->result_array();
		$order = $row[0];

		if (count($row) != 1) {
			throw new Exception("doesnt get exactly one clothing");
		} else {
			$this->db->where('product_item_number', $product_item_number);
			$this->db->update('product_clothing', $data);
		}
	}

	function get_previous_id_of($id = "") {
		$target = $this->get_single($id);

		$query = $this->db->select('id')
			->order_by("publication_time", "asc")
			->where_in('lock_status', [0, 1, 2])
			->where('add_time >', $target['add_time'])
			->where('publication_time <', date('Y-m-d H:i:s'))
			->limit(1)
			->get('product_clothing');

		$row = $query->result_array();
		// the newest or oldest clothing doesn't have 'next' or 'previous'. make it loop
		if (empty($row)) {
			$query = $this->db->select('id')
				->order_by("add_time", "asc")
				->where_in('lock_status', [0, 1, 2])
				->where('publication_time <', date('Y-m-d H:i:s'))
				->limit(1)
				->get('product_clothing');

			$row = $query->result_array();
		}

		$clothing = $row[0];

		return $clothing['id'];
	}

	function get_next_id_of($id = "") {
		$target = $this->get_single($id);

		$query = $this->db->select('id')
			->order_by("publication_time", "desc")
			->where_in('lock_status', [0, 1, 2])
			->where('add_time <', $target['add_time'])
			->where('publication_time <', date('Y-m-d H:i:s'))
			->limit(1)
			->get('product_clothing');

		$row = $query->result_array();
		// the newest or oldest clothing doesn't have 'next' or 'previous'. make it loop
		if (empty($row)) {
			$query = $this->db->select('id')
				->order_by("add_time", "desc")
				->where_in('lock_status', [0, 1, 2])
				->where('publication_time <', date('Y-m-d H:i:s'))
				->limit(1)
				->get('product_clothing');

			$row = $query->result_array();
		}
		$clothing = $row[0];

		return $clothing['id'];
	}

	function get_single($id = "") {
		$this->db->from('product_clothing')->where('id', $id);
		$query = $this->db->get();
		$row = $query->result_array();
		$clothing = $row[0];

		if (is_null($clothing)) {
			show_404();
		}
		$this->load->config('clothing_class');

		$class_name = $this->config->item('class_name');
		$season_name = $this->config->item('season_name');
		$pattern_name = $this->config->item('pattern_name');

		/* product_type, season, pattern are just numbers, not usable
		 * so map them to meaningful string
		 */
		$clothing['product_type_name'] = $class_name[$clothing['product_type']]; //notice it's different from product_type
		$clothing['season_name'] = $season_name[$clothing['season']]; //notice it's different from season
		$clothing['pattern_name'] = $pattern_name[$clothing['pattern']]; //notice it's different from season

		/* $clothing['files'] is not usable
		 * so transform it into an usable array
		 */
		$clothing['pics'] = explode(",", $clothing['files']);

		/* #1
		 * $clothing['style'] is not usable
		 * so transform it into array first
		 */
		$style_numbers = explode(",", $clothing['style']);
		$clothing['style_numbers'] = array();
		if (!empty($clothing['style'])) {
			foreach ($style_numbers as $style_number) {
				$clothing['style_numbers'][] = (int) $style_number;
			}
		}

		/* #2
		 * now mapping $clothing['style_numbers'] to $clothing['style_names']
		 * which is much usable
		 */
		$style_name = $this->config->item('style_name');
		$clothing['style_names'] = array();
		foreach ($clothing['style_numbers'] as $style_number) {
			$clothing['style_names'][] = $style_name[$style_number];
		}

		/* let's find the proper clothing measure image.
		 * it's a little bit complicated. the algorithm is copied from admin/assets/javascript/view/shelf/scan.js setInitImg function
		 * made by danny i think
		 */
		$measure_img_url = substr($clothing['product_type'], 0, 2);
		if (!($measure_img_url == "14" || $measure_img_url == "15" || $measure_img_url == "24" || $measure_img_url == "31" || $measure_img_url == "35")) {
			if (!($measure_img_url == "33" && $clothing["pattern"] == "20")) {
				$measure_img_url .= $clothing["item_long"];
			} //end if
			else {
				$measure_img_url .= 'skirt';
			}
		} //end if

		$clothing["size_code"] = $measure_img_url;

		$measure_img_url = "/admin/assets/images/clothing_table/" . $measure_img_url . ".png";
		$clothing["measure_img_url"] = $measure_img_url;

		$clothing["measure"] = json_decode($clothing["measure"], true);

		// get additional fields
		if (!empty($clothing['descript'])) {
			$additional_fields = array();
			$descript = json_decode($clothing['descript']);
			foreach ($descript as $key => $value) {
				$additional_fields[$key] = $value;
			}
			$clothing['additional_fields'] = $additional_fields;
		}

		$clothing['is_suggested'] = false;
		$query = $this->db->where('clothing_id', $clothing['id'])
			->from('suggested_items')
			->get();
		if ($query->num_rows != 0) {
			$clothing['is_suggested'] = true;
		}

		return $clothing;
	}

	function get_some($ids = array(), $withCloset = false) {
		$this->db->select('clothing.id, clothing.product_item_number, clothing.product_title, clothing.product_sell_price, clothing.files, clothing.launch_way, clothing.user_id')->from('product_clothing clothing')->where_in('clothing.id', $ids);

		if ($withCloset) {
			$this->db->join('closet', 'clothing.user_id = closet.user_id')->select('closet.title as closet_title, closet.id as closet_id');
		}

		$query = $this->db->get();
		$rows = $query->result_array();

		return $rows;
	}

	// this function check if all the passed in clothing are available for purchasing.
	function are_all_unlocked($ids = array()) {
		$this->db->select('lock_status')->from('product_clothing')->where_in('id', $ids);
		$query = $this->db->get();
		$rows = $query->result_array();

		foreach ($rows as $clothing) {
			if ($clothing['lock_status'] != 0) {
				return false;
			}

		}
		return true;
	}

	function get_some_by_order_id($order_id = '') {
		$this->db->select('product_item_number, product_title, product_sell_price, files')->from('product_clothing')->where('buy_order_id', $order_id);
		$query = $this->db->get();
		$rows = $query->result_array();

		/* $clothing['files'] is not usable
		 * so transform it into an usable array
		 */
		$this->load->helper('Product_clothing_helper');
		foreach ($rows as &$clothing) {
			$file_ids = explode(',', $clothing['files']);
			$clothing['main_pic'] = clothing_image_url($file_ids[0]);
		}

		return $rows;
	}

	/*
	 * this method is almost the same as get_some_by_order_id.
	 * Because admin site need washing number information, but I don't wanna affect
	 * other places using the method, so I duplicate and modify for a little.
	 * yes, you should combine them if you have time.
	 *
	 * @author howtomakeaturn
	 */
	function get_some_by_order_id_and_join_sellorder_product($order_id = '') {
		$this->db->select('c.id, c.product_item_number, c.product_title, c.lock_status,
                                       c.product_sell_price, c.files, s.shipping_wash_id, s.has_tag, s.has_accessory')
			->from('product_clothing c')
			->join('sellorders_products s', 's.id = c.product_id')
			->where('buy_order_id', $order_id);
		$query = $this->db->get();
		$rows = $query->result_array();

		/* $clothing['files'] is not usable
		 * so transform it into an usable array
		 */
		$this->load->helper('Product_clothing_helper');
		foreach ($rows as &$clothing) {
			$file_ids = explode(',', $clothing['files']);
			$clothing['main_pic'] = clothing_image_url($file_ids[0]);
		}

		return $rows;
	}

	function get_many_by_user_id($user_id = '') {
		$this->db->select('product_title, product_original_price,
                                        product_sell_price, files, id, lock_status, reduction_again, size');
		$this->db->from('product_clothing clothing');
		$this->db->where('user_id', $user_id);

		$query = $this->db->get();
		$items = $query->result_array();

		// check if it's suggested item
		$this->load->model('Suggested_items_model');
		$suggested_clothing_ids = $this->Suggested_items_model->get_all_clothing_ids();
		foreach ($items as &$clothing) {
			$clothing['is_recommended'] = false;
			if (in_array($clothing['id'], $suggested_clothing_ids)) {
				$clothing['is_recommended'] = true;
			}
		}

		return $items;
	}

	function assign_some_to_buy_order($order_id = '', $ids = array()) {
		$this->db->select('lock_status')->from('product_clothing')->where_in('id', $ids);
		$query = $this->db->get();
		$rows = $query->result_array();

		foreach ($rows as $clothing) {
			if ($clothing['lock_status'] != '0') {
				// throw new Exception('some of the clothing is not in lock_status 0, while assign_some_to_buy_order');
				show_error('some of the clothing is not in lock_status 0, while assign_some_to_buy_order');

			}
		}
		$data = array(
			'lock_status' => '1',
			'buy_order_id' => $order_id,
			'last_locked_time' => date("Y-m-d H:i:s"),
		);
		$this->db->where_in('id', $ids);
		$this->db->update('product_clothing', $data);
	}

	function switch_some_to_paid_status($ids = array()) {
		if (count($ids) == 0) {
			show_error("switch_some_to_paid_status: ids是空陣列");
		}

		$this->db->select('*')->from('product_clothing')->where_in('id', $ids);
		$query = $this->db->get();
		$rows = $query->result_array();

		if (count($rows) == 0) {
			show_error("switch_some_to_paid_status: ids在資料庫內找不到衣服");
		}

		foreach ($rows as $clothing) {
			if ($clothing['lock_status'] != '1') {
				show_error('some of the clothing is not in lock_status 1, while switch_some_to_paid_status');
			}
		}

		$data = array(
			'lock_status' => '2',
		);

		$this->db->trans_start();

		$this->db->where_in('id', $ids);
		$this->db->update('product_clothing', $data);

		/*
		$this->load->model('Transaction_model', 'transaction');
		foreach($rows as $clothing){
		$owner_id = $this->get_user_id_by_product_item_number($clothing['product_item_number']);
		if ((int) $clothing['product_sell_price'] != 0){
		$this->transaction->insert_to_user($owner_id, '賣出商品', $clothing['product_item_number'], (int) $clothing['product_sell_price'], '');
		}
		}
		 */

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			show_error('errors happened in transaction. in switch_some_to_paid_status');
		}

	}

	function checkout_some($ids = array()) {
		if (count($ids) == 0) {
			show_error("switch_some_to_paid_status: ids是空陣列");
		}

		$this->db->select('*')->from('product_clothing')->where_in('id', $ids);
		$query = $this->db->get();
		$rows = $query->result_array();

		if (count($rows) == 0) {
			show_error("switch_some_to_paid_status: ids在資料庫內找不到衣服");
		}

		foreach ($rows as $clothing) {
			if ($clothing['lock_status'] != '2') {
				show_error('some of the clothing is not in lock_status 2, while checkout_some');
			}
		}

		$this->db->trans_start();

		$this->load->model('Transaction_model', 'transaction');
		foreach ($rows as $clothing) {
			$owner_id = $this->get_user_id_by_product_item_number($clothing['product_item_number']);
			if ((int) $clothing['product_sell_price'] != 0) {
				$item_number = $clothing['product_item_number'];
				$final_amount = (int) floor(((int) $clothing['product_sell_price']) * 0.75);
				$this->transaction->insert_to_user($owner_id, '賣出商品', $item_number, $final_amount, '');
				$this->notify_the_clothing_is_soldout($item_number);
			}
		}

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			show_error('errors happened in transaction. in checkout_some');
		}
	}

	// this dirty and quick fix is no longer needed :D
	/*
	function unlock_unpaid_clothing(){
	$this->load->config('checkout');
	$active_time = $this->config->item('active_time');

	$this->db->select('id, lock_status, last_locked_time')->from('product_clothing')->where('lock_status', '1');
	$query = $this->db->get();
	$rows = $query->result_array();

	$unlock_ids = array();

	foreach($rows as $clothing){
	$now_time = strtotime(date("Y-m-d H:i:s"));
	$last_locked_time = strtotime($clothing['last_locked_time']);
	$delta = $now_time - $last_locked_time;

	if ($delta > 60 * $active_time)
	array_push($unlock_ids, $clothing['id']);
	}
	$data = array(
	'lock_status' => '0',
	'buy_order_id' => '',
	'last_locked_time' => ''
	);
	$this->db->where_in('id', $unlock_ids);
	$this->db->update('product_clothing', $data);
	}
	 */

	function get_all_on_shelf() {
		$this->db->select('c.added_user_id, c.product_item_number, c.product_title, o.order_number, o.user_id, c.product_brand, c.product_original_price, c.product_sell_price, c.lock_status, c.suggested_price, c.id, c.add_time, p.shipping_wash_id, c.publication_time');
		$this->db->from('product_clothing c');
		$this->db->where('c.on_shelf', '1');
		$this->db->join('sellorders_products p', 'p.id = c.product_id'); // this joins the user table to topics
		$this->db->join('sellorders o', 'o.id = p.order_id'); // this joins the quote table to the topics table

		$query = $this->db->get();

		$row = $query->result_array();
		return $row;
	}

	public function get_all_on_draft() {
		$this->db->select('c.added_user_id, c.product_item_number, c.product_title, o.order_number, o.user_id, c.product_brand, c.product_original_price, c.product_sell_price, c.lock_status, c.suggested_price, c.id, c.add_time, p.shipping_wash_id, c.publication_time');
		$this->db->from('product_clothing c');
		$this->db->where('c.lock_status', '4');
		$this->db->join('sellorders_products p', 'p.id = c.product_id'); // this joins the user table to topics
		$this->db->join('sellorders o', 'o.id = p.order_id'); // this joins the quote table to the topics table

		$query = $this->db->get();

		$row = $query->result_array();
		return $row;
	}

	public function launch_drafts_by_product_item_numbers($draft_ids) {
		return $this->db->where_in('id', $draft_ids)->update('product_clothing', array(
			'lock_status' => 0,
			'on_shelf' => 1,
			'edit_time' => time(),
			'publication_time' => date('c'),
		));
	}

	function get_all_off_shelf() {
		$this->db->select('c.product_item_number, c.product_title, o.order_number, o.user_id, c.product_brand, c.product_original_price, c.product_sell_price, c.lock_status, c.suggested_price, c.id, c.branded, c.add_time, p.shipping_wash_id, c.publication_time, bo.paid_at as soldout_time');
		$this->db->from('product_clothing c');
		$this->db->where('c.on_shelf', '0');
		$this->db->join('sellorders_products p', 'p.id = c.product_id', 'left'); // this joins the user table to topics
		$this->db->join('sellorders o', 'o.id = p.order_id', 'left'); // this joins the quote table to the topics table
		$this->db->join('buy_order bo', 'bo.id = c.buy_order_id', 'left'); // this joins the quote table to the topics table

		$query = $this->db->get();

		$row = $query->result_array();
		return $row;
	}

	function get_all() {

		$this->db->select('c.product_item_number, c.product_title, o.order_number, o.user_id, c.product_brand, c.product_original_price, c.product_sell_price, c.lock_status, c.suggested_price, c.id, c.branded, c.add_time, p.shipping_wash_id, c.publication_time');
		$this->db->from('product_clothing c');
		$this->db->join('sellorders_products p', 'p.id = c.product_id'); // this joins the user table to topics
		$this->db->join('sellorders o', 'o.id = p.order_id'); // this joins the quote table to the topics table

		$query = $this->db->get();

		$row = $query->result_array();
		return $row;
	}

	function add_stupid_row($to = 'STUPID_ROW') {
		$data = array(
			'product_title' => $to,
			'product_id' => '9999',
			'product_original_price' => '200',
			'product_sell_price' => '100',
			'add_time' => time(),
			'edit_time' => time(),
		);

		$this->db->insert('product_clothing', $data);
		return $data;
	}

	public function calculate_prices($clothing_array = array()) {
		$prices = array();
		$prices['items_price'] = 0;
		$prices['shipping_price'] = 0;
		$prices['total_price'] = 0;

		foreach ($clothing_array as $clothing) {
			$prices['items_price'] += $clothing['product_sell_price'];
		}

		if (($prices['items_price'] < 1000) && ($prices['items_price'] > 0)) {
			$prices['shipping_price'] = 80;
			$this->load->model('System_configuration_model', 'sys_config');
			if ($this->sys_config->get_free_shipping()) {
				$prices['shipping_price'] = 0;
			}
//            $prices['shipping_price'] = 0;
		}
		$prices['total_price'] = $prices['items_price'] + $prices['shipping_price'];
		return $prices;
	}

	public function notify_the_clothing_is_soldout($product_item_number) {
		$user_id = $this->get_user_id_by_product_item_number($product_item_number);
		$clothing = $this->db->where('product_item_number', $product_item_number)
			->from('product_clothing')
			->get()->result()[0];
		// $email = $this->db->get_where('users', array('id' => $user_id))->result()[0]->email;
		$this->load->model('User_model');
		$email = $this->User_model->get_effective_email($user_id);

		/*
		$html = $this->load->view('email/notify_the_clothing_is_soldout',
		['clothing' => $clothing] , true);
		 */
		$this->load->spark('codeigniter-template/1.0.0');
		$this->template->set_layout('template/email');
		$html = $this->template->render('email/notify_the_clothing_is_soldout',
			['clothing' => $clothing], true);

		$this->load->library('notification');
		$this->notification->send('Getmore衣服售出通知', $html, $email);
	}

	public function notify_the_clothing_is_reduction_again($id) {
		$likes = $this->db->from('clothing_likes')
			->where('clothing_id', $id)
			->get()->result_array();
		foreach ($likes as $like) {
			$clothing = $this->db->where('id', $like['clothing_id'])
				->from('product_clothing')
				->get()->row_array();
			$this->load->model('User_model');
			$email = $this->User_model->get_effective_email($like['user_id']);

			$this->load->spark('codeigniter-template/1.0.0');
			$this->template->set_layout('template/email');
			$html = $this->template->render('email/notify_the_clothing_is_reduction_again',
				['clothing' => $clothing], true);
			$this->load->library('notification');
			$this->notification->send('Getmore欲望清單降價通知信', $html, $email);
		}

		return true;
	}

	/*
	 * set suggested price for the clothing.
	 * also insert tuple to the notification queue if needed.
	 *
	 * @return Boolean
	 * @author howtomakeaturn
	 */
	function set_suggested_price($id, $price) {
		$this->update($id, array('suggested_price' => $price));
		$this->load->model('Notification_model');

		$row = $this->db->from('notifications')->where('notified', FALSE)->where('clothing_id', $id)->get()->result();
		// insert into the notification queue for this week
		// if it's not in the database.
		if (!empty($row)) {
			return TRUE;
		} else {
			$this->Notification_model->create($id);
			return TRUE;
		}

	}

	function like_clothing($user_id, $clothing_id) {
		if (empty($user_id)) {
			throw new Exception('empty user id');
		}
		if (empty($clothing_id)) {
			throw new Exception('empty clothing id');
		}

		$query = $this->db->where('user_id', $user_id)
			->where('clothing_id', $clothing_id)
			->from('clothing_likes')
			->get();

		// like the same item twice is not serious problem.
		// just return TRUE, how do you think :)
		if ($query->num_rows() != 0) {
			return TRUE;
		}

		// release the resources
		$query->free_result();

		$data = array(
			'user_id' => $user_id,
			'clothing_id' => $clothing_id,
		);

		$this->db->insert('clothing_likes', $data);
		return TRUE;
	}

	function unlike_clothing($user_id, $clothing_id) {
		if (empty($user_id)) {
			throw new Exception('empty user id');
		}
		if (empty($clothing_id)) {
			throw new Exception('empty clothing id');
		}

		$query = $this->db->where('user_id', $user_id)
			->where('clothing_id', $clothing_id)
			->from('clothing_likes')
			->get();

		// like the same item twice is not serious problem.
		// just return TRUE, how do you think :)
		if ($query->num_rows() == 0) {
			return TRUE;
		}

		$this->db->where('id', $query->row()->id)->delete('clothing_likes');
		return TRUE;
	}

	/*
	 * Use this function so you don't need to determine
	 * whether the user liked the clothing before or not
	 *
	 * @params Integer
	 * @params Integer
	 * @return Boolean
	 * @author howtomakeaturn
	 */
	function toggle_like_clothing($user_id, $clothing_id) {

		$query = $this->db->where('user_id', $user_id)
			->where('clothing_id', $clothing_id)
			->from('clothing_likes')
			->get();

		if ($query->num_rows() == 0) {
			return $this->like_clothing($user_id, $clothing_id);
		} else {
			return $this->unlike_clothing($user_id, $clothing_id);
		}
	}

	/*
	 * Use this function so you don't need to determine
	 * whether the user liked the clothing before or not
	 *
	 * @author howtomakeaturn
	 */
	function just_like_clothing($user_id, $clothing_id) {

		$query = $this->db->where('user_id', $user_id)
			->where('clothing_id', $clothing_id)
			->from('clothing_likes')
			->get();

		if ($query->num_rows() == 0) {
			return $this->like_clothing($user_id, $clothing_id);
		} else {
			return TRUE;
		}
	}

	/*
	 * append my_like_id attribute to the input array.
	 * because we need to know
	 *
	 * @params Array
	 * @params Integer
	 * @return Array
	 *
	 * @author howtomakeaturn
	 */

	function append_my_like_id(&$clothings, $user_id) {
		foreach ($clothings as &$clothing) {

			$query = $this->db->where('clothing_id', $clothing['id'])
				->where('user_id', $user_id)
				->from('clothing_likes')
				->get();

			if ($query->num_rows() === 0) {
				$clothing['my_like_id'] = null;
			} else {
				$like = $query->row_array();
				$clothing['my_like_id'] = $like['id'];
			}
		}

		return $clothings;
	}

	function append_my_like_id_single(&$clothing, $user_id) {

		$query = $this->db->where('clothing_id', $clothing['id'])
			->where('user_id', $user_id)
			->from('clothing_likes')
			->get();

		if ($query->num_rows() === 0) {
			$clothing['my_like_id'] = null;
		} else {
			$like = $query->row_array();
			$clothing['my_like_id'] = $like['id'];
		}
		return $clothing;
	}

	function remove_from_shelf() {

		// get sold-out clothing which are still on shelf.
		$items = $this->db->select('buy_order_id, id')
			->from('product_clothing')
			->where('on_shelf', '1')
			->where('lock_status', '2')
			->get()->result_array();

		// prepare the updated data here
		$updated_data = array('on_shelf' => '0');

		foreach ($items as $item) {
			// get the corresponding order by the item
			$order = $this->db->select('paid_at')
				->from('buy_order')
				->where('id', $item['buy_order_id'])
				->get()->row_array();

			// get the time difference
			$time_diff = abs(strtotime($order['paid_at']) - strtotime(date('c')));
			$days_diff = $time_diff / (60 * 60 * 24);

			// two weeks to remove it from shelf
			if ($days_diff > 14) {
				$this->db->where('id', $item['id'])
					->update('product_clothing', $updated_data);
			}

		} // end foreach

	}

	function get_num_of_soldout() {
		$query = $this->db// ->order_by("files", "desc")
			->order_by("publication_time", "desc")
			->where_in('lock_status', [2])
			->where('publication_time <', date('Y-m-d H:i:s'));

		$query = $query->get('product_clothing');

		return $query->num_rows();
	}

	function get_soldout($limit = 20, $offset = 0) {
		$query = $this->db// ->order_by("files", "desc")
			->order_by("publication_time", "desc")
			->where_in('lock_status', [2])
			->where('publication_time <', date('Y-m-d H:i:s'))
			->limit($limit, $offset);

		$query = $query->get('product_clothing');

		$row = $query->result_array();

		// check if it's suggested item
		$this->load->model('Suggested_items_model');
		$suggested_clothing_ids = $this->Suggested_items_model->get_all_clothing_ids();
		foreach ($row as &$clothing) {
			$clothing['is_recommended'] = false;
			if (in_array($clothing['id'], $suggested_clothing_ids)) {
				$clothing['is_recommended'] = true;
			}
		}

		return $row;

	}

	function getBySearch($keyword) {
		$query = $this->db// ->order_by("files", "desc")
			->where('on_shelf', '1')
			->order_by("publication_time", "desc")
			->where_in('lock_status', [0, 1, 2])
			->where('publication_time <', date('Y-m-d H:i:s'));

		$kws = explode(' ', $keyword);
		foreach ($kws as $kw) {
			$where = '';
			$where .= " product_title LIKE '%" . $kw .
			"%' OR product_brand REGEXP '^" . $kw .
			"' OR admin_comment LIKE '%" . $kw .
			"%' OR admin_remark LIKE '%" . $kw .
			"%' OR descript LIKE '%" . $kw . "%'";
			$where = '(' . $where . ')';
			$query = $query->where($where, NULL, false);
		}

		$query = $query->get('product_clothing');

		$row = $query->result_array();

		// check if it's suggested item
		$this->load->model('Suggested_items_model');
		$suggested_clothing_ids = $this->Suggested_items_model->get_all_clothing_ids();
		foreach ($row as &$clothing) {
			$clothing['is_recommended'] = false;
			if (in_array($clothing['id'], $suggested_clothing_ids)) {
				$clothing['is_recommended'] = true;
			}
		}

		return $row;

	}

	public function get_likes_count_by_product_id($product_id) {
		return $this->db->from('clothing_likes')->where('clothing_id', $product_id)->count_all_results();
	}

	public function make_reservation_on_shelf() {
		$this->db->where(array(
			'lock_status' => 3,
			'publication_time <' => date('Y-m-d H:i:s'),
			'on_shelf' => 0)
		)->update($this->_table, array(
			'lock_status' => 0,
			'publication_time' => date('Y-m-d H:i:s'),
			'on_shelf' => 1,
		));
	}

	public function get_brand_list() {
		$this->db->select('name, category_id');
		$this->db->from('brands');
		$this->db->where('approved', '1');

		$query = $this->db->get();
		$row = $query->result_array();

		return array_reduce($row, function ($result, $brand) {
			$result[$brand['category_id']][] = $brand['name'];
			return $result;
		}, array());
	}
}
?>
