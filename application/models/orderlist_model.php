<?php 
class Orderlist_model extends CI_Model {

    private $ch;

    function __construct(){
        parent::__construct();

        $this->ch = curl_init();

    }

    function sub_order($status){

        $url = "/restful/sub_order?status=" . $status;

        curl_setopt($this->ch, CURLOPT_URL, base_url($url));//GET
        curl_setopt($this->ch, CURLOPT_COOKIE, http_build_query($_COOKIE,'',';'));
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);

        $data = json_decode(curl_exec($this->ch));
        return $data;
    }
}
?>