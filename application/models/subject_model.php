<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subject_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        //$this->load->config('constants');
        $this->tables['subject']    = 'product_subjects';
        $this->tables['category']   = 'category';
        $this->tables['files']   = 'files';
    }

    /**
     * subjects
     *
     * @return object Categories
     * @author appleboy
     **/
    public function subjects()
    {
        // define in MY_Model
        $this->handle_process();

        // left join category
        $this->db->join($this->tables['category'], $this->tables['category'] . '.id = ' . $this->tables['subject'] . '.category_id', 'left');
        $this->db->join($this->tables['files'], $this->tables['files'] . '.file_id = SUBSTRING_INDEX(' . $this->tables['subject'] . '.file_list, ",", 1)', 'left');
        $this->response = $this->db->get($this->tables['subject']);

        return $this;
    }

    /**
     * subject
     *
     * @return object
     * @author appleboy
     **/
    public function subject($id = NULL)
    {
        $this->limit(1);
        $this->where($this->tables['subject'].'.id', $id);

        $this->subjects();

        return $this;
    }

    /**
     * Add subject
     *
     * @return bool
     * @author appleboy
     **/
    public function add_subject($data = array())
    {
        if (empty($data)) {
            return false;
        }

        $options = 0x00;
        if ($data['is_index'] == '1')
            $options = ($options | _IS_INDEX);
        if ($data['is_enabled'] == '1')
            $options = ($options | _IS_ENABLED);
        unset($data['is_enabled']);
        unset($data['is_index']);

        $another = array(
            'add_time'   => time(),
            'edit_time'  => time(),
            'options'    => $options
        );
        $data = array_merge($data, $another);

        $this->db->insert($this->tables['subject'], $data);
        $id = $this->db->insert_id();

        return $id;
    }

    /**
     * update subject
     *
     * @return bool
     * @author appleboy
     **/
    public function update_subject($id, $data = array())
    {
        if (empty($id) or empty($data)) {
            return false;
        }

        if (isset($data['is_index']) && isset($data['is_enabled'])) {
            $options = 0x00;
            if ($data['is_index'] == '1')
                $options = ($options | _IS_INDEX);
            if ($data['is_enabled'] == '1')
                $options = ($options | _IS_ENABLED);
            $data['options'] = $options;
        } else {
            if (isset($data['is_index'])) {
                if ($data['is_index'] == '1')
                    $this->db->set('options', '(options | ' . _IS_INDEX . ')', false);
                else
                    $this->db->set('options', '(options & ' . ~_IS_INDEX . ')', false);
            }
            if (isset($data['is_enabled'])) {
                if ($data['is_enabled'] == '1')
                    $this->db->set('options', '(options | ' . _IS_ENABLED . ')', false);
                else
                    $this->db->set('options', '(options & ' . ~_IS_ENABLED . ')', false);
            }
        }
        unset($data['is_enabled']);
        unset($data['is_index']);

        if(is_array($id))
            $this->db->where_in('id', $id);
        else
            $this->db->where('id', $id);

        unset($data['id']);
        $data = array_merge($data, array('edit_time' => time()));
        $this->db->update($this->tables['subject'], $data);

        return true;
    }

    /**
    * delete subject
    *
    * @return bool
    * @author appleboy
    **/
    public function delete_subject($id)
    {
        $this->db->trans_begin();

        if(is_array($id))
           $this->db->where_in('id', $id);
        else
           $this->db->where('id', $id);

        $this->db->delete($this->tables['subject']);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            return FALSE;
        }

        $this->db->trans_commit();

        return TRUE;
    }
}
