<?php 
class Marketplace_model extends CI_Model {

    private $ch;

    function __construct(){
        parent::__construct();
        $this->load->spark('curl');
        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_URL, base_url("/restful/marketplace"));
        curl_setopt($this->ch, CURLOPT_COOKIE, http_build_query($_COOKIE,'',';'));
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        $this->load->database();
    }
    
    function get_all(){
        curl_setopt($this->ch, CURLOPT_URL, base_url("/restful/marketplace"));
        $data = json_decode(curl_exec($this->ch));
        
        return $data;
    }

    function get_by_status($status = 0){
        $data = json_decode($this->curl->simple_get('/restful/marketplace', array(
            'lock_status' => $status
        ), array(
            CURLOPT_COOKIE => http_build_query($_COOKIE,'',';')
        )));
        return $data;
    }

    function add_clothing($clothing){
        curl_setopt($this->ch, CURLOPT_URL, base_url("/restful/marketplace"));
        curl_setopt($this->ch, CURLOPT_POST, true); // 啟用POST
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $clothing); 
        $data = json_decode(curl_exec($this->ch));
        // curl_close($this->ch);
        //var_dump($data);
        //exit(0);
        
        return $data;
    }

    function modify_trade($trade){
        curl_setopt($this->ch, CURLOPT_URL, base_url("/restful/trade"));
        curl_setopt($this->ch, CURLOPT_POST, true); // 啟用POST
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array("Content-Type:multipart/form-data")); // 啟用POST
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $trade); 
        $data = json_decode(curl_exec($this->ch));
        // curl_close($this->ch);
        return $data;
    }

    function getBySearch($keyword){
        $query = $this->db// ->order_by("files", "desc")
                                ->where('on_shelf', '1')
                                ->order_by("publication_time", "desc")
                                ->where_in('lock_status', [0, 1, 2])
                                ->where('publication_time <', date('Y-m-d H:i:s'));
                                
        $kws = explode(' ', $keyword);
        foreach($kws as $kw){
            $where = '';
            $where .= " product_title LIKE '%".$kw . 
                "%' OR product_brand REGEXP '^" . $kw . 
                "' OR admin_comment LIKE '%" . $kw . 
                "%' OR admin_remark LIKE '%" . $kw . 
                "%' OR descript LIKE '%" . $kw . "%'";
            $where = '(' . $where . ')';
            $query = $query->where($where, NULL, false);
        }
                                
        $query = $query->get('marketplace');

        $row = $query->result_array();
        
        // check if it's suggested item
        $this->load->model('Suggested_items_model');
        $suggested_clothing_ids = $this->Suggested_items_model->get_all_clothing_ids();
        foreach ($row as &$clothing){
            $clothing['is_recommended'] = false;
            if (in_array($clothing['id'], $suggested_clothing_ids)){
                $clothing['is_recommended'] = true;
            }
        }
        
        return $row;
        
    }
}
?>