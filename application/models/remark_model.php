<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Remark_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tables['remark'] = 'remarks';
    }

    /**
     * remarks
     *
     * @return object Categories
     * @author appleboy
     **/
    public function remarks()
    {
        // define in MY_Model
        $this->handle_process();

        $this->response = $this->db->get($this->tables['remark']);

        return $this;
    }

    /**
     * remark
     *
     * @return object
     * @author appleboy
     **/
    public function remark($id = NULL)
    {
        $this->limit(1);
        $this->where($this->tables['remark'].'.id', $id);

        $this->remarks();

        return $this;
    }

    /**
     * Add remark
     *
     * @return bool
     * @author appleboy
     **/
    public function add_remark($data = array())
    {
        if(empty($data))

            return false;

        // Remark table.
        $another = array(
            'add_time'   => time(),
            'edit_time'  => time()
        );
        $data = array_merge($data, $another);

        $this->db->insert($this->tables['remark'], $data);
        $id = $this->db->insert_id();

        return $id;
    }

    /**
     * update remark
     *
     * @return bool
     * @author appleboy
     **/
    public function update_remark($id, $data = array())
    {
        if(empty($id) or empty($data))

            return false;

        $data = array_merge($data, array('edit_time' => time()));
        $this->db->update($this->tables['remark'], $data, array('id' => $data['id']));

        return true;
    }

    /**
    * delete remark
    *
    * @return bool
    * @author appleboy
    **/
    public function delete_remark($id)
    {
        $this->db->trans_begin();

        if(is_array($id))
           $this->db->where_in('id', $id);
        else
           $this->db->where('id', $id);

        $this->db->delete($this->tables['remark']);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            return FALSE;
        }

        $this->db->trans_commit();

        return TRUE;
    }
}
