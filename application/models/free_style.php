<?php 
class Free_style extends CI_Model {

    private $ch;

    function __construct(){
        parent::__construct();

        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_URL, base_url("/restful/free_style"));
        curl_setopt($this->ch, CURLOPT_COOKIE, http_build_query($_COOKIE,'',';'));
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
    }

    function get_free_style(){
        $data = json_decode(curl_exec($this->ch));//GET
        // curl_close($this->ch);

        return $data;
    }

    function post_free_style($data){
        //$post_field_string = http_build_query($data, '', '&');
        
        curl_setopt($this->ch, CURLOPT_POST, TRUE);//POST

        curl_setopt($this->ch, CURLOPT_POSTFIELDS, http_build_query(array(
            'data' => $data
        )));

        $response = curl_exec($this->ch);
        // var_dump($data);
        // var_dump($response);
        
        // $response = json_decode($response);
        // var_dump($response);
        // $temp = array_reduce($response, function ($result, $tmp_id) use (&$i) {
        //     $result[] = $tmp_id->id;
        //     return $result;
        // }, array());
        // var_dump($temp);
        // $temp = implode(",", $temp);
        // var_dump($temp);
        // exit(0);
        return $response;
    }
}
?>