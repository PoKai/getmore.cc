<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sellorder_model extends MY_Model
{
    /**
     * Response
     *
     * @var string
     **/
    protected $response = NULL;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tables['order'] = 'sellorders';
        // $this->tables['order_event'] = 'orders_events';
        $this->tables['order_product'] = 'sellorders_products';
        $this->tables['clothing_product'] = 'product_clothing';
        $this->tables['status'] = 'orders_status';
        $this->tables['users'] = 'users';
    }

     /**
     * sellorders
     *
     * @return object Sellorders
     * @author danny
     **/
    public function sellorders()
    {
        // define in MY_Model
        $this->handle_process();

        $this->response = $this->db->get($this->tables['order']);

        return $this;
    }

     /**
     * sellorders
     *
     * @return object Sellorders
     * @author danny
     **/
    public function sellordersItem()
    {
        // define in MY_Model
        $this->handle_process();

        $this->response = $this->db->get($this->tables['order_product']);

        return $this;
    }

     /**
     * @author danny
     **/
    public function shoppingItem()
    {
        // define in MY_Model
        $this->handle_process();

	$this->db->join($this->tables['order_product'], $this->tables['order_product'] . '.order_id = ' . $this->tables['order'] . '.id', 'left');
        $this->response = $this->db->get($this->tables['order']);

        return $this;
    }


    /**
     * orders
     *
     * @return object
     * @author appleboy
     **/
    public function orders($type = 'order')
    {
        // define in MY_Model
        $this->handle_process();

        // get table
        switch ($type) {
            case 'order':
                $this->db->join($this->tables['status'], $this->tables['status'] . '.id = ' . $this->tables['order'] . '.status_id', 'left');
                $this->db->join($this->tables['users'], $this->tables['users'] . '.id = ' . $this->tables['order'] . '.user_id', 'left');
                $this->response = $this->db->get($this->tables['order']);
            break;
            case 'product':
                $this->response = $this->db->get($this->tables['order_product']);
            break;
            // case 'event':
            //     $this->response = $this->db->get($this->tables['order_event']);
            // break;
        }

        return $this;
    }

    /**
     * order
     *
     * @return object
     * @author appleboy
     **/
    public function order($id = NULL)
    {
        $this->limit(1);
        $this->where($this->tables['order'].'.id', $id);

        $this->orders();

        return $this;
    }

    /**
     * Add order
     *
     * @return int
     * @author appleboy
     **/
    public function add_order($data = array(), $type = 'order')
    {
        if(empty($data))

            return false;

        $this->db->trans_start();
        

        // get table name
        switch ($type) {
            case 'order':
                $table = $this->tables['order'];
                if (  (array_key_exists('deduction_amount', $data)) && ($data['deduction_amount'] != 0) ){
                    $user_id = $data['user_id'];
                    $order_number = $data['order_number'];
                    $deduction_amount = (int) $data['deduction_amount'];
                  
                    if ($deduction_amount > 0){
                        throw new Exception("invalid deduction amount: $deduction_amount. it can't be position");
                    }
                    
                    $this->load->model('Transaction_model', 'transaction');
                    $this->transaction->insert_to_user($user_id, "洗衣費抵扣", $order_number, $deduction_amount, '');
                    // also need transaction to lock database
                }
                if ($data['payment_method'] == 'atm'){
                    if (empty($data['last_five_number'])){
                        throw new Exception("empty last_five_number");
                    }
                    if (empty($data['estimated_remit_datetime'])){
                        throw new Exception("empty estimated_remit_datetime");
                    }
                    $data['status_id'] = '1.5';
                }
                break;
            case 'product':
                $table = $this->tables['order_product'];
            break;
            case 'clothing':
                $table = $this->tables['clothing_product'];
            break;
        }
        // select o.user_id from getmore_beta.sellorders_products products join getmore_beta.sellorders o on products.order_id = o.id where products.id = 6353;
        $another = array(
            'add_time'   => time(),
            'edit_time'  => time(),
            'user_id' => $this->db->select('user_id')->from('sellorders')->where('id', $data['order_id'])->get()->row_array()["user_id"]
        );
        unset($data['order_id']);
        $data = array_merge($data, $another);
        $this->db->insert($table, $data);
        $id = $this->db->insert_id();

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            show_error('errors happened in transaction. in add_order');
        }
        
        return $id;
    }

    /**
     * update order
     *
     * @return bool
     * @author appleboy
     **/
    public function update_order($id, $data = array())
    {
        if(empty($id) or empty($data))

            return false;

        $data = array_merge($data, array('edit_time' => time()));

        if (is_array($id)) {
            $this->db->where_in('id', $id);
        } else {
            $this->db->where('id', $id);
        }

        if (isset($data['id'])) {
            unset($data['id']);
        }

        $this->db->update($this->tables['order'], $data);

        return true;
    }

    /**
     * update userinfo
     *
     * @return bool
     * @author danny
     **/
    public function update_userinfo($id, $data = array())
    {
        if(empty($id) or empty($data))

            return false;

        $data = array_merge($data, array('edit_time' => time()));
        $this->db->where('id', $id);
        $this->db->update($this->tables['order'], $data);

        return true;
    }

    /**
     * update userinfo
     *
     * @return bool
     * @author danny
     **/
    public function update_iteminfo($id, $data = array())
    {
        if(empty($id) or empty($data))

            return false;

        $this->db->where('id', $id);
        $this->db->update($this->tables['order_product'], $data);

        return true;
    }

    /**
     * update userinfo
     *
     * @return bool
     * @author danny
     **/
    public function update_shippingstatus($id, $data = array())
    {
        //if(empty($id) or empty($data))
          //  return false;

        if(is_array($id))
            $this->db->where_in('id', $id);
        else
            $this->db->where('id', $id);

        $this->db->update($this->tables['order'], $data);

        return true;
    }

    /**
    * delete order
    *
    * @return bool
    * @author appleboy
    **/
    public function delete_order($id, $table='order')
    {
        $this->db->trans_begin();

        if(is_array($id))
           $this->db->where_in('id', $id);
        else
           $this->db->where('id', $id);

        $this->db->delete($this->tables[$table]);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            return FALSE;
        }

        $this->db->trans_commit();

        return TRUE;
    }
    /* shipped from buy_order_model
     * @author howtomakeaturn
     */
    function get_some_by_user_id($user_id=""){
        // only retrieve paid orders. in future, also retrieve unpaid orders
        // $this->db->from('sellorders')->where('user_id', $user_id)->where_in('status_id', [2,3,4,5]);
        
        
        $this->db->select('sellorders.*, COUNT(sellorders_products.id) as how_many')
                 ->from('sellorders')
                 // ->order_by('blog.id', 'desc')
                 ->order_by('add_time', 'desc')
                 ->where('user_id', $user_id)
                 ->where_in('status_id', [-1, 1, 1.5, 2,3,4,5, 6]);

        $this->db->join('sellorders_products', 'order_id = sellorders.id')
                 ->group_by('sellorders.id');        
        
        $query = $this->db->get();
        $orders = $query->result_array();
        return $orders;
    }
    
    /* shipped from buy_order_model
     * @author howtomakeaturn
     */
    function get_complete_order_by_order_number($order_number=""){
        $this->load->database();
        $this->db->from('sellorders')->where('order_number', $order_number);
        $query = $this->db->get();
        $order = $query->result_array()[0];
            
        // extends the data with some 'label' data for rendering
        // receipt
        if ($order['uniform_invoice_type'] == '0')
            $order['uniform_invoice_type_label'] = "捐贈創世基金會";
        else if ($order['uniform_invoice_type'] == '1')
            $order['uniform_invoice_type_label'] = "二聯式發票";              
        else if ($order['uniform_invoice_type'] == '2')
            $order['uniform_invoice_type_label'] = "三聯式發票";
    
        if ($order['payment_method'] == PAYMENT_CREDIT_CARD)
            $order['payment_method_label'] = "信用卡";
        else if ($order['payment_method'] == 'deduction')
            $order['payment_method_label'] = "全額折抵";


        $this->db->select('*')->from('sellorders_products')->where('order_id', $order['id']);
        $query = $this->db->get();
        $items = $query->result_array();


/*
        $this->load->model('Product_clothing_model');
        $clothing_array = $this->Product_clothing_model->get_some_by_order_id($order['id']);
        $prices = $this->Product_clothing_model->calculate_prices($clothing_array);
    */
        // send to view
        $data['order'] = $order;
        $data['items'] = $items;
        // $data['prices'] = $prices;
        
        return $data;
    }
    
    // change sellorder status
    function switch_sellorder_from_washed_to_done(){      
        $this->db->select('*')->from('sellorders')->where('status_id', '5');
        $query = $this->db->get();
        $rows = $query->result_array();

        $to_switched_ids = array();
        $to_switched_users = array();

        foreach($rows as $order){
            $now_time = strtotime(date("Y-m-d H:i:s"));
            $date_purchased = strtotime($order['date_purchased']);
            $delta = $now_time - $date_purchased;

            if ($delta > 86400 * 10){
                array_push($to_switched_ids, $order['id']);
                array_push($to_switched_users, $order['user_id']);
            }
        }
        $data = array(
            'status_id' => '6'
        );
        $this->db->where_in('id', $to_switched_ids);
        $this->db->update('sellorders', $data);

        $this->load->model('User_model');
        $this->User_model->increase_sell_count($to_switched_users);
    }
    
    /*
     * Send emails.
     * @author howtomakeaturn
     */
    public function notify_the_sellorder_is_paid($order_number){
        $complete_order = $this->get_complete_order_by_order_number($order_number);
        /*
        $html = $this->load->view('email/notify_the_sellorder_is_paid', 
            ['order' => $complete_order['order'], 
              'items' => $complete_order['items']
            ], true);
        */
        $this->load->spark('codeigniter-template/1.0.0');
        $this->template->set_layout('template/email');
        $html = $this->template->render('email/notify_the_sellorder_is_paid', 
            ['order' => $complete_order['order'], 
              'items' => $complete_order['items']
            ], true);

        
        // $email = $this->db->get_where('users', array('id' => $complete_order['order']['user_id']))->result()[0]->email;
        $this->load->model('User_model');
        $email = $this->User_model->get_effective_email($complete_order['order']['user_id']);
        
        $this->load->library('notification');
        $this->notification->send('Getmore貨單付款成功'  . '（貨單編號： ' . $order_number . '）', $html, $email);
    }    
    
    function get_by_id($id){
        return $this->db->where('id', $id)->from('sellorders')->get()->result()[0];
    }
    
}
