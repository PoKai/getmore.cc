<?php
/*
 * Suggested clothing.
 */
class Suggested_items_model extends CI_Model {

    function Suggested_items()
    {
      // Call the Model constructor
      parent::Model();
      /* load 資料庫 */
      $this->load->database();


    }

    /* pass in an array of suggested id.
     * update all the 'order' field in the table
     * 
     * @author howtomakeaturn
     */
    function updateSequences($seq_array){
        $counter = 0;
        foreach($seq_array as $suggested_id){
            $data = array(
                'order' => $counter
            );          
            $this->db->where('id', $suggested_id);
            $this->db->update('suggested_items', $data);       
            $counter ++;   
        }
    }

    // add one clothing to suggested list
    function add($product_item_number){
        $this->db->select('id')->from('product_clothing')->where('product_item_number', $product_item_number)->limit(1);
        $query = $this->db->get();
        $row = $query->result_array();
        $clothing = $row[0];
        $data = array(
            'clothing_id' => $clothing['id'],
            'order' => '-1'
        );

        $response = $this->db->insert('suggested_items', $data);
        return $response;
    }
    
    function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('suggested_items');
    }

    function get_all_clothing_ids(){
        $this->db->select('clothing_id');
        $this->db->from('suggested_items');
        $query = $this->db->get();
        $suggested_items = $query->result_array();
        $ids = array();
        foreach($suggested_items as $item){
            array_push($ids, $item['clothing_id']);
        }
        return $ids;
    }
    
    function get_num_of_all(){
        $this->db->from('suggested_items s');
        $this->db->join('product_clothing c', 's.clothing_id = c.id'); // this joins the user table to topics
        $this->db->where('c.publication_time <', date('Y-m-d H:i:s'));                                                                                  
        $this->db->where('c.on_shelf', '1');                                                                                        
        $query = $this->db->get();      
      
        return $query->num_rows();      
    }

    // get all suggested clothing.
    // support simple pagination
    function get_all($limit = 100, $offset = 0){
      $this->db->select('s.id as suggested_id, c.id, ' . 
          'c.product_title, c.files, c.product_original_price, ' . 
          'c.product_sell_price, c.id as clothing_id, ' . 
          'c.lock_status, c.reduction_again, c.size_type_major, ' . 
          'c.size, c.size_number_minor, c.size_type_minor'
      );

      $this->db->from('suggested_items s');
      $this->db->join('product_clothing c', 's.clothing_id = c.id'); // this joins the user table to topics
      $this->db->where('c.publication_time <', date('Y-m-d H:i:s'));                                                                                  
      $this->db->where('c.on_shelf', '1');                                                                                  
      $this->db->order_by("order", "asc");
      $this->db->limit($limit, $offset);
      
      $query = $this->db->get();      
      
      $row = $query->result_array();
      
      return $row;
    }    

}
?>
