<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tables['posts'] = 'posts';
        $this->tables['messages'] = 'messages';
    }

    public function get_all_posts(){
        $posts = $this->db->from($this->tables['posts'].' p')->join('users u', 'u.id = p.user_id')->join('closet l', 'l.user_id = u.id')->join('files f', 'f.file_id = l.profile_photo_file_id', 'left')->select('p.id, p.user_id, p.title, p.content, u.user_name, u.fbid, p.product_id, p.url_code, p.created_time, f.file_name')->get()->result_array();

        $posts = $this->_append_profile_pic_url($posts);

        return $this->_append_last_massage_to_posts($posts);
    }

    public function new_posts($posts){
        $user_id = $this->session->userdata('user_id');
        
        return $this->db->insert_batch($this->tables['posts'], array_map(function($post) use ($user_id){
            $post['user_id'] = $user_id;
            return $post;
        }, $posts));
    }

    public function new_post($post){
        $user_id = $this->session->userdata('user_id');

        return $this->db->insert($this->tables['posts'], array_merge($post, array(
            'user_id' => $user_id
        )));
    }

    public function new_message($message){
        $user_id = $this->session->userdata('user_id');

        return $this->db->insert($this->tables['messages'], array_merge($message, array(
            'user_id' => $user_id
        )));
    }

    public function get_posts_by_userId($user_id){
        $posts = $this->db->from($this->tables['posts'].' p')->join('users u', 'u.id = p.user_id')->join('closet l', 'l.user_id = u.id')->join('files f', 'f.file_id = l.profile_photo_file_id', 'left')->select('p.id, p.user_id, p.title, p.content, u.user_name, u.fbid, p.product_id, p.url_code, p.created_time, f.file_name')->where('p.owner_id', $user_id)->or_where('p.user_id', $user_id)->get()->result_array();
        
        $posts = $this->_append_profile_pic_url($posts);

        return $this->_append_last_massage_to_posts($posts);
    }

    public function get_posts_by_productId($product_id){
        $posts = $this->db->from($this->tables['posts'].' p')->join('users u', 'u.id = p.user_id')->join('closet l', 'l.user_id = u.id')->join('files f', 'f.file_id = l.profile_photo_file_id', 'left')->select('p.id, p.user_id,p.title, p.content, u.user_name, u.fbid, p.created_time, f.file_name')->where('p.product_id', $product_id)->get()->result_array();
        
        $posts = $this->_append_profile_pic_url($posts);

        return $this->_append_last_massage_to_posts($posts);
    }

    public function get_posts_by_urlCode($url_code){
        $posts = $this->db->from($this->tables['posts'].' p')->join('users u', 'u.id = p.user_id')->join('closet l', 'l.user_id = u.id')->join('files f', 'f.file_id = l.profile_photo_file_id', 'left')->select('p.id, p.user_id, p.title, p.content, u.user_name, u.fbid, p.created_time, f.file_name')->where('p.url_code', $url_code)->get()->result_array();
        
        $posts = $this->_append_profile_pic_url($posts);

        return $this->_append_last_massage_to_posts($posts);
    }

    public function get_messages_of_post($post_id){
        $messages = $this->db->from($this->tables['messages']." m")->join('users u', 'u.id = m.user_id')->join('closet c', 'c.user_id = u.id')->join('files f', 'f.file_id = c.profile_photo_file_id', 'left')->select('m.*, u.fbid, f.file_name')->where("m.post_id", $post_id)->order_by("m.created_time")->get()->result_array();
        return $this->_append_profile_pic_url($messages);
    }

    public function get_message_by_id($message_id){
        $messages = $this->db->from($this->tables['messages']." m")->join('users u', 'u.id = m.user_id')->join('closet c', 'c.user_id = u.id')->join('files f', 'f.file_id = c.profile_photo_file_id', 'left')->select('m.*, u.fbid, f.file_name')->where("m.id", $message_id)->order_by("m.created_time")->get()->result_array();
        $messages = $this->_append_profile_pic_url($messages);
        return $messages[0];
    }

    public function get_post_by_id($post_id){
        $posts = $this->db->from($this->tables['posts'].' p')->join('users u', 'u.id = p.user_id')->join('closet l', 'l.user_id = u.id')->join('files f', 'f.file_id = l.profile_photo_file_id', 'left')->select('p.id, p.user_id,p.title, p.content, u.user_name, u.fbid, p.product_id, p.url_code, p.created_time, f.file_name')->where('p.id', $post_id)->get()->result_array();
        $posts = $this->_append_profile_pic_url($posts);
        return $posts[0];
    }

    public function get_posts_by_ids($post_ids){
        $posts = $this->db->from($this->tables['posts'].' p')->join('users u', 'u.id = p.user_id')->join('closet l', 'l.user_id = u.id')->join('files f', 'f.file_id = l.profile_photo_file_id', 'left')->select('p.id, p.user_id,p.title, p.content, u.user_name, u.fbid, p.product_id, p.url_code, p.created_time, f.file_name')->where_in('p.id', $post_ids)->get()->result_array();
        $posts = $this->_append_profile_pic_url($posts);
        return $posts;
    }
    private function _get_last_message_of_posts($posts_id){
        $messages = $this->db->query("SELECT last_messages.*, users.fbid, files.file_name FROM (SELECT * FROM messages GROUP BY post_id DESC) last_messages JOIN users ON users.id = last_messages.user_id JOIN closet ON closet.user_id = users.id LEFT JOIN files ON files.file_id = closet.profile_photo_file_id ORDER BY last_messages.post_id")->result_array();

        return $this->_append_profile_pic_url($messages);
    }

    private function _append_profile_pic_url($arr){
        return array_map(function($sub_arr){
            if(!is_null($sub_arr['file_name'])){
                $sub_arr['profile_pic_url'] = image_url($sub_arr['file_name'], 165, 165);
            }
            else {
                $sub_arr['profile_pic_url'] = "http://graph.facebook.com/{$sub_arr['fbid']}/picture";
            }

            unset($sub_arr['file_name']);
            unset($sub_arr['fbid']);
            return $sub_arr;
        }, $arr);
    }

    private function _append_last_massage_to_posts($posts){
        $last_messages = $this->_get_last_message_of_posts(array_map(function($post){
            return $post['id'];
        }, $posts));

        return array_map(function($post) use ($last_messages){
            $post['last_message'] = $this->_multi_array_search($last_messages, array(
                'post_id'=>$post['id']
            ));

            return $post;
        }, $posts);
    }

    /**
    * Multi-array search
    *
    * @param array $array
    * @param array $search
    * @return array
    */
    private function _multi_array_search($array, $search){

        // Create the result array
        $result = array();

        // Iterate over each array element
        foreach ($array as $key => $value){

            // Iterate over each search condition
            foreach ($search as $k => $v){

                // If the array element does not meet the search condition then continue to the next element
                if (!isset($value[$k]) || $value[$k] != $v){
                    continue 2;
                }

            }

            // Add the array element's key to the result array
            $result[] = $value;
        }

        // Return the result array
        return $result;
    }
}
