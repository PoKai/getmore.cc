<?php
/*
 * An simple model for blog articles.
 * 
 */
class Article_model extends JR_Model {

    protected $return_type = 'array';
    
    function __construct(){
        parent::__construct();
        $this->load->database();
    }

}
