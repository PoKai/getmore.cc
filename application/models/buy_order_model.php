<?php
/**
 * Model for buy orders.
 *
 * @author howtomakeaturn
 **/
class Buy_order_model extends JR_Model {
	public $_table = 'buy_order';

	function Buy_order() {
		// Call the Model constructor
		parent::Model();
		/* load 資料庫 */
		$this->load->database();

		$this->load->spark('curl');
	}

	/*
	 * The refresh button in admin panel.
	 */
	function refresh_items_amount_and_price($order_id) {
		$this->load->model('Product_clothing_model');
		$clothing_array = $this->Product_clothing_model->get_many_by('buy_order_id', $order_id);
		$prices = $this->Product_clothing_model->calculate_prices($clothing_array);

		$order = $this->get($order_id);

		$new_total_price = abs($prices['total_price']) - abs($order->deduction_amount);

		$old_log = $order->logs;
		if (empty($old_log)) {
			$log = [];
		} else {
			$log = json_decode($old_log);
		}
		$message = "重新計算件數與金額：" .
		"原件數（" . $order->items_count .
		"），原商品金額（" . $order->items_price .
		"），原運費（" . $order->shipping_price .
		"），原抵扣（" . $order->deduction_amount .
		"），原總額（" . $order->total_price .
		"），新件數（" . count($clothing_array) .
		"），新商品金額（" . $prices['items_price'] .
		"），新運費（" . $prices['shipping_price'] .
		"），新抵扣（" . $order->deduction_amount .
		"），新總額（" . $new_total_price;
		/*
		$message = "重新計算件數與金額：" .
		"原件數（" . $order->items_count .
		"），原商品金額（" . $order->items_price .
		"），原運費（" . $order->shipping_price .
		"），原抵扣（" . $order->deduction_amount .
		"），原總額（" . $order->total_price .
		"新件數（" . count($clothing_array) .
		"），新商品金額（" . $prices['items_price'] .
		"），新運費（" . $prices['shipping_price'] .
		"），新抵扣（" . $order->deduction_amount .
		"），新總額（" . $prices['total_price'] + $order->deduction_amount .
		"）";
		 */
		array_push($log, $message);

		$data = array(
			'items_price' => $prices['items_price'],
			'shipping_price' => $prices['shipping_price'],
			'total_price' => $new_total_price,
			'logs' => json_encode($log),
			'items_count' => count($clothing_array),
		);

		$this->db->trans_start();

		$this->update($order_id, $data);

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			throw new Exception('errors happened in transaction. in refresh_items_amount_and_price');
		}

		return true;
	}

	/*
	 * Get the latest orders.
	 */
	public function get_many_latest_orders($many) {
		$query = $this->db->order_by('created_at', 'desc')
			->limit($many)
			->get('buy_order');
		$orders = $query->result_array();
		return $orders;
	}

	/*
	 * Physical orders are special orders which represents
	 * selling clothing in person with cash.
	 */
	function get_physical_orders() {
		$this->load->library('ion_auth');
		$admin_users = $this->ion_auth->users2(1)->result();
		$ids = array();
		foreach ($admin_users as $user) {
			array_push($ids, $user->id);
		}
		$this->db->from('buy_order')->where('status', 4)->where_in('user_id', $ids);
		$query = $this->db->get();
		$orders = $query->result_array();
		return $orders;
	}

	// Get by order number.

	function get_one_by_order_number($order_number = "", $sub = false) {
		$this->db->from('buy_order')->join('sub_order', 'buy_order.id = sub_order.buy_order_id', 'left');
		if ($sub) {
			$this->db->where('sub_order.sub_order_number', $order_number);
		} else {
			$this->db->where('buy_order.order_number', $order_number)->select('sub_order.*, buy_order.*')->group_by('buy_order.id');
		}

		$query = $this->db->get();
		$row = $query->result_array();
		$order = $row[0];

		if (count($row) != 1) {
			show_error("貨單數目不是一筆 而是" . count($row));
		} else {
			return $order;
		}

	}

	/*
	 * Get complete order including payment expiring date and other stuff.
	 */
	function get_complete_order_by_order_number($order_number = "") {
		$this->load->database();
		$this->load->model('Buy_order_model');
		$order = $this->Buy_order_model->get_one_by_order_number($order_number);

		// extends the data with some 'label' data for rendering
		// receipt
		if ($order['receipt_to'] == '1') {
			$order['receipt_to_label'] = "寄給訂購人";
		} else if ($order['receipt_to'] == '2') {
			$order['receipt_to_label'] = "寄給收件人";
		} else if ($order['receipt_to'] == '3') {
			$order['receipt_to_label'] = "捐贈創世基金會";
		}

		$this->load->helper('buyorder_helper');
		$order['payment_method_label'] = payment_method_label($order['payment_method']);

		// no longer handle this here.
		/*
		if ($order['payment_method'] == PAYMENT_CREDIT_CARD)
		$order['payment_method_label'] = "信用卡";
		else if ($order['payment_method'] == 'deduction')
		$order['payment_method_label'] = "全額折抵";
		else if ($order['payment_method'] == 'pay-at-home')
		$order['payment_method_label'] = "貨到付款";
		else if ($order['payment_method'] == 'atm')
		$order['payment_method_label'] = "ATM轉帳";
		 */

		$this->load->model('Product_clothing_model');
		$clothing_array = $this->Product_clothing_model->get_some_by_order_id($order['id']);
		$prices = $this->Product_clothing_model->calculate_prices($clothing_array);

		$exp_date = $this->calculate_expiration_time($order['created_at']);
		$order['expired_at'] = $exp_date->format('Y-m-d H:i:s');

		// because crontab just change status on the minute
		// (so the actual expiration time would be N to N+1 minutes),
		// set one variable to help tasks like hidding pay button from user
		$order['payable'] = false;
		// if ( $exp_date > new DateTime() ){
		if (($exp_date > new DateTime()) &&
			($order['status'] == 1) &&
			($order['payment_method'] == 'credit-cart')) {
			$order['payable'] = true;
		}

		// send to view
		$data['order'] = $order;
		$data['clothing_array'] = $clothing_array;
		$data['prices'] = $prices;

		return $data;
	}

	/*
	 * Get all orders related to certain user.
	 */
	function get_some_by_user_id($user_id = "") {
		$this->load->config('checkout');

		// only retrieve paid orders. in future, also retrieve unpaid orders
		$this->db->order_by('created_at', 'desc')->from('buy_order')->where('user_id', $user_id)->where_in('status', [6, -1, 1, 1.5, 2, 3, 4, 5]);
		$query = $this->db->get();
		$orders = $query->result_array();

		// calculate the expiration time
		foreach ($orders as &$order) {
			$order['payable'] = false;
			// because crontab just change status on the minute
			// (so the actual expiration time would be N to N+1 minutes),
			// set one variable to help tasks like hidding pay button from user
			if (($order['status'] == 1) && ($order['payment_method'] == 'credit-cart')) {
				$exp_date = $this->calculate_expiration_time($order['created_at']);
				$order['expired_at'] = $exp_date->format('Y/m/d H:i:s');
				if ($exp_date > new DateTime()) {
					$order['payable'] = true;
				}
			} // end if credit-cart

			if (($order['status'] == 1) && ($order['payment_method'] == 'atm')) {
				$exp_date = $this->atm_calculate_expiration_time($order['created_at']);
				$order['expired_at'] = $exp_date->format('Y/m/d H:00:00');
				if ($order['expired_at'] > date('Y/m/d H:i:s')) {
					$order['payable'] = true;
				}
			} // end if atm

			if (($order['status'] == 1) && ($order['payment_method'] == 'line_pay')) {
				$exp_date = $this->atm_calculate_expiration_time($order['created_at']);
				$order['expired_at'] = $exp_date->format('Y/m/d H:00:00');
				if ($order['expired_at'] > date('Y/m/d H:i:s')) {
					$order['payable'] = true;
				}
			} // end if line pay
		} // end foreach
		return $orders;
	}

	function get_all() {
		// only retrieve paid orders. in future, also retrieve unpaid orders
		// $this->db->from('buy_order')->where('status', '2');
		$this->db->from('buy_order');
		$query = $this->db->get();
		$orders = $query->result_array();
		return $orders;
	}

	/*
	 * Create one buy order. Including several validations!
	 */
	function create_one($order, $ids, $user_id, $payment_method,
		$deduction_amount = 0, $coupon_code = '') {

		$this->load->model('Product_clothing_model');
		$clothing_array = $this->Product_clothing_model->get_some($ids, true);

		$this->load->helper('Product_clothing_helper');

		$closets = sort_out_by_closet($clothing_array);

		$prices = calculate_prices($closets);

		$this->load->model('Account_model', 'account');
		$account = $this->account->get($user_id);

		if (($account->balance > 0) && ($account->balance + $deduction_amount < 0)) {
			throw new Exception("no enought money in account");
		}

		if (!is_numeric($deduction_amount) || !is_integer($deduction_amount)) {
			throw new Exception("invalid 'deduction_amount': $deduction_amount. it's not integer.");
		}

		if ($deduction_amount > 0) {
			throw new Exception("invalid deduction amount: $deduction_amount. it can't be position");
		}

		if ($prices['total_price'] + $deduction_amount < 0) {
			throw new Exception("invalid total price by this deduction: $deduction_amount. it makes total price negative");
		}

		$query = $this->db->where('id', $user_id)->get('users');
		if ($query->num_rows() == 0) {
			throw new Exception("invalid user id: $user_id. the user doesn't exist");
		}

		$acceptable_payment_method = array('credit-cart', 'pseudo_payment_method', 'deduction', 'pay-at-home', 'atm');
		if (!in_array($payment_method, $acceptable_payment_method)) {
			throw new Exception("invalid payment_method: $payment_method .");
		}

		// user try to use one coupon
		if (!empty($coupon_code)) {
			$this->load->model('Coupon_model', 'coupon');
			$coupon = $this->coupon->get_by('code', $coupon_code);
			if (empty($coupon)) {
				throw new Exception('invalid coupon code');
			}
			$coupon_amount = (-1) * $coupon->amount;
			if ($prices['total_price'] + $deduction_amount + $coupon_amount < 0) {
				throw new Exception("invalid final price by this coupon_amount. it makes final price negative");
			}
			if (($prices['total_price'] + $deduction_amount + $coupon_amount == 0) && ($payment_method != 'deduction')) {
				throw new Exception("final price is zero with invalid payment_method: $payment_method .");
			}

			if (($payment_method == 'deduction') && ($prices['total_price'] + $deduction_amount + $coupon_amount != 0)) {
				throw new Exception("payment method is deduction, but final_price is not zero: " . $prices['total_price'] + $deduction_amount + $coupon_amount);
			}

			if ($prices['items_price'] < $coupon->minimum_charge) {
				throw new Exception('not satisfy minimum charge');
			}

		}
		// user didn't try to use coupons
		else {
			if (($prices['total_price'] + $deduction_amount == 0) && ($payment_method != 'deduction')) {
				throw new Exception("final price is zero with invalid payment_method: $payment_method .");
			}

			if (($payment_method == 'deduction') && ($prices['total_price'] + $deduction_amount != 0)) {
				throw new Exception("payment method is deduction, but final_price is not zero: " . $prices['total_price'] + $deduction_amount);
			}

			$coupon_amount = 0;
		}

		$data = array(
			'status' => '1',
			'buyer_name' => $order->buyer->name,
			'buyer_gender' => $order->buyer->gender,
			'buyer_phone_number' => $order->buyer->contact_phone_number,
			'buyer_address' => $order->buyer->location->zipcode . $order->buyer->location->county . $order->buyer->location->district . $order->buyer->location->address,
			'receiver_name' => $order->receiver->name,
			'receiver_gender' => $order->receiver->gender,
			'receiver_phone_number' => $order->receiver->contact_phone_number,
			'receiver_address' => $order->receiver->location->zipcode . $order->receiver->location->county . $order->receiver->location->district . $order->receiver->location->address,
			'delivery_time' => $order->delivery_time,
			'receipt_to' => $order->receipt_to,
			'uniform_invoice_type' => $order->uniform_invoice_type,
			'uniform_invoice_title' => $order->uniform_invoice_title,
			'uniform_invoice_number' => $order->uniform_invoice_number,
			'items_price' => $prices['items_price'],
			'shipping_price' => $prices['shipping_price'],
			'deduction_amount' => $deduction_amount,
			'coupon_amount' => $coupon_amount,
			'total_price' => $prices['total_price'] + $deduction_amount + $coupon_amount,
			'clothing_ids' => json_encode($ids),
			'user_id' => $user_id,
			'payment_method' => $payment_method,
			'coupon_code' => $coupon_code,
			'clothing_ids' => json_encode($ids),
			'receipt_to' => $order->receipt_to,
			'uniform_invoice_type' => $order->uniform_invoice_type,
			'uniform_invoice_title' => $order->uniform_invoice_title,
			'uniform_invoice_number' => $order->uniform_invoice_number,
		);

		$this->db->trans_start();

		/*created order*/
		$order = (array) json_decode($this->curl->simple_post('/restful/buy_order/', $data));

		if ($deduction_amount != 0) {
			$this->load->model('Transaction_model', 'transaction');
			$this->transaction->insert_to_user($user_id, "消費抵扣", $order['order_number'], $deduction_amount, '');
		}
		if ($coupon_amount != 0) {
			$this->coupon->consume($coupon_code);
		}

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			show_error('errors happened in transaction. in create_one');
		}

		if ($payment_method == 'pay-at-home') {
			$this->notify_the_pay_at_home_buyorder_is_created($order['order_number']);
		}

		if ($payment_method == 'atm') {
			$this->notify_the_atm_buyorder_is_created($order['order_number']);
		}

		return $order;
	}

	/*
	 * User paid, so change the order status.
	 */
	function switch_one_to_paid_status_by_order_number($order_number = "") {
		$this->db->from('buy_order')->where('order_number', $order_number);
		$query = $this->db->get();
		$row = $query->result_array();
		$order = $row[0];

		if (count($row) != 1) {
			show_error("貨單數目不是一筆");
		} else {
			$data = array(
				'status' => '2',
				'paid_at' => date("Y-m-d H:i:s"),
			);

			if ($order['payment_method'] == 'pay-at-home') {
				$data['paid_at'] = '';
			}

			$this->db->trans_start();

			$this->db->where('order_number', $order_number);
			$this->db->update('buy_order', array('paid_at' => $data['paid_at']));

			/*update sub order*/
			$this->db->update('sub_order', array('status' => $data['status']), array('buy_order_id' => $order['id']));

			/*
			 * need to update all the clothing
			 * change their lock status
			 */
			$this->load->model('Product_clothing_model');

			$this->db->select('id')->from('product_clothing')->where('buy_order_id', $order['id']);
			$query = $this->db->get();
			$rows = $query->result_array();

			$ids = array();
			foreach ($rows as $row) {
				array_push($ids, $row['id']);
			}
			$order_id = $this->get_one_by_order_number($order_number)['id'];

			// only lock clothing with credit-cart..
			// implement this and related codes in the future
			// if ($order['payment_method']=='credit-cart'){
			$this->Product_clothing_model->switch_some_to_paid_status($ids);
			// }

			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE) {
				show_error('errors happened in transaction. in switch_one_to_paid_status_by_order_number');
			}

			// send email to user to notify that the system received the money successfully
			$this->notify_the_buyorder_is_paid($order_number);
		}
	}

	/*
	 * Switch one order to done status.
	 */

	function switch_one_to_done_status_by_order_number($order_number = "") {
		$this->db->from('buy_order')->where('order_number', $order_number);
		$query = $this->db->get();
		$row = $query->result_array();
		$order = $row[0];
		if (count($row) != 1) {
			show_error("貨單數目不是一筆");
		} else {
			$data = array(
				'status' => '5',
			);

			// done order doesn't neeed to checkout again
			if ($order['status'] == '5') {
				return;
			}

			$this->db->trans_start();

			$this->db->where('order_number', $order_number);
			$this->db->update('buy_order', $data);

			$this->load->model('User_model');
			/*
			 *  increase buy count of user
			 */
			$this->User_model->increase_buy_count($order['user_id']);
			/*
			 * need to update all the clothing
			 * change their lock status
			 */
			$this->load->model('Product_clothing_model');

			$this->db->select('id')->from('product_clothing')->where('buy_order_id', $order['id']);

			$query = $this->db->get();
			$rows = $query->result_array();

			$ids = array();
			foreach ($rows as $row) {
				array_push($ids, $row['id']);
			}
			$order_id = $this->get_one_by_order_number($order_number)['id'];

			// allow empty orders to be done.
			// which is valid. (customer may reject the items)
			if (count($ids) !== 0) {
				$this->Product_clothing_model->checkout_some($ids);
			}

			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE) {
				show_error('errors happened in transaction. in switch_one_to_done_status_by_order_number');
			}
		}
	}

	/*
	 * Reset the order expiration time.
	 */
	function reset_expiration_time($id = "") {
		$this->db->trans_start();

		$this->db->from('buy_order')->where('id', $id);
		$query = $this->db->get();
		$row = $query->result_array();
		$order = $row[0];

		if (count($row) != 1) {
			throw new Exception("doesnt get exactly one order");
			return false;
		}
		if ($order['status'] != 1) {
			throw new Exception("the order is not at status 1 (unpaid satus)");
			return false;
		}

		// reset the 'created_at' for resetting expiration time
		$data = array(
			'created_at' => date("Y-m-d H:i:s"),
		);

		$this->db->where('id', $id);
		$this->db->update('buy_order', $data);

		// reset related clothing 'last_locked_time'
		$this->db->select('*')->from('product_clothing')->where('buy_order_id', $id);
		$query = $this->db->get();
		$rows = $query->result_array();

		$ids = array();

		foreach ($rows as $clothing) {
			if ($clothing['lock_status'] != '1') {
				throw new Exception('some of the clothing is not in lock_status 1');
				return false;
			}
			array_push($ids, $clothing['id']);
		}

		$data = array(
			'last_locked_time' => date("Y-m-d H:i:s"),
		);
		$this->db->where_in('id', $ids);
		$this->db->update('product_clothing', $data);

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			throw new Exception('errors happened in transaction.');
		}
	}

	function update_one_by_order_number($order_number = "", $data = []) {
		$this->db->from('buy_order')->where('order_number', $order_number);
		$query = $this->db->get();
		$row = $query->result_array();
		$order = $row[0];

		if (count($row) != 1) {
			throw new Exception("doesnt get exactly one order");
		} else {
			$this->db->where('order_number', $order_number);
			$this->db->update('buy_order', $data);
		}
	}

	/* find out unpaid orders which actually are expired,
	 * turn them into expired status
	 * @author howtomakeaturn
	 */
	function handle_expired_buyorders_with_credit_card() {
		$this->db->select('created_at, id')->from('buy_order')->where_in('status', [1, 2])->where(array('payment_method' => 'credit-cart', 'paid_at' => '0000-00-00 00:00:00'));
		$query = $this->db->get();
		$orders = $query->result_array();
		$expired_order_ids = array();
		$expired_product_item_numbers = array();
		$expired_designer_products = array();

		foreach ($orders as $order) {
			$exp_date = $this->calculate_expiration_time($order['created_at']);
			$now_date = new DateTime();
			// if it's expired already, its status shouldn't be 1,
			// that's why we update it
			$product_item_numbers = array();
			if ($exp_date < $now_date) {
				array_push($expired_order_ids, $order['id']);

				// collect clothings needed to be released

				$sub_orders = $this->db->where('buy_order_id', $order['id'])->get('sub_order')->result();

				$product_item_numbers = array_reduce($sub_orders, function ($result, $sub_order) use (&$expired_designer_products) {
					$products = json_decode($sub_order->products);
					return array_merge($result, array_map(function ($product) use (&$expired_designer_products) {
						if (preg_match("/^D/", $product->product_item_number)) {
							array_push($expired_designer_products, $product);
						}
						return $product->product_item_number;
					}, $products));
				}, []);
			}
			$expired_product_item_numbers = array_merge($expired_product_item_numbers, $product_item_numbers);
		}

		// i dont want to see mysql tell me i shouldn't provide
		// empty id fields so i handle this manually
		if (count($expired_order_ids) == 0) {
			return $expired_order_ids;
		}

		// send emails to users
		foreach ($expired_order_ids as $id) {
			$this->notify_the_credit_card_buyorder_expired($id);
		}

		$this->db->trans_start();

		/*release sub order*/

		$data = array(
			'status' => '-1',
		);
		$this->db->where_in('id', $expired_order_ids);
		$this->db->update('buy_order', $data);

		$this->db->where_in('buy_order_id', $expired_order_ids);
		$this->db->update('sub_order', $data);

		// return the transactions
		$this->refund_orders($expired_order_ids);

		// release the clothings
		$data = array(
			'lock_status' => '0',
			'buy_order_id' => '',
			'last_locked_time' => '',
			'sub_order_id' => '0',
		);

		$product_clothings = $this->db->where_in('product_item_number', $expired_product_item_numbers)->get('product_clothing')->result();

		$product_clothing_ids = array_map(function ($product_clothing) {
			return $product_clothing->id;
		}, $product_clothings);

		if (!empty($product_clothing_ids)) {
			$this->db->where_in('id', $product_clothing_ids);
			$this->db->update('product_clothing', $data);

			$this->db->where_in('product_id', $product_clothing_ids);
			$this->db->where('type', 2);
			$this->db->set('number', 'number+1', FALSE);
			$this->db->update('stocks');
		}

		$marketplace_clothings = $this->db->where_in('product_item_number', $expired_product_item_numbers)->get('marketplace')->result();

		$marketplace_clothing_ids = array_map(function ($marketplace_clothing) {
			return $marketplace_clothing->id;
		}, $marketplace_clothings);

		if (!empty($marketplace_clothing_ids)) {
			$this->db->where_in('id', $marketplace_clothing_ids);
			$this->db->update('marketplace', $data);

			$this->db->where_in('product_id', $marketplace_clothing_ids);
			$this->db->where('type', 1);
			$this->db->set('number', 'number+1', FALSE);
			$this->db->update('stocks');
		}

		$designer_clothings = $this->db->where_in('product_item_number', $expired_product_item_numbers)->get('designer_products')->result();

		$designer_clothing_ids = array_map(function ($designer_clothing) {
			return $designer_clothing->id;
		}, $designer_clothings);

		if (!empty($designer_clothing_ids)) {
			$this->db->where_in('id', $designer_clothing_ids);
			$this->db->update('designer_products', $data);

			foreach ($expired_designer_products as $index => $product) {
				$this->db->where(array(
					'product_id' => $product->id,
					'color' => $product->color,
					'size' => $product->size,
					'type' => 3,
				));
				$this->db->set('number', 'number+' . $product->number, FALSE);
				$this->db->update('stocks');
			}
		}

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			throw new Exception('transaction failed');
		}

		return $expired_order_ids;
	}

	/*
	 * Handle expired orders in atm way.
	 */
	function handle_expired_buyorders_with_atm() {
		$this->db->select('created_at, id')->from('buy_order')->where_in('status', [1, 2])->where(array('payment_method' => 'atm', 'paid_at' => '0000-00-00 00:00:00'));
		$query = $this->db->get();
		$orders = $query->result_array();
		$expired_order_ids = array();
		$expired_product_item_numbers = array();
		$expired_designer_products = array();

		foreach ($orders as $order) {
			$exp_date = $this->atm_calculate_expiration_time($order['created_at'])->format('Y-m-d H:00:00');
			$now_date = date('Y-m-d H:i:s');
			// if it's expired already, its status shouldn't be 1,
			// that's why we update it
			$product_item_numbers = array();
			if ($exp_date < $now_date) {
				array_push($expired_order_ids, $order['id']);

				// collect clothings needed to be released
				$sub_orders = $this->db->where('buy_order_id', $order['id'])->get('sub_order')->result();

				$product_item_numbers = array_reduce($sub_orders, function ($result, $sub_order) use (&$expired_designer_products) {
					$products = json_decode($sub_order->products);
					return array_merge($result, array_map(function ($product) use (&$expired_designer_products) {
						if (preg_match("/^D/", $product->product_item_number)) {
							array_push($expired_designer_products, $product);
						}
						return $product->product_item_number;
					}, $products));
				}, []);
			}
			$expired_product_item_numbers = array_merge($expired_product_item_numbers, $product_item_numbers);
		}

		// i dont want to see mysql tell me i shouldn't provide
		// empty id fields so i handle this manually
		if (count($expired_order_ids) == 0) {
			return $expired_order_ids;
		}

		// send emails to users
		foreach ($expired_order_ids as $id) {
			$this->notify_the_atm_buyorder_expired($id);
		}

		$this->db->trans_start();

		/*release sub order*/
		$data = array(
			'status' => '-1',
		);

		$this->db->where_in('id', $expired_order_ids);
		$this->db->update('buy_order', $data);

		$this->db->where_in('buy_order_id', $expired_order_ids);
		$this->db->update('sub_order', $data);

		// return the transactions
		$this->refund_orders($expired_order_ids);

		// release the clothings
		$data = array(
			'lock_status' => '0',
			'buy_order_id' => '',
			'last_locked_time' => '',
			'sub_order_id' => '0',
		);

		$product_clothings = $this->db->where_in('product_item_number', $expired_product_item_numbers)->get('product_clothing')->result();

		$product_clothing_ids = array_map(function ($product_clothing) {
			return $product_clothing->id;
		}, $product_clothings);

		if (!empty($product_clothing_ids)) {
			$this->db->where_in('id', $product_clothing_ids);
			$this->db->update('product_clothing', $data);

			$this->db->where_in('product_id', $product_clothing_ids);
			$this->db->where('type', 2);
			$this->db->set('number', 'number+1', FALSE);
			$this->db->update('stocks');
		}

		$marketplace_clothings = $this->db->where_in('product_item_number', $expired_product_item_numbers)->get('marketplace')->result();

		$marketplace_clothing_ids = array_map(function ($marketplace_clothing) {
			return $marketplace_clothing->id;
		}, $marketplace_clothings);

		if (!empty($marketplace_clothing_ids)) {
			$this->db->where_in('id', $marketplace_clothing_ids);
			$this->db->update('marketplace', $data);

			$this->db->where_in('product_id', $marketplace_clothing_ids);
			$this->db->where('type', 1);
			$this->db->set('number', 'number+1', FALSE);
			$this->db->update('stocks');
		}

		$designer_clothings = $this->db->where_in('product_item_number', $expired_product_item_numbers)->get('designer_products')->result();

		$designer_clothing_ids = array_map(function ($designer_clothing) {
			return $designer_clothing->id;
		}, $designer_clothings);

		if (!empty($designer_clothing_ids)) {
			$this->db->where_in('id', $designer_clothing_ids);
			$this->db->update('designer_products', $data);

			foreach ($expired_designer_products as $index => $product) {
				$this->db->where(array(
					'product_id' => $product->id,
					'color' => $product->color,
					'size' => $product->size,
					'type' => 3,
				));
				$this->db->set('number', 'number+' . $product->number, FALSE);
				$this->db->update('stocks');
			}
		}

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			throw new Exception('transaction failed');
		}

		return $expired_order_ids;
	}

      /*
         * Handle expired orders in line pay way.
         */
        function handle_expired_buyorders_with_line_pay() {
          $this->db->select('created_at, id')->from('buy_order')->where('status', 1)->where(array('payment_method' => 'line_pay', 'paid_at' => '0000-00-00 00:00:00'));
          $query = $this->db->get();
          $orders = $query->result_array();
          $expired_order_ids = array();
          $expired_product_item_numbers = array();
          $expired_designer_products = array();

          foreach ($orders as $order) {
            $exp_date = $this->line_pay_calculate_expiration_time($order['created_at'])->format('Y-m-d H:i:s');
            $now_date = date('Y-m-d H:i:s');
            // if it's expired already, its status shouldn't be 1,
            // that's why we update it
            $product_item_numbers = array();
            if ($exp_date < $now_date) {
              array_push($expired_order_ids, $order['id']);

              // collect clothings needed to be released
              $sub_orders = $this->db->where('buy_order_id', $order['id'])->get('sub_order')->result();

              $product_item_numbers = array_reduce($sub_orders, function ($result, $sub_order) use (&$expired_designer_products) {
                $products = json_decode($sub_order->products);
                return array_merge($result, array_map(function ($product) use (&$expired_designer_products) {
                  if (preg_match("/^D/", $product->product_item_number)) {
                    array_push($expired_designer_products, $product);
                  }
                  return $product->product_item_number;
                }, $products));
              }, []);
            }
            $expired_product_item_numbers = array_merge($expired_product_item_numbers, $product_item_numbers);
          }

          // i dont want to see mysql tell me i shouldn't provide
          // empty id fields so i handle this manually
          if (count($expired_order_ids) == 0) {
            return $expired_order_ids;
          }

          // send emails to users
          foreach ($expired_order_ids as $id) {
            $this->notify_the_atm_buyorder_expired($id);
          }

          $this->db->trans_start();

          /*release sub order*/
          $data = array(
            'status' => '-1',
          );

          $this->db->where_in('id', $expired_order_ids);
          $this->db->update('buy_order', $data);

          $this->db->where_in('buy_order_id', $expired_order_ids);
          $this->db->update('sub_order', $data);

          // return the transactions
          $this->refund_orders($expired_order_ids);

          // release the clothings
          $data = array(
            'lock_status' => '0',
            'buy_order_id' => '',
            'last_locked_time' => '',
            'sub_order_id' => '0',
          );

          $product_clothings = $this->db->where_in('product_item_number', $expired_product_item_numbers)->get('product_clothing')->result();

          $product_clothing_ids = array_map(function ($product_clothing) {
            return $product_clothing->id;
          }, $product_clothings);

          if (!empty($product_clothing_ids)) {
            $this->db->where_in('id', $product_clothing_ids);
            $this->db->update('product_clothing', $data);

            $this->db->where_in('product_id', $product_clothing_ids);
            $this->db->where('type', 2);
            $this->db->set('number', 'number+1', FALSE);
            $this->db->update('stocks');
          }

          $marketplace_clothings = $this->db->where_in('product_item_number', $expired_product_item_numbers)->get('marketplace')->result();

          $marketplace_clothing_ids = array_map(function ($marketplace_clothing) {
            return $marketplace_clothing->id;
          }, $marketplace_clothings);

          if (!empty($marketplace_clothing_ids)) {
            $this->db->where_in('id', $marketplace_clothing_ids);
            $this->db->update('marketplace', $data);

            $this->db->where_in('product_id', $marketplace_clothing_ids);
            $this->db->where('type', 1);
            $this->db->set('number', 'number+1', FALSE);
            $this->db->update('stocks');
          }

          $designer_clothings = $this->db->where_in('product_item_number', $expired_product_item_numbers)->get('designer_products')->result();

          $designer_clothing_ids = array_map(function ($designer_clothing) {
            return $designer_clothing->id;
          }, $designer_clothings);

          if (!empty($designer_clothing_ids)) {
            $this->db->where_in('id', $designer_clothing_ids);
            $this->db->update('designer_products', $data);

            foreach ($expired_designer_products as $index => $product) {
              $this->db->where(array(
                'product_id' => $product->id,
                'color' => $product->color,
                'size' => $product->size,
                'type' => 3,
              ));
              $this->db->set('number', 'number+' . $product->number, FALSE);
              $this->db->update('stocks');
            }
          }

          $this->db->trans_complete();
          if ($this->db->trans_status() === FALSE) {
            throw new Exception('transaction failed');
          }

          return $expired_order_ids;
        }

	/*
	 * Calculate the expiration time.
	 * @author howtomakeaturn
	 * @param string $created_at
	 * @return date object
	 */
	private function calculate_expiration_time($created_at) {
		$this->load->config('checkout');
		$active_time = $this->config->item('active_time');
		$exp_date = new DateTime($created_at);
		$exp_date->modify("+" . $active_time . " minutes");
		return $exp_date;
	}

	// Calculate expiration time for orders in atm way.
	private function atm_calculate_expiration_time($created_at) {
		$this->load->config('checkout');
		$atm_active_time = $this->config->item('atm_active_time');
		$exp_date = new DateTime($created_at);
		$exp_date->modify("+" . $atm_active_time . " hours");
		return $exp_date;
	}

      // Calculate expiration time for orders in atm way.
      private function line_pay_calculate_expiration_time($created_at) {
        $this->load->config('checkout');
        $line_pay_active_time = $this->config->item('line_pay_active_time');
        $exp_date = new DateTime($created_at);
        $exp_date->modify("+" . $line_pay_active_time . " minutes");
        return $exp_date;
      }

	/**
	 * @author popchieh, shipped/edited by howtomakeaturn
	 */
	public function generate_order_number() {
		$this->load->helper('date');
		$datestring = "%Y%m%d";
		$time = time();

		$order_num = "B";
		// $order_num = "T";

		if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
			$order_num = "T";
		}
		if (site_url() == 'http://beta.getmore.cc/') {
			$order_num = "T";
		}

		$time_str = mdate($datestring, $time);

		$result = $this->db->select("DATE_FORMAT(created_at, '%Y%m%d') as created_day", false)
			->from('buy_order')
			->get()
			->result_array();

		$sn_today = 1;

		foreach ($result as $row) {
			if ($row['created_day'] == $time_str) {
				$sn_today += 1;
			}

		}

//        $sn_today = count($result) + 1;
		$sn_today = str_pad($sn_today, 4, '0', STR_PAD_LEFT);

		$time_str = substr($time_str, 2);
		$order_num .= $time_str . $sn_today;

		return $order_num;
	}

	/*
	 * Return the money to users.
	 */
	private function refund_orders($expired_order_ids) {
		$this->load->model('Transaction_model', 'transaction');
		foreach ($expired_order_ids as $id) {
			$order = $this->get($id);
			$order_number = $order->order_number;
			$deduction_amount = abs($order->deduction_amount);
			$user_id = $order->user_id;
			if ($deduction_amount != 0) {
				$this->transaction->insert_to_user($user_id, "過期訂單抵扣額退還", $order_number, $deduction_amount, '');
			}
		}
	}

	// send emails
	public function notify_the_credit_card_buyorder_expired($order_id) {
		$order = $this->get($order_id);
		$complete_order = $this->get_complete_order_by_order_number($order->order_number);
		//$html = $this->load->view('email/notify_the_credit_card_buyorder_expired', ['order' => $complete_order['order'], 'clothings' => $complete_order['clothing_array']], true);

		$this->load->spark('codeigniter-template/1.0.0');
		$this->template->set_layout('template/email');
		$html = $this->template->render('email/notify_the_credit_card_buyorder_expired', ['order' => $complete_order['order'], 'clothings' => $complete_order['clothing_array']], true);

		//$email = $this->db->get_where('users', array('id' => $order->user_id))->result()[0]->email;
		$this->load->model('User_model');
		$email = $this->User_model->get_effective_email($order->user_id);

		$this->load->library('notification');
		$this->notification->send('Getmore訂單過期通知信' . '（訂單編號： ' . $order->order_number . '）', $html, $email);
	}

	// send emails
	public function notify_the_atm_buyorder_expired($order_id) {
		$order = $this->get($order_id);
		$complete_order = $this->get_complete_order_by_order_number($order->order_number);
		// $html = $this->load->view('email/notify_the_atm_buyorder_expired', ['order' => $complete_order['order'], 'clothings' => $complete_order['clothing_array']], true);

		$this->load->spark('codeigniter-template/1.0.0');
		$this->template->set_layout('template/email');
		$html = $this->template->render('email/notify_the_atm_buyorder_expired', ['order' => $complete_order['order'], 'clothings' => $complete_order['clothing_array']], true);

		//$email = $this->db->get_where('users', array('id' => $order->user_id))->result()[0]->email;
		$this->load->model('User_model');
		$email = $this->User_model->get_effective_email($order->user_id);

		$this->load->library('notification');
		$this->notification->send('Getmore訂單過期通知信' . '（訂單編號： ' . $order->order_number . '）', $html, $email);
	}

	// send emails
	public function notify_the_buyorder_is_paid($order_number) {
		$complete_order = $this->get_complete_order_by_order_number($order_number);
		/*
		$html = $this->load->view('email/notify_the_buyorder_is_paid',
		['order' => $complete_order['order'],
		'clothings' => $complete_order['clothing_array']
		], true);
		 */

		$this->load->spark('codeigniter-template/1.0.0');
		$this->template->set_layout('template/email');
		$html = $this->template->render('email/notify_the_buyorder_is_paid',
			['order' => $complete_order['order'],
				'clothings' => $complete_order['clothing_array'],
			], true);

		//$email = $this->db->get_where('users', array('id' => $complete_order['order']['user_id']))->result()[0]->email;
		$this->load->model('User_model');
		$email = $this->User_model->get_effective_email($complete_order['order']['user_id']);

		$this->load->library('notification');
		$this->notification->send('Getmore訂單付款成功' . '（訂單編號： ' . $order_number . '）', $html, $email);
	}

	public function notify_the_buyorder_is_created($order) {
		$this->load->spark('codeigniter-template/1.0.0');
		$this->template->set_layout('template/email');

		$this->load->model('User_model');
		$email = $this->User_model->get_effective_email($order->user_id);

		$this->load->library('notification');

		if ($order->status == 2) {
			foreach ($order->SubOrders as $sub_order) {
				if ($sub_order->sellorder_id) {
					$html = $this->template->render('email/notify_the_marketplace_seller', array_merge((array) $sub_order, array(
						'buyer_name' => $order->buyer_name,
						'buyer_phone_number' => $order->buyer_phone_number,
						'receiver_name' => $order->receiver_name,
						'receiver_phone_number' => $order->receiver_phone_number,
						'receiver_address' => $order->receiver_address,
						'payment_method' => $order->payment_method,
					)), true);
					$this->notification->send('Getmore通知：您收到新訂單囉（' . $sub_order->sub_order_number . '）！- 已經完成付款，等待出貨', $html, $sub_order->User->active_email);
				}
			}
		}

		switch ($order->payment_method) {
		case 'credit-cart':
			$html = $this->template->render('email/notify_the_credit_cart_buyorder_is_created', (array) $order, true);
			$this->notification->send('Getmore通知：您的訂單已經成立囉！（訂單編號： ' . $order->order_number . '）-以信用卡付款成功', $html, $email);
			break;
		case 'atm':
			$html = $this->template->render('email/notify_the_atm_buyorder_is_created', (array) $order, true);
			$this->notification->send('Getmore二次時尚ATM訂單成立' . '（訂單編號： ' . $order->order_number . '）', $html, $email);
			$this->load->library('system');
			$this->system->send_sms_msg($order->buyer_phone_number, '感謝您在GetMore二次時尚的訂購，您的ATM訂單已經成立！貨品已幫您保留，記得在限定時間內匯款喔！匯款資訊請至會員中心查詢。');
			break;
		case 'pay-at-home':
			$html = $this->template->render('email/notify_the_pay_at_home_buyorder_is_created', (array) $order, true);
			$this->notification->send('Getmore貨到付款訂單成立' . '（訂單編號： ' . $order->order_number . '）', $html, $email);
			break;
		}
	}

	// 將訂單轉成已過期
	function reject($order_number) {
		$order = $this->db->where('order_number', $order_number)
			->from('buy_order')
			->get()
			->row_array();

		$query = $this->db->where('buy_order_id', $order['id'])
			->from('product_clothing')
			->get();

		if ($query->num_rows()) {
			throw new Exception('can not reject non-empty buyorder.');
			return false;
		}

		#   $this->db->where('order_number', $order_number)
		#           ->update('buy_order', array('status'=>'-2'));

		return true;
	}

	// send emails
	public function notify_atm_buyorder_almost_due($order_id) {
		$order = $this->db->from('buy_order')->where('id', $order_id)->get()->row_array();
		$this->load->library('system');
		$this->system->send_sms_msg($order['buyer_phone_number'],
			'GetMore系統提醒：親愛的會員，您的ATM訂單尚未付款，快把握最後機會享受時尚！轉帳資訊請至會員中心查詢。如已轉帳請勿理會本通知。');
	}

	// send SMS
	public function handle_almost_due_atm_buyorder() {
		$orders = $this->db->from('buy_order')
			->where(array(
				'payment_method' => 'atm',
				'status' => '1',
			))
			->select('created_at, id')
			->get()->result_array();
		$counter = 0;

		$this->load->helper('checkout_helper');
		foreach ($orders as $order) {
			$duedate = get_atm_duedate($order['created_at']);
			if ($duedate <= new DateTime()) {
				$this->notify_atm_buyorder_almost_due($order['id']);
				$counter = $counter + 1;
			}
		}

		return $counter;
	}

}
?>
