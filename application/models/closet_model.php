<?php
    /*
     * My closet.
     * 
     * @author howtomakeaturn
     */
class Closet_model extends CI_Model {
    function Closet()
    {
        // Call the Model constructor
        parent::Model();
        /* load 資料庫 */
        $this->load->database();
        $this->load->spark('curl');
    }
    
    public function get_user_id_by_url_code($url_code){
        return $this->db->from('closet')->select('user_id')->where('url_code', $url_code)->get()->row_array()['user_id'];
    }

    public function get_closet_settings(){
        return json_decode($this->curl->simple_get('/restful/closet', array(), array(
            CURLOPT_COOKIE => http_build_query($_COOKIE,'',';')
        )));
    }

    public function increase_view_count($url_code){
        return $this->db->set('view_count', 'view_count+1', FALSE)->where('url_code', $url_code)->update('closet');
    }

    public function dis_follow($user_id){
        $current_user_id = $this->session->userdata('user_id');
        if($user_id === $current_user_id) return false;
        try{
            $this->db->delete('follow_users', array(
                'follower' => $current_user_id,
                'followed' => $user_id
            ));
            return true;
        }
        catch(Exception $e){
            return true;
        }
    }

    public function to_follow($user_id, $type='users'){
        $current_user_id = $this->session->userdata('user_id');
        if($user_id === $current_user_id) return false;
        try{
            $this->db->insert('follow_'.$type, array(
                'follower' => $current_user_id,
                'followed' => $user_id
            ));
            return true;
        }
        catch(Exception $e){
            return true;
        }
    }

    public function had_followed($user_id){
        $current_user_id = $this->session->userdata('user_id');
        // if(!$current_user_id) return false;

        return $this->db->from('follow_users')->where(array(
            'follower' => $current_user_id,
            'followed' => $user_id
        ))->count_all_results();
    }

    public function get_follower_count($user_id){
        return $this->db->from('follow_users')->where('followed', $user_id)->count_all_results();
    }

    public function get_follow_count($user_id){
        return $this->db->from('follow_users')->where('follower', $user_id)->count_all_results();
    }

    public function get_followlist($type){
        $current_user_id = $this->session->userdata('user_id');
        if($type=='designers')
            $followed_users=$this->db->from('follow_designers fd')->join('users u', 'fd.followed = u.id')->join('closet c', 'u.id = c.user_id')->select('c.title, c.url_code, c.description, u.fbid, c.profile_photo_file_id c.user_id')->where('fd.follower', $current_user_id)->get()->result_array();
        else
            $followed_users = $this->db->from('follow_users fu')->join('users u', 'fu.followed = u.id')->join('closet c', 'u.id = c.user_id')->select('c.title, c.url_code, c.description, u.fbid, c.profile_photo_file_id, c.user_id')->where('fu.follower', $current_user_id)->get()->result_array();
        $this->load->library('typography');
        return array_map(function($followed_user){
            if(!is_null($followed_user['file_name'])){
                $followed_user['profile_pic_url'] = user_photo_url($followed_user['profile_photo_file_id']);
            }
            else if($followed_user['fbid']) {
                $followed_user['profile_pic_url'] = "http://graph.facebook.com/{$followed_user['fbid']}/picture?type=large";
            }
            else {
                $followed_user['profile_pic_url'] = "/assets/images/closet/profile.jpg";
            }
            $description = mb_substr(str_replace(array('.', "\n", "\t", "\r"), '', $followed_user['description']), 0, 62);
            $description .= (mb_strlen($followed_user['description']) <= 62)?"":"...<a href='/closet/view/".$followed_user["url_code"]."' style='color:inherit;'>more</a>";
            $followed_user['description_tag'] = $this->typography->auto_typography($description);
            unset($followed_user['file_name']);
            unset($followed_user['fbid']);
            return $followed_user;
        }, $followed_users);
    }

    function get_favorites($user_id){
        // product_clothing, marketplace, designer_products
        $types=['marketplace', 'product_clothing', 'designer_products'];
        $i=1;
        foreach ($types as $type) {
            $query = $this->db->select('c.*, l.id as my_like_id')
                                ->from($type.' c')
                                ->order_by("c.publication_time", "desc")
                                ->where_in('lock_status', [0, 1, 2])
                                ->join('clothing_likes l', 'c.id = l.clothing_id')
                                ->where('l.user_id', $user_id)
                                ->where('l.type', $i)
                                ->where('c.publication_time <', date('Y-m-d H:i:s'))
                                ->get();
            $liked_items[$type]=$query->result_array();
            $i++;
        }//return $this->db->last_query();

        $this->load->model('Suggested_items_model');
        $suggested_clothing_ids=$this->Suggested_items_model->get_all_clothing_ids();
        foreach ($liked_items as $type => $items) {
            foreach($items as $item => $value){
                $liked_items[$type][$item]['is_recommended']=false;
                if(in_array($liked_items[$type][$item]['id'], $suggested_clothing_ids)){
                    $liked_items[$type][$item]['is_recommended']=true;
                }//return $value;
            }//return $type_items;
        }

        return $liked_items;

        /*$this->load->model('Product_clothing_model');
        $items = $this->Product_clothing_model->get_entries();
        return $items;*/
    }
    
    function create_if_not_exit($user_id){
        $this->load->model('Closet_model');
        // if the closet already exist, return it directly
        $closet = $this->get_by_user_id($user_id);
        if (!empty($closet)){
            return $closet;
        }

        $this->db->select('nickname')->from('users')->where('id', $user_id);
        $query = $this->db->get();
        $row = $query->result_array();
        $user = $row[0];
        
        $title = '我的衣櫥';
        if ( !empty($user['nickname']) ){
            $title = $user['nickname'];
        }
        
        $description = '這是我在GetMore的衣櫥，歡迎光臨！
Shop my closet on GetMore!
— GetMore是亞洲第一個二手衣時尚網站，一起加入吧！';

        $data = array(
            'user_id'      => $user_id,
            'url_code'    => $user_id+9881,
            'title'            => $title,
            'description'=> $description,
            'banner_photo_file_id' => 0,
            'profile_photo_file_id' => 0
        );

        $this->db->insert('closet', $data);
        
        $closet = $this->get_by_user_id($user_id);
        return $closet;
    }
    
    
    function get_by_user_id($user_id=""){
        $this->db->from('closet')->where('user_id', $user_id);
        $query = $this->db->get();
        $row = $query->result_array();
        $closet = $row[0];

        if(empty($closet)){
            return null;
        }

        // set banner photo
        $closet['banner_pic_url'] = '/assets/images/closet/banner.jpg';        
        if ( !empty($closet['banner_photo_file_id']) ){
            $closet['banner_pic_url'] = user_banner_url($closet['banner_photo_file_id']);
        }

        // set profile photo
        
        // set default photo
        $closet['profile_pic_url'] = '/assets/images/closet/profile.jpg';
        $closet['profile_pic_url_big'] = site_url('/assets/images/closet/profile_big.jpg');
        // replace the default photo if user upload photo before
        if ( !empty($closet['profile_photo_file_id']) ){
            $closet['profile_pic_url'] = image_url($closet['profile_photo_file_id'], 'small', 'profile');
            $closet['profile_pic_url_big'] = image_url($closet['profile_photo_file_id'], 'medium', 'profile');
        }
        // if not, did user register with facebook?
        else{
            $user = $this->db->select('fbid')
                                           ->from('users')
                                           ->where('id', $closet['user_id'])
                                           ->get()->row_array();
            if ($user['fbid'] && $user['fbid'] != 2147483647){
                $fbid = $user['fbid'];
                $uri = "http://graph.facebook.com/$fbid/picture?type=large";
                $closet['profile_pic_url'] = $uri;
                $closet['profile_pic_url_big'] = $uri;              
            }
        }

        return $closet;
    }
    
    function change_pic($photo_type, $user_id, $file_id){
        if ( empty($user_id) ){
            throw new Exception('$user_id is empty');
        }
        if ( $photo_type === 'banner' ){
            $data = array(
                'user_id' => $user_id,
                'banner_photo_file_id' => $file_id
            );
        }
        else if ( $photo_type === 'profile' ){
            $data = array(
                'user_id' => $user_id,
                'profile_photo_file_id' => $file_id
            );
        }
        else{
            throw new Exception('invalid photo type for updating closet data.');
        }
      
        $this->db->where('user_id', $user_id);
        $this->db->update('closet', $data);
    }

    function change_text($text_type, $user_id, $content){
        if ( empty($user_id) ){
            throw new Exception('$user_id is empty');
        }
        if ( $text_type === 'title' ){
            $data = array(
                'user_id' => $user_id,
                'title' => $content
            );
        }
        else if ( $text_type === 'description' ){
            $data = array(
                'user_id' => $user_id,
                'description' => $content
            );
        }
        else{
            throw new Exception('invalid text type for editing closet data.');
        }
      
        $this->db->where('user_id', $user_id);
        $this->db->update('closet', $data);
    }
    
    function closet_update($data){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, base_url("/restful/closet"));
        curl_setopt($ch, CURLOPT_COOKIE, http_build_query($_COOKIE,'',';'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:multipart/form-data")); // 啟用POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        return curl_exec($ch);
    }
}
?>
