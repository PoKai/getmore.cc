<?php
    /*
     * Support the 'vendor'(Pop-up shop) feature in admin panel.
     * 
     * @author howtomakeaturn
     */
class Vendor_model extends CI_Model {
    
    function __construct(){
        // 呼叫模型(Model)的建構函數
        parent::__construct();
        $this->load->database();
        $this->load->model('Buy_order_model', 'buyorder');
    }
    
    /*
     * Get those pay with 'cash'
     */
    function get_list(){
        $orders = $this->db->where('payment_method', 'cash')
                                            ->order_by('created_at', 'desc')
                                            ->from('buy_order')
                                            ->get()->result_array();
                                            
        return $orders;
    }
    
    function get_by_order_number($order_number){
        $order = $this->buyorder->get_one_by_order_number($order_number);
        $this->load->model('Product_clothing_model');
        $clothings = $this->Product_clothing_model->get_some_by_order_id_and_join_sellorder_product($order['id']);
        
        foreach($clothings as &$clothing){
            if ($clothing['lock_status']==1){
                $clothing['cancelable'] = true;
            }
        }
        
        $data = array(
            'order' => $order,
            'clothings' => $clothings
        );
        
        return $data;
    }
    
    function create($user_id){
        $this->load->library('session');
      
        $order_number = $this->buyorder->generate_order_number();

        $user = $this->db->where('id', $user_id)->from('users')
                                        ->get()->row_array();
                                        
        $this->buyorder->insert(array(
            'user_id' => $user_id,
            'buyer_name' =>$user['user_name'],
            'receiver_name' =>$user['user_name'],
            // 'status' => '4',            
            'status' => '1',            
            'payment_method' => 'cash',
            'order_number' => $order_number,
            'created_at' => date("Y-m-d H:i:s"),            
            // 'paid_at' => date("Y-m-d H:i:s"),            
            // 'shipped_at' => date("Y-m-d H:i:s"),
            'admin_id'    => $this->session->userdata('user_id')
        ));      
        
        return $order_number;
    }
    
    function put_in_by_washing_id($washing_id, $order_number){
        
        $query = $this->db->select('product_item_number')
                                        ->from('sellorders_products')
                                        ->where('shipping_wash_id', $washing_id)
                                        ->get();
        
        if ($query->num_rows() !== 1){
            throw new Exception('not exactly one row');
        }
        
        $clothing = $query->row_array();
        
        $this->load->model('Product_clothing_model');
        $this->Product_clothing_model
                ->put_one_into_physical_buyorder($clothing['product_item_number'], $order_number, true);
        
        return true;        
    }

    function remove_from_order($clothing_id){
        $clothing = $this->db->where('id', $clothing_id)
                                            ->get('product_clothing')
                                            ->row_array();

        if ($clothing['lock_status'] != 1){
            throw new Exception('clothing id ' . $clothing_id . ' is not in lock_status 1');
        }

        $order = $this->db->where('id', $clothing['buy_order_id'])
                                            ->get('buy_order')
                                            ->row_array();

        $items_count = $order['items_count'] - 1;
        $items_price = $order['items_price'] - $clothing['product_sell_price'];
        $total_price = $order['total_price'] - $clothing['product_sell_price'];
        
        $order_data = array(
            'items_count' => $items_count,
            'items_price' => $items_price,
            'total_price' => $total_price
        );
        $this->buyorder->update($clothing['buy_order_id'], $order_data);

        // release the clothing
        $data = array(
            'lock_status' => '0',
            'buy_order_id' => '',
            'last_locked_time' => ''
        );
        $this->db->where('id', $clothing_id);
        $this->db->update('product_clothing', $data);

        
        return true;        
    }


    function checkout_order($order_number){
        $this->buyorder->switch_one_to_paid_status_by_order_number($order_number);

        $data = array( 
            'status' => '4',
            'shipped_at' => date("Y-m-d H:i:s")
        );                
        $this->db->where('order_number', $order_number);
        $this->db->update('buy_order', $data);        

        return true;        
    }
    
}
