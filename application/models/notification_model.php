<?php
class Notification_model extends JR_Model {
  
    function create($clothing_id, $owner_id){
        // check if clothing id valid
        $this->load->model('Product_clothing_model');
        $clothing = $this->Product_clothing_model->get($clothing_id);
        if (empty($clothing)){
            throw new Exception('clothing id does not exist');
        }
        
        $user_id = $this->Product_clothing_model->get_user_id_by_product_item_number($clothing->product_item_number);
        
        $created_at = date("Y-m-d H:i:s");
        $data = array( 'clothing_id' => $clothing_id, 'owner_id' => $user_id,
            'notified' => 0, 'created_at' => $created_at );
        
        $this->insert($data);
        return $this->db->insert_id();
    }
    
    function get_clothings_to_be_notified_by_user_id($user_id){
        // only fetch unnotified clothing_id
        $this->db->where('notified', false);
        $rows = $this->get_many_by('owner_id', $user_id);
        
        // fetch clothing information
        $clothings = array();
        foreach($rows as $row){
            $this->load->model('Product_clothing_model');
            $clothing = $this->db->from('product_clothing')->
                where('id', $row->clothing_id)->get()->result()[0];
            array_push($clothings, $clothing);
        }
        
        return $clothings;      
    }
    
    function notify_many_users_has_suggested_price(){
        // get all the users
        $rows = $this->db->distinct()
                                                  ->select('owner_id')
                                                  ->where('notified', FALSE)
                                                  ->from('notifications')
                                                  ->get()->result();        
        foreach($rows as $row){
            $this->notify_user_has_suggested_price($row->owner_id);
        }

        foreach($rows as $row){
            $this->update_by( array('owner_id'=>$row->owner_id), array('notified'=>TRUE) );
        }
        
        return TRUE;
    }
    
    public function notify_user_has_suggested_price($user_id){
        $clothings = $this->get_clothings_to_be_notified_by_user_id($user_id);

        $this->load->spark('codeigniter-template/1.0.0');
        $this->template->set_layout('template/email');
        $html = $this->template->render(
            'email/notify_user_has_suggested_price', ['clothings' => $clothings], true);
        
        // $email = $this->db->get_where('users', array('id' => $user_id))->result()[0]->email;
        $this->load->model('User_model');
        $email = $this->User_model->get_effective_email($user_id);
        
        $this->load->library('notification');
        $this->notification->send('Getmore衣服降價建議', $html, $email);        

        return true;
    }
}
?>
