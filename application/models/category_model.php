<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tables['category'] = 'category';
    }

    /**
     * categories
     *
     * @return object Categories
     * @author appleboy
     **/
    public function categories()
    {
        // define in MY_Model
        $this->handle_process();

        $this->response = $this->db->get($this->tables['category']);

        return $this;
    }

    /**
     * category
     *
     * @return object
     * @author appleboy
     **/
    public function category($id = NULL)
    {
        $this->limit(1);
        $this->where($this->tables['category'].'.id', $id);

        $this->categories();

        return $this;
    }

    /**
     * Add category
     *
     * @return bool
     * @author appleboy
     **/
    public function add_category($data = array())
    {
        if(empty($data))

            return false;

        // Category table.
        $data = array(
            'type'       => $data['type'],
            'tw_name'    => $data['tw_name'],
            'en_name'    => $data['en_name'],
            'sort_order' => $data['sort_order'],
            'add_time'   => time(),
            'edit_time'  => time()
        );

        $this->db->insert($this->tables['category'], $data);
        $id = $this->db->insert_id();

        return $id;
    }

    /**
     * update category
     *
     * @return bool
     * @author appleboy
     **/
    public function update_category($id, $data = array())
    {
        if(empty($id) or empty($data))

            return false;

        $data = array_merge($data, array('edit_time' => time()));
        $this->db->update($this->tables['category'], $data, array('id' => $data['id']));

        return true;
    }

    /**
    * delete category
    *
    * @return bool
    * @author appleboy
    **/
    public function delete_category($id)
    {
        $this->db->trans_begin();

        if(is_array($id))
           $this->db->where_in('id', $id);
        else
           $this->db->where('id', $id);

        $this->db->delete($this->tables['category']);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            return FALSE;
        }

        $this->db->trans_commit();

        return TRUE;
    }
}
