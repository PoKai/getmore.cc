<?php
class Clothing_model extends CI_Model {
    private $ch;

    function __construct(){
        parent::__construct();
        $this->load->spark('curl');
        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_URL, base_url("/restful/marketplace"));
        curl_setopt($this->ch, CURLOPT_COOKIE, http_build_query($_COOKIE,'',';'));
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        $this->load->database();
    }

    function _toggle_like_clothing($user_id, $clothing_id, $item_type){
        //return $product_item_number[2];
        $query = $this->db->where('user_id', $user_id)
            ->where('clothing_id', $clothing_id)
            ->where('type', $item_type)
            ->from('clothing_likes')
            ->get();

        if($query->num_rows()==0){
            $query->free_result();
            $data = array(
                'user_id' => $user_id, 
                'clothing_id' => $clothing_id,
                'planA' => $item_type,
                'type' => $item_type
                );
            $this->db->insert('clothing_likes', $data);
            $msg=array('msg' => 'success_like');
        }
        else{
            $this->db->where('id', $query->row()->id)->delete('clothing_likes');
            $msg=array('msg' => 'success_unlike');
        }
        return json_encode($msg);
    }

    function _item_is_liked($clothing_id, $type){
        $user_id=$this->session->userdata('user_id');
        if(empty($user_id)) return false;

        $query=$this->db->where('user_id', $user_id)
            ->where('clothing_id', $clothing_id)
            ->where('type', $type)
            ->from('clothing_likes')
            ->get();

        return $query->num_rows()!=0;
    }

    function _new_clothing_thumbnail($item, $type){
        $this->load->helper('html');
        $this->load->model('Clothing_model');
        $item_html='';

        if($type=='designer_products'){
            $link_type='designer';
            $item_type='3';
        }
        else if($type=='marketplace'){
            $link_type='marketplace';
            $item_type='1';
        }
        else{
            $link_type='brandsonly';
            $item_type='2';
        }

        if($this->Clothing_model->_item_is_liked($item->id, $item_type))
            $is_liked='liked';
        else
            $is_liked='to_like';

        $colors_div='';
        foreach ($item->colors as $color) {
            $colors_div.=
            '<div style="width: 0;height: 0;border-width: 5px;border-style: solid;border-color: '.$color.';display: inline-block;margin-right:0px;" class="ng-scope"></div>';
        }

        if(is_array($item->files)){
            $img_src = image_url($item->files[0]);
        }
        else {
            $img_src = image_url(explode(',', $item->files)[0]);
        }

        if($item->lock_status==2)
            $sold_out_text='
                <p class="sold-out-text text-center" style="position:absolute;top:0;left:0;bottom:0;right:0;padding-top:125px;font-weight:bold;font-size:1.4em;background-color:#ffffff;opacity:0.7;">
                    SOLD<br/>OUT
                </p>';
        else $sold_out_text='';

        $item_html.=
        '<a href="/clothing/'.$link_type.'/'.$item->id.'" target="_blank">
          <div class="fresh-product-view lock-status-'.$item->lock_status.'">'.
            $sold_out_text.'
            <img src="'.$img_src.'">
            <div class="product-brand">'.$item->product_brand.'</div>
            <div class="fresh-product-title">'.$item->product_title.'</div>
            <div class="fresh-product-price" style="overflow:hidden;">
              <span class="is-deleted">NT$ '.$item->product_original_price.'</span>
              <span class="fresh-product-sale">NT$ '.$item->product_sell_price.'</span>
            </div>'.
            $colors_div.
            '<div class="likeslideup like-fade">
              <div class="likeslideupcontent '.$is_liked.'" data-id="'.$item->id.'"><i class="fa fa-gratipay fa-2x"></i>
                <div>'.
                    ($is_liked?'SAVED':'SAVE')
                .'</div>
                <input type="hidden" class="like-item-type" value="'.$item_type.'">
              </div>
            </div>
          </div>
        </a>';
        return $item_html;
    }

    function buyer_history($item, $type) {
         $this->load->helper('html');
        $item_html='';

        if($type=='designer_products'){
            $link_type='designer';
            $item_type='3';
        }
        else if($type=='marketplace'){
            $link_type='marketplace';
            $item_type='1';
        }else{
            $link_type='brandsonly';
            $item_type='2';
        }

        if(is_array($item->files)){
            $img_src = image_url($item->files[0]);
        }
        else {
            $img_src = image_url(explode(',', $item->files)[0]);
        }

        $item_html.=
        '<a href="/clothing/'.$link_type.'/" target="_blank">
          <div class="fresh-product-view">
            <img src="'.$img_src.'">
            <div class="fresh-product-title">'.$item->product_title.'</div>
            <div class="fresh-product-price" style="overflow:hidden;">
              <span class="fresh-product-sale">NT$ '.$item->product_sell_price.'</span>
            </div>
          </div>
        </a>';
        return $item_html;
    }
}