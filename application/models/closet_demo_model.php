<?php
    /*
     * The photos show in the closet show room.
     * @author howtomakeaturn
     */
class Closet_demo_model extends JR_Model {
  
    public $_table = 'closet_demos';
  
    function Closet_demo()
    {
        // Call the Model constructor
        parent::Model();
        /* load 資料庫 */
        $this->load->database();
    }
    
    function change_text($user_id, $demo_id, $description){
        if ( empty($user_id) || empty($demo_id) ){
            throw new Exception('some variables are empty');
        }
        
        $this->db->from('closet_demos');
        $this->db->where('id', $demo_id);

        $query = $this->db->get();
        $demo = $query->result_array()[0];

        if ($demo['user_id'] !== $user_id){
            throw new Exception('user_id is not the same as creator of the demo');          
        }
                
        $data = array(
            'id' => $demo_id,
            'description' => $description
        );
      
        $this->db->where('id', $demo_id);
        $this->db->update('closet_demos', $data);
    }    
    
    function change_pic($user_id, $demo_id, $file_id){
        if ( empty($user_id) || empty($demo_id) || empty($file_id) ){
            throw new Exception('some variables are empty');
        }

        $demo = $this->get($demo_id);
        if ($demo->user_id !== $user_id){
            throw new Exception('user_id is not the same as creator of the demo');          
        }

        $data = array(
            'file_id' => $file_id,
        );
        
        $this->update($demo_id, $data);
    }
    
    function create($closet_id, $clothing_id, $file_id, $user_id, $description){
        
        if ( empty($file_id) || empty($user_id) || empty($clothing_id) || empty($closet_id) ){
            throw new Exception('some variables are empty');
        }

        $this->db->from('closet_demos');
        $this->db->where('user_id', $user_id);

        $query = $this->db->get();
        $rows = $query->result_array();        
        
        // users can only have 30 demos
        if (count($rows) >= 30){
            return false;
        }
        
        $data = array(
            'closet_id'      => $closet_id,
            'clothing_id'    => $clothing_id,
            'file_id'            => $file_id,
            'user_id'            => $user_id,
            'description'=> $description,
            'num_of_likes' => 0,
            'created_at' => date("Y-m-d H:i:s")
        );

        $this->db->insert('closet_demos', $data);        
    }
    
    function get_many_by_user_id($user_id=''){
        $this->db->select('c.product_title, d.description, d.file_id, c.id, d.created_at, d.num_of_likes, d.id as demo_id');
        $this->db->from('closet_demos d');
        $this->db->join('product_clothing c', 'd.clothing_id = c.id');
        $this->db->where('user_id', $user_id);

        $query = $this->db->get();
        $items = $query->result_array();
        
        // get main picture name from files
        foreach ($items as &$clothing){
            $this->db->select('file_name')->from('files')->where('file_id', $clothing['file_id'])->limit(1);//get the first picture file
            $query_files = $this->db->get();
            $row_files = $query_files->result_array();
            $clothing['picture_name'] = $row_files[0]['file_name'];
        }
        
        $this->load->library('typography');
        // change text format for html rendering
        foreach ($items as &$clothing){
            $clothing['description_tag'] = $this->typography->auto_typography($clothing['description']);
        }


        
        return $items;
    }
    
    /*
     * Delete the demo with some simple validation.
     */
    function delete_with_validation($demo_id, $user_id){
        if ( empty($user_id) || empty($demo_id) ){
            throw new Exception('some variables are empty');
        }
        
        $demo = $this->get($demo_id);
        if ($demo->user_id !== $user_id){
            throw new Exception('user_id is not the demo owner');
        }

        $this->db->trans_start();

        $this->delete($demo_id);
        $this->load->model('Demo_like_model');
        $this->Demo_like_model->delete_by('demo_id', $demo_id);
        
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            throw new Exception('errors happened in transaction. in delete_with_validation');
        }
    }    
    
}
?>
