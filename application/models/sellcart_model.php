<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sellcart_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tables['cart'] = 'sellers_carts';
    }

    /**
     * carts
     *
     * @return object
     * @author appleboy
     **/
    public function carts()
    {
        // define in MY_Model
        $this->handle_process();

        $this->response = $this->db->get($this->tables['cart']);

        return $this;
    }

    /**
     * cart
     *
     * @return object
     * @author appleboy
     **/
    public function cart($id = NULL)
    {
        $this->limit(1);
        $this->where($this->tables['cart'].'.id', $id);

        $this->carts();

        return $this;
    }

    /**
     * Add cart
     *
     * @return bool
     * @author appleboy
     **/
    public function add_cart($data = array())
    {
      /*
        if(empty($data))

            return false;
*/
        // Cart table.
        $another = array(
            'add_time'   => time(),
            'edit_time'  => time()
        );

        $data = array_merge($data, $another);
        $this->db->insert($this->tables['cart'], $data);
        $id = $this->db->insert_id();

        return $id;
    }

    /**
     * update cart
     *
     * @return bool
     * @author appleboy
     **/
    public function update_cart($id, $data = array())
    {
        if(empty($id) or empty($data))

            return false;

        $data = array_merge($data, array('edit_time' => time()));
        $this->db->where('id', $id)->update($this->tables['cart'], $data);

        return true;
    }

    /**
    * delete cart
    *
    * @return bool
    * @author appleboy
    **/
    public function delete_cart($id)
    {
        $this->db->trans_begin();

        if (isset($id['user_id'])) {
            $this->db->where('user_id', $id['user_id']);
        } else {
            if(is_array($id))
               $this->db->where_in('id', $id);
            else
               $this->db->where('id', $id);
        }

        $this->db->delete($this->tables['cart']);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            return FALSE;
        }

        $this->db->trans_commit();

        return TRUE;
    }
}
