<?php

/*
 * Actions related to 'sellorders_products' table
 * 
 * @author howtomakeaturn
 */

class Product_model extends JR_Model {
    
    public $_table = 'sellorders_products';
    // When inserting data, there are necessary.
    // Let validation library check them for you.
    public $validate = array(
        array( 'field' => 'order_id',
               'rules' => 'required' ),
        array( 'field' => 'product_id',
               'rules' => 'required' ),
        array( 'field' => 'product_title',
               'rules' => 'required' ),
        array( 'field' => 'product_item_number',
               'rules' => 'required' ),
        array( 'field' => 'product_original_price',
               'rules' => 'required' ),
        array( 'field' => 'product_sell_price',
               'rules' => 'required' ),
        array( 'field' => 'shipping_price',
               'rules' => 'required' ),
        array( 'field' => 'product_type',
               'rules' => 'required' ),
        array( 'field' => 'donative',
               'rules' => 'required' )
    );

    function __construct(){
        parent::__construct();
        $this->load->database();
    }
    
    function add_to_order($order_id, $item_data){
        $data = $item_data;
        $data['order_id'] = $order_id;
        $data['product_id'] = $this->generate_product_id($order_id);
        $data['product_item_number'] = $this->generate_product_item_number($order_id);
        $data['shipping_price'] = $this->get_type_washing_fee($data['product_type']);
        $data['add_time'] = time();
        $id = $this->insert($data);
        if (!$id){
            throw new Exception('fail to pass the model validation');
        }
        return $id;      
    }
    
    function generate_product_id($order_id){
        $query = $this->db->from('sellorders_products')
                                         ->where('order_id', $order_id)
                                         ->get();
        
        $product_id = $query->num_rows() + 1;

        return $product_id;      
    }
    
    function generate_product_item_number($order_id){

        $product_id = $this->generate_product_id($order_id);
       
        $product_id_str = str_pad($product_id,3,'0',STR_PAD_LEFT);
        
        $order_number = $this->db->select('order_number')
                                                       ->from('sellorders')
                                                       ->where('id', $order_id)
                                                       ->get()->row_array()['order_number'];

        $product_item_number = $order_number.$product_id_str;
        
        return $product_item_number;
    }
    
    function not_rejected(){
        $this->db->where('remove', FALSE);
        return $this;
    }
    
    function get_type_washing_fee($product_type){
        $this->load->config('clothing_class');
        $type_price = $this->config->item('class_price');
        $result = null;
        if (strlen($product_type)===2){
            $result = $type_price[substr($product_type, 0, 1)][$product_type];
        }
        if (strlen($product_type)===3){
            $result = $type_price[substr($product_type, 0, 1)][substr($product_type, 0, 2)][$product_type];
        }
        
        if (!$result){
            throw new Exception('no washing fee matched.');
        }
        return $result;
    }
    
}
