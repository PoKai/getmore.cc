<?php
    /**
     * provide marketing information
     * @author howtomakeaturn
     **/
class Marketing_model extends CI_Model {

    /*
     * @author howtomakeaturn
     */
    function get_all_emails(){
        $user_ids = array();
        $datas = $this->db->select('id')->from('users')->get()->result();
        foreach($datas as $data){
            array_push($user_ids, $data->id);
        }
        
        return $this->get_many_email_address_by_user_id($user_ids);
    }
    

    /*
     * @author howtomakeaturn
     */
    function get_buyer_emails(){
        $effective_users = array();
        $effective_orders = $this->db->select('user_id')
                                                            ->from('buy_order')
                                                            ->where_in('status', [2,3,4,5])
                                                            ->get()->result();
        
        foreach($effective_orders as $order){
            if ( !in_array($order->user_id, $effective_users) ){
                array_push( $effective_users, $order->user_id );
            }
        }
        
        return $this->get_many_email_address_by_user_id($effective_users);          
    }
    
    /*
     * @author howtomakeaturn
     */
    function get_seller_emails(){
        $effective_users = array();
        $effective_orders = $this->db->select('user_id')
                                                            ->from('sellorders')
                                                            ->where_in('status_id', [2,3,4,5,6])
                                                            ->get()->result();
        
        foreach($effective_orders as $order){
            if ( !in_array($order->user_id, $effective_users) ){
                array_push( $effective_users, $order->user_id );
            }
        }
        
        return $this->get_many_email_address_by_user_id($effective_users);          
    }    
      
    /*
     * Get stdClass instances with address and email type(active or general)
     * @return Array
     * @author howtomakeaturn
     */
    private function get_many_email_address_by_user_id($user_ids){
        $final_emails = array();        
        $datas = $this->db->select('email, active_email')->from('users')->where_in('id', $user_ids)->get()->result();
        foreach($datas as $data){
            $email_obj = new stdClass();

            if ( !empty($data->active_email) ){
                $email_obj->address = $data->active_email;
                $email_obj->type = 'active';
                array_push($final_emails, $email_obj);
            }
            else{
                $email_obj->address = $data->email;
                $email_obj->type = 'general';
                array_push($final_emails, $email_obj);
            }
        }
        
        return $final_emails;          
    }
    
}
?>
