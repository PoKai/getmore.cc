<?php
    /*
     * The withdraw transactions.
     * 
     * @author howtomakeaturn
     */
class Withdraw_model extends JR_Model {
    
    function __construct()
    {
        // 呼叫模型(Model)的建構函數
        parent::__construct();
        $this->load->database();
        $this->load->model('Transaction_model', 'transaction');
    }
    
    /*
     * @author howtomakeaturn
     * @return Boolean
     */
    function create_record( $user_id = '', $amount = 0, $bank_name='', $branch_name= '', $bank_account=''){
        if (count(func_get_args())!=5){
            throw new Exception('number of parameters is not correct');
        }
        if ( empty($user_id) || empty($amount) || empty($bank_name) || 
              empty($branch_name) || empty($bank_account)  )
        {
            throw new Exception('one of the parameters is empty');
        }
        
        // it MUST to be processed in transaction mode.
        $this->db->trans_start();

        $this->transaction->insert_to_user($user_id, '申請匯出現金', '', -1 * $amount, '');
        
        $this->withdraw->insert(array(
            'user_id' => $user_id,
            'amount' => $amount,
            'bank_name' => $bank_name,
            'branch_name' => $branch_name,
            'bank_account' => $bank_account,
            'created_at' => date("Y-m-d H:i:s")
        ));
                
        
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            throw new Exception('errors happened in transaction.
                in create_record');
        }
        return TRUE;
    }
    
    public function latest_withdraws(){
        $this->db->order_by('created_at', 'desc');
        return $this;
    }
    public function join_users(){
        $this->db->join('users', 'users.id = withdraws.user_id');
        return $this;
    }        
    
}
