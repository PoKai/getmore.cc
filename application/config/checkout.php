<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

// how long does one unpaid credit-cart order lives (in minutes)
$config['active_time'] = 10;

// how long does one unpaid atm order lives (in hours)
$config['atm_active_time'] = 24;

// how long does one unpaid line pay order lives (in minutes)
$config['line_pay_active_time'] = 20;