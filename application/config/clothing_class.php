<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

$class_name[1] = 'Women';
$class_name[11] = '上衣';
$class_name[12] = '洋裝';
$class_name[13] = '褲子';
$class_name[14] = '裙子';
$class_name[15] = '外套';
$class_name[151] = '一般外套';
$class_name[152] = '羽絨衣';
$class_name[153] = '真皮外套';
$class_name[2] = 'Men';
$class_name[21] = '上衣';
$class_name[22] = '襯衫';
$class_name[23] = '褲子';
$class_name[24] = '外套';
$class_name[241] = '一般外套';
$class_name[242] = '羽絨衣';
$class_name[243] = '真皮外套';
$class_name[3] = 'Kids / Baby';
$class_name[31] = '連身衣';
$class_name[32] = '上衣';
$class_name[33] = '下身';
$class_name[34] = '洋裝';
$class_name[35] = '外套';
$class_name[351] = '一般外套';
$class_name[352] = '羽絨衣';

$class_name['1S1'] = '靴子';
$class_name['1S2'] = '平底鞋';
$class_name['1S3'] = '雨鞋';
$class_name['1S4'] = '涼拖鞋';
$class_name['1S5'] = '運動休閒鞋';
$class_name['1S6'] = '高跟鞋';

$class_name['1B1'] = '包';

$class_name['1A1'] = '飾品';
$class_name['1A2'] = '錢包';
$class_name['1A3'] = '手錶';
$class_name['1A4'] = '皮帶';
$class_name['1A5'] = '手套';
$class_name['1A6'] = '髮飾';
$class_name['1A7'] = '帽子';
$class_name['1A8'] = '鑰匙圈/鑰匙包';
$class_name['1A9'] = '絲巾/圍巾';
$class_name['1A10'] = '襪子';
$class_name['1A11'] = '眼鏡/太陽眼鏡';
$class_name['1A12'] = '3C配件';
$class_name['1A13'] = '文具';
$class_name['1A14'] = '家飾/居家用品';
$class_name['1A15'] = '旅行用品';
$class_name['1A16'] = '雨傘';

$class_name['2S1'] = '靴子';
$class_name['2S2'] = '皮鞋';
$class_name['2S3'] = '休閒鞋';
$class_name['2S4'] = '運動鞋';
$class_name['2S5'] = '涼拖鞋';
$class_name['2S6'] = '鞋把';

$class_name['2B1'] = '包';

$class_name['2A1'] = '飾品';
$class_name['2A2'] = '手錶';
$class_name['2A3'] = '皮帶';
$class_name['2A4'] = '手套';
$class_name['2A5'] = '領帶/領結';
$class_name['2A6'] = '手帕/口袋巾';
$class_name['2A7'] = '帽子';
$class_name['2A8'] = '鑰匙圈/鑰匙包';
$class_name['2A9'] = '絲巾/圍巾';
$class_name['2A10'] = '襪子';
$class_name['2A11'] = '眼鏡/太陽眼鏡';
$class_name['2A12'] = '皮夾/鈔票夾';
$class_name['2A13'] = '3C配件';
$class_name['2A14'] = '文具';
$class_name['2A15'] = '家飾/居家用品';
$class_name['2A16'] = '旅行用品';
$class_name['2A17'] = '雨傘';

$class_name['3S1'] = '靴子';
$class_name['3S2'] = '平底鞋';
$class_name['3S3'] = '雨鞋';
$class_name['3S4'] = '涼拖鞋';
$class_name['3S5'] = '運動休閒鞋';
$class_name['3S6'] = '學步鞋';

$class_name['3B1'] = '包';

$class_name['3A1'] = '飾品';
$class_name['3A2'] = '手錶';
$class_name['3A3'] = '皮帶';
$class_name['3A4'] = '手套';
$class_name['3A5'] = '髮飾';
$class_name['3A6'] = '帽子';
$class_name['3A7'] = '絲巾/圍巾';
$class_name['3A8'] = '襪子';
$class_name['3A9'] = '眼鏡/太陽眼鏡';
$class_name['3A10'] = '文具';
$class_name['3A11'] = '雨傘';

$s_price = 99; //小衣
$b_price = 99; //大衣
$f_price = 99; //羽絨衣

$config['s_price'] = $s_price;
$config['b_price'] = $b_price;
$config['f_price'] = $f_price;

$class_price[1][11] = $s_price;
$class_price[1][12] = $b_price;
$class_price[1][13] = $s_price;
$class_price[1][14] = $s_price;
$class_price[1][15][151] = $b_price;
$class_price[1][15][152] = $f_price;
$class_price[1][15][153] = $f_price;

$class_price[2][21] = $s_price;
$class_price[2][22] = $s_price;
$class_price[2][23] = $s_price;
$class_price[2][24][241] = $b_price;
$class_price[2][24][242] = $f_price;
$class_price[2][24][243] = $f_price;

$class_price[3][31] = $s_price;
$class_price[3][32] = $s_price;
$class_price[3][33] = $s_price;
$class_price[3][34] = $s_price;
$class_price[3][35][351] = $s_price;
$class_price[3][35][352] = $f_price;

$class_transport['less'] = 180;
$class_transport['more'] = 80;

$config['class_name'] = $class_name;
$config['class_price'] = $class_price;
$config['class_transport'] = $class_transport;
/**
1.Women	11.上衣			小衣	75
12.洋裝			大衣	120
13.褲子			小衣	75
14.裙子			小衣	75
15.外套	151.一般外套	大衣	120
152.羽絨衣/背心	羽絨衣	150
皮衣	另計

2.Men	21.上衣			小衣	75
22.襯衫			小衣	75
23.褲子			小衣	75
24.外套	241.一般外套	大衣	120
242.羽絨衣/背心	羽絨衣	150
皮衣	另計

3.Kids / Baby	31.連身衣		小衣	75
32.上衣			小衣	75
33.下身			小衣	75
34.洋裝			小衣	75
35.外套	351.一般外套	小衣	75
352.羽絨衣/背心	羽絨衣	150
 **/

/**

無袖  ns
短袖  ss
長袖  ls
短褲  sp
長褲  lp
短版  sb
長版  lb

 **/

/* season mapping
 * @author howtomakeaturn
 */
$season_name[1] = '春夏';
$season_name[2] = '秋冬';
$season_name[3] = '無季節';

$config['season_name'] = $season_name;

/* style mapping
 * @author howtomakeaturn
 */

$style_name[1] = '商務';
$style_name[2] = '復古';
$style_name[3] = '浪漫';
$style_name[4] = '小碎花';
$style_name[5] = '幾何';
$style_name[6] = '格子';
$style_name[7] = '條紋';
$style_name[8] = '正式場合';
$style_name[9] = '牛仔';
$style_name[10] = '運動風';

$style_name[11] = '休閒';
$style_name[12] = '學院';
$style_name[13] = '可愛';
$style_name[14] = '奢華';
$style_name[15] = '宴會';
$style_name[16] = '中國風';

$style_name[17] = '小紳士';
$style_name[18] = '小淑女';

$style_name[19] = '動物紋';

$style_name[20] = '印花';
$style_name[21] = '個性';
$style_name[22] = '民俗風';
$style_name[23] = '性感';

$style_name[24] = '皮質';
$style_name[25] = '吊帶';
$style_name[26] = '孕婦';
$style_name[27] = '棉質';
$style_name[28] = '絲質';

$style_name[29] = '婚禮';
$style_name[30] = '新娘';
$style_name[31] = '新郎';
$style_name[32] = '小花童';
$style_name[33] = 'Bling Bling';
$style_name[34] = '量身訂做';
$style_name[35] = '鉚釘';

$config['style_name'] = $style_name;

/* pattern mapping
 * @author howtomakeaturn
 */
$pattern_name[1] = 'Legging';
$pattern_name[2] = 'Polo衫';
$pattern_name[3] = 'T-Shirt';
$pattern_name[4] = '夾克';
$pattern_name[5] = '休閒褲';
$pattern_name[6] = '休閒襯衫';
$pattern_name[7] = '其他';
$pattern_name[8] = '商務襯衫';
$pattern_name[9] = '大衣';
$pattern_name[10] = '娃娃裝';
$pattern_name[11] = '斗篷';
$pattern_name[12] = '棉質外套';
$pattern_name[13] = '牛仔褲';
$pattern_name[14] = '皮衣';
$pattern_name[15] = '短洋裝';
$pattern_name[16] = '短裙';
$pattern_name[17] = '羽絨衣';
$pattern_name[18] = '羽絨衣';
$pattern_name[19] = '背心';
$pattern_name[20] = '裙子';
$pattern_name[21] = '褲子';
$pattern_name[22] = '褲裙';
$pattern_name[23] = '襯衫';
$pattern_name[24] = '西裝外套';
$pattern_name[25] = '西裝褲';
$pattern_name[26] = '連身褲';
$pattern_name[27] = '運動外套';
$pattern_name[28] = '針織/毛衣';
$pattern_name[29] = '針織/毛衣外套';
$pattern_name[30] = '長上衣';
$pattern_name[31] = '長洋裝';
$pattern_name[32] = '長裙';
$pattern_name[33] = '雪紡';
$pattern_name[34] = '背心外套';
$pattern_name[35] = '休閒運動衫';
$pattern_name[36] = 'A Line/圓裙';
$pattern_name[37] = '窄裙/鉛筆裙';
$pattern_name[38] = '風衣';
$pattern_name[39] = '海灘褲';
$pattern_name[40] = '運動褲';

$pattern_name[41] = '踝靴';
$pattern_name[42] = '短靴';
$pattern_name[43] = '中筒靴';
$pattern_name[44] = '長靴';
$pattern_name[45] = '過膝靴';
$pattern_name[46] = '芭蕾舞鞋';
$pattern_name[47] = '尖頭平底鞋';
$pattern_name[48] = '草編鞋';
$pattern_name[49] = '樂福鞋/莫卡辛';
$pattern_name[50] = '牛津鞋';
$pattern_name[51] = '涼鞋';
$pattern_name[52] = '拖鞋';
$pattern_name[53] = '夾腳涼拖鞋';
$pattern_name[54] = '運動鞋';
$pattern_name[55] = '休閒鞋';
$pattern_name[56] = 'slip on';
$pattern_name[57] = '圓頭鞋';
$pattern_name[58] = '尖頭鞋';
$pattern_name[59] = '魚口鞋';
$pattern_name[60] = '化妝包';
$pattern_name[61] = '手拿包';
$pattern_name[62] = '手提包';
$pattern_name[63] = '水桶包';
$pattern_name[64] = '托特包';
$pattern_name[65] = '肩背包';
$pattern_name[66] = '後背包';
$pattern_name[67] = '斜背包';
$pattern_name[68] = '旅行袋';
$pattern_name[69] = '行李箱';
$pattern_name[70] = '媽媽包';
$pattern_name[71] = '手鍊/手環';
$pattern_name[72] = '項鍊';
$pattern_name[73] = '戒指';
$pattern_name[74] = '耳環/耳骨夾';
$pattern_name[75] = '胸針/別針';
$pattern_name[76] = '身體首飾/裝飾';
$pattern_name[77] = '珠寶盒';
$pattern_name[78] = '皮夾';
$pattern_name[79] = '卡夾';
$pattern_name[80] = '零錢包';
$pattern_name[81] = '髮圈';
$pattern_name[82] = '髮箍';
$pattern_name[83] = '髮梳';
$pattern_name[84] = '髮夾';
$pattern_name[85] = '髮帶/頭巾';
$pattern_name[86] = '頭飾/頭紗';
$pattern_name[87] = '新娘花圈';
$pattern_name[88] = '軟呢帽';
$pattern_name[89] = '毛帽';
$pattern_name[90] = '漁夫帽';
$pattern_name[91] = '草帽/遮陽帽';
$pattern_name[92] = '棒球帽';
$pattern_name[93] = '貝蕾帽';
$pattern_name[94] = '報童帽';
$pattern_name[95] = '絲巾';
$pattern_name[96] = '圍巾';
$pattern_name[97] = '圍脖';
$pattern_name[98] = '披肩';
$pattern_name[99] = '手機殼/手機包';
$pattern_name[100] = '筆電包';
$pattern_name[101] = '平板配件';
$pattern_name[102] = '耳機';
$pattern_name[103] = 'USB';
$pattern_name[104] = '收藏擺飾';
$pattern_name[105] = '香氛';
$pattern_name[106] = '毛巾/浴巾';
$pattern_name[107] = '洗滌';
$pattern_name[108] = '寵物';
$pattern_name[109] = '公事包';
$pattern_name[110] = '信差包';
$pattern_name[111] = '運動包/旅行袋';
$pattern_name[112] = '腰包';
$pattern_name[113] = '盥洗包';
$pattern_name[114] = '耳環';
$pattern_name[115] = '領帶夾';
$pattern_name[116] = '袖扣';
$pattern_name[117] = '胸花';
$pattern_name[118] = '紳士帽';
$pattern_name[119] = '棒球帽';
$pattern_name[120] = '草帽';
$pattern_name[121] = '頭帶/頭巾';
$pattern_name[122] = '收藏品';
$pattern_name[123] = '薰香' /*'熏香'*/;
$pattern_name[124] = '書包/後背包';
$pattern_name[125] = '別針';

$config['pattern_name'] = $pattern_name;

/* sub_pattern mapping */
$sub_pattern_1 = [
	1 => '靴型',
	2 => '窄管',
	3 => '喇叭',
	4 => '直筒',
	5 => '男友褲',
	6 => '縮口',
	7 => '短版上衣',
	8 => '長上衣',
];
$sub_pattern_2 = [
	1 => '高腰',
	3 => '低腰',
];
$sub_pattern_3 = [
	1 => '低檔',
];

/* clothing subtype-patterns mapping
 * @author howtomakeaturn
 */
$subtype_patterns[1] = array();
$subtype_patterns[11] = array(3, 23, 19, 10, 2, 33, 28, 30, 7);
$subtype_patterns[12] = array(31, 15);
$subtype_patterns[13] = array(5, 25, 13, 26, 1, 22, 7);
$subtype_patterns[14] = array(16, 32);
$subtype_patterns[15] = array(24, 29, 14, 12, 18, 9, 11, 7, 34);

$subtype_patterns[2] = array();
$subtype_patterns[21] = array(3, 2, 28, 19);
$subtype_patterns[22] = array(8, 6);
$subtype_patterns[23] = array(5, 25, 13, 7);
$subtype_patterns[24] = array(24, 14, 27, 17, 9, 29, 7, 34);

$subtype_patterns[3] = array();
$subtype_patterns[31] = array();
$subtype_patterns[32] = array(3, 19, 28, 23);
$subtype_patterns[33] = array(21, 20, 22);
$subtype_patterns[34] = array();
$subtype_patterns[35] = array(4, 18, 34, 7);

$config['subtype_patterns'] = $subtype_patterns;

/*
 * item_long chinese name
 * @author howtomakeaturn
 */
$item_long_name['ns'] = '無袖';
$item_long_name['ss'] = '短袖';
$item_long_name['ls'] = '長袖';

$item_long_name['sp'] = '短褲';
$item_long_name['lp'] = '長褲';

$item_long_name['sb'] = '短版';
$item_long_name['lb'] = '長版';

$item_long_name['mini'] = '短裙';
$item_long_name['knee'] = '及膝裙';
$item_long_name['midi'] = '中長裙';
$item_long_name['maxi'] = '長裙';

$config['item_long_name'] = $item_long_name;

/* clothing subtype-item_long mapping
 * @author howtomakeaturn
 */
$subtype_item_longs[1] = array();
$subtype_item_longs[11] = array('ns', 'ss', 'ls');
$subtype_item_longs[12] = array('ns', 'ss', 'ls');
$subtype_item_longs[13] = array('sp', 'lp');
$subtype_item_longs[14] = array();
$subtype_item_longs[15] = array('sb', 'lb');

$subtype_item_longs[2] = array();
$subtype_item_longs[21] = array('ns', 'ss', 'ls');
$subtype_item_longs[22] = array('ns', 'ss', 'ls');
$subtype_item_longs[23] = array('sp', 'lp');
$subtype_item_longs[24] = array('sb', 'lb');

$subtype_item_longs[3] = array();
$subtype_item_longs[31] = array();
$subtype_item_longs[32] = array('ns', 'ss', 'ls');
$subtype_item_longs[33] = array('sp', 'lp');
$subtype_item_longs[34] = array('ns', 'ss', 'ls');
$subtype_item_longs[35] = array();

$config['subtype_item_longs'] = $subtype_item_longs;

$config['style_lists'] = [
	1 => [
		'依場合' => [
			1 => $style_name[1],
			15 => $style_name[15],
			29 => $style_name[29],
			30 => $style_name[30],
		],
		'依風格' => [
			2 => $style_name[2],
			3 => $style_name[3],
			10 => $style_name[10],
			11 => $style_name[11],
			12 => $style_name[12],
			13 => $style_name[13],
			14 => $style_name[14],
			16 => $style_name[16],
			21 => $style_name[21],
			23 => $style_name[23],
		],
		'依花色' => [
			7 => $style_name[7],
			5 => $style_name[5],
			6 => $style_name[6],
			4 => $style_name[4],
			20 => $style_name[20],
			22 => $style_name[22],
			19 => $style_name[19],
			33 => $style_name[33],
			35 => $style_name[35],
		],
		'依材質' => [
			9 => $style_name[9],
			24 => $style_name[24],
			27 => $style_name[27],
			28 => $style_name[28],
		],
		'其他' => [
			25 => $style_name[25],
			34 => $style_name[34],
			// 26 => '孕婦'
		],
	],
	2 => [
		'依場合' => [
			1 => $style_name[1],
			15 => $style_name[15],
			29 => $style_name[29],
			31 => $style_name[31],
		],
		'依風格' => [
			2 => $style_name[2],
			10 => $style_name[10],
			11 => $style_name[11],
			12 => $style_name[12],
			21 => $style_name[21],
		],
		'依花色' => [
			7 => $style_name[7],
			5 => $style_name[5],
			6 => $style_name[6],
			4 => $style_name[4],
			20 => $style_name[20],
			22 => $style_name[22],
			35 => $style_name[35],
		],
		'依材質' => [
			9 => $style_name[9],
			24 => $style_name[24],
			27 => $style_name[27],
			28 => $style_name[28],
		],
		'其他' => [
			25 => $style_name[25],
			34 => $style_name[34],
		],
	],
	3 => [
		'依場合' => [
			17 => $style_name[17],
			18 => $style_name[18],
			29 => $style_name[29],
			32 => $style_name[32],
		],
		'依風格' => [
			10 => $style_name[10],
			11 => $style_name[11],
			12 => $style_name[12],
		],
		'依花色' => [
			7 => $style_name[7],
			5 => $style_name[5],
			6 => $style_name[6],
			4 => $style_name[4],
			35 => $style_name[35],
		],
		'依材質' => [
			9 => $style_name[9],
			24 => $style_name[24],
			27 => $style_name[27],
			28 => $style_name[28],
		],
		'其他' => [
			25 => $style_name[25],
			34 => $style_name[34],
		],
	],
];