<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// credit card store number
$config['payment_credit_merchant_number'] = '758975';
// payment code for encoding
$config['payment_credit_code'] = 'dyy7ray4';
// payment credit card url
$config['payment_credit_url'] = 'https://taurus.neweb.com.tw/NewebmPP/cdcard.jsp';



// none credit card store number
$config['payment_none_credit_merchant_number'] = '457990';
// payment none credit card code for encoding
$config['payment_none_credit_code'] = '27xynhxw';
// $config['payment_none_credit_code'] = 'abcd1234';
// payment none credit card url
$config['payment_none_credit_url'] = 'https://aquarius.neweb.com.tw/CashSystemFrontEnd/Payment';
// $config['payment_none_credit_url'] = 'https://testmaple2.neweb.com.tw/CashSystemFrontEnd/Payment';




// credit card feedback array
$config['payment_credit_feedback'] = array('final_result', 'P_MerchantNumber', 'P_OrderNumber',
    'P_Amount', 'P_CheckSum', 'final_return_PRC', 'final_return_SRC', 'final_return_ApproveCode',
    'final_return_BankRC', 'final_return_BatchNumber'
);
// web atm & atm feedback array
$config['payment_atm_feedback'] = array('merchantnumber', 'ordernumber', 'serialnumber',
    'writeoffnumber', 'timepaid', 'paymenttype', 'amount', 'tel', 'hash'
);
