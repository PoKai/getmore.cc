<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| image
| -------------------------------------------------------------------------
| defined Image Manipulation
| note: the library path of ImageMagick or NetPBM
|
*/

/*
 * Please modified library_path for Windows or Linux, example:
 * Linux: /usr/bin/
 * Windows: C://ImageMagic/bin/
 */

$config['library_path'] = "/usr/local/bin";

$config['gd2'] = array(
    "image_library" => "GD2",
);

$config['ImageMagick'] = array(
    "image_library" => "ImageMagick",
    "library_path" => $config['library_path'],
);

$config['NetPBM'] = array(
    "image_library" => "NetPBM",
    "library_path" => $config['library_path'],
);

$config['image_size'] = array(
//    "banner"    => array("900x360"),
    "banner"    => array("980x410"),
    "event"     => array("420x315", "264x198", "540x385", "82x82"),
    "subject"   => array("420x315", "500x120", "264x198", "350x350"),
//    "product"   => array("370x350", "140x130", "264x198", "82x82", "250x375")
    "product"   => array("200x300", "250x375", "800x1200"),
    "profile"   => array("165x165", "300x300")
);
