<?php
class Suggest extends MY_Controller{

	private $season_list = [
        '1' => '春夏',
        '2' => '秋冬',
        '3' => '無季節'
    ];

    private $style_list = [
        '1' => '上班族', 
        '2' => '復古', 
        '3' => '浪漫', 
        '4' => '小碎花', 
        '5' => '幾何', 
        '6' => '格子', 
        '7' => '條紋', 
        '8' => '正式場合', 
        '9' => '牛仔', 
        '10' => '運動風', 
        '11' => '休閒', 
        '12' => '學院', 
        '13' => '可愛', 
        '14' => '奢華', 
        '15' => '宴會', 
        '16' => '中國風', 
        '17' => '小紳士', 
        '18' => '小淑女', 
        '19' => '動物紋', 
        '20' => '印花', 
        '21' => '個性', 
        '22' => '民俗風', 
        '23' => '性感'
    ];

    private $gender_list = [
        'type=women' => 'WOMEN',
        'type=men' => 'MEN',
        'type=kids' => 'KIDS'
    ];

    private $product_type_list = [
        'WOMEN' => [
            'product_type=11' => '上衣',
            'product_type=12' => '洋裝',
            'product_type=13&pattern=26' => '連身褲',
            'product_type=13&pattern=13' => '牛仔褲',
            'product_type=13&item_long=sp' => '短褲',
            'product_type=13&item_long=lp' => '長褲',
            'product_type=14' => '裙子',
            'product_type=151&product_type=152&product_type=153' => '外套'
        ],
        'MEN' => [
            'product_type=21'=> '上衣',
            'product_type=22'=> '襯衫',
            'product_type=23&pattern=13'=> '牛仔褲',
            'product_type=23&item_long=sp'=> '短褲',
            'product_type=23&item_long=lp'=> '長褲',
            'product_type=251&pattern=24'=> '西裝外套',
            'product_type=241&product_type=242&product_type=243'=> '外套'
        ],
        'KIDS' => [
            'product_type=31'=> '連身衣',
            'product_type=32'=> '上衣',
            'product_type=33'=> '下身',
            'product_type=34'=> '洋裝',
            'product_type=351&product_type=352'=> '外套'
        ]
    ];

    private $patterns = [
        'product_type=11' => [
            'pattern=3' => 'T恤',
            'pattern=19' => '背心',
            'pattern=23' => '襯衫',
            'pattern=2' => 'Polo 衫',
            'pattern=28' => '毛衣/針織衫',
            'pattern=10' => '娃娃裝',
            'pattern=33' => '雪紡',
            'pattern=35' => '休閒運動衫',
            'pattern=7' => '其他',
            'sub_pattern=9' => '長上衣',
            'sub_pattern=8' => '短版上衣'
        ],
        'product_type=12' => [
            'pattern=15' => '短洋裝',
            'pattern=31' => '長洋裝',
            'style=11' => '休閒洋裝',
            'style=8&style=14' => '正式場合',
            'style=1' => 'OL洋裝',
            'item_long=lp' => '長袖洋裝'
        ],
        'product_type=13&pattern=26' => [
            'item_long=sp' => '短褲', 
            'item_long=lp' => '長褲',
            'style=25' => '吊帶褲'
        ],
        'product_type=13&pattern=13' => [
            'item_long=sp' => '短褲',
            'style=25' => '吊帶褲',
            'sub_pattern=1' => '靴型',
            'sub_pattern=2' => '窄管',
            'sub_pattern=3' => '喇叭',
            'sub_pattern=4' => '直筒',
            'sub_pattern=5' => '男友褲'
        ],
        'product_type=13&item_long=sp' => [
            'pattern=13' => '牛仔褲',
            'pattern=5' => '休閒褲',
            'pattern=25' => '西裝褲',
            'pattern=26' => '連身褲',
            'pattern=22' => '褲裙',
            'style=24' => '皮褲',
            'pattern=7' => '其他'
        ],
        'product_type=13&item_long=lp' => [
            'pattern=13'=>'牛仔褲',
            'pattern=5'=>'休閒褲',
            'pattern=25'=>'西裝褲',
            'pattern=26'=>'連身褲',
            'pattern=22'=>'寬褲/褲裙',
            'pattern=1'=>'Legging',
            'style=24'=>'皮褲',
            'pattern=7'=>'其他'
        ],
        'product_type=14' => [
            'item_long=mini' => '短裙',
            'item_long=knee' => '及膝裙',
            'item_long=midi' => '中長裙',
            'item_long=maxi' => '長裙',
            'pattern=36' => 'A Line/圓裙',
            'pattern=37' => '窄裙/鉛筆裙',
            'style=9' => '牛仔裙',
            'style=24' => '皮裙',
            'style=25' => '吊帶裙'
        ],
        'product_type=151&product_type=152&product_type=153' => [
            'style=9'=>'牛仔外套',
            'pattern=24'=>'西裝外套',
            'pattern=12'=>'休閒外套(棉質外套)',
            'pattern=14'=>'皮衣',
            'pattern=29'=>'針織/毛衣外套',
            'pattern=34'=>'背心外套',
            'pattern=18'=>'羽絨衣',
            'pattern=11'=>'斗篷',
            'pattern=9'=>'大衣',
            'pattern=38'=>'風衣',
            'pattern=7'=>'其他'
        ],
        'product_type=21' => [
            'pattern=3'=> 'T-Shirt',
            'pattern=2'=> 'Polo衫',
            'psttern=35'=> '休閒運動衫',
            'pattern=28'=> '針織/毛衣',
            'pattern=19'=> '背心'
        ],
        'product_type=22' => [
            'pattern=8'=> '商務襯衫',
            'pattern=6'=> '休閒襯衫',
            'item_long=ss'=> '短袖襯衫',
            'item_long=ls'=> '長袖襯衫'
        ],
        'product_type=23&pattern=13' => [
            'item_long=sp'=> '短褲',
            'sub_pqttern1=4'=> '直筒',
            'sub_pqttern1=2'=> '窄管',
            'style=25'=> '吊帶褲'
        ],
        'product_type=23&item_long=sp' => [
            'pattern=13'=> '牛仔褲',
            'pattern=5'=> '休閒褲',
            'pattern=25'=> '西裝褲',
            'pattern=39'=> '海灘褲',
            'pattern=40'=> '運動褲',
            'pattern=7'=> '其他'
        ],
        'product_type=23&item_long=lp' => [
            'pattern=13'=> '牛仔褲',
            'pattern=5'=> '休閒褲',
            'pattern=25'=> '西裝褲',
            'pattern=40'=> '運動褲',
            'pattern=7'=> '其他'
        ],
        'product_type=251&pattern=24' => [
            'style=1&style=8&style=15'=> '正式場合',
            'style=11'=> '休閒西裝外套'
        ],
        'product_type=241&product_type=242&product_type=243' => [
            'style=9'=> '牛仔外套',
            'pattern=24'=> '西裝外套',
            'pattern=12'=> '休閒外套(棉質外套)',
            'pattern=14'=> '皮衣',
            'pattern=29'=> '針織/毛衣外套',
            'pattern=34'=> '背心外套',
            'pattern=17'=> '羽絨衣',
            'pattern=9'=> '大衣',
            'pattern=38'=> '風衣',
            'pattern=7'=> '其他'
        ],
        'product_type=32' => [
            'pattern=19'=> '背心',  
            'pattern=3'=> 'T-Shirt',
            'pattern=23'=> '襯衫',
            'pattern=28'=> '針織/毛衣'
        ],
        'product_type=33' => [
            'pattern=13'=> '牛仔褲',  
            'pattern=21&item_long=sp'=> '短褲',
            'pattern=21&item_long=lp'=> '長褲',
            'pattern=22'=> '褲裙',
            'pattern=20'=> '裙子'
        ],
        'product_type=351&product_type=352' => [
            'pattern=4'=> '夾克(一般外套)',
            'pattern=18'=> '羽絨衣',
            'pattern=34'=> '背心外套',
            'pattern=29'=> '針織/毛衣外套',
            'pattern=38'=> '風衣',
            'pattern=7'=> '其他'
        ]
    ];

	public function __construct(){
		parent::__construct();
		$this->load->spark('Twiggy/0.8.5');
		$this->twiggy->theme('');
        $this->twiggy->set(array('lang' => 'zh-tw', 'meta_charset' => 'UTF-8', 'site_title' => 'GetMore - 二手衣時尚購物網站/綠色時尚/買二手衣/賣二手衣', 'site_description' => 'GetMore 是亞洲第一個二手衣時尚買賣網站，提供穿二手衣的生活新理念，輕鬆享受二手衣獨特、環保、時尚的二次生活。', 'site_keywords' => '時尚,衣著,服飾,時裝,流行,褲子,買二手衣,賣二手衣,環保,電子商務,startup,二次時尚', 'logined' => $this->ion_auth->logged_in(),), NULL, TRUE);
	}


	public function index($query){
	}

	public function getSuggest($suggestName) {
        $this->config->load('clothing_class');

		$this->twiggy->set([
            'season_list' => $this->season_list,
            'style_list' =>  array_reduce($this->config->item('style_lists')[1], function ($result, $style_list) {
                $result += $style_list;
                return $result;
            }, []),
            'gender_list' => $this->gender_list,
            'product_type_list' => $this->product_type_list,
            'patterns' => $this->patterns,
            'sub_patterns' => $this->sub_patterns,
            'url' => $suggestName,
            'user_id' => $this->session->userdata('user_id'),
        ])->template('suggest/newcate')->display();

		/*$this->twiggy->template('suggest/suggest')->set(array(
            'data' => $data,
            'title' => $pageData->name
        ), null)->display();*/
	}
}