<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Message extends MY_Controller{
    protected $data = array();

    public function __construct() {
        parent::__construct();
        $this->load->library(array('lib_static_url', 'lib_subject', 'lib_config', 'lib_event', 'files_lib', 'ion_auth'));
        $this->load->library(array('form_validation'));
        $this->load->config('clothing_class');
        
        $this->load->spark('curl');
        $this->curl->option(CURLOPT_COOKIE, http_build_query($_COOKIE, '', ';'));
        
        $this->load->spark('Twiggy/0.8.5');
        $this->twiggy->theme('');
        
        $this->twiggy->set(array('lang' => 'zh-tw', 'meta_charset' => 'UTF-8', 'site_title' => 'Getmore - 二手衣時尚購物網站/綠色時尚/買二手衣/賣二手衣', 'site_description' => 'GetMore 是亞洲第一個二手衣時尚買賣網站，提供穿二手衣的生活新理念，輕鬆享受二手衣獨特、環保、時尚的二次生活。', 'site_keywords' => '時尚,衣著,服飾,時裝,流行,褲子,買二手衣,賣二手衣,環保,電子商務,startup,二次時尚', 'logined' => $this->ion_auth->logged_in(),), NULL, TRUE);
    }

    // function index(){
    //     if (!$this->ion_auth->logged_in()) {
    //         $this->system->redirect();
    //     }


    //     // $this->template->render('message/message', $this->data);

    // }
    function fin($select){
        if (!$this->ion_auth->logged_in()) {
            $this->system->redirect();
        }
        $this->twiggy->register_function('post_messages');
        $this->twiggy->register_function('get_post');
        $this->twiggy->register_function('post_message');
        $this->twiggy->register_function('count_notReadPost');
        $this->twiggy->register_function('post_message');
        
        $this->twiggy->set('select', $select);
        $this->twiggy->template('message/message')->display();        
    }    
}