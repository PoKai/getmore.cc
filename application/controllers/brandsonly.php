<?php
class Brandsonly extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->spark('Twiggy/0.8.5');
		$this->twiggy->theme('');
		$this->twiggy->set(array('lang' => 'zh-tw', 'meta_charset' => 'UTF-8', 'site_title' => 'Getmore 專注在「時尚」社群的購物網站 - 原創新銳設計/時尚配件/時尚買手/二手精品/二手包/品牌二手代售', 'site_description' => 'Getmore 是專注在「時尚」社群的購物網站。聚集新銳設計師、買手、時尚愛好者的購物網站，供應獨特單品以及二手時尚的時尚購物平台。時尚很多元，就像是一個個精彩的小圈圈，Getmore 負責把時尚圈在一起，你也可以稱這裡是「時尚小圈圈」。', 'site_keywords' => '時尚,衣著,服飾,時裝,二手衣,流行,新銳設計師,亞洲時尚,褲子,買二手衣,賣二手衣,環保,電子商務,startup,二次時尚', 'logined' => $this->ion_auth->logged_in()), NULL, TRUE);
	}
	public function index() {
		$this->load->model('Product_clothing_model');
		$query = $this->Product_clothing_model->get_brand_list();
		$data = array('brands' => $query);

		$this->twiggy->set($data);
		$this->twiggy->template('brandsonly/brandsonly')->display();
	}
	public function sellchoice() {
		$this->twiggy->template('brandsonly/sellchoice')->display();
	}
	public function startsell() {
		$this->twiggy->template('brandsonly/startsell')->display();
	}
}