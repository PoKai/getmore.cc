<?php
if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Shop extends MY_Controller {

	/**
	 * Default __construct
	 *
	 * @author appleboy
	 *
	 */

	public function __construct() {
		parent::__construct();
		$this->load->library(array('lib_static_url', 'lib_subject', 'lib_config', 'lib_event', 'files_lib', 'ion_auth', 'user_agent'));
		$this->load->library(array('form_validation'));
		$this->load->config('clothing_class');

		$this->load->spark('curl');
		$this->curl->option(CURLOPT_COOKIE, http_build_query($_COOKIE, '', ';'));
		$this->load->spark('Twiggy/0.8.5');
		$this->twiggy->theme('');
		$this->twiggy->set(array('lang' => 'zh-tw', 'meta_charset' => 'UTF-8', 'site_title' => 'Getmore 專注在「時尚」社群的購物網站 - 原創新銳設計/時尚配件/時尚買手/二手精品/二手包/品牌二手代售', 'site_description' => 'Getmore 是專注在「時尚」社群的購物網站。聚集新銳設計師、買手、時尚愛好者的購物網站，供應獨特單品以及二手時尚的時尚購物平台。時尚很多元，就像是一個個精彩的小圈圈，Getmore 負責把時尚圈在一起，你也可以稱這裡是「時尚小圈圈」。', 'site_keywords' => '時尚,衣著,服飾,時裝,二手衣,流行,新銳設計師,亞洲時尚,褲子,買二手衣,賣二手衣,環保,電子商務,startup,二次時尚', 'logined' => $this->ion_auth->logged_in()), NULL, TRUE);
	}

	public function buyer($url_code = '') {
		$this->load->model('Clothing_model');
		$buyer = (array) json_decode($this->curl->simple_get(base_url('/restful/buyer/' . $url_code), array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));
		$buyer = (array) $buyer[0];
		$buyer_id = $buyer['user_id'];
		if (empty($url_code) || is_null($buyer_id)) {
			redirect('/', 'refresh');
		}
		$query = array(
			'user_id' => $buyer_id,
		);

		$items = (array) json_decode($this->curl->simple_get(base_url('/restful/buyer_products'), $query)); //var_dump($items);exit(0);

		foreach ($items as $item) {
			$item->is_liked = $this->Clothing_model->_item_is_liked($item->id, $item->launch_way);
		}

		$designer['banner_pic_url'] = user_banner_url($buyer_id); //var_dump(user_banner_url($designer['user_id']));exit(0);
		$designer['profile_pic_url'] = user_photo_url($buyer_id);
		$product['trade_rule'] = (array) json_decode($this->curl->simple_get(base_url("/restful/trade/" . $buyer_id), array(), array(CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'))));

		$user_id = $this->session->userdata('user_id');
		$followList = (array) json_decode($this->curl->simple_get(base_url("/restful/follow_designer/" . $user_id), array(), array(CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'))));

		$follow = false;
		if (!empty($user_id)) {
			foreach ($followList as $key => $value) {
				if ($buyer_id == $value->followed) {
					$follow = true;
					break;
				}
			}
		}
		$trade = (array) json_decode($this->curl->simple_get('/restful/trade', array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));
		$this->twiggy->template('shop/buyer')->set(array(
			'url_code' => $url_code,
			'items' => $items,
			'buyer' => $buyer,
			'buyer_id' => $buyer_id,
			'product' => $product,
			'follow' => $follow,
			"user_id" => $user_id,
			"trade" => $trade,
		))->display();
	}

	public function designer($url_code = '') {

		$this->load->model('Clothing_model');
		$designer = (array) json_decode($this->curl->simple_get(base_url('/restful/designer/' . $url_code), array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));
		$designer = (array) $designer[0];
		$designer_id = $designer['user_id'];
		if (empty($url_code) || is_null($designer_id)) {
			redirect('/', 'refresh');
		}
		$query = array(
			'user_id' => $designer_id,
		);

		$items = (array) json_decode($this->curl->simple_get(base_url('/restful/designer_products'), $query)); //var_dump($items);exit(0);

		foreach ($items as $item) {
			$item->is_liked = $this->Clothing_model->_item_is_liked($item->id, $item->launch_way);
		}

		$designer['banner_pic_url'] = user_banner_url($designer_id); //var_dump(user_banner_url($designer['user_id']));exit(0);
		$designer['profile_pic_url'] = user_photo_url($designer_id);
		$product['trade_rule'] = (array) json_decode($this->curl->simple_get(base_url("/restful/trade/" . $designer_id), array(), array(CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'))));

		$user_id = $this->session->userdata('user_id');
		$followList = (array) json_decode($this->curl->simple_get(base_url("/restful/follow_designer/" . $user_id), array(), array(CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'))));

		$follow = false;
		if (!empty($user_id)) {
			foreach ($followList as $key => $value) {
				if ($designer_id == $value->followed) {
					$follow = true;
					break;
				}
			}
		}
		$trade = (array) json_decode($this->curl->simple_get('/restful/trade', array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));
		$this->twiggy->template('shop/designer')->set(array(
			'url_code' => $url_code,
			'items' => $items,
			'designer' => $designer,
			'designer_id' => $designer_id,
			'product' => $product,
			'follow' => $follow,
			"user_id" => $user_id,
			"trade" => $trade,
		))->display();
	}

	public function disfollow() {
		$disfollow_type = $this->input->post('disfollow_type');
		$disfollow_id = $this->input->post('user_id_disfollow');
		//var_dump($disfollow_type);var_dump($disfollow_id);exit(0);
		$this->db
			->where('followed', $disfollow_id)
			->delete('follow_' . $disfollow_type);
		redirect($this->agent->referrer(), 'refresh');
	}
}