<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Launch extends MY_Controller
{

    /**
     * new subject or event value
     *
     * @var int
     **/
    private $_news_count = 4;

    /**
     * sale subject or event value, order by sale time
     *
     * @var int
     **/
    private $_sales_count = 9;

    /**
     * Default __construct
     *
     * @author appleboy
     **/
    public function __construct()
    {
        parent::__construct();
        //$this->load->library(array('ion_auth', 'session', 'form_validation', 'lib_cart', 'lib_event', 'lib_product', 'lib_order'));        
        //$this->load->library(array('lib_static_url', 'lib_subject', 'lib_config', 'lib_event', 'files_lib'));
        $this->load->library(array('form_validation'));
        $this->template->set('is_home', true);
    }

    /**
     * Default home page
     *
     * @author howtomakeaturn
     **/
    public function index()
    {
        if ($this->ion_auth->logged_in()) {
          $this->load->view('launch/index2');         
        }
        else{
          $this->data['reg_message'] = $this->session->flashdata('reg_message');
          $this->load->view('launch/index', $this->data);          
        }
        //$this->template->render('launch/index', $data);
    }

    /**
     * Quick register for launch page
     *
     * @author howtomakeaturn
     **/
    public function quick_register(){
        $this->form_validation->set_rules('email', '電子郵件', 'required|valid_email');
        $this->form_validation->set_rules('password', '密碼', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']');
        $redirect_url = urldecode($this->input->post('redirect_url'));

        if ($this->form_validation->run() == true) {
            $username = '';
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $additional_data = array(
//                'user_name' => $this->input->post('user_name')
                'user_name' => "GetMore會員"
            );
        }
        if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data) !== FALSE) { //check to see if we are creating the user
            //redirect them back to the admin page
            redirect($redirect_url, 'refresh');
        } else { //display the create user form
            //set the flash data error message if there is one
            $this->data['reg_message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            $this->session->set_flashdata('reg_message',  $this->data['reg_message']);
            redirect($redirect_url, 'refresh');
        }
    }
    

    /**
     * Default Static Page
     *
     * @author appleboy
     **/
    public function url($name = null)
    {
        $this->template->set('is_hidden_header', true);
        if (empty($name)) {
            redirect('/', 'refresh');
        }

        $row = $this->lib_static_url->select('id, title, url, content')->where('url', $name)->items()->row_array();

        if (empty($row) or empty($row['content'])) {
            redirect('/', 'refresh');
        }

        $this->template->render('home/static', $row);
    }

}
