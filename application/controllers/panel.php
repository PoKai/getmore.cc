<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Panel extends MY_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->library(array('ion_auth', 'user_agent'));
        $this->load->model('User_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session'));
        $this->load->spark(array('Twiggy/0.8.5', 'curl'));

        $this->twiggy->theme('panel')->set(array('title' => 'panel'));
        $this->curl->option(CURLOPT_COOKIE, http_build_query($_COOKIE,'',';'));

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_seller()) {
            $this->system->redirect();
        }
    }

    public function index(){
        $this->order(2);
    }

    public function order($status = 2){
        $sub_orders = json_decode($this->curl->simple_get('/restful/sub_order/status/'.$status));
        
        if($status == 2){
            $this->twiggy->set(array('changeStatus' => true));
        }
        else {
            $this->twiggy->set(array('changeStatus' => false));
        }
        
        $this->twiggy->template('orders')->set(array('sub_orders' => $sub_orders))->display();
    }

    public function clothings(){
        $clothings = json_decode($this->curl->simple_get('/restful/product_clothing/myclothings'));
        $count = 5; /*limit error count*/
        while ($this->curl->error_code && $count--) {
            $clothings = json_decode($this->curl->simple_get('/restful/product_clothing/myclothings'));
        }
        $this->twiggy->template('clothings')->set(array('clothings' => $clothings))->display();
    }
}
