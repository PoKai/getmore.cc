<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sellorder extends MY_Controller
{
    /**
     * User id
     *
     * @var int
     **/
    public $user_id = NULL;

    /**
     * __construct
     *
     * @return void
     * @author appleboy
     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->spark('curl/1.2.1');
        $this->load->library(array('ion_auth', 'lib_event', 'lib_product', 'lib_sellorder', 'lib_config', 'lib_sellcart', 'files_lib'));
        $this->load->config('payment');
        $this->template->set('is_hidden_header', true);

        // get current user data
        $this->user_id = $this->session->userdata('user_id');
        $this->default_address = array(
            'county' => '',
            'district' => '',
            'zipcode' => '',
            'address' => ''
        );

        $this->load->library('zend');
        $this->zend->load('Zend/Barcode');

    }

    /**
     * index
     *
     * @author appleboy
     **/
    public function index()
    {
        redirect('/');
    }

    /**
     * Selling steps
     *
     * @author popchieh
     **/
    public function steps($action)
    {
        $data = $counts = $events = $products = $events_id = $products_id = array();

        // get user carts
        $carts = $this->lib_sellcart->contents();
        // if carts is empty, redirect to home page
        // if (empty($carts)) {
        //     redirect('/', 'refresh');
        // }

        // steps 1,2,3,4
        switch ($action) {
            case '1':
                // get user order list
                $data = $this->lib_sellorder->user_order();
                if ($data['total_price'] == 0) {
                    $data['message'] = $this->system->message('您尚未購買任何商品!!', 'error');
                } elseif ($data['is_status_change']) {
                    $data['message'] = $this->system->message('您購買商品或活動有變動!!請更新或移除', 'error');
                }
                $this->session->set_userdata("step-1", "step-1");
                break;
            case "2":
                if (!$this->session->userdata("step-1")) {
                    redirect("/sellorder/steps/1");
                }

                // delete session data from step-1
                $this->session->unset_userdata("step-1");

                // get user data
                $user_data = $this->ion_auth->user($this->user_id)->row_array();
                $user_data['p_address'] = (!empty($user_data['p_address'])) ? json_decode($user_data['p_address'], true) : $this->default_address;
                $user_data['s_address'] = (!empty($user_data['s_address'])) ? json_decode($user_data['s_address'], true) : $this->default_address;
          
                $steps2 = array(
                    "profile"           => $user_data
                );

                // get user order list and merge it.
                $data = array_merge($this->lib_sellorder->user_order(), $steps2);

                $this->session->set_userdata("total_price", $this->input->get_post('total_price'));

                // check total price
                if ($data['total_price'] == 0) {
                    $this->system->message('您尚未購買任何商品!!', 'error');
                    redirect('/sellorder/steps/1', 'refresh');
                } elseif ($data['is_status_change']) {
                    $this->system->message('您購買商品或活動有變動!!請更新或移除', 'error');
                    redirect('/sellorder/steps/1', 'refresh');
                }

                $this->session->set_userdata("step-2", "step-2");
                break;
            case "3":
                if (!$this->session->userdata("step-2")) {
                    redirect("/sellorder/steps/2");
                }

                // delete session data from step-2
                $this->session->unset_userdata("step-2");

                // get user order list
                $data = $this->lib_sellorder->user_order();

                // check total price
                if ($data['total_price'] == 0) {
                    $this->system->message('您尚未購買任何商品!!', 'error');
                    redirect('/sellorder/steps/1', 'refresh');
                } elseif ($data['is_status_change']) {
                    $this->system->message('您購買商品或活動有變動!!請更新或移除', 'error');
                    redirect('/sellorder/steps/1', 'refresh');
                }

                $total_price = $data['total_price'];

                // start to add order
                $delivery_address_str = $this->input->get_post('delivery_zipcode') . ' ' . $this->input->get_post('delivery_county') . $this->input->get_post('delivery_district') . $this->input->get_post('delivery_address');
                $user_address_str = $this->input->get_post('user_zipcode') . ' ' . $this->input->get_post('user_county') . $this->input->get_post('user_district') . $this->input->get_post('user_address');
                $delivery_address_json = array(
                    'county' => $this->input->post('delivery_county'),
                    'district' => $this->input->post('delivery_district'),
                    'zipcode' => $this->input->post('delivery_zipcode'),
                    'address' => $this->input->post('delivery_address')
                );  
                $user_address_json = array(
                    'county' => $this->input->post('user_county'),
                    'district' => $this->input->post('user_district'),
                    'zipcode' => $this->input->post('user_zipcode'),
                    'address' => $this->input->post('user_address')
                );
                // add order
                $formdata = array(
                    "user_id"                   => $this->user_id,
                    "user_name"                 => $this->input->get_post('user_name') ? $this->input->get_post('user_name') : NULL,
                    "user_sex"                  => $this->input->get_post('user_sex') ? $this->input->get_post('user_sex') : NULL,
                    "user_mobile_phone"         => $this->input->get_post('user_mobile_phone') ? $this->input->get_post('user_mobile_phone') : NULL,
                    "user_telephone"             => $this->input->get_post('user_telephone') ? $this->input->get_post('user_telephone') : NULL,
                    "user_address"              => $user_address_str,
                    "delivery_name"             => $this->input->get_post('delivery_name') ? $this->input->get_post('delivery_name') : NULL,
                    "delivery_sex"              => $this->input->get_post('delivery_sex') ? $this->input->get_post('delivery_sex') : NULL,
                    "delivery_mobile_phone"     => $this->input->get_post('delivery_mobile_phone') ? $this->input->get_post('delivery_mobile_phone') : NULL,
                    "delivery_telephone"         => $this->input->get_post('delivery_telephone') ? $this->input->get_post('delivery_telephone') : NULL,
                    "delivery_address"          => $delivery_address_str,
                    "uniform_invoice_title"     => $this->input->get_post('uniform_invoice_title') ? $this->input->get_post('uniform_invoice_title') : NULL,
                    "uniform_invoice_number"    => $this->input->get_post('uniform_invoice_number') ? $this->input->get_post('uniform_invoice_number') : NULL,
                    "uniform_invoice_type"      => $this->input->get_post('uniform_invoice_type') ? $this->input->get_post('uniform_invoice_type') : NULL,
                    "order_number"              => $this->generate_order_number(),
                    // "total_price"               => $this->session->userdata('total_price'),
                    "total_price"               => $total_price,
                    "date_purchased"            => date("Y-m-d H:i:s"),
                    // "payment_method"            => $this->input->get_post('payment_method'),
                    "status_id"                 => ORDER_PAYMENT,
                    "no_return"                 => $this->input->get_post("no_return") ? $this->input->get_post('no_return') : 0
                );

                // save data in session
                $this->session->set_userdata("formdata", $formdata);

                // update user data
                $userdata = array(
                    "user_name"     => $this->input->get_post('user_name') ? $this->input->get_post('user_name') : NULL,
                    "sex"           => $this->input->get_post('user_sex') ? $this->input->get_post('user_sex') : NULL,
                    "mobile_phone"  => $this->input->get_post('user_mobile_phone') ? $this->input->get_post('user_mobile_phone') : NULL,
                    "telephone"      => $this->input->get_post('user_telephone') ? $this->input->get_post('user_telephone') : NULL,
                    "p_address"     => json_encode($user_address_json),
                    "s_address"     => json_encode($delivery_address_json)
                );
                $this->ion_auth->update_user($this->user_id, $userdata);

                $this->session->set_userdata("step-3", "step-3");
                break;
            case "4":
                if (!$this->session->userdata("step-3") or !$this->input->get_post("payment_method") or !$this->session->userdata("formdata")) {
                    redirect("/sellorder/steps/3");
                }

                // get user order list
                $data = $this->lib_sellorder->user_order();

                // check total price
                if ($data['total_price'] == 0) {
                    $this->system->message('您尚未購買任何商品!!', 'error');
                    redirect('/sellorder/steps/1', 'refresh');
                } elseif ($data['is_status_change']) {
                    $this->system->message('您購買商品或活動有變動!!請更新或移除', 'error');
                    redirect('/sellorder/steps/1', 'refresh');
                }

                // $order = $data;

                $formdata = $this->session->userdata("formdata");

                $steps4 = array(
                    "payment_method"    => $this->input->get_post("payment_method")
                );

                // get user order list and merge it.
                $formdata = array_merge($formdata, $steps4);

                // return order id
                $order_id = $this->lib_sellorder->add_order($formdata);

                // add order event and product to database and update event and product counts
                $data = $this->lib_sellorder->user_order('4', $order_id, $formdata['order_number']);

                // update total price
                $this->lib_sellorder->update_order($order_id, array('total_price' => $data['total_price'], 'transportation_charge' => $data['transportation_charge']));

                // delete session data from step-3
                $this->session->unset_userdata("step-3");

                // delete carts database
                // $this->lib_sellcart->delete_cart(array('user_id' => $this->user_id));

                // delete carts session
                $this->session->unset_userdata('carts_time');
                $this->session->unset_userdata('carts');

                // payment method is ATM
                // if ($this->input->get_post('payment_method') == PAYMENT_ATM) {
                if ($formdata['payment_method'] == PAYMENT_ATM) {

                    // get total price and checksum
                    $total_price = (int) ($data['total_price']);
                    $checksum = md5($this->config->item('payment_none_credit_merchant_number') . $this->config->item('payment_none_credit_code') . $total_price . $formdata['order_number']);

                    $data = array(
                        'merchantnumber' => $this->config->item('payment_none_credit_merchant_number'),
                        'ordernumber' => $formdata['order_number'],
                        'amount' => $total_price,
                        'Paymenttype' => 'ATM',
                        'hash' => $checksum,
                        'bankid' => '007',
                        'returnvalue' => 1
                    );

                    // add payment for ATM
                    $return = $this->curl->simple_post($this->config->item('payment_none_credit_url'), $data, array('SSLVERSION' => 3, 'SSL_VERIFYPEER' => false));
                    parse_str($return, $response);

                    // update bank id and virtual account
                    if (isset($response['rc']) and $response['rc'] == '0') {
                        $this->lib_sellorder->update_order($order_id, array('bank_id' => $response['bankid'], 'virtual_account' => $response['virtualaccount']));
                    }
                } else {
                    // redirect to payment page
                    $this->pay($formdata['order_number']);
                }

                break;
        }

        $template = 'sellorder/step-' . $action;
        $this->template->render($template, $data);
    }

    /**
     * Order list
     *
     * @param  int
     * @author appleboy
     **/
    public function lists($id = '')
    {
        if (!$this->ion_auth->logged_in()) {
            $this->system->redirect();
        }

        $data = $outputs = array();

        $order = $this->lib_sellorder->select('orders_status.name as orders_status, sellorders.*')->where('user_id', $this->user_id)->order_by('add_time', 'desc')->orders()->result_array();

        foreach ($order as $row) {
            $data = array(
                'order_id'          => $row['id'],
                'order_number'      => $row['order_number'],
                'date_purchased'    => date('Y-m-d', strtotime($row['date_purchased'])),
                'payment_method'    => $this->lib_sellorder->payment_method($row['payment_method']),
                'total_price'       => $this->lib_sellcart->format_number($row['total_price']),
                'status'            => $row['orders_status'],
                'pay'               => ($row['status_id'] == ORDER_PAYMENT) ? true : false
            );

            $outputs[] = $data;
        }

        $data = array(
            'outputs' => $outputs
        );

        $this->template->render("sellorder/list", $data);
    }

    /**
     * Single Order
     *
     * @param  string
     * @return json
     * @author appleboy
     **/
    public function single($order_number = '')
    {
        $order = $event = $product = $payment_data = array();

        if (empty($order_number)) {
            redirect('/sellorder/lists', 'refresh');
        }

        // get order
        $order = $this->lib_sellorder->select('sellorders.*')->where('user_id', $this->user_id)->where('sellorders.order_number', $order_number)->orders()->row_array();

        if (empty($order)) {
            redirect('/sellorder/lists', 'refresh');
        }

        $event = $this->lib_sellorder->select('*')->where('order_id', $order['id'])->orders('event')->result_array();
        $product = $this->lib_sellorder->select('*')->where('order_id', $order['id'])->orders('product')->result_array();

        if ($order['status_id'] == ORDER_COMPLETE) {
            $message = $this->system->message('此訂單付款成功!! 付款時間: ' . $order['date_finished'], 'success', false);
            $show_pay_button = false;
        } elseif ($order['payment_method'] == PAYMENT_WEB_ATM and !empty($order['lock_time']) and (($order['lock_time'] + 60*15) > $this->_time)) {
            $message = $this->system->message('此交易正由系統處理中，暫時無法提供交易進度查詢，請於15分鐘後重新確認', 'error', false);
            $show_pay_button = false;
        } elseif ($order['status_id'] == ORDER_EXPIRED) {
            $message = $this->system->message('超過繳費期限，訂單已取消', 'error', false);
            $show_pay_button = false;
        } elseif ($order['status_id'] == ORDER_CANCEL) {
            $message = $this->system->message('訂單已取消', 'error', false);
            $show_pay_button = false;
        } else {
            $message = $this->system->message('此訂單尚未付款', 'error', false);
            $show_pay_button = true;
        }

        if ($order['payment_method'] == PAYMENT_ATM) {
            $show_pay_button = false;
            $order['bank_id'] = (!empty($order['bank_id'])) ? $order['bank_id'] : '';
            $order['virtual_account'] = (!empty($order['virtual_account'])) ? $order['virtual_account'] : '';
        }

        $data = array(
            'order'             => $order,
            // 'event'             => $event,
            'product'           => $product,
            'total_sell_price'  => 0,
            // 'total_event_price' => 0,
            'payment_method'    => $this->lib_sellorder->payment_method($order['payment_method']),
            'uniform_invoice'   => $this->lib_sellorder->uniform_invoice($order),
            'show_pay_button'   => $show_pay_button,
            'status'            => $message
        );

        $this->template->render("sellorder/view", $data);
    }

   /**
     * Print Order
     *
     * @param  string
     * @return json
     * @author popchieh
     **/
    public function prints($order_number = '')
    {
        $this->template->set_layout('template/print');

        $order = $event = $product = $payment_data = array();
        $total_sell_price = $total_original_price = $count_small = $count_medium = $count_big = 0;
 
        if (empty($order_number)) {
            redirect('/sellorder/lists', 'refresh');
        }

        // get order
        $order = $this->lib_sellorder->select('sellorders.*')->where('user_id', $this->user_id)->where('sellorders.order_number', $order_number)->orders()->row_array();

        if (empty($order)) {
            redirect('/sellorder/lists', 'refresh');
        }

        // $event = $this->lib_sellorder->select('*')->where('order_id', $order['id'])->orders('event')->result_array();
        $product = $this->lib_sellorder->select('*')->where('order_id', $order['id'])->orders('product')->result_array();

        // if ($order['status_id'] == ORDER_COMPLETE) {
        //     $message = $this->system->message('此訂單付款成功!! 付款時間: ' . $order['date_finished'], 'success', false);
        //     $show_pay_button = false;
        // } elseif ($order['payment_method'] == PAYMENT_WEB_ATM and !empty($order['lock_time']) and (($order['lock_time'] + 60*15) > $this->_time)) {
        //     $message = $this->system->message('此交易正由系統處理中，暫時無法提供交易進度查詢，請於15分鐘後重新確認', 'error', false);
        //     $show_pay_button = false;
        // } elseif ($order['status_id'] == ORDER_EXPIRED) {
        //     $message = $this->system->message('超過繳費期限，訂單已取消', 'error', false);
        //     $show_pay_button = false;
        // } elseif ($order['status_id'] == ORDER_CANCEL) {
        //     $message = $this->system->message('訂單已取消', 'error', false);
        //     $show_pay_button = false;
        // } else {
        //     $message = $this->system->message('此訂單尚未付款', 'error', false);
        //     $show_pay_button = true;
        // }

        if ($order['payment_method'] == PAYMENT_ATM) {
            $show_pay_button = false;
            $order['bank_id'] = (!empty($order['bank_id'])) ? $order['bank_id'] : '';
            $order['virtual_account'] = (!empty($order['virtual_account'])) ? $order['virtual_account'] : '';
        }

        // get shipping_price
        $data = $this->lib_sellorder->user_order();

        foreach($product as $row):
            if($row['shipping_price'] == $data['shipping_price_small'])
                $count_small++;
            elseif($row['shipping_price'] == $data['shipping_price_medium'])
                $count_medium++;
            elseif($row['shipping_price'] == $data['shipping_price_big'])
                $count_big++;
        endforeach;

        $data = array(
            'order'             => $order,
            // 'event'             => $event,
            'product'           => $product,
            'total_sell_price'  => 0,
            // 'total_event_price' => 0,
            'payment_method'    => $this->lib_sellorder->payment_method($order['payment_method']),
            'uniform_invoice'   => $this->lib_sellorder->uniform_invoice($order),
            'show_pay_button'   => $show_pay_button,
            'status'            => $message,
            'count_small'       => $count_small,
            'count_medium'      => $count_medium,
            'count_big'         => $count_big
        );

        $this->template->render("sellorder/prints", $data);
    }

    /**
     * Order Payment
     *
     * @param  string
     * @return view
     */
    public function pay($order_number = '')
    {

        if (empty($order_number)) {
            $this->system->message('無此訂單，操作錯誤!!', 'error');
            redirect('/sellorder/lists', 'refresh');
        }

        // get order
        $order = $this->lib_sellorder->select('sellorders.*')->where('sellorders.order_number', $order_number)->orders()->row_array();

        // validate order data
        $this->_validate($order);

        $is_pay = ($order['status_id'] == ORDER_PAYMENT) ? true : false;

        // credit card or web atm
        if ($is_pay and $order['payment_method'] == PAYMENT_CREDIT_CARD) {
            $total_price = (int) ($order['total_price']);
            $checksum = md5($this->config->item('payment_credit_merchant_number') . $order['order_number'] . $this->config->item('payment_credit_code') . $total_price);

            $data = array(
                'MerchantNumber'    => $this->config->item('payment_credit_merchant_number'),
                'OrderNumber'       => $order['order_number'],
                'Amount'            => (int) ($order['total_price']),
                'OrgOrderNumber'    => $order['order_number'],
                'ApproveFlag'       => 1,
                'DepositFlag'       => 0,
                'Englishmode'       => 0,
                'iphonepage'        => 0,
                'OrderURL'          => site_url('/sellorder/receive'),
                'ReturnURL'         => site_url('/sellorder/receive'),
                'checksum'          => $checksum,
                'op'                => 'AcceptPayment',
                'payment_url'       => $this->config->item('payment_credit_url')
            );

            $this->template->set_layout("template/empty", $data);
            $this->template->render("sellorder/payment/credit_card", $data);

        } elseif ($is_pay and $order['payment_method'] == PAYMENT_WEB_ATM) {

            // check excute time out
            if (!empty($order['lock_time']) and (($order['lock_time'] + 60*15) > $this->_time)) {
                $this->system->message('此訂單正在處理中，請於15分鐘後再操作', 'error');
                redirect('/sellorder/lists', 'refresh');
            }

            // regenerate new order number
            // $order['order_number'] = $this->generate_order_number();
            // update order number
            // $this->lib_sellorder->update_order($order['id'], array('order_number' => $order['order_number'], 'lock_time' => $this->_time));

            $total_price = (int) ($order['total_price']);
            $checksum = md5($this->config->item('payment_none_credit_merchant_number') . $this->config->item('payment_none_credit_code') . $total_price . $order['order_number']);

            $data = array(
                'merchantnumber'    => $this->config->item('payment_none_credit_merchant_number'),
                'ordernumber'       => $order['order_number'],
                'amount'            => (int) ($order['total_price']),
                'paymenttype'       => 'WEBATM',
                'bankid'            => '007',
                'hash'              => $checksum,
                'nexturl'           => site_url('/sellorder/steps/4'),
                'payment_url'       => $this->config->item('payment_none_credit_url')
            );

            $this->template->set_layout("template/empty", $data);
            $this->template->render("sellorder/payment/webatm", $data);
        } else {
            $this->system->message('操作錯誤!!', 'error');
            redirect('/sellorder/lists', 'refresh');
        }
    }

    /**
     * Order Payment Receive (ATM & WebATM)
     *
     * get payment feedback
     *
     * @return void
     */
    public function writeoff()
    {
        $payment_code = $this->config->item('payment_none_credit_code');
        $payment_feedback = $this->config->item('payment_atm_feedback');

        foreach ($payment_feedback as $row) {
            $$row = $this->input->post($row, false, '');
        }

        // verify hash value
        $verify = array(
            'merchantnumber' => $merchantnumber,
            'ordernumber' => $ordernumber,
            'serialnumber' => $serialnumber,
            'writeoffnumber' => $writeoffnumber,
            'timepaid' => $timepaid,
            'paymenttype' => $paymenttype,
            'amount' => $amount,
            'tel' => $tel
        );

        $verify = md5(http_build_query($verify) . $payment_code);

        if ($hash != $verify) {
            return;
        }

        // get pay order time
        preg_match('/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/', $timepaid, $time);

        $data = array(
            'status_id' => ORDER_COMPLETE,
            'date_finished' => sprintf('%s-%s-%s %s:%s:%s',
                $time[1], $time[2], $time[3], $time[4], $time[5], $time[6])
        );

        // update order status
        $this->db->set($data)->where('orders.order_number', $ordernumber)->update('orders');
    }

    /**
     * Order Payment Receive (Credit Card)
     *
     * get payment feedback
     *
     * @return void
     */
    public function receive()
    {
        $order = null;
        $payment_feedback = $this->config->item('payment_credit_feedback');

        foreach ($payment_feedback as $row) {
            $$row = $this->input->post($row);
        }

        // get order
        if (isset($P_OrderNumber) and !empty($P_OrderNumber)) {
            $order = $this->lib_sellorder->select('sellorders.*')->where('sellorders.order_number', $P_OrderNumber)->orders()->row_array();
        }

        // validate order data
        $this->_validate($order, false);

        // successs
        if ($final_result == '1' and $final_return_PRC  == '0' and $final_return_SRC == '0') {
            if (!empty($P_CheckSum)) {
                $checkstr = md5($P_MerchantNumber . $P_OrderNumber . $final_result . $final_return_PRC . $this->config->item('payment_credit_code') . $final_return_SRC . $P_Amount);

                if ($checkstr != strtolower($P_CheckSum)) {
                    $this->system->message('交易發生問題，驗證碼錯誤!!', 'error');
                    redirect('/sellorder/single/' . $order['order_number'], 'refresh');
                }

                // update order payment status
                $this->db->where('id', $order['id'])->set(array('status_id' => ORDER_COMPLETE, 'date_finished' => date("Y-m-d H:i:s")))->update('sellorders');

                $this->system->message(date("Y-m-d H:i:s") . ' 線上付款成功!!', 'success');
                redirect('/sellorder/single/' . $order['order_number'], 'refresh');
            }
        } else {
            if ($final_return_PRC  == '8' and $final_return_SRC == '204') {
                $this->system->message('訂單編號重覆，請與商家聯絡!', 'error');
            } elseif ($final_return_PRC  == '15' and $final_return_SRC == '1018') {
                $this->system->message('主機忙碌中請稍後再試，或與商家聯絡!', 'error');
            } else {
                $this->system->message('交易失敗，請與商家聯絡!', 'error');
            }
            redirect('/sellorder/single/' . $order['order_number'], 'refresh');
        }
    }

    /**
     * Order Payment Validate
     *
     * @param  object
     * @return void
     */
    private function _validate($order = null, $check_login_user = true)
    {
        // check if empty
        if (empty($order)) {
            $this->system->message('並無此訂單', 'error');
            redirect('/sellorder/lists', 'refresh');
        }

        // check if order user is equal session user
        if ($check_login_user and $order['user_id'] != $this->user_id) {
            $this->system->message('您並非是此訂單使用者', 'error');
            redirect('/sellorder/lists', 'refresh');
        }

        // check if order user is equal session user
        if ($order['status_id'] != ORDER_PAYMENT) {
            $this->system->message('您已經付款完成或取消訂單', 'success');
            redirect('/sellorder/lists', 'refresh');
        }
    }

    /**
     * Generate Order Number
     *
     * @return string
     * @author popchieh
     */
    private function generate_order_number()
    {
        $this->load->helper('date');

        // $datestring = "Year: %Y Month: %m Day: %d - %h:%i %a";
        $datestring = "%Y%m%d";
        $time = time();
        // $time = $this->_time;

        // date('Ymd') == date('Ymd', strtotime($timestamp));
        // update order status
        // $this->db->set($data)->where('orders.order_number', $ordernumber)->update('orders');
        // query order_number

        $order_num = "S";
        $time_str = mdate($datestring, $time);

        // select count(*), DATE_FORMAT(date_purchased,"%Y-%m-%d") as created_day FROM sellorders GROUP BY created_day
        $result = $this->lib_sellorder->select('count(*), DATE_FORMAT(date_purchased,"%Y%m%d") as created_day')
             ->group_by('created_day')
             ->orders()
             ->result_array();

        foreach ($result as $row) {
            if($time_str == $row['created_day']){
                $count_today = $row['count(*)'];
                $count_today++;
            }else{
                $count_today = 1;
            }
        }

        $sn_today = $count_today;
        $sn_today = str_pad($sn_today,4,'0',STR_PAD_LEFT);

        $time_str = substr($time_str, 2);
        $order_num .= $time_str.$sn_today;

        // return strtoupper($this->system->generate_code('1', 'word')) . $this->system->generate_code('10', 'digit');
        return $order_num;
    }

}
