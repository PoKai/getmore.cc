<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Verify extends MY_Controller{

    protected $data = array();

    public function __construct(){
        parent::__construct();
        $this->load->library(array('ion_auth'));

        $this->load->spark('curl');

        if (!$this->ion_auth->logged_in()) {
            $this->system->redirect();
        }
        
        $user_id = $this->session->userdata('user_id');
        $this->data['user_id'] = $user_id;

        $this->load->model('User_model');
        
        if ( $this->User_model->is_verified($user_id) ){
            redirect('/sell/index', 'refresh');
        }
    }
    
    public function index(){
        redirect('/verify/personal_information', 'refresh');
    }
    
    public function personal_information(){
        $this->template->render('verify/personal_information', array());
    }
    
    public function save_personal_information(){
        $address = array(
            'county' => $this->input->post('user_county'),
            'district' => $this->input->post('user_district'),
            'zipcode' => $this->input->post('user_zipcode'),
            'address' => $this->input->post('user_address')
        );
        
        $user_data = array(
            "user_name"                => $this->input->post('chinese_name'),
            "sex"                              => $this->input->post('gender'),
            "mobile_phone"          => $this->input->post('phone'),
            "telephone"                 => $this->input->post('phone'),
            "birthday"                    => $this->input->post('birthday'),
            "p_address"                 => json_encode($address),
            "s_address"                  => json_encode($address),
            "identification_card_number"  => $this->input->post('id'),
        );
        try{
            $this->User_model->save_personal_information($this->data['user_id'], $user_data);
            redirect('/verify/phone_number', 'refresh');              
        }
        catch(Exception $e){
            $this->session->set_flashdata('message', $e->getMessage());
            redirect('/verify/personal_information', 'refresh');
        }
    }
    
    public function phone_number(){
        if ( !$this->User_model->is_filled_information($this->session->userdata('user_id')) ){
            redirect('/verify/personal_information', 'refresh');
        }
        
        if (!($this->session->userdata('sms_code'))){
            $sms_code = $this->User_model->generate_sms_code();
            $user = $this->User_model->as_array()->get($this->data['user_id']);
            $this->session->set_userdata("sms_code", $sms_code);
            $this->session->set_userdata("sms_timestamp", time());
            $this->system->send_sms_msg(
                $user['mobile_phone'], "Getmore驗證碼:".$sms_code);
        }
        
        $this->template->render('verify/phone_number', array());      
    }
    
    function resend_sms_code(){
        $time_elapsed = time() - $this->session->userdata('sms_timestamp');
        if ($time_elapsed < 120){
            $this->session->set_flashdata('message', '離上次發送驗證碼須間隔2分鐘');
            redirect('/verify/phone_number', 'refresh');
        }
        $this->session->unset_userdata('sms_code');
        $this->session->unset_userdata('sms_timestamp');

        $this->session->set_flashdata('message', '已重新發送認證碼');
        redirect('/verify/phone_number', 'refresh');      
    }
    
    function check_sms_code(){
        $sms_code = $this->input->post('verification_code');

        if ($this->User_model->check_sms_code($sms_code)){
            $this->session->unset_userdata('sms_code');
            $this->session->unset_userdata('sms_timestamp');
            $this->curl->simple_put('/restful/user', array('verify' => 1), array(
                CURLOPT_COOKIE => http_build_query($_COOKIE,'',';')
            ));
            redirect('/sell/first', 'refresh');
        }
        else{
            $this->session->set_flashdata('message', '認證碼錯誤');
            redirect('/verify/phone_number', 'refresh');      
        }
    }
    
    public function done(){
        echo 'verify/done';
    }

}
