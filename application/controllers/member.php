<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Member extends MY_Controller {

	/**
	 * Default __construct
	 *
	 * @author howtomakeaturn
	 **/
	public function __construct() {
		parent::__construct();
		$this->load->library(array('ion_auth'));
		if (!$this->ion_auth->logged_in()) {
			$this->system->redirect();
		}
		$this->load->spark('curl');
		$this->load->spark('Twiggy/0.8.5');
		$this->twiggy->theme('');
		$this->twiggy->set(array('lang' => 'zh-tw', 'meta_charset' => 'UTF-8', 'site_title' => 'Getmore - 二手衣時尚購物網站/綠色時尚/買二手衣/賣二手衣', 'site_description' => 'GetMore 是亞洲第一個二手衣時尚買賣網站，提供穿二手衣的生活新理念，輕鬆享受二手衣獨特、環保、時尚的二次生活。', 'site_keywords' => '時尚,衣著,服飾,時裝,流行,褲子,買二手衣,賣二手衣,環保,電子商務,startup,二次時尚', 'logined' => $this->ion_auth->logged_in()), NULL, TRUE);
	}

	public function withdraw() {
		if (user_in_group('Designer') || user_in_group('Buyer')) {
			show_404();
		}

		$account = (array) json_decode($this->curl->simple_get('/restful/account', array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));

		$bank_info = (array) json_decode($this->curl->simple_get('/restful/bank', array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));

		if ($this->input->server('REQUEST_METHOD') == 'GET') {
			$user_id = $this->session->userdata('user_id');

			$this->load->database();

			$user = $this->db->from('users')->where('id', $user_id)->get()->result()[0];

			$data['account'] = $account;

			$data['bank_info'] = $bank_info;

			$data['user'] = $user;

			$this->twiggy->set($data);
			$this->twiggy->template('member/withdraw')->display();
		} else if ($this->input->server('REQUEST_METHOD') == 'POST') {

			if ($bank = $this->input->post('bank')) {
				$this->load->model('Bank_model');

				list($code, $title) = explode('-', $bank['code']);
				$bank['code'] = $code;
				$bank['title'] = $title;
				$bank['bank_book_cover'] = new CURLFile($_FILES['bank']['tmp_name']['bank_book_cover']);
				$this->Bank_model->bank_activate($bank);
			} else if (!$bank_info['title'] || !$bank_info['branch'] || !$bank_info['account'] || !$bank_info['code']) {
				redirect('/member/withdraw', 'refresh');
			} else {
				$amount = (int) $this->input->post('amount');

				if ($amount > $account['balance']) {
					$amount = $account['balance'];
				}

				$this->curl->simple_put('/restful/account', array(
					'balance' => $account['balance'] - $amount,
				), array(
					CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
				));

				$this->curl->simple_post('/restful/transactions', array(
					'name' => '申請匯出現金',
					'amount' => 0 - $amount,
				), array(
					CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
				));

				$this->curl->simple_post('/restful/withdraws', array(
					'amount' => $amount,
					'bank_name' => $bank_info['title'],
					'branch_name' => $bank_info['branch'],
					'bank_account' => $bank_info['account'],
				), array(
					CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
				));
			}

			redirect('/member/withdraw', 'refresh');
		}
	}

	function information() {
		// update database if it's POST request
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$user_id = $this->session->userdata('user_id');
			$active_email = $this->input->post('active_email');
			$result = $this->ion_auth->update_active_email($user_id, $active_email);

			if ($result) {
				$this->session->set_flashdata('message', '更新常用email成功！');
				redirect('/member/information', 'refresh');
			} else {
				$this->session->set_flashdata('message', 'email格式錯誤！');
				redirect('/member/information', 'refresh');
			}
		}
		// display the form if it's GET request
		else {
			$user = $this->ion_auth->user()->result()[0];
			$data = [];
			$data['user'] = $user;

			$data['bank_info'] = json_decode($this->curl->simple_get('/restful/bank', array(), array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));

			$this->twiggy->set($data);
			$this->twiggy->template('member/information')->display();
		}
	}

	function change_password() {
		if ($this->input->server('REQUEST_METHOD') != 'POST') {
			redirect('/member/information', 'refresh');
		}

		$user_id = $this->session->userdata('user_id');
		$old_password = $this->input->post('old_password');
		$new_password = $this->input->post('new_password');
		$confirm_password = $this->input->post('confirm_password');
		try {
			$this->ion_auth->change_password_wrapper($user_id, $old_password,
				$new_password, $confirm_password);
			$this->session->set_flashdata('message', '密碼更新成功！');
			redirect('/member/information', 'refresh');
		} catch (Exception $e) {
			$this->session->set_flashdata('message', $e->getMessage());
			redirect('/member/information', 'refresh');
		}
	}

	public function clothing_management() {
		$user_id = $this->session->userdata('user_id');

		$this->load->database();
		$this->load->model('Product_clothing_model', 'clothing');
		$clothings = $this->clothing->get_clothing_by_user_id($user_id);

		$data['clothings'] = $clothings;

		$this->twiggy->set($data);
		$this->twiggy->template('member/clothing_management')->display();
	}

	public function account() {
		$user_id = $this->session->userdata('user_id');

		$account = json_decode($this->curl->simple_get('/restful/account', array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));

		$transactions = json_decode($this->curl->simple_get('/restful/transactions', array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));

		$data['account'] = $account;
		$data['transactions'] = $transactions;

		$this->twiggy->set($data);
		$this->twiggy->template('member/account')->display();
	}

	public function order() {
		$user_id = $this->session->userdata('user_id');
		$this->load->helper('buyorder_helper');

		$this->load->database();
		$this->load->model('Buy_order_model');
		$orders = $this->Buy_order_model->get_some_by_user_id($user_id);

		// extends the data with some 'label' data for rendering
		// name
		foreach ($orders as &$order) {
			$order['status_label'] = status_label($order['status']);

			$created_at = new DateTime($order['created_at']);
			$order['created_at_label'] = $created_at->format('Y-m-d');

		}

		$this->twiggy->set(array(
			'orders' => $orders,
		));
		$this->twiggy->template('member/order')->display();
	}

	public function view_order() {
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			/*$this->load->database();
			$this->load->model('Buy_order_model');*/

			$order = json_decode($this->curl->simple_get('/restful/buy_order/order_number/' . $this->input->post('order_number') . '/all', array(), array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));

			if (strtotime($order->created_at) <= strtotime('2015/05/19')) {
				$products = json_decode($this->curl->simple_get('/restful/product_clothing/', array(
					"buy_order_id" => $order->id,
				), array(
					CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
				)));

				$this->twiggy->set(array(
					'order' => $order,
					'products' => $products,
				));
				return $this->twiggy->template('member/view_old_order')->display();
			}

			list($getmore, $others) = array_reduce($order->SubOrders, function ($result, $sub_order) {

				if ($sub_order->sellorder_id) {
					$result[1][] = $sub_order;
				} else {
					$result[0] = $sub_order;
				}

				return $result;
			}, array());

			// send to view
			$data['order'] = $order;
			$data['getmore'] = $getmore;
			$data['others'] = $others;

			$this->twiggy->set($data);
			$this->twiggy->register_function('post_user_message_modal_for_layout_without_link_bootstrap');
			$this->twiggy->template('member/view_order')->display();
		} else if ($this->input->server('REQUEST_METHOD') == 'GET') {
			redirect('/member/order/', 'refresh');
		}
	}

	function logistics_information() {
		if ($this->input->server('REQUEST_METHOD') == 'GET') {
			redirect('/member/order/', 'refresh');
		}
		$this->load->model('Sellorder_model');
		$order_number = $this->input->post('order_number');
		$complete_order = $this->Sellorder_model->get_complete_order_by_order_number($order_number);

		$total = 0;
		$small = 0;
		$big = 0;
		$fly = 0;
		foreach ($complete_order['items'] as $product) {
			$total++;
			if ($product["shipping_price"] < 120) {
				$small++;
			} else if ($product["shipping_price"] < 150) {
				$big++;
			} else {
				$fly++;
			}

		}

		$data = array(
			'order' => $complete_order['order'],
			'small' => $small,
			'big' => $big,
			'fly' => $fly,
			'total' => $total,
		);
		$this->template->render('member/logistics_information', $data);
	}

	public function sellorder() {
		$user_id = $this->session->userdata('user_id');

		$this->load->database();
		$this->load->model('Sellorder_model');
		$orders = $this->Sellorder_model->get_some_by_user_id($user_id);

		$data['orders'] = $orders;

		$this->twiggy->set($data);
		$this->twiggy->template('member/sellorder')->display();
	}

	public function view_sellorder() {
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$this->load->database();
			$this->load->model('Sellorder_model');
			$complete_order = $this->Sellorder_model->get_complete_order_by_order_number($this->input->post('order_number'));

			$success_num = 0;
			foreach ($complete_order['items'] as $product) {
				if ($product["remove"]) {
					continue;
				}

				$success_num++;
			}

			// send to view
			$data['complete_order'] = $complete_order;
			$data['success_num'] = $success_num;

			$this->load->helper('checkout_helper');

			$this->twiggy->set($data);
			$this->twiggy->template('member/view_sellorder')->display();
		} else if ($this->input->server('REQUEST_METHOD') == 'GET') {
			redirect('/member/order/', 'refresh');
		}
	}

}
