<?php
if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Clothing extends MY_Controller {

	/**
	 * new subject or event value
	 *
	 * @var int
	 *
	 */
	private $_news_count = 4;

	/**
	 * sale subject or event value, order by sale time
	 *
	 * @var int
	 *
	 */
	private $_sales_count = 9;

	/**
	 * Default __construct
	 *
	 * @author appleboy
	 *
	 */
	private $season_list = [
		'1' => '春夏',
		'2' => '秋冬',
		'3' => '無季節',
	];

	private $gender_list = [
		'type=women' => 'WOMEN',
		'type=men' => 'MEN',
		'type=kids' => 'KIDS',
	];

	private $product_type_list = [
		'2' => [
			'WOMEN' => [
				'product_type=11' => '上衣',
				'product_type=12' => '洋裝',
				'product_type=13&pattern=26' => '連身褲',
				'product_type=13&pattern=13' => '牛仔褲',
				'product_type=13&item_long=sp' => '短褲',
				'product_type=13&item_long=lp' => '長褲',
				'product_type=14' => '裙子',
				'product_type=151&product_type=152&product_type=153' => '外套',
			],
			'MEN' => [
				'product_type=21' => '上衣',
				'product_type=22' => '襯衫',
				'product_type=23&pattern=13' => '牛仔褲',
				'product_type=23&item_long=sp' => '短褲',
				'product_type=23&item_long=lp' => '長褲',
				'product_type=243&product_type=242&product_type=241&pattern=24' => '西裝外套',
				'product_type=241&product_type=242&product_type=243' => '外套',
			],
			'KIDS' => [
				'product_type=31' => '連身衣',
				'product_type=32' => '上衣',
				'product_type=33' => '下身',
				'product_type=34' => '洋裝',
				'product_type=351&product_type=352' => '外套',
			],
		],
		'3' => [
			'gender=1&group=Clothing' => [
				'product_type=11' => '上衣',
				'product_type=12' => '洋裝',
				'product_type=13&pattern=26' => '連身褲',
				'product_type=13&pattern=13' => '牛仔褲',
				'product_type=13&item_long=sp' => '短褲',
				'product_type=13&item_long=lp' => '長褲',
				'product_type=14' => '裙子',
				'product_type=15' => '外套',
			],
			'gender=1&group=Bags' => [
				'product_type=1B1' => '包',
			],
			'gender=1&group=Shoes' => [
				/*
					'product_type=1S1' => '靴子',
					'product_type=1S2' => '平底鞋',
					'product_type=1S3' => '雨鞋',
					'product_type=1S4' => '涼拖鞋',
					'product_type=1S5' => '運動休閒鞋',
					'product_type=1S6' => '高跟鞋',
					'product_type=1S7' => '鞋用品',
				*/
			],
			'gender=1&group=Accessories' => [
				/*
					'product_type=1A1' => '飾品',
					'product_type=1A2' => '錢包',
					'product_type=1A3' => '手錶',
					'product_type=1A4' => '皮帶',
					'product_type=1A5' => '手套',
					'product_type=1A6' => '髮飾',
					'product_type=1A7' => '帽子',
					'product_type=1A8' => '鑰匙圈/鑰匙包',
					'product_type=1A9' => '絲巾/圍巾',
					'product_type=1A10' => '襪子',
					'product_type=1A11' => '眼鏡/太陽眼鏡',
					'product_type=1A12' => '3C配件',
					'product_type=1A13' => '文具',
					'product_type=1A14' => '家飾/居家用品',
					'product_type=1A15' => '旅行用品',
					'product_type=1A16' => '雨傘',
				*/
			],
			'gender=2&group=Clothing' => [
				'product_type=21' => '上衣',
				'product_type=22' => '襯衫',
				'product_type=23&pattern=13' => '牛仔褲',
				'product_type=23&item_long=sp' => '短褲',
				'product_type=23&item_long=lp' => '長褲',
				'pattern=24&product_type=24' => '西裝外套',
				'product_type=24' => '外套',
			],
			'gender=2&group=Bags' => [
				'product_type=2B1' => '包',
			],
			'gender=2&group=Shoes' => [
				/*
					'product_type=2S1' => '靴子',
					'product_type=2S2' => '皮鞋',
					'product_type=2S3' => '休閒鞋',
					'product_type=2S4' => '運動鞋',
					'product_type=2S5' => '涼拖鞋',
					'product_type=2S6' => '鞋用品/鞋把',
				*/
			],
			'gender=2&group=Accessories' => [
				/*
					'product_type=2A1' => '飾品',
					'product_type=2A2' => '手錶',
					'product_type=2A3' => '皮帶',
					'product_type=2A4' => '手套',
					'product_type=2A5' => '領帶/領結',
					'product_type=2A6' => '手帕/口袋巾',
					'product_type=2A7' => '帽子',
					'product_type=2A8' => '鑰匙圈/鑰匙包',
					'product_type=2A9' => '絲巾/圍巾',
					'product_type=2A10' => '襪子',
					'product_type=2A11' => '眼鏡/太陽眼鏡',
					'product_type=2A12' => '皮夾/鈔票夾',
					'product_type=2A13' => '3C配件',
					'product_type=2A14' => '文具',
					'product_type=2A15' => '家飾/居家用品',
					'product_type=2A16' => '旅行用品',
					'product_type=2A17' => '雨傘',
				*/
			],
			'gender=3&group=Clothing' => [
				'product_type=31' => '連身衣',
				'product_type=32' => '上衣',
				'product_type=33' => '下身',
				'product_type=34' => '洋裝',
				'product_type=35' => '外套',
			],
			'gender=3&group=Bags' => [
				'product_type=3B1' => '包',
			],
			'gender=3&group=Shoes' => [
				/*
					'product_type=3S1' => '靴子',
					'product_type=3S2' => '平底鞋',
					'product_type=3S3' => '雨鞋',
					'product_type=3S4' => '涼拖鞋',
					'product_type=3S5' => '運動休閒鞋',
					'product_type=3S6' => '學步鞋',
				*/
			],
			'gender=3&group=Accessories' => [
				/*
					'product_type=3A1' => '飾品',
					'product_type=3A2' => '手錶',
					'product_type=3A3' => '皮帶',
					'product_type=3A4' => '手套',
					'product_type=3A5' => '髮飾',
					'product_type=3A6' => '帽子',
					'product_type=3A7' => '絲巾/圍巾',
					'product_type=3A8' => '襪子',
					'product_type=3A9' => '眼鏡/太陽眼鏡',
					'product_type=3A10' => '文具',
					'product_type=3A11' => '雨傘',
				*/
			],
		],
	];

	private $group_list = [
		'3' => [
			'WOMEN' => [
				'gender=1&group=Clothing' => '衣服',
				'gender=1&group=Bags&product_type=1B1' => '包包',
				'gender=1&group=Shoes' => '鞋子',
				'gender=1&group=Accessories' => '配件',
			],
			'MEN' => [
				'gender=2&group=Clothing' => '衣服',
				'gender=2&group=Bags&product_type=2B1' => '包包',
				'gender=2&group=Shoes' => '鞋子',
				'gender=2&group=Accessories' => '配件',
			],
			'KIDS' => [
				'gender=3&group=Clothing' => '衣服',
				'gender=3&group=Bags&product_type=3B1' => '包包',
				'gender=3&group=Shoes' => '鞋子',
				'gender=3&group=Accessories' => '配件',
			],
		],
	];

	private $patterns = [
		'2' => [
			'product_type=11' => [
				'pattern=3' => 'T恤',
				'pattern=19' => '背心',
				'pattern=23' => '襯衫',
				'pattern=2' => 'Polo 衫',
				'pattern=28' => '毛衣/針織衫',
				'pattern=33&pattern=10' => '雪紡',
				'pattern=30' => '長上衣',
			],
			'product_type=12' => [
				'pattern=15' => '短洋裝',
				'pattern=31' => '長洋裝',
				'style=11' => '休閒洋裝',
				'style=8&style=14' => '正式場合',
				'style=1' => 'OL洋裝',
				'item_long=ls' => '長袖洋裝',
			],
			'product_type=13&pattern=26' => [
				'item_long=sp' => '短褲',
				'item_long=lp' => '長褲',
				'style=25' => '吊帶褲',
			],
			'product_type=13&pattern=13' => [
				'item_long=sp' => '短褲',
				'item_long=lp' => '長褲',
				'style=25' => '吊帶褲',
			],
			'product_type=13&item_long=sp' => [
				'pattern=13' => '牛仔褲',
				'pattern=5' => '休閒褲',
				'pattern=25' => '西裝褲',
				'pattern=26' => '連身褲',
				'pattern=22' => '褲裙',
				'style=24' => '皮褲',
			],
			'product_type=13&item_long=lp' => [
				'pattern=13' => '牛仔褲',
				'pattern=5' => '休閒褲',
				'pattern=25' => '西裝褲',
				'pattern=26' => '連身褲',
				'pattern=22' => '寬褲/褲裙',
				'pattern=1' => 'Legging',
				'style=24' => '皮褲',
			],
			'product_type=14' => [
				'pattern=16' => '短裙',
				'pattern=32' => '長裙',
				'style=9' => '牛仔裙',
				'style=24' => '皮裙',
				'style=25' => '吊帶裙',
			],
			'product_type=151&product_type=152&product_type=153' => [
				'style=9' => '牛仔外套',
				'pattern=24' => '西裝外套',
				'pattern=12' => '休閒/棉質外套',
				'pattern=14' => '皮衣',
				'pattern=29' => '針織/毛衣外套',
				'pattern=34' => '背心外套',
				'pattern=18' => '羽絨衣',
				'pattern=11' => '斗篷',
				'pattern=9' => '大衣',
			],
			'product_type=21' => [
				'pattern=3' => 'T-Shirt',
				'pattern=2' => 'Polo衫',
				'pattern=28' => '針織/毛衣',
				'pattern=19' => '背心',
			],
			'product_type=22' => [
				'pattern=8' => '商務襯衫',
				'pattern=6' => '休閒襯衫',
				'item_long=ss' => '短袖襯衫',
				'item_long=ls' => '長袖襯衫',
			],
			'product_type=23&pattern=13' => [
				'item_long=sp' => '短褲',
				'item_long=lp' => '長褲',
				'style=25' => '吊帶褲',
			],
			'product_type=23&item_long=sp' => [
				'pattern=13' => '牛仔褲',
				'pattern=5' => '休閒褲',
				'pattern=25' => '西裝褲',
			],
			'product_type=23&item_long=lp' => [
				'pattern=13' => '牛仔褲',
				'pattern=5' => '休閒褲',
				'pattern=25' => '西裝褲',
			],
			'product_type=243&product_type=242&product_type=241&pattern=24' => [
				'style=1&style=8&style=15' => '正式場合',
				'style=11' => '休閒西裝外套',
			],
			'product_type=241&product_type=242&product_type=243' => [
				'style=9' => '牛仔外套',
				'pattern=24' => '西裝外套',
				'pattern=12' => '休閒/棉質外套',
				'pattern=14' => '皮衣',
				'pattern=29' => '針織/毛衣外套',
				'pattern=34' => '背心外套',
				'pattern=17' => '羽絨衣',
				'pattern=9' => '大衣',
			],
			'product_type=32' => [
				'pattern=19' => '背心',
				'pattern=3' => 'T-Shirt',
				'pattern=23' => '襯衫',
				'pattern=28' => '針織/毛衣',
			],
			'product_type=33' => [
				'pattern=13' => '牛仔褲',
				'pattern=21&item_long=sp' => '短褲',
				'pattern=21&item_long=lp' => '長褲',
				'pattern=22' => '褲裙',
				'pattern=20' => '裙子',
			],
			'product_type=351&product_type=352' => [
				'pattern=4' => '夾克外套',
				'pattern=18' => '羽絨衣',
				'pattern=34' => '背心外套',
				'pattern=29' => '針織/毛衣外套',
			],
		],
		'3' => [
			'product_type=11' => [
				'pattern=3' => 'T恤',
				'pattern=19' => '背心',
				'pattern=23' => '襯衫',
				'pattern=2' => 'Polo 衫',
				'pattern=28' => '毛衣/針織衫',
				'pattern=33&pattern=10' => '雪紡',
				'pattern=35' => '休閒運動衫',
				'sub_pattern_1=9' => '長上衣',
				'sub_pattern_1=8' => '短版上衣',
			],
			'product_type=12' => [
				'pattern=15' => '短洋裝',
				'pattern=31' => '長洋裝',
				'style=11' => '休閒洋裝',
				'style=8&style=14' => '正式場合',
				'style=1' => 'OL洋裝',
				'item_long=ls' => '長袖洋裝',
			],
			'product_type=13&pattern=26' => [
				'item_long=sp' => '短褲',
				'item_long=lp' => '長褲',
				'style=25' => '吊帶褲',
			],
			'product_type=13&pattern=13' => [
				'item_long=sp' => '短褲',
				'style=25' => '吊帶褲',
				'sub_pattern_1=1' => '靴型',
				'sub_pattern_1=2' => '窄管',
				'sub_pattern_1=3' => '喇叭',
				'sub_pattern_1=4' => '直筒',
				'sub_pattern_1=5' => '男友褲',
			],
			'product_type=13&item_long=sp' => [
				'pattern=13' => '牛仔褲',
				'pattern=5' => '休閒褲',
				'pattern=25' => '西裝褲',
				'pattern=26' => '連身褲',
				'pattern=22' => '褲裙',
				'style=24' => '皮褲',
			],
			'product_type=13&item_long=lp' => [
				'pattern=13' => '牛仔褲',
				'pattern=5' => '休閒褲',
				'pattern=25' => '西裝褲',
				'pattern=26' => '連身褲',
				'pattern=22' => '寬褲/褲裙',
				'pattern=1' => 'Legging',
				'style=24' => '皮褲',
			],
			'product_type=14' => [
				'item_long=mini' => '短裙',
				'item_long=knee' => '及膝裙',
				'item_long=midi' => '中長裙',
				'item_long=maxi' => '長裙',
				'pattern=36' => 'A Line/圓裙',
				'pattern=37' => '窄裙/鉛筆裙',
				'style=9' => '牛仔裙',
				'style=24' => '皮裙',
				'style=25' => '吊帶裙',
			],
			'product_type=15' => [
				'style=9' => '牛仔外套',
				'pattern=24' => '西裝外套',
				'pattern=12' => '休閒/棉質外套',
				'pattern=14' => '皮衣',
				'pattern=29' => '針織/毛衣外套',
				'pattern=34' => '背心外套',
				'pattern=18' => '羽絨衣',
				'pattern=11' => '斗篷',
				'pattern=9' => '大衣',
				'pattern=38' => '風衣',
			],
			'product_type=1S1' => [
				'pattern=41' => '踝靴',
				'pattern=42' => '短靴',
				'pattern=43' => '中筒靴',
				'pattern=44' => '長靴',
				'pattern=45' => '過膝靴',
			],
			'product_type=1S2' => [
				'pattern=46' => '芭蕾舞鞋',
				'pattern=47' => '尖頭平底鞋',
				'pattern=48' => '草編鞋',
				'pattern=49' => '樂福鞋/莫卡辛',
				'pattern=50' => '牛津鞋',
			],
			'product_type=1S4' => [
				'pattern=51' => '涼鞋',
				'pattern=52' => '拖鞋',
				'pattern=53' => '夾腳涼拖鞋',
			],
			'product_type=1S5' => [
				'pattern=54' => '運動鞋',
				'pattern=55' => '休閒鞋',
				'pattern=56' => 'slip on',
			],
			'product_type=1S6' => [
				'pattern=57' => '圓頭鞋',
				'pattern=58' => '尖頭鞋',
				'pattern=59' => '魚口鞋',
			],
			'gender=1&group=Bags&product_type=1B1' => [
				/*
					'pattern=60' => '化妝包',
					'pattern=61' => '手拿包',
					'pattern=62' => '手提包',
					'pattern=63' => '水桶包',
					'pattern=64' => '托特包',
					'pattern=65' => '肩背包',
					'pattern=66' => '後背包',
					'pattern=67' => '斜背包',
					'pattern=68' => '旅行袋',
					'pattern=69' => '行李箱',
				*/
			],
			'product_type=1A1' => [
				'pattern=71' => '手鍊/手環',
				'pattern=72' => '項鍊',
				'pattern=73' => '戒指',
				'pattern=74' => '耳環/耳骨夾',
				'pattern=75' => '胸針/別針',
				'pattern=76' => '身體首飾/裝飾',
				'pattern=77' => '珠寶盒',
			],
			'product_type=1A2' => [
				'pattern=78' => '皮夾',
				'pattern=79' => '卡夾',
				'pattern=80' => '零錢包',
			],
			'product_type=1A4' => [
				'item_long=siim' => '細版',
				'item_long=wide' => '寬版',
			],
			'product_type=1A6' => [
				'pattern=81' => '髮圈',
				'pattern=82' => '髮箍',
				'pattern=83' => '髮梳',
				'pattern=84' => '髮夾',
				'pattern=85' => '髮帶/頭巾',
				'pattern=86' => '頭飾/頭紗',
				'pattern=87' => '新娘花圈',
			],
			'product_type=1A7' => [
				'pattern=88' => '軟呢帽',
				'pattern=89' => '毛帽',
				'pattern=90' => '漁夫帽',
				'pattern=91' => '草帽/遮陽帽',
				'pattern=92' => '棒球帽',
				'pattern=93' => '貝蕾帽',
				'pattern=94' => '報童帽',
			],
			'product_type=1A9' => [
				'pattern=95' => '絲巾',
				'pattern=96' => '圍巾',
				'pattern=97' => '圍脖',
				'pattern=98' => '披肩',
			],
			'product_type=1A10' => [
				'item_long=stock' => '短襪',
				'item_long=stocking' => '長襪',
				'item_long=tights' => '褲襪',
			],
			'product_type=1A12' => [
				'pattern=99' => '手機殼/手機包',
				'pattern=100' => '筆電包',
				'pattern=101' => '平板配件',
				'pattern=102' => '耳機',
				'pattern=103' => 'USB',
			],
			'product_type=1A14' => [
				'pattern=104' => '收藏擺飾',
				'pattern=105' => '香氛',
				'pattern=106' => '毛巾/浴巾',
				'pattern=107' => '洗滌',
				'pattern=108' => '寵物',
			],
			'product_type=21' => [
				'pattern=3' => 'T-Shirt',
				'pattern=2' => 'Polo衫',
				'pattern=35' => '休閒運動衫',
				'pattern=28' => '針織/毛衣',
				'pattern=19' => '背心',
			],
			'product_type=22' => [
				'pattern=8' => '商務襯衫',
				'pattern=6' => '休閒襯衫',
				'item_long=ss' => '短袖襯衫',
				'item_long=ls' => '長袖襯衫',
			],
			'product_type=23&pattern=13' => [
				'item_long=sp' => '短褲',
				'sub_pattern_1=4' => '直筒',
				'sub_pattern_1=2' => '窄管',
				'style=25' => '吊帶褲',
			],
			'product_type=23&item_long=sp' => [
				'pattern=13' => '牛仔褲',
				'pattern=5' => '休閒褲',
				'pattern=25' => '西裝褲',
				'pattern=39' => '海灘褲',
				'pattern=40' => '運動褲',
			],
			'product_type=23&item_long=lp' => [
				'pattern=13' => '牛仔褲',
				'pattern=5' => '休閒褲',
				'pattern=25' => '西裝褲',
				'pattern=40' => '運動褲',
			],
			'pattern=24&product_type=24' => [
				'style=1&style=8&style=15' => '正式場合',
				'style=11' => '休閒西裝外套',
			],
			'product_type=24' => [
				'style=9' => '牛仔外套',
				'pattern=24' => '西裝外套',
				'pattern=12' => '休閒/棉質外套',
				'pattern=14' => '皮衣',
				'pattern=29' => '針織/毛衣外套',
				'pattern=34' => '背心外套',
				'pattern=17' => '羽絨衣',
				'pattern=9' => '大衣',
				'pattern=38' => '風衣',
			],
			'product_type=2S4' => [
				'pattern=54' => '運動鞋',
				'pattern=56' => 'slip on',
			],
			'gender=2&group=Bags&product_type=2B1' => [
				/*
					'pattern=66' => '後背包',
					'pattern=109' => '公事包',
					'pattern=110' => '信差包',
					'pattern=64' => '托特包',
					'pattern=111' => '運動包/旅行袋',
					'pattern=69' => '行李箱',
					'pattern=112' => '腰包',
					'pattern=61' => '手拿包',
					'pattern=113' => '盥洗包',
				*/
			],
			'product_type=2A1' => [
				'pattern=71' => '手鍊/手環',
				'pattern=72' => '項鍊',
				'pattern=73' => '戒指',
				'pattern=114' => '耳環',
				'pattern=115' => '領帶夾',
				'pattern=116' => '袖扣',
				'pattern=117' => '胸花',
			],
			'product_type=2A7' => [
				'pattern=118' => '紳士帽',
				'pattern=89' => '毛帽',
				'pattern=119' => '棒球帽',
				'pattern=90' => '漁夫帽',
				'pattern=120' => '草帽',
				'pattern=94' => '報童帽',
				'pattern=121' => '頭帶/頭巾',
			],
			'product_type=2A13' => [
				'pattern=99' => '手機殼/手機包',
				'pattern=100' => '筆電包',
				'pattern=101' => '平板配件',
				'pattern=102' => '耳機',
				'pattern=103' => 'USB',
			],
			'product_type=2A15' => [
				'pattern=122' => '收藏品',
				'pattern=123' => '薰香',
				'pattern=106' => '毛巾/浴巾',
				'pattern=107' => '洗滌',
				'pattern=108' => '寵物',
			],
			'product_type=32' => [
				'pattern=19' => '背心',
				'pattern=3' => 'T-Shirt',
				'pattern=23' => '襯衫',
				'pattern=28' => '針織/毛衣',
			],
			'product_type=33' => [
				'pattern=13' => '牛仔褲',
				'pattern=21&item_long=sp' => '短褲',
				'pattern=21&item_long=lp' => '長褲',
				'pattern=22' => '褲裙',
				'pattern=20' => '裙子',
			],
			'product_type=35' => [
				'pattern=4' => '夾克外套',
				'pattern=18' => '羽絨衣',
				'pattern=34' => '背心外套',
				'pattern=29' => '針織/毛衣外套',
				'pattern=38' => '風衣',
			],
			'gender=3&group=Bags&product_type=3B1' => [
				/*
					'pattern=124' => '書包/後背包',
					'pattern=67' => '斜背包',
					'pattern=62' => '手提包',
				*/
			],
			'product_type=3A1' => [
				'pattern=71' => '手鍊/手環',
				'pattern=72' => '項鍊',
				'pattern=73' => '戒指',
				'pattern=114' => '耳環',
				'pattern=125' => '別針',
			],
		],
	];

	private $dsner_list = [
		1 => '吳日云',
		2 => '邱美寧',
		3 => 'IF&N',
		4 => '版塊位移',
	];

	public function __construct() {
		parent::__construct();
		$this->load->library(array('lib_static_url', 'lib_subject', 'lib_config', 'lib_event', 'files_lib', 'ion_auth'));
		$this->load->library(array('form_validation'));
		$this->load->config('clothing_class');

		$this->load->spark('curl');
		$this->curl->option(CURLOPT_COOKIE, http_build_query($_COOKIE, '', ';'));

		$this->load->spark('Twiggy/0.8.5');
		$this->twiggy->theme('');

		$this->twiggy->set(array('lang' => 'zh-tw', 'meta_charset' => 'UTF-8', 'site_title' => 'Getmore - 新銳設計與時尚二手線上市集/亞洲新銳/二手精品/二手包', 'site_description' => 'Getmore 是為時尚愛好者以及設計師打造的時尚平台，我們的使命是挖掘世界各地的時尚愛好者，以及獨樹一格的時裝設計師，打造一流的網路平台以及服務，讓消費者可以盡情的在 Getmore 選購新銳設計師新品，以及國際品牌二手古著單品。', 'site_keywords' => '時尚,衣著,服飾,時裝,二手衣,流行,新銳設計師,亞洲時尚,褲子,買二手衣,賣二手衣,環保,電子商務,startup,二次時尚', 'logined' => $this->ion_auth->logged_in()), NULL, TRUE);
	}

	function view($id = '') {
		$this->load->helper('url');

		return redirect('/clothing/brandsonly/' . $id, 'refresh');
	}

	function update_price() {
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$this->load->library('session');
			$user_id = $this->session->userdata('user_id');
			$new_price = $this->input->post('value');
			$id = $this->input->post('id');

			$this->load->database();
			$this->load->model('Product_clothing_model', 'clothing');

			try {
				$this->clothing->update_price($id, $new_price, $user_id);
				echo $new_price;
			} catch (Exception $e) {
				$clothing = $this->clothing->get($id);
				echo $clothing->product_sell_price;
			}
		}
	}

	/**
	 * getmorex
	 *
	 * @author howtomakeaturn
	 *
	 */
	public function getmorex($page) {
		if (empty($page)) {
			redirect('/clothing/getmorex/index', 'refresh');
		}
		$this->load->database();
		$this->load->model('Product_clothing_model', 'clothing');

		if ($page == 'index') {
			$clothings = $this->clothing->get_branded_clothings(8, 0);
			$data['clothings'] = $clothings;
			$this->template->render('clothing/getmorex/index', $data);
			return;
		}
		if ($page == 'aboutx') {
			$this->template->render('clothing/getmorex/aboutx', $data);
			return;
		}
		if ($page == 'how_we_choose') {
			$this->template->render('clothing/getmorex/how_we_choose', $data);
			return;
		}
		if ($page == 'brand_list') {
			$this->template->render('clothing/getmorex/brand_list', $data);
			return;
		}
		if ($page == 'promise') {
			$this->template->render('clothing/getmorex/promise', $data);
			return;
		}
		if ($page == 'special') {
			$this->template->render('clothing/getmorex/special', $data);
			return;
		}
		if ($page == 'new') {
			$clothings = $this->clothing->get_branded_clothings(40, 0);
			$data['clothings'] = $clothings;
			$title = 'New';
		}
		if ($page == 'women') {
			$clothings = $this->clothing->get_branded_clothings(40, 0, 'women');
			$data['clothings'] = $clothings;
			$title = 'Women';
		}

		/*
			if ($page=='men'){
			$clothings = $this->clothing->get_branded_clothings(40, 0, 'men');
			$data['clothings'] = $clothings;
			$title = 'Men';
			}
		*/

		$data['title'] = $title;
		$this->template->render('clothing/getmorex/page', $data);
	}

	/**
	 * clothing category page
	 *
	 * @author howtomakeaturn
	 *
	 */
	public function category($type, $page = 0) {
		if (empty($type)) {
			redirect('/clothing/category/new', 'refresh');
		}
		$this->load->model('Brand_model');
		$this->config->load('clothing_class');
		$styles = $this->config->item('style_name');

		$data['styles'] = $styles;
		$data['brands'] = $this->Brand_model->get_many_by('approved', TRUE);
		$this->template->render('clothing/category', $data);
	}

	/**
	 * clothing category page
	 *
	 * @author howtomakeaturn
	 *
	 */
	public function category_refactor($type, $page = 0) {
		if (empty($type)) {
			redirect('/clothing/category/new', 'refresh');
		}
		$this->load->model('Brand_model');
		$this->config->load('clothing_class');
		$styles = $this->config->item('style_name');

		$data['styles'] = $styles;
		$data['brands'] = $this->Brand_model->get_many_by('approved', TRUE);
		$this->template->render('clothing/category_refactor', $data);
	}

	/**
	 * clothing category page
	 *
	 * @author howtomakeaturn
	 *
	 */
	public function category_via_ajax() {
		$type = $this->input->post('type');
		$page = $this->input->post('page');
		$season = $this->input->post('season');
		$color = $this->input->post('color');
		$style = $this->input->post('style');
		$subtype = $this->input->post('subtype');
		$pattern = $this->input->post('pattern');
		$keyword = $this->input->post('keyword');
		$item_long = $this->input->post('item_long');
		$size = $this->input->post('size');
		$status = $this->input->post('status');
		$price_to = $this->input->post('price_to');
		$price_from = $this->input->post('price_from');

		if (empty($type)) {
			redirect('/clothing/category/new', 'refresh');
		}
		$per_page = 15;
		if ($type === 'new' || $type === 'marketplace') {
			$per_page = 20;
		}
		$offset = ($page - 1) * $per_page;

		$data = new stdClass();

		if ($type === 'marketplace') {
			$data->clothing_array = json_decode($this->curl->simple_get('/restful/combine/marketplace', array('per_page' => $per_page, 'page' => $page, 'season' => $season, 'color' => $color, 'style' => $style, 'subtype' => $subtype, 'pattern' => $pattern, 'keyword' => $keyword, 'item_long' => $item_long, 'size_standard' => $size, 'item_status' => $status, 'price_to' => $price_to, 'price_from' => $price_from)));

			$data->total_rows = json_decode($this->curl->simple_get('/restful/combine/marketplace/count', array('season' => $season, 'color' => $color, 'style' => $style, 'subtype' => $subtype, 'pattern' => $pattern, 'keyword' => $keyword, 'item_long' => $item_long, 'size_standard' => $size, 'item_status' => $status, 'price_to' => $price_to, 'price_from' => $price_from)));
		} else {
			$data->clothing_array = json_decode($this->curl->simple_get('/restful/combine/product_clothings', array('per_page' => $per_page, 'page' => $page, 'type' => $type, 'season' => $season, 'color' => $color, 'style' => $style, 'subtype' => $subtype, 'pattern' => $pattern, 'keyword' => $keyword, 'item_long' => $item_long, 'size_standard' => $size, 'item_status' => $status, 'price_to' => $price_to, 'price_from' => $price_from)));

			$data->total_rows = json_decode($this->curl->simple_get('/restful/combine/product_clothings/count', array('type' => $type, 'season' => $season, 'color' => $color, 'style' => $style, 'subtype' => $subtype, 'pattern' => $pattern, 'keyword' => $keyword, 'item_long' => $item_long, 'size_standard' => $size, 'item_status' => $status, 'price_to' => $price_to, 'price_from' => $price_from)));
		}

		$data->offset = $offset;
		// acturally, this seems to be useless

		$this->config->load('clothing_class');
		$subtype_patterns = $this->config->item('subtype_patterns');
		$matched_patterns = $subtype_patterns[$subtype];
		$data->matched_patterns = $matched_patterns;

		$pattern_name = $this->config->item('pattern_name');
		$data->pattern_name = $pattern_name;

		$this->config->load('clothing_class');
		$subtype_item_longs = $this->config->item('subtype_item_longs');
		$matched_item_longs = $subtype_item_longs[$subtype];
		$data->matched_item_longs = $matched_item_longs;

		$item_long_name = $this->config->item('item_long_name');
		$data->item_long_name = $item_long_name;

		echo json_encode($data);
		//$this->template->render('clothing/category', $data);

	}

	public function special() {

		$this->load->database();
		$this->load->model('Product_clothing_model');

		$clothing_array = array();

		// 大衣
		$array_one = $this->Product_clothing_model->get_many_by_pattern(9);

		// 羽絨衣
		$array_two = $this->Product_clothing_model->get_many_by_pattern(17);

		// 羽絨衣/背心
		$array_three = $this->Product_clothing_model->get_many_by_pattern(18);

		$clothing_array = array_merge($array_one, $array_two, $array_three);

		$data['clothings'] = $clothing_array;
		$this->template->render('clothing/special', $data);
	}

	public function earthhour() {
		$this->template->render('clothing/earthhour', []);
	}

	public function index() {
		$banner_data = array();

		$banner_data = (array) json_decode($this->curl->simple_get(base_url('/restful/banner/'), [
			"banner_location" => range(0, 5),
		]));

		$banner_data = array_reduce($banner_data, function ($result, $banner) {
			if (empty($result[$banner->banner_location])) {
				$result[$banner->banner_location] = [];
			}
			$result[$banner->banner_location][] = (array) $banner;

			return $result;
		}, []);

		// get getmore suggest data
		$data = $this->curl->simple_get(base_url('/restful/suggested_product/1'));
		$data = json_decode($data);
		$suggestData = array();
		foreach ($data as $index => $product) {
			if ($index % 6 == 0) {
				$suggestData[$index / 6] = array();
			}

			array_push($suggestData[count($suggestData) - 1], $product);
		}

		// get recently view
		$recently_viewed_item_numbers = (array) json_decode($this->curl->simple_get(base_url("/restful/recently_viewed"), [], [
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		]));

		if (empty($recently_viewed_item_numbers)) {
			$recently_viewed = [];
		} else {
			$recently_viewed = (array) json_decode($this->curl->simple_get(base_url("/restful/combine/all_clothings"), [
				'product_item_number' => $recently_viewed_item_numbers,
			]));
			$results = array();
			$index = 0;
			foreach ($recently_viewed as $product) {
				if ($index % 6 == 0) {
					$results[$index / 6] = array();
				}

				array_push($results[count($results) - 1], $product);
				$index++;
			}
			$recently_viewed = $results;
		}

		$dataNotComplete = false;
		$userdata = '';
		if (!empty($this->session->userdata('user_id'))) {
			$userdata = (array) json_decode($this->curl->simple_get('/restful/user/me', array(), array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));

			if ($userdata) {
				if (empty($userdata['nickname']) || empty($userdata['active_email']) || empty($userdata['birthday'])) {
					$dataNotComplete = true;
				}
				if (!empty($userdata['birthday'])) {
					$birthday = explode(" ", $userdata['birthday']);
					$userdata['birthday'] = $birthday[0];

					if ($userdata['birthday'] == "Invalid ") {
						$userdata['birthday'] = '';
					}
				}
			}
		}

		$this->twiggy->template('clothing/newview')->set(array(
			'banners' => $banner_data[0],
			'banner1' => $banner_data[1][rand(0, count($banner_data[1]) - 1)],
			'banner2' => $banner_data[2][rand(0, count($banner_data[2]) - 1)],
			'banner3' => $banner_data[3][rand(0, count($banner_data[3]) - 1)],
			'banner4' => $banner_data[4][rand(0, count($banner_data[4]) - 1)],
			'suggest' => $suggestData,
			'recently_viewed' => $recently_viewed,
			'dataNotComplete' => $dataNotComplete,
			'userdata' => $userdata,
		))->display();
	}

	public function up_and_coming_dsner() {
		$this->twiggy->set([
			'season_list' => $this->season_list,
			'style_list' => $this->style_list,
			'dsner_list' => $this->dsner_list,
			'product_type_list' => $this->product_type_list,
			'patterns' => $this->patterns,
			'sub_patterns' => $this->sub_patterns,
		])->template('clothing/up_and_coming_dsner')->display();
	}

	public function sale($type = null) {
		$this->twiggy->set([
			'season_list' => $this->season_list,
			'style_list' => array_reduce($this->config->item('style_lists')[1], function ($result, $style_list) {
				return array_merge($result, array_values($style_list));
			}, []),
			'gender_list' => $this->gender_list,
			'product_type_list' => $this->product_type_list,
			'patterns' => $this->patterns,
			'sub_patterns' => $this->sub_patterns,
			'type' => $type,
			'user_id' => $this->session->userdata('user_id')
		])->template('clothing/sale')->display();
	}

	public function dsnerview() {
		$this->twiggy->set([
			'price_range' => json_decode($this->curl->simple_get('/restful/designer_products/min_and_max', ['launch_way' => 3])),
			'season_list' => $this->season_list,
			'style_list' => array_reduce($this->config->item('style_lists')[1], function ($result, $style_list) {
				$result += $style_list;
				return $result;
			}, []),
			'gender_list' => $this->gender_list,
			'group_list' => $this->group_list['3'],
			'product_type_list' => $this->product_type_list['3'],
			'patterns' => $this->patterns['3'],
			'sub_patterns' => $this->sub_patterns,
			'user_id' => $this->session->userdata('user_id')
		])->template('clothing/dsnerview')->display();
	}

	public function newbrandsonly() {
		$this->twiggy->set([
			'price_range' => json_decode($this->curl->simple_get('/restful/product_clothing/min_and_max')),
			'season_list' => $this->season_list,
			'style_list' => array_reduce($this->config->item('style_lists')[1], function ($result, $style_list) {
				$result += $style_list;
				return $result;
			}, []),
			'gender_list' => $this->gender_list,
			'product_type_list' => $this->product_type_list['2'],
			'patterns' => $this->patterns['2'],
			'sub_patterns' => $this->sub_patterns,
			'user_id' => $this->session->userdata('user_id')
		])->template('clothing/newbrandsonly')->display();
	}

	public function designer_closet() {
		$this->twiggy->template('clothing/designer_closet')->display();
	}
	public function moi_product() {
		$this->twiggy->template('clothing/moi_product')->display();
	}

	public function follow() {
		$this->redirect_if_not_login();
		$this->load->model('Closet_model');
		$user_id = $this->session->userdata('user_id');
		// need to get user's follow list by user_id
		$follow_users = $this->Closet_model->get_followlist(); //var_dump($follow_users);exit(0);
		$follow_designers = (array) json_decode($this->curl->simple_get('/restful/follow_designer/' . $user_id, array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		))); //var_dump($follow_designers);exit(0);
		foreach ($follow_designers as $designer) {
			$tmp = (array) json_decode($this->curl->simple_get('/restful/designer/id/' . $designer->followed, array(), array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));
			$designer->info = $tmp;
			//var_dump($tmp);
		} //var_dump($follow_designers);exit(0);

		$this->twiggy->template('clothing/follow')->set(array(
			'follow_users' => $follow_users,
			'follow_designers' => $follow_designers,
			'user_id' => $this->session->userdata('user_id'),
		))->display();
	}

	public function subpage() {
		$this->twiggy->template('clothing/subpage')->display();
	}
	public function boutiques() {
		$this->twiggy->template('clothing/boutiques')->display();
	}

	// designer product mobile
	public function designer_mobile($product_id) {
		$this->load->model('Clothing_model');
		$user_id = $this->session->userdata('user_id');
		$product = (array) json_decode($this->curl->simple_get('/restful/designer_products/' . $product_id, array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));
		if (($product['product_type'] == 14) || ($product['product_type'] == 15) || ($product['product_type'] == 24) ||
			($product['product_type'] == 31) || ($product['product_type'] == 35)) {
			$size_code = $product['product_type'];
		} else {
			$size_code = $product['product_type'] . $product['item_long'];
		}

		$stock_info = array_reduce($product['Stocks'], function ($result, &$stock) use ($size_code) {
			$stock = (array) $stock;

			if (!array_key_exists($stock['color'], $result['color_list'])) {
				$result['color_list'][$stock['color']] = $stock['color_comment'];
			}
			if (!in_array($stock['size'], $result['size_list'])) {
				array_push($result['size_list'], $stock['size']);
			}
			if (!array_key_exists($stock['size'], $result['measure_labels_list'])) {
				if ($product['group'] == 'Clothing') {
					$result['measure_labels_list'][$stock['size']] = (array) $stock['measure']; //get_semantic_measure_name($size_code, (array) $stock['measure']);
				} else {
					$_tmp_measure = [];
					foreach ((array) $stock['measure'] as $key => $value) {
						if (!empty($value)) {
							$_tmp_measure[$key] = $value . ' ' . (($key == 'weight') ? 'g' : 'cm');
						}
					}

					$result['measure_labels_list'][$stock['size']] = $_tmp_measure + array_reduce((array) $stock['custom_info'], function ($result, $info) {
						if (!empty($info->value)) {
							$result[$info->key] = $info->value . ' ' . $info->unit;
						}
						return $result;
					}, []);
				}
			}
		}); //var_dump($follow);exit(0);

		if ($stock_info['size_type_major'] === 'S/M/L') {
			$xml_order = ['XXS', 'XS', 'S', 'M', 'L', 'XL', 'XXL'];

			usort($stock_info['size_list'], function ($a, $b) use ($xml_order) {
				return array_search($a, $xml_order) > array_search($b, $xml_order);
			});
		} else {
			usort($stock_info['size_list'], function ($a, $b) use ($xml_order) {
				return $a > $b;
			});
		}

		$url_code = $product['Designer']->url_code;

		$data = $this->curl->simple_get(base_url('/restful/may_like/' . $product["product_item_number"]));
		$data = json_decode($data);
		$mayLike = array();
		foreach ($data as $index => $designer_product) {
			if ($index % 6 == 0) {
				$mayLike[$index / 6] = array();
			}

			array_push($mayLike[count($mayLike) - 1], $designer_product);
		}

		$data = $this->curl->simple_get(base_url('/restful/suggested_product/1'));
		$data = json_decode($data);
		$suggestData = array();
		foreach ($data as $index => $suggest_product) {
			if ($index % 6 == 0) {
				$suggestData[$index / 6] = array();
			}

			array_push($suggestData[count($suggestData) - 1], $suggest_product);
		}

		$this->twiggy->set(array(
			'designer' => $product['Designer'],
			'product' => $product,
			'stock_info' => $stock_info,
			"season_list" => $this->season_list,
			"pattern" => $this->config->item('pattern_name')[$product['pattern']],
			'user_id' => $user_id,
			'url_code' => $product['Designer']->url_code,
			'owner_id' => $product['Designer']->user_id,
			'follow' => $follow,
			'mayLike' => $mayLike,
			'suggest' => $suggestData,
		))->template('clothing/designer_product_mobile')->display();

		$this->curl->simple_post(base_url("/restful/recently_viewed"), [
			'product_item_number' => $product['product_item_number'],
		], [
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		]);
	}

	// designer product desktop
	public function designer($product_id) {
		$this->load->model('Clothing_model');
		$user_id = $this->session->userdata('user_id');
		$product = (array) json_decode($this->curl->simple_get('/restful/designer_products/' . $product_id, array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));

		if (($product['product_type'] == 14) || ($product['product_type'] == 15) || ($product['product_type'] == 24) ||
			($product['product_type'] == 31) || ($product['product_type'] == 35) || in_array($product['group'], ['Shoes', 'Bags', 'Accessories'])) {
			$size_code = $product['product_type'];
		} else {
			$size_code = $product['product_type'] . $product['item_long'];
		}

		usort($product['Stocks'], function ($stock_a, $stock_b) {
			if ($stock_a->size_type_major === 'S/M/L') {
				$xml_order = ['XXS', 'XS', 'S', 'M', 'L', 'XL', 'XXL'];
				return array_search($stock_a->size, $xml_order) > array_search($stock_b->size, $xml_order);
			} else {
				return $stock_a->size > $stock_b->size;
			}
		});

		$stock_info = array_reduce($product['Stocks'], function ($result, &$stock) use ($size_code, $product) {
			$stock = (array) $stock;

			if (!array_key_exists($stock['color'], $result['color_list'])) {
				$result['color_list'][$stock['color']] = $stock['color_comment'];
			}
			if (!in_array($stock['size'], $result['size_list'])) {
				array_push($result['size_list'], $stock['size']);
			}
			if (!array_key_exists($stock['size'], $result['measure_labels_list'])) {
				if ($product['group'] == 'Clothing') {
					$result['measure_labels_list'][$stock['size']] = array_merge((array) json_decode($product['measure']), (array) $stock['measure']); //get_semantic_measure_name($size_code, (array) $stock['measure']);
				} else {
					$_tmp_measure = [];
					foreach ((array) $stock['measure'] as $key => $value) {
						if (!empty($value)) {
							$_tmp_measure[$key] = $value . ' ' . (($key == 'weight') ? 'g' : 'cm');
						}
					}

					$result['measure_labels_list'][$stock['size']] = $_tmp_measure + array_reduce((array) $stock['custom_info'], function ($result, $info) {
						if (!empty($info->value)) {
							$result[$info->key] = $info->value . ' ' . $info->unit;
						}
						return $result;
					}, []);
				}

				$result['measure_labels_list'][$stock['size']]['尺寸特殊說明'] = $stock['size_comment'];
			}
			if (!array_key_exists($stock['size'], $result['measure_detail'])) {
				$result['measure_detail'][$stock['size']] = (array) $stock['measure'];
			}

			$result['size_type_major'] = $stock['size_type_major'];
			return $result;
		}, array('color_list' => [], 'size_list' => [], 'measure_labels_list' => [], 'measure_detail' => [], 'size_type_major' => ''));

		$product['trade_rule'] = (array) json_decode($this->curl->simple_get(base_url("/restful/trade/" . $product['user_id']), array(), array(CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'))));

		$this->config->load('clothing_class');

		$style_name = $this->config->item('style_name');

		$product['style'] = array_map(function ($style_value) use ($style_name) {
			return array(
				'style_number' => $style_value,
				'text' => $style_name[(int) $style_value]);
		}, $product['style']);

		$followList = (array) json_decode($this->curl->simple_get(base_url("/restful/follow_designer/" . $user_id), array(), array(CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'))));

		$follow = false;
		if (!empty($this->session->userdata('user_id'))) {
			foreach ($followList as $key => $value) {
				if ($product['Designer']->user_id == $value->followed) {
					$follow = true;
					break;
				}
			}
		} //var_dump($follow);exit(0);

		$url_code = $product['Designer']->url_code;

		$data = $this->curl->simple_get(base_url('/restful/may_like/' . $product["product_item_number"]));
		$data = json_decode($data);
		$mayLike = array();
		foreach ($data as $index => $designer_product) {
			if ($index % 6 == 0) {
				$mayLike[$index / 6] = array();
			}

			array_push($mayLike[count($mayLike) - 1], $designer_product);
		}

		$data = $this->curl->simple_get(base_url('/restful/suggested_product/1'));
		$data = json_decode($data);
		$suggestData = array();
		foreach ($data as $index => $suggest_product) {
			if ($index % 6 == 0) {
				$suggestData[$index / 6] = array();
			}

			array_push($suggestData[count($suggestData) - 1], $suggest_product);
		}

		$this->twiggy->set(array(
			'designer' => $product['Designer'],
			'product' => $product,
			'stock_info' => $stock_info,
			"season_list" => $this->season_list,
			"pattern" => $this->config->item('pattern_name')[$product['pattern']],
			'user_id' => $user_id,
			'url_code' => $url_code,
			'follow' => $follow,
			'mayLike' => $mayLike,
			'suggest' => $suggestData,
			'size_code' => $size_code,
			'owner_id' => $product['Designer']->user_id,
		))->template('clothing/designer_product')->display();

		$this->curl->simple_post(base_url("/restful/recently_viewed"), [
			'product_item_number' => $product['product_item_number'],
		], [
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		]);
	}

	public function buyer($product_id) {
		$this->load->model('Clothing_model');
		$user_id = $this->session->userdata('user_id');
		$product = (array) json_decode($this->curl->simple_get('/restful/buyer_products/' . $product_id, array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));

		if (($product['product_type'] == 14) || ($product['product_type'] == 15) || ($product['product_type'] == 24) ||
			($product['product_type'] == 31) || ($product['product_type'] == 35) || in_array($product['group'], ['Shoes', 'Bags', 'Accessories'])) {
			$size_code = $product['product_type'];
		} else {
			$size_code = $product['product_type'] . $product['item_long'];
		}

		$stock_info = array_reduce($product['Stocks'], function ($result, &$stock) use ($size_code, $product) {
			$stock = (array) $stock;

			if (!array_key_exists($stock['color'], $result['color_list'])) {
				$result['color_list'][$stock['color']] = $stock['color_comment'];
			}
			if (!in_array($stock['size'], $result['size_list'])) {
				array_push($result['size_list'], $stock['size']);
			}
			if (!array_key_exists($stock['size'], $result['measure_labels_list'])) {
				if ($product['group'] == 'Clothing') {
					$result['measure_labels_list'][$stock['size']] = array_merge((array) json_decode($product['measure']), (array) $stock['measure']); //get_semantic_measure_name($size_code, (array) $stock['measure']);
				} else {
					$_tmp_measure = [];
					foreach ((array) $stock['measure'] as $key => $value) {
						if (!empty($value)) {
							$_tmp_measure[$key] = $value . ' ' . (($key == 'weight') ? 'g' : 'cm');
						}
					}

					$result['measure_labels_list'][$stock['size']] = $_tmp_measure + array_reduce((array) $stock['custom_info'], function ($result, $info) {
						if (!empty($info->value)) {
							$result[$info->key] = $info->value . ' ' . $info->unit;
						}
						return $result;
					}, []);
				}

				$result['measure_labels_list'][$stock['size']]['尺寸特殊說明'] = $stock['size_comment'];
			}
			if (!array_key_exists($stock['size'], $result['measure_detail'])) {
				$result['measure_detail'][$stock['size']] = (array) $stock['measure'];
			}

			$result['size_type_major'] = $stock['size_type_major'];
			return $result;
		}, array('color_list' => [], 'size_list' => [], 'measure_labels_list' => [], 'measure_detail' => [], 'size_type_major' => ''));

		$product['trade_rule'] = (array) json_decode($this->curl->simple_get(base_url("/restful/trade/" . $product['user_id']), array(), array(CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'))));

		$this->config->load('clothing_class');

		$style_name = $this->config->item('style_name');

		$product['style'] = array_map(function ($style_value) use ($style_name) {
			return array(
				'style_number' => $style_value,
				'text' => $style_name[(int) $style_value]);
		}, $product['style']);

		$followList = (array) json_decode($this->curl->simple_get(base_url("/restful/follow_buyer/" . $user_id), array(), array(CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'))));

		$follow = false;
		if (!empty($this->session->userdata('user_id'))) {
			foreach ($followList as $key => $value) {
				if ($product['Buyer']->user_id == $value->followed) {
					$follow = true;
					break;
				}
			}
		} //var_dump($follow);exit(0);

		if ($stock_info['size_type_major'] === 'S/M/L') {
			$xml_order = ['XXS', 'XS', 'S', 'M', 'L', 'XL', 'XXL'];

			usort($stock_info['size_list'], function ($a, $b) use ($xml_order) {
				return array_search($a, $xml_order) > array_search($b, $xml_order);
			});
		} else {
			usort($stock_info['size_list'], function ($a, $b) use ($xml_order) {
				return $a == 'custom_made' ? true : $a > $b;
			});
		}

		$url_code = $product['Buyer']->url_code;

		$data = $this->curl->simple_get(base_url('/restful/may_like/' . $product["product_item_number"]));
		$data = json_decode($data);
		$mayLike = array();
		foreach ($data as $index => $buyer_product) {
			if ($index % 6 == 0) {
				$mayLike[$index / 6] = array();
			}

			array_push($mayLike[count($mayLike) - 1], $buyer_product);
		}

		$data = $this->curl->simple_get(base_url('/restful/suggested_product/1'));
		$data = json_decode($data);
		$suggestData = array();
		foreach ($data as $index => $suggest_product) {
			if ($index % 6 == 0) {
				$suggestData[$index / 6] = array();
			}

			array_push($suggestData[count($suggestData) - 1], $suggest_product);
		}

		$this->twiggy->set(array(
			'buyer' => $product['Buyer'],
			'product' => $product,
			'stock_info' => $stock_info,
			"season_list" => $this->season_list,
			"pattern" => $this->config->item('pattern_name')[$product['pattern']],
			'user_id' => $user_id,
			'url_code' => $url_code,
			'follow' => $follow,
			'mayLike' => $mayLike,
			'suggest' => $suggestData,
			'size_code' => $size_code,
			'owner_id' => $product['Buyer']->user_id
		))->template('clothing/buyer_product')->display();

		$this->curl->simple_post(base_url("/restful/recently_viewed"), [
			'product_item_number' => $product['product_item_number'],
		], [
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		]);
	}

	public function newcate() {
		//var_dump($this->ion_auth->logged_in());exit(0);
		$this->twiggy->set([
			'price_range' => json_decode($this->curl->simple_get('/restful/combine/all_clothings/min_and_max')),
			'season_list' => $this->season_list,
			'style_list' => array_reduce($this->config->item('style_lists')[1], function ($result, $style_list) {
				$result += $style_list;
				return $result;
			}, []),
			'gender_list' => $this->gender_list,
			'group_list' => $this->group_list['3'],
			'product_type_list' => $this->product_type_list['3'],
			'patterns' => $this->patterns['3'],
			'sub_patterns' => $this->sub_patterns,
			'logined' => $this->ion_auth->logged_in(),
			'user_id' => $this->session->userdata('user_id')
		])->template('clothing/newcate')->display();
	}

	public function mkplace() {
		$proposal = json_decode($this->curl->simple_get('/restful/proposal/1', array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));

		$this->twiggy->template('clothing/mkplace')->set(array(
			'proposal' => $proposal,
		))->display();
	}

	public function buyerview() {
		$this->twiggy->set([
			'price_range' => json_decode($this->curl->simple_get('/restful/buyer_products/min_and_max', ['launch_way' => 4])),
			'season_list' => $this->season_list,
			'style_list' => array_reduce($this->config->item('style_lists')[1], function ($result, $style_list) {
				$result += $style_list;
				return $result;
			}, []),
			'gender_list' => $this->gender_list,
			'group_list' => $this->group_list['3'],
			'product_type_list' => $this->product_type_list['3'],
			'patterns' => $this->patterns['3'],
			'sub_patterns' => $this->sub_patterns,
			'user_id' => $this->session->userdata('user_id')
		])->template('clothing/buyerview')->display();
	}

	public function bonly() {
		$this->template->render('clothing/bonly', []);
	}
	public function dsner() {
		$this->template->render('clothing/dsner', []);
	}

	function suggested($page = 1) {
		$this->load->database();
		$this->load->model('Product_clothing_model');
		$this->load->model('Suggested_items_model');

		$per_page = 20;

		//       $clothings = $this->Product_clothing_model->get_entries(8, 0);
		$clothings = $this->Suggested_items_model->get_all($per_page, ($page - 1) * $per_page);

		$this->load->library('pagination');
		$config['use_page_numbers'] = TRUE;
		$config['base_url'] = '/clothing/suggested/';
		//        $config['total_rows'] = $this->db->count_all('suggested_items');
		$config['total_rows'] = $this->Suggested_items_model->get_num_of_all();
		$config['per_page'] = $per_page;

		$config['full_tag_open'] = '<ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();
		$data['clothings'] = $clothings;
		$this->template->render('clothing/suggested', $data);
	}

	function soldout($page = 1) {
		$this->load->database();
		$this->load->model('Product_clothing_model');

		$per_page = 20;

		$clothings = $this->Product_clothing_model->get_soldout($per_page, ($page - 1) * $per_page);

		$this->load->library('pagination');
		$config['use_page_numbers'] = TRUE;
		$config['base_url'] = '/clothing/soldout/';
		$config['total_rows'] = $this->Product_clothing_model->get_num_of_soldout();
		$config['per_page'] = $per_page;

		$config['full_tag_open'] = '<ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();
		$data['clothings'] = $clothings;
		$this->template->render('clothing/soldout', $data);
	}

	public function reduction() {

		$this->load->database();
		$this->load->model('Product_clothing_model');

		$clothings = $this->Product_clothing_model->reduction_again_clothing();

		$data['clothings'] = $clothings;
		$this->template->render('clothing/reduction', $data);
	}

	public function brandsonlyview() {
		$this->twiggy->set([
			'season_list' => $this->season_list,
			'style_list' => $this->style_list,
			'gender_list' => $this->gender_list,
			'product_type_list' => $this->product_type_list,
			'patterns' => $this->patterns,
			'sub_patterns' => $this->sub_patterns,
			'user_id' => $this->session->userdata('user_id'),
		])->template('clothing/newbrandsonly')->display();
	}

	public function brandsonly($id = "", $type = "") {
		$this->load->helper('url');

		if (in_array($type, array('previous', 'next'))) {
			$clothing = json_decode($this->curl->simple_get(base_url("/restful/product_clothing/" . $id . "/" . $type)));
			if (!$clothing->id) {
				$clothing->id = $id;
				switch ($type) {
				case 'previous':
					$this->session->set_flashdata('no_previous', 1);
					break;
				case 'next':
					$this->session->set_flashdata('no_next', 1);
					break;
				}
			}
			return redirect(base_url('/clothing/brandsonly/' . $clothing->id), 'refresh');
		}

		$this->load->database();

		$this->load->model('Closet_model');
		$this->load->model('Clothing_model');

		$clothing = (array) json_decode($this->curl->simple_get(base_url("/restful/product_clothing/" . $id), [], [
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		]));

		if (gettype($clothing['measure']) == "object") {
			$clothing['measure'] = (array) $clothing['measure'];
		} else if (gettype($clothing['measure']) == "string") {
			$clothing['measure'] = (array) json_decode($clothing['measure']);
		}

		$clothing['clothing_id'] = $clothing['id'];
		if ($clothing['Stocks']) {
			$clothing = array_merge($clothing, (array) $clothing['Stocks'][0]);
			$clothing['measure'] = (array) $clothing['measure'];
		}

		$clothing['descript'] = ($clothing['descript'] != '') ? get_object_vars($clothing['descript']) : '';
		$clothing['product_type'] = substr($clothing['product_type'], 0, 2);
		if (($clothing['product_type'] == 14) || ($clothing['product_type'] == 15) || ($clothing['product_type'] == 24) ||
			($clothing['product_type'] == 31) || ($clothing['product_type'] == 35)) {
			$clothing['size_code'] = $clothing['product_type'];
		} else {
			$clothing['size_code'] = $clothing['product_type'] . $clothing['item_long'];
		}

		$clothings = (array) json_decode($this->curl->simple_get(base_url("/restful/product_clothing/" . $id . "/similar_clothings")));

		$data['clothing'] = $clothing;
		$data['clothings'] = $clothings;

		$data['owner_id'] = $clothing['user_id'];

		$closet = $this->Closet_model->get_by_user_id($data['owner_id']);

		$data['clothing']['closet_id'] = $closet['id'];
		$data['clothing']['closet_title'] = $closet['title'];
		$data['clothing']['closet_url_code'] = $closet['url_code'];

		$data['clothing']['gender'] = substr($clothing['product_type'], 0, 1);

		switch ($data['clothing']['gender']) {
		case '1':
			$data['href'] = '/clothing/newbrandsonly#?type=women';
			break;

		case '2':
			$data['href'] = '/clothing/newbrandsonly#?type=men';
			break;

		case '3':
			$data['href'] = '/clothing/newbrandsonly#?type=kids';
			break;
		}

		$data['subtype'] = substr($clothing['product_type'], 0, 2);

		$data['pattern_name_mapping'] = $this->config->item('pattern_name');
		$data['subtype_name_mapping'] = array('11' => '上衣', '12' => '洋裝', '15' => '外套', '13' => '褲子', '14' => '裙子', '21' => '上衣', '22' => '襯衫', '24' => '外套', '23' => '褲子', '31' => '連身衣', '32' => '上衣', '33' => '下身', '34' => '洋裝', '35' => '外套');
		$data['measure_labels'] = get_semantic_measure_name($data['clothing']['size_code'], $data['clothing']['measure']);

		$data['no_previous'] = $this->session->flashdata('no_previous');
		$data['no_next'] = $this->session->flashdata('no_next');
		$data['user_id'] = $this->session->userdata('user_id');

		$this->twiggy->set($data);
		$this->twiggy->template('clothing/brandsonly')->display();
		$this->curl->simple_post(base_url("/restful/recently_viewed"), [
			'product_item_number' => $clothing['product_item_number'],
		], [
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		]);
	}

	public function marketplace($id = "", $type = "") {
		$this->load->helper('url');

		if (in_array($type, array('previous', 'next'))) {
			$clothing = json_decode($this->curl->simple_get(base_url("/restful/marketplace/" . $id . "/" . $type)));
			if (!$clothing->id) {
				$clothing->id = $id;
				switch ($type) {
				case 'previous':
					$this->session->set_flashdata('no_previous', 1);
					break;
				case 'next':
					$this->session->set_flashdata('no_next', 1);
					break;
				}
			}

			return redirect(base_url('/clothing/marketplace/' . $clothing->id), 'refresh');
		}

		$this->load->database();

		$this->load->model('Closet_model');

		$clothing = (array) json_decode($this->curl->simple_get(base_url("/restful/marketplace/" . $id)));
		$clothing['measure'] = (array) json_decode($clothing['measure']);

		if ($clothing['Stocks']) {
			$clothing = array_merge($clothing, (array) $clothing['Stocks'][0]);
			$clothing['measure'] = (array) $clothing['measure'];
		}

		if (($clothing['product_type'] == 14) || ($clothing['product_type'] == 15) || ($clothing['product_type'] == 24) ||
			($clothing['product_type'] == 31) || ($clothing['product_type'] == 35)) {
			$clothing['size_code'] = $clothing['product_type'];
		} else {
			$clothing['size_code'] = $clothing['product_type'] . $clothing['item_long'];
		}

		$clothings = (array) json_decode($this->curl->simple_get(base_url("/restful/marketplace/" . $id . "/similar_clothings")));

		$data['clothing'] = $clothing;
		$data['clothings'] = $clothings;

		$data['owner_id'] = $clothing['user_id'];

		$closet = $this->Closet_model->get_by_user_id($data['owner_id']);
		if (!empty($closet)) {
			$data['clothing']['closet_id'] = $closet['id'];
			$data['clothing']['closet_title'] = $closet['title'];
			$data['clothing']['closet_description'] = $closet['description'];
			$data['clothing']['closet_banner_id'] = $closet['banner_photo_file_id'];
			$data['clothing']['closet_url_code'] = $closet['url_code'];
		}

		$data['clothing']['gender'] = substr($clothing['product_type'], 0, 1);

		$data['clothing']['trade_rule'] = (array) json_decode($this->curl->simple_get(base_url("/restful/trade/" . $data['owner_id']), array(), array(CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'))));

		switch ($data['clothing']['gender']) {
		case '1':
			$data['href'] = '/clothing/category/women';
			break;

		case '2':
			$data['href'] = '/clothing/category/men';
			break;

		case '3':
			$data['href'] = '/clothing/category/kids';
			break;
		}

		$data['subtype'] = substr($clothing['product_type'], 0, 2);
		$data['hash'] = '#subtype-' . $data['subtype'];
		$data['pattern_name_mapping'] = $this->config->item('pattern_name');
		$data['subtype_name_mapping'] = array('11' => '上衣', '12' => '洋裝', '15' => '外套', '13' => '褲子', '14' => '裙子', '21' => '上衣', '22' => '襯衫', '24' => '外套', '23' => '褲子', '31' => '連身衣', '32' => '上衣', '33' => '下身', '34' => '洋裝', '35' => '外套');
		$data['measure_labels'] = get_semantic_measure_name($data['clothing']['size_code'], $data['clothing']['measure']);

		//get style list
		$this->config->load('clothing_class');
		$styles = $this->config->item('style_name');
		// map the style from num to character
		foreach ($data['clothing']['style'] as $key => $value) {
			$data['clothing']['style'][$key] = $styles[$value];
		}

		//get free_style list
		$this->load->model('Free_style');
		$query = $this->Free_style->get_free_style();
		$free_styles = [];
		foreach ($query as $style) {
			$free_styles[$style->id] = $style->name;
		}
		$data['clothing']['free_style'] = explode(",", $data['clothing']['free_style']);
		//map free style from num to character
		foreach ($data['clothing']['free_style'] as $key => $value) {
			$data['clothing']['free_style'][$key] = $free_styles[$value];
		}

		//put styles and free_styles together
		$data['clothing']['all_styles'] = array_merge($data['clothing']['style'], $data['clothing']['free_style']);

		$data['no_previous'] = $this->session->flashdata('no_previous');
		$data['no_next'] = $this->session->flashdata('no_next');

		$this->twiggy->set($data);

		$this->twiggy->template('clothing/marketplace')->display();

		$this->curl->simple_post(base_url("/restful/recently_viewed"), [
			'product_item_number' => $clothing['product_item_number'],
		], [
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		]);
	}

	/**
	 * pagination
	 *
	 * @author howtomakeaturn
	 *
	 */
	public function page($number) {
		$this->load->library('pagination');

		$config['base_url'] = '/clothing/page/';
		$config['total_rows'] = 200;
		$config['per_page'] = 20;

		$this->pagination->initialize($config);

		echo $this->pagination->create_links();
	}

	/**
	 * Quick register for launch page
	 *
	 * @author howtomakeaturn
	 *
	 */
	public function quick_register() {
		$this->form_validation->set_rules('email', '電子郵件', 'required|valid_email');
		$this->form_validation->set_rules('password', '密碼', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']');
		$redirect_url = urldecode($this->input->post('redirect_url'));

		if ($this->form_validation->run() == true) {
			$username = '';
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			$additional_data = array(

				//                'user_name' => $this->input->post('user_name')
				'user_name' => "GetMore會員");
		}
		if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data) !== FALSE) {
			//check to see if we are creating the user
			//redirect them back to the admin page
			redirect($redirect_url, 'refresh');
		} else {
			//display the create user form
			//set the flash data error message if there is one
			$this->data['reg_message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->session->set_flashdata('reg_message', $this->data['reg_message']);
			redirect($redirect_url, 'refresh');
		}
	}

	/*
		 * hack CI's pagination library
		 * insert hash sign before page number, for ajax loading
		 * @author howtomakeaturn
	*/

	// actually, it's useless now since pagination is created with javascript now
	private function transform_pagination($original_pagination, $season, $color, $style, $subtype, $pattern, $keyword, $item_long) {
		$reg_pattern = '/category\/(\w*)\/([0-9]*)/';
		$replacement = 'category/$1/#offset-$2';

		/*
			if (!empty($color)){
			$replacement = $replacement . '&color-' . $color;
			}
			if (!empty($style)){
			$replacement = $replacement . '&style-' . $style;
			}
			if (!empty($season)){
			$replacement = $replacement . '&season-' . $season;
			}
			if (!empty($subtype)){
			$replacement = $replacement . '&subtype-' . $subtype;
			}
			if (!empty($pattern)){
			$replacement = $replacement . '&pattern-' . $pattern;
			}
			if (!empty($keyword)){
			$replacement = $replacement . '&keyword-' . $keyword;
			}
			if (!empty($item_long)){
			$replacement = $replacement . '&item_long-' . $item_long;
			}
		*/
		$new_pagination = preg_replace($reg_pattern, $replacement, $original_pagination);
		//show_error($new_pagination);
		return $new_pagination;
	}

	/*
		 * a super stupid function to get number for pagination
		 * @author howtomakeaturn
	*/
	private function get_num_by_type($type) {

		if ($type === 'new') {
			$query = $this->db
			// ->order_by("files", "desc")
				->order_by("add_time", "desc")->where_in('lock_status', [0, 1, 2])->get('product_clothing');
		} else if ($type === 'men') {
			$query = $this->db
			// ->order_by("files", "desc")
				->order_by("add_time", "desc")->where_in('lock_status', [0, 1, 2])->like('product_type', '2', 'after')->get('product_clothing');
		} else if ($type === 'women') {
			$query = $this->db
			// ->order_by("files", "desc")
				->order_by("add_time", "desc")->where_in('lock_status', [0, 1, 2])->like('product_type', '1', 'after')->get('product_clothing');
		} else if ($type === 'kids') {
			$query = $this->db
			// ->order_by("files", "desc")
				->order_by("add_time", "desc")->where_in('lock_status', [0, 1, 2])->like('product_type', '3', 'after')->get('product_clothing');
		}
		$row = $query->result_array();
		return count($row);
	}

	/**
	 * Default Static Page
	 *
	 * @author appleboy
	 *
	 */
	public function url($name = null) {
		$this->template->set('is_hidden_header', true);
		if (empty($name)) {
			redirect('/', 'refresh');
		}

		$row = $this->lib_static_url->select('id, title, url, content')->where('url', $name)->items()->row_array();

		if (empty($row) or empty($row['content'])) {
			redirect('/', 'refresh');
		}

		$this->template->render('home/static', $row);
	}
	function toggle_like_clothing() {
		$clothing_id = $this->input->post('clothing_id');
		$user_id = $this->session->userdata('user_id');
		$this->load->model('Product_clothing_model');
		try {
			$clothing_array = $this->Product_clothing_model->toggle_like_clothing($user_id, $clothing_id);
		} catch (Exception $e) {
			show_error($e);
		}
	}

	function just_like_clothing() {
		$clothing_id = $this->input->post('clothing_id');
		$user_id = $this->session->userdata('user_id');
		$this->load->model('Product_clothing_model');
		try {
			$clothing_array = $this->Product_clothing_model->just_like_clothing($user_id, $clothing_id);
		} catch (Exception $e) {
			show_error($e);
		}
	}

	public function brandslist() {
		$brandsData = file_get_contents("http://" . $_SERVER['HTTP_HOST'] . "/restful/brands/");
		$brandsData = json_decode($brandsData);
		$brands = array();
		foreach ($brandsData as $key => $value) {
			if ($value->avalible && $value->approved) {
				if (empty($brands[$value->category_id])) {
					$brands[$value->category_id] = array();
				}
				array_push($brands[$value->category_id], $value);
			}
		}

		$designerData = file_get_contents("http://" . $_SERVER['HTTP_HOST'] . "/restful/designer/");
		$designerData = json_decode($designerData);

		$this->twiggy->set('designers', $designerData);
		$this->twiggy->set('brands', $brands);
		$this->twiggy->template('clothing/brandslist')->display();

	}

	public function dsnerlist() {
		$designerData = file_get_contents("http://" . $_SERVER['HTTP_HOST'] . "/restful/designer/");
		$designerData = json_decode($designerData);

		$this->twiggy->set('designers', $designerData);
		$this->twiggy->template('clothing/brandslist')->display();
	}

	public function clothinglist() {
		$brandsData = file_get_contents("http://" . $_SERVER['HTTP_HOST'] . "/restful/brands/");
		$brandsData = json_decode($brandsData);
		$brands = array();
		foreach ($brandsData as $key => $value) {
			if ($value->avalible && $value->approved) {
				if (empty($brands[$value->category_id])) {
					$brands[$value->category_id] = array();
				}
				array_push($brands[$value->category_id], $value);
			}
		}

		$this->twiggy->set('brands', $brands);
		$this->twiggy->template('clothing/brandslist')->display();
	}

	public function search($keyword) {
		$this->load->database();
		$this->load->model('Product_clothing_model');
		$this->load->model('Marketplace_model');

		$keyword = urldecode($keyword);
		$clothings = $this->Product_clothing_model->getBySearch($keyword);
		// $mkclothings = $this->Marketplace_model->getBySearch($keyword);
		// $clothings = array_merge($clothings, $mkclothings);

		$sort_type = $this->input->get('s');
		if ($sort_type == 'new') {
		} else if ($sort_type == 'early') {
			usort($clothings, function ($a, $b) {
				return $a['publication_time'] > $b['publication_time'];
			});
		}

		// high
		else if ($sort_type == 'h') {
			usort($clothings, function ($a, $b) {
				return $b['product_sell_price'] - $a['product_sell_price'];
			});
		}

		// low
		else if ($sort_type == 'l') {
			usort($clothings, function ($a, $b) {
				return $a['product_sell_price'] - $b['product_sell_price'];
			});
		} else if ($sort_type == 'rec') {
			usort($clothings, function ($a, $b) {
				return $b['is_recommended'] - $a['is_recommended'];
			});
		}

		$data['clothings'] = $clothings;
		$data['keyword'] = $keyword;
		$data['mkclothings'] = $mkclothings;
		$this->template->render('clothing/search', $data);
	}
}
