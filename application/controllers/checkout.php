<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

session_start();
class Checkout extends MY_Controller {

	/**
	 * Default __construct
	 *
	 * @author howtomakeaturn
	 **/
	public function __construct() {
		parent::__construct();
		$this->load->library(array('ion_auth'));
		$this->load->library('session');

		$this->load->helper('url');

		if (!$this->ion_auth->logged_in()) {
			$this->system->redirect();
		}

		$this->load->spark('curl');
		$this->curl->option(CURLOPT_COOKIE, http_build_query($_COOKIE, '', ';'));

		// $this->load->library(array('ion_auth', 'session', 'form_validation', 'lib_cart', 'lib_event', 'lib_product', 'lib_order'));
		// $this->load->library(array('lib_static_url', 'lib_subject', 'lib_config', 'lib_event', 'files_lib'));
		// $this->load->library(array('form_validation'));
		// $this->load->config('clothing_class');
		// $this->template->set('is_home', true);

		$this->load->spark('Twiggy/0.8.5');
		$this->twiggy->theme('');
		$this->twiggy->set(array('lang' => 'zh-tw', 'meta_charset' => 'UTF-8', 'site_title' => 'Getmore - 二手衣時尚購物網站/綠色時尚/買二手衣/賣二手衣', 'site_description' => 'GetMore 是亞洲第一個二手衣時尚買賣網站，提供穿二手衣的生活新理念，輕鬆享受二手衣獨特、環保、時尚的二次生活。', 'site_keywords' => '時尚,衣著,服飾,時裝,流行,褲子,買二手衣,賣二手衣,環保,電子商務,startup,二次時尚', 'logined' => $this->ion_auth->logged_in()), NULL, TRUE);
	}

	public function coupon_code() {
		$code = $this->input->post('coupon_code');
		$items_price = $this->input->post('items_price');
		$this->load->model('Coupon_model', 'coupon');
		try {
			$this->coupon->validate_coupon_code($code);

			$coupon = $this->coupon->get_by('code', $code);

			if ($coupon->minimum_charge > $items_price) {
				throw new Exception('not satisfy minimum charge', 4);
			}

			$response = array('status' => 'success');
			$response['coupon'] = $coupon;
			echo json_encode($response);
		} catch (Exception $e) {
			$response = array('status' => 'error');
			$error_code = $e->getCode();
			if ($error_code == 1) {
				$response['message'] = '無此禮券';
			} else if ($error_code == 2) {
				$response['message'] = '禮券已過期';
			} else if ($error_code == 3) {
				$response['message'] = '禮券已用完';
			} else if ($error_code == 4) {
				$response['message'] = '未達禮券低消';
			} else {
				$response['message'] = '失敗';
			}
			echo json_encode($response);
		}
	}

	public function first() {
		$account_balance = json_decode($this->curl->simple_get('/restful/account', array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));

		$bonus_points = json_decode($this->curl->simple_get('/restful/bonus', array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));

		$user = json_decode($this->curl->simple_get('/restful/user/' . $this->session->userdata('user_id'), [], [
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		]));

		$soldout_clothings = $this->session->flashdata('soldout_clothings');

		$this->twiggy->set(array(
			'account_balance' => $account_balance,
			'bonus_points' => $bonus_points,
			'soldout_clothings' => $soldout_clothings,
			'virgin_checkout' => !$user->buy_count,
		));
		$this->twiggy->template('checkout/first')->display();
	}

	public function second() {
		$data = (array) json_decode($this->input->post("json_data"));

		if (empty($data)) {
			return redirect('/checkout/first/', 'refresh');
		}

		if (empty($data['clothings'])) {
			return $this->template->render('/sellorder/info/404', array(
				"title" => "很抱歉 您的購物車是空的",
				"message" => "麻煩您重新進行購買",
			));
		}

		$clothings = (array) json_decode($this->curl->simple_get('/restful/combine/all_clothings', array(
			'product_item_number' => implode(",", array_keys((array) $data['clothings'])),
		)));

		$clothings = array_map(function ($clothing) use ($data) {
			return array_merge(get_object_vars($clothing), (array) get_object_vars($data['clothings'])[$clothing->product_item_number]);
		}, $clothings);

		$designer_ids = array_reduce($clothings, function ($result, $clothing) {
			if ($clothing['launch_way'] !== 3) {
				return $result;
			}

			array_push($result, $clothing['user_id']);
			return $result;
		}, []);

		$buyer_ids = array_reduce($clothings, function ($result, $clothing) {
			if ($clothing['launch_way'] !== 4) {
				return $result;
			}

			array_push($result, $clothing['user_id']);
			return $result;
		}, []);

		$designers = (array) json_decode($this->curl->simple_get('/restful/designer', [
			'user_id' => $designer_ids,
		]));

		$buyers = (array) json_decode($this->curl->simple_get('/restful/buyer', [
			'user_id' => $buyer_ids,
		]));

		$designers = array_map(function ($designer) {
			return (array) $designer;
		}, $designers);

		$buyers = array_map(function ($buyer) {
			return (array) $buyer;
		}, $buyers);

		$stocks = json_decode($this->curl->simple_get('/restful/stock', array(
			'id' => array_reduce(array_values((array) $data['clothings']), function ($result, $clothing) {
				$result[] = $clothing->stock_id;
				return $result;
			}, [])
		)));

		$stocks = array_reduce($stocks, function ($result, $stock) {
			$result[$stock->id] = (array) $stock;
			return $result;
		}, []);

		$clothings = array_map(function ($clothing) use ($stocks) {
			if (!array_key_exists('stock_id', $clothing)) {
				return $clothing;
			}
			unset($stocks[$clothing['stock_id']]['number']);
			unset($stocks[$clothing['stock_id']]['id']);

			return array_merge($clothing, $stocks[$clothing['stock_id']]);
		}, $clothings);

		if (empty($clothings)) {
			return $this->template->render('/sellorder/info/404', array(
				"title" => "很抱歉 您的購物車是空的",
				"message" => "麻煩您重新進行購買",
			));
		}

		$soldout_clothings = array_values(array_filter($clothings, function ($clothing) use ($stocks) {
			return $clothing['lock_status'] !== 0 || array_key_exists('stock_id', $clothing) && $clothing['number'] > $stocks[$clothing['stock_id']]['number'];
		}));

		if (!empty($soldout_clothings)) {
			$this->session->set_flashdata('soldout_clothings', $soldout_clothings);
			redirect('/checkout/first#' . $soldout_clothings[0]['product_item_number'], 'refresh');
		}

		$items_price = 0;

		$closets = array_reduce($clothings, function ($result, &$clothing) use (&$designers, &$items_price, &$buyers) {
			$total_price = $clothing['product_sell_price'] * $clothing['number'];
			$items_price += $total_price;
			switch ($clothing['launch_way']) {
			case 1:
				$result['marketplace'] += $total_price;
				break;
			case 2:
				$result['getmore']['total_price'] += $total_price;

				if ($result['getmore']['total_price'] >= 1000) {
					$result['getmore']['shipping_price'] = 0;
				} else {
					$result['getmore']['shipping_price'] = 80;
				}
				break;
			case 3:
				$index = array_search($clothing['user_id'], array_column($result['designers'], 'user_id'));
				$designer_index = array_search($clothing['user_id'], array_column($designers, 'user_id'));

				if ($index < 0 || !$index) {
					$data = [
						'user_id' => $clothing['user_id'],
						'total_price' => $total_price,
					];

					if ($data['total_price'] >= $designers[$designer_index]['free_shipping']) {
						$data['shipping_price'] = 0;
					} else {
						$data['shipping_price'] = $designers[$designer_index]['shipping_price'];
					}

					$result['designers'][$designer_index] = $data;
				} else {
					$result['designers'][$designer_index]['total_price'] += $total_price;

					if ($result['designers'][$designer_index]['total_price'] >= $designers[$designer_index]['free_shipping']) {
						$result['designers'][$designer_index]['shipping_price'] = 0;
					} else {
						$result['designers'][$designer_index]['shipping_price'] = $designers[$designer_index]['shipping_price'];
					}
				}

				break;
			case 4:
				$index = array_search($clothing['user_id'], array_column($result['buyers'], 'user_id'));
				$buyer_index = array_search($clothing['user_id'], array_column($buyers, 'user_id'));

				if ($index < 0 || !$index) {
					$data = [
						'user_id' => $clothing['user_id'],
						'total_price' => $total_price,
					];

					if ($data['total_price'] >= $buyers[$buyer_index]['free_shipping']) {
						$data['shipping_price'] = 0;
					} else {
						$data['shipping_price'] = $buyers[$buyer_index]['shipping_price'];
					}

					$result['buyers'][$buyer_index] = $data;
				} else {
					$result['buyers'][$buyer_index]['total_price'] += $total_price;

					if ($result['buyers'][$buyer_index]['total_price'] >= $buyers[$buyer_index]['free_shipping']) {
						$result['buyers'][$buyer_index]['shipping_price'] = 0;
					} else {
						$result['buyers'][$buyer_index]['shipping_price'] = $buyers[$buyer_index]['shipping_price'];
					}
				}

				break;
			}

			return $result;
		}, ['getmore' => ['total_price' => 0, 'shipping_price' => 0], 'marketplace' => 0, 'designers' => [], 'buyers' => []]);

		$shipping_price = $closets['getmore']['shipping_price'] + array_sum(array_column($closets['designers'], 'shipping_price')) + array_sum(array_column($closets['buyers'], 'shipping_price'));

		if ($data['account_balance']) {
			$total_account_balance = json_decode($this->curl->simple_get('/restful/account', array(), array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));

			$account_balance = ($total_account_balance->balance - $data['account_balance']) >= 0 ? $data['account_balance'] : (int) $total_account_balance->balance;
		}

		if ($data['bonus_points']) {
			$total_bonus_points = json_decode($this->curl->simple_get('/restful/bonus', array(), array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));

			$bonus_points = (int) ($total_bonus_points->bonus - $data['bonus_points']) >= 0 ? $data['bonus_points'] : (int) $total_bonus_points->bonus;
		}

		$coupon_amount = 0;

		if ($data['coupon_code']) {
			$coupon = json_decode($this->curl->simple_get('/restful/coupon/' . $data['coupon_code'], array(), array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));

			if ($coupon->union) {
				if (strpos($coupon->limit, 'marketplace') !== false && $closets['marketplace'] || strpos($coupon->limit, 'product_clothing') !== false && $closets['getmore']['total_price'] || strpos($coupon->limit, 'designer') !== false && !empty($closets['designers']) || strpos($coupon->limit, 'buyer') !== false && !empty($closets['buyers'])) {
					if ($coupon->discount) {
						$coupon_amount = floor((abs($items_price) + abs($shipping_price)) * (1 - $coupon->discount / 100));
					} else {
						$coupon_amount = $coupon->amount;
					}
				}
			} else {
				if (strpos($coupon->limit, 'marketplace') !== false && $closets['marketplace']) {
					if ($coupon->discount) {
						$coupon_amount = floor($closets['marketplace'] * (1 - $coupon->discount / 100));
					} else {
						$coupon_amount = $coupon->amount;
					}
				}

				if (strpos($coupon->limit, 'product_clothing') !== false && $closets['getmore']['total_price']) {
					if ($coupon->discount) {
						$coupon_amount = floor(($closets['getmore']['total_price'] + $closets['getmore']['shipping_price']) * (1 - $coupon->discount / 100));
					} else {
						$coupon_amount = $coupon->amount;
					}
				}

				if (strpos($coupon->limit, 'designer') !== false && !empty($closets['designers'])) {
					if ($coupon->discount) {
						$coupon_amount = floor((array_sum(array_column($closets['designers'], 'total_price')) + array_sum(array_column($closets['designers'], 'shipping_price'))) * (1 - $coupon->discount / 100));
					} else {
						$coupon_amount = $coupon->amount;
					}
				}

				if (strpos($coupon->limit, 'buyer') !== false && !empty($closets['buyers'])) {
					if ($coupon->discount) {
						$coupon_amount = floor((array_sum(array_column($closets['buyers'], 'total_price')) + array_sum(array_column($closets['buyers'], 'shipping_price'))) * (1 - $coupon->discount / 100));
					} else {
						$coupon_amount = $coupon->amount;
					}
				}
			}

			if (!$coupon->union && !$coupon->limit) {
				if ($coupon->discount) {
					$coupon_amount = (abs($items_price) + abs($shipping_price)) * (1 - $coupon->discount / 100);
				} else {
					$coupon_amount = $coupon->amount;
				}
			}
		}

		if ($coupon_amount) {
			$coupon->amount = $coupon_amount;
		} else {
			$coupon = null;
		}

		$user = json_decode($this->curl->simple_get('/restful/user/' . $this->session->userdata('user_id'), [], [
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		]));

		$virgin_checkout_price = floor(!$user->buy_count ? ($items_price * 0.05 >= 250 ? 250 : $items_price * 0.05) : 0);

		$total_price = abs($items_price) + abs($shipping_price) - abs($coupon_amount) - abs($account_balance) - abs((int) ($bonus_points / 100)) - $virgin_checkout_price;
		
		$this->session->set_flashdata('order', array(
			'clothings' => $clothings,
			'items_price' => $items_price,
			'account_balance' => $account_balance,
			'bonus_points' => $bonus_points,
			'first_checkout_discount' => $virgin_checkout_price,
			'coupon' => $coupon,
			'total_price' => $total_price,
			'shipping_price' => $shipping_price,
			'seller_data' => $closets['designers'] + $closets['buyers']
		));

		$this->twiggy->set(array(
			'items_price' => $items_price,
			'virgin_checkout_price' => $virgin_checkout_price,
			'account_balance' => $account_balance,
			'bonus_points' => $bonus_points,
			'coupon_amount' => $coupon_amount,
			'no_pay_at_home' => $closets['marketplace'] != 0 || $closets['getmore']['total_price'] >= 10000 || !empty($closets['designers']),
			'total_price' => $total_price,
			'shipping_price' => $shipping_price,
		));
		$this->twiggy->template('checkout/second')->display();
	}

	public function third() {
		if ($info = $this->input->post()) {
			$order = array_merge($this->session->flashdata('order'), $info);

			$order_number = json_decode($this->curl->simple_post('/restful/buy_order/', array(
				'order' => json_encode($order),
			), array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));
			$this->pay($order_number, $order['payment_method']);
		} else {
			redirect('/checkout/first/', 'refresh');
		}
	}

	public function result($order_number) {
		$order = json_decode($this->curl->simple_get(base_url("/restful/buy_order/order_number/" . $order_number . "/all")));

		if (($order->status == 1) && ($order->payment_method == 'credit-cart')) {
			redirect('/checkout/first/', 'refresh');
		} else {
			$this->load->model('Buy_order_model');

			$this->Buy_order_model->notify_the_buyorder_is_created($order);

			$data['order'] = $order;
			$data['clear_cart'] = true; // finish checkout process, clear the cart

			$this->template->render('checkout/result', $data);
		}
	}

	/*
		 * allow user to pay again before order expired
		 * @author howtomakeaturn
	*/

	public function restart_for_pay() {
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$payment_method = $this->input->post("payment_method");
			$buy_order_number = $this->input->post("order_number");
			$this->session->set_userdata("payment_method", $payment_method);

			$this->load->database();
			$this->load->model('Buy_order_model');

			$order = $this->Buy_order_model->get_one_by_order_number($buy_order_number);

			$this->Buy_order_model->reset_expiration_time($order['id']);

			// save to session for fourth step
			$this->session->set_userdata("buy_order_number", $order['order_number']);

			$this->pay($order['order_number']);
		} else if ($this->input->server('REQUEST_METHOD') == 'GET') {
			redirect('/', 'refresh');
		}
	}

	/**
	 * Order Payment
	 * copy from hellocheers/controller/order
	 */
	public function pay($order_number, $payment_method) {
		if ($payment_method == 'line_pay') {
			redirect('https://' . $this->config->item('payment_url') . 'checkout/desktop/' . $order_number . '?redirect=' . base_url());
		} else {
			redirect('http://' . $this->config->item('payment_url') . 'checkout/desktop/' . $order_number . '?redirect=' . base_url());
		}
	}
}
