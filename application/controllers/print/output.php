<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Output extends MY_Controller
{

    public $data = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('lib_cart', 'lib_event', 'lib_product', 'lib_order', 'lib_category', 'excel'));
        $this->load->helper('download');
        $this->template->set_layout('template/print');
        $this->data = array();
    }

    public function event($type = 'user', $format = 'html')
    {
        $id = $this->input->get_post('id') ? $this->input->get_post('id') : null;
        $id = explode(',', $id);

        if (!isset($id)) {
            return false;
        }

        $event = $this->lib_event->select("category.tw_name as category_name, events.*")
                ->where('events.id', $id)
                ->events()->row_array();

        $event['items'] = $this->lib_event->select("events_users.order_id, events_users.user_name, " .
                "events_users.user_phone, orders.user_name as order_user_name, email, orders.user_telephone, " .
                "orders.user_mobile_phone, orders.add_time")
                ->where('events_users.event_id', $id)
                ->order_by('orders.add_time')
                ->registers()->result_array();

        $template = 'print/event/user';

        if ($format == 'excel') {
            $i = 1;

            // add excel header
            $this->data[] = array('編號', '姓名', '手機', '購買人', 'Email', '電話', '手機');
            foreach ($event['items'] as $row) {
                // add excel data
                $this->data[] = array($i, $row['user_name'], $row['user_phone'], $row['order_user_name'], $row['email'], $row['user_telephone'], $row['user_mobile_phone']);
                $i++;
            }

            $this->excel->addArray($this->data);
            $file_name = 'event_user_list_' . time() . '.xls';
        }

        $output = array(
            "success_text" => "ok",
            "item" => $event
        );

        switch ($format) {
            case 'excel':
                $this->excel->generateXML($file_name);
                break;
            case 'html':
            default:
                $this->template->render($template, $output);
                break;
        }
    }

    public function order($type = 'product', $format = 'html')
    {
        $id = $this->input->get_post('order_id') ? $this->input->get_post('order_id') : null;
        if (is_array($id) or strpos($id, ',')) {
            $id = explode(',', $id);
        }

        if (!isset($id)) {
            return false;
        }

        switch ($type) {
            case 'list':
                $order = $this->lib_order->select('orders.id, delivery_name, delivery_address, delivery_mobile_phone, delivery_telephone, delivery_number')
                     ->where('orders.id', $id)
                     ->orders()->result_array();

                $template = 'print/order/list';

                if ($format == 'excel') {
                    $i = 1;

                    // add excel header
                    $this->data[] = array('編號', '託運編號', '姓名', '電話', '手機', '地址');
                    foreach ($order as $row) {
                        // add excel data
                        $this->data[] = array($i, $row['delivery_number'], $row['delivery_name'], $row['delivery_telephone'], $row['delivery_mobile_phone'], $row['delivery_address']);
                        $i++;
                    }

                    $this->excel->addArray($this->data);
                    $file_name = 'order_list_' . time() . '.xls';
                }
                break;
            case 'products':
                $order = $this->lib_order->select('order_id, sum(product_quantity) as product_quantity, product_title, product_item_number')
                     ->where('order_id', $id)
                     ->group_by('product_id')
                     ->orders('product')->result_array();

                $template = 'print/order/products';

                if ($format == 'excel') {
                    $i = 1;

                    // add excel header
                    $this->data[] = array($i, $row['product_title'], $row['product_item_number'], '', $row['product_quantity'], '', '');
                    foreach ($order as $row) {
                        // add excel data
                        $this->data[] = array($i, $row['product_title'], $row['product_item_number'], '', $row['product_quantity'], '', '');
                        $i++;
                    }

                    $this->excel->addArray($this->data);
                    $file_name = 'order_product_' . time() . '.xls';
                }
                break;
            case 'product':
            default:
                $order = $this->lib_order->select('orders.id, user_id, users.email, orders.user_name, user_address, user_mobile_phone, total_price, status_id, ' .
                     'user_telephone, delivery_name, delivery_address, delivery_mobile_phone, delivery_telephone, payment_method, transportation_charge, ' .
                     'uniform_invoice_type, orders_status.name as order_status, delivery_number, date_purchased, date_finished, orders.edit_time')
                     ->where('orders.id', $id)
                     ->orders()->row_array();

                $product = $this->lib_order->select('*')->where('order_id', $id)->orders('product')->result_array();

                $order['number'] = sprintf("%08s", $id);
                $order['products'] = $product;

                $template = 'print/order/product';
                break;
        }

        $output = array(
            "success_text" => "ok",
            "item" => $order
        );

        switch ($format) {
            case 'excel':
                $this->excel->generateXML($file_name);
                break;
            case 'html':
            default:
                $this->template->render($template, $output);
                break;
        }
    }
}
