<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends MY_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->library(array('lib_cart', 'lib_product', 'files_lib'));
        $this->list_array = array('detail', 'delivery');
        $this->template->set('is_subject', true);
        $this->template->set('is_hidden_header', true);
        redirect('/');
    }

    public function index()
    {
        redirect('/');
    }

    /**
     * Single Product
     *
     * @param  int
     * @return json
     * @author appleboy
     **/
    public function single($id = '')
    {
        $id = intval($id);
        ($id <= 0) and redirect('/');

        $row = $this->lib_product->select('category.tw_name as category_name, product_subjects.category_id, product_subjects.title as subject_name, products.*')->where('products.id', $id)->products()->row_array();

        // empty row or product isn't enable
        if (empty($row) or !(_IS_ENABLED & $row['options'])) {
            redirect('/');
        }

        // set category id.
        $this->template->set('category_id', $row['category_id']);
        // get image list
        if ($row['file_list'] != '') {
            $files = $this->files_lib->where('file_id', explode(',', $row['file_list']))->order_by_field('file_id', $row['file_list'])->files()->result_array();
            $row['image_list'] = $files;
            $row['image_list_count'] = count($files);
        }

        // handle detail informtion
        foreach ($this->list_array as $item) {
            // set default value
            $row[$item] = empty($row[$item]) ? array() : json_decode($row[$item]);
        }

        // handle count dropdown
        $row['order_count'] = (empty($row['inventory_amount']) or $row['inventory_amount'] <= '0') ? '商品已售完' : $this->lib_cart->format_dropdown($row['inventory_amount'], 'product');

        // set default value
        ($row['file_list'] == "") and $row['image_list'] = array();
        ($row['file_list'] == "") and $row['image_list_count'] = 0;

        // handle product message and status
        $row['status'] = 0;
        if (empty($row['inventory_amount']) or $row['inventory_amount'] <= '0') {
            $row['message'] = '商品已售完';
        } else {
            $row['status'] = 1;
        }

        $this->template->add_css('/assets/javascript/libs/jquery/jqzoom/css/jquery.jqzoom.css');
        $this->template->render('product/single', $row);
    }
}
