<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends MY_Controller
{
    private $buy_order_title = array('序號', '訂單編號', '訂購時間', '買家姓名', '買家id', '購買件數', '商品總金額', '運費', '禮券金額', '抵扣金額', '付款方式', '第幾次購買');
    private $product_clothing = array('序號', '洗衣編號', '商品編號', '商品敘述', '品牌', '類別');
    private $return_request = array('序號', '訂單編號', '退貨處理時間', '買家id', '買家姓名', '退貨件數', '退貨金額', '是否全退');
    private $selled_products = array('序號', '洗衣編號', '商品編號', '商品敘述', '品牌', '售價', '賣家id', '買家id', '類別');
    private $sellorders = array('序號', '申請時間', '貨單編號', '會員帳號', '會員姓名', '代售件數', '洗衣金額', '運費', '禮券金額', '抵扣金額', '第幾次賣');

    public function __construct()
    {
        parent::__construct();
        // $this->load->library(array('lib_subject', 'lib_product', 'files_lib', 'lib_category'));
        $this->load->library(array('excel'));
        $this->load->model('report_model');
        $this->load->config('clothing_class');
    }

    public function index()
    {
        redirect('/');
    }

    public function buy_order(){
        $condition = $this->input->post();
        
        $buy_order = $this->report_model->buy_order($condition['begin_time'], $condition['end_time'], $condition['payment'], $condition['user_id']);
        
        $buy_counts = array();

        foreach ($buy_order as $index => $order) {
            $user_id = $order['user_id'];
            if(!isset($buy_counts[$user_id])) $buy_counts[$user_id] = 1;
            else $buy_counts[$user_id]++;

            $order['buy_count'] = $buy_counts[$user_id];
            $buy_order[$index] = $order;
        }

        $this->_generate_excel('已完成訂單', $this->buy_order_title, $buy_order, 'buy_order');
    }

    public function product_clothing(){
        $condition = $this->input->post();

        $product_clothing = $this->report_model->product_clothing($condition['begin_time'], $condition['end_time'], $condition['product_type']);
        
        $this->_generate_excel('庫內商品', $this->product_clothing, $product_clothing, 'product_clothing');
    }

    public function return_request(){
        $condition = $this->input->post();

        $return_request = $this->report_model->return_request($condition['begin_time'], $condition['end_time'], $condition['user_id'], $condition['status']);
        
        $this->_generate_excel('退貨', $this->return_request, $return_request, 'return_request');
    }

    public function selled_products(){
        $condition = $this->input->post();

        $selled_products = $this->report_model->selled_products($condition['begin_time'], $condition['end_time'], $condition['product_type']);
        
        $this->_generate_excel('售出商品', $this->selled_products, $selled_products, 'selled_products');
    }

    public function sellorders(){
        $condition = $this->input->post();

        $sellorders = $this->report_model->sellorders($condition['begin_time'], $condition['end_time'], $condition['user_id']);

        $this->_generate_excel('貨單', $this->sellorders, $sellorders, 'sellorders');
    }

    private function _generate_excel(string $sheetTitle, array $tableHeader, array $data, string $filename){
        $this->excel->setWorksheetTitle($sheetTitle);
        $this->excel->addRow($tableHeader);
        
        foreach ($data as $index => $each_data) {
            $this->excel->addRow(array_values($each_data));
        }

        $this->output->set_output($this->excel->generateXML($filename));
    }
}
