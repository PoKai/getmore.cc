<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backup extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $token = $this->input->get('token');
        if ($token!=='getmorebackup'){
            exit('invalid token');
        }
    }

    /*
     * Get all file names related to clothing id between parameters.
     */    
    function clothing_files(){
        $from = $this->input->get('from');
        $to = $this->input->get('to');
        $query = $this->db->from('product_clothing');

        if ($from){
            $query = $query->where('id >=', $from);
        }
        if ($to){
            $query = $query->where('id <', $to);
        }
   
        $clothings = $query->get()->result_array();
        $file_names = array();

        foreach($clothings as $clothing){
            $file_ids = explode(',', $clothing['files']);
            $files = $this->db->select('file_name')
                                          ->from('files')
                                          ->where_in('file_id', $file_ids)
                                          ->get()->result_array();
            foreach($files as $file){
                array_push($file_names, $file['file_name']);
            }         
        }
        
        echo json_encode($file_names);
    }
    
}
