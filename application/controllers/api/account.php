<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Account extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('ion_auth');
        if ( !$this->ion_auth->is_admin()){
            exit('forbidden');
        }
        
        $this->load->database();
        $this->load->model('Transaction_model', 'transaction');
        $this->load->model('Account_model');
    }

    public function getByUserId_get(){
        $user_id = $this->input->get('user_id');
        $account = $this->Account_model->get_by_user_id_without_clothing_id($user_id);
        $output = array(
            'account' => $account
        );
        $this->response($output);
    }

    public function getTransactions_get()
    {
        $transactions = $this->transaction->latest_transactions()->get_all();
        $output = array(
            'transactions' => $transactions
        );
        $this->response($output);
    }
    
    public function addTransaction_post()
    {
        $user_id = $this->input->post('user_id');
        $name = $this->input->post('name');
        $related_number = $this->input->post('related_number');
        $amount = (int) $this->input->post('amount');
        $note = $this->input->post('note');
        
        try{
            $this->transaction->insert_to_user($user_id , $name, $related_number, 
                $amount, $note);
        }
        catch (Exception $e){
            show_error($e->getMessage());
        }
        $output = array(
            "success_text" => "ok"
        );
        $this->response($output);
    }
}
