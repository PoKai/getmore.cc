<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Return_request extends REST_Controller
{
      
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('ion_auth');
        if ( !$this->ion_auth->is_admin()){
            exit('forbidden');
        }        
        
        $this->load->model('Return_request_model', 'rr');
    }

    public function getAll_get(){
        $this->db->order_by('created_at', 'desc');
        $rows = $this->rr->as_array()->get_all();
        $output = array(
            "success_text" => "ok",
            "rows" => $rows
        );
        $this->response($output);
    }
        
}
