<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Sellcart extends REST_Controller
{
    public $time_diff = 0;
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('lib_sellcart', 'ion_auth'));
        $this->table_fields = array("id", "user_id", "carts", "add_time", "edit_time");
        $this->time_diff = 5;
    }

    /**
     * Update Sellcarts API
     *
     * @return json
     * @author popchieh
     **/
    public function getCarts_post($method = 'post')
    {
        $output = array();
        $this->data = $this->get_input_array($this->table_fields, array("carts"), $method);
        $time = time();
        $force = $this->input->get_post('force') ? true : false;

        $this->data['carts'] = json_decode($this->data['carts']);
        // check user is login
        if (!$this->ion_auth->logged_in()) {
            $return = false;
            // if cart is empty, return error message
            if ((!is_array($this->data['carts']) or empty($this->data['carts'])) and !$this->ion_auth->logged_in()) {
                $this->session->unset_userdata("carts");
                $this->session->unset_userdata("carts_time");
                $this->response(array("error_text" => "Carts is empty"));
            }
            if ($force) {
                $this->session->set_userdata("carts", $this->data['carts']);
                $this->session->set_userdata("carts_time", $time);
            }
        } else {
            $this->data['user_id'] = $this->session->userdata('user_id');
            $row = $this->lib_sellcart->where('user_id', $this->data['user_id'])->carts()->row_array();
            // user cart is empty

            if (empty($row)) {
                if (!is_array($this->data['carts']) or empty($this->data['carts'])) {
                    $this->session->unset_userdata("carts");
                    $this->session->unset_userdata("carts_time");
                    $this->response(array("error_text" => "Carts is empty"));
                }
                $this->data['carts'] = json_encode($this->data['carts']);
                $this->lib_sellcart->add_cart($this->data);
                $this->session->set_userdata("carts", $this->data['carts']);
                $this->session->set_userdata("carts_time", $time);
            } else {
                // merge carts
                $old_data = json_decode($row['carts']);
                $new_data = $this->data['carts'];
                $update = false;

                // merge local storage and database row
                if (!$force) {
                    $session_time = $this->session->userdata("carts_time");
                    if ($session_time === FALSE) {
                        $session_time = 0;
                    }

                    $item_time = $row['edit_time'];
                    $old_count = count($old_data);
                    $new_count = count($new_data);

                    // user session time - database time > time_diff
                    if (($session_time - $item_time > $this->time_diff) or ($item_time - $session_time > $this->time_diff)) {
                        $update = true;
                        foreach ($new_data as $new) {
                            if (isset($new->type)) {
                                $insert = true;
                                if (!empty($old_data)) {
                                    $i = 0;
                                    foreach ($old_data as $old) {
                                        if ($old->type == $new->type and $old->id == $new->id) {
                                            $old_data[$i] = $new;
                                            $insert = false;
                                            break 1; // end foreach
                                        }
                                        $i++;
                                    }
                                }
                            }
                            if($insert) $old_data[] = $new;
                        }
                    }
                } else {
                    $update = true;
                    $old_data = $new_data;
                }
                if ($update) {
                    // update carts table
                    $this->session->set_userdata("carts_time", $time);
                    $this->lib_sellcart->update_cart($row['id'], array("carts" => json_encode($old_data)));
                    $this->data['carts'] = $old_data;
                }
            }
            // update carts session
            $this->session->set_userdata("carts", $this->data['carts']);
            $return = true;
        }

        $output = ($return) ? array("carts" => $this->data['carts'], "success_text" => "ok") : array("error_text" => "You are not login");
        $this->response($output);
    }
    
    public function createEmptyCart_post($method = 'post')
    {
        $data = array();
        $data['user_id'] = $this->session->userdata('user_id');
        $this->lib_sellcart->add_cart($data);
    }        
    
}
