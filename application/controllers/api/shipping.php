<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Shipping extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('lib_sellorder','excel', 'session'));
	$this->load->config('clothing_class');
        $this->table_fields = array("id", "user_id", "user_name", "user_sex", "delivery_telephone", "delivery_mobile_phone", "delivery_address", "order_number", "total_price", "transportation_charge", "date_purchased", "payment_method");
        $this->load->spark('curl');
        $this->load->library('Getmore/Services/MailSender');

    }

    /**
     * Process Search List
     *
     * @return null
     * @author danny
     **/
    private function _process_list($method = 'get')
    {
        $data = array();
        $tab_field_array = array("type","sort_by");
        $data = $this->get_input_array($tab_field_array, null, $method);
        if($data['type'] == 'disabled')
           $v = '-1';
        else if($data['type'] == 'notpay') 
	    $v='1';
        else if($data['type'] == 'atmnotpay') 
	    $v='1.5';
        else if($data['type'] == 'nottake') 
	    $v='2';
        else if($data['type'] == 'taking') 
	    $v='3';
        else if($data['type'] == 'checking') 
	    $v='4';
        else if($data['type'] == 'washed') 
	    $v='5';
        else if($data['type'] == 'done') 
	    $v='6';
        else if(is_numeric($data['type']))
	    $v=$data['type'];
        else
	    $v='1';
        $this->rows = $this->lib_sellorder->where('status_id', $v);
    }

    /**
     * get Shipping API
     *
     * @return json
     * @author danny
     */
    public function getShipping_get($method = 'get')
    {
        $this->_process_list();
        $data = $this->get_input_array(array('sort_by'), null, $method);
        $sort_by=empty($data["sort_by"])?'id':$data["sort_by"];
        $row = $this->lib_sellorder->order_by($sort_by, 'desc')->sellorders()->result_array();

        $result=array();
        foreach($row as $line){
            $items=$this->lib_sellorder->select("id")->where("order_id", $line["id"])->where("remove","false")->sellordersItem()->result_array();
            $line["count"]=count($items);
            array_push($result, $line);
        }
        $output = array(
            "success_text" => "ok",
            "items" => $result
        );
        $this->response($output);

    }

    public function getShopping_get($method = 'get')
    {
        $row = array();
        
        $washed_row = $this->lib_sellorder->where("status_id", 5)->where("remove", 0)->where("shelf", 0)->order_by('date_finished', 'asc')->shoppingItem()->result_array();
        $done_row = $this->lib_sellorder->where("status_id", 6)->where("remove", 0)->where("shelf", 0)->order_by('date_finished', 'asc')->shoppingItem()->result_array();
        
        $row = array_merge($washed_row, $done_row);
        
        $result=array();
        $output = array(
            "success_text" => "ok",
            "items" => $row
        );
        $this->response($output);

    }

    /**
     * get Shipping API
     *
     * @return json
     * @author danny
     */
   public function getShippingItems_get($method = 'get')
    {
        $data = $this->get_input_array(array("order_id","wash_id"), null, $method);
        //$this->_process_list();
	if(!empty($data["order_id"]))
            $row = $this->lib_sellorder->where("order_id",$data["order_id"])->sellordersItem()->result_array();
	else if(!empty($data["wash_id"]))
            $row = $this->lib_sellorder->where("shipping_wash_id",$data["wash_id"])->sellordersItem()->result_array();

        $output = array(
            "success_text" => "ok",
            "items" => $row
        );
        $this->response($output);
    }

    /**
     * Update User info API
     *
     * @return json
     * @author danny
     */
    public function userinfoEdit_post($method = 'post'){
        $this->data = $this->get_input_array(array("id","value"),null, $method);
        $id = $this->data['id'];
        $value = $this->data['value'];
        list($field, $id) = explode('!', $id);
        $thisdata=array($field=>$value);
        $return = $this->lib_sellorder->update_userinfo($id, $thisdata);

        echo $value;
    }

    /**
     * Update Item info API
     *
     * @return json
     * @author danny
     */
    public function iteminfoEdit_post($method = 'post'){
        $this->data = $this->get_input_array(array("id","value"),null, $method);
        $id = $this->data['id'];
        $value = $this->data['value'];
        list($field, $id) = explode('!', $id);
        $thisdata=array($field=>$value);
        $return = $this->lib_sellorder->update_iteminfo($id, $thisdata);

        echo $value;
    }

    /**
     * Update Item info API
     *
     * @return json
     * @author danny
     */
    public function setShippingStatus_post($method = 'post')
    {
        if(($user_id = $this->session->userdata('user_id')) == 0){
          return $this->response('/auth/login', 301);
        }

        $this->data = $this->get_input_array(array("chkitems","type"),null, $method);
        $id = $this->data['chkitems'];
        
        /*
         * plus one directly? genius.
         * 
         * @author howtomakeaturn
         */
        $value = array("status_id"=>$this->data['type'] + 1);
        
        // quick and dirty fix
        if ($this->data['type']=='1.5'){
            $value['status_id'] = 2;
            $this->load->model('Sellorder_model');
            
            if(is_array($id)){
                foreach($id as $i){
                    $order = $this->Sellorder_model->get_by_id($i);
                    $this->Sellorder_model->notify_the_sellorder_is_paid($order->order_number);                  
                }
            }
            else{
                $order = $this->Sellorder_model->get_by_id($id);
                $this->Sellorder_model->notify_the_sellorder_is_paid($order->order_number);
            }
        }

        $value["date_finished"] = date("Y-m-d H:i:s");
        
        if($value['status_id'] === 5){
          $value["washer"] = $user_id;
        }

        $return = $this->lib_sellorder->update_shippingstatus($id, $value);
        
        // Send reminder to tell users we are on the way picking their order.
        if ($this->data['type']=='2'){
            if(is_array($id)){
                foreach($id as $i){
                    @$this->mailsender->send_is_picking_order_reminder_mail($i);
                }
            }
            else{
                @$this->mailsender->send_is_picking_order_reminder_mail($id);
            }            
        }

        // Send reminder to tell users we are checking their order.        
        if ($this->data['type']=='3'){
            if(is_array($id)){
                foreach($id as $i){
                    @$this->mailsender->send_is_checking_order_reminder_mail($i);
                }
            }
            else{
                @$this->mailsender->send_is_checking_order_reminder_mail($id);
            }            
        }

        $output = array(
            "success_text" => $return 
        );
        $this->response($output);
    }

    /**
     * @return json
     * @author danny
     */
    public function generateXML_get($method = 'get')
    {
    // add excel header
	$this->value = $this->get_input_array(array("items"),null, $method);
        $id = explode(",", $this->value['items']);
        $this->data[] = array('編號', '姓名', '地址', '手機', '電話');

        $res = $this->lib_sellorder->select(array("order_number","user_name","user_address","user_mobile_phone","delivery_telephone"));
        $res->where('id', $id);
        $rows = $res->sellorders()->result_array();
	foreach($rows as $row){
            // add excel data
            $this->data[] = array($row['order_number'], $row['user_name'], $row['user_address'], $row['user_mobile_phone'], $row['delivery_telephone']);
        }
            $this->excel->addArray($this->data);
            $file_name = 'shipping_list_' . date("Y-m-d-His") . '.xls';

        $this->excel->generateXML($file_name);
    }

    /**
     * @return json
     * @author danny
     */
    public function delShippingItems_post($method = 'post')
    {
        $this->data = $this->get_input_array(array("order_id"),null, $method);
	$id = $this->data['order_id'];

        $return = $this->lib_sellorder->delete_order($id,'order_product');

        $output = array(
            "success_text" => $return 
        );
        $this->response($output);
	
    }
    public function getClothingSetting_get($method = 'get')
    {
        $output["name"]=$this->config->item('class_name');	
        $output["price"]=$this->config->item('class_price');	
        $output["transport"]=$this->config->item('class_transport');	
        $this->response($output);	
    }
    
    function send_email_if_needed($product_id){
        $row = $this->db->from('sellorders_products')->where('id', $product_id)
                                  ->get()->row_array();
                                  
        $query = $this->db->from('sellorders_products')->where('order_id', $row['order_id'])
                                  ->where('shelf', '1')->get();
                                  
        if (!$query->num_rows()){
            @$this->mailsender->send_is_in_shelf_reminder_mail($row['order_id']);       
        }
    }

    /**
     * Add Clothing API
     *
     * @return json
     * @author danny
     */
    public function addClothingItem_post($method = 'post')
    {
      
      $table_field=array("order_id","product_id", "product_item_number", "product_original_price",
        "product_sell_price", "product_title", "product_brand", "product_type",
        "product_comments", "color", "season", "style", "pattern", "item_long",
        "files", "size", "size_comment", "item_status", "material", "location", "measure","descript", "publication_time",
        "size_type_major", "size_type_minor", "size", "size_number_minor", 
        "size_standard", 'admin_comment', 'admin_remark', 'draft'
      );
      $this->data = $this->get_input_array($table_field, null, $method);
      
      if(($user_id = $this->session->userdata('user_id')) == 0){
        return $this->response('/auth/login', 301);
      }

      $this->data['added_user_id'] = $user_id;
      @$this->send_email_if_needed($this->data['product_id']);
      
      if ( empty($this->data['publication_time']) ){
          $this->data['publication_time'] = date('c');
      }

      if($this->data['draft'] === "true"){
        $this->data['lock_status'] = 4;
        $this->data['on_shelf'] = 0;
      }
      unset($this->data['draft']);
      
      $this->curl->simple_post('/restful/product_clothing', $this->data, array(
        CURLOPT_COOKIE => http_build_query($_COOKIE,'',';')
      ));
      /*
      $id = $this->lib_sellorder->add_order($this->data,'clothing');
      
      $this->db->where('id', $this->data['product_id'])
                      ->update( 'sellorders_products', array( 'shelf'=>1 ) );
      
      $this->load->model('Brand_model');
      $this->Brand_model->insertIfNotExist(['name'=>$this->data['product_brand']]);
      */
      $this->data['success_text'] = 'ok';
      $this->response($this->data);
    }
    
    /*
     *  Recalculate the order. 
     *  @author howtomakeaturn
     */
    public function recalculate_post(){
        $id = $this->input->post('order_id');
        $this->load->model('Order_model');
        try{
            $this->Order_model->recalculate($id);
            $output = array(
                "success_text" => 'Recalculated.',
                "id" => $id
            );
        }
        catch(Exception $e){
            $output = array(
                "error_text" => $e->getMessage(),
                "id" => $id
            );          
        }
        $this->response($output);	
    }

}
