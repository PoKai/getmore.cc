<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Sys_config extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        if ( !$this->ion_auth->is_admin() && !$this->ion_auth->is_designer()){
            exit('forbidden');
        }
        
        $this->load->database();
        $this->load->model('System_configuration_model', 'sys_config');
    }

    public function getFreeShipping_get()
    {
        $free_shipping = $this->sys_config->get_free_shipping();
        $banners = $this->sys_config->get_banners_with_image_url_and_href();
        $output = array(
            'free_shipping' => $free_shipping,
            'banners'          => $banners
        );
        $this->response($output);
    }


    public function setFreeShipping_post()
    {
        $value = $this->input->post('free_shipping');
        $free_shipping = $this->sys_config->set_free_shipping($value);
        $output = array(
            'free_shipping' => $free_shipping
        );
        $this->response($output);
    }

    public function addBanner_post()
    {
        $file_id = $this->input->post('file_id');
        $href = $this->input->post('href');
        $open_in_new_tab = $this->input->post('open_in_new_tab');
        $free_shipping = $this->sys_config->add_banner($file_id, $href, $open_in_new_tab);
        $output = array(
            'success_message' => 'success'
        );
        $this->response($output);
    }

    public function deleteBanner_post()
    {
        $banner_id = $this->input->post('banner_id');
        $banner_image_id = $this->input->post('banner_image_id');

        $this->curl->simple_delete(base_url("/restful/banner/".$banner_id), array(), array(
            CURLOPT_COOKIE => http_build_query($_COOKIE,'',';')
        ));

        $output = array(
            'success_message' => 'success'
        );
        $this->response($output);
    }

    public function set_banner_order_post()
    {
        $sequences = $this->input->post('sequences');
        $ordered_banner_ids = json_decode($sequences);
        $this->sys_config->set_banner_order($ordered_banner_ids);
        $output = array(
            'success_message' => 'success'
        );
        $this->response($output);
    }
    
}
