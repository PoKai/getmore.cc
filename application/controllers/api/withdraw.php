<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Withdraw extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('ion_auth');
        if ( !$this->ion_auth->is_admin()){
            exit('forbidden');
        }
        
        $this->load->database();
        $this->load->model('Withdraw_model', 'withdraw');
    }

    public function getWithdraws_get()
    {
        $withdraws = $this->withdraw->latest_withdraws()->join_users()->get_all();
        $output = array(
            'withdraws' => $withdraws
        );
        $this->response($output);
    }
}
