<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Remark extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('lib_remark');
        $this->load->library('session');
    }

    /**
     * get Remarks API
     *
     * @return json
     * @author appleboy
     **/
    public function getRemarks_get($method = 'get')
    {
        $output = array();
        $input_array = array("id", "r_id", "type");
        $data = $this->get_input_array($input_array, NULL, $method);

        isset($data['r_id']) and $this->lib_remark->where('r_id', $data['r_id']);
        isset($data['type']) and $this->lib_remark->where('type', $data['type']);
        $rows = $this->lib_remark->select('id, type, r_id, remarks, FROM_UNIXTIME(add_time) as add_time')->remarks()->result_array();

        $output = array(
            "success_text" => "ok",
            "item" => $rows
        );
        $this->response($output);
    }

    /**
     * get Remark API
     *
     * @return json
     * @author appleboy
     **/
    public function getRemark_get($method = 'get')
    {
        $output = array();
        $input_array = array("id");
        $data = $this->get_input_array($input_array, $input_array, $method);

        $row = $this->lib_remark->where('id', $data['id'])->remarks()->row_array();
        $output = array(
            "success_text" => "ok",
            "item" => $row
        );
        $this->response($output);
    }

    /**
     * Add Remark API
     *
     * @return json
     * @author appleboy
     **/
    public function remarkAdd_post($method = 'post')
    {
        $output = array();
        $input_array = array("type", "r_id", "remarks");
        $data = $this->get_input_array($input_array, $input_array, $method);

        $id = $this->lib_remark->add_remark($data);
        $row = array(
            "id"        => $id,
            "add_time"  => date("Y-m-d H:i:s")
        );
        $this->response(array("success_text" => "ok", "item" => $row));
    }

    /**
     * Update Remark API
     *
     * @return json
     * @author appleboy
     **/
    public function remarkEdit_post($method = 'post')
    {
        $output = array();
        $input_array = array("id", "type", "r_id", "remarks");
        $data = $this->get_input_array($input_array, $input_array, $method);

        $return = $this->lib_remark->update_remark($data['id'], $data);

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Invalid ID");
        $this->response($output);
    }

    /**
     * Delete Remark API
     *
     * @return json
     * @author appleboy
     **/
    public function remarkDelete_post($method = 'post')
    {
        $output = array();
        $input_array = array("id");
        $data = $this->get_input_array($input_array, $input_array, $method);

        $return = $this->lib_remark->delete_remark($data['id']);

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Invalid acl id");
        $this->response($output);
    }
}
