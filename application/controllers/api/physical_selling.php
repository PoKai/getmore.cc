<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Physical_selling extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('ion_auth');
        $this->load->model('Buy_order_model', 'buyorder');
        $this->load->model('Sub_order_model', 'suborder');

        $this->load->library('ion_auth');
        if ( !$this->ion_auth->is_admin()){
            exit('forbidden');
        }

    }


    public function putOneIntoPhysicalBuyorder_post()
    {
        $product_item_number = $this->input->post('product_item_number');
        $order_number = $this->input->post('order_number');
        $this->load->model('Product_clothing_model', 'clothing');

        try{
            $this->clothing->put_one_into_physical_buyorder($product_item_number, $order_number);
        }
        catch (Exception $e){
            show_error($e->getMessage());
        }

        $clothings = $this->buyorder->get_complete_order_by_order_number($order_number)['clothing_array'];

        $output = array(
            'message' => 'success',
            'clothings' => $clothings
        );
        $this->response($output);
    }


    public function getAdminList_get()
    {
        $users = $this->ion_auth->users2(1)->result();
        $output = array(
            'users' => $users
        );
        $this->response($output);
    }

    public function viewOrder_post()
    {
        $order_number = $this->input->post('order_number');
        $complete_order = $this->buyorder->get_complete_order_by_order_number($order_number);
        $output = array(
            'complete_order' => $complete_order
        );
        $this->response($output);
    }

    public function getPhysicalOrders_get()
    {
        $orders = $this->buyorder->get_physical_orders();
        $output = array(
            'orders' => $orders
        );
        $this->response($output);
    }

    public function createOrder_post()
    {
        $user_id = $this->input->post('user_id');
        $buyer_name = $this->input->post('buyer_name');

        $order_number = $this->buyorder->generate_order_number();

        $this->buyorder->insert(array(
            'user_id' => $user_id,
            'buyer_name' =>$buyer_name,
            'status' => '4',
            'order_number' => $order_number,
            'created_at' => date("Y-m-d H:i:s"),
            'paid_at' => date("Y-m-d H:i:s"),
            'shipped_at' => date("Y-m-d H:i:s")
        ));

        $this->suborder->insert([
          'buy_order_id' => $this->buyorder->db->insert_id(),
          'sub_order_number' => $this->suborder->generate_sub_order_number($order_number),
          'sellorder_id' => 0,
          'status' => '4',
          'shipping_price' => 0,
          'products' => json_encode([]),
          'shipping_number' => '',
          'done_at' => '0000-00-00 00:00:00'
        ]);

        $output = array(
            'message' => 'success'
        );
        $this->response($output);
    }

}
