<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Sellcoupon extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        // this can be further improved by validating administrative authority
        if (!$this->ion_auth->logged_in()) {
            show_error('unauthorized ajax request');
        }
        
        $this->load->database();
        $this->load->model('Sellcoupon_model', 'coupon');
    }

    public function getcoupons_get()
    {
        $coupons = $this->coupon->get_all();
        $output = array(
            'coupons' => $coupons
        );
        $this->response($output);
    }
    
    public function getcoupon_get()
    {
        $id = $this->input->get('id');
        $coupon = $this->coupon->get($id);
        $output = array(
            'coupon' => $coupon
        );
        $this->response($output);
    }    
    
    public function updateCoupon_post()
    {
        $id = $this->input->post('id');        
        $name = $this->input->post('name');
        $code = $this->input->post('code');
        $amount = $this->input->post('amount');
        $times_available = $this->input->post('times_available');
        $minimum_charge = $this->input->post('minimum_charge');
        $expired_at = $this->input->post('expired_at');
        $creator_id = $this->session->userdata('user_id');
        
        $data = array(
            'name' => $name,
            'code' => $code,
            'amount' => $amount,
            'times_available' => $times_available,
            'minimum_charge' => $minimum_charge,
            'expired_at' => $expired_at,
            'creator_id' => $creator_id      
        );
        
        try{
            $this->coupon->update($id, $data);
        }
        catch (Exception $e){
            show_error($e->getMessage());
        }
        $output = array(
            "success_text" => "ok"
        );
        $this->response($output);
    }    
    
    public function deleteCoupon_post()
    {
        $id = $this->input->post('id');        
        
        try{
            $this->coupon->delete($id);
        }
        catch (Exception $e){
            show_error($e->getMessage());
        }
        $output = array(
            "success_text" => "ok"
        );
        $this->response($output);
    }    
    

    
    public function addCoupon_post()
    {
        $name = $this->input->post('name');
        $code = $this->input->post('code');
        $amount = $this->input->post('amount');
        $times_available = $this->input->post('times_available');
        $minimum_charge = $this->input->post('minimum_charge');
        $expired_at = $this->input->post('expired_at');
        $creator_id = $this->session->userdata('user_id');
        $dedicated_user_id = $this->input->post('dedicated_user_id');
        
        $data = array(
            'name' => $name,
            'code' => $code,
            'amount' => $amount,
            'times_available' => $times_available,
            'minimum_charge' => $minimum_charge,
            'expired_at' => $expired_at,
            'creator_id' => $creator_id,
            'dedicated_user_id'  => $dedicated_user_id   
        );
        
        try{
            $this->coupon->create($data);
        }
        catch (Exception $e){
            show_error($e->getMessage());
        }
        $output = array(
            "success_text" => "ok"
        );
        $this->response($output);
    }
}
