<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Product extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('lib_product');
        $this->load->library('files_lib');
        $this->table_fields = array("id", "subject_id", "title", "description", "item_number", "manufacturer", "is_enabled", "stock_price", "sell_price", "original_price", "stock_amount", "inventory_amount", "reject_amount", "file_list", "detail_name_list", "detail_value_list", "delivery_name_list", "delivery_value_list", "introduction", "information", "edit_time", "add_time");
        $this->list_array = array('detail', 'delivery');
        $this->data = array();
    }

    /**
     * Process Search List
     *
     * @return null
     * @author appleboy
     **/
    private function _process_list($method = 'get')
    {
        $data = array();
        $search_array = array("s_subject_name", "s_title", "s_manufacturer", "s_is_enabled", "s_stock_price", "s_inventory_amount");
        $data = $this->get_input_array($search_array, null, $method);
        foreach ($data as $k => $v) {
            $k = preg_replace('/^s_/', '', $k);
            if (!empty($v) or mb_strlen($v) > 0) {
                if ($k == "subject_name") {
                    $this->lib_product->like('`product_subjects`.title', $v);
                } elseif ($k == "is_enabled") {
                        if ($v == "1")
                            $this->lib_product->where('`products`.options&' . (_IS_ENABLED) . ' >= 1');
                        else
                            $this->lib_product->where('`products`.options&' . (_IS_ENABLED) . ' = 0');
                } elseif ($k == 'title') {
                    $this->lib_product->like('`products`.title', $v);
                } else {
                    $this->lib_product->like($k, $v);
                }
            }
        }
    }

    /**
     * get Products API
     *
     * @return json
     * @author appleboy
     **/
    public function getProducts_get($method = 'get')
    {
        $this->_process_list();
        $row = $this->lib_product->select('product_subjects.title as subject_name, products.*, IF(products.options & ' . _IS_ENABLED . ', true, false) as is_enabled')->products()->result_array();
        $output = array(
            "success_text" => "ok",
            "items" => $row
        );
        $this->response($output);
    }

    /**
     * get Product API
     *
     * @return json
     * @author appleboy
     **/
    public function getProduct_get($method = 'get')
    {
        $output = array();
        $input_array = array("id");
        $this->data = $this->get_input_array($input_array, $input_array, $method);

        $row = $this->lib_product->select('products.*, IF(products.options & ' . _IS_ENABLED . ', true, false) as is_enabled')->where('products.id', $this->data['id'])->products()->row_array();
        if (isset($row['file_list']) and $row['file_list'] != '') {
            $files = $this->files_lib->where('file_id', explode(',', $row['file_list']))->order_by_field('file_id', $row['file_list'])->files()->result_array();
            $row['image_list'] = $files;
        }

        foreach ($this->list_array as $item) {
            (!empty($row[$item])) and $row[$item] = json_decode($row[$item]);
        }

        $output = array(
            "success_text" => "ok",
            "item" => $row
        );
        $this->response($output);
    }

    /**
     * Process detail API
     *
     * @return this
     * @author appleboy
     **/
    private function _process_detail()
    {
        foreach ($this->list_array as $item) {
            $list_name = $item . '_name_list';
            $list_value = $item . '_value_list';
            if (isset($this->data[$list_name])) {
                $list = array();
                $i = 0;
                foreach ($this->data[$list_name] as $row) {
                    $list[] = array('model' => $item, 'name' => $this->data[$list_name][$i], 'value' => $this->data[$list_value][$i]);
                    $i++;
                }
                unset($this->data[$list_name]);
                unset($this->data[$list_value]);
                $this->data[$item] = json_encode($list);
            }
        }

        return $this;
    }

    /**
     * Add Product API
     *
     * @return json
     * @author appleboy
     **/
    public function productAdd_post($method = 'post')
    {
        $output = array();
        $this->data = $this->get_input_array($this->table_fields, null, $method);

        $this->data['file_list'] = (isset($this->data['file_list']) and is_array($this->data['file_list'])) ? implode(',', $this->data['file_list']) : '';
        $this->_process_detail();

        $resource_id = $this->lib_product->add_product($this->data);
        $this->response(array("success_text" => "ok"));
    }

    /**
     * Update Product API
     *
     * @return json
     * @author appleboy
     **/
    public function productEdit_post($method = 'post')
    {
        $output = array();
        $this->data = $this->get_input_array($this->table_fields, array("id"), $method);

        if (isset($this->data['file_list']) and is_array($this->data['file_list'])) {
            $this->data['file_list'] = implode(',', $this->data['file_list']);
        }

        $this->_process_detail();

        $return = $this->lib_product->update_product($this->data['id'], $this->data);

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Invalid ID");
        $this->response($output);
    }

    /**
     * Delete Product API
     *
     * @return json
     * @author appleboy
     **/
    public function productDelete_post($method = 'post')
    {
        $output = array();
        $this->data = $this->get_input_array($this->table_fields, array('id'), $method);

        $return = $this->lib_product->delete_product($this->data['id']);

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Invalid acl id");
        $this->response($output);
    }
}
