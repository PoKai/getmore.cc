<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Subject extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('lib_subject');
        $this->load->library('files_lib');
        $this->table_fields = array("id", "category_id", "title", "description", "username", "options", "up_time", "down_time", "sell_price", "fix_price", "content", "file_list", "is_index", "is_enabled", "add_time", "edit_time", 'limit', 'offset');
    }

    /**
     * Process Search List
     *
     * @return null
     * @author appleboy
     **/
    private function _process_list($method = 'get')
    {
        $data = array();
        $search_array = array("s_category_name", "s_title", "s_username", "s_is_index", "s_is_enabled");
        $data = $this->get_input_array($search_array, null, $method);
        foreach ($data as $k => $v) {
            $k = preg_replace('/^s_/', '', $k);
            if (!empty($v) or mb_strlen($v) > 0) {
                if ($k == "category_name") {
                    $this->lib_subject->like('category.tw_name', $v);
                } elseif ($k == "is_index" or $k == "is_enabled") {
                    if ($k == "is_index") {
                        if ($v == "1")
                            $this->lib_subject->where('`options`&' . (_IS_INDEX) . ' >= 1');
                        else
                            $this->lib_subject->where('`options`&' . (_IS_INDEX) . ' = 0');
                    } elseif ($k == "is_enabled") {
                        if ($v == "1")
                            $this->lib_subject->where('`options`&' . (_IS_ENABLED) . ' >= 1');
                        else
                            $this->lib_subject->where('`options`&' . (_IS_ENABLED) . ' = 0');
                    }
                } else {
                    $this->lib_subject->like($k, $v);
                }
            }
        }
    }

    /**
     * get Subjects API
     *
     * @return json
     * @author appleboy
     **/
    public function getSubjects_get($method = 'get')
    {
        $this->_process_list();
        $row = $this->lib_subject->select("category.tw_name as category_name, product_subjects.*, IF(product_subjects.options & " . _IS_INDEX . ", true, false) as is_index, IF(product_subjects.options & " . _IS_ENABLED . ", true, false) as is_enabled, DATE_FORMAT(`up_time`, '%Y-%m-%d %h:%i') as up_time, DATE_FORMAT(`down_time`, '%Y-%m-%d %h:%i') as down_time")->subjects()->result_array();
        $output = array(
            "success_text" => "ok",
            "items" => $row
        );
        $this->response($output);
    }

    /**
     * Subject list API (front end)
     *
     * @return json
     * @author appleboy
     **/
    public function getSubjectList_get($method = 'get')
    {
        $output = array();
        $input_array = array('limit', 'offset', 'id');
        $this->_data = $this->get_input_array($input_array, array('limit'), $method);

        $return = $this->lib_subject->get_list($this->_data);

        $output = array(
            "success_text" => "ok",
            "items" => $return['output']
        );

        $this->response($output);
    }

    /**
     * get Subject Menus API
     *
     * @return json
     * @author appleboy
     **/
    public function getSubjectMenus_get($method = 'get')
    {
        $this->_process_list();
        $row = $this->lib_subject->select('product_subjects.id, product_subjects.title')->subjects()->result_array();
        $output = array(
            "success_text" => "ok",
            "items" => $row
        );
        $this->response($output);
    }

    /**
     * get Subject API
     *
     * @return json
     * @author appleboy
     **/
    public function getSubject_get($method = 'get')
    {
        $output = array();
        $input_array = array("id");
        $data = $this->get_input_array($input_array, $input_array, $method);

        $row = $this->lib_subject->select('product_subjects.*, IF(product_subjects.options & ' . _IS_INDEX . ', true, false) as is_index, IF(product_subjects.options & ' . _IS_ENABLED . ', true, false) as is_enabled')->where('product_subjects.id', $data['id'])->subjects()->row_array();
        if ($row['file_list'] != '') {
            $files = $this->files_lib->where('file_id', explode(',', $row['file_list']))->order_by_field('file_id', $row['file_list'])->files()->result_array();
            $row['image_list'] = $files;
        }
        $output = array(
            "success_text" => "ok",
            "item" => $row
        );
        $this->response($output);
    }

    /**
     * Add Subject API
     *
     * @return json
     * @author appleboy
     **/
    public function subjectAdd_post($method = 'post')
    {
        $output = array();
        $data = $this->get_input_array($this->table_fields, null, $method);

        $data['file_list'] = (isset($data['file_list']) and is_array($data['file_list'])) ? implode(',', $data['file_list']) : '';
        $resource_id = $this->lib_subject->add_subject($data);
        $this->response(array("success_text" => "ok"));
    }

    /**
     * Update Subject API
     *
     * @return json
     * @author appleboy
     **/
    public function subjectEdit_post($method = 'post')
    {
        $output = array();
        $data = $this->get_input_array($this->table_fields, array("id"), $method);

        if (isset($data['file_list']) and is_array($data['file_list'])) {
            $data['file_list'] = implode(',', $data['file_list']);
        }

        $return = $this->lib_subject->update_subject($data['id'], $data);

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Invalid ID");
        $this->response($output);
    }

    /**
     * Delete Subject API
     *
     * @return json
     * @author appleboy
     **/
    public function subjectDelete_post($method = 'post')
    {
        $output = array();
        $data = $this->get_input_array($this->table_fields, array("id"), $method);

        $return = $this->lib_subject->delete_subject($data['id']);

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Invalid acl id");
        $this->response($output);
    }
}
