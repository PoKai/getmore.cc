<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Users extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session', 'ion_auth'));
        $this->table_fields = array("id", "user_groups", "ip_address", "username", "password", "confirm_password", "salt", "email", "activation_code", "forgotten_password_code", "remember_code", "created_on", "last_login", "active", "first_name", "last_name", "tags", "mobile_phone", "telephone", "sex", "user_name", "birthday", "p_address", "s_address", "edit_time");
    }

    public function me_get()
    {
        $data = array(
            "success_text" => "ok",
            "item" => $this->session->all_userdata()
        );
        $this->response($data);
    }

    /**
     * Process Search List
     *
     * @return null
     * @author appleboy
     **/
    private function _process_list($method = 'get')
    {
        $data = array();
        $data = $this->get_input_array($this->table_fields, null, $method);
        foreach ($data as $k => $v) {
            if (!empty($v) or mb_strlen($v) > 0) {
                $this->ion_auth->like($k, $v);
            }
        }
    }

    /**
     * User List API
     *
     * @return json
     * @author appleboy
     **/
    public function getUsers_get($method = 'get')
    {
        $this->_process_list();
        $row = $this->ion_auth->select("id, user_name, sex, email, mobile_phone, telephone, tags, created_on")->users()->result_array();
        $output = array(
            "success_text" => "ok",
            "items" => $row
        );
        $this->response($output);
    }

    /**
     * User Data API
     *
     * @return json
     * @author appleboy
     **/
    public function getUser_get($method = 'get')
    {
        $input_array = array("id");
        $data = $this->get_input_array($input_array, $input_array, $method);

        $row = $this->ion_auth->user($data['id'])->row_array();

        $row['sex'] = ($row['sex'] == '1') ? true : false;
        $row['active'] = ($row['active'] == '1') ? true : false;
        $row['all_groups'] = $this->ion_auth->get_all_groups_by_user($data['id']);

        $output = array(
            "success_text" => "ok",
            "item" => $row
        );

        $this->response($output);
    }

    /**
     * Add User API
     *
     * @return json
     * @author appleboy
     **/
    public function userAdd_post($method = 'post')
    {
        $output = array();
        $data = $this->get_input_array($this->table_fields, array("email", "password"), $method);

        if (!(!empty($data['password']) and ($data['password'] == $data['confirm_password']))) {
            unset($data['password']);
            unset($data['confirm_password']);
        }

        if(isset($data['confirm_password']))
            unset($data['confirm_password']);

        $user_id = $this->ion_auth->add_user($data);
        $output = ($user_id) ? array("success_text" => "ok", "user_id" => $user_id) : array("error_text" => "Can't create account.");

        $this->response($output);
    }

    /**
     * Update User API
     *
     * @return json
     * @author appleboy
     **/
    public function userEdit_post($method = 'post')
    {
        $output = array();
        $data = $this->get_input_array($this->table_fields, array("id"), $method);

        if (!(!empty($data['password']) and ($data['password'] == $data['confirm_password']))) {
            unset($data['password']);
            unset($data['confirm_password']);
        }

        if (isset($data['confirm_password'])) {
            unset($data['confirm_password']);
        }

        // update user groups
        if (isset($data['user_groups'])) {
            // check is array for single checkbox
            if (!is_array($data['user_groups'])) {
                $data['user_groups'] = array($data['user_groups']);
            }
            $groups = $this->ion_auth->get_users_groups($data['id'])->result_array();
            if (!empty($groups)) {
                $user_groups = array();

                // delete old group if group id is not in checkbox data
                foreach ($groups as $row) {
                    if (!in_array($row['id'], $data['user_groups'])) {
                        // delete user group
                        $this->db
                            ->where('user_id', $data['id'])
                            ->where('group_id', $row['id'])
                            ->delete(USER_GROUP_TABLE);
                    } else {
                        $user_groups[] = $row['id'];
                    }
                }

                // add new group
                foreach ($data['user_groups'] as $row) {
                    if (!in_array($row, $user_groups)) {
                        $this->db->insert(USER_GROUP_TABLE, array(
                            'user_id' => $data['id'],
                            'group_id' => $row
                        ));
                    }
                }
            } else {
                // add all groups
                foreach ($data['user_groups'] as $row) {
                    $this->db->insert(USER_GROUP_TABLE, array(
                        'user_id' => $data['id'],
                        'group_id' => $row
                    ));
                }
            }

            unset($data['user_groups']);
        }

        $return = $this->ion_auth->update_user($data['id'], $data);

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Invalid ID");
        $this->response($output);
    }

    /**
     * Delete User API
     *
     * @return json
     * @author appleboy
     **/
    public function userDelete_post($method = 'post')
    {
        $output = array();
        $input_array = array("id");
        $data = $this->get_input_array($input_array, $input_array, $method);

        $return = $this->ion_auth->delete_user($data['id']);

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Invalid websync id");
        $this->response($output);
    }

    public function getGroups_get()
    {
        $output = $this->ion_auth->groups()->result_array();
        $data = array(
            "success_text" => "ok",
            "item" => $output
        );
        $this->response($data);
    }

    public function getGroup_get($method = 'get')
    {
        $data = $this->get_input_array(array("id"), array("id"), $method);

        $output = $this->ion_auth->where('id', $data['id'])->groups()->row_array();
        $data = array(
            "success_text" => "ok",
            "item" => $output
        );
        $this->response($data);
    }

    /**
     * Add Group API
     *
     * @return json
     * @author appleboy
     **/
    public function groupAdd_post($method = 'post')
    {
        $output = array();
        $input_array = array("name", "description", "group_order");
        $data = $this->get_input_array($input_array, $input_array, $method);

        $group_id = $this->ion_auth->add_group($data);

        // add default permission of group
        $rows = $this->db->select('resource_id')->where('parent_id !=', 0)->get(ACL_RESOURCE_TABLE)->result_array();
        foreach ($rows as $row) {
            $data = array(
                'group_id' => $group_id,
                'resource_id' => $row['resource_id'],
                'action' => 'deny'
            );

            $this->db->insert(ACL_TABLE, $data);
        }

        $output = ($group_id) ? array("success_text" => "ok", "group_id" => $group_id) : array("error_text" => "Can't create group.");

        $this->response($output);
    }

    /**
     * Update Group API
     *
     * @return json
     * @author appleboy
     **/
    public function groupEdit_post($method = 'post')
    {
        $input_array = array("id", "name", "description", "group_order");
        $input = $this->get_input_array($input_array, $input_array, $method);

        $return = $this->ion_auth->update_group($input['id'], $input);

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Can't update group.");
        $this->response($output);
    }

    /**
     * Delete Group API
     *
     * @return json
     * @author appleboy
     **/
    public function groupDelete_post($method = 'post')
    {
        $output = array();
        $input_array = array("id");
        $data = $this->get_input_array($input_array, $input_array, $method);

        $return = $this->ion_auth->delete_group($data['id']);

        // remove user groups
        $this->db->where('group_id', $data['id'])->delete(USER_GROUP_TABLE);

        // remove acl group permissions
        $this->db->where('group_id', $data['id'])->delete(ACL_TABLE);

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Invalid group id");
        $this->response($output);
    }

    public function getEmails_get()
    {
        $this->load->model('marketing_model');
        $emails = $this->marketing_model->get_all_emails();
        $addresses = array_map(create_function('$o', 'return $o->address;'), $emails);
        
        $output = array(
            "success_text" => "ok",
            "emails" => $emails
        );

        $this->response($output);
    }
    
    public function getBuyerEmails_get()
    {
        $this->load->model('marketing_model');
        $emails = $this->marketing_model->get_buyer_emails();
        $addresses = array_map(create_function('$o', 'return $o->address;'), $emails);
        
        $output = array(
            "success_text" => "ok",
            "emails" => $emails
        );

        $this->response($output);
    }
    
    public function getSellerEmails_get()
    {
        $this->load->model('marketing_model');
        $emails = $this->marketing_model->get_seller_emails();
        $addresses = array_map(create_function('$o', 'return $o->address;'), $emails);
        
        $output = array(
            "success_text" => "ok",
            "emails" => $emails
        );

        $this->response($output);
    }    
        
    
}
