<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Suggested extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('Suggested_items_model');
    }
    
    public function getAll_get()
    {
        $items =$this->Suggested_items_model->get_all();
        $output = array(
            "success_text" => "ok",
            "items" => $items
        );
        $this->response($output);
    }    
    
    public function add_post()
    {
        $product_item_number = $this->post('product_item_number');
        $this->Suggested_items_model->add($product_item_number);
        $output = array(
            "success_text" => "ok"
        );
        $this->response($output);
    }

    public function updateSequences_post()
    {
        $sequences = $this->post('sequences');
        $seq_array = json_decode($sequences);
        $this->Suggested_items_model->updateSequences($seq_array);
        $output = array(
            "success_text" => "ok"
        );
        $this->response($output);
    }



    public function delete_post()
    {
        $suggested_id = $this->post('suggested_id');
        $this->Suggested_items_model->delete($suggested_id);
        $output = array(
            "success_text" => "ok"
        );
        $this->response($output);
    }
    
}
