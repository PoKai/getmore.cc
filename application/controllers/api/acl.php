<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Acl extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('zacl');
    }

    /**
     * get ACLs API
     *
     * @return json
     * @author appleboy
     **/
    public function getAcls_get($method = 'get')
    {
        $output = array();
        $output = array(
            "resource" => $this->zacl->get_top_resource(),
            "all_group" => $this->zacl->all_group,
            "all_group_count" => count($this->zacl->all_group),
            "all_acl" => $this->zacl->get_acl()
        );

        $this->response($output);
    }

    /**
     * Add ACL API
     *
     * @return json
     * @author appleboy
     **/
    public function aclAdd_post($method = 'post')
    {
        $output = array();
        $input_array = array("parent_id", "resource_name", "resource_description", "action", "group_id");
        $data = $this->get_input_array($input_array, $input_array, $method);

        $resource_id = $this->zacl->add_resource($data);
        $this->response(array("success_text" => "ok"));
    }

    /**
     * Update ACL API
     *
     * @return json
     * @author appleboy
     **/
    public function aclEdit_post($method = 'post')
    {
        $output = array();
        $input_array = array("id", "action");
        $data = $this->get_input_array($input_array, $input_array, $method);

        $return = $this->zacl->edit_acl($data['id'], $data['action']);

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Invalid ID");
        $this->response($output);
    }

    /**
     * Delete ACL API
     *
     * @return json
     * @author appleboy
     **/
    public function aclDelete_post($method = 'post')
    {
        $output = array();
        $input_array = array("id");
        $data = $this->get_input_array($input_array, $input_array, $method);

        $return = $this->zacl->del_acl($data['id']);

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Invalid acl id");
        $this->response($output);
    }
}
