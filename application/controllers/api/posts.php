<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Posts extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Post_model', 'User_model'));

        $this->load->library('ion_auth');
        /*
        if ( !$this->ion_auth->is_admin()){
            exit('forbidden');
        }
        */
    }
    
    public function get_all_post_get(){
        $posts = $this->Post_model->get_all_posts();
        $this->load->view('sms_handle/posts_list', array('postslist' => $posts));
    }

    public function new_post_post(){
        $this->db->trans_start();
        $post = $this->post();

        if(isset($post['receiver'])){
            $users = $this->User_model->get_users_by_emails(explode(',', $post['receiver']));
            unset($post['receiver']);
            $posts = array_map(function($user) use ($post){
                return array_merge($post, array(
                    'url_code' => $user['id'] + 9881,
                    'owner_id' => $user['id']
                ));
            }, $users);
            $result = $this->Post_model->new_posts($posts);
        }
        else {
            $result = $this->Post_model->new_post($post);
        }
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        if($result){
            if($users){
                $posts = $this->Post_model->get_posts_by_ids(range($insert_id, $insert_id+count($users)-1));
                $this->response($posts, 200);
            }
            else {
                $post = $this->Post_model->get_post_by_id($insert_id);
                $this->response($post, 200);
            }
        }
        else {
            $this->response($result, 500);
        }
    }

    public function new_message_post(){
        $this->db->trans_start();
        $result = $this->Post_model->new_message($this->post());
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        if($result){
            $message = $this->Post_model->get_message_by_id($insert_id);
            $this->response($message, 200);
        }
        else {
            $this->response($result, 500);
        }
    }

    public function get_all_message_of_post_get(){
        $post_id = $this->input->get('post_id');
        $messages = $this->Post_model->get_messages_of_post($post_id);
        $this->response($messages, 200);
    }

    public function new_message_post(){
        $post = $this->input->post();
        if(isset($post['post_id'])){   
            $data = array(
                'post_id' => $post['post_id'],
                //'user_id'      => "9988",
                //'owner_id'    => 123,
                //'title'       => $post['title'],
                'content' => $post['content'],
            );          
            $ch = curl_init();
            $this->load->helper('url');
            $url = base_url("/restful/post/message");

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:multipart/form-data")); // 啟用POST
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_COOKIE, http_build_query($_COOKIE,'',';'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
            echo curl_exec($ch);
            curl_close($ch);

            $data = array(
                'post_id' => $post['post_id'],
                'user_read'    => "true",
                //'user_id'      => "9988",
                //'owner_id'    => 123,
                //'title'       => $post['title'],
                // 'content' => $post['content'],
            );          
            $ch = curl_init();
            $this->load->helper('url');
            $url = base_url("/restful/post/{$post['post_id']}");

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            //curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
            //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:multipart/form-data")); // 啟用POST
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_COOKIE, http_build_query($_COOKIE,'',';'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            //curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
            $file_id = json_decode(curl_exec($ch));
            curl_close($ch);
        }
        else {
            echo "false";
        }
    }// new_message_post


}
