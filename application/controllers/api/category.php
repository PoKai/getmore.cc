<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Category extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('lib_category');
        $this->table_fields = array("id", "type", "tw_name", "en_name", "sort_order", "add_time", "edit_time");
    }

    /**
     * Process Search User List
     *
     * @return json
     * @author appleboy
     **/
    private function _process_list($method = 'get')
    {
        $data = array();
        $data = $this->get_input_array($this->table_fields, null, $method);
        foreach ($data as $k => $v) {
            if (!empty($v) or mb_strlen($v) > 0) {
                if($k == 'type')
                    $this->lib_category->where($k, $v);
                else
                    $this->lib_category->like($k, $v);
            }
        }
    }

    /**
     * get Categories API
     *
     * @return json
     * @author appleboy
     **/
    public function getCategories_get($method = 'get')
    {
        $this->_process_list();
        $row = $this->lib_category->order_by('sort_order', 'asc')->categories()->result_array();
        $output = array(
            "success_text" => "ok",
            "item" => $row
        );
        $this->response($output);
    }

    /**
     * get Category API
     *
     * @return json
     * @author appleboy
     **/
    public function getCategory_get($method = 'get')
    {
        $output = array();
        $input_array = array("id");
        $data = $this->get_input_array($input_array, $input_array, $method);

        $row = $this->lib_category->where('id', $data['id'])->categories()->row_array();
        $output = array(
            "success_text" => "ok",
            "item" => $row
        );
        $this->response($output);
    }

    /**
     * Add Category API
     *
     * @return json
     * @author appleboy
     **/
    public function categoryAdd_post($method = 'post')
    {
        $output = array();
        $input_array = array("type", "tw_name", "en_name", "sort_order");
        $data = $this->get_input_array($input_array, array("tw_name", "en_name"), $method);

        $resource_id = $this->lib_category->add_category($data);
        $this->response(array("success_text" => "ok"));
    }

    /**
     * Update Category API
     *
     * @return json
     * @author appleboy
     **/
    public function categoryEdit_post($method = 'post')
    {
        $output = array();
        $input_array = array("id", "type", "tw_name", "en_name", "sort_order");
        $data = $this->get_input_array($input_array, $input_array, $method);

        $return = $this->lib_category->update_category($data['id'], $data);

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Invalid ID");
        $this->response($output);
    }

    /**
     * Delete Category API
     *
     * @return json
     * @author appleboy
     **/
    public function categoryDelete_post($method = 'post')
    {
        $output = array();
        $input_array = array("id");
        $data = $this->get_input_array($input_array, $input_array, $method);

        $return = $this->lib_category->delete_category($data['id']);

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Invalid acl id");
        $this->response($output);
    }
}
