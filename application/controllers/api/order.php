<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

/**
* Order API
*
* Order list API
*
* @category API
* @author   appleboy
*/
class Order extends REST_Controller
{
    /**
     * __construct
     *
     * @return null
     * @author appleboy
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('lib_order');
        $this->table_fields =  $this->db->list_fields('orders');
        $this->data = array();
    }

    /**
     * Process Search List
     *
     * @return null
     * @author appleboy
     **/
    private function _process_list($method = 'get')
    {
        $data = array();
        $search_array = array("s_user_name", "s_total_price", "s_status_id", "s_delivery_number", "s_date_purchased", "s_date_finished");
        $data = $this->get_input_array($search_array, null, $method);
        foreach ($data as $k => $v) {
            if (!empty($v) or mb_strlen($v) > 0) {
                $k = preg_replace('/^s_/', '', $k);
                $this->lib_order->like($k, $v);
            }
        }
    }

    /**
     * get Orders API
     *
     * @return json
     * @author appleboy
     */
    public function getOrders_get($method = 'get')
    {
        $outputs = $order_id = $orders = $products = $events = array();
        $this->_process_list();
        $result = $this->lib_order->select('orders.id, orders.order_number, orders.user_name, total_price, status_id, ' .
             'orders_status.name as order_status, delivery_number, date_purchased, date_finished')
             ->order_by('date_purchased', 'desc')
             ->orders()
             ->result_array();

        foreach ($result as $row) {
            $order_id[] = $row['id'];
        }

        if (empty($order_id)) {
            $output = array(
                "success_text" => "ok",
                "items" => $outputs
            );
            $this->response($output);
        }

        // get product
        $rows = $this->lib_order
            ->select('product_title, order_id, product_id')
            ->where('order_id', $order_id)
            ->orders('product')
            ->result_array();

        foreach ($rows as $row) {
            $data = array(
                'type' => 'product',
                'id' => $row['product_id'],
                'title' => $row['product_title']
            );
            $orders[$row['order_id']][] = $data;
        }

        // get event
        $rows = $this->lib_order
            ->select('event_title, order_id, event_id, event_quantity')
            ->where('order_id', $order_id)
            ->orders('event')
            ->result_array();

        foreach ($rows as $row) {
            $data = array(
                'type' => 'event',
                'id' => $row['event_id'],
                'title' => $row['event_title']
            );
            $orders[$row['order_id']][] = $data;
        }

        foreach ($result as $row) {
            // check orders exist
            if (isset($orders[$row['id']])) {
                $row['items'] = $orders[$row['id']];
                $outputs[] = $row;
            }
        }

        $output = array(
            "success_text" => "ok",
            "items" => $outputs
        );
        $this->response($output);
    }

    /**
     * get Order Status API
     *
     * @return json
     * @author appleboy
     */
    public function getOrderStatus_get($method = 'get')
    {
        $rows = $this->db->select('id, name')->get('orders_status')->result_array();
        $output = array(
            "success_text" => "ok",
            "items" => $rows
        );
        $this->response($output);
    }

    /**
     * get Order API
     *
     * @return json
     * @author appleboy
     */
    public function getOrder_get($method = 'get')
    {
        $output = array();
        $this->data = $this->get_input_array($this->table_fields, array('id'), $method);

        $order_id = $this->data['id'];
        $order = $this->lib_order->select('orders.id, user_id, users.email, orders.order_number, orders.user_name, user_address, user_mobile_phone, total_price, status_id, ' .
             'user_telephone, delivery_name, delivery_address, delivery_mobile_phone, delivery_telephone, payment_method, transportation_charge, ' .
             'uniform_invoice_type, orders_status.name as order_status, delivery_number, date_purchased, date_finished, orders.edit_time')
             ->where('orders.id', $this->data['id'])
             ->orders()->row_array();

        $event = $this->lib_order->select('*')->where('order_id', $order_id)->orders('event')->result_array();
        $product = $this->lib_order->select('*')->where('order_id', $order_id)->orders('product')->result_array();

        $order['number'] = sprintf("%08s", $order_id);
        $data = array(
            'order'             => $order,
            'event'             => $event,
            'product'           => $product,
            'total_sell_price'  => 0,
            'total_event_price' => 0,
            'payment_method'    => $this->lib_order->payment_method($order['payment_method']),
            'uniform_invoice'   => $this->lib_order->uniform_invoice($order),
        );

        $order['single'] = $this->load->view("order/single", $data, true);
        $order['products'] = $product;

        $output = array(
            "success_text" => "ok",
            "item" => $order
        );
        $this->response($output);
    }

    /**
     * Update Order API
     *
     * @return json
     * @author appleboy
     **/
    public function orderEdit_post($method = 'post')
    {
        $output = array();

        $this->data = $this->get_input_array($this->table_fields, array("id"), $method);

        $return = $this->lib_order->update_order($this->data['id'], $this->data);

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Invalid ID");
        $this->response($output);
    }

    /**
     * Update Order Delivery Number API
     *
     * @return json
     * @author appleboy
     **/
    public function orderDeliveryNumber_post($method = 'post')
    {
        $output = array();
        $return = false;
        $this->data = $this->get_input_array(array("id", "delivery_prefix", "delivery_count", "delivery_number"), array("id"), $method);

        if (isset($this->data['id']) and !empty($this->data['id'])) {

            if (is_array($this->data['id'])) {
                foreach ($this->data['id'] as $row) {
                    $delivery_number = $this->data['delivery_prefix'] . sprintf('%0' . $this->data['delivery_count'] . 'd', $this->data['delivery_number']);

                    $data = array(
                        'delivery_number' => $delivery_number
                    );
                    $return = $this->lib_order->update_order($row, $data);

                    $this->data['delivery_number'] = (int) $this->data['delivery_number'] + 1;
                }
            } else {
                $delivery_number = $this->data['delivery_prefix'] . sprintf('%0' . $this->data['delivery_count'] . 'd', $this->data['delivery_number']);

                $data = array(
                    'delivery_number' => $delivery_number
                );
                $return = $this->lib_order->update_order($this->data['id'], $data);
            }

        }

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Invalid ID");
        $this->response($output);
    }

    /**
     * Delete Order API
     *
     * @return json
     * @author appleboy
     */
    public function orderDelete_post($method = 'post')
    {

    }
}
