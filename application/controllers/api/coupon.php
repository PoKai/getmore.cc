<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Coupon extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        
        $this->load->library('ion_auth');
        if ( !$this->ion_auth->is_admin()){
            exit('forbidden');
        }        
        
        $this->load->database();
        $this->load->model('Coupon_model', 'coupon');
    }

    public function getcoupons_get()
    {
        $coupons = $this->coupon->get_all();
        $output = array(
            'coupons' => $coupons
        );
        $this->response($output);
    }
    
    public function getcoupon_get()
    {
        $id = $this->input->get('id');
        $coupon = $this->coupon->get($id);
        $output = array(
            'coupon' => $coupon
        );
        $this->response($output);
    }    
    
    public function updateCoupon_post()
    {
        $id = $this->input->post('id');        
        $name = $this->input->post('name');
        $code = $this->input->post('code');
        $amount = $this->input->post('amount');
        $times_available = $this->input->post('times_available');
        $minimum_charge = $this->input->post('minimum_charge');
        $expired_at = $this->input->post('expired_at');
        $creator_id = $this->session->userdata('user_id');
        $dedicated_user_id = $this->input->post('dedicated_user_id');
        
        $data = array(
            'name' => $name,
            'code' => $code,
            'amount' => $amount,
            'times_available' => $times_available,
            'minimum_charge' => $minimum_charge,
            'expired_at' => $expired_at,
            'creator_id' => $creator_id,
            'dedicated_user_id' => $dedicated_user_id     
        );
        
        try{
            $this->coupon->update($id, $data);
        }
        catch (Exception $e){
            show_error($e->getMessage());
        }
        $output = array(
            "success_text" => "ok"
        );
        $this->response($output);
    }    
    
    public function deleteCoupon_post()
    {
        $id = $this->input->post('id');        
        
        try{
            $this->coupon->delete($id);
        }
        catch (Exception $e){
            show_error($e->getMessage());
        }
        $output = array(
            "success_text" => "ok"
        );
        $this->response($output);
    }    
    

    
    public function addCoupon_post()
    {
        $name = $this->input->post('name');
        $code = $this->input->post('code');
        $amount = $this->input->post('amount');
        $times_available = $this->input->post('times_available');
        $minimum_charge = $this->input->post('minimum_charge');
        $expired_at = $this->input->post('expired_at');
        $creator_id = $this->session->userdata('user_id');
        $dedicated_user_id = $this->input->post('dedicated_user_id');
        
        $data = array(
            'name' => $name,
            'code' => $code,
            'amount' => $amount,
            'times_available' => $times_available,
            'minimum_charge' => $minimum_charge,
            'expired_at' => $expired_at,
            'creator_id' => $creator_id,
            'dedicated_user_id'  => $dedicated_user_id    
        );
        
        try{
            $this->coupon->create($data);
        }
        catch (Exception $e){
            show_error($e->getMessage());
        }
        $output = array(
            "success_text" => "ok"
        );
        $this->response($output);
    }

    public function uploadCoupon_post()
    {
        try{
            $csv_data = $this->coupon->parse_from_file_to_array($_FILES['csv']['tmp_name']);
            $creator_id = $this->session->userdata('user_id');
            $ids = $this->coupon->create_from_csv_array($csv_data, $creator_id);
        }
        catch(Exception $e){
            show_error(json_encode($e->getMessage()));
        }
        /*
        $data = array(
            'name' => $name,
            'code' => $code,
            'amount' => $amount,
            'times_available' => $times_available,
            'minimum_charge' => $minimum_charge,
            'expired_at' => $expired_at,
            'creator_id' => $creator_id,
            'dedicated_user_id'  => $dedicated_user_id    
        );
        

        $this->coupon->create($data);
        */
        $output = array(
            "success_text" => "ok",
            'csv'                 => $csv_data,
            'ids'                  => $ids
        );
        $this->response($output);
    }

    public function notifyCoupons_post(){
        $ids = $this->input->post('ids');
        $ids = explode(',', $ids);
        $content = $this->input->post('content');

        $this->load->library('typography');
        $content = $this->typography->auto_typography($content);        
        
        foreach($ids as $id){
            $this->coupon->notify_coupon_owner($id, $content);
        }
        $output = array(
            "success_text" => "ok"
        );
        $this->response($output);
    }


}
