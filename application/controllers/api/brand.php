<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Brand extends REST_Controller
{
    
    protected $data;
  
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('ion_auth');
        if ( !$this->ion_auth->is_admin()){
            exit('forbidden');
        }        
        
        $this->load->model('Brand_model');
    }

    public function getAll_get(){
        $this->load->helper('button');
        $this->db->order_by('name', 'asc');
        $data['approved_brands'] = $this->Brand_model->get_many_by('approved', true);
        $this->db->order_by('created_at', 'desc');
        
        $brand_categories = $this->Brand_model->get_categories();
        usort($brand_categories, function($a, $b){
            return $a['id'] < $b['id']?-1:1;
        });
        $data['brand_categories'] = $brand_categories;
        $data['unapproved_brands'] = $this->Brand_model->get_many_by('approved', false);
        $this->load->view('brand/list', $data);    
    }
    
    function approve_post(){
        if(!$category_id = $this->input->post('category_id')){
            $category_id = $this->Brand_model->new_category($this->input->post('category_name'));
        }

        $this->Brand_model->update($this->input->post('id'), ['approved'=>true, 'category_id' => $category_id]);
        redirect('/admin/#!/brand/list');
    }
    
    function remove_post(){
        $this->Brand_model->delete($this->input->post('id'));
        redirect('/admin/#!/brand/list');      
    }

    function add_post(){
        if(!$category_id = $this->input->post('category_id')){
            $category_id = $this->Brand_model->new_category($this->input->post('category_name'));
        }

        $this->Brand_model->insert([
            'approved'=>true,
            'name' => $this->input->post('name'),
            'category_id' => $category_id
        ]);
        redirect('/admin/#!/brand/list');
    }
    
}
