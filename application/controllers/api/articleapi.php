<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Articleapi extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('ion_auth');
        $this->load->library(array('curl', 'session'));
        if (!$this->ion_auth->is_admin() && !$this->ion_auth->is_blogger()){
            exit('forbidden');
        }        
        
        $this->load->library('Getmore/Article/ArticleDataMapper', '', 'article_dm');
        $this->load->library('Getmore/Article/ArticleRepository', '', 'article_rp');

    }

    public function getAll_get()
    {
        $output = array(
            "success_text" => "ok",
            "articles" => $this->article_dm->getAll()
        );
        $this->response($output);
    }
    
    public function getOne_get()
    {
        $cates = $this->db->from('article_categories')->get()->result();
        $output = array(
            "success_text" => "ok",
            "article" => $this->article_rp->get($this->input->get('id')),
            'categories' => $cates
        );
        $this->response($output);
    }    
    
    function create_post(){
        $userfile = $_FILES['userfile'];
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, base_url("/restful/blog/file/blog"));
        curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:multipart/form-data")); // 啟用POST
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIE, http_build_query($_COOKIE,'',';'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            'userfile' => "@".$userfile['tmp_name']
        )); 
        $file_id = json_decode(curl_exec($ch));
        curl_close($ch);
        
        $article = new Article();
        $article->title = $this->input->post('title');
        $article->content = $this->input->post('content');
        $article->feature_pic_id = $file_id;
        $article->author = $this->input->post('author');
        $article->about = $this->input->post('about');
        $article->published = $this->input->post('published');
        $article->category_id = $this->input->post('category_id');
        $article->intro = $this->input->post('intro');
        
        $this->article_rp->save($article);
        
        redirect('/admin/#!/article/list');
    }
    
    function delete_post(){
        $article = $this->article_dm->get($this->input->post('id'));

        $this->curl->simple_delete(base_url("/restful/blog/file/".$article->feature_pic_id));

        $this->article_dm->remove($article);
        redirect('/admin/#!/article/list');
    }

    function update_post(){
        
        $article = $this->article_rp->get($this->input->post('id'));
        
        $article->title = $this->input->post('title');
        $article->content = $this->input->post('content');
        $article->author = $this->input->post('author');
        $article->about = $this->input->post('about');
        $article->published = $this->input->post('published');
        $article->category_id = $this->input->post('category_id');
        $article->intro = $this->input->post('intro');

        // only update when user upload file
        if ($_FILES['userfile']['error'] != UPLOAD_ERR_NO_FILE) {
            $this->load->library('files_lib');
            $file_id = $this->files_lib->straight_upload();
            $article->feature_pic_id = $file_id;          
        }
        
        $this->article_rp->save($article);
      
        redirect('/admin/#!/article/list');
    }

    public function getAllCategories_get(){
        $cates = $this->db->from('article_categories')->get()->result_array();
        
        $output = array(
            "success_text" => "ok",
            "categories" => $cates
        );
        $this->response($output);
    }    

    function create_category_post(){
        $data = array('name'=>$this->input->post('name'));
        $this->db->insert('article_categories', $data);              
        redirect('/admin/#!/article/category');
    }

    function delete_category_post(){
       $this->db->where('id', $this->input->post('id'))
                         ->delete('article_categories');
        redirect('/admin/#!/article/category');
    }
    
    function update_category_post(){
        $data = array('name'=>$this->input->post('value'));
        $this->db->where('id', $this->input->post('id'))
                         ->update('article_categories', $data);
        echo $data['name'];
    }

}
