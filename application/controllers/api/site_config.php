<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Site_config extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('lib_config', 'files_lib'));
        $this->table_fields = $this->lib_config->keys();
    }

    /**
     * get site config API
     *
     * @return json
     * @author appleboy
     **/
    public function getConfigs_get($method = 'get')
    {
        $output = array();
        $data = $this->get_input_array($this->table_fields, NULL, $method);

        $rows = $this->lib_config->select('*')->configs()->result_array();

        foreach ($rows as $row) {
            if ($row['name'] == 'banner_list' and $row['value'] != '') {
                $output['banner_file_list'] = $this->lib_config->top_image();
            }
            if ($row['name'] == 'link_list' and $row['value'] != '') {
                $output['link_url_list'] = array();
                $output['link_url'] = explode(',', $row['value']);
                foreach ($output['link_url'] as $item) {
                    if (stripos($item, '::') !== false) {
                        $item = explode('::', $item);
                        $data = array(
                            "link" => $item[0],
                            "target" => ($item[1] == "1") ? true : false,
                            "target_value" => $item[1]
                        );
                        $output['link_url_list'][] = $data;
                    }
                }
            }
            $output[$row['name']] = $row['value'];
        }

        $output = array(
            "success_text" => "ok",
            "item" => $output
        );
        $this->response($output);
    }

    /**
     * Update Config API
     *
     * @return json
     * @author appleboy
     **/
    public function configEdit_post($method = 'post')
    {
        $output = array();
        $data = $this->get_input_array($this->table_fields, null, $method);

        foreach ($data as $k => $v) {
            $v = (is_array($v)) ? implode(',', $v) : $v;
            $this->lib_config->update_config($k, array('value' => $v));
        }

        $this->response(array("success_text" => "ok"));
    }
}
