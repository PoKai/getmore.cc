<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Event extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('lib_event');
        $this->load->library('lib_product');
        $this->load->library('files_lib');
        $this->table_fields = array_merge($this->db->list_fields(EVENT_TABLE), array('is_index', 'is_enabled', 'detail_name_list', 'detail_value_list'));
        $this->list_array = array('detail');
        $this->data = array();
    }

    /**
     * Process Search List
     *
     * @return null
     * @author appleboy
     **/
    private function _process_list($method = 'get')
    {
        $data = array();
        $search_array = array("s_category_name", "s_title", "s_is_index", "s_is_enabled", "s_username", "s_teacher");
        $data = $this->get_input_array($search_array, null, $method);
        foreach ($data as $k => $v) {
            $k = preg_replace('/^s_/', '', $k);
            if (!empty($v) or mb_strlen($v) > 0) {
                if ($k == "category_name") {
                    $this->lib_event->like('category.tw_name', $v);
                } elseif ($k == "is_index" or $k == "is_enabled") {
                    if ($k == "is_index") {
                        if ($v == "1")
                            $this->lib_event->where('`options`&' . (_IS_INDEX) . ' >= 1');
                        else
                            $this->lib_event->where('`options`&' . (_IS_INDEX) . ' = 0');
                    } elseif ($k == "is_enabled") {
                        if ($v == "1")
                            $this->lib_event->where('`options`&' . (_IS_ENABLED) . ' >= 1');
                        else
                            $this->lib_event->where('`options`&' . (_IS_ENABLED) . ' = 0');
                    }
                } else {
                    $this->lib_event->like($k, $v);
                }
            }
        }
    }

    /**
     * get Events API
     *
     * @return json
     * @author appleboy
     **/
    public function getEvents_get($method = 'get')
    {
        $this->_process_list();
        $row = $this->lib_event->select('category.tw_name as category_name, events.*, IF(events.options & ' . _IS_INDEX . ', true, false) as is_index, , IF(events.options & ' . _IS_ENABLED . ', true, false) as is_enabled, DATE_FORMAT(`up_time`, "%Y-%m-%d %h:%i") as up_time, DATE_FORMAT(`down_time`, "%Y-%m-%d %h:%i") as down_time')->events()->result_array();
        $output = array(
            "success_text" => "ok",
            "items" => $row
        );
        $this->response($output);
    }

    /**
     * Event list API (front end)
     *
     * @return json
     * @author appleboy
     **/
    public function getEventList_get($method = 'get')
    {
        $output = array();
        $input_array = array('limit', 'offset');
        $this->_data = $this->get_input_array($input_array, array('limit'), $method);

        $return = $this->lib_event->get_list($this->_data);

        $output = array(
            "success_text" => "ok",
            "items" => $return['output']
        );
        $this->response($output);
    }

    /**
     * get Event Register API
     *
     * @return json
     * @author appleboy
     **/
    public function getEventRegisters_get($method = 'get')
    {
        $output = array();
        $this->data = $this->get_input_array(array("id"), array("id"), $method);

        $event = $this->lib_event->select("category.tw_name as category_name, events.*")
                ->where('events.id', $this->data['id'])
                ->events()->row_array();

        $event['items'] = $this->lib_event->select("events_users.order_id, events_users.user_name, " .
                "events_users.user_phone, orders.user_name as order_user_name, email, orders.user_telephone, orders.user_mobile_phone, orders.add_time")
                ->where('events_users.event_id', $this->data['id'])
                ->order_by('orders.add_time')
                ->registers()->result_array();

        $output = array(
            "success_text" => "ok",
            "item" => $event
        );
        $this->response($output);
    }

    /**
     * get Event API
     *
     * @return json
     * @author appleboy
     **/
    public function getEvent_get($method = 'get')
    {
        $output = array();
        $input_array = array("id");
        $this->data = $this->get_input_array($input_array, $input_array, $method);

        $row = $this->lib_event->select('events.*, IF(events.options & ' . _IS_INDEX . ', true, false) as is_index, , IF(events.options & ' . _IS_ENABLED . ', true, false) as is_enabled, DATE_FORMAT(`event_time`, "%Y-%m-%d %h:%i") as event_time, DATE_FORMAT(`up_time`, "%Y-%m-%d %h:%i") as up_time, DATE_FORMAT(`down_time`, "%Y-%m-%d %h:%i") as down_time')->where('events.id', $this->data['id'])->events()->row_array();
        if ($row['file_list'] != '') {
            $files = $this->files_lib->where('file_id', explode(',', $row['file_list']))->order_by_field('file_id', $row['file_list'])->files()->result_array();
            $row['image_list'] = $files;
        }
        if ($row['product_list'] != '') {
            $files = $this->lib_product->select('products.*')->where('products.id', explode(',', $row['product_list']))->products()->result_array();
            $row['product_list'] = $files;
        }
        // handle detail informtion
        foreach ($this->list_array as $item) {
            (!empty($row[$item])) and $row[$item] = json_decode($row[$item]);
        }
        $output = array(
            "success_text" => "ok",
            "item" => $row
        );
        $this->response($output);
    }

    /**
     * Process detail API
     *
     * @return this
     * @author appleboy
     **/
    private function _process_detail()
    {
        foreach ($this->list_array as $item) {
            $list_name = $item . '_name_list';
            $list_value = $item . '_value_list';
            if (isset($this->data[$list_name])) {
                $list = array();
                $i = 0;
                foreach ($this->data[$list_name] as $row) {
                    $list[] = array('model' => $item, 'name' => $this->data[$list_name][$i], 'value' => $this->data[$list_value][$i]);
                    $i++;
                }
                unset($this->data[$list_name]);
                unset($this->data[$list_value]);
                $this->data[$item] = json_encode($list);
            }
        }

        return $this;
    }

    /**
     * Add Event API
     *
     * @return json
     * @author appleboy
     **/
    public function eventAdd_post($method = 'post')
    {
        $output = array();
        $this->data = $this->get_input_array($this->table_fields, null, $method);

        $this->data['file_list'] = (isset($this->data['file_list']) and is_array($this->data['file_list'])) ? implode(',', $this->data['file_list']) : '';
        $this->data['product_list'] = (isset($this->data['product_list']) and is_array($this->data['product_list'])) ? implode(',', $this->data['product_list']) : '';
        $this->_process_detail();

        $resource_id = $this->lib_event->add_event($this->data);
        $this->response(array("success_text" => "ok"));
    }

    /**
     * Update Event API
     *
     * @return json
     * @author appleboy
     **/
    public function eventEdit_post($method = 'post')
    {
        $output = array();
        $this->data = $this->get_input_array($this->table_fields, array("id"), $method);

        if (isset($this->data['file_list']) and is_array($this->data['file_list'])) {
            $this->data['file_list'] = implode(',', $this->data['file_list']);
        }

        if (isset($this->data['product_list']) and is_array($this->data['product_list'])) {
            $this->data['product_list'] = implode(',', $this->data['product_list']);
        }

        $this->_process_detail();

        $return = $this->lib_event->update_event($this->data['id'], $this->data);

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Invalid ID");
        $this->response($output);
    }

    /**
     * Delete Event API
     *
     * @return json
     * @author appleboy
     **/
    public function eventDelete_post($method = 'post')
    {
        $output = array();
        $input_array = array("id");
        $this->data = $this->get_input_array($input_array, $input_array, $method);

        $return = $this->lib_event->delete_event($this->data['id']);

        $output = ($return) ? array("success_text" => "ok") : array("error_text" => "Invalid acl id");
        $this->response($output);
    }
}
