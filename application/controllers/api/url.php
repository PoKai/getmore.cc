<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Url extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('lib_static_url');
        $this->table_fields = $this->db->list_fields(URL_TABLE);
        $this->data = array();
    }

    /**
     * Process Search List
     *
     * @return null
     * @author appleboy
     **/
    private function _process_list($method = 'get')
    {
        $data = array();
        $search_array = array("s_title", "s_url");
        $data = $this->get_input_array($search_array, null, $method);
        foreach ($data as $k => $v) {
            if (!empty($v) or mb_strlen($v) > 0) {
                $k = preg_replace('/^s_/', '', $k);
                $this->lib_static_url->like($k, $v);
            }
        }
    }

    /**
     * Process Routing
     *
     * @return null
     * @author appleboy
     **/
    private function _process_route($method = 'get')
    {
        $data = "<?php \n";
        $config_file = APPPATH . 'config/getmore.php';
        $rows = $this->lib_static_url->select("url")->items()->result_array();
        foreach ($rows as $row) {
            if (empty($row['url'])) {
                continue;
            }
            $data .= '$route[\'' . $row['url'] . '\'] = \'home/url/'.$row['url'].'\';' . "\n";
        }
        file_put_contents($config_file, $data);
    }

    /**
     * get Urls API
     *
     * @return json
     * @author appleboy
     */
    public function getUrls_get($method = 'get')
    {
        $this->_process_list();
        $rows = $this->lib_static_url->select("id, title, url, add_time, edit_time")->items()->result_array();
        $output = array(
            "success_text" => "ok",
            "items" => $rows
        );
        $this->response($output);
    }

    /**
     * get Url API
     *
     * @return json
     * @author appleboy
     */
    public function getUrl_get($method = 'get')
    {
        $output = array();
        $input_array = array("id");
        $this->data = $this->get_input_array($input_array, $input_array, $method);

        $row = $this->lib_static_url->select('id, title, url, content, add_time, edit_time')->item($this->data['id'])->row_array();
        $output = array(
            "success_text" => "ok",
            "item" => $row
        );
        $this->data = array_merge($this->data, $output);
        $this->response($this->data);
    }

    /**
     * Add Url API
     *
     * @return json
     * @author appleboy
     */
    public function urlAdd_post($method = 'post')
    {
        $this->data = $this->get_input_array($this->table_fields, null, $method);

        $id = $this->lib_static_url->insert($this->data);
        $this->data['success_text'] = 'ok';
        $this->_process_route();
        $this->response($this->data);
    }

    /**
     * Update Url API
     *
     * @return json
     * @author appleboy
     */
    public function urlEdit_post($method = 'post')
    {
        $this->data = $this->get_input_array($this->table_fields, array("id"), $method);

        $return = $this->lib_static_url->update($this->data['id'], $this->data);

        if ($return) {
            $this->data['success_text'] = 'ok';
        } else {
            $this->data['error_text'] = 'Invalid ID';
        }
        $this->_process_route();
        $this->response($this->data);
    }

    /**
     * Delete Url API
     *
     * @return json
     * @author appleboy
     */
    public function urlDelete_post($method = 'post')
    {
        $this->data = $this->get_input_array($this->table_fields, array("id"), $method);

        $return = $this->lib_static_url->delete($this->data['id']);

        if ($return) {
            $this->data['success_text'] = 'ok';
        } else {
            $this->data['error_text'] = 'Invalid ID';
        }
        $this->response($this->data);
    }
}
