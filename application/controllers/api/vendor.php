<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Vendor extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('ion_auth');
        if ( !$this->ion_auth->is_admin()){
            exit('forbidden');
        }
        
        $this->load->database();
        $this->load->model('Vendor_model');
    }

    public function vendor_list_get()
    {
        $orders = $this->Vendor_model->get_list();
        $output = array(
            'orders' => $orders
        );
        $this->response($output);
    }

    public function vendor_view_get()
    {
        $order_number = $this->input->get('order_number');
        $data = $this->Vendor_model->get_by_order_number($order_number);
        
        $data['order']['is_paid'] = false;
        if ($data['order']['status']!=1){
            $data['order']['is_paid'] = true;          
        }
        
        $output = array(
            'order' => $data['order'],
            'clothings' => $data['clothings']
        );
        $this->response($output);
    }
    
    public function vendor_create_post()
    {
        $customer_id = $this->input->post('user_id');
        $order_number = $this->Vendor_model->create($customer_id);
        $output = array(
            'order_number' => $order_number
        );
        $this->response($output);
    }

    public function add_to_order_post()
    {
        $washing_id = $this->input->post('washing_id');
        $order_number = $this->input->post('order_number');
        
        try{
            $result = $this->Vendor_model->put_in_by_washing_id($washing_id, $order_number);
        }
        catch(Exception $e){
            show_error($e->getMessage());          
        }
      
        $output = array(
            'result' => 'success'
        );
        $this->response($output);
    }    

    public function checkout_order_post()
    {
        $order_number = $this->input->post('order_number');
        
        try{
            $result = $this->Vendor_model->checkout_order($order_number);
        }
        catch(Exception $e){
            show_error($e->getMessage());          
        }
      
        $output = array(
            'result' => 'success'
        );
        $this->response($output);
    }    

    public function remove_from_order_post()
    {
        $clothing_id = $this->input->post('clothing_id');
        
        try{
            $result = $this->Vendor_model->remove_from_order($clothing_id);
        }
        catch(Exception $e){
            show_error($e->getMessage());          
        }
      
        $output = array(
            'result' => 'success'
        );
        $this->response($output);
    }

    
}
