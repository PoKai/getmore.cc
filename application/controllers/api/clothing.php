<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Clothing extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('Product_clothing_model');

        $this->load->library('ion_auth');
        /*
        if ( !$this->ion_auth->is_admin()){
            exit('forbidden');
        }
        */
    }
    
    function updatePrice_post(){
        $new_price = $this->input->post('value');
        $id = $this->input->post('id');

        $this->load->database();
        $this->load->model('Product_clothing_model', 'clothing');
        $clothing = $this->clothing->get($id);
        
        try{
            $this->clothing->set_suggested_price($id, $new_price);

            echo $new_price;
        }
        catch (Exception $e){
            // echo $clothing->product_sell_price;
            show_error('failed');
        }
    }

    function updateOriginalPrice_post(){
        $new_originalprice = $this->input->post('value');
        $id = $this->input->post('id');

        $this->load->database();
        $this->load->model('Product_clothing_model', 'clothing');
        
        try{
            $this->clothing->update($id, array( 'product_original_price' =>  $new_originalprice) );
            echo  $new_originalprice;
        }
        catch (Exception $e){
            // echo $clothing->product_sell_price;
            show_error('failed');
        }
    }

    function updateSellPrice_post(){
        $new_sellprice = $this->input->post('value');
        $id = $this->input->post('id');

        $this->load->database();
        $this->load->model('Product_clothing_model', 'clothing');
        
        try{
            $this->clothing->update($id, array( 'product_sell_price' =>  $new_sellprice) );
            echo  $new_sellprice;
        }
        catch (Exception $e){
            // echo $clothing->product_sell_price;
            show_error('failed');
        }
    }

    function updateBranded_post(){
        $new_branded = $this->input->post('value');
        $id = $this->input->post('id');

        $this->load->database();
        $this->load->model('Product_clothing_model', 'clothing');
        $clothing = $this->clothing->get($id);
        
        try{
            $this->clothing->update($id, array( 'branded' => $new_branded) );
            echo $new_branded;
        }
        catch (Exception $e){
            // echo $clothing->product_sell_price;
            show_error('failed');
        }
    }

    public function getAll_get()
    {
        $clothing =$this->Product_clothing_model->get_all_on_shelf();
        $output = array(
            "success_text" => "ok",
            "items" => $clothing
        );
        $this->response($output);
    }

    public function getDraft_get(){
        $clothing =$this->Product_clothing_model->get_all_on_draft();
        $output = array(
            "success_text" => "ok",
            "items" => $clothing
        );
        $this->response($output);
    }

    public function launchDraft_post(){
        $draft_ids = $this->input->post('draft_ids');
        $result = $this->Product_clothing_model->launch_drafts_by_product_item_numbers($draft_ids);
        
        if($result){
            $this->response($draft_ids, 200);
        }
        else {
            $this->response($draft_ids, 500);
        }
    }

    public function get_all_off_shelf_get(){
        $clothing =$this->Product_clothing_model->get_all_off_shelf();
        $output = array(
            "success_text" => "ok",
            "items" => $clothing
        );
        $this->response($output);
    }

    
    public function getOneByProductItemNumber_get()
    {
        $clothing =$this->Product_clothing_model->get_one_by_product_item_number($this->get('product_item_number'));
        $output = array(
            "success_text" => "ok",
            "item" => $clothing
        );
        $this->response($output);
    }
    
    /* update one item by product item number
     * this is made for admin site calling update API.
     * 
     * @author howtomakeaturn
     */    
    public function updateOneByProductItemNumber_post()
    {
        $data = [];
        $data['product_item_number'] = $this->post('product_item_number');
        $data['product_title'] = $this->post('product_title');
        $data['product_brand'] = $this->post('product_brand');
        $data['files'] = $this->post('files');

        $data['size_type_major'] = $this->post('size_type_major');
        $data['size_type_minor'] = $this->post('size_type_minor');
        $data['size'] = $this->post('size');
        $data['size_number_minor'] = $this->post('size_number_minor');
        $data['size_standard'] = $this->post('size_standard');

        $data['size_comment'] = $this->post('size_comment');
        $data['item_status'] = $this->post('item_status');
        $data['measure'] = $this->post('measure');
        $data['style'] = $this->post('style');
        $data['descript'] = $this->post('descript');

        $data['admin_comment'] = $this->post('admin_comment');
        $data['admin_remark'] = $this->post('admin_remark');

        $data['color'] = $this->post('color');

        $data['publication_time'] = $this->post('publication_time');
        
        if($data['publication_time'] === ""){
            $data['publication_time'] = date("Y-m-d H:i:s");

            if($this->post('draft') === "true"){
                $data['lock_status'] = 4;
                $data['on_shelf'] = 0;
            }
            else if($this->post('draft') === "false") {
                $data['lock_status'] = 0;
                $data['on_shelf'] = 1;
            }
        }
        // as if publication_time is set and bigger than now
        else if(strtotime($data['publication_time']) > time()){
            $data['lock_status'] = 3;
            $data['on_shelf'] = 0;
        }
        else if($data['publication_time']) {
            $data['lock_status'] = 0;
            $data['on_shelf'] = 1;
        }
        try{
            $this->Product_clothing_model->update_one_by_product_item_number($this->post('product_item_number'), $data);
        }
        catch (Exception $e) {
            show_error( 'Caught exception: ' . $e->getMessage());
        }
        $output = array(
            "success_text" => "ok"
        );
        $this->response($output);
    }    
    
    function returnItem_post(){
        $this->load->database();
        $this->load->model('Product_clothing_model', 'clothing');
        $this->load->library('Getmore/Services/MailSender');
        $item_number = $this->input->post('item_number');
        @$this->mailsender->send_return_item_reminder_mail($item_number);
        $result = $this->clothing->return_item($item_number);
        // Since it's not very important, depress the error message.
        
        if ($result){
            $output = array(
                "status" => "success",
                'result'              => $result
            );
        }
        else{
            $output = array(
                "status" => "",
            );
        }
        $this->response($output);
    }
}
