<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH. 'libraries/REST_Controller.php');

class Buyorder extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('ion_auth');
        if ( !$this->ion_auth->is_admin() && !$this->ion_auth->is_operation() ){
            exit('forbidden');
        }        
        
        $this->load->model('Buy_order_model');
        $this->load->model('Product_clothing_model');
    }

    public function getAll_get($status = null)
    {
        $query =$this->Buy_order_model->db->from('buy_order')->order_by("created_at", "desc");
        
        if($status){
            $query = $query->where('status', $status);
        }

        $clothing = $query->get()->result_array();

        $output = array(
            "success_text" => "ok",
            "items" => $clothing
        );
        $this->response($output);
    }
    
    public function getOneByOrderNumber_get()
    {
        $clothing =$this->Buy_order_model->get_one_by_order_number($this->get('order_number'));
        $output = array(
            "success_text" => "ok",
            "item" => $clothing
        );
        $this->response($output);
    }
    
    public function getItemsByOrderNumber_get()
    {
        $order =$this->Buy_order_model->get_one_by_order_number($this->get('order_number'));
      
        $clothing =$this->Product_clothing_model->get_some_by_order_id_and_join_sellorder_product($order['id']);
        $output = array(
            "success_text" => "ok",
            "items" => $clothing
        );
        $this->response($output);
    }
    
    public function updateOneByOrderNumber_post()
    {
        $data = [];
        if ($this->input->post('shipping_number')){
            $data['shipping_number'] = $this->post('shipping_number');
        }
        $data['note'] = $this->post('note');
        $data['status'] = $this->post('status');
        
        if ( $data['status'] == 3 ){
            $data['shipped_at'] = date("Y-m-d H:i:s");
        }

        if ( $data['status'] == 4 ){
            $order = $this->Buy_order_model->get_by('order_number', $this->post('order_number'));
            if ($order->payment_method == 'pay-at-home'){
                $data['paid_at'] = date("Y-m-d H:i:s");
                $this->Buy_order_model->switch_one_to_paid_status_by_order_number($this->post('order_number'));                
            }
        }

        
        try{
            if ( $data['status'] == 5 ){
                $this->Buy_order_model->switch_one_to_done_status_by_order_number($this->post('order_number'));
            }
        }
        catch (Exception $e) {
            show_error( 'Caught exception: ' . $e->getMessage());
        }
        
        
        try{
            if ( $data['status'] == '6' ){
                $this->Buy_order_model->reject($this->post('order_number'));
            }
        }
        catch (Exception $e) {
            show_error( 'Caught exception: ' . $e->getMessage());
        }        


        try{
            $this->Buy_order_model->update_one_by_order_number($this->post('order_number'), $data);
        }
        catch (Exception $e) {
            show_error( 'Caught exception: ' . $e->getMessage());
        }
        
        $output = array(
            "success_text" => "ok"
        );
        $this->response($output);
    }

    public function recalculate_post()
    {
        $order_id = $this->post('order_id');
                
        try{
            $this->Buy_order_model->refresh_items_amount_and_price($order_id);
            $output = array(
                "success_text" => "ok"
            );
        }
        catch (Exception $e) {
            $output = array(
                "error_text" => 'Caught exception: ' . $e->getMessage()
            );          
        }
        $this->response($output);
    }    

    function updateShippingNumber_post(){
        $id = $this->input->post('id');
        $value = $this->input->post('value');

        try{
            $this->Buy_order_model->update( $id, array('shipping_number'=>$value) );
            echo $value;
        }
        catch (Exception $e){
            show_error('failed');
        }
    }    
    
}
