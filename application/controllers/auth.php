<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library(array('ion_auth', 'session', 'form_validation', 'lib_cart', 'lib_event', 'lib_product', 'lib_order'));
		$this->load->database();
		$this->load->helper('url');
		$this->load->spark('Twiggy/0.8.5');
		$this->template->set('is_hidden_header', true);
		$this->form_validation->set_error_delimiters('<div class="note">*&nbsp;', '</div>');
		$this->default_address = array(
			'county' => '',
			'district' => '',
			'zipcode' => '',
			'address' => '',
		);
		$this->twiggy->theme('');
		$this->twiggy->set(array('lang' => 'zh-tw', 'meta_charset' => 'UTF-8', 'site_title' => 'Getmore 專注在「時尚」社群的購物網站', 'site_description' => 'Getmore 是聚集新銳設計師、買手、時尚愛好者的購物網站，供應獨特單品以及二手時尚的時尚購物平台。時尚很多元，就像是一個個精彩的小圈圈，Getmore 負責把時尚圈在一起，你也可以稱這裡是「時尚小圈圈」', 'site_keywords' => '時尚,衣著,服飾,時裝,二手衣,流行,新銳設計師,亞洲時尚,褲子,買二手衣,賣二手衣,環保,電子商務,startup,二次時尚', 'logined' => $this->ion_auth->logged_in()), NULL, TRUE);
	}

	function forgotten_password_code() {
		$this->load->model('Forgotten_password_code_model', 'FP');

		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$code = $this->input->post('code');
			$new_password = $this->input->post('new_password');
			$confirm_password = $this->input->post('confirm_password');
			try {
				$this->FP->handle($code, $new_password, $confirm_password);
				$this->session->set_flashdata('auth_message', '密碼重設成功！直接回首頁登入！');
				$this->template->render('auth/success_password');
			} catch (Exception $e) {
				$this->session->set_flashdata('auth_message', $e->getMessage());
				redirect('/auth/forgotten_password_code?code=' . $code, 'refresh');
			}

		}
		// GET request, handle it
		else {
			$code = $this->input->get('code');
			try {
				$this->FP->check_code($code);
				$data = ['code' => $code];
				$this->template->render('auth/forgotten_password_code', $data);
			} catch (Exception $e) {
				show_error($e->getMessage());
			}
		}
	}

	public function forget_password() {
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$email = $this->input->post('email_addr');

			// if the email isn't registered yet
			if (!$this->ion_auth->is_registered_email($email)) {
				$this->session->set_flashdata('auth_message', '此信箱並未註冊，請再想想看！');
				redirect('/auth/forget_password', 'refresh');
			}
			// send email to the email address
			else {
				$user_id = $this->db->from('users')->where('email', $email)->get()->result()[0]->id;
				$this->load->model('Forgotten_password_code_model', 'FP');

				/*
					                    require(APPPATH. 'libraries/PHPMailer-5.1.0/class.phpmailer.php');
					                    $mail = new PHPMailer();                        // 建立新物件

					                    $mail->IsSMTP();                                // 設定使用SMTP方式寄信
					                    $mail->SMTPAuth = true;                         // 設定SMTP需要驗證

					                    $mail->SMTPSecure = "ssl";                      // Gmail的SMTP主機需要使用SSL連線
					                    $mail->Host = "smtp.gmail.com";                 // Gmail的SMTP主機
					                    $mail->Port = 465;                              // Gmail的SMTP主機的port為465
					                    $mail->CharSet = "utf-8";                       // 設定郵件編碼
					                    $mail->Encoding = "base64";
					                    $mail->WordWrap = 50;                           // 每50個字元自動斷行

					                    $mail->Username = "howtomakeaturn@getmore.cc";     // 設定驗證帳號
					                    $mail->Password = "";              // 設定驗證密碼

					                    $mail->From = "howtomakeaturn@getmore.cc";         // 設定寄件者信箱
					                    $mail->FromName = "Getmore二次時尚";                 // 設定寄件者姓名

					                    $mail->Subject = "Getmore重設密碼連結信";                     // 設定郵件標題

					                    $mail->IsHTML(true);                            // 設定郵件內容為HTML

					                    $mail->AddAddress('howtomakeaturn@hotmail.com', 'Getmore member');  // 收件者郵件及名稱
					                    $mail->Body = $this->load->view('auth/email/_forgotten_password_code', ['code' => $code], true);

					                    if($mail->Send()) {                             // 郵件寄出
					                    } else {
					                        echo $mail->ErrorInfo . "<br/>";
					                    }
				*/

				try {
					$code = $this->FP->create_for_user_id($user_id);
					//$html = $this->load->view('email/_forgotten_password_code', ['code' => $code], true);
					$this->template->set_layout('template/email');
					$html = $this->template->render('email/_forgotten_password_code', ['code' => $code], true);

					$this->load->library('notification');
					$this->notification->send('Getmore重設密碼連結信', $html, $email);
				} catch (Mandrill_Error $e) {
					echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
					throw $e;
				} catch (Exception $e) {
					$this->session->set_flashdata('auth_message', $e->getMessage());
					redirect('/auth/forget_password', 'refresh');
				}
				$this->session->set_flashdata('auth_message', '系統已將重設密碼連結寄出，請前往信箱收信。');
				redirect('/auth/forget_password', 'refresh');
			}
		} else {
			$this->template->render('auth/forget_password');
		}
	}

	//redirect if needed, otherwise display the user list
	function index() {
		redirect('/', 'refresh');
		if (!$this->ion_auth->logged_in()) {
			//redirect them to the login page
			redirect('auth/login', 'refresh');
		} elseif (!$this->ion_auth->is_admin()) {
			//redirect them to the home page because they must be an administrator to view this
			redirect($this->config->item('base_url'), 'refresh');
		} else {
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user) {
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}

			$this->load->view('auth/index', $this->data);
		}
	}

	//log the user in
	function login() {
		$this->data['title'] = "Login";

		if ($this->ion_auth->logged_in()) {
			redirect('/', 'refresh');
		}
		//validate form input
		$this->form_validation->set_rules('identity', '電子郵件', 'required');
		$this->form_validation->set_rules('password', '密碼', 'required');

		// redirect url
		$redirect_url = ($this->input->get_post('redirect_url')) ? $this->input->get_post('redirect_url') : urlencode(site_url());

		if ($this->form_validation->run() == true) {
			//check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect(urldecode($redirect_url), 'refresh');
			} else {
				//if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/login?redirect_url=' . $redirect_url, 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		} else {
			//the user is not logging in so display the login page
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['redirect_url'] = $redirect_url;

			//_log($this->data);
			//var_dump(urlencode($this->input->get('redirect_url')));exit(0);
			//$this->template->render('auth/login', $this->data);
			$this->twiggy->template('auth/login')
				->set($this->data)
				->register_function('set_checkbox')
				->register_function('set_value')
				->display();
		}
	}

	function newlogin() {
		$this->twiggy->template('auth/newlogin')
			->display();
	}

	//log the user out
	function logout($redirect = NULL, $message = NULL) {
		//log the user out
		$logout = $this->ion_auth->logout();
		// redirect url
		$redirect_url = (isset($redirect)) ? $redirect : (($this->input->get_post('redirect_url')) ? $this->input->get_post('redirect_url') : urlencode(site_url()));

		// set flasg message
		if (isset($message)) {
			$this->session->set_flashdata('message', $message);
		}
		//redirect them back to the page they came from
		redirect(urldecode($redirect_url), 'refresh');
	}

	//change password
	function change_password() {
		$this->form_validation->set_rules('old', 'Old password', 'required');
		$this->form_validation->set_rules('new', 'New Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', 'Confirm New Password', 'required');

		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();

		if ($this->form_validation->run() == false) {
			//display the form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['old_password'] = array(
				'name' => 'old',
				'id' => 'old',
				'type' => 'password',
			);
			$this->data['new_password'] = array(
				'name' => 'new',
				'id' => 'new',
				'type' => 'password',
			);
			$this->data['new_password_confirm'] = array(
				'name' => 'new_confirm',
				'id' => 'new_confirm',
				'type' => 'password',
			);
			$this->data['user_id'] = array(
				'name' => 'user_id',
				'id' => 'user_id',
				'type' => 'hidden',
				'value' => $user->id,
			);

			//render
			$this->load->view('auth/change_password', $this->data);
		} else {
			$identity = $this->session->userdata($this->config->item('identity', 'ion_auth'));

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change) {
				//if the password was successfully changed
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				$this->logout();
			} else {
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/change_password', 'refresh');
			}
		}
	}

	//forgot password
	function forgot_password() {
		if ($this->ion_auth->logged_in()) {
			redirect('/', 'refresh');
		}

		$this->form_validation->set_rules('email', 'Email Address', 'required');
		if ($this->form_validation->run() == false) {
			//setup the input
			$this->data['email'] = array('name' => 'email',
				'id' => 'email',
				'class' => 'input',
				'style' => 'width:200px',
			);
			//set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->template->render('auth/forgot_password', $this->data);
		} else {
			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($this->input->post('email'));

			if ($forgotten) {
				//if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
			} else {
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("auth/forgot_password", 'refresh');
			}
		}
	}

	//reset password - final step for forgotten password
	public function reset_password($code) {
		$reset = $this->ion_auth->forgotten_password_complete($code);

		if ($reset) {
			//if the reset worked then send them to the login page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("auth/login", 'refresh');
		} else {
			//if the reset didnt work then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	//activate the user
	function activate($id, $code = false) {
		if ($code !== false) {
			$activation = $this->ion_auth->activate($id, $code);
		} else if ($this->ion_auth->is_admin()) {
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation) {
			//redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("auth", 'refresh');
		} else {
			//redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	//deactivate the user
	function deactivate($id = NULL) {
		// no funny business, force to integer
		$id = (int) $id;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('confirm', 'confirmation', 'required');
		$this->form_validation->set_rules('id', 'user ID', 'required|is_natural');

		if ($this->form_validation->run() == FALSE) {
			// insert csrf check
			$this->data['csrf'] = $this->_get_csrf_nonce();
			$this->data['user'] = $this->ion_auth->user($id)->row();

			$this->load->view('auth/deactivate_user', $this->data);
		} else {
			// do we really want to deactivate?
			if ($this->input->post('confirm') == 'yes') {
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id')) {
					show_404();
				}

				// do we have the right userlevel?
				if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
					$this->ion_auth->deactivate($id);
				}
			}

			//redirect them back to the auth page
			redirect('auth', 'refresh');
		}
	}

	//create a new user
	function register() {
		$this->data['title'] = "Create User";

		if ($this->ion_auth->logged_in()) {
			redirect('/', 'refresh');
		}

		// redirect url
		// $redirect_url = ($this->input->get_post('redirect_url')) ? $this->input->get_post('redirect_url') : urlencode(site_url());
		$redirect_url = ($this->input->get_post('redirect_url')) ? $this->input->get_post('redirect_url') : '/';
		// $redirect_url = '/';

		//validate form input
		$this->form_validation->set_rules('user_name', '姓名', 'required');
		$this->form_validation->set_rules('email', '電子郵件', 'required|valid_email');
		$this->form_validation->set_rules('password', '密碼', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', '確認密碼', 'required');
		$this->form_validation->set_rules('aggree', '服務條款', 'required');

		if ($this->form_validation->run() == true) {
			$username = '';
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			$additional_data = array(
				'user_name' => $this->input->post('user_name'),
			);
		}

		if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data) !== FALSE) {
			//check to see if we are creating the user
			//redirect them back to the admin page
			$this->ion_auth->notify_the_user_is_registered($email);

			$user = $this->db->select('id')
				->from('users')
				->where('email', $email)
				->get()->row_array();
			$id = $user['id'];
			$this->load->model('Closet_model');
			$this->Closet_model->create_if_not_exit($id);

			$this->load->model('Account_model');
			$this->Account_model->create_if_not_exist($id);

			redirect($redirect_url, 'refresh');
		} else {
			//display the create user form
			//set the flash data error message if there is one
			$this->data['reg_message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->data['redirect_url'] = $redirect_url;
			$this->twiggy->set($this->data)->template('auth/login')->display();
		}
	}

	//create a new user
	public function edit() {
		if (!$this->ion_auth->logged_in()) {
			$this->system->redirect();
		}

		$id = $this->session->userdata('user_id');

		// user meta data
		$this->data['profile'] = $this->ion_auth->user($id)->row();

		$this->form_validation->set_rules('user_name', '姓名', 'trim|xss_clean');
		$this->form_validation->set_rules('email', 'E-mail', 'trim|xss_clean');
		$this->form_validation->set_rules('mobile_phone', '聯絡手機', 'trim|xss_clean');
		$this->form_validation->set_rules('address', '聯絡地址', 'trim|xss_clean');

		if ($this->form_validation->run() == true) {
			$user_birthday = $this->input->post('year') . '-' . $this->input->post('month') . '-' . $this->input->post('day');

			$p_address = array(
				'county' => $this->input->post('county'),
				'district' => $this->input->post('district'),
				'zipcode' => $this->input->post('zipcode'),
				'address' => $this->input->post('address'),
			);
			$update_data = array(
				'id' => $id,
				'user_name' => $this->input->post('user_name') ? $this->input->post('user_name') : NULL,
				'sex' => $this->input->post('sex'),
				'birthday' => $user_birthday,
				'email' => $this->input->post('email') ? $this->input->post('email') : NULL,
				'p_address' => json_encode($p_address),
				'mobile_phone' => $this->input->post('mobile_phone') ? $this->input->post('mobile_phone') : NULL,
			);

			if (!$this->input->post('old_password') and $this->input->post('new_password') != '' and $this->input->post('password_confirm') != '') {
				if ($this->input->post('new_password') == $this->input->post('password_confirm')) {
					$update_data['password'] = $this->input->post('new_password');
				}
			}
		}

		if ($this->form_validation->run() == true && $this->ion_auth->update_user($id, $update_data)) {

			$identity = $this->session->userdata($this->config->item('identity', 'ion_auth'));
			if ($this->input->post('old_password') != '' and $this->input->post('new_password') != '' and $this->input->post('password_confirm') != '') {
				if ($this->input->post('new_password') == $this->input->post('password_confirm')) {
					$change = $this->ion_auth->change_password($identity, $this->input->post('old_password'), $this->input->post('new_password'));

					if ($change) {
						//if the password was successfully changed
						$this->session->set_flashdata('message', $this->lang->line('password_change_successful'));
						$this->logout('auth/login', $this->ion_auth->messages());
					}
				}
			}

			$this->session->set_flashdata('message', $this->lang->line('update_successful'));
			redirect("auth/edit", 'refresh');
		} else {
			//display the create user form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['email'] = array(
				'name' => 'email',
				'id' => 'email',
				'type' => 'text',
				'value' => (isset($_POST['email'])) ? $this->form_validation->set_value('email') : $this->data['profile']->email,
				'class' => 'input',
				'style' => 'width:200px',
			);

			$this->data['user_name'] = array(
				'name' => 'user_name',
				'id' => 'user_name',
				'type' => 'text',
				'value' => (isset($_POST['user_name'])) ? $this->form_validation->set_value('user_name') : $this->data['profile']->user_name,
				'class' => 'input',
				'style' => 'width:200px',
			);

			$this->data['user_sex_m'] = array(
				'name' => 'user_sex',
				'value' => '1',
				'checked' => (isset($_POST['sex'])) ? $this->form_validation->set_radio('sex', '1', FALSE) : ($this->data['profile']->sex == '1') ? TRUE : FALSE,
			);
			$this->data['user_sex_f'] = array(
				'name' => 'sex',
				'value' => '0',
				'checked' => (isset($_POST['sex'])) ? $this->form_validation->set_radio('sex', '0', TRUE) : ($this->data['profile']->sex == '0') ? TRUE : FALSE,
			);
			$this->data['p_address'] = array(
				'name' => 'p_address',
				'id' => 'p_address',
				'type' => 'text',
				'value' => (isset($_POST['p_address'])) ? $this->form_validation->set_value('p_address') : $this->data['profile']->p_address,
				'class' => 'input',
				'style' => 'width:200px',
			);
			$this->data['new_password'] = array('name' => 'new_password',
				'id' => 'new_password',
				'type' => 'password',
				'value' => '',
				'class' => 'input',
				'style' => 'width:200px',
			);
			$this->data['password_confirm'] = array('name' => 'password_confirm',
				'id' => 'password_confirm',
				'type' => 'password',
				'value' => '',
				'class' => 'input',
				'style' => 'width:200px',
			);
			$user_birthday = (isset($_POST['year'])) ? $this->input->post('year') . '-' . $this->input->post('month') . '-' . $this->input->post('day') : $this->data['profile']->birthday;
			$this->data['user_birthday'] = $this->_get_birthday_input($user_birthday);
			$this->data['profile']->p_address = (!empty($this->data['profile']->p_address)) ? json_decode($this->data['profile']->p_address, true) : $this->default_address;
			$this->data['profile']->s_address = (!empty($this->data['profile']->s_address)) ? json_decode($this->data['profile']->s_address, true) : $this->default_address;
		}

		$this->template->render('auth/edit', $this->data);
	}

	private function _get_birthday_input($data = NULL) {
		if (isset($data)) {
			$data = explode("-", $data);
		} else {
			$data = array();
		}

		$year = (isset($data[0])) ? $data[0] : "0";
		$month = (isset($data[1])) ? $data[1] : "0";
		$day = (isset($data[2])) ? $data[2] : "0";

		// year

		$options = array("0" => "請選擇");
		for ($i = (date("Y") - 1); $i >= 1911; $i--) {
			$options[$i] = $i;
		}
		$year = form_dropdown('year', $options, $year);

		// month

		$options = array("0" => "請選擇");
		for ($i = 1; $i <= 12; $i++) {
			$options[$i] = $i;
		}
		$month = form_dropdown('month', $options, $month);

		// day

		$options = array("0" => "請選擇");
		for ($i = 1; $i <= 31; $i++) {
			$options[$i] = $i;
		}
		$day = form_dropdown('day', $options, $day);

		return $year . "&nbsp;年&nbsp;" . $month . "&nbsp;月&nbsp;" . $day . "&nbsp;日&nbsp;";
	}

	function _get_csrf_nonce() {
		$this->load->helper('string');
		$key = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	function _valid_csrf_nonce() {
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * facebook login
	 *
	 * @return void
	 * @author appleboy
	 **/
	public function facebook() {
		// redirect url
		if ($this->ion_auth->logged_in()) {
			if ($this->session->userdata('redirect_url')) {
				$redirect_url = urldecode($this->session->userdata('redirect_url'));

				// remove session data
				$this->session->unset_userdata('redirect_url');
				redirect($redirect_url);
			} else {
				redirect(site_url());
			}
		}

		$this->session->set_userdata('login_type', 'facebook');
		$redirect_url = ($this->input->get_post('redirect_url')) ? $this->input->get_post('redirect_url') : urlencode($this->input->server('HTTP_REFERER'));

		if ($redirect_url != '') {
			$this->session->set_userdata('redirect_url', $redirect_url);
		}

		redirect($this->user_fb_data['loginUrl']);
	}

}
