<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Buyer extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library(array('ion_auth', 'session', 'user_agent'));
		$this->load->helper('url');
		if (!$this->ion_auth->logged_in()) {
			$this->system->redirect();
		}
		$this->load->spark('curl');
		$this->config->load('trade_rule');
		$this->load->model(array('Closet_model', 'Free_style', 'Product_clothing_model', 'Marketplace_model', 'Free_style', 'Buyer_model', 'User_model', 'Orderlist_model', 'Trade_rule_model'));

		$this->load->database();

		$this->load->spark('Twiggy/0.8.5');
		$this->twiggy->theme('');

		$this->twiggy->set('page_url', $this->uri->segment(2));
		$this->twiggy->set(array(
			'lang' => 'zh-tw',
			'meta_charset' => 'UTF-8',
			'site_title' => 'Getmore 專注在「時尚」社群的購物網站 - 原創新銳設計/時尚配件/時尚買手/二手精品/二手包/品牌二手代售',
			'site_description' => 'Getmore 是專注在「時尚」社群的購物網站。聚集新銳設計師、買手、時尚愛好者的購物網站，供應獨特單品以及二手時尚的時尚購物平台。時尚很多元，就像是一個個精彩的小圈圈，Getmore 負責把時尚圈在一起，你也可以稱這裡是「時尚小圈圈」。',
			'site_keywords' => '時尚,衣著,服飾,時裝,二手衣,流行,新銳設計師,亞洲時尚,褲子,買二手衣,賣二手衣,環保,電子商務,startup,二次時尚',
			'logined' => $this->ion_auth->logged_in(),
		), NULL, TRUE);
	}
	/*
		  upload a product for sell
	*/
	public function manage($type = 'index') {
		if (!$this->ion_auth->is_buyer()) {
			$this->system->redirect();
		}

		switch ($this->session->userdata('isBuyerVerify')) {
		case 0:
			return $this->twiggy->template('buyer/auth')->set([
				'mobile_phone' => $this->session->userdata('mobile_phone'),
				'address_info' => json_decode($this->session->userdata('s_address')),
				'email' => $this->session->userdata('email'),
				'trade' => $this->config->item('Buyer'),
			])->display();
		case -1:
			$this->send_sms_code($mobile_phone);
			return $this->_verify_mobile_phone($this->session->userdata('mobile_phone'));
		}

		$query = $this->Free_style->get_free_style();

		$free_styles = [];
		foreach ($query as $style) {
			$free_styles[] = $style->name;
		}

		$this->twiggy->set('styles', $free_styles);

		$buyer = (array) json_decode($this->curl->simple_get('/restful/buyer/id/' . $this->session->userdata('user_id'), array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));

		$trade_rule = (array) json_decode($this->curl->simple_get('/restful/trade', array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));

		if ($type != 'index') {
			switch ($type) {
			case 'clothings':
				$group = 'Clothing';
				break;
			case 'shoes':
				$group = 'Shoes';
				break;
			case 'bags':
				$group = 'Bags';
				break;
			case 'accessories':
				$group = 'Accessories';
				break;
			}
			$brandslist = $this->curl->simple_get('/restful/brands', ['group' => $group]);
			$this->twiggy->set('brandslist', $brandslist);
		}

		$this->load->config('clothing_class');
		$this->twiggy->set('trade_rule', $trade_rule);
		$this->twiggy->set('buyer', $buyer);
		$this->twiggy->set('style_lists', $this->config->item('style_lists'));
		$this->twiggy->template('buyer/launch/' . $type)->display();
	}

	public function launch_product() {
		$default = array("added_user_id" => $this->session->userdata('user_id'), "product_id" => 0, "product_original_price" => '', "product_sell_price" => '', "suggested_price" => 0, "reduction_again" => 0, "product_title" => '', "product_brand" => '', "product_type" => '', "product_comments" => '', "color" => '', "season" => '', "style" => '', "pattern" => '', "item_long" => 'ss', "files" => '', "size" => '', "item_status" => 0, "material" => '', "location" => '', "measure" => '{"1":"","2":"","3":"","4":"","5":"","6":"","7":"","8":"","9":"","10":""}', "descript" => '', "branded" => 1, "add_time" => time(), "edit_time" => time(), "buy_order_id" => 0, "lock_status" => 0, "last_locked_time" => '0000-00-00 00:00:00', "log" => '', "publication_time" => date('Y-m-d H:i:s'), "on_shelf" => 1, "size_comment" => '', "size_type_major" => '', "size_standard" => '', "size_type_minor" => '', "size_number_minor" => '', "admin_comment" => '', "admin_remark" => '', "level" => '', "launch_way" => 4, "user_id" => $this->session->userdata('user_id'), "sub_pattern_1" => "", "sub_pattern_2" => "", "sub_pattern_3" => "");
		if ($this->input->get('group') != 'Clothing') {
			$default['measure'] = '{}';
		}

		$post = $this->input->post();

		$post['free_style'] = json_decode($this->Free_style->post_free_style($post['free_style']));
		$free_style_ids = array_map($post['free_style'], function ($free_style) {
			return $free_style->id;
		}, $post['free_style']);
		$post['free_style'] = implode(",", $free_style_ids);

		if ($post['style']) {
			$post['style'] = json_encode($post['style']);
		}

		$post['files'] = json_encode($post['files']);

		unset($post['category2']);

		$new_boutique = array_merge($default, $post, $this->input->get());

		$this->Buyer_model->add_product($new_boutique);

		redirect("/buyer/manage" . "#success", "refresh");
	}

	public function orderlist($status) {

		if (!$this->ion_auth->logged_in()) {
			$this->system->redirect();
		}

		if (!$this->ion_auth->is_buyer()) {
			$this->system->redirect();
		}

		$query = $this->Orderlist_model->sub_order($status);

		$this->twiggy->set('sub_order_lists', $query);
		$orderlist_url = $this->uri->segment(3);

		$this->twiggy->set('orderlist_url', $orderlist_url);
		$this->twiggy->template('buyer/orderlist')->display();
	}

	public function sell_clothing_management() {

		if (!$this->ion_auth->logged_in()) {
			$this->system->redirect();
		}

		$clothings = json_decode($this->curl->simple_get('/restful/buyer_products', array(
			'user_id' => $this->session->userdata('user_id'),
		), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));

		$query = $this->Free_style->get_free_style();
		$free_styles = [];
		foreach ($query as $style) {
			$free_styles[$style->id] = $style->name;
		}

		$query2 = $this->Product_clothing_model->get_brand_list();
		$data = array_merge($query2[1], $query2[2], $query2[3], $query2[4], $query2[5], $query2[6], $query2[7], $query2[8]);

		$this->load->config('clothing_class');
		$this->twiggy->set('free_styles', $free_styles);
		$this->twiggy->set('brands', $data);
		$this->twiggy->set('sell_list', $clothings);
		$this->twiggy->set('style_lists', $this->config->item('style_lists'));
		$this->twiggy->template('buyer/manageinfo')->display();
	}

	private function _verify_mobile_phone($mobile_phone, $msg = null) {
		$this->twiggy->template('verify/sms_code')->set(array('msg' => $msg, 'mobile_phone' => $mobile_phone))->display();
	}

	public function account_activated() {
		if ($this->input->post()) {
			$this->load->model('Bank_model');
			$this->load->model('Invoice_model');
			$buyer = $this->input->post('buyer');

			foreach (['signboard' => $buyer['signboard'], 'logo' => $buyer['logo'], 'profile' => $buyer['profile']] as $key => $data) {
				$this->curl->simple_post(base_url('/restful/buyer/' . $key . '/file'), [
					'file' => $data,
				], [
					CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
				]);
			}
			unset($buyer['signboard']);
			unset($buyer['logo']);
			unset($buyer['profile']);

			$files = array();
			foreach ($_FILES as $type => $file) {
				foreach ($file['tmp_name'] as $key => $tmp_file) {
					if (empty($tmp_file)) {
						continue;
					}
					if (empty($files[$type])) {
						$files[$type] = array();
					}
					$files[$type][$key] = new CURLFile($tmp_file);
				}
			}

			if (!$this->Buyer_model->post_buyer($buyer)) {
				redirect($this->agent->referrer(), 'refresh');
			}

			call_user_func(function ($user) {
				if (!$this->User_model->user_update($user)) {
					redirect($this->agent->referrer(), 'refresh');
				}
			}, $this->input->post('user'));

			call_user_func_array(function ($bank, $file) {
				list($code, $title) = explode('-', $bank['code']);
				$bank['code'] = $code;
				$bank['title'] = $title;
				if (!$this->Bank_model->bank_activate(array_merge($bank, $file))) {
					redirect($this->agent->referrer(), 'refresh');
				}
			}, array($this->input->post('bank'), $files['bank']));

			if (!$this->Invoice_model->invoice_activate($this->input->post('invoice'))) {
				redirect($this->agent->referrer(), 'refresh');
			}

			$this->Trade_rule_model->create_default($this->input->post('trade'));

			$this->User_model->update(array('verify_buyer' => -1));
			$this->session->set_userdata('isBuyerVerify', -1);

			$this->_verify_mobile_phone($this->input->post('user')['mobile_phone']);
		} else {
			switch ($this->session->userdata('isBuyerVerify')) {
			case 0:
				return $this->twiggy->template('buyer/auth')->set([
					'mobile_phone' => $this->session->userdata('mobile_phone'),
					'address_info' => json_decode($this->session->userdata('s_address')),
					'email' => $this->session->userdata('email'),
					'trade' => $this->config->item('Buyer'),
				])->display();
			case -1:
				return $this->_verify_mobile_phone($this->session->userdata('mobile_phone'));
			}
		}
	}

	public function send_sms_code($mobile_phone = null) {
		if (!$mobile_phone) {
			$mobile_phone = $this->input->post('mobile_phone') ? $this->input->post('mobile_phone') : $this->session->userdata("mobile_phone");
		}

		if ((time() - $this->session->userdata("sms_timestamp")) >= 120) {
			$sms_code = $this->User_model->generate_sms_code();
			$this->session->set_userdata("mobile_phone", $mobile_phone);
			$this->session->set_userdata("sms_code", $sms_code);
			$this->session->set_userdata("sms_timestamp", time());
			$this->system->send_sms_msg($mobile_phone, "您的Getmore驗證碼是:" . $sms_code);
			if ($this->input->is_ajax_request()) {
				echo '已發送認證碼';
			} else {
				return true;
			}
		} else {
			if ($this->input->is_ajax_request()) {
				echo '離上次發送驗證碼需間隔兩分鐘';
			} else {
				return false;
			}
		}
	}

	public function closet() {
		$buyerData = file_get_contents("http://" . $_SERVER['HTTP_HOST'] . "/restful/buyer/id/" . $this->session->userdata("user_id"));

		if (!empty($buyerData)) {
			$buyerData = json_decode($buyerData);
			redirect("/shop/buyer/" . $buyerData->url_code);
		} else {
			redirect("/");
		}

	}

	public function check_sms_code() {
		$sms_code = $this->input->post('sms_code');
		$msg = '';
		if (($this->session->userdata("sms_code") === $sms_code) && ((time() - $this->session->userdata("sms_timestamp")) < 120)) {
			$this->User_model->update(array('verify_buyer' => 1));
			$this->session->set_userdata('isBuyerVerify', 1);
			redirect('/buyer/manage', 'refresh');
		} else if ($this->session->userdata("sms_code") !== $sms_code) {
			if ((time() - $this->session->userdata("sms_timestamp")) >= 120) {
				$msg .= '上次發送的驗證碼已逾時, 請按重新取得認證碼';
			} else {
				$msg = '驗證碼錯誤, 請重新輸入';
			}
		} else {
			$msg = '此驗證碼已經逾時, 請按重新取得認證碼';
		}

		$this->_verify_mobile_phone($this->session->userdata('mobile_phone'), $msg);
	}

	public function buyer_settings() {
		switch ($this->session->userdata('isBuyerVerify')) {
		case 0:
			return $this->twiggy->template('buyer/account_activated')->display();
		case -1:
			return $this->_verify_mobile_phone($this->session->userdata('mobile_phone'));
		}

		if (($buyer = $this->input->post('buyer')) && ($user = $this->input->post('user'))) {
			foreach (['signboard' => $buyer['signboard'], 'logo' => $buyer['logo'], 'profile' => $buyer['profile']] as $key => $data) {
				$this->curl->simple_post(base_url('/restful/buyer/' . $key . '/file'), [
					'file' => $data,
				], [
					CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
				]);
			}
			unset($buyer['signboard']);
			unset($buyer['logo']);
			unset($buyer['profile']);
			$this->Buyer_model->post_buyer($buyer);

			$this->User_model->user_update($user);

			return redirect('/buyer/buyer_settings', 'refresh');
		}

		$buyer = $this->Buyer_model->get_buyer_settings($this->session->userdata('id'));
		$user = $this->User_model->get_user();

		$this->twiggy->template('buyer/settings')->set(array(
			'buyer' => $buyer,
			'user' => $user,
			'page_url' => 'closet_settings',
		))->display();
	}

	public function bank_info() {
		$this->load->model('Bank_model');
		$bank = $this->Bank_model->get_bank_info();
		$this->twiggy->template('buyer/bank_info')->set(array(
			'bank' => $bank,
			'page_url' => 'bank_info',
		))->display();
	}

	public function invoice_info() {
		$this->load->model('Invoice_model');
		if ($data = $this->input->post()) {
			$this->Invoice_model->invoice_update($data);
			$invoice = $data;
		} else {
			$invoice = $this->Invoice_model->get_invoice_info();
		}

		$this->twiggy->template('buyer/invoice_info')->set(array(
			'invoice' => $invoice,
			'page_url' => 'invoice_info',
		))->display();
	}

	public function trade_rule() {

		switch ($this->session->userdata('isBuyerVerify')) {
		case 0:
			return $this->twiggy->template('buyer/account_activated')->display();
		case -1:
			return $this->_verify_mobile_phone($this->session->userdata('mobile_phone'));
		}

		$data['page_url'] = 'trade_rule';

		if ($post = $this->input->post()) {
			$trade_rule = (array) json_decode($this->curl->simple_post('/restful/trade', $post['trade'], array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));

			if (!$post['buyer']['shipping_price']) {
				$post['buyer']['shipping_price'] = 0;
			}

			if (!$post['buyer']['free_shipping']) {
				$post['buyer']['free_shipping'] = 0;
			}

			$buyer = (array) json_decode($this->curl->simple_post('/restful/buyer', $post['buyer'], array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));

			$data['trade'] = $post['trade'];
			$data['buyer'] = $post['buyer'];
		} else {
			$data['trade'] = (array) json_decode($this->curl->simple_get('/restful/trade', array(), array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));

			$data['buyer'] = (array) json_decode($this->curl->simple_get('/restful/buyer/id/' . $this->session->userdata('user_id'), [], array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));
		}

		if ($post['url']) {
			//echo '/buyer//'.$post['url'];
			redirect('/shop/buyer/' . $post['url'], 'refresh');
		} else {
			$this->twiggy->template('buyer/trade_rule')->set($data)->display();
		}

	}
}
