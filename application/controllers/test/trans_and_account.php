<?php

class Trans_and_account extends Test_Controller {
      /*
     * Unit Test for Transaction and Account Model
     */

    function index() {
        $this->load->model('Transaction_model', 'transaction');
        $this->load->model('Account_model', 'account');

        $this->config->set_item('language', 'english');
        
        $this->throw_exception_if_deduction_amount_is_zero_test();
        $this->throw_exception_if_user_id_not_exist_in_account_test();
        $this->simple_adding_test();
        $this->simple_deduction_test();
        $this->no_negative_balance_account_test();
        
        $this->wrong_trans_amount_insert_test1();
        $this->wrong_trans_amount_insert_test2();
        $this->wrong_trans_amount_insert_test3();
        
        $this->data_sync_test();
        $this->wrong_account_insert_test();
        
        echo $this->unit->report();
    }

    
    function throw_exception_if_deduction_amount_is_zero_test() {
        $found_exception = false;
        try{
            $this->transaction->insert_to_user('89', 'Testing trans', 0);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'Throw Exception If Deduction Amount Is Zero (no need to insert one tuple to database)' );
    }
    
    function throw_exception_if_user_id_not_exist_in_account_test(){
        $found_exception = false;
        try{
            $user_id = '-99';
            $this->account->get_by_user_id($user_id);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'Throw Exception If Account Not Exist' );
    }
    
    function simple_adding_test() {
        $account_before = $this->account->get('89');
        $this->transaction->insert_to_user('89', 'Testing trans', 'mocking order number', 150, 'created from unit test');
        $account_after = $this->account->get('89');
        $this->unit->run( $account_after->balance, $account_before->balance + 150, 'Simple Adding test' );
    }

    function simple_deduction_test() {
        $account_before = $this->account->get('89');
        $this->transaction->insert_to_user('89', 'Testing trans', 'mocking order number', -100, 'created from unit test');
        $account_after = $this->account->get('89');
        $this->unit->run( $account_after->balance, $account_before->balance - 100, 'Simple Deduction test' );
    }
    
    function data_sync_test() {
        $account = $this->account->with('transactions')->get('89');
        $balance_from_trans = 0;
        foreach($account->transactions as $transaction){
            $balance_from_trans += $transaction->amount;
        }
        $this->unit->run( $account->balance, $balance_from_trans, 'Data Consistency test' );        
    }

    function wrong_account_insert_test() {
        $found_exception = false;
        try{
            $this->transaction->insert_to_user('-99', 'Testing trans', 150);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'Wrong Account Insert test( no transaction should be created.)' );
    }
    
    function wrong_trans_amount_insert_test1() {
        $found_exception = false;
        try{
            $this->transaction->insert_to_user('89', 'Testing trans', 10.5);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'Wrong Amount test( float variable)' );
    }

    function wrong_trans_amount_insert_test2() {
        $found_exception = false;
        try{
            $this->transaction->insert_to_user('89', 'Testing trans', '100');
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'Wrong Amount test( string variable)' );
    }
    
    function wrong_trans_amount_insert_test3() {
        $found_exception = false;
        try{
            $this->transaction->insert_to_user('89', 'Testing trans', true);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'Wrong Amount test( boolean variable)' );
    }
    
    function no_negative_balance_account_test() {
        $found_exception = false;
        try{
            $this->transaction->insert_to_user('89', 'Testing trans', -100000);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'No Negative Balance test( insert -100000)' );
    }    
    
    
    
}


/* End of file Test_trans_and_account_Controller.php */
/* Location: ./application/controller/Test_trans_and_account_controller.php */
