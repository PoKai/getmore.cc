<?php

class Sellorder extends Test_Controller {

    function index(){
        $this->config->set_item('language', 'english');
        
        $this->test_sellorder_with_deduction(0);
        $this->test_sellorder_with_deduction(-10);
        $this->test_sellorder_with_deduction("-10");
        $this->test_sellorder_with_deduction(-1000);

        echo $this->unit->report();          
         
    }
    
    private function test_sellorder_with_deduction($deduction_amount){
        $this->load->model('Sellorder_model', 'sellorder');
        $num_of_orders_before_creation = $this->db->count_all('sellorders');
        $formdata = $this->mocking_data_with_deduction($deduction_amount);
        $this->sellorder->add_order($formdata);
        $this->unit->run( $this->db->count_all('sellorders'), 
            $num_of_orders_before_creation + 1 , 
            "test_sellorder_with_deduction: " . $deduction_amount
         );
    }
    
    private function mocking_data_with_deduction($deduction_amount){
        $formdata = array(
            "user_id"                   => '89',
            "user_name"                 => 'mocking name',
            "user_sex"                  => '',
            "order_number"              => $this->generate_order_number(),
            "total_price"               => '1000',
            "date_purchased"            => date("Y-m-d H:i:s"),
            "status_id"                 => '5',
            "no_return"                 => '',
            'deduction_amount' => $deduction_amount
        );
        return $formdata;
    }
}
