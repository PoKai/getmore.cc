<?php

class Withdraw extends Test_Controller {
  
    function index(){
        $this->load->model('Withdraw_model', 'withdraw');

        $this->config->set_item('language', 'english');
      
        $this->withdraw_create_record();
        $this->throw_exception_if_invalid_amount();
        $this->throw_exception_if_invalid_user_id();
        $this->throw_exception_if_empty_bank_name();
        $this->throw_exception_if_empty_branch_name();
        $this->throw_exception_if_empty_bank_account();
       
       
        echo $this->unit->report();      
    }


    function throw_exception_if_invalid_amount(){
        $found_exception = false;
        try{
            $user_id = '89';
            $amount = 999999;
            $bank_name = 'pseudo_bank_name';
            $branch_name = 'pseudo_branch_name';
            $bank_account = 'pseudo_bank_account';
            
            $this->withdraw->create_record($user_id, $amount, $bank_name, $branch_name, $bank_account);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'throw_exception_if_invalid_amount' );
    }    
    function withdraw_create_record(){
        $result = false;
        $user_id = '89';
        $amount = 100;
        $bank_name = 'pseudo_bank_name';
        $branch_name = 'pseudo_branch_name';
        $bank_account = 'pseudo_bank_account';
        
        $result = $this->withdraw->create_record($user_id, $amount, $bank_name, $branch_name, $bank_account);
        $this->unit->run( $result, true, 'withdraw_create_record' );
    }

    function throw_exception_if_invalid_user_id(){
        $found_exception = false;
        try{
            $user_id = '9999';
            $amount = 100;
            $bank_name = 'pseudo_bank_name';
            $branch_name = 'pseudo_branch_name';
            $bank_account = 'pseudo_bank_account';
            
            $this->withdraw->create_record($user_id, $amount, $bank_name, $branch_name, $bank_account);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'throw_exception_if_invalid_user_id' );
    }
    
    function throw_exception_if_empty_bank_name(){
        $found_exception = false;
        try{
            $user_id = '89';
            $amount = 100;
            $bank_name = '';
            $branch_name = 'pseudo_branch_name';
            $bank_account = 'pseudo_bank_account';
            
            $this->withdraw->create_record($user_id, $amount, $bank_name, $branch_name, $bank_account);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'throw_exception_if_empty_bank_name' );
    }
    
    function throw_exception_if_empty_branch_name(){
        $found_exception = false;
        try{
            $user_id = '89';
            $amount = 100;
            $bank_name = 'pseudo_bank_name';
            $branch_name = '';
            $bank_account = 'pseudo_bank_account';
            
            $this->withdraw->create_record($user_id, $amount, $bank_name, $branch_name, $bank_account);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'throw_exception_if_empty_branch_name' );
    }  
    
    function throw_exception_if_empty_bank_account(){
        $found_exception = false;
        try{
            $user_id = '89';
            $amount = 100;
            $bank_name = 'pseudo_bank_name';
            $branch_name = 'pseudo_branch_name';
            $bank_account = '';
            
            $this->withdraw->create_record($user_id, $amount, $bank_name, $branch_name, $bank_account);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'throw_exception_if_empty_bank_account' );
    }      
    

}
