<?php

class Clothing extends Test_Controller {

    function index(){
        $this->load->model('Product_clothing_model', 'clothing');
        $this->config->set_item('language', 'english');
        
        $this->update_clothing_price();
        $this->throw_exception_if_id_is_invalid();
        $this->throw_exception_if_price_is_invalid();
        $this->throw_exception_if_user_id_is_invalid();
        
        $this->get_valid_user_id_with_ten_clothing();
        $this->throw_exception_if_product_item_number_not_exist();

        echo $this->unit->report();        
    }

        function update_clothing_price(){
        $result = $this->clothing->update_price(1, 2355, 203);
        
        $this->unit->run( $result, true, 'update_clothing_price' );
    }

    function throw_exception_if_id_is_invalid(){
        $found_exception = false;
        try{
        $result = $this->clothing->update_price(9999, 1111, 89);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'throw_exception_if_id_is_invalid' );
    }
    function throw_exception_if_price_is_invalid(){
        $found_exception = false;
        try{
        $result = $this->clothing->update_price(46, -100, 89);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'throw_exception_if_price_is_invalid' );
    }
    function throw_exception_if_user_id_is_invalid(){
        $found_exception = false;
        try{
        $result = $this->clothing->update_price(46, 2222, 2);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'throw_exception_if_user_id_is_invalid' );
    }

    
    function get_valid_user_id_with_ten_clothing(){
        $this->load->library('ion_auth');
        $clothings = $this->clothing->get_all();
        $counter = 0;
        $found_owner_so_far = true;
        foreach($clothings as $clothing){
            $user_id = $this->clothing->get_user_id_by_product_item_number( $clothing['product_item_number'] );
        }
        $this->unit->run( $found_owner_so_far, true, 'get_valid_user_id_with_ten_clothing' );
    }
    
    
    
    function throw_exception_if_product_item_number_not_exist(){
        $found_exception = false;
        try{
            $this->clothing->get_user_id_by_product_item_number('SS55678');
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'throw_exception_if_product_item_number_not_exist' );
    }

    
}
