<?php

class Forget_password extends Test_Controller {

    function index(){
        $this->load->model('Forgotten_password_code_model', 'FP');
        $this->config->set_item('language', 'english');
        
        $this->empty_user_id();
        $this->invalid_user_id();
        $this->create_for_current_user();
        $this->handle_with_done_code();
        $this->handle_with_nonsense_code();
        $this->handle_with_password_containing_space();
        $this->handle_with_too_short_password();
        $this->handle_with_different_password();
        $this->handle();
        
        echo $this->unit->report();        
    }

    function empty_user_id(){
        $found_exception = false;
        try{
            $result = $this->FP->create_for_user_id('');
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'throw_exception_if_empty_user_id' );
    }

    function invalid_user_id(){
        $found_exception = false;
        try{
            $result = $this->FP->create_for_user_id(-1);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'throw_exception_if_invalid_user_id' );
    }

    function create_for_current_user(){
        $found_exception = false;
        try{
            $user_id = $this->session->userdata('user_id');
            $result = $this->FP->create_for_user_id($user_id);
        }
        catch (Exception $e){
            $found_exception = true;          
            throw $e;
        }
        $this->unit->run( $found_exception, false, 'no exception while create_for_current_user' );
    }
    
    function handle_with_done_code(){
        $found_exception = false;
        try{          
            $user_id = $this->session->userdata('user_id');

            // resume the password after the test
            $password_backup = $this->db->select('password')
                                                              ->from('users')
                                                              ->where('id', $user_id)->get()->result()[0]->password;

            $forgotten_password_code = $this->FP->get_by(array('user_id'=>$user_id, 'done'=>true));
            $this->FP->handle($forgotten_password_code->code, 'mockpassword', 'mockpassword');

            // resume the password after the test
            $this->db->update('users', array('password' => $password_backup), array('id' => $user_id));
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'throw exception while handle_with_done_code' );      
    }    

    function handle_with_nonsense_code(){
        $found_exception = false;
        try{
            $this->FP->handle('this_is_nonsense_code', 'mockpassword', 'mockpassword');
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'throw exception while handle_with_nonsense_code' );      
    }
    
    function handle_with_password_containing_space(){
        $found_exception = false;
        try{
            $user_id = $this->session->userdata('user_id');

            // resume the password after the test
            $password_backup = $this->db->select('password')
                                                              ->from('users')
                                                              ->where('id', $user_id)->get()->result()[0]->password;

            $forgotten_password_code = $this->FP->get_by(array('user_id'=>$user_id, 'done'=>false));          
            $this->FP->handle($forgotten_password_code->code, 'mockpassword ', 'mockpassword ');

            // resume the password after the test
            $this->db->update('users', array('password' => $password_backup), array('id' => $user_id));
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'throw exception while handle_with_password_containing_space' );      
    }    

    function handle_with_too_short_password(){
        $found_exception = false;
        try{
            $user_id = $this->session->userdata('user_id');

            // resume the password after the test
            $password_backup = $this->db->select('password')
                                                              ->from('users')
                                                              ->where('id', $user_id)->get()->result()[0]->password;

            $forgotten_password_code = $this->FP->get_by(array('user_id'=>$user_id, 'done'=>false));          
            $this->FP->handle($forgotten_password_code->code, 'pw', 'pw');

            // resume the password after the test
            $this->db->update('users', array('password' => $password_backup), array('id' => $user_id));
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'throw exception while handle_with_too_short_password' );      
    }    

    
    function handle_with_different_password(){
        $found_exception = false;
        try{
            $user_id = $this->session->userdata('user_id');

            // resume the password after the test
            $password_backup = $this->db->select('password')
                                                              ->from('users')
                                                              ->where('id', $user_id)->get()->result()[0]->password;

            $forgotten_password_code = $this->FP->get_by(array('user_id'=>$user_id, 'done'=>false));          
            $this->FP->handle($forgotten_password_code->code, 'mockpassword', 'notsamepassword');

            // resume the password after the test
            $this->db->update('users', array('password' => $password_backup), array('id' => $user_id));
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'throw exception while handle_with_different_password' );      
    }    
    
    function handle(){
        $found_exception = false;
        try{          
            $user_id = $this->session->userdata('user_id');

            // resume the password after the test
            $password_backup = $this->db->select('password')
                                                              ->from('users')
                                                              ->where('id', $user_id)->get()->result()[0]->password;

            $forgotten_password_code = $this->FP->get_by(array('user_id'=>$user_id, 'done'=>false));          
            $this->FP->handle($forgotten_password_code->code, 'mockpassword', 'mockpassword');

            // resume the password after the test
            $this->db->update('users', array('password' => $password_backup), array('id' => $user_id));
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, false, 'no exception while handle' );      
    }
    
}
