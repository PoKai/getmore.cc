<?php

/* 
 * unit testing controller
 * @author howtomakeaturn
 */
class Base extends Test_Controller {

    function index()
    {        
        echo "<h2>General Test</h2>";
        echo "<h3><a href='/test/base/emails_for_marketing' target='_blank'>emails_for_marketing</a></h3>";
        echo "<h3><a href='/test/base/buyer_emails' target='_blank'>buyer_emails</a></h3>";
        echo "<h3><a href='/test/base/seller_emails' target='_blank'>seller_emails</a></h3>";
        echo "<h3><a href='/test/base/search_time_series' target='_blank'>search_time_series</a></h3>";
        echo "<h3><a href='/test/base/email' target='_blank'>Email</a></h3>";
        echo "<h3><a href='/test/base/google_email' target='_blank'>Google Email</a></h3>";
        echo "<h3><a href='/test/base/general' target='_blank'>Product_clothing_model: get_clothing_by_user_id</a></h3>";
        echo "<h2>Unit Test</h2>";
        echo "<h3><a href='/test/forget_password' target='_blank'>Testing for Forgot Password</a></h3>";
        echo '<p><i>please log in to do forget_password test</i></p>';
        echo "<h3><a href='/test/clothing' target='_blank'>Testing for Clothing</a></h3>";
        echo "<h3><a href='/test/trans_and_account' target='_blank'>Testing for Transaction and Acoount</a></h3>";
        echo "<h3><a href='/test/buyorder' target='_blank'>Testing for Buy Order</a></h3>";
        echo "<h3><a href='/test/sellorder' target='_blank'>Creating Sell-Order With Deductions</a></h3>";
        echo "<h3><a href='/test/withdraw' target='_blank'>Testing for Withdraw</a></h3>";
        echo "<p>test some deductiom amount</p>";
        echo "<h2>Integration Test</h2>";        
        echo "<h3><a href='/test/base/procedure' target='_blank'>Testing From Creating Sell-Order To It's Sold Out</a></h3>";
        echo "<p>#1 create sellorder => #2 create sellorder_product =>
                         #3 create product_clothing => <br />#4 create buyorder including newest clothing =>
                         #5 change the buyorder to paid status => <br /> #6 change the buyorder to done status (noticing transaction and account should change)
                  </p>";
    }
    
    function emails_for_marketing(){
        $this->load->model('marketing_model', 'marketing');
        $emails = $this->marketing->get_all_emails();
        $addresses = array_map(create_function('$o', 'return $o->address;'), $emails);
        
        #echo implode(',', $addresses);
        var_export($addresses);
    }

    function buyer_emails(){
        $this->load->model('marketing_model', 'marketing');
        $emails = $this->marketing->get_buyer_emails();
        $addresses = array_map(create_function('$o', 'return $o->address;'), $emails);
        
        #echo implode(',', $addresses);
        var_export($addresses);
    }

    function seller_emails(){
        $this->load->model('marketing_model', 'marketing');
        $emails = $this->marketing->get_seller_emails();
        $addresses = array_map(create_function('$o', 'return $o->address;'), $emails);
        
        #echo implode(',', $addresses);
        var_export($addresses);
    }

    
    function search_time_series(){
        try {
            require(APPPATH. 'libraries/mandrill/src/Mandrill.php');
            $mandrill = new Mandrill('SXSx6ptiBJ5p-2U_kRt61A');
            $query = 'howtomakeaturn@hotmail.com';
            $date_from = '2013-01-01';
            $date_to = '2014-10-10';
            $tags = array(
            );
            $senders = array('howtomakeaturn@hotmail.com');
            $result = $mandrill->messages->searchTimeSeries($query, $date_from, $date_to, $tags, $senders);
            print_r($result);
        } catch(Mandrill_Error $e) {
            echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            throw $e;
        }
    }
    
    function google_email(){
        $this->load->library('Notification', 'notification');
        $this->notification->send('SUBJECT', 'THIS IS CONTENT', 'howtomakeaturn@hotmail.com');
      /*
        require(APPPATH. 'libraries/PHPMailer-5.1.0/class.phpmailer.php');
        $mail = new PHPMailer();                        // 建立新物件        

        $mail->IsSMTP();                                // 設定使用SMTP方式寄信        
        $mail->SMTPAuth = true;                         // 設定SMTP需要驗證

        $mail->SMTPSecure = "ssl";                      // Gmail的SMTP主機需要使用SSL連線   
        $mail->Host = "smtp.gmail.com";                 // Gmail的SMTP主機        
        $mail->Port = 465;                              // Gmail的SMTP主機的port為465      
        $mail->CharSet = "utf-8";                       // 設定郵件編碼   
        $mail->Encoding = "base64";
        $mail->WordWrap = 50;                           // 每50個字元自動斷行
          
        $mail->Username = "howtomakeaturn@getmore.cc";     // 設定驗證帳號        
        $mail->Password = "";              // 設定驗證密碼        
          
        $mail->From = "howtomakeaturn@getmore.cc";         // 設定寄件者信箱        
        $mail->FromName = "Getmore二次時尚";                 // 設定寄件者姓名        
          
        $mail->Subject = "Getmore二次時尚";                     // 設定郵件標題        
      
        $mail->IsHTML(true);                            // 設定郵件內容為HTML        
        
        $mail->AddAddress('howtomakeaturn@hotmail.com', 'Getmore member');  // 收件者郵件及名稱 
        $mail->Body =                                   // AddAddress(receiverMail, receiverName)
            "
            <p>This is FOR YOU</p>
            ";
        if($mail->Send()) {                             // 郵件寄出
            echo " succeeded<br/>";
        } else {
            echo $mail->ErrorInfo . "<br/>";
        }
 */
    }
    
    function email(){
        try {
            require(APPPATH. 'libraries/mandrill/src/Mandrill.php');
            $mandrill = new Mandrill('kCk5suxl1OwI37BZdARb2g');
            $message = array(
                'html' => '<p>Example HTML content</p>',
                'subject' => 'example subject',
                'from_email' => 'howtomakeaturn@hotmail.com',
                'from_name' => 'Example Name',
                'to' => array(
                    array(
                        'email' => 'howtomakeaturn@hotmail.com',
                        'name' => 'Recipient Name',
                        'type' => 'to'
                    )
                )
            );
            $result = $mandrill->messages->send($message);
            print_r($result);
        } catch(Mandrill_Error $e) {
            echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            throw $e;
        }
        echo 'email';
    }
    
    function general(){
        $this->load->model('Product_clothing_model', 'clothing');
        $clothings = $this->clothing->get_clothing_by_user_id('89');
        
        echo '<h1>SELLING</h1>';
        foreach( $clothings->selling as $clothing ){
            echo json_encode($clothing);
            echo ('<hr />');
        }
        echo '<h1>SENT</h1>';
        foreach( $clothings->sent as $clothing ){
            echo json_encode($clothing);
            echo ('<hr />');
        }
        echo '<h1>FINISHED</h1>';
        foreach( $clothings->finished as $clothing ){
            echo json_encode($clothing);
            echo ('<hr />');
        }
        
        #var_export($clothings);
    }
    

    /*
     * NOTICE: you need to login and put things to sellcart before this test works
     * one complete procedure to testing sell order related behaviours,
     * and its status in database
     * 
     * @author howtomakeaturn
     */
    function procedure(){
        $this->load->model('Sellorder_model', 'order');
        $this->config->set_item('language', 'english');
        
        // this also demo how to create sellorder
        $this->additional_row_created_after_creating_sellorder();
        
        // this also demo how to create items to sellorder
        $this->more_rows_in_table_after_this_method();
        
        // this also demo how to create clothing
        $this->put_last_one_sellorder_product_to_product_clothing();
        
        $this->create_buyorder_including_newest_clothing();
        
        $this->change_newest_buyorder_to_paid_status();
        $this->change_newest_buyorder_to_done_status();
        echo $this->unit->report();          
    }
    
    /*
     * REMINDER: because this action is directly related to session data,
     * please goto sellorder page and put things into cart before testing
     * 
     * this is also how to create one tuple to sellorders,
     * it's really spaghetti. guess he's not a good cooker.
     * 
     * @author howtomakeaturn
     */
    function additional_row_created_after_creating_sellorder(){
        $this->load->model('Sellorder_model', 'sellorder');
        $num_of_orders_before_creation = $this->db->count_all('sellorders');
        $formdata = array(
            "user_id"                   => '89',
            "user_name"                 => 'mocking name',
            "user_sex"                  => '',
            "order_number"              => $this->generate_order_number(),
            "total_price"               => '99999',
            "date_purchased"            => date("Y-m-d H:i:s"),
            "status_id"                 => '5',
            "no_return"                 => ''
        );
        $this->sellorder->add_order($formdata);
        $this->unit->run( $this->db->count_all('sellorders'), 
            $num_of_orders_before_creation + 1 , 
            'additional_row_created_after_creating_sellorder'
         );
    }
    /*
     * this is also how to create one tuple to sellorders_products,
     * it's really spaghetti. guess he's not a good cooker.
     */
    function more_rows_in_table_after_this_method(){
        $num_of_rows_before_creation = $this->db->count_all('sellorders_products');

        $this->load->library('lib_sellorder');
        $order = $this->db->from('sellorders')->order_by('id', 'desc')->limit(1)->get()->result()[0];
        $this->lib_sellorder->user_order(4, $order->id, $order->order_number);
        
        $difference = $this->db->count_all('sellorders_products') - $num_of_rows_before_creation;
        if ($difference > 0){
            $difference = true;
        }
        $this->unit->run( $difference, true, 'more_rows_in_table_after_this_method' );
    }
    /*
     * this is also how to create one tuple to product_clothing,
     * it's really messy. guess he's not a good cooker. maybe he is a ... fractal professor
     */
    function put_last_one_sellorder_product_to_product_clothing(){
        $item = $this->db->from('sellorders_products')->order_by('id', 'desc')->limit(1)->get()->result()[0];

        $table_field=array("product_id", "product_item_number", "product_original_price",
          "product_sell_price", "product_title", "product_brand", "product_type",
          "product_comments", "color", "season", "style", "pattern", "item_long",
          "files", "size", "item_status", "material", "location", "measure","descript"
        );

        $data = array(
            'product_id' => $item->id,
            'product_item_number' => $item->product_item_number,
            'product_sell_price' => '456',
            'product_comments' => 'from unit test'
        );

        $num_of_rows_before_creation = $this->db->count_all('product_clothing');

        $this->load->library('lib_sellorder');

        $id = $this->lib_sellorder->add_order($data,'clothing');      
        
        $this->unit->run( $this->db->count_all('product_clothing'), $num_of_rows_before_creation +1 , 'put_last_one_sellorder_product_to_product_clothing' );
    }
    
    function create_buyorder_including_newest_clothing(){      
        $clothing = $this->db->from('product_clothing')->order_by('id', 'desc')->limit(1)->get()->result()[0];
      
        $user_id = '89';
        $payment_method = 'pseudo_payment_method';

        $ids = array($clothing->id);
        
        $order = $this->mocking_order();
        $deduction_amount = -10;
        
        $num_of_buyorder = $this->db->count_all('buy_order');
        
        $this->load->model('Buy_order_model', 'buyorder');
        $this->buyorder->create_one($order, $ids, $user_id, $payment_method, $deduction_amount);
        
        $buyorder_incremented = false;
        if ($this->db->count_all('buy_order') == $num_of_buyorder +1 ){
            $buyorder_incremented = true;
        }

        $clothing_after = $this->db->from('product_clothing')->order_by('id', 'desc')->limit(1)->get()->result()[0];
        $is_locked = false;
        if ($clothing_after->lock_status == 1){
            $is_locked = true;
        }
        
        $pass = $buyorder_incremented && $is_locked;
        
        $this->unit->run( $pass, true, 'create_buyorder_including_newest_clothing test' );     
    }
    
    function change_newest_buyorder_to_paid_status(){
        $newest_order = $this->db->from('buy_order')->order_by('id', 'desc')->limit(1)->get()->result()[0];
        $this->load->model('Buy_order_model', 'buyorder');
        $user_id = $newest_order->user_id;
                
        $this->buyorder->switch_one_to_paid_status_by_order_number($newest_order->order_number);
        
        /*
        // check if transaction increments
        $transaction_incremented = false;
        if ($this->db->count_all('transactions') == $num_of_transaction +1 ){
            $transaction_incremented = true;
        }

        // check if account balance increments
        $account_incremented = false;
        if ($this->account->get_by_user_id($user_id)->balance > $balance_before_creation ){
            $account_incremented = true;
        }
        
        $pass = $transaction_incremented && $account_incremented;        
        */
        $order = $this->buyorder->get_by('order_number', $newest_order->order_number);

        $this->unit->run( $order->status, '2', 'change_newest_buyorder_to_paid_status test' );     
    }

    function change_newest_buyorder_to_done_status(){
        $newest_order = $this->db->from('buy_order')->order_by('id', 'desc')->limit(1)->get()->result()[0];
        $this->load->model('Buy_order_model', 'buyorder');
        $user_id = $newest_order->user_id;
        
        $num_of_transaction = $this->db->count_all('transactions');
        $this->load->model('Account_model', 'account');
        $balance_before_creation = $this->account->get_by_user_id($user_id)->balance;        
        
        $this->buyorder->switch_one_to_done_status_by_order_number($newest_order->order_number);
        
        // check if transaction increments
        $transaction_incremented = false;
        if ($this->db->count_all('transactions') == $num_of_transaction +1 ){
            $transaction_incremented = true;
        }

        // check if account balance increments
        $account_incremented = false;
        if ($this->account->get_by_user_id($user_id)->balance > $balance_before_creation ){
            $account_incremented = true;
        }
        
        $pass = $transaction_incremented && $account_incremented;        
        $this->unit->run( $pass, true, 'change_newest_buyorder_to_done_status test' );     
    }
    
    


} 
?>
