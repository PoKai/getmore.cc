<?php

class Buyorder extends Test_Controller {
      /*
     * Unit Test for Transaction and Account Model
     */

    function index() {
        $this->load->model('Buy_order_model', 'buyorder');
        $this->load->model('Product_clothing_model', 'clothing');

        $this->config->set_item('language', 'english');
       $this->refresh_items_amount_and_price();
       $this->throw_exception_if_payment_method_is_deduction_with_none_zero_final_price_test();
        $this->no_exception_if_total_price_is_zero_with_valid_payment_method_test();
        
        $this->simple_create_test();        
        // $this->simple_create_with_zero_deduction_amount_test();
        $this->throw_exception_if_user_id_not_exist_test();
        $this->throw_exception_if_total_price_is_zero_with_invalid_payment_method_test();
        $this->throw_exception_if_total_price_is_negative_test();
        $this->throw_exception_if_deduction_amount_is_string_test();
        $this->throw_exception_if_deduction_amount_is_positive_test();
        $this->get_exactly_ten_from_the_method_test();
        $this->latest_ten_order_price_consistent_test();

        echo $this->unit->report();      
    }


    /*
     * Unit Test for Buy_order_model
     */
    
    function refresh_items_amount_and_price(){
        $result = $this->buyorder->refresh_items_amount_and_price('217');    
        
        $this->unit->run( $result, true, 'refresh_items_amount_and_price' );
        
    }
    
    function throw_exception_if_payment_method_is_deduction_with_none_zero_final_price_test(){
        $found_exception = false;
        try{
            $user_id = '89';
            $payment_method = 'deduction';

            $ids = array(999,998,997);
            $order = $this->mocking_order();
            $deduction_amount = -79;
            
            $this->buyorder->create_one($order, $ids, $user_id, $payment_method, $deduction_amount);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'throw_exception_if_payment_method_is_deduction_with_none_zero_final_price_test' );
    }    
    
    
    function no_exception_if_total_price_is_zero_with_valid_payment_method_test(){
        $found_exception = false;
        try{
            $user_id = '89';
            $payment_method = 'deduction';

            $ids = array(999,998,997);
            $order = $this->mocking_order();
            $deduction_amount = -80;
            
            $this->buyorder->create_one($order, $ids, $user_id, $payment_method, $deduction_amount);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, false, 'no_exception_if_total_price_is_zero_with_valid_payment_method_test' );
    }


    function simple_create_with_zero_deduction_amount_test(){
        $num_of_orders_before_creation = $this->buyorder->count_all();
      
        $user_id = '89';
        $payment_method = 'pseudo_payment_method';

        $ids = array(999,998,997);
        $order = $this->mocking_order();
        $deduction_amount = 0;
        
        $this->buyorder->create_one($order, $ids, $user_id, $payment_method, $deduction_amount);
        
        $this->unit->run( $this->buyorder->count_all(), $num_of_orders_before_creation + 1, 'Simple Create With Zero Deduction amount test' );     
    }

    function throw_exception_if_user_id_not_exist_test(){
        $found_exception = false;
        try{
            $user_id = '0';
            $payment_method = 'pseudo_payment_method';

            $ids = array(999,998,997);
            $order = $this->mocking_order();
            $deduction_amount = 0;
            
            $this->buyorder->create_one($order, $ids, $user_id, $payment_method, $deduction_amount);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'Throw Exception If User Id Not Exist' );
    }

    function throw_exception_if_total_price_is_zero_with_invalid_payment_method_test(){
        $found_exception = false;
        try{
            $user_id = '89';
            $payment_method = 'pseudo_payment_method';

            $ids = array(999,998,997);
            $order = $this->mocking_order();
            $deduction_amount = -80;
            
            $this->buyorder->create_one($order, $ids, $user_id, $payment_method, $deduction_amount);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'Throw Exception If Total Price is Zero' );
    }

    function throw_exception_if_total_price_is_negative_test(){
        $found_exception = false;
        try{
            $user_id = '89';
            $payment_method = 'pseudo_payment_method';

            $ids = array(999,998,997);
            $order = $this->mocking_order();
            $deduction_amount = -100000;
            
            $this->buyorder->create_one($order, $ids, $user_id, $payment_method, $deduction_amount);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'Throw Exception If Total Price is Negative' );
    }
    
    function throw_exception_if_deduction_amount_is_string_test(){
        $found_exception = false;
        try{
            $user_id = '89';
            $payment_method = 'pseudo_payment_method';

            $ids = array(999,998,997);
            $order = $this->mocking_order();
            $deduction_amount = '-10';
            
            $this->buyorder->create_one($order, $ids, $user_id, $payment_method, $deduction_amount);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'Throw Exception If Deduction Amount Is String' );
    }

    function throw_exception_if_deduction_amount_is_positive_test(){
        $found_exception = false;
        try{
            $user_id = '89';
            $payment_method = 'pseudo_payment_method';

            $ids = array(999,998,997);
            $order = $this->mocking_order();
            $deduction_amount = +100;
            
            $this->buyorder->create_one($order, $ids, $user_id, $payment_method, $deduction_amount);
        }
        catch (Exception $e){
            $found_exception = true;          
        }
        $this->unit->run( $found_exception, true, 'Throw Exception If Deduction Amount Is Positive' );
    }
    
    function simple_create_test(){
        $num_of_orders_before_creation = $this->buyorder->count_all();
      
        $user_id = '89';
        $payment_method = 'pseudo_payment_method';

        $ids = array(999,998,997);
        $order = $this->mocking_order();
        $deduction_amount = -75;
        
        $this->buyorder->create_one($order, $ids, $user_id, $payment_method, $deduction_amount);
        
        $this->unit->run( $this->buyorder->count_all(), $num_of_orders_before_creation + 1, 'Simple Create test' );     
    }
    
    function get_exactly_ten_from_the_method_test(){
        $orders = $this->buyorder->get_many_latest_orders(10);        
        $this->unit->run( count($orders), 10, 'Get Exactly 10 Orders test' );     
    }
        
    function latest_ten_order_price_consistent_test(){
        $orders = $this->buyorder->get_many_latest_orders(10);
        $every_order_is_consistent = true;
        
        foreach($orders as $order){
            if ($order['total_price'] != $order['items_price'] + $order['shipping_price'] + $order['deduction_amount']){
                $every_order_is_consistent = false;
                break;
            }
        }
        $this->unit->run( $every_order_is_consistent, true, 'Latest 10 orders Consistent Price test' );     
    }


        
    
}


/* End of file BUYRDER.php */
/* Location: ./application/controller/BUORDER.php */
