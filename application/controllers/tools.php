<?php
/*
 * For dirty and quick debugging.
 * Find out some information for some one time purposes.
 */
class Tools extends CI_Controller {
  
    function __construct(){
        parent::__construct();
        /*
        $whitelist = array('127.0.0.1');

        if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
            exit('not allowed');
        }
        */
        $this->load->library('ion_auth');
        if ( !$this->ion_auth->is_admin()){
            exit('forbidden');
        }
    }

    public function message($to = 'World'){
        echo "Hello {$to}!".PHP_EOL;
        $this->load->database();
        $this->load->model('Product_clothing_model');
        $clothing =$this->Product_clothing_model->add_stupid_row($to);    
    }
    
    function index(){
        echo "<p></p><a href='/tools/analyze_sellorders'>analyze_sellorders</a></p>";
        echo "<p></p><a href='/tools/analyze_sellorders_products'>analyze_sellorders_products</a></p>";
        echo "<p></p><a href='/tools/analyze_product_clothing'>analyze_product_clothing</a></p>";
        echo "<p></p><a href='/tools/analyze_account'>analyze_account</a></p>";
        echo "<p></p><a href='/tools/findout_missing_clothing'>findout_missing_clothing</a></p>";
        echo "<p></p><a href='/tools/dump_latest_log'>dump_latest_log</a></p>";
    }
  
    function analyze_account(){
        echo '<meta charset="utf-8">';
        echo '<h1>analyze analyze_account</h1>';
        
        $this->load->model('Account_model', 'account');
        $accounts = $this->db
                              ->order_by('balance', 'desc')
                              ->get('accounts')->result();
        foreach($accounts as $account){
            echo '<h1>balance: ' . $account->balance . '</h1>';
            echo '<h2>user_id: ' . $account->user_id . '</h2>';
            $trans = $this->db
                                        ->where('user_id', $account->user_id)
                                        ->get('transactions')->result();
            $check_balance = 0;
            foreach($trans as $tran){
                $check_balance += $tran->amount;
                echo '<p><i>' . $tran->amount . '</i></p>';
            }
            echo '<h3>check: ' . $check_balance . '</h3>';
        }
    
    }
  
    function analyze_sellorders(){
        echo '<meta charset="utf-8">';
        echo '<h1>analyze sellorder: check if duplicate order_number</h1>';

        $this->load->library('table');
        $this->load->database();
        $query = $this->db->get('sellorders');
        
        $orders = $query->result();
        foreach($orders as $order){
            $order_number = $order->order_number;
            $order_id = $order->id;
            $query = $this->db
                ->where('order_number', $order_number)
                ->get('sellorders');
            $num_rows = $query->num_rows();
            if ($num_rows != 1){
                echo "<p style='color: red;'>$order_number (order_id: $order_id) with $num_rows tuples.</p>";
                $query = $this->db
                    ->where('order_id', $order->id)
                    ->get('sellorders_products');
                echo $this->table->generate($query);
            }
        }
        
        $query = $this->db->get('sellorders');
        echo $this->table->generate($query);
    }
    
    function analyze_sellorders_products(){
        echo '<meta charset="utf-8">';
        echo '<h1>analyze sellorders_products: check if duplicate product_item_number</h1>';

        $this->load->library('table');
        $this->load->database();
        $query = $this->db->get('sellorders_products');
        
        $items = $query->result();
        foreach($items as $item){
            $item_number = $item->product_item_number;
            $id = $item->id;
            $query = $this->db
                ->where('product_item_number', $item_number)
                ->get('sellorders_products');
            $num_rows = $query->num_rows();
            if ($num_rows != 1){
                echo "<p style='color: red;'>$item_number (id: $id) with $num_rows tuples.</p>";
                /*
                $query = $this->db
                    ->where('order_id', $order->id)
                    ->get('sellorders_products');
                echo $this->table->generate($query);
                */
            }
        }        
    }

    function analyze_product_clothing(){
        echo '<meta charset="utf-8">';
        echo '<h1>analyze product_clothing: check if duplicate product_item_number</h1>';

        $this->load->library('table');
        $this->load->database();
        $query = $this->db->get('product_clothing');
        
        $items = $query->result();
        foreach($items as $item){
            $item_number = $item->product_item_number;
            $id = $item->id;
            $query = $this->db
                ->where('product_item_number', $item_number)
                ->get('product_clothing');
            $num_rows = $query->num_rows();
            if ($num_rows != 1){
                echo "<p style='color: red;'>$item_number (id: $id) with $num_rows tuples.</p>";
            }
        }        
    }

    function findout_missing_clothing(){
        echo '<meta charset="utf-8">';
        echo '<h1>findout_missing_clothing</h1>';

        $this->load->library('table');
        $this->load->database();
        $items = $this->db->from('sellorders_products')
                                       ->where('shelf', 1)
                                       ->get()->result();

        $this->table->set_heading('id', 'title', 'item_number', 'wash_id');
        foreach($items as $item){
            $query = $this->db->from('product_clothing')
                                           ->where('product_id', $item->id)
                                           ->get();
            if (empty($query->num_rows)){
                $id = $item->id;
                $name = $item->product_title;
                $item_number = $item->product_item_number;
                $wash_id = $item->shipping_wash_id;
                $this->table->add_row($id, $name, $item_number, $wash_id);
            }
        }
        echo $this->table->generate();
        
    }

    function dump_latest_log(){
        echo '<meta charset="utf-8">';
        echo "<link rel='stylesheet' type='text/css' media='all' href='/assets/css/bootstrap.css' />";
        echo '<h1>DUMP LOG</h1>';

        $this->load->library('table');
        $this->load->database();
        $query = $this->db->from('logs')
                                        ->order_by('id', 'desc')
                                        ->limit(100)
                                        ->get();

        $tmpl = array (
            'table_open'          => '<table class="table-hover table table-bordered">',

            'heading_row_start'   => '<tr>',
            'heading_row_end'     => '</tr>',
            'heading_cell_start'  => '<th>',
            'heading_cell_end'    => '</th>',

            'row_start'           => '<tr>',
            'row_end'             => '</tr>',
            'cell_start'          => '<td>',
            'cell_end'            => '</td>',

            'row_alt_start'       => '<tr>',
            'row_alt_end'         => '</tr>',
            'cell_alt_start'      => '<td>',
            'cell_alt_end'        => '</td>',

            'table_close'         => '</table>'
        );

        $this->table->set_template($tmpl);
        echo $this->table->generate($query);
        
    }

    
}
?>
