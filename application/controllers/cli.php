<?php

class Cli extends CI_Controller
{
    /**
     * UNIX timestamp
     *
     * @var int
     **/
    public $_time = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('image_lib', 'lib_config', 'lib_product', 'files_lib', 'lib_event', 'lib_subject', 'lib_order'));
        $this->load->config('images');

        $this->_time = time();
        $this->upload_path = $this->config->item('upload_path');
        if (!defined('STDIN')) {
            echo "\nYou are not Administrator\n\n";
            exit();
        } else {
            echo "\nWelcome Command line system\n\n";
        }
        // The maximum execution time, in seconds. If set to zero, no time limit is imposed.
        set_time_limit(0);
    }

    /**
     * Image file transfer
     *
     * @author appleboy
     * @param  string
     * @return null
     **/
    public function image_transfer($type = 'all')
    {
        // get product images
        $query = $this->lib_product->select('products.file_list')->products()->result_array();

        // transfer product images
        foreach ($query as $row) {
            $files = $this->files_lib->where('file_id', explode(',', $row['file_list']))->files()->result_array();
            foreach ($files as $file) {
                $this->image_lib->image_resize($file['file_name'], 'product', false, true);
            }
        }

        // get event images
        $query = $this->lib_event->select('events.file_list')->events()->result_array();

        // transfer event images
        foreach ($query as $row) {
            $files = $this->files_lib->where('file_id', explode(',', $row['file_list']))->files()->result_array();
            foreach ($files as $file) {
                $this->image_lib->image_resize($file['file_name'], 'event', false, true);
            }
        }

        // get subject images
        $query = $this->lib_subject->select('product_subjects.file_list')->subjects()->result_array();

        // transfer subject images
        foreach ($query as $row) {
            $files = $this->files_lib->where('file_id', explode(',', $row['file_list']))->files()->result_array();
            foreach ($files as $file) {
                $this->image_lib->image_resize($file['file_name'], 'subject', false, true);
            }
        }

        // get top image and link lists
        $top_image = $this->lib_config->top_image();

        foreach ($top_image as $row) {
            $this->image_lib->image_resize($row['file_name'], 'banner', false, true);
        }
    }

    /**
     * transfer order
     *
     * check payment limit time
     *
     * @return null
     * @author appleboy
     **/
    public function order_transfer()
    {

        // get payment limit time config
        $limit = $this->lib_order->limit_payment_time();

        // get all orders if order status == ORDER_PAYMENT
        $query = $this->lib_order->select('orders.id, orders.add_time, payment_method')
                    ->where('orders.status_id', ORDER_PAYMENT)->orders()->result_array();

        foreach ($query as $row) {

            $limit_time = $limit[$row['payment_method']]['time'];

            if ($row['add_time'] > ($this->_time - $limit_time)) {
                continue;
            }

            // handle order products
            $products = $this->lib_order->where('order_id', $row['id'])->orders('product')->result_array();

            foreach ($products as $product) {
                // restore inventory_amount count
                $this->lib_product->update_product($product['product_id'], array('inventory_amount' => 'inventory_amount + ' . $product['product_quantity']), false);
            }

            // handle order events
            $events = $this->lib_order->where('order_id', $row['id'])->orders('event')->result_array();

            foreach ($events as $event) {
                // restore peoples count
                $this->lib_event->update_event($event['event_id'], array('peoples' => 'peoples + ' . $event['event_quantity']), false);
            }

            $this->lib_order->update_order($row['id'], array('status_id' => ORDER_EXPIRED));
            echo '取消訂單: ' . $row['id'] . ', 下單日期: ' . date('Y-m-d H:i:s', $row['add_time']) .
                ', 付款方式: ' . $row['payment_method'] . "\n";
        }
    }

    public function upload_product_clothings_to_s3($from = 0){
        $this->load->database();

        $clothings = $this->db->select('files')->get('product_clothing')->where('files !=', '')->result_array();

        $file_ids = array_reduce($clothings, function($result, $clothing){
            $clothing_files = explode(",", $clothing["files"]);
            return array_merge($result, $clothing_files);
        }, []);

        $this->db->select('file_id,file_name')->where_in('file_id', $file_ids);
        
        if($from){
            $this->db->where('file_id >=', $from);
        }
        
        $files = $this->db->get('files')->result_array();

        $size = array('large' => '800x1200', 'medium' => '250x375', 'small' => '200x300');

        $this->load->library('aws_sdk');

        foreach ($files as $index => $file) {
            if(file_exists('./upload/'.$file['file_name'])){
                $file_path = './upload/'.$file['file_name'];
            }
            else if(file_exists('./upload/thumbs/800_1200_'.$file['file_name'])){
                $file_path = './upload/thumbs/800_1200_'.$file['file_name'];
            }
            else {
                continue;
            }
            $thumb = new Imagick($file_path);
            $thumb->setImageFormat('jpeg');

            foreach ($size as $version => $dimension) {
                list($width, $height) = explode('x', $dimension);
                $thumb->resizeImage($width,$height,Imagick::FILTER_LANCZOS,1, true);

                $this->aws_sdk->putObject(array(
                    'Bucket' => 'getmore',
                    'Key' => 'clothing/'.$file['file_id'].'/'.$version.'.jpg',
                    'Body' => $thumb->getimageblob(),
                    'ContentType' => 'image/jpeg',
                    'ACL' => 'public-read'
                ));
            }
        }

        echo "done!!\n";
    }

    public function upload_user_photo_to_s3($from = 0){
        $this->load->database();
        
        $closets = $this->db->select('profile_photo_file_id')->where('profile_photo_file_id !=', 0)->get('closet')->result_array();

        $file_ids = array_reduce($closets, function($result, $closet){
            array_push($result, $closet['profile_photo_file_id']);
            return $result;
        }, []);
        
        $this->db->select('file_id,file_name')->where_in('file_id', $file_ids);
        
        if($from){
            $this->db->where('file_id >=', $from);
        }
        
        $files = $this->db->get('files')->result_array();

        $size = array('medium' => '300x300', 'small' => '165x165');

        $this->load->library('aws_sdk');

        foreach ($files as $index => $file) {
            if(!file_exists('./upload/'.$file['file_name'])) continue;
            $thumb = new Imagick('./upload/'.$file['file_name']);
            $thumb->setImageFormat('jpeg');

            foreach ($size as $version => $dimension) {
                list($width, $height) = explode('x', $dimension);
                $thumb->resizeImage($width,$height,Imagick::FILTER_LANCZOS,1, true);

                $this->aws_sdk->putObject(array(
                    'Bucket' => 'getmore',
                    'Key' => 'profile/'.$file['file_id'].'/'.$version.'.jpg',
                    'Body' => $thumb->getimageblob(),
                    'ContentType' => 'image/jpeg',
                    'ACL' => 'public-read'
                ));
            }
        }

        echo "done!!\n";
    }

    public function upload_user_banner_to_s3($from = 0){
        $this->load->database();
        
        $closets = $this->db->select('banner_photo_file_id')->where('banner_photo_file_id !=', 0)->get('closet')->result_array();
        $file_ids = array_reduce($closets, function($result, $closet){
            array_push($result, $closet['banner_photo_file_id']);
            return $result;
        }, []);

        $this->db->select('file_id,file_name')->where_in('file_id', $file_ids);
        
        if($from){
            $this->db->where('file_id >=', $from);
        }
        
        $files = $this->db->get('files')->result_array();

        $this->load->library('aws_sdk');

        foreach ($files as $index => $file) {
            if(!file_exists('./upload/'.$file['file_name'])) continue;
            $thumb = new Imagick('./upload/'.$file['file_name']);
            $thumb->setImageFormat('jpeg');

            $thumb->resizeImage(980,430,Imagick::FILTER_LANCZOS,1, true);

            $this->aws_sdk->putObject(array(
                'Bucket' => 'getmore',
                'Key' => 'user_banner/'.$file['file_id'].'.jpg',
                'Body' => $thumb->getimageblob(),
                'ContentType' => 'image/jpeg',
                'ACL' => 'public-read'
            ));
        }

        echo "done!!\n";
    }

    public function upload_banner_to_s3($from = 0){
        $this->load->database();
        
        $banners = $this->db->select('file_id')->get('banners')->result_array();
        $file_ids = array_reduce($banners, function($result, $banner){
            array_push($result, $banner['file_id']);
            return $result;
        }, []);

        $this->db->select('file_id,file_name')->where_in('file_id', $file_ids);
        
        if($from){
            $this->db->where('file_id >=', $from);
        }
        
        $files = $this->db->get('files')->result_array();

        $this->load->library('aws_sdk');

        foreach ($files as $index => $file) {
            if(!file_exists('./upload/'.$file['file_name'])) continue;
            $thumb = new Imagick('./upload/'.$file['file_name']);
            $thumb->setImageFormat('jpeg');

            $thumb->resizeImage(980,410,Imagick::FILTER_LANCZOS,1, true);

            $this->aws_sdk->putObject(array(
                'Bucket' => 'getmore',
                'Key' => 'banner/'.$file['file_id'].'.jpg',
                'Body' => $thumb->getimageblob(),
                'ContentType' => 'image/jpeg',
                'ACL' => 'public-read'
            ));
        }

        echo "done!!\n";
    }

    public function upload_blog_to_s3($from = 0){
        $this->load->database();
        
        $blogs = $this->db->select('feature_pic_id')->get('articles')->result_array();
        $file_ids = array_reduce($blogs, function($result, $blog){
            array_push($result, $blog['feature_pic_id']);
            return $result;
        }, []);

        $this->db->select('file_id,file_name')->where_in('file_id', $file_ids);
        
        if($from){
            $this->db->where('file_id >=', $from);
        }
        
        $files = $this->db->get('files')->result_array();

        $this->load->library('aws_sdk');

        foreach ($files as $index => $file) {
            if(!file_exists('./upload/'.$file['file_name'])) continue;
            $thumb = new Imagick('./upload/'.$file['file_name']);
            $thumb->setImageFormat('jpeg');

            $thumb->resizeImage(620,417,Imagick::FILTER_LANCZOS,1, true);

            $this->aws_sdk->putObject(array(
                'Bucket' => 'getmore',
                'Key' => 'blog/'.$file['file_id'].'.jpg',
                'Body' => $thumb->getimageblob(),
                'ContentType' => 'image/jpeg',
                'ACL' => 'public-read'
            ));
        }

        echo "done!!\n";
    }

    public function upload_blog_to_s3($from = 0){
        $this->load->database();
        
        $blogs = $this->db->select('feature_pic_id')->get('articles')->result_array();
        $file_ids = array_reduce($blogs, function($result, $blog){
            array_push($result, $banner['feature_pic_id']);
            return $result;
        }, []);

        $this->db->select('file_id,file_name')->where_in('file_id', $file_ids);
        
        if($from){
            $this->db->where('file_id >=', $from);
        }
        
        $files = $this->db->get('files')->result_array();

        $this->load->library('aws_sdk');

        foreach ($files as $index => $file) {
            $thumb = new Imagick('./upload/'.$file['file_name']);
            $thumb->setImageFormat('jpeg');

            $thumb->resizeImage(620,417,Imagick::FILTER_LANCZOS,1);

            $this->aws_sdk->putObject(array(
                'Bucket' => 'getmore',
                'Key' => 'blog/'.$file['file_id'].'.jpg',
                'Body' => $thumb->getimageblob(),
                'ContentType' => 'image/jpeg',
                'ACL' => 'public-read'
            ));
        }

        echo "done!!\n";
    }
}

/* End of file cli.php */
/* Location: ./application/controllers/cli.php */

