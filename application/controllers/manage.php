<?php

/*
 * admin site without backbone.js
 * 
 * @author howtomakeaturn
 */
class Manage extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->spark('codeigniter-template/1.0.0');        
        $this->load->library('ion_auth');
        if ( !$this->ion_auth->is_admin() && !$this->ion_auth->is_operation()){
            exit('forbidden');
        }
        $this->load->database();
        $this->load->model('Manage_model');
    }
    
    function order_detail($order_number){
        $this->load->library('parser');
        $this->load->helper('buyorder_helper');
        
        $data = $this->Manage_model->order_detail($order_number);
        
        $this->template->set_layout("template/empty", []);
        $this->template->render('manage/order_detail', $data);
    }
}
