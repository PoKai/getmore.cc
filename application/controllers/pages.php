<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Pages extends MY_Controller {
	protected $data = array();

	function __construct() {
		parent::__construct();
		$this->load->database();

		$this->load->spark('Twiggy/0.8.5');
		$this->twiggy->theme('');

		$this->twiggy->set(array('lang' => 'zh-tw', 'meta_charset' => 'UTF-8', 'site_title' => 'Getmore 專注在「時尚」社群的購物網站 - 原創新銳設計/時尚配件/時尚買手/二手精品/二手包/品牌二手代售', 'site_description' => 'Getmore 是專注在「時尚」社群的購物網站。聚集新銳設計師、買手、時尚愛好者的購物網站，供應獨特單品以及二手時尚的時尚購物平台。時尚很多元，就像是一個個精彩的小圈圈，Getmore 負責把時尚圈在一起，你也可以稱這裡是「時尚小圈圈」。', 'site_keywords' => '時尚,衣著,服飾,時裝,二手衣,流行,新銳設計師,亞洲時尚,褲子,買二手衣,賣二手衣,環保,電子商務,startup,二次時尚', 'logined' => $this->ion_auth->logged_in()), NULL, TRUE);

	}

	// smart way to render
	function render() {
		$view_path = $this->router->fetch_class() . '/' .
		$this->router->fetch_method();
		$view_path = strtolower($view_path);
		$this->twiggy->set($this->data)->template($view_path)->display();
	}

	function index() {
		$this->render();
	}
	function annual2015aw() {
		$this->render();
	}
	function about() {
		$this->render();
	}
	function hellodsner() {
		$this->render();
	}
	function jointogether() {
		$this->render();
	}
	function happy1111() {
		$this->render();
	}
	function mobile_index() {
		$this->render();
	}
	function mobile_about() {
		$this->render();
	}
	function mobile_product() {
		$this->render();
	}
	function mobile_checkout1() {
		$this->render();
	}
	function mobile_checkout2() {
		$this->render();
	}
	function mobile_orderlist() {
		$this->render();
	}
	function mobile_productlist() {
		$this->render();
	}
	function closetsell() {
		$this->render();
	}

	function christmas2015() {
		$this->render();
	}
	function luxury() {
		$this->render();
	}
	function fashionbuyer() {
		$this->render();
	}
}
