<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subject extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('lib_subject', 'lib_product', 'files_lib', 'lib_category'));
        $this->template->set('is_hidden_header', true);
        $this->template->set('is_subject', true);
        redirect('/');        
    }

    public function index()
    {
        redirect('/');
    }

    /**
     * Subject list
     *
     * @param  int
     * @author appleboy
     **/
    public function lists($id = '')
    {
        $this->template->set('category_id', $id);

        $return = $this->lib_subject->get_list(array('id' => $id));

        if ($return['count'] == 0) {
            redirect('/error404');
        }

        $data = array(
            'id' => $id,
            'count' => $return['count'],
            'output' => $return['output']
        );

        $this->template->render('subject/lists', $data);
    }

    /**
     * Single Subject
     *
     * @param  int
     * @author appleboy
     **/
    public function single($id = '')
    {
        $id = intval($id);
        ($id <= 0) and redirect('/');

        $row = $this->lib_subject->select('category.tw_name as category_name, product_subjects.*')->where('product_subjects.id', $id)->subjects()->row_array();

        // empty row or isn't enable
        if (empty($row) or !(_IS_ENABLED & $row['options'])) {
            redirect('/');
        }

        // set category id.
        $this->template->set('subject_id', $row['category_id']);
        $row['image_list'] = array();
        if ($row['file_list'] != '') {
            $files = $this->files_lib->where('file_id', explode(',', $row['file_list']))->order_by_field('file_id', $row['file_list'])->files()->result_array();
            $row['image_list'] = $files;
        }
        // set image list default value
        $row['image_list'][0]['file_name'] = isset($row['image_list'][0]['file_name']) ? $row['image_list'][0]['file_name'] : '';
        $row['image_list'][1]['file_name'] = isset($row['image_list'][1]['file_name']) ? $row['image_list'][1]['file_name'] : '';
        $row['image_list'][2]['file_name'] = isset($row['image_list'][2]['file_name']) ? $row['image_list'][2]['file_name'] : '';

        $products = array();
        $products = $this->lib_product->select('files.file_name, products.id, products.title, products.description, products.original_price, products.sell_price, products.inventory_amount')->where('products.subject_id', $id)->where('(products.options &' . (_IS_ENABLED) . ' >= 1)')->products()->result_array();
        $row['products'] = $products;

        $this->template->render('subject/single', $row);
    }
}
