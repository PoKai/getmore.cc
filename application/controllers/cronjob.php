<?php
class Cronjob extends CI_Controller {

    // this dirty and quick fix is no longer needed :D
    /*
    public function unlock_unpaid_clothing(){
        $this->load->database();
        $this->load->model('Product_clothing_model');
        $this->Product_clothing_model->unlock_unpaid_clothing();    
    }
    */
    
    public function handle_expired_buyorders_with_credit_card(){
        try{
            $this->load->database();
            $this->load->model('Buy_order_model');
            $expired_order_ids = $this->Buy_order_model->handle_expired_buyorders_with_credit_card();
            echo 'turned ' . count($expired_order_ids) . ' orders from unpaid(1) to expired(-1).';
        }
        catch(Exception $e){
            show_error($e->getMessage());
        }
    }
    
    public function handle_expired_buyorders_with_atm(){
        try{
            $this->load->database();
            $this->load->model('Buy_order_model');
            $expired_order_ids = $this->Buy_order_model->handle_expired_buyorders_with_atm();
            echo 'turned ' . count($expired_order_ids) . ' orders from unpaid(1) to expired(-1).';
        }
        catch(Exception $e){
            show_error($e->getMessage());
        }
    }
    
    public function handle_expired_buyorders_with_line_pay(){
        try{
            $this->load->database();
            $this->load->model('Buy_order_model');
            $expired_order_ids = $this->Buy_order_model->handle_expired_buyorders_with_line_pay();
            echo 'turned ' . count($expired_order_ids) . ' orders from unpaid(1) to expired(-1).';
        }
        catch(Exception $e){
            show_error($e->getMessage());
        }
    }
    
    public function switch_sellorder_from_washed_to_done(){
        try{
            $this->load->database();
            $this->load->model('Sellorder_model');
            $this->Sellorder_model->switch_sellorder_from_washed_to_done();          
        }
        catch(Exception $e){
            show_error($e->getMessage());
        }
    }

    public function notify_many_users_has_suggested_price(){
        try{
            $this->load->database();
            $this->load->model('Notification_model');
            $this->Notification_model->notify_many_users_has_suggested_price();    
            echo 'emails sent.';     
        }
        catch(Exception $e){
            show_error($e->getMessage());
        }
    }

    public function notify_atm_buyorder_almost_due(){
        $this->load->database();
        $this->load->model('Buy_order_model');
        
        $how_many = $this->Buy_order_model->handle_almost_due_atm_buyorder();
        echo $how_many . ' orders is sent sms.';
    }
    
    public function daily_tasks(){
        try{
            $this->load->database();
          
            $this->load->model('Product_clothing_model');
            $this->Product_clothing_model->remove_from_shelf();
            echo ' # Checked clothing to be removed from shelf.';

            echo ' # Daily tasks finished.';
        }
        catch(Exception $e){
            show_error($e->getMessage());
        }      
    }
    
    function backup_database(){
        $this->load->library('Getmore/Services/DatabaseManager');
        $this->databasemanager->send_backup_to_email();
        #$this->databasemanager->dumb_mail();
        echo 'success';
    }
  
    public function launch_reservation(){
        $this->load->database();
          
        $this->load->model('Product_clothing_model');

        $this->Product_clothing_model->make_reservation_on_shelf();
    }

}
?>
