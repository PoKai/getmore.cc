<?php

/* one-time excution task for helping upgrate database data,
 * or something like that. these tasks dont need to be done
 * in the cronjob.
 * @author howtomakeaturn
 */
class Fixjob extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('ion_auth');
        if ( !$this->ion_auth->is_admin()){
            exit('forbidden');
        }
    }  
    
    function fill_in_final_price(){
        $this->load->model('Order_model');

        $query = $this->db->from('sellorders')
                                        ->get();
        
        echo $query->num_rows() . ' rows in sellorders. <br />';
        
        $query = $this->db->select('id, total_price, transportation_charge, deduction_amount, coupon_amount, payment_method')
                                            ->from('sellorders')
                                            ->where('final_price', 0)
                                            ->get();
        
        echo $query->num_rows() . ' rows have value 0 in final price. <br />';
        
        $orders = $query->result_array();
        
        $this->db->trans_start();
        foreach($orders as $order){
            $id = $order['id'];
            
            $final_price = (int) $order['total_price'] + 
                (int) $order['transportation_charge'] + 
                (int) $order['deduction_amount'] +
                (int) $order['coupon_amount'];

     //       echo "fill in $final_price for: " . $id . '<br />';
       //     echo json_encode($order);
         //   echo '<hr />';
            $this->db->where('id', $id)
                              ->update('sellorders', array('final_price' => $final_price));
          
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            echo('transaction failed.');
        }      
    }
    // Deprecated this function since now the '-2' have
    // other meaning.
    /*
    function change_rejected_status_number(){
        $query = $this->db->from('buy_order')
                                        ->where('status', '-2')
                                        ->get();

        echo 'status with -2: ' . $query->num_rows() . ' rows. <br />';

        $query->free_result();
        
        $data = array( 'status' => '6' );
        $this->db->where('status', '-2')
                        ->update('buy_order', $data);
        
        echo 'after process<br />';
        $query = $this->db->from('buy_order')
                                        ->where('status', '-2')
                                        ->get();

        echo 'status with -2: ' . $query->num_rows() . ' rows. <br />';

        $query->free_result();
    }
    */

	public function handle_expired_buyorders(){
    try{
        $this->load->database();
        $this->load->model('Buy_order_model');
        $expired_order_ids = $this->Buy_order_model->handle_expired_buyorders();
        echo 'turned ' . count($expired_order_ids) . ' orders from unpaid(1) to expired(-1).';
    }
    catch(Exception $e){
        show_error($e->getMessage());
    }
	}
  
	public function handle_old_users(){
    try{
        $this->load->database();
        $this->load->model('Account_model', 'account');
        $users = $this->db->get('users')->result();
        foreach($users as $user){
            $created_now = $this->account->create_if_not_exist($user->id);
            if ($created_now){
                echo 'create account for user_id: ' . $user->id;
                echo '<br />';
            }
        }
    }
    catch(Exception $e){
        show_error($e->getMessage());
    }
	}
  function fill_in_publication_time(){
      try{
          $this->load->database();
          $clothings = $this->db->get('product_clothing')->result();
          echo count($clothings);
          foreach ($clothings as $clothing){
              if ($clothing->publication_time == '0000-00-00 00:00:00'){
                  $publication_time = date('Y-m-d H:i:s', $clothing->add_time);
                  $this->db->where('id', $clothing->id);
                  $this->db->update('product_clothing', array('publication_time' => $publication_time ));
                  echo '<p>set id: ' . $clothing->id . 'at ' . $publication_time . '</p>';
              }
          }
      }
      catch(Exception $e){
          show_error($e->getMessage());
      }      
  }

    function fill_in_closet(){
        try{
            $this->load->database();
            $this->load->model('Closet_model');
            $users = $this->db->select('id')
                                            ->from('users')
                                            ->get()->result_array();
            
            foreach($users as $user){
                $this->Closet_model->create_if_not_exit($user['id']);        
                echo '<p>create closet for user_id: ' . $user['id'] . '</p>';
            }
        }
        catch(Exception $e){
            show_error($e->getMessage());
        }      
    }
    
    function fill_in_brands(){
        $rows = $this->db->distinct()
                                        ->select('product_brand')
                                        ->from('product_clothing')
                                        ->get()->result_array();
        $this->load->model('Brand_model');
        $counter = 0;
        foreach($rows as $row){
            if (!$this->Brand_model->insertIfNotExist(['name'=>$row['product_brand']])){
                continue;
            }
            $counter ++;
        }
        echo 'insert ' . $counter . ' rows';       
    }
  
}
?>
