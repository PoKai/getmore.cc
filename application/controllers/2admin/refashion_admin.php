<?php

class Refashion_admin extends Admin_Controller {

    public function __construct(){
        parent::__construct();
        $this->template->set_layout("template/admin", []);
        $this->load->model('Refashion_model');
    }

    function index() {
        $posts = $this->Refashion_model->get_all();
        $this->template->render('/2admin/refashion/index', ['posts'=>$posts]);
    }


    
}
