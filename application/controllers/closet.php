<?php
if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Closet extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library(array('ion_auth', 'user_agent', 'lib_sellorder', 'session'));
		$this->load->database();
		$this->load->model('Closet_model');
		$this->load->model('Closet_demo_model');
		$this->load->model('Demo_like_model');
		$this->load->model('Product_clothing_model');
		$this->load->model('Marketplace_model');
		$this->load->model('Brand_model');
		$this->load->model('Free_style');
		$this->load->model('User_model');
		$this->load->model('Orderlist_model');
		$this->load->helper(array('sellorder_helper'));
		$this->load->spark('curl');
		$this->load->spark('Twiggy/0.8.5');
		$this->twiggy->theme('');

		$this->twiggy->set('page_url', $this->uri->segment(2));

		$this->twiggy->set(array(
			'lang' => 'zh-tw',
			'meta_charset' => 'UTF-8',
			'site_title' => 'Getmore - 新銳設計與時尚二手線上市集/亞洲新銳/二手精品/二手包',
			'site_description' => 'Getmore 是為時尚愛好者以及設計師打造的時尚平台，我們的使命是挖掘世界各地的時尚愛好者，以及獨樹一格的時裝設計師，打造一流的網路平台以及服務，讓消費者可以盡情的在 Getmore 選購新銳設計師新品，以及國際品牌二手古著單品。',
			'site_keywords' => '時尚,衣著,服飾,時裝,二手衣,流行,新銳設計師,亞洲時尚,褲子,買二手衣,賣二手衣,環保,電子商務,startup,二次時尚',
			'logined' => $this->ion_auth->logged_in(),
		), NULL, TRUE);
	}

	/*
	 * user enter in this page. create one closet for user if it didn't exist.
	 * @author howtomakeaturn
	 */
	public function my($to = 'tabs') {
		if (!$this->ion_auth->logged_in()) {
			$this->system->redirect();
		}

		$user_id = $this->session->userdata('user_id');
		$closet = $this->Closet_model->create_if_not_exit($user_id);
		redirect('/closet/' . $to . '/' . $closet['url_code'], 'refresh');
	}
	/*
	upload a product for sell
	 */
	public function manage() {
		return redirect('/marketplace/manage', 'refresh');
	}

	public function disfollow($url_code) {
		$user_id = $url_code - 9881;

		if (!$this->ion_auth->logged_in()) {
			return redirect('/auth/login', 'refresh');
		}

		$this->Closet_model->dis_follow($user_id);

		redirect($this->agent->referrer(), 'refresh');
	}

	public function follow($url_code) {
		$user_id = $url_code - 9881;

		if (!$this->ion_auth->logged_in()) {
			return redirect('/auth/login', 'refresh');
		}

		$this->Closet_model->to_follow($user_id);

		redirect($this->agent->referrer(), 'refresh');
	}

	/*
	 * render the closet page according to the url_code
	 * @author howtomakeaturn
	 */
	public function update_information() {
		$this->load->model('Clothing_model');
		$user_id = $_POST['user_id'];
		$hash = $_POST['current_hash'];
		$closet = $this->_get_closet($user_id);
		$query = array('user_id' => $closet['user_id']);
		if (!$closet['is_owner']) {
			$query = array_merge($query, array('lock_status' => [0, 1, 2]));
		}
		$url = '/restful/';
		$items_html = '';
		$advertise_message = '';
		$is_empty = 0;
		switch ($hash) {
		case '#view':
			$url .= 'product_clothing';
			$items = (array) json_decode($this->curl->simple_get(base_url($url), $query, [
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			]));
			$empty_msg = "<div class='alert alert-info'>這個衣櫃內還沒有衣服喔！</div><div class='alert alert-info zero-select'>這個選項中還沒有衣服喔！</div>";
			foreach ($items as $item) {
				$items_html .= _new_clothing_thumbnail_html($item, 'product_clothing');
			}
			if ($items_html == '') {
				$is_empty = 1;
			}

			break;
		case '#marketplace':
			$url .= 'marketplace';
			$items = (array) json_decode($this->curl->simple_get(base_url($url), $query, [
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			]));
			$empty_msg = "<div class='alert alert-info'>這個衣櫃內還沒有衣服喔！</div><div class='alert alert-info zero-select'>這個選項中還沒有衣服喔！</div>";
			foreach ($items as $item) {
				$items_html .= _new_clothing_thumbnail_html($item, 'marketplace');
			}
			if ($items_html == '') {
				$is_empty = 1;
			}

			break;
		case '#my_favorite':
			$liked_clothings = $this->Closet_model->get_favorites($user_id);
			//echo json_encode($liked_clothings);return false;
			foreach ($liked_clothings as $type => $type_items) {
				foreach ($type_items as $item => $value) {
//$tmp=(object)$value;
					$items_html .= _new_clothing_thumbnail_html((object) $value, $type);
				}
			}
			if ($items_html == '') {
				$is_empty = 1;
			}

			$empty_msg = "<div class='alert alert-info'>欲望清單內還沒有衣服喔！</div>";
			//echo json_encode($items_html);return false;
			break;
		case '#advertise':
			$advertise_message = alert_message($this->session->userdata('message'), 'info');
			$this->session->unset_userdata('message');
			//$alert_message=alert_message($advertise_message, 'info');
			//var_dump($advertise_message);exit(0);
			break;
		case '#followlist':
			$follow_users = $this->Closet_model->get_followlist('users');
			$follow_designers = (array) json_decode($this->curl->simple_get('/restful/follow_designer/' . $this->session->userdata('user_id'), array(), array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));
			foreach ($follow_designers as $designer) {
				$tmp = (array) json_decode($this->curl->simple_get('/restful/designer/id/' . $designer->followed, array(), array(
					CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
				)));
				$designer->info = $tmp;
				//var_dump($tmp);
			} //var_dump($follow_designers);exit(0);
			$followlist_html = _followlist_html($follow_designers, $follow_users);

			break;
		case '#history':
			$url .= 'buy_history';
			$history = '';
			$items = (array) json_decode($this->curl->simple_get('/restful/buy_history', array(), array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));
			$empty_msg = "<div class='alert alert-info zero-select'>您還沒有購買紀錄喔！</div>";
			if ($history == '') {
				$is_empty = 1;
			}

			$history = json_encode($items);
			break;
		case '#rating_history':
			$rating_history = '';
			$items = (array) json_decode($this->curl->simple_get('/restful/buy_history/rating_history', array(), array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));
			$empty_msg = "<div class='alert alert-info zero-select'>您還沒有購買紀錄喔！</div>";
			if ($rating_history == '') {
				$is_empty = 1;
			}

			$rating_history = json_encode($items);
		case '#rated_history':
			$rated_history = '';
			$items = (array) json_decode($this->curl->simple_get('/restful/buy_history/rated_history', array(), array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));
			$empty_msg = "<div class='alert alert-info zero-select'>您還沒有購買紀錄喔！</div>";
			if ($rating_history == '') {
				$is_empty = 1;
			}

			$rated_history = json_encode($items);
		default:
			# code...
			break;
		}

		$co2_amount = $this->_calculate_co2_amount($items);
		$data = array(
			'co2_amount' => $co2_amount,
			'items_html' => $items_html,
			'is_empty' => $is_empty,
			'empty_msg' => $empty_msg,
			'advertise_message' => $advertise_message,
			'followlist_html' => $followlist_html,
			'history' => $history,
			'rating_history' => $rating_history,
			'rated_history' => $rated_history
		);
		echo json_encode($data);
	}
	public function tabs($url_code = "") {
		$user_id = $this->Closet_model->get_user_id_by_url_code($url_code);
		$closet = $this->_get_closet($user_id); //var_dump($closet['is_owner']);exit(0);

		if (!$closet) {
			redirect('/');
		}

		$query = array(
			'user_id' => $closet['user_id'],
		);

		if (!$closet['is_owner']) {
			$query = array_merge($query, array(
				'lock_status' => [0, 1, 2],
			));
		}

		$items = (array) json_decode($this->curl->simple_get(base_url('/restful/product_clothing'), $query));

		$data['closet'] = $closet;
		$data['items'] = $items; //var_dump($data['items']);exit(0);
		$data['co2_amount'] = $this->_calculate_co2_amount($items);
		$data['is_owner'] = $closet['is_owner'];
		$data['session'] = $this->session->userdata;
		//$data['uri']=array($this->uri->segment(1), $this->uri->segment(2));

		//$this->template->render('closet/view', $data);
		$this->twiggy->set($data);
		$this->twiggy->register_function('clothing_image_url');
		$this->twiggy->register_function('post_user_message_modal_for_layout_without_link_bootstrap');
		$this->twiggy->register_function('_calculate_co2_amount');
		$this->twiggy->template('closet/tabs')->display();

		if (!$closet['is_owner']) {
			$this->Closet_model->increase_view_count($url_code);
		}

	}

	public function view($url_code = "") {
		$user_id = $this->Closet_model->get_user_id_by_url_code($url_code);
		$closet = $this->_get_closet($user_id);

		if (!$closet) {
			redirect('/');
		}

		$query = array(
			'user_id' => $closet['user_id'],
		);

		if (!$closet['is_owner']) {
			$query = array_merge($query, array(
				'lock_status' => [0, 1, 2],
			));
		}

		$items = (array) json_decode($this->curl->simple_get(base_url('/restful/product_clothing'), $query));

		$data['closet'] = $closet;
		$data['items'] = $items; //var_dump($data['items']);exit(0);
		$data['co2_amount'] = $this->_calculate_co2_amount($items);
		$data['is_owner'] = $closet['is_owner'];
		$data['uri'] = array($this->uri->segment(1), $this->uri->segment(2));
		$data['user_id'] = $this->session->userdata('user_id');
		//$this->template->render('closet/view', $data);
		$this->twiggy->set($data);
		$this->twiggy->register_function('clothing_image_url');
		$this->twiggy->register_function('post_user_message_modal_for_layout_without_link_bootstrap');
		$this->twiggy->template('closet/view')->display();

		if (!$closet['is_owner']) {
			$this->Closet_model->increase_view_count($url_code);
		}
	}

	public function marketplace($url_code = "") {
		$user_id = $this->Closet_model->get_user_id_by_url_code($url_code);
		$closet = $this->_get_closet($user_id);

		if (!$closet) {
			redirect('/');
		}

		$query = array(
			'user_id' => $closet['user_id'],
		);

		if (!$closet['is_owner']) {
			$query = array_merge($query, array(
				'lock_status' => [0, 1, 2],
			));
		}

		$items = (array) json_decode($this->curl->simple_get(base_url('/restful/marketplace'), $query));

		$data['closet'] = $closet;
		$data['items'] = $items;
		$data['co2_amount'] = $this->_calculate_co2_amount($items);
		$data['is_owner'] = $closet['is_owner'];
		$data['uri'] = array($this->uri->segment(1), $this->uri->segment(2));
		//var_dump($data['uri']);
		//exit(0);

		//$this->template->render('closet/view', $data);
		$this->twiggy->set($data);
		$this->twiggy->register_function('clothing_image_url');
		$this->twiggy->register_function('post_user_message_modal_for_layout_without_link_bootstrap');
		$this->twiggy->template('closet/view')->display();

		if (!$closet['is_owner']) {
			$this->Closet_model->increase_view_count($url_code);
		}
	}

	/*function like() {

		// only handle POST request
		if ($this->input->server('REQUEST_METHOD') === 'GET') {
			redirect('/', 'refresh');
		}

		$demo_id = $this->input->post('demo_id');
		$user_id = $this->session->userdata('user_id');

		// only handle valid request
		if (empty($demo_id) || empty($user_id)) {
			redirect('/', 'refresh');
		}

		$this->Demo_like_model->create($demo_id, $user_id);

		// redirect to previous page
		//$this->load->library('user_agent');
		//redirect($this->agent->referrer(), 'refresh');

	}*/

	public function like() {
		if (empty($this->session->userdata('user_id'))) {
			redirect('/', 'refresh');
		} else {
			$this->twiggy->set([
				'price_range' => json_decode($this->curl->simple_get('/restful/combine/all_clothings/min_and_max')),
				'season_list' => $this->season_list,
				'style_list' => array_reduce($this->config->item('style_lists')[1], function ($result, $style_list) {
					$result += $style_list;
					return $result;
				}, []),
				'gender_list' => $this->gender_list,
				'group_list' => $this->group_list['3'],
				'product_type_list' => $this->product_type_list['3'],
				'patterns' => $this->patterns['3'],
				'sub_patterns' => $this->sub_patterns,
				'logined' => $this->ion_auth->logged_in(),
				'user_id' => $this->session->userdata('user_id')
			])->template('closet/like')->display();
		}
	}

	function change_pic() {

		// only handle POST request
		if ($this->input->server('REQUEST_METHOD') === 'GET') {
			redirect('/', 'refresh');
		}

		$file_id = $this->input->post('file_id');
		$user_id = $this->session->userdata('user_id');
		$photo_type = $this->input->post('photo_type');

		// only handle valid request
		if (empty($file_id) || empty($user_id) || empty($photo_type)) {
			redirect('/', 'refresh');
		}

		$this->Closet_model->change_pic($photo_type, $user_id, $file_id);

		// redirect to previous page
		$this->load->library('user_agent');
		redirect($this->agent->referrer(), 'refresh');
	}

	function change_text() {

		// only handle POST request
		if ($this->input->server('REQUEST_METHOD') === 'GET') {
			redirect('/', 'refresh');
		}

		$text_type = $this->input->post('type');
		$user_id = $this->session->userdata('user_id');
		$content = $this->input->post('content');

		// only handle valid request
		if (empty($text_type) || empty($user_id) || empty($content)) {
			redirect('/', 'refresh');
		}

		$this->Closet_model->change_text($text_type, $user_id, $content);

		// redirect to previous page
		$this->load->library('user_agent');
		redirect($this->agent->referrer(), 'refresh');
	}

	function update_demo() {

		// only handle POST request
		if ($this->input->server('REQUEST_METHOD') === 'GET') {
			redirect('/', 'refresh');
		}

		$description = $this->input->post('description');
		$demo_id = $this->input->post('demo_id');
		$user_id = $this->session->userdata('user_id');
		$file_id = $this->input->post('file_id');

		// only handle valid request
		if (empty($user_id) || empty($demo_id)) {
			redirect('/', 'refresh');
		}
		if (!empty($description)) {
			$this->Closet_demo_model->change_text($user_id, $demo_id, $description);
		}

		if (!empty($file_id)) {
			$this->Closet_demo_model->change_pic($user_id, $demo_id, $file_id);
		}

		// redirect to previous page
		$this->load->library('user_agent');
		redirect($this->agent->referrer(), 'refresh');
	}

	function delete_demo() {

		// only handle POST request
		if ($this->input->server('REQUEST_METHOD') === 'GET') {
			redirect('/', 'refresh');
		}

		$demo_id = $this->input->post('demo_id');
		$user_id = $this->session->userdata('user_id');

		// only handle valid request
		if (empty($user_id) || empty($demo_id)) {
			redirect('/', 'refresh');
		}

		$this->Closet_demo_model->delete_with_validation($demo_id, $user_id);

		// redirect to previous page
		$this->load->library('user_agent');
		redirect($this->agent->referrer(), 'refresh');
	}

	function create_closet_demo() {

		// only handle POST request
		if ($this->input->server('REQUEST_METHOD') === 'GET') {
			redirect('/', 'refresh');
		}

		$file_id = $this->input->post('file_id');
		$user_id = $this->session->userdata('user_id');
		$clothing_id = $this->input->post('clothing_id');
		$description = $this->input->post('description');
		$closet_id = $this->input->post('closet_id');

		// only handle valid request
		if (empty($file_id) || empty($user_id) || empty($clothing_id) || empty($closet_id)) {
			redirect('/', 'refresh');
		}

		$this->db->from('closet_demos');
		$this->db->where('user_id', $user_id);

		$query = $this->db->get();
		$rows = $query->result_array();

		// users can only have 30 demos
		if (count($rows) >= 30) {
			$data['title'] = "超過上限";
			$data['message'] = "每位使用者只能上傳30張照片到SHOW ROOM喔！";
			$this->template->render('sellorder/info/404', $data);
			return false;
		}

		$this->Closet_demo_model->create($closet_id, $clothing_id, $file_id, $user_id, $description);

		// redirect to previous page
		$this->load->library('user_agent');
		redirect($this->agent->referrer(), 'refresh');
	}

	function my_favorite() {

		// this is AOP technique
		$this->redirect_if_not_login();

		$user_id = $this->session->userdata('user_id');

		$closet = $this->_get_closet($user_id);

		redirect('/closet/tabs/' . $closet['url_code'] . '#my_favorite', 'refresh');
	}

	public function postslist() {
		$this->redirect_if_not_login();
		$user_id = $this->session->userdata('user_id');

		$closet = $this->_get_closet($user_id);
		$this->load->model('Post_model');
		$postslist = $this->Post_model->get_posts_by_userId($user_id);
		$is_owner = false;
		if ($closet['user_id'] === $this->session->userdata('user_id')) {
			$is_owner = true;
		}
		$data['closet'] = $closet;
		$data['postslist'] = $postslist;
		$data['is_owner'] = $is_owner;

		//$this->template->render('closet/postslist', $data);
		$this->twiggy->set($data);
		$this->twiggy->template('closet/postslist')->display();
	}

	public function followlist() {
		$this->redirect_if_not_login();

		$user_id = $this->session->userdata('user_id');

		$closet = $this->_get_closet($user_id);

		$followlist = $this->Closet_model->get_followlist();

		$data['closet'] = $closet;
		$data['followlist'] = $followlist;
		$data['is_owner'] = $closet['is_owner'];

		$items = $this->Product_clothing_model->get_many_by_user_id($closet['user_id']);
		$data['co2_amount'] = $this->_calculate_co2_amount($items);

		$this->template->render('closet/followlist', $data);
	}

	public function post($url_code = "") {
		/*todo transfer url_code to user_id*/
		$user_id = $this->Closet_model->get_user_id_by_url_code($url_code);
		$closet = $this->_get_closet($user_id);

		$this->load->model('Post_model');
		if (!$closet) {
			redirect('/');
		}
		$data['closet'] = $closet;
		$data['postslist'] = $this->Post_model->get_posts_by_urlCode($url_code);
		$data['has_login'] = $this->ion_auth->logged_in();
		//$this->template->render('closet/post', $data);
		$this->twiggy->set($data);
		$this->twiggy->template('closet/post')->display();
	}

	function advertise() {

		// this is AOP technique
		$this->redirect_if_not_login();

		$user_id = $this->session->userdata('user_id');

		$closet = $this->_get_closet($user_id);

		$items = $this->Product_clothing_model->get_many_by_user_id($closet['user_id']);
		$data['co2_amount'] = $this->_calculate_co2_amount($items);

		$data['is_owner'] = $closet['is_owner'];
		$data['closet'] = $closet;
		$data['message'] = $this->session->flashdata('message');
		$this->template->render('closet/advertise', $data);
	}

	function invite_friends() {
		$this->load->library('Getmore/Services/MailSender');
		$this->load->helper('typography');
		$data = ['receiver' => $this->input->post('receiver'), 'title' => $this->input->post('title'), 'sender' => $this->input->post('sender'), 'content' => $this->input->post('content'), 'user_id' => $this->session->userdata('user_id')];
		$this->mailsender->advertise_to_friends($data);
		$this->session->set_flashdata('message', '成功送出郵件。');
		redirect('/closet/advertise');
	}

	private function _calculate_co2_amount($items) {
		$co2_amount = array_reduce($items, function ($carry, $item) {
			$item = (array) $item;
			$carry += $item['lock_status'] === 2 ? 2 : 1;
			return $carry;
		}, 0) * 1.8;
		return $co2_amount ? money_format('%i kg', $co2_amount) : '0 kg';
	}

	private function _get_closet($user_id) {
		$closet = $this->Closet_model->get_by_user_id($user_id);
		if (empty($closet['id'])) {
			return false;
		}
		$closet['follower_count'] = $this->Closet_model->get_follower_count($closet['user_id']);
		$closet['follow_count'] = $this->Closet_model->get_follow_count($closet['user_id']);
		if ($closet['user_id'] === $this->session->userdata('user_id')) {
			$closet['is_owner'] = true;
		} else {
			$closet['had_followed'] = $this->Closet_model->had_followed($closet['user_id']);
		}
		$this->load->library('typography');
		$closet['description_tag'] = $this->typography->auto_typography($closet['description']);

		return $closet;
	}
}
