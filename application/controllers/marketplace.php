<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Marketplace extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library(array('ion_auth', 'session', 'user_agent'));
		if (!$this->ion_auth->logged_in()) {
			$this->system->redirect();
		}
		$this->load->spark('curl');
		$this->load->model(array('Product_clothing_model', 'Marketplace_model', 'Free_style', 'Closet_model', 'User_model', 'Orderlist_model'));
		$this->load->database();

		$this->load->spark('Twiggy/0.8.5');
		$this->twiggy->theme('');

		$this->twiggy->set('page_url', $this->uri->segment(2));
		$this->twiggy->set(array(
			'lang' => 'zh-tw',
			'meta_charset' => 'UTF-8',
			'site_title' => 'Getmore - 二手衣時尚購物網站/綠色時尚/買二手衣/賣二手衣',
			'site_description' => 'GetMore 是亞洲第一個二手衣時尚買賣網站，提供穿二手衣的生活新理念，輕鬆享受二手衣獨特、環保、時尚的二次生活。',
			'site_keywords' => '時尚,衣著,服飾,時裝,流行,褲子,買二手衣,賣二手衣,環保,電子商務,startup,二次時尚',
			'logined' => $this->ion_auth->logged_in(),
		), NULL, TRUE);
	}
	/*
	upload a product for sell
	 */
	public function manage() {
		if (!$this->ion_auth->is_seller()) {
			$this->system->redirect();
		}

		switch ($this->session->userdata('isplanAverify')) {
		case 0:
			return $this->twiggy->template('marketplace/account_activated')->display();
		case -1:
			$this->send_sms_code($mobile_phone);
			return $this->_verify_mobile_phone($this->session->userdata('mobile_phone'));
		}

		$query = $this->Product_clothing_model->get_brand_list();
		$data = array_merge($query[1], $query[2], $query[3], $query[4], $query[5], $query[6], $query[7], $query[8]);
		$query2 = $this->Free_style->get_free_style();

		$free_styles = [];
		foreach ($query2 as $style) {
			$free_styles[] = $style->name;
		}

		$this->twiggy->set('styles', $free_styles);
		$this->twiggy->set('brands', $data);
		$trade_rule = (array) json_decode($this->curl->simple_get('/restful/trade', array(), array(
			CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
		)));
		$this->twiggy->set('trade_rule', $trade_rule);
		$this->twiggy->template('marketplace/launch')->display();
	}

	/*
	view and modify settings of closet of current seller
	 */
	public function closet_settings() {
		if (($closet = $this->input->post('closet')) && ($user = $this->input->post('user'))) {
			$files = array();

			foreach ($_FILES as $type => $file) {
				foreach ($file['tmp_name'] as $key => $tmp_file) {
					if (empty($tmp_file)) {
						continue;
					}
					if (empty($files[$type])) {
						$files[$type] = array();
					}
					$files[$type][$key] = new CURLFile($tmp_file);
				}
			}

			if (empty($files)) {
				$this->Closet_model->closet_update($closet);
			} else {
				$this->Closet_model->closet_update(array_merge($closet, $files['closet']));
			}
			$this->User_model->user_update($user);
		}

		$closet = $this->Closet_model->get_closet_settings();
		$user = $this->User_model->get_user();

		$this->twiggy->template('marketplace/settings')->set(array(
			'closet' => $closet,
			'user' => $user,
			'page_url' => 'closet_settings',
		))->display();
	}
	/*
	view and modify information of invoice of current seller
	 */
	public function invoice_info() {
		$this->load->model('Invoice_model');
		if ($data = $this->input->post()) {
			$this->Invoice_model->invoice_update($data);
			$invoice = $data;
		} else {
			$invoice = $this->Invoice_model->get_invoice_info();
		}

		$this->twiggy->template('marketplace/invoice_info')->set(array(
			'invoice' => $invoice,
			'page_url' => 'invoice_info',
		))->display();
	}
	/*
	view and modify information of account of bank of current seller
	 */
	public function bank_info() {
		$this->load->model('Bank_model');
		$bank = $this->Bank_model->get_bank_info();
		$this->twiggy->template('marketplace/bank_info')->set(array(
			'bank' => $bank,
			'page_url' => 'bank_info',
		))->display();
	}
	/*
	activated account of current user
	 */
	public function account_activated() {
		if ($this->input->post()) {
			$this->load->model('Bank_model');
			$this->load->model('Invoice_model');
			$files = array();

			foreach ($_FILES as $type => $file) {
				foreach ($file['tmp_name'] as $key => $tmp_file) {
					if (empty($tmp_file)) {
						continue;
					}
					if (empty($files[$type])) {
						$files[$type] = array();
					}
					$files[$type][$key] = new CURLFile($tmp_file);
				}
			}

			if (!$this->Closet_model->closet_update(array_merge($this->input->post('closet'), $files['closet']))) {
				redirect($this->agent->referrer(), 'refresh');
			}

			call_user_func(function ($user) {
				$user['birthday'] = $user['birthday']['year'] . '-' . $user['birthday']['month'] . '-' . $user['birthday']['day'];
				if (!$this->User_model->user_update($user)) {
					redirect($this->agent->referrer(), 'refresh');
				}
			}, $this->input->post('user'));

			call_user_func_array(function ($bank, $file) {
				list($code, $title) = explode('-', $bank['code']);
				$bank['code'] = $code;
				$bank['title'] = $title;
				if (!$this->Bank_model->bank_activate(array_merge($bank, $file))) {
					redirect($this->agent->referrer(), 'refresh');
				}
			}, array($this->input->post('bank'), $files['bank']));

			if (!$this->Invoice_model->invoice_activate($this->input->post('invoice'))) {
				redirect($this->agent->referrer(), 'refresh');
			}

			$this->User_model->update(array('verify_planA' => -1));
			$this->session->set_userdata('isplanAverify', -1);

			$this->_verify_mobile_phone($this->input->post('user')['mobile_phone']);
		} else {
			switch ($this->session->userdata('isplanAverify')) {
			case 0:
				return $this->twiggy->template('marketplace/account_activated')->display();
			case -1:
				return $this->_verify_mobile_phone($this->session->userdata('mobile_phone'));
			}
		}
	}
	/*
	check sms code that send to mobile phone before
	 */
	public function check_sms_code() {
		$this->load->model('Trade_rule_model');
		$this->config->load('trade_rule');

		$sms_code = $this->input->post('sms_code');
		$msg = '';
		if (($this->session->userdata("sms_code") === $sms_code) && ((time() - $this->session->userdata("sms_timestamp")) < 120)) {
			$this->User_model->update(array('verify_planA' => 1));
			$this->session->set_userdata('isplanAverify', 1);
			$this->Trade_rule_model->create_default($this->config->item('MarketPlace'));
			redirect('/marketplace/manage', 'refresh');
		} else if ($this->session->userdata("sms_code") !== $sms_code) {
			if ((time() - $this->session->userdata("sms_timestamp")) >= 120) {
				$msg .= '上次發送的驗證碼已逾時, 請按重新取得認證碼';
			} else {
				$msg = '驗證碼錯誤, 請重新輸入';
			}
		} else {
			$msg = '此驗證碼已經逾時, 請按重新取得認證碼';
		}

		$this->_verify_mobile_phone($this->session->userdata('mobile_phone'), $msg);
	}

	private function _verify_mobile_phone($mobile_phone, $msg = null) {

		$this->twiggy->template('verify/sms_code')->set(array('msg' => $msg, 'mobile_phone' => $mobile_phone))->display();
	}

	public function send_sms_code($mobile_phone = null) {
		if (!$mobile_phone) {
			$mobile_phone = $this->input->post('mobile_phone') ? $this->input->post('mobile_phone') : $this->session->userdata("mobile_phone");
		}

		if ((time() - $this->session->userdata("sms_timestamp")) >= 120) {
			$sms_code = $this->User_model->generate_sms_code();
			$this->session->set_userdata("mobile_phone", $mobile_phone);
			$this->session->set_userdata("sms_code", $sms_code);
			$this->session->set_userdata("sms_timestamp", time());
			$this->system->send_sms_msg($mobile_phone, "您的GetMore驗證碼是:" . $sms_code);
			if ($this->input->is_ajax_request()) {
				echo '已重新發送認證碼';
			} else {
				return true;
			}
		} else {
			if ($this->input->is_ajax_request()) {
				echo '離上次發送驗證碼需間隔兩分鐘';
			} else {
				return false;
			}
		}
	}

	public function launch_product() {

		if (!$this->ion_auth->logged_in()) {
			$this->system->redirect();
		}

		if ($this->input->server('REQUEST_METHOD') === 'GET') {
			redirect('marketplace/manage', 'refresh');
		}

		$default = array("added_user_id" => $this->session->userdata('user_id'), "product_id" => 0, "product_original_price" => '', "product_sell_price" => '', "suggested_price" => 0, "reduction_again" => 0, "product_title" => '', "product_brand" => '', "product_type" => '', "product_comments" => '', "color" => '', "season" => '', "style" => '', "pattern" => '', "item_long" => 'ss', "files" => '', "size" => '', "item_status" => '', "material" => '', "location" => '', "measure" => '{"1":"","2":"","3":"","4":"","5":"","6":"","7":"","8":"","9":"","10":""}', "descript" => '', "branded" => 0, "add_time" => time(), "edit_time" => time(), "buy_order_id" => 0, "lock_status" => 0, "last_locked_time" => '0000-00-00 00:00:00', "log" => '', "publication_time" => date('Y-m-d H:i:s'), "on_shelf" => 1, "size_comment" => '', "size_type_major" => '', "size_standard" => '', "size_type_minor" => '', "size_number_minor" => '', "admin_comment" => '', "admin_remark" => '', "level" => '', "launch_way" => 1, "user_id" => $this->session->userdata('user_id'));

		$post = $this->input->post();

		$post['free_style'] = json_decode($this->Free_style->post_free_style($post['free_style']));
		$free_style_ids = array_map($post['free_style'], function ($free_style) {
			return $free_style->id;
		}, $post['free_style']);
		$post['free_style'] = implode(",", $free_style_ids);

		$post['product_type'] = $post['category2'];
		$post['measure'] = json_encode($post['measure']);

		if ($post['style']) {
			$post['style'] = json_encode($post['style']);
		}

		$post['files'] = json_encode($post['files']);

		unset($post['category2']);

		$product = array_merge($default, $post);

		$this->Marketplace_model->add_clothing($product);
		redirect($this->agent->referrer() . "#success", 'refresh');
	}

	public function in_business() {
		if (!$this->ion_auth->logged_in()) {
			$this->system->redirect();
		}

		if (!$this->ion_auth->is_seller()) {
			$this->system->redirect();
		}

		switch ($this->session->userdata('isplanAverify')) {
		case 0:
			return $this->twiggy->template('marketplace/account_activated')->display();
		case -1:
			return $this->_verify_mobile_phone($this->session->userdata('mobile_phone'));
		}

		$clothings = $this->Marketplace_model->get_by_status([1, 2]);
		$query = $this->Free_style->get_free_style();
		$free_styles = [];
		foreach ($query as $style) {
			$free_styles[$style->id] = $style->name;
		}

		$query2 = $this->Product_clothing_model->get_brand_list();
		$data = array_merge($query2[1], $query2[2], $query2[3], $query2[4], $query2[5], $query2[6], $query2[7], $query2[8]);

		$this->load->config('clothing_class');
		$this->twiggy->set('styles', $this->config->item('style_name'));
		$this->twiggy->set('free_styles', $free_styles);
		$this->twiggy->set('brands', $data);
		$this->twiggy->set('sell_list', $clothings);

		$this->twiggy->template('marketplace/in_business')->display();
	}

	/*
	manage products of current seller
	 */
	public function sell_clothing_management() {

		if (!$this->ion_auth->logged_in()) {
			$this->system->redirect();
		}

		if (!$this->ion_auth->is_seller()) {
			$this->system->redirect();
		}

		switch ($this->session->userdata('isplanAverify')) {
		case 0:
			return $this->twiggy->template('marketplace/account_activated')->display();
		case -1:
			return $this->_verify_mobile_phone($this->session->userdata('mobile_phone'));
		}

		$clothings = $this->Marketplace_model->get_by_status(0);
		$query = $this->Free_style->get_free_style();
		$free_styles = [];
		foreach ($query as $style) {
			$free_styles[$style->id] = $style->name;
		}

		$query2 = $this->Product_clothing_model->get_brand_list();
		$data = array_merge($query2[1], $query2[2], $query2[3], $query2[4], $query2[5], $query2[6], $query2[7], $query2[8]);

		$this->load->config('clothing_class');
		$this->twiggy->set('styles', $this->config->item('style_name'));
		$this->twiggy->set('free_styles', $free_styles);
		$this->twiggy->set('brands', $data);
		$this->twiggy->set('sell_list', $clothings);

		$this->twiggy->template('marketplace/manageinfo')->display();
	}
	/*
	view and modify trade rule of current seller
	 */
	public function trade_rule() {

		switch ($this->session->userdata('isplanAverify')) {
		case 0:
			return $this->twiggy->template('marketplace/account_activated')->display();
		case -1:
			return $this->_verify_mobile_phone($this->session->userdata('mobile_phone'));
		}

		$data['page_url'] = 'trade_rule';

		if ($post = $this->input->post()) {
			$trade_rule = (array) json_decode($this->curl->simple_post('/restful/trade', $post['trade'], array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));

			$data['trade'] = $trade_rule;
		} else {
			$data['trade'] = (array) json_decode($this->curl->simple_get('/restful/trade', array(), array(
				CURLOPT_COOKIE => http_build_query($_COOKIE, '', ';'),
			)));
		}

		$this->twiggy->template('marketplace/trade_rule')->set($data)->display();
	}

	public function orderlist($status) {

		if (!$this->ion_auth->logged_in()) {
			$this->system->redirect();
		}

		if (!$this->ion_auth->is_seller()) {
			$this->system->redirect();
		}

		$query = $this->Orderlist_model->sub_order($status);
		//var_dump($query);
		$this->twiggy->set('sub_order_lists', $query);
		$orderlist_url = $this->uri->segment(3);

		$this->twiggy->set('orderlist_url', $orderlist_url);
		$this->twiggy->template('marketplace/orderlist')->display();
	}

}
