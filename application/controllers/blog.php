<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends MY_Controller{

    protected $data;

    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('Getmore/Article/ArticleDataMapper', '', 'article_dm');
        $this->load->library('Getmore/Article/ArticleRepository', '', 'article_rp');
        $this->load->model('Article_model');
        $categories = $this->db->from('article_categories')
                                                  ->get()->result_array();
        $this->data['categories'] = $categories;        
    }
    
    function index(){
        redirect('/blog/page');
    }

    function page($page = 1){
        $this->data['articles'] = $this->article_rp->getAllPublishedPaginated($page);

        $this->load->library('pagination');
        $config['use_page_numbers'] = TRUE;
        $config['base_url'] = '/blog/page/';
        $config['total_rows'] = $this->article_rp->countPublished();
        $config['per_page'] = ArticleRepository::PER_PAGE_NUM;
    
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';  
          
        $config['num_tag_open'] = '<li>';        
        $config['num_tag_close'] = '</li>';
        
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';        
        $config['first_tag_close'] = '</li>';
            
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';        
        $config['last_tag_close'] = '</li>';
        
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 

        $this->data['pagination'] = $this->pagination->create_links();               
        
        
        $this->template->render('/blog/index', $this->data);
    }

    function category($category_id, $page = 1){
        
        if ($category_id){      
            $this->data['articles'] = $this->article_rp->getAllPublishedByCategoryIdPaginated($category_id, $page);

            $this->load->library('pagination');
            $config['use_page_numbers'] = TRUE;
            $config['base_url'] = '/blog/category/' . $category_id . '/';
            $config['total_rows'] = $this->article_rp->countPublishedByCategoryId($category_id);
            $config['per_page'] = ArticleRepository::PER_PAGE_NUM;
            $config['uri_segment'] = 4;    
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';  
              
            $config['num_tag_open'] = '<li>';        
            $config['num_tag_close'] = '</li>';
            
            $config['first_link'] = 'First';
            $config['first_tag_open'] = '<li>';        
            $config['first_tag_close'] = '</li>';
                
            $config['last_link'] = 'Last';
            $config['last_tag_open'] = '<li>';        
            $config['last_tag_close'] = '</li>';
            
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            
            $this->pagination->initialize($config); 

            $this->data['pagination'] = $this->pagination->create_links();               

            $this->template->render('/blog/index', $this->data);
        }
        else{
            $this->template->render('/blog/category_list', $this->data);        
        }
    }

    
    function view($id){
        /*
         * So here we have data mapper, repository, and CI built-in model.
         * ;) 
         */
      
#        $this->data['article'] = $this->Article_model->get($id);      
        $this->data['article'] = $this->article_rp->get($id);      
        $this->data['recent_articles'] = $this->article_rp->getRecentArticles();      
        
        $this->template->render('/blog/view', $this->data);      
    }
    
}
