<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sell extends MY_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->system->redirect();
        }        
        $this->load->model('Order_model');
        $this->load->model('Product_model');        
        $this->load->model('Account_model');
        $this->load->config('payment');

        $this->load->library('zend');
        $this->zend->load('Zend/Barcode');
        $this->load->helper('url');
        $this->load->model('User_model');        
        if ( !$this->User_model->is_verified($this->session->userdata('user_id')) ){
            redirect('/verify/index', 'refresh');
        }

    }
    
    public function coupon_code(){
        $code = $this->input->post('coupon_code');
        $items_price = $this->input->post('items_price');
        $this->load->model('Sellcoupon_model', 'coupon');
        try{
            $this->coupon->validate_coupon_code($code);
            $this->coupon->validate_coupon_owner($code, $this->session->userdata('user_id'));
            
            $coupon = $this->coupon->as_array()->get_by('code', $code);
            
            if ($coupon['minimum_charge'] > $items_price){
                throw new Exception('not satisfy minimum charge', 4);                              
            }
            
            $response = array('status' => 'success');
            $response['coupon'] = $coupon;
            echo json_encode($response);
        }
        catch(Exception $e){        
            $response = array( 'status' => 'error' );
            $error_code = $e->getCode();
            if ($error_code==1){
                $response['message'] = '無此禮券';
            }
            else if ($error_code==2){
                $response['message'] = '禮券已過期';
            }
            else if ($error_code==3){
                $response['message'] = '禮券已用完';
            }
            else if ($error_code==4){
                $response['message'] = '未達禮券低消';
            }
            else if ($error_code==5){
                $response['message'] = '專屬ID不符';
            }
            else{
                $response['message'] = '失敗';              
            }
            echo json_encode($response);
        }
    }

    public function barcode($bartext = '') {
        $test = Zend_Barcode::draw('code39', 'image', array('text' => $bartext), array());
        imagejpeg($test, './assets/images/barcode/'.$bartext.'.jpg', 100);
    }
    
    public function index(){
        echo 'sell/index';
        redirect('/sell/first', 'refresh');
    }
    
    public function first(){
        $this->load->config('clothing_class');
        $data = array();
        $data["type_name"]=$this->config->item('class_name');
        $data["type_price"]=$this->config->item('class_price');

        $this->load->model('Brand_model');
        $data['brands'] = $this->Brand_model->get_many_by('approved', TRUE);        
        
        $this->template->render('sell/first', $data);
    }

    public function second(){
        $this->template->render('sell/second');
    }

    public function third(){
        $this->load->config('clothing_class');
        $data = array();
        $data["type_price"]=$this->config->item('class_price');
        $data['account'] = $this->Account_model->get($this->session->userdata('user_id'));
        $this->template->render('sell/third', $data);
    }
    
    public function fourth(){        
        $order_data = $this->make_order_data_from_json($this->input->post('order'));
        $items_data = $this->make_items_data_from_json($this->input->post('items'));
        try{
            $order_id = $this->Order_model->create($order_data, $items_data, $this->session->userdata('user_id'));
            $this->Order_model->notify_the_atm_order_created($order_id);
            $this->pay($order_id);
        }
        catch(Exception $e){
            show_error($e->getMessage());
        }
    }
    
    public function pay($order_id){    
        $order = $this->Order_model->get($order_id);      
        
        if ($order['payment_method'] == PAYMENT_CREDIT_CARD) {
            // $this->pay_with_deduction($order_id);
            redirect('http://'.$this->config->item('payment_url').'checkout/desktop/'.$order['order_number'].'?redirect='.base_url());
        }
        else if ($order['payment_method'] == PAYMENT_WEB_ATM) {
            // $this->pay_with_deduction($order_id);
            $this->pay_with_web_atm($order_id);
        }
        else if( $order['payment_method'] == PAYMENT_ATM ){
            // We don't need template system here.
            // Sometimes you just don't need things that complicated ;)
            // $this->template->set_layout("template/empty");
            // $this->template->render("sellorder/payment/atm", ['order_number'=>$order['order_number']]);          
            $this->load->view('sell/payment/atm', ['order_number'=>$order['order_number']]);
        }
        else if( $order['payment_method'] == PAYMENT_DEDUCTION ){
            $this->pay_with_deduction($order_id);
        }
        else {
            show_error('no such payment method.');
            redirect('/member/sellorder', 'refresh');
        }
    }
    
    function pay_with_deduction($order_id){
        $this->Order_model->complete($order_id);
        // $this->Sellorder_model->notify_the_sellorder_is_paid($order['order_number']);        
        $this->complete($order_id);      
    }
    
    function pay_with_credit_card($order_id){
        $order = $this->Order_model->get($order_id);

        $checksum = md5(
            $this->config->item('payment_credit_merchant_number') . 
            $order['order_number'] . 
            $this->config->item('payment_credit_code') . 
            $order['final_price']
        );

        $data = array(
            'MerchantNumber'    => $this->config->item('payment_credit_merchant_number'),
            'OrderNumber'       => $order['order_number'],
            'Amount'            => $order['final_price'],
            'OrgOrderNumber'    => $order['order_number'],
            'ApproveFlag'       => 1,
            'DepositFlag'       => 0,
            'Englishmode'       => 0,
            'iphonepage'        => 0,
            'OrderURL'          => site_url('/sell/receive'),
            'ReturnURL'         => site_url('/sell/receive'),
            'checksum'          => $checksum,
            'op'                => 'AcceptPayment',
            'payment_url'       => $this->config->item('payment_credit_url')
        );

        $this->template->set_layout("template/empty", $data);
        $this->template->render("sellorder/payment/credit_card", $data);      
    }
    
    function pay_with_web_atm($order_id){
        $order = $this->Order_model->get($order_id);

        $checksum = md5(
            $this->config->item('payment_none_credit_merchant_number') . 
            $this->config->item('payment_none_credit_code') . 
            $order['final_price'] . 
            $order['order_number']
        );

        $data = array(
            'merchantnumber'    => $this->config->item('payment_none_credit_merchant_number'),
            'ordernumber'       => $order['order_number'],
            'amount'            => $order['final_price'],
            'paymenttype'       => 'WEBATM',
            'bankid'            => '007',
            'hash'              => $checksum,
            'nexturl'           => site_url('/sell/webatm_meta/' . $order['order_number']),
            'payment_url'       => $this->config->item('payment_none_credit_url')
        );

        $this->template->set_layout("template/empty", $data);
        $this->template->render("sellorder/payment/webatm", $data);
    }
    
    /*
     * Hide the meta url from users.
     */    
    function webatm_meta($order_number){
        $this->session->set_flashdata('order_number', $order_number);
        redirect('/sell/webatm_done', 'refresh');
    }
    
    /*
     * Display this page doesn't mean we are sure the user
     * pay with webatm already, since we don't know when
     * the bank would notice us.
     */    
    function webatm_done(){
        $order_number = $this->session->flashdata('order_number');
        $order = $this->Order_model->get_by('order_number', $order_number);
        $this->complete($order['id']);      
    }
    
    // help fourth step to tranform data from Backbone
    // to PHP format.    
    function make_order_data_from_json($json_order){
        $order = json_decode($json_order, true);

        $order_data = array(
            'user_id' => $this->session->userdata('user_id'),
          
            'user_name' => $order['seller']['name'],
            'user_telephone' => $order['seller']['phone'],
            'user_mobile_phone' => $order['seller']['phone'],
            'user_address' => $order['seller']['location']['zipcode'] .
                ' ' . $order['seller']['location']['county'] . 
                $order['seller']['location']['district'] . 
                ' ' . $order['seller']['location']['address'],

            'delivery_name' => $order['receiver']['name'],
            'delivery_telephone' => $order['receiver']['phone'],
            'delivery_mobile_phone' => $order['receiver']['phone'],
            'delivery_address' => $order['receiver']['location']['zipcode'] .
                ' ' . $order['receiver']['location']['county'] . 
                $order['receiver']['location']['district'] . 
                ' ' . $order['receiver']['location']['address'],
            
            'order_number' => $this->Order_model->generate_order_number(),
            'payment_method' => $order['other']['payment_method'],
            'last_five_number' => $order['other']['last_five_number'],
            'estimated_remit_datetime' => $order['other']['estimated_remit_datetime'],
            
            'deduction_amount' => -1 * (int) $order['deduction_amount'],
            'coupon_code' => $order['coupon_code'],
            
            'uniform_invoice_type' => $order['invoice']['type'],
            'uniform_invoice_number' => $order['invoice']['number'],
            'uniform_invoice_title' => $order['invoice']['title'],
            
            'no_return' => $order['no_return'],
            
            'date_purchased' => date('c'),
            'status_id' => '1'
        );        
        
        if ($order_data['payment_method'] == 'atm'){
            $order_data['status_id'] = '1.5';
        }
      
        return $order_data;
    }
    
    // help fourth step to tranform data from Backbone
    // to PHP format.
    function make_items_data_from_json($json){
        $items = json_decode($json, true);
      
        $items_data = array();
        
        foreach($items as $item){
            $item_data = array(
                'product_title' => $item['product_title'],
                'product_brand' => $item['product_brand'],
                'product_original_price' => $item['product_original_price'],
                'product_sell_price' => $item['product_sell_price'],
                'product_type' => $item['product_type'],
                'comments' => $item['comment'],
                'donative' => $item['donative']
            );          
            
            array_push($items_data, $item_data);
        }

        return $items_data;
    }
    
    function test_receive(){
        $this->template->render('/sell/test_receive', []);
    }

    /**
     * Order Payment Receive (Credit Card)
     * get payment feedback
     */
    public function receive(){
        $order = null;
        $payment_feedback = $this->config->item('payment_credit_feedback');

        foreach ($payment_feedback as $row) {
            $$row = $this->input->post($row);
        }

        // get order
        if (isset($P_OrderNumber) and !empty($P_OrderNumber)) {
            $order = $this->Order_model->get_by('order_number', $P_OrderNumber);
        }

        // successs
        if ($final_result == '1' and $final_return_PRC  == '0' and $final_return_SRC == '0') {
            if (!empty($P_CheckSum)) {
                $checkstr = md5($P_MerchantNumber . $P_OrderNumber . $final_result . $final_return_PRC . $this->config->item('payment_credit_code') . $final_return_SRC . $P_Amount);

                if ($checkstr != strtolower($P_CheckSum)) {
                    $data['title'] = "交易發生問題，驗證碼錯誤";
                    $data['message'] = "請通知網站人員協助處理";
                    $this->template->render('sellorder/info/404', $data);                   
                }

                $this->Order_model->complete($order['id']);
                // $this->Sellorder_model->notify_the_sellorder_is_paid($order['order_number']);
                
                $this->complete($order['id']);
            }
        }
        else{
            if ($final_return_PRC  == '8' and $final_return_SRC == '204') {
                $data['title'] = "訂單編號重覆";
                $data['message'] = "請與商家聯絡";
                $this->template->render('sellorder/info/404', $data);               
            } elseif ($final_return_PRC  == '15' and $final_return_SRC == '1018') {
                $data['title'] = "主機忙碌中";
                $data['message'] = "請稍後再試，或與商家聯絡";
                $this->template->render('sellorder/info/404', $data);               
            }
            else{
                $data['title'] = "交易失敗";
                $data['message'] = "請與商家聯絡";
                $this->template->render('sellorder/info/404', $data);               
            }
        }
    }    

    public function complete($order_number){
        $order = $this->Order_model->db->where('order_number', $order_number)->get('sellorders')->row_array();
        $this->barcode($order['order_number']);        
        $data = array();
        $data["order"]= $order;
        
        $items = $this->Product_model->as_array()->get_many_by('order_id', $order['id']);
        
        $total=0;
        $small=0;
        $big=0;
        $fly=0;
        foreach($items as $product){
            $total++;
            if($product["shipping_price"]<120)
                $small++;
            else if($product["shipping_price"]<150)
                $big++;
            else
                $fly++;
        }
        
        $data = array(
            'order' => $order,
            'small' => $small,
            'big'     => $big,
            'fly'       => $fly,
            'total'   => $total
        );        
        
        $this->template->render('sell/complete', $data);        
    }

}
