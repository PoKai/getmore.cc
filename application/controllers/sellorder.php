<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sellorder extends MY_Controller
{
    /**
     * User id
     *
     * @var int
     **/
    public $user_id = NULL;

    /**
     * __construct
     *
     * @return void
     * @author appleboy
     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->spark('curl/1.2.1');
        $this->load->library(array('ion_auth', 'lib_event', 'lib_product', 'lib_sellorder', 'lib_config', 'lib_sellcart', 'files_lib'));
        $this->load->config('payment');
        $this->load->config('clothing_class');
        $this->template->set('is_hidden_header', true);

        // get current user data
        $this->user_id = $this->session->userdata('user_id');
        $this->default_address = array(
            'county' => '',
            'district' => '',
            'zipcode' => '',
            'address' => ''
        );

        $this->load->library('zend');
        $this->zend->load('Zend/Barcode');
        $this->load->model('Sellorder_model');
    }

    /**
     * index
     *
     * @author appleboy
     **/
    public function index()
    {
        redirect('/');
    }

    /**
     * Selling info 
     *
     * @author Danny
     **/
    public function info($action){
        switch ($action) {
            case 'joinus':
                $template="sellorder/info/{$action}";
                break;
            case 'orderlist':
                $template="sellorder/info/{$action}";
                break;
            case 'showme':
                $template="sellorder/info/{$action}";
                break;
            case 'presell':
                $template="sellorder/info/{$action}";
                break;
            case 'dannyhurryup':
                $template="sellorder/info/{$action}";
                break;
            case 'getredressed':
                $template="sellorder/info/{$action}";
                break;
            case 'redress':
                $template="sellorder/info/{$action}";
                break;
            case 'gradress':
                $template="sellorder/info/{$action}";
                break;
            case 'news':
                $template="sellorder/info/{$action}";
                break;
            case 'whybuy':
                $template="sellorder/info/{$action}";
                break;
            case 'bigvip':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
/*            case 'sellpolicy':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
*/
            case 'pricing':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
            case 'intro':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
            case 'energy':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
            case 'idea':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
            case 'coper':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
/*            case 'sellintro':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
*/
            case 'buyintro':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
            case 'washintro':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
            case 'userpolicy':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
            case 'paynfeepolicy':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
            case 'deliverintro':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
            case 'returnpolicy':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
            case 'getmorelove':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
            case 'qalist':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
            case 'benefit':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
            case '404':
                $template="sellorder/info/{$action}";
                $this->template->set('show_foot', true);
                break;
            default:
                $template="sellorder/info/404";
                $this->template->set('show_foot', true);
                break;
        }
       	$this->template->render($template);
    }

    public function restart_for_pay(){
        if ($this->input->server('REQUEST_METHOD') == 'POST'){
            $order_number = $this->input->post("order_number");
            $this->pay($order_number);
        }
        else if ($this->input->server('REQUEST_METHOD') == 'GET'){
            redirect('/', 'refresh');
        }
    }    
    
    /*
     * DEPRECATED!
     */
     
    /**
     * Selling steps
     *
     * @author popchieh
     **/
    public function steps($action)
    {
        /*
         * Valid Code
         */
        if (!$this->ion_auth->logged_in()) {
            $this->system->redirect();
        }
        
        $user_id = $this->session->userdata('user_id');

        $this->load->model('User_model');
        
        if ( !$this->User_model->is_verified($user_id) ){
            redirect('/verify/index', 'refresh');
        }
        else{
            redirect('/sell/first', 'refresh');          
        }
        
        /*
         * Code bellow are INVALID.
         */
            
        $data = $counts = $events = $products = $events_id = $products_id = array();

        // get user carts
        $carts = $this->lib_sellcart->contents();
        // if carts is empty, redirect to home page
        // if (empty($carts)) {
        //     redirect('/', 'refresh');
        // }

        // steps 1,2,3,4
        switch ($action) {
          case '1':
            // get user order list
            //$data = $this->lib_sellorder->user_order();
            $data=array();
            if(!$this->session->userdata("isverify"))
              redirect("/sellorder/verify");
            $this->session->set_userdata("step-1", "step-1");
            break;
            
          case "2":
            if (!($this->session->userdata("step-1") || $this->session->userdata("step-3"))) {
              redirect("/sellorder/steps/1");
            }

            // delete session data from step-1
            $this->session->unset_userdata("step-1");

            // get user data
            $user_data = $this->ion_auth->user($this->user_id)->row_array();
            $user_data['p_address'] = (!empty($user_data['p_address'])) ? json_decode($user_data['p_address'], true) : $this->default_address;
            $user_data['s_address'] = (!empty($user_data['s_address'])) ? json_decode($user_data['s_address'], true) : $this->default_address;
        
            $steps2 = array(
              "profile"           => $user_data
            );

            // get user order list and merge it.
            //$data = array_merge($this->lib_sellorder->user_order(), $steps2);
            $data = ($this->session->userdata("step-2")["profile"])?$this->session->userdata("step-2"):array_merge($steps2);

            $this->session->set_userdata("total_price", $this->input->get_post('total_price'));

            $this->session->set_userdata("step-2", $data);
            if($this->session->userdata("print_info") && $this->session->userdata("step-3"))
              $data=$this->session->userdata("print_info");
            break;
              
          case "3":
            if (!($this->session->userdata("step-2") || $this->session->userdata("step-4"))) {
                redirect("/sellorder/steps/2");
            }

            // delete session data from step-2
            $this->session->unset_userdata("step-2");

            // get user order list
            $data = $this->lib_sellorder->user_order();

            $total_price = $data['total_price'];

            // start to add order
            $delivery_address_str = $this->input->get_post('deliveryzipcode') . ' ' . $this->input->get_post('deliverycounty') . $this->input->get_post('deliverydistrict') . ' ' . $this->input->get_post('deliveryaddress');
            $user_address_str = $this->input->get_post('userzipcode') . ' ' . $this->input->get_post('usercounty') . $this->input->get_post('userdistrict') . ' ' . $this->input->get_post('useraddress');
            $delivery_address_json = array(
                'county' => $this->input->post('deliverycounty'),
                'district' => $this->input->post('deliverydistrict'),
                'zipcode' => $this->input->post('deliveryzipcode'),
                'address' => $this->input->post('deliveryaddress')
            );  
            $user_address_json = array(
                'county' => $this->input->post('usercounty'),
                'district' => $this->input->post('userdistrict'),
                'zipcode' => $this->input->post('userzipcode'),
                'address' => $this->input->post('useraddress')
            );

            // add order
            $formdata = array(
                "user_id"                   => $this->user_id,
                "user_name"                 => $this->input->get_post('user_name') ? $this->input->get_post('user_name') : NULL,
                "user_sex"                  => $this->input->get_post('user_sex') ? $this->input->get_post('user_sex') : NULL,
                "user_mobile_phone"         => $this->input->get_post('user_telephone') ? $this->input->get_post('user_telephone') : NULL,
                "user_telephone"             => $this->input->get_post('user_telephone') ? $this->input->get_post('user_telephone') : NULL,
                "user_address"              => $user_address_str,
                "delivery_name"             => $this->input->get_post('delivery_name') ? $this->input->get_post('delivery_name') : NULL,
                "delivery_sex"              => $this->input->get_post('delivery_sex') ? $this->input->get_post('delivery_sex') : NULL,
                "delivery_mobile_phone"     => $this->input->get_post('delivery_telphone') ? $this->input->get_post('delivery_telphone') : NULL,
                "delivery_telephone"         => $this->input->get_post('delivery_telephone') ? $this->input->get_post('delivery_telephone') : NULL,
                "delivery_address"          => $delivery_address_str,
                "uniform_invoice_title"     => $this->input->get_post('uniform_invoice_title') ? $this->input->get_post('uniform_invoice_title') : NULL,
                "uniform_invoice_number"    => $this->input->get_post('uniform_invoice_number') ? $this->input->get_post('uniform_invoice_number') : NULL,
                "uniform_invoice_type"      => $this->input->get_post('uniform_invoice_type'),
                "order_number"              => $this->generate_order_number(),
                "total_price"               => $this->session->userdata('total_price'),
                //"total_price"               => $total_price,
                "date_purchased"            => date("Y-m-d H:i:s"),
                // "payment_method"            => $this->input->get_post('payment_method'),
                "status_id"                 => ORDER_PAYMENT,
                "no_return"                 => $this->input->get_post("no_return") ? $this->input->get_post('no_return') : 0
            );

            // save data in session
            $this->session->set_userdata("formdata", $formdata);

            // update user data
            $userdata = array(
            //    "user_name"     => $this->input->get_post('user_name') ? $this->input->get_post('user_name') : NULL,
            //    "sex"           => $this->input->get_post('user_sex') ? $this->input->get_post('user_sex') : NULL,
            //    "mobile_phone"  => $this->input->get_post('user_mobile_phone') ? $this->input->get_post('user_mobile_phone') : NULL,
            //    "telephone"      => $this->input->get_post('user_telephone') ? $this->input->get_post('user_telephone') : NULL,
            //    "p_address"     => json_encode($user_address_json),
                "s_address"     => json_encode($delivery_address_json)
            );
            $this->ion_auth->update_user($this->user_id, $userdata);

            $this->session->set_userdata("step-3", "step-3");
            $total=0;
            $small=0;
            $big=0;
            $fly=0;
            $tran_p=0;
            foreach($data['products'] as $product){
                          $total++;
                if($product["shipping_price"]<120)
                    $small++;
                else if($product["shipping_price"]<150)
                    $big++;
                else
                    $fly++;
            }
            // if($total_price<400)
            //           $tran_p=$this->config->item('class_transport')["less"];
            //       else if($total_price<600)
            //           $tran_p=$this->config->item('class_transport')["more"];
            //       else
            //           $tran_p=0;
            $tran_p = 120;

        /*
        //special event, special price!
        $data['total_shipping_price'] = 299;
        $tran_p = 0;
        if (count($data['products']) > 3){
          $data['total_shipping_price'] = 299 + (count($data['products']) - 3) * 88;
        }
        */

		$data1=array("total_num"=>$total,
			    "small_num"=>$small,
			    "big_num"=>$big,
			    "fly_num"=>$fly,
			    "total_all_price"=>$data['total_shipping_price']+$tran_p,
			    "tran_price"=>$tran_p,
			    "wash_price"=>$data['total_shipping_price']
			    );
		$data=array_merge($data1, $formdata);
		$this->session->set_userdata("print_info", $data);

    // fucked by howtomakeaturn
    // 299 is also related here
    $this->session->set_userdata("total_price", $total_price);

                $this->load->model('Account_model', 'account');
                $account = $this->account->get_by_user_id( $this->session->userdata('user_id') );
                $data['account'] = $account;

    
                break;
            case "4":
                // get user order list
                $data = $this->lib_sellorder->user_order();
                $formdata = $this->session->userdata("formdata");
                $steps4 = array(
                    "payment_method"    => $this->input->get_post("payment_method"),
                    "last_five_number"    => $this->input->get_post("last_five_code"),
                    "estimated_remit_datetime"    => $this->input->get_post("datetimepicker")
                );

                // get user order list and merge it.
                $formdata = array_merge($formdata, $steps4);

                /*
                 * form data 'show' is sent from view of step3,
                 * so this means user come from step3
                 * ------------------------------------------------------------------------------------
                 * @guesser howtomakeaturn
                 */
                if($this->input->get_post('show')){
                    $deduction_amount = $this->input->post('deduction_amount');
                    $formdata['deduction_amount'] =  -1 * (int) $deduction_amount;
                    // create the tuple to sellorders table
                    $order_id = $this->lib_sellorder->add_order($formdata);
                    $this->session->set_userdata("pay_order_id",$order_id);
                    $add=4;
                }
                // if user not come from view of step3
                else{
                    $order_id = $this->session->userdata("pay_order_id");
                    $add=null;
                }

                // tuples in sellorders_products are created here
                $data = $this->lib_sellorder->user_order($add, $order_id, $formdata['order_number']);

                /*
                 * I guess the system update the price again here, 
                 * because tuples in sellordes_products are created after the order created?
                 * 
                 * @guesser howtomakeaturn
                 */
                
                // update total price
                $this->lib_sellorder->update_order($order_id, 
                array('total_price' => $this->session->userdata("total_price"), 
                          'transportation_charge' => $data['transportation_charge']));

                /* if user not come from view of step3
                 * it may come from 1. $this->pay() or 2. $this->receive
                 * the 1. means it come from paying with web-atm,
                 * which doesn't need $this->receive()
                 * the 2. means it come from credit-car't'
                 * ------------------------------------------------------------------------------------
                 * @guesser howtomakeaturn
                 */
                
                if(!$this->input->get_post('show')){
                    /*
                     * checkout succesfully, clean up the session,
                     * and ready to show the success page
                     * -----------------------------------------------------------------------------------
                     * @guesser howtomakeaturn
                     */
                    $this->session->unset_userdata('carts_time');
                    $this->session->unset_userdata('carts');

                    $data['ordernumber']=$formdata['order_number'];
                    $this->barcode($formdata['order_number']);
                }
                /*
                 * ok. so the user come from view of step3 again
                 * let's see what we need to do now
                 * ------------------------------------------------------------------------------------------
                 * @guesser howtomakeaturn
                 */
                else{
                    /*
                     * notice the usage of this session data
                     * it shows why global variables are awful
                     * and what does 'bad practice' mean
                     * 
                     * @noticer howtomakeaturn
                     */
                    $this->session->unset_userdata("receive");
                    /*
                     * this if block is meaningless since we don't implement ATM method
                     * yet. ignore it now until you need it.
                     * btw, handle ATM checkout procedure here is masterpiece.
                     * it totally mess up the codes and logics.
                     * bravo for the senior developers! :D
                     * --------------------------------------------------------------------------------------
                     * @guesser howtomakeaturn
                     */
                    if ($formdata['payment_method'] == PAYMENT_ATM) {
                        $this->pay($formdata['order_number']);
                        /*
                        // get total price and checksum
                        $total_price = (int) ($data['total_price'])+(int)$data['transportation_charge'];
                        $checksum = md5($this->config->item('payment_none_credit_merchant_number') . $this->config->item('payment_none_credit_code') . $total_price . $formdata['order_number']);

                        $data = array(
                            'merchantnumber' => $this->config->item('payment_none_credit_merchant_number'),
                            'ordernumber' => $formdata['order_number'],
                            'amount' => $total_price,
                            'Paymenttype' => 'ATM',
                            'hash' => $checksum,
                            'bankid' => '007',
                            'returnvalue' => 1
                        );

                        // add payment for ATM
                        $return = $this->curl->simple_post($this->config->item('payment_none_credit_url'), $data, array('SSLVERSION' => 3, 'SSL_VERIFYPEER' => false));
                        parse_str($return, $response);

                        // update bank id and virtual account
                        if (isset($response['rc']) and $response['rc'] == '0') {
                            $this->lib_sellorder->update_order($order_id, array('bank_id' => $response['bankid'], 'virtual_account' => $response['virtualaccount']));
                        }
                        */
                    }
                    /*
                     * the else block means user come from step3 and it doesn't
                     * wanna pay with ATM. now pass the ball to 'pay' method.
                     * oh, btw, why we don't handle the ATM in 'pay' method, too?
                     * because everyone is kind of naughty I guess
                     * ---------------------------------------------------------------------------------------------
                     * @guesser howtomakeaturn
                     */
                    else {
                        // redirect to payment page
                        $this->pay($formdata['order_number']);
                    }
                }// end else

                /*
                 * I don't want to think what the following codes mean.
                 * too scary to even think about it.
                 * -------------------------------------------------------------------------
                 * @guesser howtomakeaturn
                 */
                $data=$this->session->userdata("print_info");		
                $data['ordernumber']=$formdata['order_number'];
	    
                if(!$this->session->userdata("receive"))
                $action='3';
                break;
        }// end switch

        $template = 'sellorder/step-' . $action;
        $this->template->render($template, $data);
    }

    public function barcode($bartext = '') {
        $test = Zend_Barcode::draw('code39', 'image', array('text' => $bartext), array());
        imagejpeg($test, './assets/images/barcode/'.$bartext.'.jpg', 100);
    }

    /**
     * Order list
     *
     * @param  int
     * @author appleboy
     **/
    public function lists($id = '')
    {
        if (!$this->ion_auth->logged_in()) {
            $this->system->redirect();
        }

        $data = $outputs = array();

        $order = $this->lib_sellorder->select('orders_status.name as orders_status, sellorders.*')->where('user_id', $this->user_id)->order_by('add_time', 'desc')->orders()->result_array();

        foreach ($order as $row) {
            $data = array(
                'order_id'          => $row['id'],
                'order_number'      => $row['order_number'],
                'date_purchased'    => date('Y-m-d', strtotime($row['date_purchased'])),
                'payment_method'    => $this->lib_sellorder->payment_method($row['payment_method']),
                'total_price'       => $this->lib_sellcart->format_number($row['total_price']),
                'status'            => $row['orders_status'],
                'pay'               => ($row['status_id'] == ORDER_PAYMENT) ? true : false
            );

            $outputs[] = $data;
        }

        $data = array(
            'outputs' => $outputs
        );

        $this->template->render("sellorder/list", $data);
    }

    // deprecated
    public function verify(){
      redirect('/verify');
      /*
      if (!$this->ion_auth->logged_in()) {
        $this->system->redirect();
      }

      if($this->session->userdata("isverify"))
        redirect("/sellorder/steps/1");
      $data = $outputs = array();
      
      $data = array(
        'outputs' => $outputs
      );

      if ($this->session->flashdata('wrong_id_number'))
          $data['message'] = true;

      $this->session->set_userdata("sms_code", $this->generateSMSCode());

      $this->template->render("sellorder/verify", $data);
      */
    }

    // deprecated
    public function verify_code(){
        redirect('/verify');
      
        /*
        if (!$this->ion_auth->logged_in()) {
            $this->system->redirect();
        }
	
        if($this->session->userdata("isverify"))
            redirect("/");
        $chinese_name=trim($this->input->get_post("chinese_name"));
        $gender=trim($this->input->get_post("gender"));
        $birthday=trim($this->input->get_post("birthday"));
        $phone=trim($this->input->get_post("phone"));
        $verification_code=trim($this->input->get_post("verification_code"));
        $identification_card_number = trim($this->input->get_post("id"));
        
        // for resuming user data if it failed
        $this->session->set_flashdata('chinese_name', $chinese_name);
        $this->session->set_flashdata('gender', $gender);
        $this->session->set_flashdata('phone', $phone);
        $this->session->set_flashdata('id', $identification_card_number);

        if(empty($phone)){
            $this->system->message('電話號碼輸入錯誤!!', 'error');
                  redirect('/sellorder/verify', 'refresh');

        }

        if(!$this->checkNick($identification_card_number)){
            $this->system->message('identification card number check failed!!', 'error');// actually, this doesnt affect anything
            $this->session->set_flashdata('wrong_id_number', '1');
            redirect('/sellorder/verify', 'refresh');
        }
        
        if(!empty($phone) && !empty($verification_code)){//from verify_code
            if($this->session->userdata("sms_code")==$verification_code ){
                // do update user verify status
                $this->ion_auth->update_user($this->user_id, array("verify"=>1));
                $outputs['verify_status']="yes";
                $this->session->set_userdata("isverify",1);
            }
        }else{//from verify
             //do update user info
            $address_json = array(
                'county' => $this->input->post('county'),
                'district' => $this->input->post('district'),
                'zipcode' => $this->input->post('zipcode'),
                'address' => $this->input->post('address')
            );

            $userdata = array(
                "user_name"     => empty($chinese_name) ? NULL:$chinese_name,
                "sex"           => empty($gender) ? NULL:$gender,
                "mobile_phone"  => empty($phone) ? NULL:$phone,
                "telephone"     => empty($phone) ? NULL:$phone,
                "birthday"     => empty($birthday) ? NULL:$birthday,
                "p_address"     => json_encode($address_json),
                "s_address"     => json_encode($address_json),
                "identification_card_number" => $identification_card_number// added by howtomakeaturn
            );
            $this->ion_auth->update_user($this->user_id, $userdata);
            $rs=$this->system->send_sms_msg($phone,"Getmore驗證碼:".$this->session->userdata('sms_code'));
        }// end else
        
        $outputs['phone']=$phone;
        $outputs['id']=$identification_card_number;
        
        // $outputs['sms']=$this->session->userdata("sms_code");
        $data = array(
            'outputs' => $outputs
        );

        $this->template->render("sellorder/verify_code", $data);
        */
    }

    private function generateSMSCode(){
	$chars = "0123456789"; 
        srand((double)microtime()*1000000); 
        $i = 0; 
        $pass = '' ; 

        while ($i < 7) { 
            $num = rand() % 10; 
            $tmp = substr($chars, $num, 1); 
            $pass = $pass . $tmp; 
            $i++; 
        } 

        return $pass; 
    }

    /**
     * Single Order
     *
     * @param  string
     * @return json
     * @author appleboy
     **/
    public function single($order_number = '')
    {
        $order = $event = $product = $payment_data = array();

        if (empty($order_number)) {
            redirect('/sellorder/lists', 'refresh');
        }

        // get order
        $order = $this->lib_sellorder->select('sellorders.*')->where('user_id', $this->user_id)->where('sellorders.order_number', $order_number)->orders()->row_array();

        if (empty($order)) {
            redirect('/sellorder/lists', 'refresh');
        }

        $event = $this->lib_sellorder->select('*')->where('order_id', $order['id'])->orders('event')->result_array();
        $product = $this->lib_sellorder->select('*')->where('order_id', $order['id'])->orders('product')->result_array();

        if ($order['status_id'] == ORDER_COMPLETE) {
            $message = $this->system->message('此訂單付款成功!! 付款時間: ' . $order['date_finished'], 'success', false);
            $show_pay_button = false;
        } elseif ($order['payment_method'] == PAYMENT_WEB_ATM and !empty($order['lock_time']) and (($order['lock_time'] + 60*15) > $this->_time)) {
            $message = $this->system->message('此交易正由系統處理中，暫時無法提供交易進度查詢，請於15分鐘後重新確認', 'error', false);
            $show_pay_button = false;
        } elseif ($order['status_id'] == ORDER_EXPIRED) {
            $message = $this->system->message('超過繳費期限，訂單已取消', 'error', false);
            $show_pay_button = false;
        } elseif ($order['status_id'] == ORDER_CANCEL) {
            $message = $this->system->message('訂單已取消', 'error', false);
            $show_pay_button = false;
        } else {
            $message = $this->system->message('此訂單尚未付款', 'error', false);
            $show_pay_button = true;
        }

        if ($order['payment_method'] == PAYMENT_ATM) {
            $show_pay_button = false;
            $order['bank_id'] = (!empty($order['bank_id'])) ? $order['bank_id'] : '';
            $order['virtual_account'] = (!empty($order['virtual_account'])) ? $order['virtual_account'] : '';
        }

        $data = array(
            'order'             => $order,
            // 'event'             => $event,
            'product'           => $product,
            'total_sell_price'  => 0,
            // 'total_event_price' => 0,
            'payment_method'    => $this->lib_sellorder->payment_method($order['payment_method']),
            'uniform_invoice'   => $this->lib_sellorder->uniform_invoice($order),
            'show_pay_button'   => $show_pay_button,
            'status'            => $message
        );

        $this->template->render("sellorder/view", $data);
    }

   /**
     * Print Order
     *
     * @param  string
     * @return json
     * @author popchieh
     **/
    public function prints($order_number = '')
    {
        $this->template->set_layout('template/print');

        $order = $event = $product = $payment_data = array();
        $total_sell_price = $total_original_price = $count_small = $count_medium = $count_big = 0;
 
        if (empty($order_number)) {
            redirect('/sellorder/lists', 'refresh');
        }

        // get order
        $order = $this->lib_sellorder->select('sellorders.*')->where('sellorders.order_number', $order_number)->orders()->row_array();
        $check = $this->lib_sellorder->select('sellorders.*')->where('user_id', $this->user_id)->where('sellorders.order_number', $order_number)->orders()->row_array();

        if (empty($check)) {
            redirect('/sellorder/lists', 'refresh');
        }

        // $event = $this->lib_sellorder->select('*')->where('order_id', $order['id'])->orders('event')->result_array();
        $product = $this->lib_sellorder->select('*')->where('order_id', $order['id'])->orders('product')->result_array();

      
        if ($order['payment_method'] == PAYMENT_ATM) {
            $show_pay_button = false;
            $order['bank_id'] = (!empty($order['bank_id'])) ? $order['bank_id'] : '';
            $order['virtual_account'] = (!empty($order['virtual_account'])) ? $order['virtual_account'] : '';
        }

        /*
         * What a buggy code.
         * What a genius trying to store config value in database.
         * @commenter howtomakeaturn
         */
        // get shipping_price
        //$data = $this->lib_sellorder->user_order();

        $this->load->config('clothing_class');

        foreach($product as $row):
            if($row['shipping_price'] == $this->config->item('s_price'))
                $count_small++;
            elseif($row['shipping_price'] == $this->config->item('b_price'))
                $count_medium++;
            elseif($row['shipping_price'] == $this->config->item('f_price'))
                $count_big++;
        endforeach;

        $data = array(
            'order'             => $order,
            // 'event'             => $event,
            'product'           => $product,
            'total_sell_price'  => 0,
            // 'total_event_price' => 0,
 //           'payment_method'    => $this->lib_sellorder->payment_method($order['payment_method']),
   //         'uniform_invoice'   => $this->lib_sellorder->uniform_invoice($order),
            'show_pay_button'   => $show_pay_button,
            'status'            => $message,
            'count_small'       => $count_small,
            'count_medium'      => $count_medium,
            'count_big'         => $count_big
        );

        $this->template->render("sellorder/prints", $data);
    }
    
    /*
     * if it's zero dollar sellorder,
     * change payment method to 'deduction'
     * 
     * @authro howtomakeaturn
     */
    
    private function fix_payment_method_if_needed($order_number){
        $this->db->from('sellorders')->where('order_number', $order_number);
        $query = $this->db->get();
        $row = $query->result();
        $order = $row[0];
        
        $final_price = $order->total_price + $order->transportation_charge + $order->deduction_amount;
        if ($final_price == 0){
            $data = array( 
                'payment_method' => 'deduction'
            );
                        
            $this->db->where('order_number', $order_number);
            $this->db->update('sellorders', $data);
        }
    }

    /**
     * Order Payment
     *
     * @param  string
     * @return view
     */
    public function pay($order_number = '')
    {

        if (empty($order_number)) {
            $this->system->message('無此訂單，操作錯誤!!', 'error');
            redirect('/sellorder/lists', 'refresh');
        }

        // check if if's zero-dollar sellorder
        // it totally should not implement this way, but
        // thank god the previous engineers are genius
        // fix it sometime
        $this->fix_payment_method_if_needed($order_number);

        // get order
        $order = $this->lib_sellorder->select('sellorders.*')->where('sellorders.order_number', $order_number)->orders()->row_array();

        // validate order data
        $this->_validate($order);
        

        $is_pay = ($order['status_id'] == ORDER_PAYMENT) ? true : false;

        // credit card or web atm
        if ($is_pay and $order['payment_method'] == PAYMENT_CREDIT_CARD) {
            $total_price = (int) ($order['total_price'])+(int)$order['transportation_charge']+(int)$order['deduction_amount'];
            $checksum = md5($this->config->item('payment_credit_merchant_number') . $order['order_number'] . $this->config->item('payment_credit_code') . $total_price);

            $data = array(
                'MerchantNumber'    => $this->config->item('payment_credit_merchant_number'),
                'OrderNumber'       => $order['order_number'],
                'Amount'            => $total_price,
                'OrgOrderNumber'    => $order['order_number'],
                'ApproveFlag'       => 1,
                'DepositFlag'       => 0,
                'Englishmode'       => 0,
                'iphonepage'        => 0,
                'OrderURL'          => site_url('/sellorder/receive'),
                'ReturnURL'         => site_url('/sellorder/receive'),
                'checksum'          => $checksum,
                'op'                => 'AcceptPayment',
                'payment_url'       => $this->config->item('payment_credit_url')
            );

            $this->template->set_layout("template/empty", $data);
            $this->template->render("sellorder/payment/credit_card", $data);

        } elseif ($is_pay and $order['payment_method'] == PAYMENT_WEB_ATM) {

            // check excute time out
            if (!empty($order['lock_time']) and (($order['lock_time'] + 60*15) > $this->_time)) {
                $this->system->message('此訂單正在處理中，請於15分鐘後再操作', 'error');
                redirect('/sellorder/lists', 'refresh');
            }

            // regenerate new order number
            // $order['order_number'] = $this->generate_order_number();
            // update order number
            // $this->lib_sellorder->update_order($order['id'], array('order_number' => $order['order_number'], 'lock_time' => $this->_time));

            $total_price = (int) ($order['total_price'])+(int)$order['transportation_charge']+(int)$order['deduction_amount'];

            $checksum = md5($this->config->item('payment_none_credit_merchant_number') . $this->config->item('payment_none_credit_code') . $total_price . $order['order_number']);

            $data = array(
                'merchantnumber'    => $this->config->item('payment_none_credit_merchant_number'),
                'ordernumber'       => $order['order_number'],
                'amount'            => $total_price,
                'paymenttype'       => 'WEBATM',
                'bankid'            => '007',
                'hash'              => $checksum,
                'nexturl'           => site_url('/sellorder/steps/4'),
                'payment_url'       => $this->config->item('payment_none_credit_url')
            );

            $this->template->set_layout("template/empty", $data);
            $this->template->render("sellorder/payment/webatm", $data);
        }
        elseif ($is_pay and $order['payment_method'] == 'deduction') {
            // update order payment status
            $this->db->where('id', $order['id'])->set(array('status_id' => ORDER_COMPLETE, 'date_finished' => date("Y-m-d H:i:s")))->update('sellorders');
            $this->Sellorder_model->notify_the_sellorder_is_paid($order['order_number']);

            $this->system->message(date("Y-m-d H:i:s") . ' 成功!!', 'success');
            //redirect('/sellorder/single/' . $order['order_number'], 'refresh');
            $this->session->set_userdata("receive", "ok");
            redirect('/sellorder/steps/4', 'refresh');
        }
        elseif ($order['payment_method'] == 'atm') {
            /*
             * because the atm order doesn't handle online with Neweb company now,
             * I comment these codes for now.
             * They work actually. Use them someday.
             * 
             * @author howtomakeaturn
             */
            
            /*
            $total_price = (int) ($order['total_price'])+(int)$order['transportation_charge']+(int)$order['deduction_amount'];
            $checksum = md5($this->config->item('payment_none_credit_merchant_number') . $this->config->item('payment_none_credit_code') . $total_price . $order['order_number']);

            // calculate the duedate for neweb company
            $this->load->helper('checkout_helper');
            // $duedate = get_atm_duedate()->format('Ymd');
                        
            $data = array(
                'MerchantNumber'    => $this->config->item('payment_none_credit_merchant_number'),
                'OrderNumber'       => $order['order_number'],
                'Amount'            => $total_price,
                'Paymenttype' => 'ATM',
                'hash' => $checksum,
                'bankid' => '007',
                'returnvalue' => 1,
                // 'duedate' => $duedate
            );
            
            // add payment for ATM            
            $this->load->spark('curl/1.2.1');
            $return = $this->curl->simple_post($this->config->item('payment_none_credit_url'), $data, array('SSLVERSION' => 3, 'SSL_VERIFYPEER' => false));
            parse_str($return, $response);
            */
            // update bank id and virtual account
            // if (isset($response['rc']) and $response['rc'] == '0') {
              
             //   $this->db->where('order_number', $order['order_number']);
              //  $this->db->update('sellorders', array('bank_id' => $response['bankid'], 'virtual_account' => $response['virtualaccount']));               
                  $this->template->set_layout("template/empty");
                  $this->template->render("sellorder/payment/atm", ['order_number'=>$order['order_number']]);
            // }
            /*
            else{
                echo $return;
            }
            */                    
        }        
         else {
            $this->system->message('操作錯誤!!', 'error');
            redirect('/sellorder/lists', 'refresh');
        }
    }

    /**
     * Order Payment Receive (ATM & WebATM)
     *
     * get payment feedback
     *
     * @return void
     */
    public function writeoff()
    {
        $payment_code = $this->config->item('payment_none_credit_code');
        $payment_feedback = $this->config->item('payment_atm_feedback');

        foreach ($payment_feedback as $row) {
            $$row = $this->input->post($row, false, '');
        }

        // verify hash value
        $verify = array(
            'merchantnumber' => $merchantnumber,
            'ordernumber' => $ordernumber,
            'serialnumber' => $serialnumber,
            'writeoffnumber' => $writeoffnumber,
            'timepaid' => $timepaid,
            'paymenttype' => $paymenttype,
            'amount' => $amount,
            'tel' => $tel
        );

        $verify = md5(http_build_query($verify) . $payment_code);

        if ($hash != $verify) {
            return;
        }
        
        // handle if it's buyorder
        if ( substr($ordernumber, 0, 1)=='B' ){
            $this->load->model('Buy_order_model', 'buyorder');
            $this->buyorder->switch_one_to_paid_status_by_order_number($ordernumber);          
        }
        // handle if it's sellorder
        else if ( substr($ordernumber, 0, 1)=='S' ){
            $this->db->where('order_number', $ordernumber)->set(array('status_id' => ORDER_COMPLETE, 'date_finished' => date("Y-m-d H:i:s")))->update('sellorders');
            $this->Sellorder_model->notify_the_sellorder_is_paid($ordernumber);          
        }
        
        /*
        // get pay order time
        preg_match('/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/', $timepaid, $time);

        $data = array(
            'status_id' => ORDER_COMPLETE,
            'date_finished' => sprintf('%s-%s-%s %s:%s:%s',
                $time[1], $time[2], $time[3], $time[4], $time[5], $time[6])
        );

        // update order status
        $this->db->set($data)->where('orders.order_number', $ordernumber)->update('orders');
        */
    }

    /**
     * Order Payment Receive (Credit Card)
     *
     * get payment feedback
     *
     * @return void
     */
    public function receive()
    {
        $order = null;
        $payment_feedback = $this->config->item('payment_credit_feedback');

        foreach ($payment_feedback as $row) {
            $$row = $this->input->post($row);
        }

        // get order
        if (isset($P_OrderNumber) and !empty($P_OrderNumber)) {
            $order = $this->lib_sellorder->select('sellorders.*')->where('sellorders.order_number', $P_OrderNumber)->orders()->row_array();
        }

        // validate order data
        $this->_validate($order, false);

        // successs
        if ($final_result == '1' and $final_return_PRC  == '0' and $final_return_SRC == '0') {
            if (!empty($P_CheckSum)) {
                $checkstr = md5($P_MerchantNumber . $P_OrderNumber . $final_result . $final_return_PRC . $this->config->item('payment_credit_code') . $final_return_SRC . $P_Amount);

                if ($checkstr != strtolower($P_CheckSum)) {
                    $this->system->message('交易發生問題，驗證碼錯誤!!', 'error');
                    redirect('/sellorder/single/' . $order['order_number'], 'refresh');
                }

                // update order payment status
                $this->db->where('id', $order['id'])->set(array('status_id' => ORDER_COMPLETE, 'date_finished' => date("Y-m-d H:i:s")))->update('sellorders');
                $this->Sellorder_model->notify_the_sellorder_is_paid($order['order_number']);

                $this->system->message(date("Y-m-d H:i:s") . ' 線上付款成功!!', 'success');
                //redirect('/sellorder/single/' . $order['order_number'], 'refresh');
		$this->session->set_userdata("receive", "ok");
                redirect('/sellorder/steps/4', 'refresh');
            }
        } else {
            if ($final_return_PRC  == '8' and $final_return_SRC == '204') {
                $this->system->message('訂單編號重覆，請與商家聯絡!', 'error');
            } elseif ($final_return_PRC  == '15' and $final_return_SRC == '1018') {
                $this->system->message('主機忙碌中請稍後再試，或與商家聯絡!', 'error');
            } else {
                $this->system->message('交易失敗，請與商家聯絡!', 'error');
            }
	    $this->session->unset_userdata("receive");
            redirect('/sellorder/single/' . $order['order_number'], 'refresh');
        }
    }

    /**
     * Order Payment Validate
     *
     * @param  object
     * @return void
     */
    private function _validate($order = null, $check_login_user = true)
    {
        // check if empty
        if (empty($order)) {
            $this->system->message('並無此訂單', 'error');
            redirect('/sellorder/lists', 'refresh');
        }

        // check if order user is equal session user
        if ($check_login_user and $order['user_id'] != $this->user_id) {
            $this->system->message('您並非是此訂單使用者', 'error');
            redirect('/sellorder/lists', 'refresh');
        }

        // Because atm order status_id may be 1.5
        // so comment this for now.
        // @author howtomakeaturn
        /*
        if ($order['status_id'] != ORDER_PAYMENT) {
            $this->system->message('您已經付款完成或取消訂單', 'success');
            redirect('/sellorder/lists', 'refresh');
        }
        */
    }

    /**
     * Generate Order Number
     *
     * @return string
     * @author popchieh
     */
    private function generate_order_number()
    {
        $this->load->helper('date');

        // $datestring = "Year: %Y Month: %m Day: %d - %h:%i %a";
        $datestring = "%Y%m%d";
        $time = time();
        // $time = $this->_time;

        // date('Ymd') == date('Ymd', strtotime($timestamp));
        // update order status
        // $this->db->set($data)->where('orders.order_number', $ordernumber)->update('orders');
        // query order_number

        $order_num = "S";
        if($_SERVER['REMOTE_ADDR'] == '127.0.0.1' ){
            $order_num = "T";
        }
        
        $time_str = mdate($datestring, $time);

        // select count(*), DATE_FORMAT(date_purchased,"%Y-%m-%d") as created_day FROM sellorders GROUP BY created_day
        $result = $this->lib_sellorder->select('count(*), DATE_FORMAT(date_purchased,"%Y%m%d") as created_day')
             ->group_by('created_day')
             ->orders()
             ->result_array();

        foreach ($result as $row) {
            if($time_str == $row['created_day']){
                $count_today = $row['count(*)'];
		if(!$count_today || $count_today==0)
                    $count_today = 1;
		else	
                    $count_today++;
            }else{
                $count_today = 1;
            }
        }

        $sn_today = $count_today;
        $sn_today = str_pad($sn_today,4,'0',STR_PAD_LEFT);

        $time_str = substr($time_str, 2);
        $order_num .= $time_str.$sn_today;

        // return strtoupper($this->system->generate_code('1', 'word')) . $this->system->generate_code('10', 'digit');
        return $order_num;
    }
    
    //**************
    // 身份證檢查
    // http://doublekai.org/blog/?p=25
    //**************
    private function checkNick($id){
        //建立字母分數陣列
        $head = array('A'=>1,'I'=>39,'O'=>48,'B'=>10,'C'=>19,'D'=>28,
                      'E'=>37,'F'=>46,'G'=>55,'H'=>64,'J'=>73,'K'=>82,
                      'L'=>2,'M'=>11,'N'=>20,'P'=>29,'Q'=>38,'R'=>47,
                      'S'=>56,'T'=>65,'U'=>74,'V'=>83,'W'=>21,'X'=>3,
                      'Y'=>12,'Z'=>30);
        //建立加權基數陣列
        $multiply = array(8,7,6,5,4,3,2,1);
        //檢查身份字格式是否正確
        if (ereg("^[a-zA-Z][1-2][0-9]+$",$id) && strlen($id) == 10){
            //切開字串
            $len = strlen($id);
            for($i=0; $i<$len; $i++){
                $stringArray[$i] = substr($id,$i,1);
            }
            //取得字母分數
            $total = $head[array_shift($stringArray)];
            //取得比對碼
            $point = array_pop($stringArray);
            //取得數字分數
            $len = count($stringArray);
            for($j=0; $j<$len; $j++){
                $total += $stringArray[$j]*$multiply[$j];
            }
            //檢查比對碼
            if (($total%10 == 0 )?0:10-$total%10 != $point) {
                return false;
            } else {
                return true;
            }
        }  else {
           return false;
        }
    }
    
    

}
