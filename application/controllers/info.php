<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Info extends MY_Controller{
    protected $data = array();

    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->model('Product_clothing_model');

        $this->load->spark('Twiggy/0.8.5');
        $this->twiggy->theme('');

        $this->twiggy->set('page_url', $this->uri->segment(2));
        $this->twiggy->set(array(
            'lang' => 'zh-tw',
            'meta_charset' => 'UTF-8',
            'site_title' => 'Getmore - 二手衣時尚購物網站/綠色時尚/買二手衣/賣二手衣',
            'site_description' => 'Getmore 是亞洲第一個二手衣時尚買賣網站，提供穿二手衣的生活新理念，輕鬆享受二手衣獨特、環保、時尚的二次生活。',
            'site_keywords' => '時尚,衣著,服飾,時裝,流行,褲子,買二手衣,賣二手衣,環保,電子商務,startup,二次時尚',
            'logined' => $this->ion_auth->logged_in()
        ), NULL, TRUE);
    }
    
    // smart way to render
    function render(){
        $view_path = $this->router->fetch_class() . '/' . 
        $this->router->fetch_method();
        $view_path = strtolower($view_path);
        $this->template->render($view_path, $this->data);
    }
    
    function sell_type(){
        $clothing_list1 = [1048, 469, 595, 597];
        $clothings1 = [];
        foreach($clothing_list1 as $id){
            array_push( $clothings1,
                $this->Product_clothing_model->get_single($id) );          
        }

        $clothing_list2 = [378, 206, 379, 254];
        $clothings2 = [];
        foreach($clothing_list2 as $id){
            array_push( $clothings2,
                $this->Product_clothing_model->get_single($id) );          
        }

        $clothing_list3 = [429, 67, 622, 480];
        $clothings3 = [];
        foreach($clothing_list3 as $id){
            array_push( $clothings3,
                $this->Product_clothing_model->get_single($id) );          
        }

        $clothing_list4 = [584, 586, 395, 446];
        $clothings4 = [];
        foreach($clothing_list4 as $id){
            array_push( $clothings4,
                $this->Product_clothing_model->get_single($id) );          
        }

        $clothing_list5 = [463, 421, 427, 649];
        $clothings5 = [];
        foreach($clothing_list5 as $id){
            array_push( $clothings5,
                $this->Product_clothing_model->get_single($id) );          
        }
        
        $this->data['clothings1'] = $clothings1;
        $this->data['clothings2'] = $clothings2;
        $this->data['clothings3'] = $clothings3;
        $this->data['clothings4'] = $clothings4;
        $this->data['clothings5'] = $clothings5;
        $this->render();
    }

    function about(){
        $this->render();        
    }

    function team(){
        $this->render();        
    }

    function joinus(){
        $this->render();        
    }

    function sell(){
        $this->render();        
    }

    function startsell(){
        $this->render();        
    }

    function buy(){
        $this->render();        
    }

    function perfectbuy(){
        $this->render();        
    }
    
    function product(){
        $this->render();        
    }

    function qalist(){
        $this->render();        
    }

    function people(){
        $this->render();        
    }
    
    function refashion(){
        $this->render();
    }

    function remuxgetmore(){
        $this->render();
    }
    function winter2014(){
        $this->render();
    }
    function brandsonly(){
        $query = $this->Product_clothing_model->get_brand_list();
        $this->data = array('brands' => $query);
        $this->render();
    }
    function intern2015(){
        $this->render();
    }
    function message(){
        $this->render();
    }
    function sellchoice(){
        $this->render();
    }
   /* function newpage(){
        $this->render();
    }*/
    function newcart1(){
        $this->render();
    }
    function newcart2(){
        $this->render();
    }
    function editproduct(){
        $this->render();
    }
    function uploadproduct(){
        $this->render();
    }
    function washintro(){
        $this->render();
    }

}

