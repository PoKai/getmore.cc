<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller
{

    /**
     * new subject or event value
     *
     * @var int
     **/
    private $_news_count = 4;

    /**
     * sale subject or event value, order by sale time
     *
     * @var int
     **/
    private $_sales_count = 9;

    /**
     * Default __construct
     *
     * @author appleboy
     **/
    public function __construct()
    {
        parent::__construct();
        redirect('/');
        $this->load->library(array('lib_static_url', 'lib_subject', 'lib_config', 'lib_event', 'files_lib'));
        $this->template->set('is_home', true);
    }

    /**
     * Default home page
     *
     * @author appleboy
     **/
    public function index()
    {
        // set default value
        $top_image_list = $output = array();

        // get top image and link lists
        $top_image = $this->lib_config->top_image();
        $top_link = $this->lib_config->top_link();
        $i = 0;
        foreach ($top_image as $row) {
            $data = array(
                'image' => $row['file_name'],
                'link' => (isset($top_link[$i]['link'])) ? $top_link[$i]['link'] : '#',
                'target' => (isset($top_link[$i]['target']) and $top_link[$i]['target'] == true) ? '_blank' : '_self'
            );
            $top_image_list[$i] = $data;
            $i++;
        }

        // get product subject
        $subject = $this->lib_subject->select("files.file_name, category.tw_name as category_name, product_subjects.*, DATE_FORMAT(`up_time`, '%Y-%m-%d %H:%i') as up_time, DATE_FORMAT(`down_time`, '%Y-%m-%d %H:%i') as down_time")->where('`options`&' . (_IS_INDEX) . ' >= 1')->where('`options`&' . (_IS_ENABLED) . ' >= 1')->subjects()->result_array();
        $event = $this->lib_event->select("deadline_time, files.file_name, category.tw_name as category_name, events.*, DATE_FORMAT(`up_time`, '%Y-%m-%d %H:%i') as up_time, DATE_FORMAT(`down_time`, '%Y-%m-%d %H:%i') as down_time")->where('`options`&' . (_IS_INDEX) . ' >= 1')->where('`options`&' . (_IS_ENABLED) . ' >= 1')->events()->result_array();

        foreach ($subject as $row) {
            $up_time = ($row['up_time'] != '') ? strtotime($row['up_time']) : $this->_time;
            $down_time = ($row['down_time'] != '') ? strtotime($row['down_time']) : $this->_time;
            // check topic expire time
            if (!empty($row['down_time']) and $down_time < $this->_time) {
                continue;
            }

            $row['type'] = 'subject';
            $row['icon'] = 'buy-icon';
            $row['sold'] = true;
            $row['url'] = site_url('subject/single/' . $row['id']);
            $row['time'] = $this->system->get_time_out($down_time);
            $row['message'] = (!empty($row['time'])) ? '剩 '. $row['time'] . ' 將結束特價' : '優惠尚未開始或已經結束';

            while (isset($output_1[$up_time])) {
                $up_time += 1;
            }
            while (isset($output_2[$down_time])) {
                $down_time += 1;
            }
            $output_1[$up_time] = $row;
            $output_2[$down_time] = $row;
        }

        // get event
        foreach ($event as $row) {
            $up_time = ($row['up_time'] != '') ? strtotime($row['up_time']) : $this->_time;
            $deadline_time = ($row['deadline_time'] != '') ? strtotime($row['deadline_time']) : $row['edit_time'];
            // check event expire time
            if (!empty($row['deadline_time']) and $deadline_time < $this->_time) {
                continue;
            }

            $row['type'] = 'event';
            $row['icon'] = 'fun-icon';
            $row['sold'] = (($row['peoples'] - $row['participants']) <= 0) ? false : true;
            $row['sell_price'] = $row['price'];
            $row['url'] = site_url('event/single/' . $row['id']);
            $row['time'] = $this->system->get_time_out($deadline_time);
            $row['message'] = (!empty($row['time'])) ? '剩 '. $row['time'] . ' 將結束報名' : '報名尚未開始或已經結束';

            while (isset($output_1[$up_time])) {
                $up_time += 1;
            }
            while (isset($output_2[$deadline_time])) {
                $deadline_time += 1;
            }

            $output_1[$up_time] = $row;
            $output_2[$deadline_time] = $row;
        }

        // 最新商品：主題及活動依照最新上架時間排序
        krsort($output_1);
        $filter = $new_list_data = $sale_list_data = array();

        $i = 0;
        foreach ($output_1 as $k => $v) {
            if ($i == $this->_news_count) {
                break;
            }
            $key = $v['type'] . '_' . $v['id'];
            $filter[$key] = true;
            $new_list_data[] = $v;
            $i++;
        }

        // 限時特惠：主題依照下架時間，活動依照截止時間，越接近現在時間排在最前面
        ksort($output_2);
        $i = 0;
        foreach ($output_2 as $k => $v) {
            $key = $v['type'] . '_' . $v['id'];
            if (isset($filter[$key])) {
                continue;
            }
            $sale_list_data[] = $v;
            $i++;

            if ($i == $this->_sales_count) {
                break;
            }
        }

        $data = array(
            'new_list_data' => $new_list_data,
            'sale_list_data' => $sale_list_data,
            'count' => count($sale_list_data)
        );

        $this->template->set('top_image', $top_image);
        $this->template->set('top_image_count', count($top_image));
        $this->template->set('top_image_list', $top_image_list);
        $this->template->render('home/main', $data);

    }

    /**
     * Default Static Page
     *
     * @author appleboy
     **/
    public function url($name = null)
    {
        $this->template->set('is_hidden_header', true);
        if (empty($name)) {
            redirect('/', 'refresh');
        }

        $row = $this->lib_static_url->select('id, title, url, content')->where('url', $name)->items()->row_array();

        if (empty($row) or empty($row['content'])) {
            redirect('/', 'refresh');
        }

        $this->template->render('home/static', $row);
    }

}
