<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class New_page extends MY_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->spark('Twiggy/0.8.5');
        $this->twiggy->theme('');
        $this->load->library(array('lib_static_url', 'lib_subject', 'lib_config', 'lib_event', 'files_lib'));
        $fburl = site_url('/auth/facebook?redirect_url=' . urlencode(current_url()));
        $islogin = 1;
        $nickname;
        // if (!$this->session->userdata('user_id'))
        //     $islogin = 0;

        $this->load->database();
        // $nickname = $this->db->from('users')->where('id', $this->session->userdata('user_id'))->get()->result();
        // $this->twiggy->set(array(
        //     'islogin'  => $islogin,
        //     'nickname' => $nickname[0]->nickname,
        //     'fburl'    => $fburl
        // ));


    }
    
    public function index() {
        $this->twiggy->template('home/new_page')->display();
    }

    public function closet($url_code) {
        $photo = $this->db->from('closet')->where('url_code', $url_code)->get()->result();
        $user_id = $photo[0]->user_id;
        $trade = $this->db->from('trade_rule')->where('user_id', $user_id)->get()->result();
        $clothing_plan_a = (array)$this->db->from('marketplace')->where('user_id', $user_id)->get()->result();
        $this->twiggy->set(array(
            'banner_photo_file_id'    => $photo[0]->banner_photo_file_id,
            'profile_photo_file_id'   => $photo[0]->profile_photo_file_id,
            'trade_time'              => $trade[0]->time,
            'trade_method'            => $trade[0]->method,
            'trade_transport'         => $trade[0]->transport, 
            'trade_returns'           => $trade[0]->returns,
            'clothing_plan_a'         => $clothing_plan_a
        ));
        $this->twiggy->template('home/new_page')->display();
    }

}

?>