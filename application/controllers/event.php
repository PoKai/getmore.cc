<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        redirect('/');
        $this->load->library(array('lib_cart', 'lib_event', 'lib_product', 'lib_category', 'files_lib'));
        $this->list_array = array('detail');
        $this->template->set('is_hidden_header', true);
        $this->template->set('is_event', true);
    }

    public function index()
    {
        redirect('/');
    }

    /**
     * Event list
     *
     * @param  int
     * @author appleboy
     **/
    public function lists($id = '')
    {
        $return = $this->lib_event->get_list(array('id' => $id));

        if ($return['count'] == 0) {
            redirect('/error404');
        }

        $data = array(
            'count' => $return['count'],
            'output' => $return['output']
        );

        $this->template->render('event/lists', $data);
    }

    /**
     * Single Event
     *
     * @param  int
     * @return json
     * @author appleboy
     **/
    public function single($id = '')
    {
        $id = intval($id);
        ($id <= 0) and redirect('/');

        $row = $this->lib_event->select('events.id, title, price, file_list, product_list, detail, content, introduction, peoples, participants, event_time, category.tw_name as category_name, DATE_FORMAT(`up_time`, "%Y-%m-%d %h:%i") as up_time, DATE_FORMAT(`down_time`, "%Y-%m-%d %h:%i") as down_time, options')->where('events.id', $id)->events()->row_array();

        // empty row or isn't enable
        if (empty($row) or !(_IS_ENABLED & $row['options'])) {
            redirect('/');
        }

        if ($row['file_list'] != '') {
            $files = $this->files_lib->select('file_name')->where('file_id', explode(',', $row['file_list']))->order_by_field('file_id', $row['file_list'])->files()->result_array();
            $row['image_list'] = $files;
        }
        if ($row['product_list'] != '') {
            $output = array();
            $products = $this->lib_product->select('products.id, products.title, products.description, products.original_price, products.sell_price, products.inventory_amount, products.file_list')->where('products.id', explode(',', $row['product_list']))->products()->result_array();
            foreach ($products as $item) {
                if ($item['file_list'] != '') {
                    $files = $this->files_lib->select('file_name')->where('file_id', explode(',', $item['file_list']))->order_by_field('file_id', $item['file_list'])->files()->result_array();
                    $item['image_list'] = $files;
                }
                $output[] = $item;
            }
            $row['product_list'] = $output;
        }
        // handle detail informtion
        foreach ($this->list_array as $item) {
            // set default value
            $row[$item] = empty($row[$item]) ? array() : json_decode($row[$item]);
        }
        // handle count dropdown
        $count = $row['peoples'] - $row['participants'];
        $row['order_count'] = $this->lib_cart->format_dropdown($count, 'event');

        // set default value
        ($row['product_list'] == "") and $row['product_list'] = array();
        ($row['file_list'] == "") and $row['image_list'] = array();

        // handle register message and status
        $row['status'] = 0;
        if (!empty($row['deadline_time']) and strtotime($row['deadline_time']) < $this->_time) {
            $row['message'] = '報名時間以截止';
        } else {
            if ($row['participants'] >= $row['peoples']) {
                $row['message'] = '活動已額滿';
            } else {
                $row['status'] = 1;
            }
        }

        $this->template->render('event/single', $row);
    }
}
