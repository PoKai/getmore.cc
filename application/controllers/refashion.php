<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Refashion extends MY_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->library(array('ion_auth'));
        if (!$this->ion_auth->logged_in()) {
            $this->system->redirect();
        }
        $this->load->model('Refashion_model');
        $this->load->library('files_lib');
    }
    
    function index(){
        $this->template->render('refashion/index', []);      
    }
    
    function send(){
        $data = $this->input->post();
        $data['user_id'] = $this->session->userdata('user_id');
        $data['file_id'] = $this->files_lib->straight_upload();
        $id = $this->Refashion_model->insert($data);
        $this->template->render('refashion/send', []);      
    }
      
}
