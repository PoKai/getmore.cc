<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edm extends MY_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->library(array('ion_auth', 'user_agent'));
        $this->load->database();
        $this->load->model('User_model');
        $this->load->model('Edm_vip_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation', 'session'));
    }

    public function index(){
        $this->_validate();

        $profile = $this->ion_auth->profile();

        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $result = $this->_handle_post();
        }
        else {
            $user_data = $this->User_model->db->select('user_name, email, mobile_phone, sex')->where('id', $profile['id'])->get('users')->row_array();
        }
        
        $this->template->render('edm/index', array(
            'user' => $user_data,
            'success' => $result
        )); 
    }

    private function _handle_post(){
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('mobile_phone', 'Mobile Phone', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_rules('company', 'Company', 'required');
        $this->form_validation->set_rules('industry', 'Industry', 'required');

        if ($this->form_validation->run() == FALSE){
            return false;
        }
        else{
            $this->Edm_vip_model->insert_data(array_merge($this->input->post(), array(
                'user_id' => $this->ion_auth->profile()['id']
            )));

            // redirect('/edm', 301);
            return true;
        }
    }

    private function _validate(){
        if($this->Edm_vip_model->get_all_count() >= 15){
            $data['title'] = "很抱歉 名額已滿";
            $data['message'] = "";
            $this->template->render('sellorder/info/404', $data);
            return;
        }

        if (!$this->ion_auth->logged_in()) {
            $this->system->redirect();
        }
/*
        if(!$this->ion_auth->is_vip()){
            $data['title'] = "很抱歉 您沒收到邀請喔";
            $data['message'] = "";
            $this->template->render('sellorder/info/404', $data);
            return;
        }
*/
        if($this->Edm_vip_model->has_register($this->ion_auth->profile()['id'])){
            $data['title'] = "恭喜您 您已經註冊成功";
            $data['message'] = "";
            $this->template->render('sellorder/info/404', $data);
            return;
        }
    }
}
