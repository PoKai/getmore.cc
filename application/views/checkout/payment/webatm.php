<form action="<?php echo $payment_url; ?>" method="POST" id="payment">
    <input type=hidden name="merchantnumber" value="<?php echo $merchantnumber; ?>">
    <input type=hidden name="ordernumber" value="<?php echo $ordernumber; ?>">
    <input type=hidden name="amount" value="<?php echo $amount; ?>">
    <input type=hidden name="paymenttype" value="<?php echo $paymenttype; ?>">
    <input type=hidden name="bankid" value="<?php echo $bankid; ?>">
    <input type=hidden name="hash" value="<?php echo $hash; ?>">
    <input type=hidden name="nexturl" value="<?php echo $nexturl; ?>">
</form>
<script>
    document.getElementById("payment").submit();
</script>
