<link rel="stylesheet" href="/assets/css/newcart.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<div class='pagecontent'>
  <div class='totalpricebox'>
    <div class='well totalprice'>
      <p>本次消費總金額共 $<?=$items_price?> 元 <? if($account_balance): ?>，帳戶折抵 $<?=$account_balance?> 元 <? endif; ?><? if($bonus_points): ?>，紅利折抵 $<?=(int) ($bonus_points / 100)?> 元<? endif; ?><? if($coupon_amount): ?>，優惠代碼折抵 $<?=$coupon_amount?> 元<? endif; ?><? if($shipping_price): ?>，運費 $<?=$shipping_price;?> 元<? endif; ?>。<br>
        <span style='font-size:10px; color:#bbb;'>(提醒您，商品來自不同賣家可能個別出貨，請留意手機來電！) </span>
      </p>
      <div class='endprice'>總計 $<?=$total_price?>元</div>
    </div>
  </div>
  <hr>
  <form method="post" action="/checkout/third" id="order_info">
  <div class="panel panel-success check-info check-panel">
    <div class="panel-body-title check-info">
      <div class="check-info-title text-center">
          <div>填入寄送資料</div>
      </div>
    </div>
    <div class="panel-body check-info">
        <div class="check-info-left">
            <div class="checksubtitle text-center">
              訂購人資料
            </div>
            <div class="form-group">
              <label for="putname">中文姓名<sup>*必填</sup></label><br>
              <div class="nameandgender">
                <div class="namebox">
                <input type="text" name="buyer[name]" class="form-control required" placeholder="請填寫中文姓名">
                </div>
                <div class="genderbox">
                  <label>
                    <input type="radio" name="buyer[gender]" value="0" checked="checked">小姐
                  </label>
                  <label>
                    <input type="radio" name="buyer[gender]" value="1">先生
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="putphone">聯絡電話<sup>*請填入聯絡到您的電話</sup></label>
              <div class="phonebox">
                <input type="tel" name="buyer[telephone]" class="form-control required tel" placeholder="請填寫聯絡電話">
              </div>
            </div>
            <div class="form-group">
              <label for="putname">地址<sup>*必填</sup></label><br>
              <div class="nameandgender">
                <div id="buyer_twzipcode">
                  <div class="address-city">
                    <div data-role="county" data-name="buyer[county]" data-style="form-control" data-value="台北市"></div>
                  </div>
                  <div class="address-city">
                    <div data-role="district" data-name="buyer[district]" data-style="form-control" data-value="大安區"></div>
                  </div>
                  <div class="address-zip">
                    <div data-role="zipcode" data-name="buyer[zipcode]" data-style="form-control" data-value="106"></div>
                  </div>
                </div>
                <div class="addressdetail">
                    <input type="text" name="buyer[address]" class="form-control required" placeholder="請填寫詳細地址">
                </div>
              </div>
            </div>
        </div>
        <div class="check-info-right">
            <div class="checkbox aspay">
              <label>
                <input type="checkbox" id="fill_receiver_from_buyer"> 同訂購人
              </label>
            </div>
            <div class="checksubtitle text-center">
              收件人資料
            </div>
            <div class="form-group">
              <label for="putname">中文姓名<sup>*必填</sup></label><br>
              <div class="nameandgender">
                <div class="namebox">
                <input type="text" name="receiver[name]" class="form-control required" placeholder="請填寫中文姓名">
                </div>
                <div class="genderbox">
                  <label>
                    <input type="radio" name="receiver[gender]" value="0" checked="checked">小姐
                  </label>
                  <label>
                    <input type="radio" name="receiver[gender]" value="1">先生
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="putphone">聯絡電話<sup>*請填入聯絡到收件人的電話</sup></label>
              <div class="phonebox">

                <input type="tel" name="receiver[telephone]" class="form-control required tel" placeholder="請填寫聯絡電話">
              </div>
            </div>
            <div class="form-group">
              <label for="putname">地址<sup>*必填</sup></label><br>
              <div class="nameandgender">
                <div id="receiver_twzipcode">
                  <div class="address-city">
                    <div data-role="county" data-name="receiver[county]" data-style="form-control" data-value="台北市"></div>
                  </div>
                  <div class="address-city">
                    <div data-role="district" data-name="receiver[district]" data-style="form-control" data-value="大安區"></div>
                  </div>
                  <div class="address-zip">
                    <div data-role="zipcode" data-name="receiver[zipcode]" data-style="form-control" data-value="106"></div>
                  </div>
                </div>
                <div class="addressdetail">
                    <input type="text" name="receiver[address]" class="form-control required" placeholder="請填寫詳細地址">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="delivery_time">指定寄送時間</label>
              <div>
                <select name="receiver[delivery_time]" id="delivery_time">
                  <option value="any">不指定</option>
                  <option value="0-12">中午前</option>
                  <option value="12-17">12~17</option>
                  <option value="17-20">17~20</option>
                </select>
              </div>
            </div>
        </div>
    </div>
  </div>
  <? if($total_price): ?>
  <div class="panel panel-success check-info check-panel">
    <div class="panel-body-title check-info">
      <div class="check-info-title text-center">
          <div>選擇付款方式</div>
      </div>
    </div>
    <div class="panel-body-payment check-info text-center">
      <div class="cart_3_pay">
        <div class="cart_3_choose_pay">
            <label>
              <input type="radio" name="payment_method" value="credit-cart" checked="checked" id="credit-cart">信用卡<br>
              <img src="/assets/images/checkout/credit_card.png">
            </label>
        </div>
        <div class="cart_3_choose_pay">
          <label>
            <input type="radio" name="payment_method" value="atm">ATM轉帳<br>
            <img src="/assets/images/checkout/atm.png">
          </label>
        </div>
        <div class="cart_3_choose_pay">
          <label>
            <input type="radio" name="payment_method" value="pay-at-home" <? if($no_pay_at_home): ?> disabled <? endif; ?>>貨到付款 <span>&nbsp; <a id="question-popover" tabindex="0" data-container="body" role="button" data-toggle="popover" data-trigger="focus"><i class="fa fa-question-circle lg"></i></a></span><br>
            <img src="/assets/images/checkout/bcat.png">
          </label>
        </div>
      </div>
    </div>
  </div>
  <? endif; ?>
  <hr>
    <div class='totalpricebtn'>
      <div class='text-center'>
          <a href="/checkout/first" class="btn btn-default btn-lg">上一步，確認訂單</a> &nbsp
          <input class="btn btn-primary btn-lg" type="submit" value="下一步，確認付款" />
      </div>
    </div>
</div>
</form>
<script type="text/javascript" src="/assets/vendor/jquery-twzipcode/jquery.twzipcode.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.min.js"></script>
<script type="text/javascript">
jQuery.validator.addMethod('tel', function(value, element){
  return this.optional(element) || value.length >= 9 && /^[0-9]+$/.test(value);
});

var validator = $("#order_info").validate({
  errorPlacement: function(error, element){
    $(element).parent('div').addClass('has-error');
  },
  success: function(label, element){
    $(element).parent('div').removeClass('has-error');
  }
});

$('#buyer_twzipcode,#receiver_twzipcode').twzipcode({
  readonly: true
});

$('#fill_receiver_from_buyer').bind('click', function(e){
  if($(this).prop('checked')){
    $('input[name="receiver[name]"]').val($('input[name="buyer[name]"]').val());

    validator.element('input[name="receiver[name]"]');

    if($('input[name="buyer[gender]"][value=1]').prop('checked')){
      $('input[name="receiver[gender]"][value=1]').prop('checked', true);
    }
    else {
      $('input[name="receiver[gender]"][value=0]').prop('checked', true);
    }

    $('input[name="receiver[telephone]"]').val($('input[name="buyer[telephone]"]').val());

    validator.element('input[name="receiver[telephone]"]');

    $('option[value='+$('select[name="buyer[county]"]').val()+']', 'select[name="receiver[county]"]').prop('selected', true).trigger('change');

    $('option[value='+$('select[name="buyer[district]"]').val()+']', 'select[name="receiver[district]"]').prop('selected', true).trigger('change');

    $('input[name="receiver[address]"]').val($('input[name="buyer[address]"]').val());

    validator.element('input[name="receiver[address]"]');
  }
});
</script>
<script>
$(function () {
  $('#question-popover').popover({
    trigger: 'hover click',
    'content': '<span style="font-size:10px">商品包含「狂熱市集」，或是結帳金額大於$10,000，無法使用貨到付款結帳。</span>',
    'html': true
  });
})
</script>