 <link rel="stylesheet" href="/assets/css/newcart.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<div class='pagecontent'>
</div>

<script type="text/javascript">
    var soldout_clothings = null;
    <? if(!empty($soldout_clothings)): ?>
    soldout_clothings = <?=json_encode($soldout_clothings);?>;
    <? endif; ?>
</script>

<script type="text/template" id="artist-list-template">
    <div class='totalpricebox'>
        <div class='well totalprice'>
          <p>本次消費總金額共 $<%= itemsPrice %> 元<% if(account_balance){ %>，帳戶折抵 <%= account_balance %> 元<% }if(bonus_points){ %>，紅利折抵 $<%= parseInt(bonus_points/100, 10) %>元<% }if(coupon.amount){ %>，折扣碼折抵$<%= coupon.amount %>元<% }if(shippingPrice){ %>，運費 $<%= shippingPrice %> 元<% } %>。<br>
            <span style='font-size:10px; color:#bbb;'>(提醒您，商品來自不同賣家可能個別出貨，請留意手機來電！) </span>
          </p>
          <div class='endprice'>總計$<%= totalPrice %>元</div>
        </div>
      </div>
      <div class='totalpricebox'>
        <div class='well totalprice'>
            <p><i class="fa fa-gratipay fa-lg red"></i>&nbsp有折扣碼？<input type="text" name="coupon_code" class="form-control coupon_code" placeholder="請輸入折扣碼" value="<%= coupon.code %>"><button class="btn btn-info btn-xs coupon-btn">確定</button>可折抵<span class="coupon_amount"> <%= coupon.amount %> </span>元。</p>
            <? if($bonus_points->bonus): ?><p><i class="fa fa-gratipay fa-lg red"></i>&nbsp您的紅利G幣共有<span class="bonus_points" data-value="<%= <?=$bonus_points->bonus;?>-bonus_points %>"><%= <?=$bonus_points->bonus;?>-bonus_points %></span>點&nbsp<button class="btn btn-xs btn-info deduct_bonus">按我<% if(bonus_points){ %>取消<% } %>折抵</button><!--<a href='#' class='label label-primary'>了解更多</a>--></p><? endif; ?>
            <? if($account_balance->balance): ?><p><i class="fa fa-gratipay fa-lg red"></i>&nbsp您的帳戶共有<span class="account_balance" data-value="<%= <?=$account_balance->balance;?>-account_balance %>"><%= <?=$account_balance->balance;?>-account_balance %></span>元，請輸入抵扣金額<input type="text" name="deduction_amount" class="form-control deduction_amount" value="<%= account_balance %>" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57">元。&nbsp<button class="btn btn-xs btn-info deduction_amount">確定</button><span style="color:red;margin-left:25px;">＊輸入金額後請按下「確定」</span><!--<a href='#' class='label label-primary'>了解更多</a>--></p><? endif; ?>
        </div>
      </div>
    <hr>
  <% if(!_.isEmpty(getmore)){ %>
  <div class="carttitle">
    賣家寄倉商品 / BRANDS ONLY
  </div>
  <div class="cartbplan">
    <table class="table table-condensed billtable">
     <thead>
      <tr>
        <th></th><th>商品編號</th><th>商品敘述</th><th>單價</th><th>件數</th><th>移除</th>
      </tr>
    </thead>
     <tbody>
        <% _.each(getmore.items, function(item){ %>
            <tr <% if(item.get('lock_status') || item.get('Stock') && !item.get('Stock').number){ %> style="background-color: #f9f2f4;" <% } %> id="<%= item.get('product_item_number') %>">
                <th>
                    <img class="cartimg" src="http://s3.getmore.cc/clothing/<%= item.get('files')[0] %>/medium.jpg">
                </th>
                <th>
                    <%= item.get('product_item_number') %>
                    <br>
                    <% if(item.get('lock_status') || item.get('Stock') && !item.get('Stock').number){ %>
                    被搶先購買了！請先<a href="#" class="delete_item" data-id="<%= item.get('id') %>">移除</a>再進行下一步
                    <% } else { %>
                    <a href="#" class="buy-next-time" data-id="<%= item.get('id') %>">下次再買，先加入欲望清單</a>
                    <% } %>
                </th>
                <th>
                    <%= item.get('product_title') %>(<%= item.get('color') %>)
                </th>
                <th>
                    <%= item.get('product_sell_price') %>
                </th>
                <th>
                    <% if (item.get('Stock') && item.get('Stock').number) { %>
                    <select class="number" data-product_id="<%= item.get('product_id') %>" data-launch_way="<%= item.get('launch_way') %>">
                    <% _.each(_.range(1, item.get('Stock').number+1), function(value){ %>
                      <option value="<%= value %>" <% if(_.isEqual(item.get('number'), value)){ %> selected="selected" <% } %> ><%= value %></option>
                    <% }) %>
                    </select>
                    <% } else { %>
                      1
                    <%} %>
                </th>
                <th>
                    <a href="#" class="delete_item" data-id="<%= item.get('id') %>"><i class="fa fa-times fa-lg"></i></a>
                </th>
            </tr>
        <% }); %>
     </tbody>
    </table>
    <div class="subtotal">
      <div class="panel panel-default everyreturn">
          <div class="panel-heading">
            <h3 class="panel-title">退貨條款</h3>
          </div>
          <div class="panel-body" style="font-size:10px; margin-bottom:10px;">
          品牌代售 Getmore 僅提供賣家統一出貨等服務，所有權為個人賣家所有。在收到商品後，欲退貨請於5天內填寫退貨申請單Email至客服，物流人員將與您聯繫或登門收件，電話聯絡2次或超過2日未能向您收貨視放棄退貨。
          來回運費由買家負擔，單趟運費以$80計算，運費將於退貨金額中扣除 。
          </div>
      </div>
      <div class="pull-right">
        <table style="width:300px;">
          <tbody>
            <tr>
              <td class="cart_1_money">
                  <p>
                    商品件數<span class="cart_1_count"><%= getmore.count %></span>件，
                    商品金額<span class="cart_1_price"><%= getmore.total_price %></span>元
                  </p>
                  <p>
                    運費金額 <span class="cart_1_tran"><%= getmore.shipping_price %></span>元
                  </p>
              </td>
            </tr>
            <tr>
              <td class="cart_1_money">
                  <p class="cart_1_sum_block">
                    消費小計NT$<span class="cart_1_sum"><%= getmore.total_price + getmore.shipping_price %></span>元
                  </p>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
  </div>
  <hr>
  <% } %>
  <% if(!_.isEmpty(others)){ %>
    <div class="carttitle">
        賣家自出商品 / MARKETPLACE
    </div>
  <% } %>
  <% _.each(others, function(other){ %>
    <div class="cartbplan">
    <div>
        <p>
            賣家：<a href="#" target="_blank"><%= other.title %></a>
        </p>
    </div>
    <table class="table table-condensed billtable">
        <thead>
            <tr>
                <th></th><th>商品編號</th><th>商品敘述</th><th>單價</th><th>件數</th><th>移除</th>
            </tr>
        </thead>
        <tbody>
            <% _.each(other.items, function(item){ %>
            <tr <% if(item.get('lock_status') || item.get('Stock') && !item.get('Stock').number){ %> style="background-color: #f9f2f4;" <% } %> id="<%= item.get('product_item_number') %>">
                <th>
                    <img class="cartimg" src="http://s3.getmore.cc/clothing/<%= item.get('files')[0] %>/medium.jpg">
                </th>
                <th>
                    <%= item.get('product_item_number') %>
                    <br>
                    <% if(item.get('lock_status') || item.get('Stock') && !item.get('Stock').number){ %>
                    被搶先購買了！請先<a href="#" class="delete_item" data-id="<%= item.get('id') %>">移除</a>再進行下一步
                    <% } else { %>
                    <a href="#" class="buy-next-time" data-id="<%= item.get('id') %>">下次再買，先加入欲望清單</a>
                    <% } %>
                </th>
                <th>
                    <%= item.get('product_title') %>(<%= item.get('color') %>)
                </th>
                <th>
                    <%= item.get('product_sell_price') %>
                </th>
                <th>
                    <% if(item.get('Stock') && item.get('Stock').number){ %>
                    <select class="number" data-product_id="<%= item.get('product_id') %>" data-launch_way="<%= item.get('launch_way') %>">
                    <% _.each(_.range(1, item.get('Stock').number+1), function(value){ %>
                      <option value="<%= value %>" <% if(_.isEqual(item.get('number'), value) || _.isEqual(item.get('Stock').number, value) && item.get('Stock').number <= item.get('number')){ %> selected="selected" <% } %> ><%= value %></option>
                    <% }) %>
                    </select>
                    <% } else { %>
                    1
                    <% } %>
                </th>
                <th>
                    <a href="" class="delete_item" data-id="<%= item.get('id') %>"><i class="fa fa-times fa-lg"></i></a>
                </th>
            </tr>
            <% }); %>
        </tbody>
    </table>
      <div class="subtotal">
        <div class="panel panel-default everyreturn">
            <div class="panel-heading">
              <h3 class="panel-title">退貨條款</h3>
            </div>
            <div class="panel-body" style="font-size:10px; margin-bottom:10px;">
              <%= other.trade_rule.returns %>
            </div>
        </div>
        <div class="pull-right">
          <table style="width:300px;">
            <tbody>
              <tr>
                <td class="cart_1_money">
                    <p>
                      商品件數<span class="cart_1_count"><%= other.count %></span>件，
                      商品金額<span class="cart_1_price"><%= other.total_price %></span>元
                    </p>
                </td>
              </tr>
              <tr>
                <td class="cart_1_money">
                    <p class="cart_1_sum_block">
                      消費小計NT$<span class="cart_1_sum"><%= other.total_price %></span>元
                    </p>
                </td>
              </tr>
              </tbody>
            </table>
          </div>
      </div>
  </div>
  <% }); %>
 <% if(!_.isEmpty(others)){ %>
 <hr>
 <% } %>
 <% if(!_.isEmpty(designers)){ %>
    <div class="carttitle">
        設計師商品 / Getmore select
    </div>
  <% } %>
  <% _.each(designers, function(designer){ %>
    <div class="cartbplan">
    <div>
        <p>
            賣家：<a href="#" target="_blank"><%= designer.user_id %></a>
        </p>
    </div>
    <table class="table table-condensed billtable">
        <thead>
            <tr>
                <th></th><th>商品編號</th><th>商品敘述</th><th>單價</th><th>件數</th><th>移除</th>
            </tr>
        </thead>
        <tbody>
            <% _.each(designer.items, function(item){ %>
            <tr <% if(item.get('lock_status') || item.get('Stock') && !item.get('Stock').number){ %> style="background-color: #f9f2f4;" <% } %> id="<%= item.get('product_item_number') %>">
                <th>
                    <img class="cartimg" src="http://s3.getmore.cc/clothing/<%= item.get('files')[0] %>/medium.jpg">
                </th>
                <th>
                    <%= item.get('product_item_number') %>
                    <br>
                    <% if(item.get('lock_status') || !item.get('Stock').number){ %>
                    被搶先購買了！請先<a href="#" class="delete_item" data-id="<%= item.get('id') %>">移除</a>再進行下一步
                    <% } else if(item.get('Stock').number < item.get('number')) { %>
                    <p style="color: red;">目前僅剩 <%= item.get('Stock').number %> 件喔!!</p>
                    <% } else { %>
                    <a href="#" class="buy-next-time" data-id="<%= item.get('id') %>">下次再買，先加入欲望清單</a>
                    <% } %>
                </th>
                <th>
                    <%= item.get('product_title') %>(<%= item.get('color') %>)
                </th>
                <th>
                    <%= item.get('product_sell_price') %>
                </th>
                <th>
                    <% if(item.get('Stock') && item.get('Stock').number){ %>
                    <select class="number" data-product_id="<%= item.get('product_id') %>" data-launch_way="<%= item.get('launch_way') %>">
                    <% _.each(_.range(1, item.get('Stock').number+1), function(value){ %>
                      <option value="<%= value %>" <% if(_.isEqual(item.get('number'), value) || _.isEqual(item.get('Stock').number, value) && item.get('Stock').number <= item.get('number')){ %> selected="selected" <% } %> ><%= value %></option>
                    <% }) %>
                    </select>
                    <% } else { %>
                    1
                    <% } %>
                </th>
                <th>
                    <a href="" class="delete_item" data-id="<%= item.get('id') %>"><i class="fa fa-times fa-lg"></i></a>
                </th>
            </tr>
            <% }); %>
        </tbody>
    </table>
      <div class="subtotal">
        <div class="panel panel-default everyreturn">
            <div class="panel-heading">
              <h3 class="panel-title">退貨條款</h3>
            </div>
            <div class="panel-body" style="font-size:10px; margin-bottom:10px;">
              <%= designer.trade_rule.returns %>
            </div>
        </div>
        <div class="pull-right">
          <table style="width:300px;">
            <tbody>
              <tr>
                <td class="cart_1_money">
                    <p>
                      商品件數<span class="cart_1_count"><%= designer.count %></span>件，
                      商品金額<span class="cart_1_price"><%= designer.total_price %></span>元
                    </p>
                    <p>
                      運費金額 <span class="cart_1_tran"><%= designer.shipping_price %></span>元
                    </p>
                </td>
              </tr>
              <tr>
                <td class="cart_1_money">
                    <p class="cart_1_sum_block">
                      消費小計NT$<span class="cart_1_sum"><%= designer.total_price + designer.shipping_price %></span>元
                    </p>
                </td>
              </tr>
              </tbody>
            </table>
          </div>
      </div>
  </div>
  <% }); %>
  <hr>
    <div class='totalpricebtn'>
      <form method="post" action="/checkout/second">
        <input type="hidden" name="json_data" value=<%= JSON.stringify({clothings: clothings, account_balance: account_balance, bonus_points: bonus_points, coupon_code: coupon.code}) %> />
        <div class='text-center'>
            <a href="/" style="vertical-align:bottom;font-size:10px;color:#bbb;margin-right:5px;">繼續購物</a>
            <input type="submit" class="btn btn-primary btn-lg" id="go_second_step" value="下一步，填寫資訊" />
        </div>
      </form>
    </div>
</script>