<img class='banner-s' src="/assets/images/banner-s/sellorder.jpg">
<div class='row' id='pagerow'>    
    <div style=" width:680px;margin:0 auto; overflow:auto;">
        <h4><img id='key_icon' src="/assets/images/secondhand_management/bottom_key.png" alt="" />權限開通</h4>
        <hr class="bold_hr" />
        <?php echo alert_message($message) ?>
        <h3>為確保正確將售後所得交給您,請完成以下權限開通程序</h3>
        <form id="verify" action="/verify/save_personal_information" method="POST" role='form'>
            <div class="form-group col-md-7">
                <label for="chinese_name">中文姓名</label>
                <input type="text" class="form-control" id="chinese_name" name="chinese_name">
                <p class='description'>請正確填入個人身分證上真實姓名。</p>
            </div>
            
            <div class="form-group col-md-7">
                <label><input type="radio" name="gender" class='gender' id="female" value="0">小姐</label>
                <label><input type="radio" name="gender" class='gender' id="male" value="1">先生</label>				
            </div>
						
            <div class="form-group col-md-7">
                <label>生日</label>
                <div id="birthday_picker"></div>
                <input type="hidden" name="birthday" id="birthday">
            </div>
            
            <div class="form-group col-md-7">
                <label id='label_for_id' for="id">身份證字號</label>
								<input type="text" class="form-control" name="id" id="id" maxlength="10">
                <p class='description'>請正確填入身份證上身分證字號。</p>
            </div>
            
            <div class="form-group col-md-7">
                <label>戶籍地址</label>
                <div id="twzipcode" style="overflow: auto;">
                    <div data-role="county"
                         data-name="user_county"
                         data-value="台北市"
                         data-style="form-control" style="float: left;">
                    </div>
                    <div data-role="district"
                         data-name="user_district"
                         data-value="大安區"
                         data-style="form-control" style="float: left;">
                    </div>
                    <div data-role="zipcode"
                         data-name="user_zipcode"
                         data-value="160"
                         data-style="form-control" style="float: left;width: 173.6px">
                    </div>
                    <div>
                        <input type="text" name="user_address" class="form-control">
                    </div>
                </div>
            </div>
            
            <div class="form-group col-md-7">
                <label for="phone">手機號碼</label>
								<input type="text" class="form-control" name="phone" id="phone">
								<p class='description'>簡訊認證用。</p>
            </div>
            
            <div class="checkbox col-md-7">
								<label>
                    <input type="checkbox" id="chk_read"> 我已詳細閱讀並同意使用條款
								</label>
            </div>
            
            <div class="termwindow col-md-7">
                <p>二手衣代洗代售服務條款</p>
                <p>一、	在您開始使用GetMore 網站（http://getmore.cc，以下稱本網站）所提供之所有服務前，請您先閱讀本網站所公佈之所有條款，一旦開始使用，代表您已閱讀所有條文內容且接受條文規定。並且，建議您常查閱服務條款，GetMore 有權隨時更改所有條文內容，若您不接受，請立即停止使用服務。</p>
                <p>二、	本網站提供之二手衣代洗代售服務，包含以下所稱之「代洗服務」及「代售服務」</p>
                <p>三、	代洗服務：</p>
                <p>(一)	本網站所提供之二手衣代洗代售服務，為保證所有販售二手衣皆為清洗過，所有代售二手衣皆需經過代洗服務過程，代洗過程未詳載之事項悉依經濟部公布之「洗衣定型化契約」辦理。</p>
                <p>(二)	代洗費用</p>
                <p>代洗費用依照類別分級收費，不論品牌，賣家於提出需求時預先支付。若待洗衣物檢查到有瑕疵，或類別或數量不符情形，客服人員將以電話、手機簡訊或是Email擇一告知賣家，並重新計算代洗費用，若與預先支付之代洗費用有所出入，差額則轉入會員帳戶中紀錄，若差額為正，賣家可提出申請領回，若差額為負，本網站有權請求再付款。若賣家不同意本網站重新計算之費用，賣家可以取消代洗服務，惟上述過程中若有手續費支出，須由賣家負擔。</p>
                <p>(三)	賠償責任</p>
                <p>若遇衣物不能返還或衣物毀損時之賠償數額，依照悉依經濟部公布之「洗衣定型化契約」辦理。</p>
                <p>四、	代售服務</p>
                <p>(一)	參考原價及商品售價之提出</p>
                <p>賣家在提出二手衣代洗代售申請時，必須提出原購買時之參考原價區間，並且依二手衣狀態、款式、稀有程度等因素，由賣家自行訂定「售價」。</p>
                <p>(二)	代售手續費</p>
                <p>代售手續費係指賣家支付本網站提供代售服務，包含拍照、上架、理貨、包裝、出貨等程序之服務費用。依以下方式收取：</p>
                <p>1.	若二手衣商品售出：於售出後，由本網站直接由售出所得中扣取。</p>
                <p>2.	若於代售期間屆滿時，二手衣商品未售出：本網站不收取任何代售手續費。</p>
                <p>3.	若於代售期間內賣家主動取消代售服務：賣家應支付本網站運費等一定金額之手續費。</p>
                <p>(三)	代售期間</p>
                <p>二手衣代售期間為六個月，自賣家提出代洗代售服務之申請日開始計算，代售期滿若未受出，依照代洗代售申請時賣家提供之寄回地址送回，若賣家於申請時選擇捐出，則所有權歸於咩斯特數位整合有限公司，咩斯特數位整合有限公司有權選定需要的機構進行無償捐出。若賣家於代洗代售期間內主動取消服務，則必須支付運費手續費等相關費用，若本網站於代洗代售期間內主動取消代售服務，賣家無需支付運費。</p>
                <p>五、	正確提供資訊及不得侵權義務</p>
                <p>賣家有提供本網站正確衣物資訊之責任，包含商品敘述、品牌、參考原價、欲售價格等，並且瞭解到所有資訊必須負擔相關法律責任，若後續出售發現問題，買家有權追責，若因此造成本網站損害，賣家應負賠償責任。又本網站嚴禁代售任何侵權商品，賣家在提出代售申請時，必須清楚來源，並且負完全責任。</p>
                <p>六、	網站行銷</p>
                <p>賣家同意本網站得為行銷本網站之目的，使用本網站為代洗代售商品所拍攝之照片、文字資料等，賣家亦同意本網站得揭露使用賣家部分資訊（如使用之暱稱），以供本網站行銷使用。</p>
                <p>七、	銷售發票開立</p>
                <p>二手衣銷售依國稅局規定，由咩斯特數位整合有限公司統一開立發票。並依照賣家二手衣銷售所得，寄發「個人一時貿易資料申報表」。</p>
                <p>八、	合約轉移</p>
                <p>賣家瞭解並同意，如果本網站將其相關之權利義務關係及交易等合約移轉予第三人，則本網站之權利義務關係、相關交易、以及相關資料等，包括且不限於相關之權利義務關係等，亦得一併移轉予該第三人承受、並由該第三人繼續提供服務。</p>
                <p>九、	條款修改</p>
                <p>GetMore網站保留彈性調整二手衣代售服務條款之權利，調整內容即公佈於網頁，不另行通知賣家。</p>
                <p>十、	準據法及管轄</p>
                <p>因本條款所生之爭議，雙方同意以中華民國法律為準據法，並以臺灣臺北地方法院為第一審管轄法院。</p>
            </div>
							
            <div id='submit' class="col-md-7">
                <button class="btn btn-info " type='button' id="btnverify" data-form="verify">確認</button>
            </div>
        </form>

    </div>
</div>

<script>
    // since the module here doesn't seem to be reusable,
    // I just write the script in page.
    $(document).ready(function(){
        /*
         * Initialization Process
         */
        $.cookie.json = true;
        
        // initialize the birthday plugin
        var bir = $.cookie('birthday') || "1985-06-15";
        $('#birthday_picker').birthdaypicker({
          dateFormat: "bigEndian",
          monthFormat: "long",
          placeholder: false,
          hiddenDate: false,
          defaultDate: bir
        });
        
        // initialize the zipcode plugin
        var zipcode = 106;
        var address = '';
                
        if ($.cookie('location')){
            zipcode = $.cookie('location').zipcode;
            address = $.cookie('location').address;
        }
        
        $('#twzipcode').twzipcode();
        $("input[name=user_zipcode]", '#twzipcode').val(zipcode).blur();
        $("input[name=user_address]", '#twzipcode').val(address);

        // restore them from cookie
        $('#chinese_name').val($.cookie('chinese_name'));
        $('.gender').filter('[value=' + $.cookie('gender') + ']').prop('checked', true);
        $('#phone').val($.cookie('phone'));
        $('#id').val($.cookie('id'));
 
        /*
         * Register The Submit Click Event!
         */        
        $('#btnverify').click(function(e){
            
            if (!$('#chk_read').is(':checked')){
                alert('請先閱讀使用條款');
                return false;
            }
          
            // store the personal information
            $.cookie('chinese_name', $('#chinese_name').val());
            $.cookie('gender', $('.gender:checked').val());
            $.cookie('phone', $('#phone').val());
            $.cookie('id', $('#id').val());
            
            // store the birthday plugin information
            var bir=$('.birth-year').val()+'-'+$('.birth-month').val()+'-'+$('.birth-day').val();
            $.cookie('birthday', bir);
            $('#birthday').val(bir);
            
            // store the location plugin information
            var location = {};
            location.county = $("select[name='user_county']").val();
            location.district = $("select[name='user_district']").val();
            location.zipcode = $("input[name='user_zipcode']").val();            
            location.address = $("input[name='user_address']").val();
            $.cookie('location', location);

            $('#verify').submit();
        });

    });
</script>
