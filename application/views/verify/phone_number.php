<img class='banner-s' src="/assets/images/banner-s/sellorder.jpg">
<div class='row' id='pagerow'>
	<div style=" width:680px;margin:0 auto; overflow:auto;">	
		<form id="verify_code" action="/verify/check_sms_code" method="POST" role="form">
			<h4><img id='key_icon' src="/assets/images/secondhand_management/bottom_key.png" alt="" />權限開通</h4>
			<hr class="bold_hr" />
			<?php echo alert_message($message, 'info') ?>
			<div id='input_code'>
			請輸入您手機收到的驗證碼:
			<input type="text" name="verification_code" id="verification_code"/>
			<button data-form="verify_code" type="submit" class="btn btn-info btn-xs" id="btnverify_code" >確認</button>
			<span id="error"></span><!-- 認證失敗!您輸入的認證碼不正確,請重新輸入! -->
			</div>
			<div id='resend_code'>
			如果您沒有收到或遺失認證碼簡訊：
			<a href='/verify/resend_sms_code' class="btn btn-success btn-xs" >重新認證</a>
			</div>
		</form>
	</div>
</div>
