<article>
				
							<table>
							<tr>
								<td rowspan="2"><img src='/assets/logo.png' /></td>
								<td rowspan="2"><span class='order_header'>訂單明細</span></td>
							</tr>
        </table>

        <div class='row' id='pagerow'>
					<center>
						<div>
							<table align=center class="table">
<!--							<caption>訂單明細</caption>-->
							<tr>
								<td align=center>訂購日期</td>
								<td><?php echo $order['created_at'] ?></td>
								<td align=center rowspan="2">付款方式</td>
								<td rowspan="2"><?php echo payment_information($order['payment_method'], $order['status']) ?></td>
							</tr>
							<tr>
								<td align=center>訂單編號</td>
								<td><?php echo $order['order_number'] ?></td>
							</tr>
							<tr>
								<td align=center rowspan="3">購買人</td>
								<td><?php echo $order['buyer_name'] ?></td>
								<td align=center rowspan="3">收貨人</td>
								<td><?php echo $order['receiver_name'] ?></td>
							</tr>
							<tr>
								<td><?php echo $order['buyer_address'] ?></td>
								<td><?php echo $order['receiver_address'] ?></td>
							</tr>
							<tr>
								<td><?php echo $order['buyer_phone_number'] ?></td>
								<td><?php echo $order['receiver_phone_number'] ?></td>
							</tr>
							<tr>
								<td align=center>商品編號</td>
								<td align=center colspan="2">商品敘述</td>
								<td align=center>售價</td>
							</tr>
              <?php foreach($clothing_array as $clothing): ?>
              <tr>
                <td><?php echo $clothing['product_item_number'] ?></td>
                <td colspan="2"><?php echo $clothing['product_title'] ?></td>
                <td align=right><?php echo $clothing['product_sell_price'] ?></td>
              </tr>              
              <?php endforeach; ?>
							<tr align=right>
								<td colspan="4">
                    共<?php echo $order['items_count'] ?>件
                    <br>
                    <?php echo $order['items_price'] ?>(購物金額)
                    +<?php echo $order['shipping_price'] ?>(運費)
                    <?php if (!empty($order['deduction_amount'])): ?>+ <?php echo $order['deduction_amount'] ?>(抵扣額)<?php endif; ?>
                    <?php if (!empty($order['coupon_amount'])): ?>+<?php echo $order['coupon_amount'] ?>(禮券折抵)<?php endif; ?>
                    ＝總共<?php echo $order['total_price'] ?>                
                </td>
							</tr>
							<tr>
								<td align=center>配送時間</td>
								<td><?php echo delivery_time_label($order['delivery_time']) ?></td>
								<td align=center>發票類型</td>
								<td>
                    <?php echo invoice_information($order['receipt_to'], $order['uniform_invoice_type']) ?>
                </td>
							</tr>
							</table>
						</div>
					</center>
          <p><注意事項></p>
          <p>
              請注意，鑑賞期非試用期，收到衣服後若擔心不適合，請先“在家”正常試穿（請留意臉上的粉妝不要沾到衣物上），並請不要拆下洗衣標與GetMore吊牌，更請不要穿到室外或穿著用餐。若正常試穿後仍決定退貨，請保留衣服原包裝與所有配件，以及購物發票。
          </p>
          <footer>
              Contact us<br />
              service@getmore.cc
          </footer>
				</div>
</article>

<style>
    article{
        width: 980px;
        margin: 0 auto;
        margin-top: 20px;
    }
    .order_header{
        font-size: 36px;
        padding-left: 160px;
    }
    table{
        margin-top: 40px;
    }
    .table td{
        border: 1px solid black !important;
    }
    footer{
        color: #999;
        padding: 40px 0;
        text-align: right;
    }
</style>
