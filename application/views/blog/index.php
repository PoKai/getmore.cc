<img class='banner-s' src="/assets/images/banner-s/blog.png">
<p class="article-breadcrumb">
    <a href="/">首頁</a>
    &gt; 
    <a href="/blog/">二次時尚誌</a>
</p>
<div class="row">  
<div class="col-md-9">
<ul class="nav nav-pills blog-category">
  <li <?php if ($this->uri->segment(2) == 'page') echo "class='active'"; ?>><a href='/blog'>全部</a></li>
<?php foreach($categories as $category): ?>
  <li <?php if (($this->uri->segment(3) == $category['id'])&&($this->uri->segment(2)=='category')) echo "class='active'"; ?>><a href='/blog/category/<?php echo $category['id'] ?>'><?php echo $category['name'] ?></a></li>
<?php endforeach; ?>
</ul>
</div>                  
<div class="col-md-3 paddingtop10">
        <div class="pull-right getmore-pagination"><?php echo $pagination; ?></div>        
        <div style='clear: both;'></div>
</div>
</div><hr class='hr-margintop0'>

				<div class='row' id='pagerow'>
          <?php foreach($articles as $article): ?>
					<div class='article-box'>
						<a href='/blog/view/<?php echo $article->id ?>' target='_blank'>
						<div class='article-title'>
						  <?php echo$article->title ?>
						</div>
						<div class='article-thumbnail'>
								<img src='<?= image_url($article->feature_pic_id, null, 'blog'); ?>'>	
						</div>
						<div class='article-time'>
						<span class="article-date">
        				<?php echo (new DateTime($article->created_at))->format('Y/m/d') ?>                
    					</span>
    					<span class="author-name">
       					 | <!--<a href="#"> GetMore 二次時尚編輯</a>--><?php echo $article->author ?> 
       					</span>
       					</div>
						<div class='article-abstract'>
		<!--					同樣來自美國的設計師Jessi Arrington，熱愛服裝搭配，卻力行不購買新衣，用二手衣來呈現一週七天的穿搭...
-->              <?php echo strip_tags($article->getIntro()) ?>
						</div>
						</a>
					</div>
          <?php endforeach; ?>
                    
					<img class='banner-s' src="/assets/images/banner-s/followus.jpg">
			
					<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fgetmore.cc&amp;width=980&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true&amp;appId=207910605914306" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:980px; height:258px;" allowTransparency="true"></iframe>
					
				</div>

<style>
    .pagination{
        margin: 0;
    }
</style>
