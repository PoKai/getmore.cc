				<link rel="stylesheet" href="/assets/font-awesome-4.0.3/css/font-awesome.min.css">
				<div class='row' id='pagerow'>
          <hr>
					<p class="article-breadcrumb">
            		<a href="/">首頁</a>
               		&gt; 
                    <a href="/blog/">二次時尚誌</a>
                    &gt;
                    <a href="/blog/category/<?php echo $article->category->id ?>"><?php echo $article->category->name ?></a>
          </p>
					<div class='article-content'>
						<h3><?php echo $article->title ?></h3>
						<div class='article-author'>
						<span class="article-date">
        				<?php echo (new DateTime($article->created_at))->format('Y/m/d') ?>                
    					</span>
    					<span class="author-name">
       					 | <!--<a href="#"> GetMore 二次時尚編輯</a>--><?php echo $article->author ?>
     					 </span>
     					</div>
            <?php if ($article->feature_pic_id != 0): ?>
                <img class='img-thumbnail'  src="<?= image_url($article->feature_pic_id, null, 'blog'); ?>"><br />
            <?php endif; ?>
            <?php echo $article->content ?>
						<br><br>
						<div class="fb-comments" data-href="<?php echo site_url('/blog/view/' . $article->id) ?>" data-width="620" data-numposts="5" data-colorscheme="light"></div>
					</div>
					<div class='article-side'>
						<div class='article-side-title'>ABOUT</div>
            <!--
						<p>多多，來自外星的厲害小狗。</p>
						<p><a href="#">瞧瞧他的衣櫃</a><br>
						<a href="#">看看他的慾望清單</a></p>
						-->
            <?php echo $article->about ?>
            <hr>
						<div class='article-side-title'>分享給朋友</div>
              <section class='article-social-funcs'>
                  <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo site_url('/blog/view/' . $article->id) ?>" target='_blank' class="new-social-share-button facebook">
                      <i class="fa fa-facebook"></i>
                  </a>
                  <a href="https://twitter.com/intent/tweet?url=<?php echo site_url('/blog/view/' . $article->id) ?>&amp;text=<?php echo $article->title ?>" target='_blank' class="new-social-share-button twitter">
                      <i class="fa fa-twitter"></i>
                  </a>
                  <a target='_blank' href="http://pinterest.com/pin/create/button/?media=<?php echo $article->feature_pic_path ?>&amp;url=<?php echo site_url('/blog/view/' . $article->id) ?>&amp;description=<?php echo $article->title ?>" class="new-social-share-button pinterest">
                      <i class="fa fa-pinterest"></i>
                  </a>
                  <div class='fbiframe floatright'>
                  <iframe src="//www.facebook.com/plugins/like.php?href=<?php echo site_url('/blog/view/' . $article->id) ?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21&amp;appId=239678122794926" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:70px; height:21px;" allowTransparency="true"></iframe>
                  </div>
              </section>	
              			<hr>
                    <!--
              			<div class='article-side-title'>
			              <a class="to_like" href="#" data-id="1136">收藏這篇文章</a>
			            </div>
						<hr>-->
						<div class='article-side-title'>其他文章</div>
						<ul>
              <?php foreach($recent_articles as $article): ?>
							<li><a href='/blog/view/<?php echo $article->id ?>'><?php echo $article->title ?></a></li>              
              <?php endforeach; ?>
						</ul>
						<hr>
						<!-- <div class="fb-like-box" data-href="https://www.facebook.com/getmore.cc" data-width="300" data-height="450" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="true"></div> -->
					</div>
				</div>
<script>        
        $(document).ready(function(){
            $('#target').lunZhuan({
                itemClass: 'suggested-item',
                buttonClass: 'suggested-item-button'
            });
            
            $('.new-social-share-button').click(function(e){
                e.preventDefault();
                window.open($(this).attr('href'), 'just_new', config='height=600,width=600');
            });
            
        });
 </script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&appId=239678122794926&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<style>
        .go-left{
            position: absolute;
            top: 200px;
            margin-left: -70px;
        }
        .go-right{
            position: absolute;
            top: 200px;
            right: -70px;
        }
                      
        .new-social-share-button {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-transition: all .15s ease-in-out;
            -moz-transition: all .15s ease-in-out;
            transition: all .15s ease-in-out;
            display: inline-block;
            text-align: center;
            padding-top: 2.5px;
            width: 24px;
            height: 24px;
            font-size: 15px;
            border-radius: 99px;
            background: #f5f5f5;
            color: #333
        }

        .new-social-share-button.facebook:hover {
            background: #45629d
        }

        .new-social-share-button.twitter:hover {
            background: #55acee
        }

        .new-social-share-button.tumblr:hover {
            background: #529ecc
        }

        .new-social-share-button.pinterest:hover {
            background: #cb2027
        }

        .new-social-share-button:hover{
            color: white;
        }
        
        .item_status_comment{
            font-size: 12px;
            color: #bbbbbb;
        }
        
    </style>
