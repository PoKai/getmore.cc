
<h3>出貨單號:<?php echo $item['number']?></h3>
<h3>託運單號:<?php echo $item['delivery_number']?></h3>
<h3>收件人:<?php echo $item['delivery_name']?></h3>
<h3>地址:<?php echo $item['delivery_address']?></h3>

<table class="table table-striped table-bordered table-condensed">
    <thead>
        <tr>
            <th>編號</th>
            <th>貨號</th>
            <th>品名</th>
            <th>數量</th>
            <th>檢查</th>
        </tr>
    </thead>
    <tbody>
    <?php
        $i = 1;
        foreach($item['products'] as $row):
    ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row['product_item_number']; ?></td>
            <td><?php echo $row['product_title']; ?></td>
            <td><?php echo $row['product_quantity']; ?></td>
            <td>&nbsp;</td>
        </tr>
    <?php
        $i++;
        endforeach;
    ?>
    </tbody>
</table>
