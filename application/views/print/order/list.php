<h2>批次託運單 列印日期: <?php echo date('Y-m-d'); ?></h2>

<table class="table table-striped table-bordered table-condensed">
    <thead>
        <tr>
            <th>編號</th>
            <th>託運編號</th>
            <th>姓名</th>
            <th>電話</th>
            <th>手機</th>
            <th>地址</th>
        </tr>
    </thead>
    <tbody>
    <?php
        $i = 1;
        foreach($item as $row):
    ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row['delivery_number']; ?></td>
            <td><?php echo $row['delivery_name']; ?></td>
            <td><?php echo $row['delivery_telephone']; ?></td>
            <td><?php echo $row['delivery_mobile_phone']; ?></td>
            <td><?php echo $row['delivery_address']; ?></td>
        </tr>
    <?php
        $i++;
        endforeach;
    ?>
    </tbody>
</table>
