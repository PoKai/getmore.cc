<h2>批次揀貨單 列印日期: <?php echo date('Y-m-d'); ?></h2>

<table class="table table-striped table-bordered table-condensed">
    <thead>
        <tr>
            <th>項次</th>
            <th>商品編號</th>
            <th>商品名稱</th>
            <th>欄位編號</th>
            <th>數量</th>
            <th>檢查</th>
            <th>備註</th>
        </tr>
    </thead>
    <tbody>
    <?php
        $i = 1;
        foreach($item as $row):
    ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row['product_item_number']; ?></td>
            <td><?php echo $row['product_title']; ?></td>
            <td>&nbsp;</td>
            <td><?php echo $row['product_quantity']; ?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    <?php
        $i++;
        endforeach;
    ?>
    </tbody>
</table>
