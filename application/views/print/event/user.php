
<h3>活動標題:<?php echo $item['title']?></h3>
<h3>活動時間:<?php echo $item['event_time']?></h3>
<h3>PM:<?php echo $item['username']?></h3>
<h3>講師:<?php echo $item['teacher']?></h3>

<table class="table table-striped table-bordered table-condensed">
    <thead>
        <tr>
            <th>編號</th>
            <th>姓名</th>
            <th>手機</th>
            <th>購買人</th>
            <th>Email</th>
            <th>電話</th>
            <th>手機</th>
        </tr>
    </thead>
    <tbody>
    <?php
        $i = 1; 
        foreach($item['items'] as $row): 
    ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row['user_name']; ?></td>
            <td><?php echo $row['user_phone']; ?></td>
            <td><?php echo $row['order_user_name']; ?></td>
            <td><?php echo $row['email']; ?></td>
            <td><?php echo $row['user_telephone']; ?></td>
            <td><?php echo $row['user_mobile_phone']; ?></td>
        </tr>    
    <?php
        $i++; 
        endforeach; 
    ?>
    </tbody>
</table>
