				<img class='banner-s' src="/assets/images/banner-s/about/intro.jpg">
				<div class='row' id='pagerow'>

					<div class='col-800'>
						<div class='rightcol fontp15'>
						<strong><h2>GetMore Team</h2></br>
						<p>GetMore團隊源自於台灣。</p>
						<p>總部座落在大安森林公園附近，我們把這裡取名為”getmore lab”。我們喜歡在工作的時候，有好咖啡、甜點的陪伴，我們愛這樣的美好事物，它們使我們激發出更多好的點子。 "GetMore二次時尚"就是這樣開始的，它是一個創新的想法，卻也貼近我們的生活，為我們生活中遇到的問題提供解法和價值。</p>
						<br><img class='banner-s' src="/assets/images/content/about/team.png">
						</strong>
						</div>
						<h3>GetMore 的工作室，你的咖啡館</h3>
						<p>厭倦了OA隔間，GetMore 的團隊愛好咖啡也愛甜點，就決定把工作的空間設計成一個咖啡店的樣子。
						取名為 getmore lab，那裡有很好的單品咖啡以及手工甜點，
						在 “Coffee Hours” 的時間裡，開放找個位子坐，點杯咖啡，吃個甜點。</p>
						<p>歡迎來玩！(站立免費，入座請點一杯飲料)</p>
						<p>Coffee Hours：</br>
						10:00 AM~ 7:00 PM</br>
						Tue.公休</p>
						<a href="http://goo.gl/maps/sUxzb" target="_blank"><img class='banner-s' src="/assets/images/content/about/map.jpg"></a>
					</div>
				</div>
