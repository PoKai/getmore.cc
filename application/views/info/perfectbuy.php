				<img class='banner-s' src="/assets/images/info/buy/perfect-08.png">
				<div class='row' id='pagerow'>
							<div class='rightcol'>
								<center>
									<h3>完美的購物體驗 / Perfect Experience at GetMore</h3><br>
									<p>我們在意您的每一筆購買，</p>
									<p>希望每個人都可以在GetMore找到適合自己的好衣服。</p>
									<p>輕鬆享受二次時尚的生活！</p>
								</center>
								<hr><br><br>
								<h4>用心處理</h4><br>
								<p>我們用心處理每件在GetMore的好衣，包括嚴格篩選機制、每件衣服都經過洗衣公司清洗整燙、全區為輕度使用好衣。</p><br>
								<div class='text-center'><img src="/assets/images/info/buy/perfect-01.png" alt=""></div><br>
								<h4>細心丈量</h4><br>
								<p>GetMore針對不同類型的好衣,提供非常詳盡的尺寸丈量與商品資訊,希望買家可以安心地選購適合自已的商品。</p><br>
								<div class='text-center'><img src="/assets/images/info/buy/perfect-06.png" alt=""></div><br>
								<h4>貼心包裝</h4><br>
								<p>我們用最高規格對待您的購買，包括防塵紙、訂單明細、品牌標籤（未拆標商品）、貼紙，以及一個專屬的紙提袋。</p><br>
								<div class='text-center'><img src="/assets/images/info/buy/perfect-02.png" alt=""></div><br>
								<h4>完美價格</h4><br>
								<p>只要用平均2.5折的價格，就能買到現下知名品牌、設計師款的時尚好衣。令你心動的品牌好衣，你可以用完美的價格買到它。</p><br>
								<div class='text-center'><img src="/assets/images/info/buy/perfect-03.png" alt=""></div><br>
								<h4>獨一無二，只有一件</h4><br>
								<p>在GetMore，每件衣服都是獨一無二，古著、個性、特色衣著，更能裝扮個性、彰顯風格，創造自己的二次時尚！</p><br>
								<div class='text-center'><img src="/assets/images/info/buy/perfect-04.png" alt=""></div>
							</div>
								<img class='banner-s' src="/assets/images/info/buy/perfect-07.png">
								<center><a href="/clothing/category/new/#type-new&page-1"><button type="button" class="btn btn-primary">逛逛新上架</button></a>&nbsp;<a href="/clothing/category/women/#type-women&page-1"><button type="button" class="btn btn-primary">WOMEN</button></a>&nbsp;<a href="/clothing/category/men/#type-men&page-1"><button type="button" class="btn btn-primary">MEN</button></a>&nbsp;<a href="/clothing/category/kids/#type-kids&page-1"><button type="button" class="btn btn-primary">KIDS</button></a></center>
				</div>
				<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fgetmore.cc&amp;width=980&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true&amp;appId=207910605914306" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:980px; height:258px;" allowTransparency="true"></iframe>
