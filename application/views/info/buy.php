<script src="/assets/javascript/bootstrap.js"></script>

				<img class='banner-s' src="/assets/images/banner-s/buydetail.jpg">
				<div class='row' id='pagerow'>
					<div class='col-254'>
							<div class='leftcol'>
								<p>我要買衣服 / Buy</p>
								<p>
		  							<a href="/info/perfectbuy"><button type="button" class="btn btn-primary btn-m">完美的購物體驗</button></a>
								</p>
								<p>
		  							<a href="/info/product"><button type="button" class="btn btn-primary btn-m">商品頁說明</button></a>
								</p>
								<p>
		  							<a class='nav-a' href="#paynfee" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">購買說明</button></a>
								</p>
								<p>
		  							<a class='nav-a' href="#return" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">關於退貨</button></a>
								</p>
								<p>
		  							<a class='nav-a' href="#buypolicy" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">購買條款</button></a>
								</p>
							</div>
						<hr>
						<div class='contactus'><p> Contact us <br> service@getmore.cc </p> </div>
					</div>
					
					<div class='col-726'>
					  <div class="tab-content article-line">

						<div class="tab-pane active" id="paynfee">				
							<div class='rightcol fontp15'>
								<h2>付款方式</h2></br>
								<h4>信用卡線上刷卡</h4></br>
								<p>GetMore與<a href="https://www.cathaybk.com.tw/cathaybk/" target="_blank">國泰銀行</a>直接合作，採用安全的線上付款機制，每一筆線上刷卡交易在您按下確認付款後，GetMore會與國泰世華進行加密安全連線，您將在國泰世華銀行的安全機制頁面中輸入您的信用卡資訊，進行授權等作業，所有刷卡資訊將在國泰世華的系統內，請您安心！在付款成功後，將會再度回到GetMore的訂單頁面，並收到訂購成功訊息，並安排您的出貨。</p></br>
								<ul>
								<li>※ 本站直接與國泰世華銀行，受『128-bit SSL』加密保護。</li>
								<li>※ 歡迎使用全世界各國發行之VISA、MasterCard、聯銀卡。</li>
								<li>※ 目前暫時無法接受AE卡</li>
								<li>※ GetMore無法得知您的個人信用卡持卡狀況，請在刷卡前先行確認您的信用卡狀況。</li>
								<li>※ GetMore無法得知您的個人3D認證密碼，若有3D認證密碼問題，請洽發卡銀行。</li>
								</ul></br>
								<img class='banner-s' src="/assets/images/content/paynfee/payment1.png">
								<br>
								<h4>貨到付款</h4></br>
								<p>採用宅急便,客樂得貨到付款服務，將款項交由寄件的黑貓宅急便人員。</p>
								<p>當商品出貨後，送達指定地點若未被領取或拒收，未來將無法再使用貨到付款方式訂購！請您與GetMore共同珍惜寶貴的訂購資源及維護自身權益。</p>
								</br>
								<img class='banner-s' src="/assets/images/content/paynfee/payment2.png">
								</br>
								<h4>ATM轉帳</h4></br>
								<p>採用國泰世華虛擬帳號安全轉帳，您選擇ATM付款時，請在指定時間內，利用付款流程網站上顯示的ATM帳號以及金額，至實體ATM或是線上eATM付款，轉帳完成後，系統將於1小時內自動為您辦理入帳手續，您可不需再傳真或是簡訊確認。</p>
								<li>※ GetMore不會在簡訊或是Email中提及匯款帳號，退款也無法直接於ATM上操作，請您特別注意。</li>
								</br>
								<img class='banner-s' src="/assets/images/content/paynfee/payment3.png">
								</br><hr>
								<h2>運費計算</h2></br>
								<p>宅急便配送到府：單筆消費金額未滿千元，運費一律80元。</p>
								<p>單筆消費金額1000元以上，免運費。</p>
								<br><hr>
								<h2>配送方式</h2></br>
								<h4>每筆訂單皆由國內最專業的黑貓宅急便進行配送。</h4></br>
								<p>配送範圍</p>
								<p>目前配送範圍僅限台灣本島，外島及海外請洽客服，不便之處敬請見諒。</p></br>
								<p>配送時間</p>
								<p>常態的出貨時間為：今天下單，隔天出貨，後天就可以收到您訂購的衣物（若遇非工作日則順延）</p>
								<p>GetMore出貨人員會盡快完成您的出貨，若因貨量過多而延遲，請您多多見諒</p>
								<p>(您可至【會員中心】—>【訂單查詢】來查看訂單處理進度喔!)</p>
								<p>若遇不可抗力之因素，出貨時間請依據首頁之公告說明為準！</p></br></br>
							</div>
					  	</div>
						<div class="tab-pane" id="return">				
							<div class='rightcol fontp15'>
							<h2>關於退貨</h2></br>
							<p>「我們不喜歡衝動購買造成的輕易退貨」- 雖然妳可以放心的在7日鑑賞期內在家試穿。</p>
							<p>很多購物網站或是大型量販告訴我們，「先買回家，不喜歡再來退就好」，讓我們養成了「衝動購買」或是「過度消費」的習慣，刺激了消費，但卻有可能傷害環境，堆積如山的無用商品反而降低了生活品質。</p>
							<p>GetMore的理念是提供一個管道，讓好衣服有再次被愛的機會，讓賣家可以賺取合理利潤；也讓買家可用完美價格買到心愛好衣。我們無法也不會拒絕客人退貨，但GetMore的每一件衣服都是獨一無二的，我們在幫她尋找再次被愛的機會，因此希望每位買家下訂前，可以理性考慮過衣服的尺寸、版型是否適合自己，甚至是能否與自己現有的衣服做搭配。當然，這不是件容易的事情，但還是希望大家在GetMore上避免衝動購買，輕易退貨的消費習慣。畢竟，不輕言退貨也是節能減碳、愛護地球的方式之一，也是GetMore一直提倡的理念。</p>
							<p>承如上述，GetMore提供一個平台，希望賣家與買家雙贏，因此我們只在衣服成交後收取25%做為代售手續費，買家輕易就能退貨，但運費必須由GetMore負擔，坦白說是有些辛苦的。</p>
							<p>感謝您的支持，GetMore會持續努力為您找好衣服，網站功能也會持續改善，提供您更好的購物環境。</p></br>
							<hr>
							<h4>只退不換</h4></br>
							<p>GetMore上的每一件衣服都是獨一無二的，若您收到的衣服尺寸不合或不如預期，很抱歉我們也無法提供相同款式的衣服供您更換。</p></br>
							<h4>退貨方式</h4></br>
							<p>GetMore提供拆單退貨的服務（若您購買5件衣服中其中僅1件不合穿，只要退回不合穿的1件，其餘心愛的衣服都可以留下喔）。請於7日鑑賞期內填妥【<a href='http://dl.dropboxusercontent.com/u/33336146/return_apply.doc'>退貨申請單</a>】，email(service@getmore.cc)通知我們，同時將要退貨的衣物連同購物發票完整包裝並妥善密封，物流士將於您提出申請後3個工作日內到府收件，退貨運費將由GetMore為您負擔（若退貨後訂單金額未達免運標準，則將加計公告運費）。
							請注意，鑑賞期非試用期，收到衣服後若擔心不適合，請先“在家”正常試穿（請留意臉上的粉妝不要沾到衣物上），並請不要拆下洗衣標與GetMore吊牌，更請不要穿到室外或穿著用餐。若正常試穿後仍決定退貨，請保留衣服原包裝與所有配件，連同購物發票與退貨申請單一併包好交給物流士，GetMore感謝您的配合！</p></br>
							<h4>退款流程</h4></br>
							<p>依據您的付款方式，GetMore的退款流程如下：</p></br>
							<img src="/assets/images/content/return/return.jpg">
							</div>
					  	</div>
					  <div class="tab-pane" id="buypolicy">
						<div class='rightcol'>
							<h2>二手衣購買條款</h2></br>
							<p>一、	在您開始使用GetMore 網站（http://getmore.cc，以下稱本網站）所提供之所有服務前，請您先閱讀本網站所公佈之所有條款，一旦開始使用，代表您已閱讀所有條文內容且接受條文規定。並且，建議您常查閱服務條款，GetMore 有權隨時更改所有條文內容，若您不接受，請立即停止使用服務。</p>
							<h4>二、	買家註冊</h4>
							<p>買家於本網站進行註冊時，應使用合法取得且得有效使用之電子郵件，並自行保管註冊資訊（包含密碼），日後對於所有使用該組電子郵件帳號及密碼登入本服務系統所為之一切行為，都視為買家自己之行為，並應由買家負其責任。若買家發現電子郵件帳號或密碼遭他人冒用，請立即通知本網站，本網站將立即暫停該電子郵件帳號之交易。</p>
							<h4>三、	訂單及交易成立</h4>
							<p>除本條款外，商品銷售網頁及訂購流程中所呈現之資訊，包括相關交易條件、限制及說明等，亦為契約之一部分。買家完成線上訂購程序後，本網站會出現已完成訂購之通知，惟此僅代表本網站已收到買家的訂購訊息，不代表交易已經完成，本網站保留接受訂單與否之權利。經本網站確認交易無誤、存貨、及無其他無法接受訂單之情形後，本網站將聯絡第三方物流出貨，不另行通知，買家可以在網站上查詢出貨狀況。</p>
							<h4>四、	訂購注意事項</h4>
							<p>(一)	所有標示「原價」、「原購買價」或類似語意的價格，都只是賣家所提供之當時購買售價參考，不代表任何特定區域、商家、或特定時間的實際成交價格。</p>
							<p>(二)	所有標示「品牌」或類似語意的品牌，皆為賣家所提供，本網站不保證真偽，惟遇標示不實，買家可無條件退貨，本網站將依本網站與賣家間之契約，要求賣家負相關責任。本網站承諾協助買家解決因為線上消費所產生的疑問或爭議。</p>
							<p>(三)	所有標示「驗收狀態」、「風格」、「尺寸」、「特色」、「點評」或類似語意的資訊，皆為本網站依照主觀判斷提出之參考，買家必須於收到貨後，於七天退貨期限內再次檢驗。</p>
							<p>(四)	本網站所販售之服飾，若無特別標註，皆為賣家提供之二手服飾，經由第三方洗衣公司經過嚴格清洗。</p>
							<p>(五)	關於買家所訂購之商品，其運費之計價及負擔方式，於結帳交易時顯示。</p>
							<p>(六)	買家可以依照各該商品銷售網頁及訂購流程中相關網頁所記載之方式、條件及限制，選擇您所訂購之商品之交付地點及方式。買家所訂購的商品，由本網站委託第三方物流業者配送，依照物流業者提供之服務規定，若配送無法送達，將取消該筆訂單、並全額退款。</p>
							<p>(七)	本網站內可能包含許多連結，這些被連結網站或網頁上的所有資訊，都是由被連結網站所提供，本網站Chome Online不以任何明示或默示的方式擔保其內容的正確性、可信度或即時性。</p>
							<h4>五、	免責事由</h4>
							<p>本網站僅提供代售服務，買家於本網站所瀏覽的二手衣商品，其部分訊息（包含標示「原價」、「原購買價」或類似語意的價格），可能由第三方（賣家等）提供，且買家係向該二手衣之賣家進行訂購，並由該賣家負責履行，本網站僅提供行銷服務、連結、代售、或電子商務平台服務。本網站要求所有賣家委託代售之二手衣商品須非侵權商品，否則該賣家應負完全責任，本網站不負擔保責任。</p>
							<h4>六、	銷售發票開立</h4>
							<p>二手衣銷售依國稅局規定，由咩斯特數位整合有限公司統一開立發票。</p>
							<h4>七、	退貨</h4>
							<p>(一)	買家可以依消費者保護法第十九條第一項之規定，於收受商品後七日內退回商品或行使相關權利。</p>
							<p>(二)	買家已清楚瞭解本網站所販售之服飾，若無特別標註，皆為賣家提供之二手服飾，經由第三方洗衣公司經過嚴格清洗。若超過退貨期限，不得以此為由要求退貨。</p>
							<p>(三)	關於退貨退款方式及條件，依照問與答以及相關網頁記載。</p>
							<p>(四)	買家所退回的商品，必須保持所有商品、贈品、附件、包裝、洗標、及所有附隨文件或資料在出貨當時狀態的完整性，如果有實體發票，並應連同發票一併退回及簽署折讓單等相關法令所要求之單據；否則，本網站得拒絕接受買家的退貨退款要求。</p>
							<p>(五)	買家瞭解並同意，若因買家要求退貨、或因本網站無法接受買家全部或部分之訂單、或契約因故解除或失其效力，而需為買家辦理退款事宜時，本網站得代買家處理發票或折讓單等相關法令所要求之單據。</p>
							<h4>八、	賠償責任</h4>
							<p>買家瞭解並同意，如果本網站因故依法或依約必須對買家負擔賠償或補償責任，本網站之賠償或補償責任，應以退還有爭議之該筆交易之實收金額為賠償或補償責任之上限。</p>
							<h4>九、	條款修改</h4>
							<p>GetMore網站保留彈性調整二手衣購買服務條款之權利，調整內容即公佈於網頁，不另行通知買家。</p>
							<h4>十、	合約轉移</h4>
							<p>買家瞭解並同意，如果本網站將其相關之權利義務關係及交易等合約移轉予第三人，則本網站之權利義務關係、相關交易、以及相關資料等，包括且不限於相關之權利義務關係等，亦得一併移轉予該第三人承受、並由該第三人繼續提供服務。</p>
							<h4>十一、	準據法及管轄條款</h4>
							<p>因本條款所生之爭議，雙方同意以中華民國法律為準據法，並以臺灣臺北地方法院為第一審管轄法院。</p><br>								
							</div>	
						</div>
					  </div>
				    </div>
			    </div> 	
	
				 	 <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fgetmore.cc&amp;width=980&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true&amp;appId=207910605914306" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:980px; height:258px;" allowTransparency="true"></iframe>

<script>
    $(document).ready(function(){
        var hash = window.location.hash;
        hash && $('.nav-a[href="' + hash + '"]').tab('show');

        $('.nav-a').click(function (e) {
          $(this).tab('show');
          var scrollmem = $('body').scrollTop();
          window.location.hash = this.hash;
          $('html,body').scrollTop(scrollmem);
          
        });      
    });
</script>
