<link rel="stylesheet" href="/assets/css/newcart.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<div class='pagecontent'>
  <div class='totalpricebox'>
    <div class='well totalprice'>
      <p>本次消費總金額共 $1000 元，帳戶折抵 $100 元，紅利折抵 $100元。<br>
        <span style='font-size:10px; color:#bbb;'>(提醒您，商品來自不同賣家可能個別出貨，請留意手機來電！) </span>
      </p>
      <div class='endprice'>總計 $800元</div>
    </div>
  </div>
  <div class='totalpricebox'>
    <div class='well totalprice'>
      <p><i class="fa fa-gratipay fa-lg red"></i>&nbsp您的帳戶共有<span class="account_balance">1009</span>元，請輸入抵扣金額<input type="text" name="deduction_amount" class="form-control deduction_amount" value="0" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57">元。&nbsp<button class="btn btn-xs btn-info">確定</button><!--<a href='#' class='label label-primary'>了解更多</a>--></p>
      <p><i class="fa fa-gratipay fa-lg red"></i>&nbsp您的紅利G幣共有<span class="account_balance">1009</span>點&nbsp<button class="btn btn-xs btn-info">按我折抵</button><!--<a href='#' class='label label-primary'>了解更多</a>--></p>
      <p><i class="fa fa-gratipay fa-lg red"></i>&nbsp有折扣碼？<input type="text" name="coupon_code" class="form-control coupon_code" placeholder="請輸入折扣碼"><button class="btn btn-info btn-xs coupon-btn">確定</button>可折抵<span class="coupon_amount"> 0 </span>元。</p>
    </div>
  </div>
    <hr>
  
	<div class="carttitle">
		賣家寄倉商品 / BRANDS ONLY
	</div>  
	<div class="cartbplan">
  	<table class="table table-condensed billtable">
  	 <thead>	
      <tr>
  	 		<th></th><th>商品編號</th><th>商品敘述</th><th>價格</th><th>移除</th>
  	 	</tr>
    </thead>
     <tbody>
  	 	<tr>
  	 		<th><img class="cartimg" src="http://getmore.cc/upload/thumbs/800_1200_978d1b82ec9f2856d70c476c8e774434.JPG"></th><th>S1401090015004<br><a href="#" class="buy-next-time" data-id="50">下次再買，先加入購物清單</a></th><th>男 藍色短袖商務襯衫</th><th>250</th><th><a href="#"><i class="fa fa-times fa-lg"></i></a></th>
  	 	</tr>
      <tr bgcolor="#f9f2f4">
        <th><img class="cartimg" src="http://getmore.cc/upload/thumbs/800_1200_978d1b82ec9f2856d70c476c8e774434.JPG"></th><th>S1401090015004<br><div class="delete-alert">被搶先購買了！請先<a href="#" style="color:#c7254e;text-decoration: underline;">移除</a>再進行下一步</div></th><th>男 藍色短袖商務襯衫</th><th>250</th><th><a href="#"><i class="fa fa-times fa-lg"></i></a></th>
      </tr>
     </tbody>
  	</table>
    <div class="subtotal">
      <div class="panel panel-default everyreturn">
          <div class="panel-heading">
            <h3 class="panel-title">退貨條款</h3>
          </div>
          <div class="panel-body" style="font-size:10px; margin-bottom:10px;">
            GetMore上的每一件衣服都是獨一無二的，若您收到的衣服尺寸不合或不如預期，很抱歉我們也無法提供相同款式的衣服供您更換。
          </div>
      </div>
      <div class="pull-right">
        <table style="width:300px;">
          <tbody>
            <tr>
              <td class="cart_1_money">
                  <p>
                    商品件數<span class="cart_1_count">1</span>件，
                    商品金額<span class="cart_1_price">250</span>元
                  </p>
                  <p>
                    運費金額 <span class="cart_1_tran">80</span>元
                  </p>
              </td>
            </tr>
            <tr>
              <td class="cart_1_money">
                  <p class="cart_1_sum_block">
                    消費小計NT$<span class="cart_1_sum">330</span>元
                  </p>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
	</div>
  <hr>
    <div class="carttitle">
    賣家自出商品 / MARKETPLACE
  </div>  
  <div class="cartbplan">
    <div><p>賣家：<a href="#" target="_blank">PoKai</a></p></div>
  <table class="table table-condensed billtable">
   <thead>  
    <tr>
      <th></th><th>商品編號</th><th>商品敘述</th><th>價格</th><th>移除</th>
    </tr>
  </thead>
   <tbody>
    <tr>
      <th><img class="cartimg" src="http://getmore.cc/upload/thumbs/800_1200_978d1b82ec9f2856d70c476c8e774434.JPG"></th><th>S1401090015004<br><a href="#" class="buy-next-time" data-id="50">下次再買，先加入購物清單</a></th><th>男 藍色短袖商務襯衫</th><th>250</th><th><a href="#"><i class="fa fa-times fa-lg"></i></a></th>
    </tr>
    <tr>
      <th><img class="cartimg" src="http://getmore.cc/upload/thumbs/800_1200_978d1b82ec9f2856d70c476c8e774434.JPG"></th><th>S1401090015004<br><a href="#" class="buy-next-time" data-id="50">下次再買，先加入購物清單</a></th><th>男 藍色短袖商務襯衫</th><th>250</th><th><a href="#"><i class="fa fa-times fa-lg"></i></a></th>
    </tr>
   </tbody>
  </table>
  <div class="subtotal">
    <div class="panel panel-default everyreturn">
        <div class="panel-heading">
          <h3 class="panel-title">退貨條款</h3>
        </div>
        <div class="panel-body" style="font-size:10px; margin-bottom:10px;">
          GetMore上的每一件衣服都是獨一無二的，若您收到的衣服尺寸不合或不如預期，很抱歉我們也無法提供相同款式的衣服供您更換。
        </div>
    </div>
    <div class="pull-right">
      <table style="width:300px;">
        <tbody>
          <tr>
            <td class="cart_1_money">
                <p>
                  商品件數<span class="cart_1_count">1</span>件，
                  商品金額<span class="cart_1_price">250</span>元
                </p>
            </td>
          </tr>
          <tr>
            <td class="cart_1_money">
                <p class="cart_1_sum_block">
                  消費小計NT$<span class="cart_1_sum">330</span>元
                </p>
            </td>
          </tr>
          </tbody>
        </table>
      </div>
  </div>
  </div>
  <hr>
    <div class='totalpricebtn'>
      <div class='text-center'>
          <a href="#" style="vertical-align:bottom;font-size:10px;color:#bbb;margin-right:5px;">繼續購物</a>
          <a href='/info/newcart2'><button class="btn btn-primary btn-lg" id="go_second_step">下一步，填寫資訊</button></a>
      </div>
    </div>
</div>