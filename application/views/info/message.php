<?= post_message_modal();?>

<!-- 個人訊息檢視 -->
<img class='banner-s' src="/assets/images/banner-s/about/intro.jpg">
<div class='row' id='pagerow'>
    <div class='col-254'>
        <div class='leftcol'>
            <p>我的信箱 / message</p>
                <p>
                    <a class='nav-a' href="#GetMore" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">訊息夾</button></a>
                </p>
                <p>
                    <a href="/info/joinus"><button type="button" class="btn btn-primary btn-m">已寄出</button></a>
                </p>
                <p>
                    <a data-toggle="modal" href="#postmsg"><button type="button" class="btn btn-primary btn-m">留言</button></a>
                </p>
        </div>
        <hr>
        <div class='contactus'><p> Contact us <br> service@getmore.cc </p> </div>
    </div>

    <div class='col-726'>
        <div class="tab-content article-line">
            <div class="tab-pane active rightcol" id="GetMore">
                   <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                            <?= post_message();?>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <?= message();?>
                            <?= message();?>
                            <?= message();?>
                            <?= post_message_form();?>
                          </div>
                        </div>
                      </div>
                   </div>
           </div>
       </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        var hash = window.location.hash;
        hash && $('.nav-a[href="' + hash + '"]').tab('show');

        $('.nav-a').click(function (e) {
          $(this).tab('show');
          var scrollmem = $('body').scrollTop();
          window.location.hash = this.hash;
          $('html,body').scrollTop(scrollmem);

        });
    });
</script>
