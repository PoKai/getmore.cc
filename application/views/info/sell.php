<script src="/assets/javascript/bootstrap.js"></script>

				<img class='banner-s' src="/assets/images/banner-s/sellintro.jpg">
				<div class='row' id='pagerow'>
					<div class='col-254'>
							<div class='leftcol'>
								<p>我要賣衣服 / Sell</p>
								<p>
		  							<a class='nav-a' href="#howtosell" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">如何賣衣服</button></a>
								</p>
								<p>
		  							<a href="/info/sell_type"><button type="button" class="btn btn-primary btn-m">GetMore搶手貨</button></a>
								</p>
								<p>
		  							<a href="/info/startsell"><button type="button" class="btn btn-primary btn-m">開始賣衣服</button></a>
								</p>
								<p>
		  							<a class='nav-a' href="#wash" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">保證嚴格清洗</button></a>
								</p>
								<p>
		  							<a class='nav-a' href="#sellpolicy" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">條款說明</button></a>
								</p>
							</div>
						<hr>
						<div class='contactus'><p> Contact us <br> service@getmore.cc </p> </div>
					</div>
					
					<div class='col-726'>
					  <div class="tab-content article-line">	
						<div class="tab-pane active" id="howtosell">				
							<div class='rightcol fontp15'>
								<div class='text-center'><img src="/assets/images/content/sell/PR-01.png" alt=""></div>
							</div>
							<div class='text-center'>
								<div class="sellstart"><p class="text-center"><a href="/info/brandsonly"><button type="button" class="btn btn-primary">→品牌限定準備中</button></a></p></div>
							</div>
					  	</div>		
					   <div class="tab-pane" id="wash">				
						 <div class='rightcol fontp15'>
							<h2>保證嚴格清洗</h2></br>
							<h4>專業洗衣流程</h4></br>
							<img src="/assets/images/content/wash/washprocess1.jpg">
							<img src="/assets/images/content/wash/washprocess2.jpg">
							<img src="/assets/images/content/wash/washprocess3.jpg">
							<img src="/assets/images/content/wash/washprocess4.jpg"></br>
							<p>所有洗衣程序皆由高品質的洗衣公司進行嚴格處理， 洗劑採用具SGS 認證標準的環保洗劑， 衣物洗滌後不殘留任何危害
							健康的化學物質； 大型隧道蒸汽機殺菌， 即使小Baby 的衣服爸媽都安心； 以及專業整燙流程， 使每一件上架的衣服亮
							麗如新。</p></br>
						</div>
					   </div>

					  <div class="tab-pane" id="sellpolicy">
						<div class='rightcol'>									
							<h2>二手衣代洗代售服務條款</h2></br>
							<p>一、	在您開始使用GetMore 網站（http://getmore.cc，以下稱本網站）所提供之所有服務前，請您先閱讀本網站所公佈之所有條款，一旦開始使用，代表您已閱讀所有條文內容且接受條文規定。並且，建議您常查閱服務條款，GetMore 有權隨時更改所有條文內容，若您不接受，請立即停止使用服務。</p>
							<p>二、	本網站提供之二手衣代洗代售服務，包含以下所稱之「代洗服務」及「代售服務」</p>
							<h4>三、	代洗服務：</h4>
							<p>(一)	本網站所提供之二手衣代洗代售服務，為保證所有販售二手衣皆為清洗過，所有代售二手衣皆需經過代洗服務過程，代洗過程未詳載之事項悉依經濟部公布之「洗衣定型化契約」辦理。</p>
							<p>(二)	代洗費用</p>
							<p>代洗費用依照類別分級收費，不論品牌，賣家於提出需求時預先支付。若待洗衣物檢查到有瑕疵，或類別或數量不符情形，客服人員將以電話、手機簡訊或是Email擇一告知賣家，並重新計算代洗費用，若與預先支付之代洗費用有所出入，差額則轉入會員帳戶中紀錄，若差額為正，賣家可提出申請領回，若差額為負，本網站有權請求再付款。若賣家不同意本網站重新計算之費用，賣家可以取消代洗服務，惟上述過程中若有手續費支出，須由賣家負擔。</p>
							<p>(三)	賠償責任</p>
							<p>若遇衣物不能返還或衣物毀損時之賠償數額，依照悉依經濟部公布之「洗衣定型化契約」辦理。</p>
							<h4>四、	代售服務</h4>
							<p>(一)	參考原價及商品售價之提出</p>
							<p>賣家在提出二手衣代洗代售申請時，必須提出原購買時之參考原價區間，並且依二手衣狀態、款式、稀有程度等因素，由賣家自行訂定「售價」。</p>
							<p>(二)	代售手續費</p>
							<p>代售手續費係指賣家支付本網站提供代售服務，包含拍照、上架、理貨、包裝、出貨等程序之服務費用。依以下方式收取：</p>
							<p>1.	若二手衣商品售出：於售出後，由本網站直接由售出所得中扣取。</p>
							<p>2.	若於代售期間屆滿時，二手衣商品未售出：本網站不收取任何代售手續費。</p>
							<p>3.	若於代售期間內賣家主動取消代售服務：賣家應支付本網站運費等一定金額之手續費。</p>
							<p>(三)	代售期間</p>
							<p>二手衣代售期間為六個月，自賣家提出代洗代售服務之申請日開始計算，代售期滿若未受出，依照代洗代售申請時賣家提供之寄回地址送回，若賣家於申請時選擇捐出，則所有權歸於咩斯特數位整合有限公司，咩斯特數位整合有限公司有權選定需要的機構進行無償捐出。若賣家於代洗代售期間內主動取消服務，則必須支付運費手續費等相關費用，若本網站於代洗代售期間內主動取消代售服務，賣家無需支付運費。</p>
							<h4>五、	正確提供資訊及不得侵權義務</h4>
							<p>賣家有提供本網站正確衣物資訊之責任，包含商品敘述、品牌、參考原價、欲售價格等，並且瞭解到所有資訊必須負擔相關法律責任，若後續出售發現問題，買家有權追責，若因此造成本網站損害，賣家應負賠償責任。又本網站嚴禁代售任何侵權商品，賣家在提出代售申請時，必須清楚來源，並且負完全責任。</p>
							<h4>六、	網站行銷</h4>
							<p>賣家同意本網站得為行銷本網站之目的，使用本網站為代洗代售商品所拍攝之照片、文字資料等，賣家亦同意本網站得揭露使用賣家部分資訊（如使用之暱稱），以供本網站行銷使用。</p>
							<h4>七、	銷售發票開立</h4>
							<p>二手衣銷售依國稅局規定，由咩斯特數位整合有限公司統一開立發票。並依照賣家二手衣銷售所得，寄發「個人一時貿易資料申報表」。</p>
							<h4>八、	合約轉移</h4>
							<p>賣家瞭解並同意，如果本網站將其相關之權利義務關係及交易等合約移轉予第三人，則本網站之權利義務關係、相關交易、以及相關資料等，包括且不限於相關之權利義務關係等，亦得一併移轉予該第三人承受、並由該第三人繼續提供服務。</p>
							<h4>九、	條款修改</h4>
							<p>GetMore網站保留彈性調整二手衣代售服務條款之權利，調整內容即公佈於網頁，不另行通知賣家。</p>
							<h4>十、	準據法及管轄</h4>
							<p>因本條款所生之爭議，雙方同意以中華民國法律為準據法，並以臺灣臺北地方法院為第一審管轄法院。</p>
						</div>	
						</div>
					  </div>
				    </div>
			    </div> 	
	
				 	 <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fgetmore.cc&amp;width=980&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true&amp;appId=207910605914306" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:980px; height:258px;" allowTransparency="true"></iframe>

<script>
    $(document).ready(function(){
        var hash = window.location.hash;
        hash && $('.nav-a[href="' + hash + '"]').tab('show');

        $('.nav-a').click(function (e) {
          $(this).tab('show');
          var scrollmem = $('body').scrollTop();
          window.location.hash = this.hash;
          $('html,body').scrollTop(scrollmem);
          
        });      
    });
</script>
