<link rel="stylesheet" href="/assets/css/newcart.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<div class='pagecontent'>
  <div class='totalpricebox'>
    <div class='well totalprice'>
      <p>本次消費總金額共 $1000 元，帳戶折抵 $100 元，紅利折抵 $100元。<br>
        <span style='font-size:10px; color:#bbb;'>(提醒您，商品來自不同賣家可能個別出貨，請留意手機來電！) </span>
      </p>
      <div class='endprice'>總計 $800元</div>
    </div>
  </div>
  <hr>
  <div class="panel panel-success check-info check-panel">
    <div class="panel-body-title check-info">
      <div class="check-info-title text-center">
          <div>填入寄送資料</div>
      </div>
    </div>
    <div class="panel-body check-info">
        <div class="check-info-left">
          <form>
            <div class="checksubtitle text-center">
              訂購人資料
            </div>
            <div class="form-group">
              <label for="putname">中文姓名<sup>*必填</sup></label><br>
              <div class="nameandgender">
                <div class="namebox">
                <input type="text" name="user_name" class="form-control" id="buyer_name" placeholder="">
                </div>
                <div class="genderbox">
                  <input type="radio" name="receiver_gender" value="0" checked="checked">小姐
                  <input type="radio" name="receiver_gender" value="1">先生
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="putphone">聯絡電話<sup>*請填入聯絡到您的電話</sup></label>
              <div class="phonebox">
                <input type="text" name="user_telephone" class="form-control" id="buyer_contact_phone_number" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <label for="putname">地址<sup>*必填</sup></label><br>
              <div class="nameandgender">
                <div id="twzipcode2">
                  <div class="address-city">
                    <select name="receiver_county" class="form-control"><option value="">縣市</option><option value="基隆市">基隆市</option><option value="台北市">台北市</option><option value="新北市">新北市</option><option value="宜蘭縣">宜蘭縣</option><option value="新竹市">新竹市</option><option value="新竹縣">新竹縣</option><option value="桃園縣">桃園縣</option><option value="苗栗縣">苗栗縣</option><option value="台中市">台中市</option><option value="彰化縣">彰化縣</option><option value="南投縣">南投縣</option><option value="嘉義市">嘉義市</option><option value="嘉義縣">嘉義縣</option><option value="雲林縣">雲林縣</option><option value="台南市">台南市</option><option value="高雄市">高雄市</option><option value="屏東縣">屏東縣</option><option value="台東縣">台東縣</option><option value="花蓮縣">花蓮縣</option></select>
                  </div>
                  <div class="address-city">
                    <select name="receiver_district" class="form-control"><option value="中正區">中正區</option><option value="大同區">大同區</option><option value="中山區">中山區</option><option value="松山區">松山區</option><option value="大安區">大安區</option><option value="萬華區">萬華區</option><option value="信義區">信義區</option><option value="士林區">士林區</option><option value="北投區">北投區</option><option value="內湖區">內湖區</option><option value="南港區">南港區</option><option value="文山區">文山區</option></select>
                  </div>
                  <div class="address-zip">
                    <input type="text" class="form-control" id="buyer_contact_phone_number"  name="receiver_zipcode" readonly>
                  </div>
                </div>
                <div class="addressdetail">
                    <input type="text" name="receiver_address" class="form-control" placeholder="">
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="check-info-right">
            <div class="checkbox aspay">
              <label>
                <input type="checkbox" id="fill_receiver_from_buyer"> 同訂購人
              </label>
            </div>
          <form>
            <div class="checksubtitle text-center">
              收件人資料
            </div>
            <div class="form-group">
              <label for="putname">中文姓名<sup>*必填</sup></label><br>
              <div class="nameandgender">
                <div class="namebox">
                <input type="text" name="user_name" class="form-control" id="buyer_name" placeholder="">
                </div>
                <div class="genderbox">
                  <input type="radio" name="receiver_gender" value="0" checked="checked">小姐
                  <input type="radio" name="receiver_gender" value="1">先生
                </div>
              </div>
            </div>
            <div class="form-group has-error">
              <label for="putphone">聯絡電話<sup>*請填入聯絡到收件人的電話</sup></label>
              <div class="phonebox">
                <input type="text" name="user_telephone" class="form-control" id="buyer_contact_phone_number inputError1" placeholder="請填寫聯絡電話">
              </div>
            </div>
            <div class="form-group">
              <label for="putname">地址<sup>*必填</sup></label><br>
              <div class="nameandgender">
                <div id="twzipcode2">
                  <div class="address-city">
                    <select name="receiver_county" class="form-control"><option value="">縣市</option><option value="基隆市">基隆市</option><option value="台北市">台北市</option><option value="新北市">新北市</option><option value="宜蘭縣">宜蘭縣</option><option value="新竹市">新竹市</option><option value="新竹縣">新竹縣</option><option value="桃園縣">桃園縣</option><option value="苗栗縣">苗栗縣</option><option value="台中市">台中市</option><option value="彰化縣">彰化縣</option><option value="南投縣">南投縣</option><option value="嘉義市">嘉義市</option><option value="嘉義縣">嘉義縣</option><option value="雲林縣">雲林縣</option><option value="台南市">台南市</option><option value="高雄市">高雄市</option><option value="屏東縣">屏東縣</option><option value="台東縣">台東縣</option><option value="花蓮縣">花蓮縣</option></select>
                  </div>
                  <div class="address-city">
                    <select name="receiver_district" class="form-control"><option value="中正區">中正區</option><option value="大同區">大同區</option><option value="中山區">中山區</option><option value="松山區">松山區</option><option value="大安區">大安區</option><option value="萬華區">萬華區</option><option value="信義區">信義區</option><option value="士林區">士林區</option><option value="北投區">北投區</option><option value="內湖區">內湖區</option><option value="南港區">南港區</option><option value="文山區">文山區</option></select>
                  </div>
                  <div class="address-zip">
                    <input type="text" class="form-control" id="buyer_contact_phone_number"  name="receiver_zipcode" readonly>
                  </div>
                </div>
                <div class="addressdetail">
                    <input type="text" name="receiver_address" class="form-control" placeholder="">
                </div>
              </div>
            </div>
          </form>
        </div>
    </div>
  </div>

  <div class="panel panel-success check-info check-panel">
    <div class="panel-body-title check-info">
      <div class="check-info-title text-center">
          <div>選擇付款方式</div>
      </div>
    </div>
    <div class="panel-body-payment check-info text-center">
      <div class="cart_3_pay">
        <div class="cart_3_choose_pay">
            <input type="radio" name="payment_method" value="credit-cart" checked="checked" id="credit-cart">信用卡<br>
            <img src="/assets/images/checkout/credit_card.png">
            <input type="radio" name="payment_method" value="deduction" class="hidden" id="deduction">
        </div>
        <div class="cart_3_choose_pay">
          <input type="radio" name="payment_method" value="atm">ATM轉帳<br>
          <img src="/assets/images/checkout/atm.png">
        </div>
        <div class="cart_3_choose_pay">
          <input type="radio" name="payment_method" value="pay-at-home" disabled>貨到付款<br>
          <img src="/assets/images/checkout/bcat.png">
        </div>
      </div>
    </div>
  </div>
  <hr>
    <div class='totalpricebtn'>
      <div class='text-center'>
          <a href="/info/newcart1" class="btn btn-default btn-lg">上一步，確認訂單</a> &nbsp
          <a href='#'><button class="btn btn-primary btn-lg" id="go_second_step">下一步，確認付款</button></a>
      </div>
    </div>
</div>