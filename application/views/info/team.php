				<img class='banner-s' src="/assets/images/banner-s/GetMoreLove.jpg">
				<div class='row' id='pagerow'>
					<div class='article-box'>
						<a href='#'>
						<div class='article-title'>
						  【TED】設計師Jessi Arrington的二手時尚
						</div>
						<div class='article-thumbnail'>
								<img src='http://getmore.cc/upload/210f235a1748d0e08fb66695ed3022f1.png'>	
						</div>
						<div class='article-time'>
						<span class="article-date">
        				2014/07/18                
    					</span>
    					<span class="author-name">
       					 | <!--<a href="#"> GetMore 二次時尚編輯</a>-->Ting 
       					</span>
       					</div>
						<div class='article-abstract'>
							同樣來自美國的設計師Jessi Arrington，熱愛服裝搭配，卻力行不購買新衣，用二手衣來呈現一週七天的穿搭...
						</div>
						</a>
					</div>
					<div class='article-box'>
						<a href='#'>
						<div class='article-title'>
							<strong>
							來自波蘭的二次時尚穿搭
							</strong>
						</div>
						<div class='article-thumbnail'>
								<img src='http://getmore.cc/upload/883b73b6c3d503a2390f190047284c38.jpg'>	
						</div>
						<div class='article-time'>
						<span class="article-date">
        				2014/07/18                
    					</span>
    					<span class="author-name">
       					 | <!--<a href="#"> GetMore 二次時尚編輯</a>-->Ting 
       					</span>
       					</div>
						<div class='article-abstract'>
							帶點灰色，卻個性的生活主義，是許多無國域年輕人的共同語言。來自波蘭，Maria Joanna，是學生模特兒，同時也是一位部落客...
						</div>
						</a>
					</div>
					<div class='article-box'>
						<a href='#'>
						<div class='article-title'>
							<strong>
							穿二手衣的理由
							</strong>
						</div>
						<div class='article-thumbnail'>
								<img src='http://getmore.cc/upload/b9c44965431560c20e10ae88c74235ce.png'>	
						</div>
						<div class='article-time'>
						<span class="article-date">
        				2014/07/18                
    					</span>
    					<span class="author-name">
       					 | <!--<a href="#"> GetMore 二次時尚編輯</a>-->Ting 
       					</span>
       					</div>
						<div class='article-abstract'>
						現在的服飾快速、平價、大喊追求流行時尚的口號，所以當我們在逛街的時候，常常不自覺看看現在流行什麼，是不是也該買一件才不會落伍...
						</div>
						</a>
					</div>
					<div class='article-box'>
						<a href='#'>
						<div class='article-title'>
							<strong>
							打造二次時尚的生活
							</strong>
						</div>
						<div class='article-thumbnail'>
								<img src='http://getmore.cc/upload/a746ff50e7d3422d1882b0a91edee3ed.png'>	
						</div>
						<div class='article-time'>
						<span class="article-date">
        				2014/07/18                
    					</span>
    					<span class="author-name">
       					 | <!--<a href="#"> GetMore 二次時尚編輯</a>-->Ting 
       					</span>
       					</div>
						<div class='article-abstract'>
						簡約、華麗、復古、幾何、優雅……跨越時代的流行元素與文化，自由組合、碰撞。在GetMore二次時尚，每件好衣都是獨一無二...
						</div>
						</a>
					</div>
					<div class='article-box'>
						<a href='#'>
						<div class='article-title'>
							<strong>
							反「快速時尚」，重新思考衣服的價值
							</strong>
						</div>
						<div class='article-thumbnail'>
								<img src='http://getmore.cc/upload/cbdf2739b482ca6a26dad85206fbdf40.jpg'>	
						</div>
												<div class='article-time'>
						<span class="article-date">
        				2014/07/18                
    					</span>
    					<span class="author-name">
       					 | <!--<a href="#"> GetMore 二次時尚編輯</a>-->Ting 
       					</span>
       					</div>
						<div class='article-abstract'>
							我們從觀察市場、生活型態、生活經驗發現，每個人或多或少都有幾件明明還很新，卻不常穿的衣服，甚至不少人有衣櫃爆炸的困擾...
						</div>
						</a>
					</div>
					<img class='banner-s' src="/assets/images/banner-s/member.jpg">
			
					<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fgetmore.cc&amp;width=980&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true&amp;appId=207910605914306" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:980px; height:258px;" allowTransparency="true"></iframe>
					
				</div>
