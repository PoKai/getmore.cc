<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<style>
.editpimg {
  width: 30px;
  margin: 5px;
}
.col-254 {
    padding-right: 10px;
    padding-top:65px;"
}
.col-726{
    padding-right: 0;
}
.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
  vertical-align: middle;
  font-size: 10px;
}
.edit-icon{
    vertical-align: bottom;
    margin-left: 3px;
}
a{
    color: #687E84;
}
.list-group-item.active, .list-group-item.active:hover, .list-group-item.active:focus{
  background-color: #006699;
  border-color: #006699;
}

.upload-direction{
  color:#565a5c;
  letter-spacing: 3px;
  padding: 10px 20px; 

}

.upload-direction p{
  font-size: 14px;
  line-height: 20px;
}

.hellpseller{
 padding: 20px; 
}

.planabox{
    padding:30px 20px;
    margin-bottom: 14px;
    min-height: 500px;
    background-color: #f7f7f9;
    border: 1px solid #e1e1e8;
    border-radius: 4px;
}

.plananext{
    position:absolute;
    bottom:0px;
    right:0px;
}

.btn-plananext{
    min-width: 150px;
}

.planaex{
    font-size: 10px;
    color: #bbb;
    margin: 0 0 10px;
}

.textarea{
    resize: vertical;
}

.planabtn{
    margin: 20px 0;
    text-align: center;
}

.upload-direction{
  color:#565a5c;
  letter-spacing: 3px;
  padding: 10px 20px; 

}

.selectcolor{
    margin-bottom: 20px;
}

.selectstyle{
    margin-bottom: 20px;
}

.form-control{
    font-size: 10px;
    letter-spacing: 2px;
}

.form-control[disabled], fieldset[disabled] .form-control {
    cursor: not-allowed;
}

.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: #eee;
    opacity: 1;
}

.show-title{
    text-decoration: underline;
    color: #565a5c;
    font-size: 14px;
    margin-bottom: 10px;
}

.show-content{
    color: #000;
    font-size: 12px;
    margin-bottom: 10px;
}

.show-img img{
    width: 120px;
    margin-bottom: 10px;
}

</style>
    <!-- modals -->
<div class="modal fade" id='edit-img'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">註冊會員</h4>
            </div>
            <div class="modal-body">
                <div class="fb_login">
                    <h4>GetMore 是亞洲第一個二手衣時尚購物網站</h4>
                    <a class='fb_login_btn btn btn-primary' href="<?php echo site_url('/auth/facebook?redirect_url=' . urlencode(current_url()))?>">用facebook登入</a> 快使用facebook快速登入!
                    <hr class="dashed_hr"/>
                </div>
                <div id="getmore_login">
                    <div class="row">
                        <div class="col-md-4">
                            <h4>註冊會員</h4> 
                        </div>
                        <div class="col-md-8 pull-right announcement getmore_instruction">
                            還不是GetMore的會員嗎？ 免費加入！
                        </div>
                    </div>
                    <form method="POST" action="/auth/register" name="dig_register">
                        <div class="form-group">
                            <label for="sign_up_email">信箱</label><div class='col-md-5 pull-right description'>此為您的會員登入帳號</div>
                            <input type="email" class="form-control" id="email" name="email" placeholder="email">
                        </div>
                        <div class="form-group">
                            <label for="nickname">暱稱</label> <div class='col-md-5 pull-right description'>14字元以內</div>
                            <input type="text" class="form-control" id="user_name" name="user_name" placeholder="暱稱">
                        </div>
                        <div class="form-group">
                            <label for="new_password">密碼</label>
                            <input type="password" class="form-control" id="new_password" name="password" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label for="password_confirm">確認密碼</label>
                            <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm Password">
                        </div>
                        <div class="form-group">
                            <input type="checkbox" value="1" name="aggree" style="width:20px;">我已閱讀，並同意<a href="/info/buy#buypolicy">GetMore 服務條款</a>
                        </div>
                        <a id='start_to_getmore' href="javascript:document.forms['dig_register'].submit();" class='btn btn-primary'>開始GetMore</a>
                        <div id='registration' class="col-md-4 pull-right">
                            已是會員？ <a id='logIn' href="#logInModal" data-toggle="modal" onClick='$("#signUpModal").modal("hide");'>由此登入！</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <p class='announcement text-center'>已成為「會員」或是點擊「用facebook 登入」按鈕的同時,<br>表示您已詳讀並同意GetMore相關的<a href="/info/buy#buypolicy" target='_blank'>購買條款</a>以及<a href="/info/sell#sellpolicy" target='_blank'>代洗代售服務條款</a>。</p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

    <!-- modals -->
<div class="modal fade" id='testmodal'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">註冊會員</h4>
            </div>
            <div class="modal-body">
                <div class="fb_login">
                    <h4>GetMore 是亞洲第一個二手衣時尚購物網站</h4>
                    <a class='fb_login_btn btn btn-primary' href="<?php echo site_url('/auth/facebook?redirect_url=' . urlencode(current_url()))?>">用facebook登入</a> 快使用facebook快速登入!
                    <hr class="dashed_hr"/>
                </div>
                <div id="getmore_login">
                    <div class="row">
                        <div class="col-md-4">
                            <h4>註冊會員</h4> 
                        </div>
                        <div class="col-md-8 pull-right announcement getmore_instruction">
                            還不是GetMore的會員嗎？ 免費加入！
                        </div>
                    </div>
                    <form method="POST" action="/auth/register" name="dig_register">
                        <div class="form-group">
                            <label for="sign_up_email">信箱</label><div class='col-md-5 pull-right description'>此為您的會員登入帳號</div>
                            <input type="email" class="form-control" id="email" name="email" placeholder="email">
                        </div>
                        <div class="form-group">
                            <label for="nickname">暱稱</label> <div class='col-md-5 pull-right description'>14字元以內</div>
                            <input type="text" class="form-control" id="user_name" name="user_name" placeholder="暱稱">
                        </div>
                        <div class="form-group">
                            <label for="new_password">密碼</label>
                            <input type="password" class="form-control" id="new_password" name="password" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label for="password_confirm">確認密碼</label>
                            <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm Password">
                        </div>
                        <div class="form-group">
                            <input type="checkbox" value="1" name="aggree" style="width:20px;">我已閱讀，並同意<a href="/info/buy#buypolicy">GetMore 服務條款</a>
                        </div>
                        <a id='start_to_getmore' href="javascript:document.forms['dig_register'].submit();" class='btn btn-primary'>開始GetMore</a>
                        <div id='registration' class="col-md-4 pull-right">
                            已是會員？ <a id='logIn' href="#logInModal" data-toggle="modal" onClick='$("#signUpModal").modal("hide");'>由此登入！</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <p class='announcement text-center'>已成為「會員」或是點擊「用facebook 登入」按鈕的同時,<br>表示您已詳讀並同意GetMore相關的<a href="/info/buy#buypolicy" target='_blank'>購買條款</a>以及<a href="/info/sell#sellpolicy" target='_blank'>代洗代售服務條款</a>。</p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="col-254">
    <div class="list-group">
      <a href="#" class="list-group-item active">
        上架二手衣
      </a>
      <a href="#" class="list-group-item">修改已上架資訊</a>
      <a href="#" class="list-group-item">設定交易政策</a>
      <a href="#" class="list-group-item">宣傳我的衣櫃</a>
      <a href="#" class="list-group-item">加入賣家的社群</a>
      <a href="#" class="list-group-item">賣家規範</a>
      <a href="#" class="list-group-item">賣家Q&A</a>
    </div>
    <hr>
    <div class="contactus">
        <p> Contact us
            <br> service@getmore.cc </p>
    </div>
</div>

<div class="col-726 ng-scope" ng-app="getmore_sell_manage">
    <div class="tab-content article-line">
        <div class="tab-pane active" id="GetMore">
            <div class="rightcol fontp15">
                <div id="carousel-example-generic" class="carousel slide ng-scope" data-ride="carousel" data-interval="false" style="height: auto;" ng-controller="launch">
                    <ul class="list-unstyled list-inline text-center">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active">選擇分類</li>
                        <li data-target="#carousel-example-generic" data-slide-to="1">風格???</li>
                        <li data-target="#carousel-example-generic" data-slide-to="2">商品敘述</li>
                        <li data-target="#carousel-example-generic" data-slide-to="3">填寫尺寸</li>
                        <li data-target="#carousel-example-generic" data-slide-to="4">購買資訊</li>
                        <li data-target="#carousel-example-generic" data-slide-to="5">確認上架</li>
                    </ul>
                    <div class="planabox">
                        <form name="launchForm" method="post" action="/closet/launch_product" class="carousel-inner ng-pristine ng-valid" role="listbox" enctype="multipart/form-data">                       
                        <div class="item active filelist">
                            <div class="upload-direction hellpseller">
                                <h3><i class="fa fa-bullhorn 3x" style="color:#FFCC22;"></i>&nbsp Hello! 自行上架的賣家！</h3>
                                <p>恭喜您完成審核成為GetMore的賣家，一起享受二次時尚生活的樂趣，在販售前，務必詳讀<a href="#">販售規定</a>，切勿私下交易破壞規則。</p>
                            </div>
                            <hr>
                            <div  class="upload-direction">
                                <h3>選擇商品的分類</h3>
                                <p>選擇一個正確的分類，讓買家正確找到商品。</p>
                            </div>
                            <!-- just view -->
                            <div class="form-group upload-direction">
                                <label for="product_title"><u>商品分類</u><sup>*必填</sup></label>
                                <div style="overflow:hidden;">
                                    <input type="hidden" value="" id="product_type">
                                    <div class="col-md-4 level_1">
                                        <h6>服裝</h6>
                                        <div class="radio">
                                            <label>
                                            <input type="radio" value="1" class="item_class1 item_class ng-pristine ng-untouched ng-valid" ng-model="category" name="3">Women</label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                            <input type="radio" value="2" class="item_class1 item_class ng-pristine ng-untouched ng-valid" ng-model="category" name="4">Men</label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                            <input type="radio" value="3" class="item_class1 item_class ng-pristine ng-untouched ng-valid" ng-model="category" name="5">Kids / Baby</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 level_2">
                                        <h6>類別</h6>
                                        <div class="category women" ng-show="category == 1" ng-hide="category != 1">
                                            <div class="radio">
                                                <label>
                                                <input type="radio" value="11" name="category2" class="item_class2 item_class ng-pristine ng-untouched ng-valid" ng-model="category2">上衣</label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                <input type="radio" value="12" name="category2" class="item_class2 item_class ng-pristine ng-untouched ng-valid" ng-model="category2">洋裝</label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                <input type="radio" value="13" name="category2" class="item_class2 item_class ng-pristine ng-untouched ng-valid" ng-model="category2">褲子</label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                <input type="radio" value="14" name="category2" class="item_class2 item_class ng-pristine ng-untouched ng-valid" ng-model="category2">裙子</label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                <input type="radio" value="15" name="category2" class="item_class2 item_class ng-pristine ng-untouched ng-valid" ng-model="category2">外套</label>
                                            </div>
                                        </div>
                                        <div class="category men ng-hide" ng-show="category == 2" ng-hide="category != 2">
                                            <div class="radio">
                                                <label>
                                                <input type="radio" value="21" name="category2" class="item_class2 item_class ng-pristine ng-untouched ng-valid" ng-model="category2">上衣</label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                <input type="radio" value="22" name="category2" class="item_class2 item_class ng-pristine ng-untouched ng-valid" ng-model="category2">襯衫</label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                <input type="radio" value="23" name="category2" class="item_class2 item_class ng-pristine ng-untouched ng-valid" ng-model="category2">褲子</label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                <input type="radio" value="24" name="category2" class="item_class2 item_class ng-pristine ng-untouched ng-valid" ng-model="category2">外套</label>
                                            </div>
                                        </div>
                                        <div class="category kid ng-hide" ng-show="category == 3" ng-hide="category != 3">
                                            <div class="radio">
                                                <label>
                                                <input type="radio" value="31" name="category2" class="item_class2 item_class ng-pristine ng-untouched ng-valid" ng-model="category2">連身衣</label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                            <input type="radio" value="32" name="category2" class="item_class2 item_class ng-pristine ng-untouched ng-valid" ng-model="category2">上衣</label>
                                                </div>
                                                <div class="radio">
                                            <label>
                                                <input type="radio" value="33" name="category2" class="item_class2 item_class ng-pristine ng-untouched ng-valid" ng-model="category2">下身</label>
                                                </div>
                                            <div class="radio">
                                                <label>
                                                <input type="radio" value="34" name="category2" class="item_class2 item_class ng-pristine ng-untouched ng-valid" ng-model="category2">洋裝</label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                <input type="radio" value="35" name="category2" class="item_class2 item_class ng-pristine ng-untouched ng-valid" ng-model="category2">外套</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 level_3" style="display: block;">
                                    <h6>進階分類</h6>
                                    <div class="category2 women ng-hide" ng-show="category2 == 15" ng-hide="category2 != 15">
                                    <div class="radio">
                                    <label>
                                    <input type="radio" value="151" name="category3" class="item_class3 item_class ng-pristine ng-untouched ng-valid" ng-model="category3">一般外套</label>
                                    </div>
                                    <div class="radio">
                                    <label>
                                    <input type="radio" value="152" name="category3" class="item_class3 item_class ng-pristine ng-untouched ng-valid" ng-model="category3">羽絨衣</label>
                                    </div>
                                    <div class="radio">
                                    <label>
                                    <input type="radio" value="153" name="category3" class="item_class3 item_class ng-pristine ng-untouched ng-valid" ng-model="category3">真皮外套</label>
                                    </div>
                                    </div>
                                    <div class="category2 men ng-hide" ng-show="category2 == 24" ng-hide="category2 != 24">
                                    <div class="radio">
                                    <label>
                                    <input type="radio" value="241" name="category3" class="item_class3 item_class ng-pristine ng-untouched ng-valid" ng-model="category3" ng-checked="true" checked="checked">一般外套</label>
                                    </div>
                                    <div class="radio">
                                    <label>
                                    <input type="radio" value="242" name="category3" class="item_class3 item_class ng-pristine ng-untouched ng-valid" ng-model="category3">羽絨衣</label>
                                    </div>
                                    <div class="radio">
                                    <label>
                                    <input type="radio" value="243" name="category3" class="item_class3 item_class ng-pristine ng-untouched ng-valid" ng-model="category3">真皮外套</label>
                                    </div>
                                    </div>
                                    <div class="category2 kid ng-hide" ng-show="category2 == 35" ng-hide="category2 != 35">
                                    <div class="radio">
                                    <label>
                                    <input type="radio" value="351" name="category3" class="item_class3 item_class ng-pristine ng-untouched ng-valid" ng-model="category3" ng-checked="true" checked="checked">一般外套</label>
                                    </div>
                                    <div class="radio">
                                    <label>
                                    <input type="radio" value="352" name="category3" class="item_class3 item_class ng-pristine ng-untouched ng-valid" ng-model="category3">羽絨衣</label>
                                    </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div>
                                <div class="upload-direction">
                                    <h3>選擇款式</h3>
                                    <p>對於不同分類，選擇適當的款式，讓您的商品正確曝光。</p>
                                </div>
                                <div class="form-group  upload-direction">
                                    <!-- ngIf: category2 != 34 && category2 != 31; --><label for="product_title" ng-if="category2 != 34 &amp;&amp; category2 != 31;" class="ng-scope"><u>款式</u><sup>*必填</sup></label><!-- end ngIf: category2 != 34 && category2 != 31; -->
                                    <!-- ngIf: category == 1 --><div ng-if="category == 1" class="ng-scope">
                                    <!-- ngIf: category2 == 11 --><div class="item_pattern ng-scope" ng-if="category2 == 11">
                                    <label>
                                    <input type="radio" value="3" name="pattern" ng-model="r_pattern" class="ng-pristine ng-untouched ng-valid"> T-Shirt
                                    </label>
                                    <label>
                                    <input type="radio" value="23" name="pattern" ng-model="r_pattern" class="ng-pristine ng-untouched ng-valid"> 襯衫
                                    </label>
                                    <label>
                                    <input type="radio" value="19" name="pattern" ng-model="r_pattern" class="ng-pristine ng-untouched ng-valid"> 背心
                                    </label>
                                    <label>
                                    <input type="radio" value="10" name="pattern" ng-model="r_pattern" class="ng-pristine ng-untouched ng-valid"> 娃娃裝
                                    </label>
                                    <label>
                                    <input type="radio" value="2" name="pattern" ng-model="r_pattern" class="ng-pristine ng-untouched ng-valid"> Polo衫
                                    </label>
                                    <label>
                                    <input type="radio" value="33" name="pattern" ng-model="r_pattern" class="ng-pristine ng-untouched ng-valid"> 雪紡
                                    </label>
                                    <label>
                                    <input type="radio" value="28" name="pattern" ng-model="r_pattern" class="ng-pristine ng-untouched ng-valid"> 針織/毛衣
                                    </label>
                                    <label>
                                    <input type="radio" value="30" name="pattern" ng-model="r_pattern" class="ng-pristine ng-untouched ng-valid"> 長上衣
                                    </label>
                                    <label>
                                    <input type="radio" value="7" name="pattern" ng-model="r_pattern" class="ng-pristine ng-untouched ng-valid"> 其他
                                    </label>
                                    </div><!-- end ngIf: category2 == 11 -->
                                    <!-- ngIf: category2 == 12 -->
                                    <!-- ngIf: category2 == 13 -->
                                    <!-- ngIf: category2 == 14 -->
                                    <!-- ngIf: category2 == 15 -->
                                    </div><!-- end ngIf: category == 1 -->
                                    <!-- ngIf: category == 2 -->
                                    <!-- ngIf: category == 3 -->
                                </div>
                            </div>
                            <div class="form-group  upload-direction">
                                <!-- ngIf: category2 == 11 || category2 == 12 || category2 == 21 || category2 == 22 || category2 == 32 || category2 == 34 --><div ng-if="category2 == 11 || category2 == 12 || category2 == 21 || category2 == 22 || category2 == 32 || category2 == 34" class="ng-scope">
                                <label for="product_title"><u>袖長</u><sup>*必填</sup></label>
                                <div class="item_pattern">
                                    <label>
                                        <input type="radio" value="ns" name="item_long" ng-model="r_long" class="ng-pristine ng-untouched ng-valid"> 無袖
                                    </label>
                                    <label>
                                        <input type="radio" value="ss" name="item_long" ng-model="r_long" class="ng-pristine ng-untouched ng-valid"> 短袖
                                    </label>
                                    <label>
                                        <input type="radio" value="ls" name="item_long" ng-model="r_long" class="ng-pristine ng-untouched ng-valid"> 長袖
                                    </label>
                                </div>
                                </div>
                            </div>
                            <div class="form-group upload-direction">
                                <label for="product_title"><u>季節</u><sup>*必填</sup></label>
                                <div class="item_pattern" ng-init="r_season = 1">
                                    <label>
                                        <input type="radio" value="1" name="season" ng-model="r_season" class="ng-pristine ng-untouched ng-valid"> 春夏
                                    </label>
                                    <label>
                                        <input type="radio" value="2" name="season" ng-model="r_season" class="ng-pristine ng-untouched ng-valid"> 秋冬
                                    </label>
                                    <label>
                                        <input type="radio" value="3" name="season" ng-model="r_season" class="ng-pristine ng-untouched ng-valid"> 無季節
                                    </label>
                                </div>
                            </div>
                            <div class="planabtn">
                                <a class="btn btn-info" data-target="#carousel-example-generic" data-slide-to="1">下一步，填寫風格</a>
                            </div>
                        </div>

                        <!-- add  size-->
                        <div class="item">
                            <div class="upload-direction">
                                <h3>填寫尺寸</h3>
                                <p>利用GetMore的系統，並依照您的尺寸標籤標示尺寸。不明瞭？來看看<a>影片說明</a>。</p>
                            </div>
                            <div class="form-group upload-direction">
                                <label for="size_type_major">先選單位</label>
                                <select class="form-control width375 ng-pristine ng-untouched ng-valid" name="size_type_major" id="size_type_major" ng-model="size_type_major">
                                <option value="US">US</option>
                                <option value="UK">UK</option>
                                <option value="EU">EU</option>
                                <option value="IT">IT</option>
                                <option value="JP">JP</option>
                                <option value="腰圍">腰圍</option>
                                <option value="其他">其他</option>
                                </select>
                            </div>

                            <div class="form-group upload-direction">
                                <label  for="size">再選SIZE</label>
                                <select class="form-control width375 ng-pristine ng-untouched ng-valid" name="size_type_major" id="size_type_major" ng-model="size_type_major" disabled>
                                <option value="US">US</option>
                                <option value="UK">UK</option>
                                <option value="EU">EU</option>
                                <option value="IT">IT</option>
                                <option value="JP">JP</option>
                                <option value="腰圍">腰圍</option>
                                <option value="其他">其他</option>
                                </select>
                            </div>
                            <div class="form-group upload-direction">
                                <label for="size_comment">其他尺寸敘述</label>
                                <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="size_comment" maxlength="20" ng-model="size_comment" name="size_comment" placeholder="加上更多尺寸註解">
                                <div class="planaex">例如：很寬鬆適合各式尺寸</div>
                            </div>
                            <div class="form-group upload-direction">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="measure_table">
                                    <tbody>
                                        <tr>
                                            <td rowspan="10" align="right">
                                                <img ng-src="[[ category2|measure_img:r_long:r_pattern ]]" id="measure_img" style="height: auto;">
                                            </td>
                                            <td style="height: 30px;">
                                            <div class="input-group">
                                                <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure1" name="measure[1]" ng-model="measure[1]">
                                                <span class="input-group-addon">cm</span>
                                            </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure2" name="measure[2]" ng-model="measure[2]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure3" name="measure[3]" ng-model="measure[3]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure4" name="measure[4]" ng-model="measure[4]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure5" name="measure[5]" ng-model="measure[5]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure6" name="measure[6]" ng-model="measure[6]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure7" name="measure[7]" ng-model="measure[7]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure8" name="measure[8]" ng-model="measure[8]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure9" name="measure[9]" ng-model="measure[9]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure10" name="measure[10]" ng-model="measure[10]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="planabtn">
                                <a class="btn btn-success" role="button" data-target="#carousel-example-generic" data-slide-to="2">上一步</a>
                                <a class="btn btn-info" role="button" data-target="#carousel-example-generic" data-slide-to="4">下一步，填寫販售資訊</a>
                            </div>
                        </div>      
           <div class="item">
                        <div class="upload-direction">
                            <h3>設定價格</h3>
                            <p>原始價格以及定價，建議您設定的價格是原價的2折以下，請勿超過4折。</p>
                        </div>
                        <div class="form-group upload-direction">
                            <label for="product_original_price"><u>原始價格</u></label>
                            <select class="form-control width375" id="product_original_price" name="product_original_price">
                            <option value="1000">500元~1,000元</option><option value="2000">1,001元~2,000元</option><option value="3000">2,001元~3,000元</option><option value="4000">3,001元~4,000元</option><option value="5000">4,001元~5,000元</option><option value="6000">5,001元~6,000元</option><option value="7000">6,001元~7,000元</option><option value="8000">7,001元~8,000元</option><option value="9000">8,001元~9,000元</option><option value="10000">9,001元~10,000元</option><option value="15000">10,001元~15,000元</option><option value="20000">15,001元~20,000元</option><option value="25000">20,001元~25,000元</option><option value="30000">25,001元~30,000元</option><option value="35000">30,001元~35,000元</option><option value="40000">35,001元~40,000元</option><option value="45000">40,001元~45,000元</option><option value="50000">45,001元~50,000元</option><option value="60000">50,001元~60,000元</option><option value="70000">60,001元~70,000元</option><option value="80000">70,001元~80,000元</option><option value="90000">80,001元~90,000元</option><option value="100000">90,001元~100,000元</option><option value="999999">超過100,000元</option>
                            </select>
                            <p>建議價商品定價為：<font class="red">$800</font> </p>
                        </div>
                        <div class="form-group upload-direction">
                            <label for="product_sell_price"><u>定價</u></label>
                            <div class="input-group" style="width:120px;">
                                <div class="input-group-addon">$</div>
                                <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="product_sell_price" maxlength="20" ng-model="product_sell_price" name="product_sell_price">
                            </div>
                            <div class="planaex">建議是原價的2折，並包含平均運費。</div>
                        </div>
                        <hr>
                        <div class="upload-direction">
                            <h3>編輯交易政策</h3>
                            <p>已自動帶入您衣櫃的交易政策，會套用到所有商品上。請參考<a>交易政策影片</a>。</p>
                        </div>
                        <div class="form-group upload-direction">
                        <label for="trade_time"><u>寄出時間</u></label>
                        <p>請選擇一種寄出時間，並且預估送達時間讓買家參考。</p>
                            <textarea class="form-control ng-pristine ng-untouched ng-valid" id="trade_time" name="trade_time" ng-model="trade_time" rows="5" placeholder=" 商品出貨時間：
                            確認付款後的 3 天內會將商品郵寄給您。
                            商品運送時間 (台灣)：
                            寄出後約 3 個工作天內送達 (不含週六日及國定假日)。">                                
                            </textarea>
                            <div class="planaex">字數限制100字，還剩100字。</div>
                        </div>
                        <div class="form-group upload-direction">
                            <label for="trade_method"><u>寄送方式</u></label>
                            <p>請選擇一種寄送方式</p>
                            <select class="form-control width375" name="trade_method" id="trade_method">
                            <option value="黑貓宅急便">黑貓宅急便</option>
                            <option value="郵局">郵局</option>
                            <option value="宅配通">宅配通</option>
                            <option value="新竹物流">新竹物流</option>
                            <option value="其他">其他</option>
                            </select>
                        </div>
                        <div class="form-group upload-direction">
                            <label for="trade_returns"><u>退貨政策</u></label>
                            <p>寫明商品退貨的原則，可以減少日後糾紛。</p>
                            <textarea class="form-control ng-pristine ng-untouched ng-valid" id="trade_returns" name="trade_returns" ng-model="trade_returns" rows="5" placeholder="退換貨政策：在收到商品後，請立刻檢查商品是否正確，如果有問題請於 3 天內與我連絡，並於我回覆地址後 2 天內保持完整連同原商品包裝寄回，超過時限即視為無誤。商品為二手商品只有一件，並非完美，退貨請自行負擔運費。">                                </textarea>
                            <div class="planaex">字數限制100字，還剩100字。</div>
                        </div>
                        <div class="planabtn">
                            <a class="btn btn-success" role="button" data-target="#carousel-example-generic" data-slide-to="3">上一步</a>
                            <a class="btn btn-info" role="button" data-target="#carousel-example-generic" data-slide-to="5">下一步，確認上架資訊</a>
                        </div>
                    </div>
                    <div class="item">
                        <div  class="upload-direction">

                            <h3><i class="fa fa-pencil-square-o 3x" style="color:#4DBAA9;"></i>&nbsp 確認您填入的資料</h3>
                            <p>以下為您填入的資料，可按「上一步」往前編輯內容，並再次確認是否符合<a href="">販售規定</a>，確認無誤後請按「確認上架」，商品會立即上架。</p>
                        </div>
                        <hr>
                        <div class="upload-direction">
                            <div class="show-title">商品分類</div>
                            <div class="show-content">Women > 外套 > 羽絨衣</div>

                            <div class="show-title">款式</div>
                            <div class="show-content">Women > 外套 > 羽絨衣</div>

                            <div class="show-title">袖長</div>
                            <div class="show-content">Women > 外套 > 羽絨衣</div>

                            <div class="show-title">季節</div>
                            <div class="show-content">Women > 外套 > 羽絨衣</div>
                            <hr>

                            <div class="show-title">商品名稱</div>
                            <div class="show-content">Women > 外套 > 羽絨衣</div>
                            <div class="show-title">品牌</div>
                            <div class="show-content">Women > 外套 > 羽絨衣</div>
                            <div class="show-title">商品描述</div>
                            <div class="show-content">Women > 外套 > 羽絨衣</div>
                            <div class="show-title">商品圖片</div>
                            <div class="show-img">
                                <img src="http://getmore.cc/upload/thumbs/250_375_4cc2b099423968084a2035c57000a8ef.JPG">
                                <img src="http://getmore.cc/upload/thumbs/250_375_4cc2b099423968084a2035c57000a8ef.JPG">
                                <img src="http://getmore.cc/upload/thumbs/250_375_4cc2b099423968084a2035c57000a8ef.JPG">
                            </div>
                            <hr>
                            <div class="show-title">商品尺寸</div>
                            <div class="show-content">US 5 / </div>
                            <div class="show-title">丈量尺寸</div>
                            <div class="show-title">商品價格</div>
                            <div class="show-content">原始價格：$1000~$2000<br>
                            販售價格：$500
                            </div>
                            <div class="show-title">交易政策</div>
                            <div class="show-content">交易政策是全專櫃統一的，您可以<a href="#">另開視窗</a>確認及修改您的交易政策。<br>
                                <hr>
                            </div>
                            <div class="planabtn">
                                <a class="btn btn-success" role="button" data-target="#carousel-example-generic" data-slide-to="4">上一步</a>
                                <a type="submit" class="btn btn-info" data-target="#carousel-example-generic">完成上架！</a>
                            </div>
                        </div>
                    </div> <!--item -->
                </form>
                    </div>
                </div>
            </div> <!--Planbboc-->
        </div>
    </div>
</div>