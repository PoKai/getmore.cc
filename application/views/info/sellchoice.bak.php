 			<img class='banner-s' src="/assets/images/banner-s/GetMoreLove.jpg">
		<div class="row rightcol marginleft40 marginright40">
	        <h4 class="marginbottom20">你有衣櫃爆炸的煩惱嗎?想賣卻不知該如何下手嗎?</h4>
	        <p>
                感謝各位2014年開站來的支持，目前GetMore網站上，有超過2,000款二手衣可供喜歡時尚的朋友選購，也感謝優質賣家，利用GetMore的賣衣服功能，出售精選的好衣，讓好衣服再次被愛。
            <p />
            <p>
                我們了解賣家對衣服的喜愛以及分享的狂熱，2015年，GetMore 預計推出全新的賣家方案(A方案)，提供喜歡分享衣服品味的賣家一個全新的自行管理販售方式，不限品牌，只限好衣，敬請期待。
            </p>
            <p>
                為了在專業代售上將更專注在高品質品牌服飾，GetMore首推的代洗代售方案(B方案)將採品牌限定。<br/>
                <u>2015年2月1日開始，代售申請只限定品牌，務必參照<a href="/info/brandsonly">品牌列表</a>！</u>
            </p>
            <p>
                自行上架方案再等我們一下，賣家趕快動手整理衣櫃中閒置如新的品牌服飾，簡單填寫商品資訊，分享時尚品味增加收入。
            </p>
            <p></br><center><a href="/info/brandsonly"><button type="button" class="btn btn-primary">開始代售申請</button></a></center></p>
    	</div>
    	<hr>
				<div class='contentbox'>
					<div class="sellchoice">
						<ul class="levels">
							<li class="planA">
								<div class="sellbox">
									<div class="marginbottom60 getmoregreen"><h3>自行上架賣家</h3><h3>Market Boss</h3></div>
									<button class="btn btn-lg levelsbtn btn-notwork" disabled="disabled">即將推出</button>
									<div class="margintop30">
										<ul class="whychoice">
                                            <li>自由的分享樂趣</li>
											<li>自行拍照</li>
											<li>自行清洗整燙</li>
											<li>自行管理</li>
											<li>當個自由的小老闆</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="planB">
								<div class="sellbox">
									<div class="marginbottom60 getmoregreen"><h3>品牌代售賣家</h3><h3>Consignment</h3></div>
                                    <a href="/info/brandsonly"><button class="btn btn-success btn-lg levelsbtn">申請品牌代售</button></a>
									<div class="margintop30">
										<ul class="whychoice">
                                            <li>尊榮的品牌管家</li>
											<li>GetMore 管理</li>
											<li>代為送洗</li>
											<li>統一出貨</li>
											<li>交給GetMore，專業的品牌代售</li>
										</ul>
									</div>
								</div>
							</li>
						</ul>

					</div>
				</div>

			<hr>
		<div class="getmorecenter-block">
		  <center>
		  	<table class="table table-bordered" id="sellchoisetable">
		  	<tr>
		  		<td></td>
		  		<td class="getmoregreen font-size18">
		  		<center>
		  			A方案：自行上架賣家
		  		</center>
		  		</td>
		  		<td class="getmoregreen font-size18">
		  		<center>
		  			B方案：品牌代售賣家
		  		</center>
		  		</td>
		  	</tr>
		  	<tr>
		  		<td align="center" valign="center">
		  			適用賣家
		  		</td>
		  		<td>
		  			對穿搭攝影有興趣，覺得賣衣服是一種分享樂趣，可以把產品照拍得美美的服裝狂熱份子。
		  		</td>
		  		<td>
		  			對於國際品牌以及高級成衣迷戀者，卻無暇處理洗衣拍照上架流程，不想自行出貨者。
		  		</td>
		  	</tr>
		  	<tr>
		  		<td align="center">
		  			代表人物
		  		</td>
		  		<td align="center">
		  			達人、文青、嬉皮、設計人、Boutique店家
		  		</td>
		  		<td align="center">
		  			貴婦、職場女性、明星、模特兒
		  		</td>
		  	</tr>
		  	<tr>
		  		<td align="center">
		  			管理方式
		  		</td>
		  		<td>
		  			自行管理<br/>
					賣家自行清洗、拍照、丈量、上架、包裝、管理訂單以及出貨
		  		</td>
		  		<td>
		  			GetMore管理<br/>
					預付洗衣費，GetMore全部幫您搞定
		  		</td>
		  	</tr>
		  	<tr>
		  		<td align="center">
		  			出貨方式
		  		</td>
		  		<td>
		  			自行出貨
		  		</td>
		  		<td>
		  			代售期間內由GetMore統一出貨
		  		</td>
		  	</tr>
		  	<tr>
		  		<td align="center">
		  			客服聯繫
		  		</td>
		  		<td>
		  			透過GetMore訊息系統管理客服
		  		</td>
		  		<td>
		  			由GetMore管理
		  		</td>
		  	</tr>
		  	<tr>
		  		<td align="center">
		  			收費方式
		  		</td>
		  		<td>
		  			相關費用於A方案上線後公佈
		  		</td>
		  		<td>
		  			其他相關費用請參照詳細收費方式<br>
					洗衣費用＋成交所得 X 25%<br>
                    A方案上線前免收<s>金流服務費5%及營業稅5%</s><br>
                    <a href="/blog/view/69"  target="_block">說明</a>
		  		</td>
		  	</tr>
		  	<tr>
		  		<td align="center">
		  			如何領取現金
		  		</td>
		  		<td colspan="2">
		  			鑑賞期過後，扣除費用後的賣衣服所得將於15~30天內入到「線上帳戶」，
					您可選擇在購買時抵扣，或是選擇匯出現金至您指定帳戶。
					由於會計作業，現金匯出依照您每月15日前的每筆申請(每筆匯款費用為15元)，
					統一每月20日匯入您指定的銀行帳戶(非上班日則順延)。
		  		</td>
		  	</tr>
		  	<tr>
		  		<td align="center">
		  			如何加入
		  		</td>
		  		<td>
		  			審核制，通過後開通系統即可開始
                    <br><a href="/blog/view/70" target="_block">我有興趣參與，說明會請通知我</a>
		  		</td>
		  		<td>
		  			詳閱相關規定，線上申請即可開始
		  		</td>
		  	</tr>
		  	<tr>
		  		<td>

		  		</td>
		  		<td>
		  			<center>
		  			<button class="btn btn-lg levelsbtn btn-notwork"  disabled="disabled">即將推出</button>
		  			</center>
		  		</td>
		  		<td>
		  			<center>
                        <a href="/info/brandsonly"><button class="btn btn-success btn-lg levelsbtn">申請品牌代售</button></a>
					</center>
		  		</td>
		  	</tr>



		  </table>
		</center>
		</div>
