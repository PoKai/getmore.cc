<script src="/assets/javascript/howtomakeaturn/clothing.js"></script>

<img class='banner-s' id='top' src="/assets/images/images/info/sell-02.jpg">

<div class='row rightcol'>
    <div class='col-md-7'>
        <h4 class="marginbottom20">你有衣櫃爆炸的煩惱嗎?想賣卻不知該如何下手嗎?</h4>
        
        <p>
          GetMore專為時尚人士打造了一個全新的二手衣代售平台,<br />
          若你有不少專櫃品牌、設計師款且符合GetMore代售條件的好衣,<br />
          趕快動手整理,讓你閒置在衣櫃的好衣變成秒殺夯貨!
        </p>
    </div>
    <div class='col-md-5 margintop20'>
        <strong>
            <table style="width:320px; height:180px; border:0; text-align:center">
                <tr>
                    <td><img src="/assets/images/info/sell_type/brand.png"></td>
                    <td><img src="/assets/images/info/sell_type/design.png"></td>
                </tr>
                 <tr>
                    <td><center>專櫃品牌</center></td>
                    <td><center>設計師款</center></td>
                </tr>
            </table>
        </strong>
    </div>
</div>

<div>
    <center>
    <a href='/clothing/soldout' target="_blank" class='btn btn-primary marginleft10 marginright10' style='background-color: #FFF78F; color: black;'>已售出的好衣</a>
    <a href='/info/sell#howtosell' target="_blank" class='btn btn-primary marginleft10 marginright10'>賣衣服流程</a>
    <a href='/info/startsell' target="_blank" class='btn btn-primary marginleft10 marginright10'>開始賣衣服</a>
    </center>
</div>

<div class='getmore-hot'>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />

    <section class='types'>
        
        <div class='row'>
            
            <div class='col-md-1'>
                <a href='#type1'>
                    <img class='banner-s' src="/assets/images/info/sell_type/sell-05.png">        
                </a>
            </div>
            
            <div class='col-md-1'>
                <a href='#type2'>
                    <img class='banner-s' src="/assets/images/info/sell_type/sell-06.png">
                </a>
            </div>
            
            <div class='col-md-1'>
                <a href='#type3'>
                    <img class='banner-s' src="/assets/images/info/sell_type/sell-07.png">        
                </a>
            </div>
            
            <div class='col-md-1'>
                <a href='#type4'>
                    <img class='banner-s' src="/assets/images/info/sell_type/sell-08.png">
                </a>
            </div>
            
            <div class='col-md-1'>
                <a href='#type5'>
                    <img class='banner-s' src="/assets/images/info/sell_type/sell-09.png">
                </a>
            </div>
                    
        </div>

        <div class='row'>
            
            <div class='col-md-1'>
                <a href='#type1'>
                經典品牌
                </a>
            </div>
            
            <div class='col-md-1'>
                <a href='#type2'>
                個性單品
                </a>
            </div>
            
            <div class='col-md-1'>

                <a href='#type3'>
                甜美氣質
                </a>
            </div>
            
            <div class='col-md-1'>
                <a href='#type4'>
                清新休閒
                </a>
            </div>
            
            <div class='col-md-1'>
                <a href='#type5'>
                率性牛仔
                </a>
            </div>
                    
        </div>
    
    </section>

</div>


    <!-- fresh product -->
    <div class="fresh">
      <span class="fresh-title" id='type1'>
        經典品牌 
      </span>
      世界名牌、知名品牌一直都是熱門搶手貨。
      <a href='#top'>Go To Top↑</a>
      <div class="fresh-box">
      <?php foreach ($clothings1 as $clothing):?>  
        <?php echo clothing_thumbnail_layout($clothing); ?>
      <?php endforeach;?>
      </div>
    </div>
    <!-- fresh product -->
    <div style='clear: both;'></div>

    <!-- fresh product -->
    <div class="fresh">
      <span class="fresh-title" id='type2'>
        個性單品 
      </span>
      不管是特殊剪裁、設計師款、別緻的小設計等等，簡單搭配就能展現與眾不同的時尚感，深愛許多個性女孩喜愛 。
      <a href='#top'>Go To Top↑</a>
      <div class="fresh-box">
      <?php foreach ($clothings2 as $clothing):?>  
        <?php echo clothing_thumbnail_layout($clothing); ?>
      <?php endforeach;?>
      </div>
    </div>
    <!-- fresh product -->
    <div style='clear: both;'></div>

    <!-- fresh product -->
    <div class="fresh">
      <span class="fresh-title" id='type3'>
        甜美氣質 
      </span>
      約會、舞會、獨自的美好時光，女孩們總是希望自己打扮得甜美可愛、氣質出眾，衣櫃裡一定要有這樣的洋裝！
      <a href='#top'>Go To Top↑</a>

      <div class="fresh-box">
      <?php foreach ($clothings3 as $clothing):?>  
        <?php echo clothing_thumbnail_layout($clothing); ?>
      <?php endforeach;?>
      </div>
    </div>
    <!-- fresh product -->
    <div style='clear: both;'></div>

    <!-- fresh product -->
    <div class="fresh">
      <span class="fresh-title" id='type4'>
        清新休閒 
 
      </span>
      小清新、自然系列，舒適又自在的風格是現在輕旅行、小踏青的熱門打扮。
     <a href='#top'>Go To Top↑</a>


      <div class="fresh-box">
      <?php foreach ($clothings4 as $clothing):?>  
        <?php echo clothing_thumbnail_layout($clothing); ?>
      <?php endforeach;?>
      </div>
    </div>
    <!-- fresh product -->
    <div style='clear: both;'></div>

    <!-- fresh product -->
    <div class="fresh">
      <span class="fresh-title" id='type5'>
        率性牛仔 
      </span>
      兼具休閒與時尚的牛仔系列，一直是大家心中的必備基本款，特別是牛仔襯衫，人人愛不釋手。
      <a href='#top'>Go To Top↑</a>

      <div class="fresh-box">
      <?php foreach ($clothings5 as $clothing):?>  
        <?php echo clothing_thumbnail_layout($clothing); ?>
      <?php endforeach;?>
      </div>
    </div>
    <!-- fresh product -->
    <div style='clear: both;'></div>

<script type="text/javascript">
  
    $(function () {
        LikeButtonModule.initialize();
        <?php if($logined): ?>
            LikeButtonModule.setIsLogined(true);
        <?php endif; ?>
    });
</script>

<style>
    .getmore-hot{
        background-color: red;      
        padding: 40px 20px;
        margin: 40px 0;
        position: relative;
        background-image: url('/assets/images/info/sell_type/sell-04.png');
        width: 980px;
        height: 326px;
    }
    
    .getmore-hot h3{
        color: red;
    }
    
    .popular-style{
        position: absolute;
        right: 40px;
        top: 40px;
        color: red;
        border: 1px solid red;
        padding: 5px;
    }
    
    .types{
        width: 700px;
        margin: 0 auto;
    }
  
    @media (min-width: 768px){
        .types .col-md-1{
            width: 100%;
        }
    }

    @media (min-width: 992px) {
        .types .col-md-1{
            width: 20%;
        }
    }
</style>
