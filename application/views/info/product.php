				<div class='row' id='pagerow'>
					<hr>
						<table>
							<tr>
								<td rowspan="3"><a href="#a"><img src="/assets/images/info/buy/productg-01.png"></a></td>
								<td><a href="#b"><img src="/assets/images/info/buy/productg-02.png"></a></td>
							</tr>
							<tr>
								<td><a href="#c"><img src="/assets/images/info/buy/productg-03.png"></a></td>
							</tr>
							<tr>
								<td><a href="#d"><img src="/assets/images/info/buy/productg-04.png"></a></td>
							</tr>
						</table>
						<div class="marginbottom40"></div>
						<h2  id="a">商品頁說明</h2>
						<hr>
						<div class="row">
							<div class="col-md-6"><img src="/assets/images/info/buy/productg-05.png"></div>
								<div class="col-md-6 ">
									<div class="alert alert-info bggetmorecolor alernheight">
										<div class="row infoproductalert">商品照片</div>
										<ul >
											<li class="infoproductalertli">一件商品基本上都會拍3張照片，正面、側面、背面。</li>
											<li class="infoproductalertli">將滑鼠移到照片上，右邊會出現商品放大的畫面，可以看每個部位的細節喔！</li>
											<li class="infoproductalertli">最後一張)會放詳細尺寸圖，點下即可看到詳細尺寸的丈量圖。</li>
										</ul>
										<div class="row infogotopheight">
										<div class="pull-right"  id="b"><a href='#top'>Go To Top↑</a></div>
										</div>
									</div>
								</div>
						</div>
						<hr>
						<div class="row" >
							<div class="col-md-6"><img src="/assets/images/info/buy/productg-06.png"></div>
								<div class="col-md-6 ">
									<div class="alert alert-info bggetmorecolor alernheight1">
										<div class="row infoproductalert">商品敘述</div>
										<ul >
											<li class="infoproductalertli">Size：按照商品布標上的尺寸做標示。若吊牌/布標遺失、無尺碼，我們會依據丈量尺寸結果，給予衣服一個近似尺碼。
																		<br>ex. Fits L：表示這件好衣尺寸近似 L。</li>
											<li class="infoproductalertli">由於每件好衣由不同國家、不同廠牌出產，標示尺碼的方式也會有所不同，可以參考【Learn more】做選購，但詳細尺寸還請務必參考細部丈量結果。</li>
											<li class="infoproductalertli">品牌：標示好衣品牌，若是無品牌商品會標示 N/A。</li>
											<li class="infoproductalertli">狀態驗收：GetMore將好衣狀態分為三類<br>
												 - 全新未拆吊牌：商品附原吊牌，且沒有使用痕跡。<br>
												 - 幾近全新：商品可能被穿過，但幾乎看不出使用痕跡。<br>
												 - 狀況良好：商品看得出使用痕跡，但整體狀況佳，沒有明顯的瑕疵。</li>
										    <li class="infoproductalertli">點評：衣管家對這件好衣的comment。</li>
										    <li class="infoproductalertli">備註：針對尺寸較特殊的好衣會再加強說明，或是有附配件等額外資訊。</li>
										</ul>
										<div class="pull-right"  id="c"><a href='#top'>Go To Top↑</a></div>
									</div>
								</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-6"><img src="/assets/images/info/buy/productg-07.png"></div>
								<div class="col-md-6 ">
									<div class="alert alert-info bggetmorecolor alernheight2">
										<div class="row infoproductalert">價格</div>
										<ul >
											<li class="infoproductalertli">除了這件好衣的售價外,還會有賣家提供的參考原價區間,好衣售價基本上都是 5 折以下的價格喔!若是有些猶豫不決,可以先將好衣加入欲望清單,賣家降價時您會收到通知,搶先購買!</li>
										</ul>
										<div class="pull-right"  id="d"><a href='#top'>Go To Top↑</a></div>
									</div>
								</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-6"><img src="/assets/images/info/buy/productg-08.png"></div>
								<div class="col-md-6 ">
									<div class="alert alert-info bggetmorecolor alernheight3">
										<div class="row infoproductalert">商品說明</div>
										<ul >
											<li class="infoproductalertli">提供商品的基本資訊,包含款式、顏色、材質、產地等,以及最重要的詳細尺寸。GetMore針對不同類型的好衣,提供非常詳盡的尺寸丈量資訊,希望買家可以安心地選購適合自已的商品。</li>
										</ul>
										<div class="pull-right"><a href='#top'>Go To Top↑</a></div>
									</div>
								</div>
						</div>
				</div>

<style>
    .infoproductalert{
    	margin-top: 10px;
    	padding-left: 40px;
		font-size: 15px;
		color: black;
		margin-bottom: 15px;
    }

    .infoproductalertli{
		font-size: 12px;
		color: black;
		margin-bottom: 10px;
    }

    .alernheight {
    	min-height: 298px;
    }

    .alernheight1 {
    	min-height: 420px;
    }

    .alernheight2 {
    	min-height: 160px;
    }

    .alernheight3 {
    	min-height: 200px;
    }

    .infogotopheight{
    	position: absolute;
    	top: 260px;
    	width: 400px;
    }

    .infogotopheight{
    	position: absolute;
    	top: 260px;
    	width: 400px;
    }
  
</style>