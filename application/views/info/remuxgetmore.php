				<div class='row' id='pagerow'>

					<center>
					<div class="margintop40 marginbottom40">
							<h2>RE.MU X GetMore</h2>
							<h2>2014.09.28 名人二手衣拍賣</h2>
					</div>
						
					
					<div class="fontp15">
						<p><u>世界最大造型師互動平台</u><a href="http://re.mu" target="_blank"><span class="text-danger"> RE.MU </span></a>與<u>亞洲第一個二次時尚網站</u><span class="getmoregreen"> GetMore </span>攜手合作，首場名人二手拍賣會!</p>
						<p>邀請藝人以及部落客，提供許多二手私服來拍賣噢！</p>
						<p>近距離接觸 & 買到明星二手衣 & 現場神祕互動，各位粉絲千萬別錯過！</p>
						<p class="remutitle"><a href="http://www.facebook.com/dingyifan0924" target="_blank">丁衣凡</a>｜<a href="http://www.facebook.com/LipstickCatwalk" target="_blank">Leah</a>｜<a href="http://www.facebook.com/meikuo0816" target="_blank">MeiMei</a>｜<a href="http://www.facebook.com/dailylifeofmomomax" target="_blank">MOMO小桃娘</a>｜<a href="https://www.facebook.com/cathyfans" target="_blank">Twinko徐凱希</a>｜<a href="http://www.facebook.com/hsyan0625" target="_blank">篠崎泫</a>｜<a href="https://www.facebook.com/love.shuwei" target="_blank">黃書維</a>｜<a href="https://www.facebook.com/remufashionstyle" target="_blank">RE.MU</a>｜<a href="https://www.facebook.com/getmore.cc" target="_blank">GetMore</a></p>
						<hr>
					</div>
					</center>
						<img src="/assets/images/event/BANNER-01.png">
						<hr>
						<ol><div class="remutitle">入場方式：</div>
						<li>下載RE.MU app 並上傳2張照片</li>
						<li>在GetMore粉絲團按讚並登入GetMore網站（http://getmore.cc）</li>
						</ol>
						<hr>
						<ol><div class="remutitle">活動方式：</div>
						<li>邀請的藝人提供二手服飾販售，部分藝人會到現場參與活動與粉絲拍照互動。</li>
						<li>到場藝人會現場改造T-shirt於GetMore網站上義賣。</li>
						<li>活動結束後沒有賣出的衣服於GetMore網站上架販售，無法到場的粉絲可以在網站上選購！</li>
						</ol>
						<hr>
						<div class="alert alert-success" role="alert">
						<ul class="list-unstyled">
						<li>時間｜2014.09.28(日) 13:00~18:00</li>
						<li>地點｜getmore lab咖啡 </li>
						<li>地址｜台北市大安區新生南路一段165巷18號1樓（大安森林公園站1號出口）<a href="https://goo.gl/maps/PAaGr" target="_blank">地圖</a></li>
						</ul>
						</div>

						
						<!--<center>
							<img src="/assets/images/event/map.jpg">
						</center>
						<br>
						<br> -->

						<div class="alert alert-danger" role="alert">
						<ul>注意事項：
						<li>為地球盡一份心力，請自備購物袋。</li>
						<li>二手商品皆需付現，現場不提供刷卡服務。</li>
						<li>請現場檢查商品狀況，售後不提供退換貨。</li>
						<li>請維持周邊寧靜及環境整潔。</li>
						</ul>
						</div>
						<br>
						</hr>
						
							<h3>認識RE.MU & GetMore</h3><br>
							<div class="introremu"><p>*RE.MU 是屬於時尚愛好者的分享平台.有十幾個國家的用戶.<br>不像一般時尚社群網站只分享全身穿搭.RE.MU做了分類的概念讓用戶全身上下服裝及配件都可以一一的分享給大家的交流平台<br>近期RE.MU推出[HIRE ME]新功能.用戶不在是只能分享穿搭讓不知道如何穿搭的人用照片仿照別人風格而已.<br>HIRE ME讓每個人都可以成為造型師.讓不懂穿搭的人更貼近自己喜歡的穿搭達人教你如何把現有衣服穿得更有型!
							</p>
							</div>
							<div class="introremu"><p>*GetMore 是一個為時尚生活打造的二手服飾買賣網站,2013年年底上線.提供了最簡單、有趣的方式,讓人們享受二手衣時尚的生活!<br>GetMore二次時尚提供簡單且全面的線上及線下服務。 以三折以下完美價格,可以買到品牌或是設計師服飾,且每一項商品都是保證清洗整燙的，輕鬆享受二手衣時尚生活的樂趣。<br>
							</p>
							</div><br>
							
						
						<h3>搶先看看暖身活動 [RE.MU X GetMore 一日造型師]</h3><br>
						<div class="youtube-box">						
							<iframe width="460" height="259" src="//www.youtube.com/embed/WBJhcVZwkIA" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="youtube-box">						
							<iframe width="460" height="259" src="//www.youtube.com/embed/MJZITmEpZuo" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="youtube-box">						
							<iframe width="460" height="259" src="//www.youtube.com/embed/_OOy4nGdcr8" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="youtube-box">						
							<iframe width="460" height="259" src="//www.youtube.com/embed/K-NcF_nieGU" frameborder="0" allowfullscreen></iframe>
						</div>
				<style>

					.remutitle{
						color: red;
						font-size: 14px;
					}
					.youtube-box {
						width: 460px;
						height: 259px;
						float: left;
						margin: 0px 10px 20px 10px;
						border: 1px solid #4DBAA9;
						}
					.introremu {
						width: 460px;
						height: 259px;
						float: left;
						margin: 0px 10px 20px 10px;
						border: 0px;
						}
					.remutitle a {
						color: red;
					}
				</style>