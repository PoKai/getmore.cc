<img class='banner-s' id='top' src="/assets/images/banner/brandsonly.jpg">


<div class='row rightcol marginleft40 marginright40'>
	<div>
		<div>
        <h4 class="marginbottom20">你有衣櫃爆炸的煩惱嗎?想賣卻不知該如何下手嗎?</h4>        
        <p>
         感謝各位的支持，目前GetMore網站上，約有2,000款二手衣可供喜歡時尚的朋友選購，也感謝優質賣家，利用GetMore的賣衣服功能，出售精選的好衣，讓好衣服再次被愛。<br />
         由於韓風與無品牌服飾在比例上已達飽和，10/20~12/31期間，GetMore將舉辦品牌限定召集的企劃，此時段代售將限定在以下所列品牌上。<br />
         趕快動手整理，簡單填寫商品訊息，分享時尚品味增加收入。
        </p>
    	</div>
        <hr>
        <span class="fresh-title" id='type2'>
        Q&A 
      	</span>
      					<div class="panel-group margintop20" id="accordion">
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q001">
						        Q1：何時可以開始申請代售？
						        </a>
						      </h4>
						    </div>
						    <div id="q001" class="panel-collapse collapse">
						      <div class="panel-body">
								A1：10/20正式開始。
						      </div>
						    </div>
						  </div>

				  		  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q002">
						         Q2：真的不能拿不合所列品牌以外的衣服來代售嗎？
						        </a>
						      </h4>
						    </div>
						    <div id="q002" class="panel-collapse collapse">
						      <div class="panel-body">
						        A2：很抱歉！品牌召集活動期間若不符合品牌或是原有規定將會收取運費退回喔！
						      </div>
						    </div>
						  </div>
						<div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q003">
						          Q3：品牌計畫會影響目前上架商品嗎？
						        </a>
						      </h4>
						    </div>
						    <div id="q003" class="panel-collapse collapse">
						      <div class="panel-body">
						          A3：由於目前日韓服飾上架比例已飽和，品牌計畫其中一個目的是讓更多歐美品牌進駐，以平衡商品種類，吸引更多購買，並不會影響目前或是已上架的商品，反而，因為同性質的商品競爭較少，且引入品牌商品作為吸引，會更加強現有商品的成交。
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q004">
						         Q4：多久會上架？
						        </a>
						      </h4>
						    </div>
						    <div id="q004" class="panel-collapse collapse">
						      <div class="panel-body">
								 A4：到貨期間約14個工作天內完成上架，若非正常程序則會延遲，其中會經過洗衣，驗收，拍照，上架等工作，其實還蠻多事要做的，就請您耐心等候囉！
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q005">
						        Q5：我有品牌要建議，該如何建議？
						        </a>
						      </h4>
						    </div>
						    <div id="q005" class="panel-collapse collapse">
						      <div class="panel-body">
								A5：歡迎在右下角留言，經過工作人員評估，若適合我們會加上去喔！（恕不一一回覆喔！）
						      </div>
						    </div>
						  </div>
						</div>
    </div>


    <div class="fresh marginbottom20">
      <span class="fresh-title" id='type2'>
        品牌列表 
      </span>
      輸入想要賣的品牌，看看是否在列表中。
    </div>
    <div class="overbanner">
    	<div class="overbannerbox">
	      <span id="bloodhound">
	 	  <input class="typeahead " type="text" placeholder="請輸入品牌">
	      </span>
	      <span class="checkbrands"id="validatedresult"></span>
      	</div>
  	</div>
    <table class="table borderless">
    	<tr>
    		<td><h4>國際名品</h4></td>
    		<td><h4>概念服裝</h4></td>
    		<td></td>
    		<td><h4>時尚成衣</h4></td>
    	</tr>
    	<tr>
    		<td>
    			<ul class="list-unstyled">
					<li>ANNA SUI</li>
					<li>Aquascutum</li>
					<li>BALENCIAGA</li>
					<li>BURBERRY</li>
					<li>CELINE</li>
					<li>CHANEL</li>
					<li>Chloé</li>
					<li>DAKS LONDON</li>
					<li>Dior</li>
					<li>DOLCE & GABBANA</li>
					<li>FENDI</li>
					<li>GIVENCHY</li>
					<li>GUCCI</li>
					<li>ISSEY MIYAKE</li>
					<li>Jean Paul GAULTIER</li>
					<li>JIL SANDER</li>
					<li>LANVIN</li>
					<li>LOUIS VUITTON</li>
					<li>MARC JACOBS</li>
					<li>MICHAEL KORS</li>
					<li>miu miu</li>
					<li>Moncler</li>
					<li>PAUL & JOE</li>
					<li>Paul Smith</li>
					<li>PRADA</li>
					<li>SAINT LAURENT</li>
					<li>SHIATZY CHEN 夏姿</li>
					<li>SONIA RYKIEL</li>
					<li>TOD'S</li>
					<li>VERSACE</li>
				</ul>
			</td>
			<td>
    			<ul class="list-unstyled">
					<li>3.1 Phillip Lim</li>
					<li>45rpm</li>
					<li>ACNE STUDIOS</li>
					<li>agnes`b</li>
					<li>ALEXANDER WANG</li>
					<li>ALICE+OLIVIA</li>
					<li>ARMANI</li>
					<li>CARVEN</li>
					<li>COMME CA DU MODE</li>
					<li>Cotélac</li>
					<li>Cynthia Rowley</li>
					<li>DVF</li>
					<li>Ed Hardy</li>
					<li>EMILIO PUCCI</li>
					<li>HERVÉ LÉGER</li>
					<li>HIROKO KOSHINO</li>
					<li>IRO</li>
					<li>ISABEL MARANT</li>
					<li>JASON WU</li>
					<li>JILL STUART</li>
					<li>JOYRICH</li>
					<li>JUICY COUTURE</li>
					<li>KENZO</li>
					<li>M MISSONI</li>
					<li>Maison Martin Margiela</li>
					<li>MAJE</li>
					<li>MARC BY MARC JACOBS</li>
					<li>MARNI</li>
				</ul>
			</td>
			<td>    		
    			<ul class="list-unstyled">
					<li>MAX&CO.</li>
					<li>MaxMara</li>
					<li>Milida</li>
					<li>MOSCHINO</li>
					<li>MSGM</li>
					<li>PAUL & JOE SISTER</li>
					<li>PLEATS PLEASE</li>
					<li>REBECCA TAYLOR</li>
					<li>Romance Was Born</li>
					<li>See by Chloé</li>
					<li>SitDownPlz</li>
					<li>SONIA BY SONIA RYKIEL</li>
					<li>SPORT b.</li>
					<li>Stella McCartney</li>
					<li>TIBI</li>
					<li>TORY BURCH</li>
					<li>TSUMORI CHISATO</li>
					<li>vanessabruno</li>
					<li>Vivienne Westwood</li>
					<li>VIKTOR&ROLF</li>
					<li>Y-3</li>
					<li>上海灘</li>
					<li>陳季敏</li>
					<li>黃淑琦</li>
					<li>溫慶珠</li>
					<li>葉珈伶</li>
					<li>竇騰璜張李玉菁</li>
				</ul>
			</td>
			<td>
    			<ul class="list-unstyled">
					<li>ALLSAINTS</li>
					<li>ASOS</li>
					<li>BANANA REPUBLIC</li>
					<li>BCBGMAXAZRIA</li>
					<li>Brooks Brothers</li>
					<li>Calvin Klein</li>
					<li>CLUB MONACO</li>
					<li>cop.copine</li>
					<li>DKNY</li>
					<li>EQ:IQ</li>
					<li>FCUK French Connection</li>
					<li>FORNARINA</li>
					<li>Fred Perry</li>
					<li>GAP</li>
					<li>GUESS</li>
					<li>iBLUES</li>
					<li>J.CREW</li>
					<li>JOSEPH</li>
					<li>KOOKAI</li>
					<li>MANGO</li>
					<li>MARELLA</li>
					<li>Massimo Dutti</li>
					<li>Mexx</li>
					<li>NARA CAMICIEE</li>
					<li>PENNY BLACK</li>
					<li>POLO RALPH LAUREN</li>
					<li>Roots</li>
					<li>Roxy</li>
					<li>SISLEY</li>
					<li>Superdry</li>
					<li>Tommy Hilfiger</li>
					<li>TOPSHOPS</li>
					<li>ZARA</li>
				</ul>
			</td>
    	</tr>
    	<tr>
    		<td><h4>流行女裝</h4></td>
    		<td><h4>潮流服飾</h4></td>
    		<td><h4>牛仔丹寧</h4></td>
    		<td><h4></h4></td>
    	</tr>
    	<tr>
			<td>
    			<ul class="list-unstyled">
					<li>22 OCTOBRE</li>
					<li>23區</li>
					<li>ANAYI</li>
					<li>BOSCH</li>
					<li>CLEAR IMPRESSION</li>
					<li>ECCO</li>
					<li>GINKOO</li>
					<li>giordano/ladies</li>
					<li>GRACE CONTINENTAL</li>
					<li>I.C.B</li>
					<li>IF</li>
					<li>INDEX</li>
					<li>INDIVI</li>
					<li>iROO</li>
					<li>JESSICA</li>
					<li>JILL BY JILLSTUART</li>
					<li>LAGUNAMOON</li>
					<li>LAUTREAMONT</li>
					<li>LE SOUK</li>
					<li>LIP SERVICE</li>
					<li>MAJESTIC LEGON</li>
					<li>MK</li>
					<li>Natural Beauty Basic</li>
					<li>PROPORTION BODY DRESSING</li>
					<li>QUEENS COURT</li>
					<li>UNTITLED</li>
					<li>U're</li>
					<li>VICKY</li>
					<li>自由區</li>
				</ul>
			</td>
    		<td>
    			<ul class="list-unstyled">
					<li>2%</li>
					<li>8 seconds</li>
					<li>87MM</li>
					<li>a la sha</li>
					<li>Abercrombie & Fitch</li>
					<li>American Eagle Outfitters</li>
					<li>Arnold Palmer</li>
					<li>bread n butter</li>
					<li>earth music & ecology</li>
					<li>EMSEXCITE</li>
					<li>GALOOP</li>
					<li>GOZO</li>
					<li>Heather</li>
					<li>HER&HIM</li>
					<li>HOLLISTER</li>
					<li>iiMK</li>
					<li>izzue</li>
					<li>JEANASIS</li>
					<li>Knights Bridge</li>
					<li>Lily Brown</li>
					<li>LOWRYS FARM</li>
					<li>Maru.a</li>
					<li>Noue-Rou</li>
					<li>OLIVE des OLIVE</li>
					<li>one way</li>
					<li>PS company</li>
					<li>RETRO GIRL</li>
					<li>ROUGH</li>
					<li>Scolar</li>
					<li>SCOTTISH HOUSE</li>
					<li>snidel</li>
					<li>STYLENANDA</li>
					<li>TOUGH</li>
					<li>T-PARTS</li>
					<li>V_K</li>
					<li>VERO MODA</li>
					<li>ココディール(COCO DEAL)</li>
					<li>ココルル(COCOLULU)</li>
				</ul>
			</td>
			 <td>
    			<ul class="list-unstyled">
					<li>7 FOR ALL MANKIND</li>
					<li>AG Adriano Goldschmied</li>
					<li>Current/Elliott</li>
					<li>Diesel</li>
					<li>EDWIN</li>
					<li>Et Boîte</li>
					<li>J BRAND</li>
					<li>LEE</li>
					<li>LEVI'S</li>
					<li>MISS SIXTY</li>
					<li>PAIGE</li>
					<li>Pepe Jeans</li>
					<li>R13</li>
					<li>rag & bone</li>
					<li>Replay</li>
					<li>Siwy</li>
					<li>TRUE RELIGION</li>
				</ul>
			</td>
		</tr>	
		
    </table>
    <script type="text/javascript" src="/assets/vendor/typeahead.js/dist/typeahead.bundle.min.js"></script>
    <script type="text/javascript">
    	(function(){
    		var origin_brands = [];

	    	$.each($('.table.borderless li'), function(index, li){
		  origin_brands.push($(li).text())
		});

	    	var brands = new Bloodhound({
		  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
		  queryTokenizer: Bloodhound.tokenizers.whitespace,
		  local: $.map(origin_brands, function(brand) { return { name: brand }; })
		});
		
		brands.initialize();
		 
		$('#bloodhound .typeahead').typeahead({
			hint: true,
		  	highlight: true,
		  	minLength: 1
		},{
			name: 'brands',
		  	displayKey: 'name',
		  	source: brands.ttAdapter()
		});

	    	$('#bloodhound .typeahead').bind('blur keypress', function(e){
	    		if(e.type === 'keypress' && e.keyCode !== 13) return;
	    		if(origin_brands.indexOf($(this).val()) >= 0){

	    			$("#validatedresult").html('Ｏ');
	    		}
	    		else {
	    			$("#validatedresult").html('Ｘ');
	    		}
	    	});
    	}());
    </script>
                <center>
                	<br>
                	<a href="/info/startsell" class="btn btn-primary">
                    下一步，代售注意事項
                	</a><br>
                	<p>極富設計感且材質良好的獨立設計師品牌也歡迎利用右下「聯絡我們」提出建議</p>
              	</center>
</div>

<style>
.overbanner{
	width: 980px;
}
.overbannerbox{
	margin: 10px 0px 20px 0px;
	width: 980px;
}
.checkbrands{
	font-size: 40px !important;
	color: red;
	margin-left: 30px;
}
span.twitter-typeahead {
	display: inline !important;
}
</style>