<script src="/assets/javascript/bootstrap.js"></script>


				<img class='banner-s' src="/assets/images/banner-s/about/intro.jpg">
				<div class='row' id='pagerow'>
					<div class='col-254'>
							<div class='leftcol'>
								<p>關於 / About</p>
								<p>
		  							<a class='nav-a' href="#GetMore" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">關於GetMore</button></a>
								</p>
								<p>
		  							<a class='nav-a' href="#team" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">GetMore Team</button></a>
								</p>
								<p>
		  							<a href="/info/joinus"><button type="button" class="btn btn-primary btn-m">加入我們</button></a>
								</p>
								<p>
		  							<a class='nav-a' href="#vender" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">合作夥伴</button></a>
								</p>
								<p>
		  							<a class='nav-a' href="#news" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">媒體報導</button></a>
								</p>
							</div>
						<hr>
						<div class='contactus'><p> Contact us <br> service@getmore.cc </p> </div>
					</div>
					
					<div class='col-726'>
					  <div class="tab-content article-line">	
						<div class="tab-pane active" id="GetMore">				
						<div class='rightcol fontp15'>
							<strong>
							<h2>關於GetMore</h2></br>
							<p>GetMore成立於2011年，總部位於台北的大安區。「GetMore二次時尚」是一個為時尚生活打造的二手服飾買賣網站，2013年年底上線，我們提供了最簡單、有趣的方式，讓人們享受二手衣時尚的生活！</p>
						    </strong>
							<br>
							<p>GetMore二次時尚提供簡單且全面的線上及線下服務。</p>
							<p>GetMore，以三折以下完美價格，可以買到品牌或是設計師服飾，且每一項商品都是保證清洗整燙的。或是透過GetMore的代售流程，線上填單，付洗衣費，將九成新或是全新卻穿不到的衣服交給GetMore，輕鬆利用衣櫃賺取收入。</p>
							<br>
							<center>
								<iframe width="640" height="360" src="//www.youtube.com/embed/OGBxLxGuQ3c?rel=0" frameborder="0" allowfullscreen></iframe>
						    </center>

							<br><h4>從台灣開始，打造顛覆傳統服飾工業的二手衣買賣服務！</h4><br>
							<p>GetMore二次時尚的創意來源，來自創辦人温博凱，因為女友有衣櫃塞爆的問題，許多品牌衣服甚至連吊牌都沒拆，認為可以有更好的方式來管理衣服，因而有創立二次時尚的想法。</p>
							<p>在瞭解歐美時尚圈引領的二手衣或古著衣風潮後，決定付諸行動。</p>
							<p>GetMore 成立於 2011年9月，總部位於台北大安區巷弄內的老公寓一樓，在2013年，獲得台北市政府地方產業創新研發推動計畫（SBIR）的輔導企業。</p><br>
							<ul>
								<li>2011年9月成立</li>
								<li>100年度教育部U-START計畫專案</li>
								<li>進駐交通大學創新育成中心</li>
								<li>進駐政大創立方</li>
								<li>2012年10月，我們的工作室，你的咖啡館，getmore lab 開幕</li>
								<li>102年度台北市SBIR創新研發計劃</li>
								<li>2013年12月，GetMore Storage成立，專業二手衣倉櫥空間</li>
								<li>2013年12月底，亞洲第一個二次時尚購物網站兼二手衣代售服務-GetMore，正式上線</li>
								<li>2014年1月，推出「超級VIP計畫·衣管家到你家」服務，迴響熱烈</li>
								<li>2014年2月，入圍數位時代第5屆綠色品牌</li>
							</ul>
						</div>
					  </div>	

					
					  <div class="tab-pane" id="team">			
						<div class='rightcol fontp15'>
							<strong><h2>GetMore Team</h2></br>
							<p>GetMore團隊源自於台灣。</p>
							<p>總部座落在大安森林公園附近，我們把這裡取名為”getmore lab”。我們喜歡在工作的時候，有好咖啡、甜點的陪伴，我們愛這樣的美好事物，它們使我們激發出更多好的點子。 "GetMore二次時尚"就是這樣開始的，它是一個創新的想法，卻也貼近我們的生活，為我們生活中遇到的問題提供解法和價值。</p>
							<br><img class='banner-s' src="/assets/images/content/about/team.png">
							</strong>
							<h3>GetMore 的工作室，你的咖啡館</h3>
							<p>厭倦了OA隔間，GetMore 的團隊愛好咖啡也愛甜點，就決定把工作的空間設計成一個咖啡店的樣子。
							取名為 getmore lab，那裡有很好的單品咖啡以及手工甜點，
							在 “Coffee Hours” 的時間裡，開放找個位子坐，點杯咖啡，吃個甜點。</p>
							<p>歡迎來玩！(站立免費，入座請點一杯飲料)</p>
							<p>Coffee Hours：</br>
							10:00 AM~ 7:00 PM</br>
							Tue.公休</p>
							<a href="http://goo.gl/maps/sUxzb" target="_blank"><img class='banner-s' src="/assets/images/content/about/map.jpg"></a>
						</div>
					  </div>

					   <div class="tab-pane" id="vender">				
						 <div class='rightcol fontp15'>
							<strong>
							<h2>合作夥伴</h2></br>
							</strong>
							<h4 ><font color='#52AF7E'>洗衣公司</font></h4></br>
							<p>「臻穎股份有限公司」為國內最專業的專業精緻洗衣工廠，是全台唯一政府認可環保洗衣工廠。</p>
							<p>他們同時也是全家便利商店「潔衣家」的合作夥伴。</p>
							<p>GetMore 二次時尚的每一件服飾，皆經過專業清洗、高溫殺菌、整燙等過程。</p></br>
							<img class='banner-s' src="/assets/images/content/about/tww.jpg">
							<h4><font color='#52AF7E'>物流配送</font></h4></br>
							<p>不論是送件、送洗、或是購物，皆由國內最專業的黑貓宅急便進行配送。</p></br>
							<img class='banner-s' src="/assets/images/content/about/t-cat.jpg">
						  </div>
						</div>
					  <div class="tab-pane" id="news">
						<div class='rightcol fontp15'>
						<h2>新聞室</h2></br>
						<p>Hi，各位媒體朋友，</p></br>

						<p>對我們的網站有興趣嗎？想要了解更多關於GetMore二次時尚嗎? </p>
						<p>請參考我們的新聞稿，或直接與我們聯繫。</p></br>
						<p><a href="mailto:service@getmore.cc" target='_blank'>service@getmore.cc</a></br>
						02 23259881</p></br>
						<hr>
						
						<h4>媒體報導</h4><br>
						<img src="/assets/images/info/report.jpg">
						<p><a href="http://www.bnext.com.tw/article/view/id/31468" target="_blank">● [Meet創業之星] GetMore二次時尚－二手衣代售服務，讓無用衣物發揮新價值！</a></p>
						<p><a href="http://www.bnext.com.tw/article/view/id/31842" target="_blank">● 從生活中找尋創業新點子</a></p>
						<p><a href="http://www.chinatimes.com/realtimenews/20140327004155-260402" target="_blank">● 自身經驗萌想法 年輕人創業</a></p>
						<hr>
						<h4>新聞稿</h4><br>
						<p>● 網路二手衣代售服務：亞洲第一個二次時尚購物網站，正式上線</p></br>
						<p>GetMore 是亞洲第一個「二次時尚」購物網站，並且提供雲端二手衣代售服務。</p></br>
						<p>鎖定「衣櫃塞爆」的時尚族群，利用GetMore二次時尚的雲端系統，簡單輸入衣服資料，線上支付洗衣費後，
						會有物流人員上門收件，之後送洗、拍照、丈量、上架、包裝出貨的事情，網站業者通通幫你搞定! 
						GetMore二次時尚同時也是新型態網路服飾商店，上千件品牌二手衣，平均折扣2.5折起，
						標榜每一件衣服都是經過嚴格清洗，且獨一無二，只有一件。</p><br>
						
						<p>● 創造時尚新文化運動：二次時尚</p></br>
						<p>面對當今服飾產業的潮流「快速時尚」，GetMore反其道而行，提出新概念「二次時尚」。</p></br>
						<p>二次時尚所表達的含義，不只是二手衣的時尚，而是時尚的再次創造、再次定義、再次思考，以及重視質感、
							環保、獨特的精神。相對於「快速時尚」追求流行、快速丟棄所造成的浪費，
							GetMore二次時尚相信服飾的價值是可以再被創造的，提倡環保、注重品質、忠於自我的時尚態度。
							二次時尚GetMore想傳達的是，穿二手衣是一種時尚、一種生活態度。</p><br>

						<p>● 延續歐美「分享經濟」概念，讓好衣服有再次被愛的機會</p></br>
						<p>近年來，歐美開始興起「分享經濟」，這是一個足以改變我們熟悉的各種經濟生活的革命。GetMore二次時尚將這樣的經濟概
						   念運用在現在的服飾產業，服飾某部分的來源出自於大家的衣櫃，這些我們平時不常穿的好衣，可以以合理的價格分享出去，
						   讓好衣服有再次被愛的機會。</p></br>

						<p>● 品質嚴格把關，一律送洗最安心</p></br>
						<p> GetMore二次時尚想打造的是一個環保、獨特、注重服飾質感的時尚文化，因此對於二手衣的代售條件也相對嚴謹許多。
							例如：GetMore只收全新或九成新的衣服、不能有瑕疵、建議大家拿有品牌或是高質感的衣服等等。最特別的是，
							在GetMore的每件衣服，一律會送到專業的洗衣公司送洗，因為即使是全新的衣服，也要經過清洗才能保證安全。
							GetMore透過這樣的方式，維持一貫的品質與保證，讓買賣家都能安心。</p></br>

						<img src="/assets/images/content/news/nt-04.jpg"></br>
						</div>
					  </div>	
				    </div>
				  </div>
				</div>
				 	 <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fgetmore.cc&amp;width=980&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true&amp;appId=207910605914306" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:980px; height:258px;" allowTransparency="true"></iframe>

<script>
    $(document).ready(function(){
        var hash = window.location.hash;
        hash && $('.nav-a[href="' + hash + '"]').tab('show');

        $('.nav-a').click(function (e) {
          $(this).tab('show');
          var scrollmem = $('body').scrollTop();
          window.location.hash = this.hash;
          $('html,body').scrollTop(scrollmem);
          
        });      
    });
</script>
