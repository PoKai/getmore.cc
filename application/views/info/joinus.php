<script src="/assets/javascript/bootstrap.js"></script>

				<img class='banner-s' src="/assets/images/banner-s/news.jpg">
				<div class='row' id='pagerow'>
					<div class='rightcol padding0'>
		



						<center>
						<h2>加入GetMore</h2></br>
						<p>一同改變時尚產業吧！/ Join us and change the fashion industry</p>
						<br>
						<p>GetMore成立於2011年，總部位於台北的大安區。<br>「GetMore二次時尚」是一個為時尚生活打造的二手服飾買賣網站，<br>我們提供了最簡單、有趣的方式，讓人們享受二手衣時尚的生活！
						</p>
						<br>
						<p>「年輕的時候加入新創公司有很多好處，你可以擁有自己的舞台、擔起重大責任、<br>影響產品與公司走向、更全面理解一家企業運作、跟著公司一起成長，<br>甚至有機會因為併購、上市而得到人生的第一桶金。」</p>
						<br>
						<div class="width600">
						</div>
						</center>
						<hr>

					<div class='col-254'>	
						<div class='leftcol'>
						<p>職缺 / Jobs</p>
						<p>
  							<a class='nav-a' href="#engineer" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">軟體工程師</button></a>
						</p>
						<p>
  							<a class='nav-a' href="#marketing" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">行銷專員</button></a>
						</p>
						<p>
  							<a class='nav-a' href="#designer" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">視覺設計師</button></a>
						</p>
						<p>
  							<a class='nav-a' href="#community" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">社群文字編輯</button></a>
						</p>
						<p>
  							<a class='nav-a' href="#operation" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">營運專員</button></a>
						</p>
						<p>
		  					<a href="/info/about"><button type="button" class="btn btn-success btn-m">關於GetMore</button></a>
						</p>
						</div>
						<hr>
						<div class='contactus'><p> Contact us <br> service@getmore.cc </p> </div>
					</div>
				
					<div class='col-726'>
						<div class="tab-content article-line">
							<div class="tab-pane" id="marketing">
							<div class="floatright">
							<div class="fb-like" data-href="http://getmore.cc/info/joinus#marketing" data-width="300" data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div>
							</div>
							<h3>行銷專員 / Marketing</h3><br>
							<p>GetMore是一個快速成長的新創公司，歡迎熱血積極的社群行銷人才加入，和公司一起成長。</p>
							<p>我們的目標是從台灣出發，一起改變時尚產業！</p>
							<p>如果你擅於商業操作或是廣告操作，可以依季節、話題準確企劃出有效的服飾行銷計劃，有創意、熱情，對商業數字有概念，認同GetMore二次時尚的理念，對於服裝產業很有興趣，積極樂觀，歡迎加入我們。</p>
							<p>行銷的工作表現，將反映在一些科學的數據上，包含會員數、瀏覽量、轉換率、ROI、粉絲數、回應數、觀看數、分享數。我們會一起hack出讓更多人使用GetMore服務的方法。</p>


							<p>工作內容:</p>
							<ol>
							<li>依照目標提出行銷企劃</li>
							<li>執行行銷計畫、分析結果</li>
							<li>實體活動規劃與執行</li>
							<li>文案內容撰寫</li>
							<li>廣告內容產生以及後台操作</li>
						    </ol>

							<p>必備條件:</p>		
							<ol>
							<li>對網路廣告工具以及媒體熟悉</li>
							<li>擅於文字編輯，有網路內容經營的經驗</li>
							<li>追蹤分析網站數據的能力</li>
							<li>樂於溝通，活潑樂觀</li>
							<li>對於新創公司或創新商業模式有興趣</li>
							</ol><br>

							<p>符合上述條件，快與我們聯繫。</p><br>

							<p>其他條件</p>
							<p>符合以下條件，大大加分。</p>
							<ol>
							<li>認識很多服裝品牌，瞭解國際流行趨勢，注重穿衣的品質</li>
							<li>對簡單美工或是攝影略懂</li>
                            </ol>

							<br>
							<p>GetMore 是亞洲第一個「二次時尚」購物網站，並提供網路二手衣代售服務。</p>

							<p>我們正在尋找一位社群行銷專員，若妳（你）具備上述條件，請儘速與我們聯絡。</p>
							<br>
							<p>公司福利:</p>
							<ol>
								<li>免費的飲料，時時補充的零食</li>
								<li>我們有自己經營的精品咖啡店，每周一可以領一杯頂級的好咖啡</li>
								<li>達到目標時，每半年會有激勵獎金</li>
								<li>我們離大安森林公園超級近，公司提供淋浴設備，下班跑步超健康</li>
							</ol>
							<br>
							<p>數位時代報導：</p>
							</p><a href="http://www.bnext.com.tw/article/view/id/31468">http://www.bnext.com.tw/article/view/id/31468</a></p><br>
							<p>Apply Information</p>
							<p>請將履歷Email至 <a mailto="pokai@getmore.cc">pokai@getmore.cc</a></p>				
							</div>

							<div class="tab-pane active" id="engineer">
							<div class="floatright">
							<div class="fb-like" data-href="http://getmore.cc/info/joinus#engineer" data-width="300" data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div>
							</div>
							<h3>軟體工程師 / Soft Engineer</h3><br>
							<p>GetMore是一個快速成長的新創公司，歡迎PHP程式有相關經驗的軟體工程師加入，和公司一起成長。</p>

							<p>工作內容:</p>
							
							<ol>
							<li>使用php按照原本網站核心進行延續開發，使用Linux作業系統</li>
							<li>使用git版本控制</li>
							<li>使用coffeescript，backbone</li>
							<li>前端工程略懂</li>
							<li>javascript略懂</li>
							<li>使用MVC架構</li>
							</ol>

							<p>必備條件:</p>		
							<ol>
							<li>有web開發經驗,可獨立完成工作</li>
							<li>良好溝通能力</li>
							<li>喜歡利用程式解決問題</li>
							<li>學習能力強</li>
							<li>對於新創公司或創新商業模式有興趣</li>
							</ol><br>

							<p>GetMore 是亞洲第一個「二次時尚」購物網站，並提供網路二手衣代售服務。</p><br>
							<p>公司福利:</p>
							<ol>
								<li>免費的飲料，時時補充的零食</li>
								<li>我們有自己經營的精品咖啡店，每周一可以領一杯頂級的好咖啡</li>
								<li>達到目標時，每半年會有激勵獎金</li>
								<li>我們離大安森林公園超級近，公司提供淋浴設備，下班跑步超健康</li>
							</ol>
							<br>
							<p>數位時代報導：</p>
							</p><a href="http://www.bnext.com.tw/article/view/id/31468" target="_blank">http://www.bnext.com.tw/article/view/id/31468</a></p>
							<p>若你具備上述條件，請儘速與我們聯絡。</p><br>
							
							<p>Apply Information</p>
							<p>請將履歷Email至 <a mailto="pokai@getmore.cc">pokai@getmore.cc</a></p>
							</div>

							<div class="tab-pane" id="designer">
							<div class="floatright">
							<div class="fb-like" data-href="http://getmore.cc/info/joinus#designer" data-width="300" data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div>
							</div>
							<h3>視覺設計師 / Designer</h3><br>
							<p>GetMore是亞洲第一個二手衣時尚買賣網站，是正在快速成長的新創公司，歡迎對善於運用影像的視覺設計師加入，和公司一起成長。</p>
							<p>需要的技能會是攝影、平面，以及源源不絕的創意。</p>
							<p></p>
							</p>工作內容:</p>
							<ol>
							<li>網站UI設計</li>
							<li>平面設計及廣告編排設計</li>
							<li>攝影及修圖</li>
							<li>產品上架</li>
							</ol>

							<p>必備條件:</p>
							<ol>
							<li>平面設計、攝影、網站排版</li>
							<li>樂於溝通，主動樂觀</li>
							<li>對於新創公司或創新商業模式有興趣</li>
							</ol>

							<p>加分條件:</p>
							<ol>
							<li>強大攝影能力，有作品集</li>
							<li>有排版及網站經驗</li>
							<li>對於國際精品以及高級時裝品牌有所認識</li>
							</ol><br>

							<p>符合上述條件，快與我們聯繫。</p><br>

							<p>GetMore 是亞洲第一個「二次時尚」購物網站，並提供網路二手衣代售服務。</p><br>
							<p>公司福利:</p>
							<ol>
								<li>免費的飲料，時時補充的零食</li>
								<li>我們有自己經營的精品咖啡店，每周一可以領一杯頂級的好咖啡</li>
								<li>達到目標時，每半年會有激勵獎金</li>
								<li>我們離大安森林公園超級近，公司提供淋浴設備，下班跑步超健康</li>
							</ol>
							<br>
							<p>數位時代報導：</p>
							</p><a href="http://www.bnext.com.tw/article/view/id/31468" target="_blank">http://www.bnext.com.tw/article/view/id/31468</a></p>
							<p>若你具備上述條件，請儘速與我們聯絡。</p>
							<p>Apply Information</p>
							<p>請將履歷Email至 <a mailto="pokai@getmore.cc">pokai@getmore.cc</a>，隨信附上Blog網址或經營的粉絲頁網址。</p>
							</div>

							<div class="tab-pane" id="operation">
							<div class="floatright">
							<div class="fb-like" data-href="http://getmore.cc/info/joinus#operation" data-width="300" data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div>
							</div>
							<h3>營運專員 / Operation Manager</h3><br>
							<p>GetMore是亞洲第一個二手衣時尚買賣網站，是正在快速成長的新創公司。</p>
							<p>對二手服飾有熱情且細心靈活的營運夥伴歡迎加入，工作內容會使用我們建置的專業上架系統完成二手衣拍照、上架以及客戶聯繫的工作，讓好衣服能再次被愛!</p>
							<p></p>
							</p>工作內容:</p>
							<ol>
							<li>貨單檢查</li>
							<li>二手衣攝影丈量上架</li>
							<li>為衣服寫上點評</li>
							<li>客戶聯繫及客服</li>
							<li>出貨包裝</li>
							</ol>

							<p>必備條件:</p>
							<ol>
							<li>對時尚有敏銳度</li>
							<li>做事敏捷靈活</li>
							<li>細心可獨立完成工作</li>	
							<li>具服飾業經驗或服裝相關科系</li>
							<li>對網路熟悉</li>
							<li>樂於溝通，主動樂觀</li>
							</ol><br>

							<p>符合上述條件，快與我們聯繫。</p><br>

							<p>GetMore 是亞洲第一個「二次時尚」購物網站，並提供網路二手衣代售服務。</p><br>
							<p>公司福利:</p>
							<ol>
								<li>免費的飲料，時時補充的零食</li>
								<li>我們有自己經營的精品咖啡店，每周一可以領一杯頂級的好咖啡</li>
								<li>達到目標時，每半年會有激勵獎金</li>
								<li>我們離大安森林公園超級近，公司提供淋浴設備，下班跑步超健康</li>
							</ol>
							<br>
							<p>數位時代報導：</p>
							</p><a href="http://www.bnext.com.tw/article/view/id/31468" target="_blank">http://www.bnext.com.tw/article/view/id/31468</a></p>
							<p>若你具備上述條件，請儘速與我們聯絡。</p>
							<p>Apply Information</p>
							<p>請將履歷Email至 <a mailto="pokai@getmore.cc">pokai@getmore.cc</a>，隨信附上Blog網址或經營的粉絲頁網址。</p>
							</div>

							<div class="tab-pane" id="community">
							<div class="floatright">
							<div class="fb-like" data-href="http://getmore.cc/info/joinus#pt-community" data-width="300" data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div>
							</div>
							<h3>社群文字編輯 / Community Manager</h3><br>
							<p>GetMore是亞洲第一個二手衣時尚買賣網站，是正在快速成長的新創公司。</p>
							<p>若你有強大的文字才華，總是可以寫出吸引人的內容，有源源不絕的創意，可以精確接收與表達品牌精神，喜歡在網路上與人交流，這是一個絕佳的職位，歡迎加入。</p>
							<p></p>
							</p>工作內容:</p>
							<ol>
							<li>粉絲團操作</li>
							<li>時尚相關文章編輯</li>
							<li>部落格編輯</li>
							<li>廣告文案發想</li>
							</ol>

							<p>必備條件:</p>
							<ol>
							<li>擅於文字編輯，有網路內容經營的經驗</li>
							<li>對於國際精品以及高級時裝有所認識</li>
							<li>能與同事溝通配合、主動積極</li>
							<li>樂於溝通，主動樂觀</li>
							<li>對於新創公司或創新商業模式有興趣</li>
							</ol><br>

							<p>符合上述條件，快與我們聯繫。</p><br>

							<p>GetMore 是亞洲第一個「二次時尚」購物網站，並提供網路二手衣代售服務。</p><br>
							<p>公司福利:</p>
							<ol>
								<li>免費的飲料，時時補充的零食</li>
								<li>我們有自己經營的精品咖啡店，每周一可以領一杯頂級的好咖啡</li>
								<li>達到目標時，每半年會有激勵獎金</li>
								<li>我們離大安森林公園超級近，公司提供淋浴設備，下班跑步超健康</li>
							</ol>
							<br>
							<p>數位時代報導：</p>
							</p><a href="http://www.bnext.com.tw/article/view/id/31468" target="_blank">http://www.bnext.com.tw/article/view/id/31468</a></p>
							<p>若你具備上述條件，請儘速與我們聯絡。</p>
							<p>Apply Information</p>
							<p>請將履歷Email至 <a mailto="pokai@getmore.cc">pokai@getmore.cc</a>，隨信附上Blog網址或經營的粉絲頁網址。</p>
							</div>

						</div>
					</div>
				</div>
				</div>
				<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fgetmore.cc&amp;width=980&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true&amp;appId=207910605914306" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:980px; height:258px;" allowTransparency="true"></iframe>

<script>
    $(document).ready(function(){
        var hash = window.location.hash;
        hash && $('.nav-a[href="' + hash + '"]').tab('show');
        
        $('.nav-a').click(function (e) {
          $(this).tab('show');
          var scrollmem = $('body').scrollTop();
          window.location.hash = this.hash;
          $('html,body').scrollTop(scrollmem);
          
        });      
    });
</script>	


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&appId=239678122794926&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>