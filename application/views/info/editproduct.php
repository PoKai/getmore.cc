<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<style>
.editpimg {
  width: 30px;
  margin: 5px;
}
.col-254 {
    padding-right: 10px;
    padding-top:65px;"
}
.col-726{
    padding-right: 0;
}
.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
  vertical-align: middle;
  font-size: 10px;
}
.edit-icon{
    vertical-align: bottom;
    margin-left: 3px;
}
a{
    color: #687E84;
}
.list-group-item.active, .list-group-item.active:hover, .list-group-item.active:focus{
  background-color: #006699;
  border-color: #006699;
}

.upload-direction{
  color:#565a5c;
  letter-spacing: 3px;
  padding: 10px 20px; 

}

.upload-direction p{
  font-size: 14px;
  line-height: 20px;
}

.hellpseller{
 padding: 20px; 
}

.planabox{
    padding:30px 20px;
    margin-bottom: 14px;
    min-height: 500px;
    background-color: #f7f7f9;
    border: 1px solid #e1e1e8;
    border-radius: 4px;
}

.plananext{
    position:absolute;
    bottom:0px;
    right:0px;
}

.btn-plananext{
    min-width: 150px;
}

.planaex{
    font-size: 10px;
    color: #bbb;
    margin: 0 0 10px;
}

.textarea{
    resize: vertical;
}

.planabtn{
    margin: 20px 0;
    text-align: center;
}

.upload-direction{
  color:#565a5c;
  letter-spacing: 3px;
  padding: 10px 20px; 

}

.selectcolor{
    margin-bottom: 20px;
}

.selectstyle{
    margin-bottom: 20px;
}

.form-control{
    font-size: 10px;
    letter-spacing: 2px;
}

.form-control[disabled], fieldset[disabled] .form-control {
    cursor: not-allowed;
}

.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: #eee;
    opacity: 1;
}

.show-title{
    text-decoration: underline;
    color: #565a5c;
    font-size: 14px;
    margin-bottom: 10px;
}

.show-content{
    color: #000;
    font-size: 12px;
    margin-bottom: 10px;
}

.show-img img{
    width: 120px;
    margin-bottom: 10px;
}

.show-img-s img{
    width: 80px;
    margin-bottom: 10px;
}



</style>

                    <!-- modals -->
                <div class="modal fade" id='edit-style'>
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">編輯顏色以及風格</h4>
                            </div>
                            <div class="modal-body">
                          <div class='upload-direction'>
                                <h3>選擇顏色以及風格</h3>
                                <p>選擇這件衣服相近的顏色，以及1~3項風格，買家可以更快找到想要的商品，也可以加入GetMore的行銷活動．</p>
                            </div>
                            <div class="form-group  upload-direction">
                                <div ng-init="r_color = black" class="selectcolor">
                                    <label for="color"><u>顏色</u><sup>*必填</sup></label>
                                    <div>
                                        <label>
                                            <input type="radio" value="black" name="color" ng-model="r_color" class="ng-pristine ng-untouched ng-valid">
                                            <img src="/admin/assets/images/colorbox/black.png">
                                        </label>
                                        <label>
                                            <input type="radio" value="white" name="color" ng-model="r_color" class="ng-pristine ng-untouched ng-valid">
                                            <img src="/admin/assets/images/colorbox/white.png">
                                        </label>
                                        <label>
                                            <input type="radio" value="gray" name="color" ng-model="r_color" class="ng-pristine ng-untouched ng-valid">
                                            <img src="/admin/assets/images/colorbox/gray.png">
                                        </label>
                                        <label>
                                            <input type="radio" value="red" name="color" ng-model="r_color" class="ng-pristine ng-untouched ng-valid">
                                            <img src="/admin/assets/images/colorbox/red.png">
                                        </label>

                                        <label>
                                            <input type="radio" value="green" name="color" ng-model="r_color" class="ng-pristine ng-untouched ng-valid">
                                            <img src="/admin/assets/images/colorbox/green.png">
                                        </label>
                                        <label>
                                            <input type="radio" value="brown" name="color" ng-model="r_color" class="ng-pristine ng-untouched ng-valid">
                                            <img src="/admin/assets/images/colorbox/brown.png">
                                        </label>
                                        <label>
                                            <input type="radio" value="blue" name="color" ng-model="r_color" class="ng-pristine ng-untouched ng-valid">
                                            <img src="/admin/assets/images/colorbox/blue.png">
                                        </label>
                                        <label>
                                            <input type="radio" value="orange" name="color" ng-model="r_color" class="ng-pristine ng-untouched ng-valid">
                                            <img src="/admin/assets/images/colorbox/orange.png">
                                        </label>
                                
                                        <label>
                                            <input type="radio" value="purple" name="color" ng-model="r_color" class="ng-pristine ng-untouched ng-valid">
                                            <img src="/admin/assets/images/colorbox/purple.png">
                                        </label>
                                        <label>
                                            <input type="radio" value="pink" name="color" ng-model="r_color" class="ng-pristine ng-untouched ng-valid">
                                            <img src="/admin/assets/images/colorbox/pink.png">
                                        </label>
                                        <label>
                                            <input type="radio" value="yellow" name="color" ng-model="r_color" class="ng-pristine ng-untouched ng-valid">
                                            <img src="/admin/assets/images/colorbox/yellow.png">
                                        </label>
                                        <label>
                                            <input type="radio" value="color" name="color" ng-model="r_color" class="ng-pristine ng-untouched ng-valid">
                                            <img src="/admin/assets/images/colorbox/color.png">
                                        </label>
                                    </div>
                                </div>
                                <div class="selectstyle">
                                    <label for="style"><u>風格</u></label>
                                    <div>
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="1" class="ng-pristine ng-untouched ng-valid">上班族
                                        </label>
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="2" class="ng-pristine ng-untouched ng-valid">復古
                                        </label>
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="3" class="ng-pristine ng-untouched ng-valid">浪漫
                                        </label>
                                        
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="4" class="ng-pristine ng-untouched ng-valid">小碎花
                                        </label>
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="5" class="ng-pristine ng-untouched ng-valid">幾何
                                        </label>
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="6" class="ng-pristine ng-untouched ng-valid">格子
                                        </label>
                                        
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="7" class="ng-pristine ng-untouched ng-valid">條紋
                                        </label>
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="8" class="ng-pristine ng-untouched ng-valid">正式場合
                                        </label>
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="9" class="ng-pristine ng-untouched ng-valid">牛仔
                                        </label>
                                        
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="10" class="ng-pristine ng-untouched ng-valid">運動風
                                        </label>
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="11" class="ng-pristine ng-untouched ng-valid">休閒
                                        </label>
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="12" class="ng-pristine ng-untouched ng-valid">學院
                                        </label>
                                        
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="13" class="ng-pristine ng-untouched ng-valid">可愛
                                        </label>
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="14" class="ng-pristine ng-untouched ng-valid">奢華
                                        </label>
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="15" class="ng-pristine ng-untouched ng-valid">宴會
                                        </label>
                                        
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="16" class="ng-pristine ng-untouched ng-valid">中國風
                                        </label>
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="17" class="ng-pristine ng-untouched ng-valid">動物紋
                                        </label>
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="18" class="ng-pristine ng-untouched ng-valid">印花
                                        </label>
                                        
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="19" class="ng-pristine ng-untouched ng-valid">個性
                                        </label>
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="20" class="ng-pristine ng-untouched ng-valid">民俗風
                                        </label>
                                        <label>
                                            <input type="checkbox" name="style[]" ng-model="r_style[]" ngtruevalue="21" class="ng-pristine ng-untouched ng-valid">性感
                                        </label>
                                    </div>
                                </div>
                                <div class="filltag">
                                    <label for="style"><u>自訂標籤</u></label>
                                        <input type="text" class="form-control" placeholder="">
                                    <span id="helpBlock" class="help-block" style="font-size:10px;">填寫自訂的標籤，請使用空白區隔多個標籤。</span>   
                                    <p>
                                        <span id="helpBlock" class="help-block" style="font-size:10px;">或從以下選取之前設定過的：</span>
                                      <a href="#" class="btn btn-primary btn-xs" style="border-radius:10px;">搖滾</a>
                                      <a href="#" class="btn btn-primary btn-xs" style="border-radius:10px;">神秘</a>
                                    </p> 
                                </div>
                            </div>
                        </div>   
                              <div class="planabtn">
                                  <a type="submit" class="btn btn-info" data-target="#carousel-example-generic">確認更改</a>
                              </div>
                            </div>
                            <div class="modal-footer">
                                <p class='announcement text-center'>已成為「會員」或是點擊「用facebook 登入」按鈕的同時,<br>表示您已詳讀並同意GetMore相關的<a href="/info/buy#buypolicy" target='_blank'>購買條款</a>以及<a href="/info/sell#sellpolicy" target='_blank'>代洗代售服務條款</a>。</p>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->


                    <!-- modals -->
                <div class="modal fade" id='edit-size'>
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">編輯尺寸</h4>
                            </div>
                            <div class="modal-body">
                            <div class="upload-direction">
                                <h3>填寫尺寸</h3>
                                <p>利用GetMore的系統，並依照您的尺寸標籤標示尺寸。不明瞭？來看看<a>影片說明</a>。</p>
                            </div>
                            <div class="form-group upload-direction">
                                <label for="size_type_major">先選單位</label>
                                <select class="form-control width375 ng-pristine ng-untouched ng-valid" name="size_type_major" id="size_type_major" ng-model="size_type_major">
                                    <option value="US">US</option>
                                    <option value="UK">UK</option>
                                    <option value="EU">EU</option>
                                    <option value="IT">IT</option>
                                    <option value="JP">JP</option>
                                    <option value="腰圍">腰圍</option>
                                    <option value="其他">其他</option>
                                </select>
                            </div>

                            <div class="form-group upload-direction">
                                <label  for="size">再選SIZE</label>
                                <select class="form-control width375 ng-pristine ng-untouched ng-valid" name="size_type_major" id="size_type_major" ng-model="size_type_major" disabled>
                                    <option value="US">US</option>
                                    <option value="UK">UK</option>
                                    <option value="EU">EU</option>
                                    <option value="IT">IT</option>
                                    <option value="JP">JP</option>
                                    <option value="腰圍">腰圍</option>
                                    <option value="其他">其他</option>
                                </select>
                            </div>
                            <div class="form-group upload-direction">
                                <label for="size_comment">其他尺寸敘述</label>
                                <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="size_comment" maxlength="20" ng-model="size_comment" name="size_comment" placeholder="加上更多尺寸註解">
                                <div class="planaex">例如：很寬鬆適合各式尺寸</div>
                            </div>
                            <div class="form-group upload-direction">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="measure_table">
                                    <tbody>
                                        <tr>
                                            <td rowspan="10" align="right">
                                                <img ng-src="[[ category2|measure_img:r_long:r_pattern ]]" id="measure_img" style="height: auto;">
                                            </td>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure1" name="measure[1]" ng-model="measure[1]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure2" name="measure[2]" ng-model="measure[2]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure3" name="measure[3]" ng-model="measure[3]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure4" name="measure[4]" ng-model="measure[4]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure5" name="measure[5]" ng-model="measure[5]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure6" name="measure[6]" ng-model="measure[6]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure7" name="measure[7]" ng-model="measure[7]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure8" name="measure[8]" ng-model="measure[8]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure9" name="measure[9]" ng-model="measure[9]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 30px;">
                                                <div class="input-group">
                                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="measure10" name="measure[10]" ng-model="measure[10]">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="planabtn">
                                <a class="btn btn-success" role="button" data-target="#carousel-example-generic" data-slide-to="2">上一步</a>
                                <a class="btn btn-info" role="button" data-target="#carousel-example-generic" data-slide-to="4">下一步，填寫販售資訊</a>
                            </div>
                              <div class="planabtn">
                                  <a type="submit" class="btn btn-info" data-target="#carousel-example-generic">確認更改</a>
                              </div>
                            </div>
                            <div class="modal-footer">
                                <p class='announcement text-center'>已成為「會員」或是點擊「用facebook 登入」按鈕的同時,<br>表示您已詳讀並同意GetMore相關的<a href="/info/buy#buypolicy" target='_blank'>購買條款</a>以及<a href="/info/sell#sellpolicy" target='_blank'>代洗代售服務條款</a>。</p>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                    <!-- modals -->
                <div class="modal fade" id='edit-price'>
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">編輯價格</h4>
                            </div>
                            <div class="modal-body">
      
                            <div class="upload-direction">
                                <h3>設定價格</h3>
                                <p>原始價格以及定價，建議您設定的價格是原價的2折，請勿超過4折，如何定價？<a href="#">看影片就對了！</a>。</p>
                            </div>
                               
                                <div class="form-group upload-direction">
                                    <label for="product_original_price"><u>原始價格</u></label>
                                    <select class="form-control width375" id="product_original_price" name="product_original_price">
                                        <option value="1000">500元~1,000元</option><option value="2000">1,001元~2,000元</option><option value="3000">2,001元~3,000元</option><option value="4000">3,001元~4,000元</option><option value="5000">4,001元~5,000元</option><option value="6000">5,001元~6,000元</option><option value="7000">6,001元~7,000元</option><option value="8000">7,001元~8,000元</option><option value="9000">8,001元~9,000元</option><option value="10000">9,001元~10,000元</option><option value="15000">10,001元~15,000元</option><option value="20000">15,001元~20,000元</option><option value="25000">20,001元~25,000元</option><option value="30000">25,001元~30,000元</option><option value="35000">30,001元~35,000元</option><option value="40000">35,001元~40,000元</option><option value="45000">40,001元~45,000元</option><option value="50000">45,001元~50,000元</option><option value="60000">50,001元~60,000元</option><option value="70000">60,001元~70,000元</option><option value="80000">70,001元~80,000元</option><option value="90000">80,001元~90,000元</option><option value="100000">90,001元~100,000元</option><option value="999999">超過100,000元</option>
                                    </select>
                                    <p>建議價商品定價為：<font class="red">$800</font> </p>
                                </div>
                                <div class="form-group upload-direction">
                                    <label for="product_sell_price"><u>定價</u></label>
                                    <div class="input-group" style="width:120px;">
                                        <div class="input-group-addon">$</div>
                                        <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="product_sell_price" maxlength="20" ng-model="product_sell_price" name="product_sell_price">
                                    </div>
                                    <div class="planaex">建議是原價的2折，並包含平均運費。</div>
                                </div>
                              <div class="planabtn">
                                  <a type="submit" class="btn btn-info" data-target="#carousel-example-generic">確認更改</a>
                              </div>
                            </div>
                            <div class="modal-footer">
                                <p class='announcement text-center'>已成為「會員」或是點擊「用facebook 登入」按鈕的同時,<br>表示您已詳讀並同意GetMore相關的<a href="/info/buy#buypolicy" target='_blank'>購買條款</a>以及<a href="/info/sell#sellpolicy" target='_blank'>代洗代售服務條款</a>。</p>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                    <!-- modals -->
                <div class="modal fade" id='edit-detail'>
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">編輯商品資訊</h4>
                            </div>
                            <div class="modal-body">
      
                            <div class='upload-direction'>
                                <h3>填寫商品資訊</h3>
                                <p>填寫顯示在商品頁面的商品名稱,品牌及敘述。沒有idea嗎？看<a href="#">教學影片</a>。</p>
                            </div>
                            
                                <div class="form-group upload-direction">
                                    <label for="product_title"><u>商品名稱</u><sup>*必填</sup></label>
                                    <p>幫商品取一個好的名字，也讓使用者能方便搜尋到。</p>
                                    <input type="text" class="form-control ng-pristine ng-untouched ng-valid" id="product_title" maxlength="20" ng-model="product_title" name="product_title" placeholder="填入名稱">
                                    <div class="planaex">例如：AJ 深藍棉質背心</div>
                                </div>
                                <div class="form-group upload-direction">
                                    <label for="product_brand"><u>填寫品牌</u><sup>*必填</sup></label>
                                    <p>系統會自動帶出資料庫內品牌，若顯示請直接點選，若沒有請自行輸入。</p>
                                    <input type="text" class="form-control ui-autocomplete-input ng-pristine ng-untouched ng-valid" id="product_brand" name="product_brand" maxlength="30" autocomplete="off" ng-model="product_brand" placeholder="商品品牌">
                                    <div class="planaex">輸入正確品牌，並注意您的<a>法律責任</a>(拒絕盜版)。</div>
                                </div>
                                <div class="form-group upload-direction">
                                    <label for="condition"><u>商品狀況</u><sup>*必填</sup></label>
                                    <p>請仔細檢查商品狀況，商品狀況，若有瑕疵請於商品描述中填寫。不明瞭？來看看<a>影片說明</a>。</p>
                                    <select class="form-control width375 ng-pristine ng-untouched ng-valid" name="size_type_major" id="size_type_major" ng-model="size_type_major">
                                        <option value="US">US</option>
                                        <option value="UK">UK</option>
                                        <option value="EU">EU</option>
                                        <option value="IT">IT</option>
                                        <option value="JP">JP</option>
                                        <option value="腰圍">腰圍</option>
                                        <option value="其他">其他</option>
                                    </select>
                                </div>
                                <div class="form-group upload-direction">
                                    <label for="product_comments"><u>商品敘述</u><sup>*必填</sup></label>
                                    <textarea class="form-control ng-pristine ng-untouched ng-valid" id="product_comments" name="product_comments" ng-model="product_comments" rows="5" placeholder="請詳細描述這衣服的特徵及穿起來的感受等等，請勿在此留下您的聯絡資訊或是私下交易訊息，會被取消賣家資格喔！"></textarea>
                                    <div class="planaex ng-binding">字數限制100字，還剩 [[ remain_count ]] 字。</div>
                                </div>
                              <div class="planabtn">
                                  <a type="submit" class="btn btn-info" data-target="#carousel-example-generic">確認更改</a>
                              </div>

                            </div>
                            <div class="modal-footer">
                                <p class='announcement text-center'>已成為「會員」或是點擊「用facebook 登入」按鈕的同時,<br>表示您已詳讀並同意GetMore相關的<a href="/info/buy#buypolicy" target='_blank'>購買條款</a>以及<a href="/info/sell#sellpolicy" target='_blank'>代洗代售服務條款</a>。</p>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <!-- modals -->
                <div class="modal fade" id='edit-img'>
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">修改圖片</h4>
                            </div>
                            <div class="modal-body">
                              <div class="fb_login">
                                  <h4>您可以新增刪除圖片，或直接拖曳更換順序。</h4>
                              </div>
                              <div id="getmore_login">
                                <div class="show-img-s text-center">
                                    <img src="http://getmore.cc/upload/thumbs/250_375_4cc2b099423968084a2035c57000a8ef.JPG">
                                    <img src="http://getmore.cc/upload/thumbs/250_375_4cc2b099423968084a2035c57000a8ef.JPG">
                                    <img src="http://getmore.cc/upload/thumbs/250_375_4cc2b099423968084a2035c57000a8ef.JPG">
                                    <br><a class="btn btn-default btn-xs" ng-click="addFile($event)">新增檔案</a>
                                </div>
                              </div>
                              <div class="planabtn">
                                  <a type="submit" class="btn btn-info" data-target="#carousel-example-generic">確認更改</a>
                              </div>
                            </div>
                            <div class="modal-footer">
                                <p class='announcement text-center'>已成為「會員」或是點擊「用facebook 登入」按鈕的同時,<br>表示您已詳讀並同意GetMore相關的<a href="/info/buy#buypolicy" target='_blank'>購買條款</a>以及<a href="/info/sell#sellpolicy" target='_blank'>代洗代售服務條款</a>。</p>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                    <!-- modals -->
                <div class="modal fade" id='testmodal'>
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">註冊會員</h4>
                            </div>
                            <div class="modal-body">
                                <div class="fb_login">
                                    <h4>GetMore 是亞洲第一個二手衣時尚購物網站</h4>
                                    <a class='fb_login_btn btn btn-primary' href="<?php echo site_url('/auth/facebook?redirect_url=' . urlencode(current_url()))?>">用facebook登入</a> 快使用facebook快速登入!
                                    <hr class="dashed_hr"/>
                                </div>
                                <div id="getmore_login">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <h4>註冊會員</h4> 
                                        </div>
                                        <div class="col-md-8 pull-right announcement getmore_instruction">
                                            還不是GetMore的會員嗎？ 免費加入！
                                        </div>
                                    </div>
                                    <form method="POST" action="/auth/register" name="dig_register">
                                        <div class="form-group">
                                            <label for="sign_up_email">信箱</label><div class='col-md-5 pull-right description'>此為您的會員登入帳號</div>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="email">
                                        </div>
                                        <div class="form-group">
                                            <label for="nickname">暱稱</label> <div class='col-md-5 pull-right description'>14字元以內</div>
                                            <input type="text" class="form-control" id="user_name" name="user_name" placeholder="暱稱">
                                        </div>
                                        <div class="form-group">
                                            <label for="new_password">密碼</label>
                                            <input type="password" class="form-control" id="new_password" name="password" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <label for="password_confirm">確認密碼</label>
                                            <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm Password">
                                        </div>
                                        <div class="form-group">
                                            <input type="checkbox" value="1" name="aggree" style="width:20px;">我已閱讀，並同意<a href="/info/buy#buypolicy">GetMore 服務條款</a>
                                        </div>
                                        <a id='start_to_getmore' href="javascript:document.forms['dig_register'].submit();" class='btn btn-primary'>開始GetMore</a>
                                        <div id='registration' class="col-md-4 pull-right">
                                            已是會員？ <a id='logIn' href="#logInModal" data-toggle="modal" onClick='$("#signUpModal").modal("hide");'>由此登入！</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <p class='announcement text-center'>已成為「會員」或是點擊「用facebook 登入」按鈕的同時,<br>表示您已詳讀並同意GetMore相關的<a href="/info/buy#buypolicy" target='_blank'>購買條款</a>以及<a href="/info/sell#sellpolicy" target='_blank'>代洗代售服務條款</a>。</p>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->


<div class="col-254">
<div class="list-group">
  <a href="#" class="list-group-item active">
    上架二手衣
  </a>
  <a href="#" class="list-group-item">修改已上架資訊</a>
  <a href="#" class="list-group-item">設定交易政策</a>
  <a href="#" class="list-group-item">宣傳我的衣櫃</a>
  <a href="#" class="list-group-item">加入賣家的社群</a>
  <a href="#" class="list-group-item">賣家規範</a>
  <a href="#" class="list-group-item">賣家Q&A</a>
</div>
    <hr>
    <div class="contactus">
        <p> Contact us
            <br> service@getmore.cc </p>
    </div>
</div>

<div class="col-726">
    <div class="member_order_list_title">
      <i class="fa fa-pencil-square-o fa-3x"></i>
      <span>編輯已上架商品</span>
    </div>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>圖片</th>
          <th>詳細資訊</th>
          <th>價格</th>
          <th>尺寸</th>
          <th>風格</th>
          <th>其他</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row"><a data-toggle="modal" href="#edit-img"><img class="editpimg" src="http://getmore.cc/upload/thumbs/800_1200_978d1b82ec9f2856d70c476c8e774434.JPG"></a><a data-toggle="modal" href="#edit-img"><i class="fa fa-pencil lg edit-icon"></i></a></th>
          <td><a data-toggle="modal" href="#edit-detail">BLANCA 變形蟲圖騰洋裝</a><a href="#edit-detail"><i class="fa fa-pencil lg edit-icon"></i></a></td>
          <td><a data-toggle="modal" href="#edit-price">$1000</a><a data-toggle="modal" href="#edit-price"><i class="fa fa-pencil lg edit-icon"></i></a></td>
          <td><a data-toggle="modal" href="#edit-size">US 10</a><a data-toggle="modal" href="#edit-size"><i class="fa fa-pencil lg edit-icon"></i></a></td>
          <td><a data-toggle="modal" href="#edit-style">風格</a><a data-toggle="modal" href="#edit-style"><i class="fa fa-pencil lg edit-icon"></i></a></td>
          <td><a href="#">下架</a>｜<a href="#">下架</a>｜<a href="#">下架</a></td>
        </tr>
        <tr>
          <th scope="row"><a href="#"><img class="editpimg" src="http://getmore.cc/upload/thumbs/800_1200_978d1b82ec9f2856d70c476c8e774434.JPG"><i class="fa fa-pencil lg edit-icon"></i></a></th>
          <td><a href="#">BLANCA 變形蟲圖騰洋裝<i class="fa fa-pencil lg edit-icon"></i></a></td>
          <td><a href="#">$1000<i class="fa fa-pencil lg edit-icon"></i></a></td>
          <td><a href="#">US 10<i class="fa fa-pencil lg edit-icon"></i></a></td>
          <td><a href="#">US 10<i class="fa fa-pencil lg edit-icon"></i></a></td>
          <td><a href="#">US 10<i class="fa fa-pencil lg edit-icon"></i></a></td>
        </tr>
        <tr>
          <th scope="row"><a href="#"><img class="editpimg" src="http://getmore.cc/upload/thumbs/800_1200_978d1b82ec9f2856d70c476c8e774434.JPG"><i class="fa fa-pencil lg edit-icon"></i></a></th>
          <td><a href="#">BLANCA 變形蟲圖騰洋裝<i class="fa fa-pencil lg edit-icon"></i></a></td>
          <td><a href="#">$1000<i class="fa fa-pencil lg edit-icon"></i></a></td>
          <td><a href="#">US 10<i class="fa fa-pencil lg edit-icon"></i></a></td>
          <td><a href="#">US 10<i class="fa fa-pencil lg edit-icon"></i></a></td>
          <td><a href="#">US 10<i class="fa fa-pencil lg edit-icon"></i></a></td>
        </tr>
      </tbody>
    </table>
</div>
