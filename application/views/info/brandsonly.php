<div class='row rightcol marginleft40 marginright40'>
    <h3>品牌代售 / Brands only</h3>
    <center><img class='margintop50' src="/assets/images/info/process.png"></center><br>
                        <center>
                    <br>
                    <a href="/info/startsell" class="btn btn-primary">
                    下一步，代售注意事項
                    </a><br>
                    <p>極富設計感且材質良好的獨立設計師品牌也歡迎利用右下「聯絡我們」提出建議</p>
                </center>
                <hr />
    <div class="fresh marginbottom20">
      <span class="fresh-title" id='type2'>
        品牌列表 
      </span>
      輸入想要賣的品牌，看看是否在列表中。
    </div>
    <div class="overbanner">
        <div class="overbannerbox">
          <span id="bloodhound">
          <input class="typeahead " type="text" placeholder="請輸入品牌">
          </span>
          <span class="checkbrands"id="validatedresult"></span>
        </div>
    </div>
<div>
    <table class="table borderless">
        <tr>
            <td><h4>國際名品</h4></td>
            <td><h4>概念服裝</h4></td>
            <td><h4>時尚成衣</h4></td>
            <td><h4>潮流女裝</h4></td>
        </tr>
        <tr>
            <td>
                <ul class="list-unstyled">
                    <?= '<li>'.join('</li><li>',$brands[1]).'</li>'; ?>
                </ul>
            </td>
            <td>
                <ul class="list-unstyled">
                    <?= '<li>'.join('</li><li>',$brands[2]).'</li>'; ?>
                </ul>
            </td>
            <td>
                <ul class="list-unstyled">
                    <?= '<li>'.join('</li><li>',$brands[3]).'</li>'; ?>
                </ul>
            </td>
            <td>
                <ul class="list-unstyled">
                    <?= '<li>'.join('</li><li>',$brands[4]).'</li>'; ?>
                </ul>
            </td>
        </tr>
        <tr>
            <td><h4>潮流服飾</h4></td>
            <td><h4>牛仔丹寧</h4></td>
            <td><h4>Select Shop</h4></td>
        <td><h4>運動休閒</h4></td>
        </tr>
        <tr>
            <td>
                <ul class="list-unstyled">
                    <?= '<li>'.join('</li><li>',$brands[5]).'</li>'; ?>
                </ul>
            </td>
            <td>
                <ul class="list-unstyled">
                    <?= '<li>'.join('</li><li>',$brands[6]).'</li>'; ?>
                </ul>
            </td>
            <td>
                <ul class="list-unstyled">
                    <?= '<li>'.join('</li><li>',$brands[7]).'</li>'; ?>
                </ul>
            </td>
            <td>
                <ul class="list-unstyled">
                    <?= '<li>'.join('</li><li>',$brands[8]).'</li>'; ?>
                </ul>
            </td>
        </tr>
    </table>
</div>
    <script type="text/javascript" src="/assets/vendor/typeahead.js/dist/typeahead.bundle.min.js"></script>
    <script type="text/javascript">
        (function(){
            var origin_brands = [];
            var origin_brands_lowerCase = [];

            $.each($('.table.borderless li'), function(index, li){

          origin_brands.push($(li).text()),
          origin_brands_lowerCase.push($(li).text().toLowerCase())
        });

            var brands = new Bloodhound({
          datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          local: $.map(origin_brands, function(brand) { return { name: brand }; })
        });
        
        brands.initialize();
         
        $('#bloodhound .typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },{
            name: 'brands',
            displayKey: 'name',
            source: brands.ttAdapter()
        });

            $('#bloodhound .typeahead').bind('blur input keypress typeahead:selected', function(e){
                if(origin_brands_lowerCase.indexOf($(this).val().toLowerCase()) >= 0){
                    $("#validatedresult").html('Ｏ');
                }
                else {
                    $("#validatedresult").html('Ｘ');
                }
            });
        }());
    </script>
                <center>
                    <br>
                    <a href="/info/startsell" class="btn btn-primary">
                    下一步，代售注意事項
                    </a><br>
                    <p>極富設計感且材質良好的獨立設計師品牌也歡迎利用右下「聯絡我們」提出建議</p>
                </center>
</div>
<style>
.overbanner{
    width: 980px;
}
.overbannerbox{
    margin: 10px 0px 20px 0px;
    width: 980px;
}
.checkbrands{
    font-size: 40px !important;
    color: red;
    margin-left: 30px;
}
span.twitter-typeahead {
    display: inline !important;
}
</style>