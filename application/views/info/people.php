				<link rel="stylesheet" href="/assets/font-awesome-4.0.3/css/font-awesome.min.css">


				<div class='row' id='pagerow'>
					<img class='banner-s' src="/assets/images/banner-s/news.jpg">
					<p class="article-breadcrumb">
            		<a href="/">首頁</a>
               		&gt; 
                    <a href="/clothing/category/women">二次時尚誌</a>
                    &gt;
                    <a href="/clothing/category/women#subtype-13">人物專訪</a>
           			</p>
					<div class='article-content'>
						<h3>專訪 PoKai</h3>
						<div class='article-author'>
						<span class="article-date">
        				2014/05/20
    					</span>
    					<span class="author-name">
       					 | <a href="#"> GetMore 二次時尚編輯</a>
     					 </span>
     					</div>
						<img class='img-thumbnail'  src="/assets/images/people/IMG_12222.jpg">
						<p>
							頂著交通大學碩士學歷的溫博凱，放棄在竹科擔任工程師的工作，選擇創業，打造二手衣雲端買賣網站「GetMore二次時尚」。
							溫博凱說，創業緣起來自於看到女友的衣櫃被衣服塞爆，有些甚至連品牌標都尚未取下，他以自身經驗觀察到，許多藝人或時尚人士，因為工作或購物習慣，常有汰換衣服的需要，卻無便利管道。
							靈機一動的溫博凱，想到歐美流行已久的「分享經濟」，不僅有許多時尚界人士響應穿二手衣的生活、環保態度，二手衣還有一季季流傳下去的「慢時尚」特色。
							溫博凱打造的二手衣買賣網站，賣家為衣物標價後，只要上網登錄，就有專人來收取衣物，這些衣物經過送洗、上架後，宛如新衣，賣方只要等著賺錢。
						</p>
						<br><br>
						<div class="fb-comments" data-href="http://beta.getmore.cc/info/people" data-width="620" data-numposts="5" data-colorscheme="light"></div>
					</div>
					<div class='article-side'>
						<div class='article-side-title'>ABOUT</div>
						<p>多多，來自外星的厲害小狗。</p>
						<p><a href="#">瞧瞧他的衣櫃</a><br>
						<a href="#">看看他的慾望清單</a></p>
						<hr>
						<div class='article-side-title'>分享給朋友</div>
              <section class='article-social-funcs'>
                  <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo current_url() ?>" target='_blank' class="new-social-share-button facebook">
                      <i class="fa fa-facebook"></i>
                  </a>
                  <a href="https://twitter.com/intent/tweet?url=<?php echo current_url() ?>&amp;text=♥<?php echo $clothing['product_title'] ?>" target='_blank' class="new-social-share-button twitter">
                      <i class="fa fa-twitter"></i>
                  </a>
                  <a target='_blank' href="http://pinterest.com/pin/create/button/?media=<?php echo clothing_image_url(explode(',', $clothing['files'])[0], 'large') ?>&amp;url=<?php echo current_url() ?>&amp;description=♥<?php echo $clothing['product_title'] ?>" class="new-social-share-button pinterest">
                      <i class="fa fa-pinterest"></i>
                  </a>
                  <div class='fbiframe floatright'>
                  <iframe src="//www.facebook.com/plugins/like.php?href=<?php echo current_url() ?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21&amp;appId=239678122794926" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:70px; height:21px;" allowTransparency="true"></iframe>
                  </div>
              </section>	
              			<hr>
              			<div class='article-side-title'>
			              <a class="to_like" href="#" data-id="1136">收藏這篇文章</a>
			            </div>
						<hr>
						<div class='article-side-title'>其他文章</div>
						<ul>
							<li><a href='#'>Master的專訪</a></li>
							<li><a href='#'>多多專訪下集</a></li>
							<li><a href='#'>再見阿福，流浪的故事</a></li>
						</ul>
						<hr>
						<div class="fb-like-box" data-href="https://www.facebook.com/getmore.cc" data-width="300" data-height="450" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="true"></div>
					</div>
				</div>
<script>        
        $(document).ready(function(){
            $('#target').lunZhuan({
                itemClass: 'suggested-item',
                buttonClass: 'suggested-item-button'
            });
            
            $('.new-social-share-button').click(function(e){
                e.preventDefault();
                window.open($(this).attr('href'), 'just_new', config='height=600,width=600');
            });
            
        });
 </script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&appId=239678122794926&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<style>
        .go-left{
            position: absolute;
            top: 200px;
            margin-left: -70px;
        }
        .go-right{
            position: absolute;
            top: 200px;
            right: -70px;
        }
                      
        .new-social-share-button {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-transition: all .15s ease-in-out;
            -moz-transition: all .15s ease-in-out;
            transition: all .15s ease-in-out;
            display: inline-block;
            text-align: center;
            padding-top: 2.5px;
            width: 24px;
            height: 24px;
            font-size: 15px;
            border-radius: 99px;
            background: #f5f5f5;
            color: #333
        }

        .new-social-share-button.facebook:hover {
            background: #45629d
        }

        .new-social-share-button.twitter:hover {
            background: #55acee
        }

        .new-social-share-button.tumblr:hover {
            background: #529ecc
        }

        .new-social-share-button.pinterest:hover {
            background: #cb2027
        }

        .new-social-share-button:hover{
            color: white;
        }
        
        .item_status_comment{
            font-size: 12px;
            color: #bbbbbb;
        }
        
    </style>