<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" href="/assets/css/bootstrap.css"/>
		<link rel="stylesheet" href="/assets/css/launch_home.css"/>

<script type="text/javascript">
var fb_param = {};
fb_param.pixel_id = '6012069404096';
fb_param.value = '0.00';
fb_param.currency = 'TWD';
(function(){
  var fpw = document.createElement('script');
  fpw.async = true;
  fpw.src = '//connect.facebook.net/en_US/fp.js';
  var ref = document.getElementsByTagName('script')[0];
  ref.parentNode.insertBefore(fpw, ref);
})();
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6012069404096&amp;value=0&amp;currency=TWD" /></noscript>

  </head>
  <body>
    <div id='launch_main'>
      <div class='launchlogo'><img class='center' src='/assets/images/logo.png'/></div>
      <div class='joinus text-center white-color'>一起加入GetMore的行列!</div>
      <div class='slogan text-center white-color'>GetMore 是亞洲第一個二手衣時尚購物網站</div>
      <form class="pwloginform form-inline center" role="form" action="/launch/quick_register" method="POST">
        <div class="form-group">
          <input type="email" class="form-control form-opacity white-color" name="email"  placeholder="請輸入電子郵件">
        </div> &nbsp &nbsp
        <div class="form-group">
          <input type="password" class="form-control form-opacity white-color" name="password"  placeholder="請輸入密碼">
        </div>
        <input type="hidden" name="redirect_url" value="<?php echo urlencode(uri_string()); ?>" />
        <button type="submit" id="launch_register_btn" class="btn btn-default">確認</button>
        <div class="form-group">
          <p class='white-color btnnote'>搶先註冊,獲得最新消息!</p>
          <?php if(isset($reg_message) and !empty($reg_message)): ?>
            <div class='white-color'>
              <?php echo $reg_message; ?>
            </div>
          <?php endif; ?>  
        </div>
      </form>      

      
      <p class='text-center white-color fb_login'>or<a class='btn btn-primary fb_login_btn' href="<?php echo site_url('/auth/facebook?redirect_url=' . urlencode(current_url()))?>">用facebook登入</a>使用facebook快速登入!</p>
        <div class='launchvideo'>
          <iframe src="//player.vimeo.com/video/74014664?color=4bb1a1" width="480" height="270" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>        <br />
          <div class='pull-right buyonfb'>
          <a href="https://docs.google.com/a/getmore.cc/forms/d/1qiKfPrGstTgVRsrLzRM2t08S_di_NWPmdsZyiQaDBV4/viewform" target="_blank">→我要賣衣服</a>
          <a target='_blank' href='https://www.facebook.com/getmore.cc'><img src='/assets/images/launch/fb.png' /></a>
        </div>
        </div>
    </div>
  </body>
</html>
