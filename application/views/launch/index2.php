<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" href="/assets/css/bootstrap.css"/>
		<link rel="stylesheet" href="/assets/css/launch_home.css"/>
  </head>
  <body>
    <div id='launch_main'>
      <div class='launchlogo'><img class='center' src='/assets/images/logo.png'/></div>
      <div class='joinus text-center white-color'>恭喜加入GetMore!</div>
      <div class='slogan text-center white-color'>GetMore 是亞洲第一個二手衣時尚購物網站</div>

      <div id='bottom_half'>
        <a href="http://goo.gl/GBSzK6" target="_blank"><img src='/assets/images/launch/banner.png' /></a>
        <br />
        <div class='pull-right buyonfb'>
          <a href="https://docs.google.com/a/getmore.cc/forms/d/1qiKfPrGstTgVRsrLzRM2t08S_di_NWPmdsZyiQaDBV4/viewform" target="_blank">→我要賣衣服</a>
          <a target='_blank' href='https://www.facebook.com/getmore.cc'><img src='/assets/images/launch/fb.png' /></a>
        </div>
      </div>
    </div>
  </body>
</html>
