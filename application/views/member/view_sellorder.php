
            <div class='member_order_list_title'>
              <img src='/assets/images/little_clothing.png' />
              <span>二手衣申請進度查詢</span>
            </div>
        </br>

            <?php if ((in_array($complete_order['order']['payment_method'], ['atm']) && ($complete_order['order']['status_id']==1.5))): ?>
                <div class="alert alert-success">此訂單為ATM轉帳（未付款）！！訂單送出時間：<?php echo $complete_order['order']['date_purchased'] ?><br><br>


                    銀行：國泰世華銀行（銀行代號013）<br /><br />

                    分行：東門分行 <br /><br />

                    帳號：032-03-500587-0 <br /><br />

                    匯款金額 : <?php echo $complete_order['order']['final_price'] ?><br/><br/>

                    帳號後5碼 : <?php echo $complete_order['order']['last_five_number']; ?><br/><br/>

                    預計匯款時間 : <?php echo $complete_order['order']['estimated_remit_datetime']; ?><br/><br/>
                </div>
            <?php endif; ?>

            <?php if (in_array($complete_order['order']['status_id'], [2, 3])): ?>
                <div class="alert alert-success" style="padding:20px 20px 30px 20px;">
                    <h3>此張貨單付款成功！</h3><br />
                    <p>GetMore 會通知物流到您指定的地點取件，請將代售衣服用<u>適當大小</u>的包材裝成「1袋/箱」，並確實封口。您必須列印（或手寫）託運資訊貼在外箱，並保持手機暢通，宅急便物流士將直接與您聯繫。</p>
                    <br />
                    <div style="float:left;margin-right:8px;">
                      <i class="fa fa-hand-o-right fa-3x"></i>
                    </div>
                    <div style="float:left;">
                    <form action='/member/logistics_information' method='post'>
                        <input type='hidden' name='order_number' value='<?php echo $complete_order['order']['order_number'] ?>' />
                        <input type='submit' value='檢視托運資訊' class='btn btn-success btn-lg' />
                    </form>
                    </div>
                    <div style='clear: both;'></div>
                    <br />
                    <p style="font-size:10px;">※ 請注意，委託派件是透過 GetMore 通知黑貓宅急便至您府上取件，將由黑貓宅急便物流士直接以電話或是按電鈴通知您，您可直接將已經包裝好商品交給物流士，或是與物流士另外約定取貨時間。請務必保持手機暢通，在取件過程中，GetMore 無法追蹤到您與宅急便物流士聯繫狀況，若未接到物流士來電，或是無法完成取件的任何狀況，請來信 GetMore客服，我們將協助您完成收件。</p>
                </div>
            <?php endif; ?>
            
            
        
        
            <h5>申請日期：<?php echo $complete_order['order']['date_purchased'] ?></h5>
            <h5>申請單號：<?php echo $complete_order['order']['order_number'] ?></h5>
						<div id="info" class='col-md-8'>
							<div class="row">
								<div class="col-md-12">
									實際代售件數：<span id="total_num_value"><?php echo $success_num; ?></span> 
								</div>
								<!-- <div class="col-md-4" id='small_num'>
									<span id="small_num_value"><?php echo $small_num; ?></span> 小衣
								</div>
								<div class="col-md-4" id='big_num'>
									<span id="big_num_value"><?php echo $big_num; ?></span> 大衣
								</div>
								<div class="col-md-4" id='small_num'>
									<span id="fly_num_value"><?php echo $fly_num; ?></span> 羽絨衣
								</div> -->
							</div>
              <hr />
                  <span>
                  付款金額：<?php echo $complete_order['order']['total_price'] ?> (洗衣費) + 
                  <?php echo $complete_order['order']['transportation_charge'] ?> (運費)
                  <?php if ($complete_order['order']['deduction_amount'] != '0'): ?>
                      <?php echo $complete_order['order']['deduction_amount']?> (抵扣額)
                   <?php endif; ?> 
                  <?php if ($complete_order['order']['coupon_amount'] != '0'): ?>
                      <?php echo $complete_order['order']['coupon_amount']?> (洗衣卷)
                   <?php endif; ?> 
                   </span>
                   <span style='display: inline-block;'>
                   = <?php echo $complete_order['order']['final_price'] ?> 元
                    </span>
              
              
						</div>
            
            <div style='clear: both;'></div>

                <div class='order_other_information'>
                    <p>付款方式：<?php echo $complete_order['order']['payment_method'] ?></p>
                    <p>發票狀態：<?php echo $complete_order['order']['uniform_invoice_type_label'] ?></p>
                    <p>期滿送回地址：<?php echo $complete_order['order']['delivery_address'] ?></p>
                </div>
                              <?php if  ( ($complete_order['order']['status_id']==='1') && ($complete_order['order']['payment_method'] != 'atm') )  : ?>
                                  <form action='<?= payment_url(); ?>checkout/restart_for_pay?redirect=<?= urlencode(base_url()); ?>' method='post'>
                                      <input type='hidden' name='order_number' value='<?php echo $complete_order['order']['order_number'] ?>' />
                                      <center><input type='submit' value='立即付款' class='btn btn-warning' / style="margin-bottom:20px;width:300px;"></center>
                                  </form>
                              <?php endif; ?>             

<table class='table'>
    <tr>
        <th>商品描述</th>
        <th>服飾 / 類別</th>
        <th>品牌</th>
        <th>原價 / 二手售價</th>   
        <th>期滿</th>
        <th>備註</th>
        <th>狀態</th>
    </tr>
    <?php foreach ($complete_order['items'] as $item): ?>
    <tr <? if($item['remove']): ?> style="color: gray;" <? endif; ?>>
        <td><?php echo $item['product_title'] ?></td>
        <td><?php echo get_product_rough_type_label($item['product_type']) ?> / <?php echo get_product_type_label($item['product_type']) ?></td>
        <td><?php echo $item['product_brand'] ?></td>
        <td><?php echo get_price_label($item['product_original_price']) ?> / <?php echo $item['product_sell_price'] ?></td>   
        <td>
            <?php if ($item['donative']==1){
                            echo '捐贈';
                        }
                        else if ($item['donative']==0){
                            echo '送回';                          
                        }
            ?>
        </td>
        <td><?php echo $item['comments'] ?></td>
        <td <? if($item['remove']): ?> style="color: red;" <? endif; ?>>
            <?php if ($item['remove']==1){
                            echo '撤單('.reason_label($item['remove_reason']).')';
                        }
                        else if ($item['remove']==0){
                            echo get_sellorder_status_label($complete_order['order']['status_id']);                          
                        }
            ?>
        </td>
    </tr>    
    <?php endforeach; ?>
</table>



<style>
    .order_other_information{
          padding: 20px;
    }
</style>
