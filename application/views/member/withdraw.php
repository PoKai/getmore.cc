<div class='member_order_list_title'>
  <img src='/assets/images/my_account.png' />
  <span>匯出現金申請</span>
</div>
<br />
<div class='alert alert-info'>
請您填妥以下匯款資料。由於會計作業，將統一每月20日匯入您指定的銀行帳戶（若非上班日，則順延；匯款帳號後五碼為"05870"）
</div>
<div class='alert alert-danger'>
請留意：<br />匯出之金額會先扣除匯款手續費15元，再將餘額存入您的銀行帳戶（依照您提出的每筆申請匯款，不作併單）。
</div>
<form id='form_cash_output' class="form-horizontal" method='POST' action=''>

  <div class="form-group">
    <label class="col-sm-2 control-label">帳戶總額</label>
    <div class="col-sm-10">
        <p class="form-control-static"><?= $account['balance'] ?></p>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">欲匯出</label>
    <div class="col-sm-10">
      <input type="number" id='amount' class="form-control" name='amount'>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">入帳金額</label>
    <div class="col-sm-10">
      <input type="text" id='real_amount' class="form-control" disabled>
      <span class="help-block">欲匯出之金額-15元手續費</span>      
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" id='submit' class="btn btn-success" >確定</button>
      <a href='/member/account' class="btn btn-default">回上頁</a>
    </div>
  </div>
  <hr />
  <div class="form-group">
    <label class="col-sm-2 control-label">銀行名稱</label>
    <div class="col-sm-10">
        <select class="form-control" disabled>      
            <option><?= $bank_info['code'] ?> - <?= $bank_info['title'] ?></option>
        </select>
    </div>
 </div>


  <div class="form-group">
    <label class="col-sm-2 control-label">分行名稱</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" disabled value="<?= $bank_info['branch'] ?>">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label" >銀行帳號</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" disabled value="<?= $bank_info['account'] ?>">
    </div>
  </div>

  <p>＊匯款帳戶若需要更改，請Email 聯繫客服人員 <a href="mailto:service@getmore.c" target="_blank">service@getmore.cc</a>。</p>
</form>
<!-- modals -->
<div class="modal fade" id='enter'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">輸入您的銀行帳戶</h4>
            </div>
            <div class="modal-body" style="padding:30px 30px 0 30px;">
                <div style="margin:0 30px;">
                    <h3>輸入您的銀行帳戶，後續申請匯出更方便！</h3>
                    <div style="margin:20px 0; color:#717170">請提供與申請人相同戶名之匯款帳戶，並上傳存摺封面檔案，後續匯款將匯入您此次輸入的銀行帳戶。</div>
                 </div>
                 <hr />
                    <form class="panel-body" method="post" action="" enctype="multipart/form-data">
                        <div class="form-group form-horizontal" style="overflow: auto;">
                            <label for="bank_number" class="col-md-4 control-label">銀行名稱(代號)：</label>
                            <div class="col-md-8">
                                <select class="form-control" name="bank[code]" id="bank_number">
                                    <option value="0"> </option>
                                    <option value="004-臺灣銀行">004 - 臺灣銀行</option> 
                                    <option value="005-土地銀行">005 - 土地銀行</option> 
                                    <option value="006-合作商銀">006 - 合作商銀</option> 
                                    <option value="007-第一銀行">007 - 第一銀行</option> 
                                    <option value="008-華南銀行">008 - 華南銀行</option> 
                                    <option value="009-彰化銀行">009 - 彰化銀行</option> 
                                    <option value="011-上海商業儲蓄銀行">011 - 上海商業儲蓄銀行</option> 
                                    <option value="012-台北富邦銀行">012 - 台北富邦銀行</option> 
                                    <option value="013-國泰世華銀行">013 - 國泰世華銀行</option> 
                                    <option value="016-高雄銀行">016 - 高雄銀行</option> 
                                    <option value="017-兆豐國際商業銀行">017 - 兆豐國際商業銀行</option> 
                                    <option value="018-農業金庫">018 - 農業金庫</option> 
                                    <option value="021-花旗(台灣)商業銀行">021 - 花旗(台灣)商業銀行</option> 
                                    <option value="025-首都銀行">025 - 首都銀行</option> 
                                    <option value="039-澳商澳盛銀行">039 - 澳商澳盛銀行</option> 
                                    <option value="040-中華開發工業銀行">040 - 中華開發工業銀行</option> 
                                    <option value="050-臺灣企銀">050 - 臺灣企銀</option> 
                                    <option value="052-渣打國際商業銀行">052 - 渣打國際商業銀行</option> 
                                    <option value="053-台中商業銀行">053 - 台中商業銀行</option> 
                                    <option value="054-京城商業銀行">054 - 京城商業銀行</option> 
                                    <option value="072-德意志銀行">072 - 德意志銀行</option> 
                                    <option value="075-東亞銀行">075 - 東亞銀行</option> 
                                    <option value="081-匯豐(台灣)商業銀行">081 - 匯豐(台灣)商業銀行</option> 
                                    <option value="085-新加坡商新加坡華僑銀行">085 - 新加坡商新加坡華僑銀行</option> 
                                    <option value="101-大台北銀行">101 - 大台北銀行</option> 
                                    <option value="102-華泰銀行">102 - 華泰銀行</option> 
                                    <option value="103-臺灣新光商銀">103 - 臺灣新光商銀</option> 
                                    <option value="104-台北五信">104 - 台北五信</option> 
                                    <option value="106-台北九信">106 - 台北九信</option> 
                                    <option value="108-陽信商業銀行">108 - 陽信商業銀行</option> 
                                    <option value="114-基隆一信">114 - 基隆一信</option> 
                                    <option value="115-基隆二信">115 - 基隆二信</option> 
                                    <option value="118-板信商業銀行">118 - 板信商業銀行</option> 
                                    <option value="119-淡水一信">119 - 淡水一信</option> 
                                    <option value="120-淡水信合社">120 - 淡水信合社</option> 
                                    <option value="124-宜蘭信合社">124 - 宜蘭信合社</option> 
                                    <option value="127-桃園信合社">127 - 桃園信合社</option> 
                                    <option value="130-新竹一信">130 - 新竹一信</option> 
                                    <option value="132-新竹三信">132 - 新竹三信</option> 
                                    <option value="146-台中二信">146 - 台中二信</option> 
                                    <option value="147-三信商業銀行">147 - 三信商業銀行</option> 
                                    <option value="158-彰化一信">158 - 彰化一信</option> 
                                    <option value="161-彰化五信">161 - 彰化五信</option> 
                                    <option value="162-彰化六信">162 - 彰化六信</option> 
                                    <option value="163-彰化十信">163 - 彰化十信</option> 
                                    <option value="165-鹿港信合社">165 - 鹿港信合社</option> 
                                    <option value="178-嘉義三信">178 - 嘉義三信</option> 
                                    <option value="179-嘉義四信">179 - 嘉義四信</option> 
                                    <option value="188-台南三信">188 - 台南三信</option> 
                                    <option value="204-高雄三信">204 - 高雄三信</option> 
                                    <option value="215-花蓮一信">215 - 花蓮一信</option> 
                                    <option value="216-花蓮二信">216 - 花蓮二信</option> 
                                    <option value="222-澎湖一信">222 - 澎湖一信</option> 
                                    <option value="223-澎湖二信">223 - 澎湖二信</option> 
                                    <option value="224-金門信合社">224 - 金門信合社</option> 
                                    <option value="512-雲林區漁會">512 - 雲林區漁會</option> 
                                    <option value="515-嘉義區漁會">515 - 嘉義區漁會</option> 
                                    <option value="517-南市區漁會">517 - 南市區漁會</option> 
                                    <option value="518-南縣區漁會">518 - 南縣區漁會</option> 
                                    <option value="520-小港區漁會">520 - 小港區漁會</option> 
                                    <option value="520-高雄區漁會">520 - 高雄區漁會</option> 
                                    <option value="521-彌陀區漁會">521 - 彌陀區漁會</option> 
                                    <option value="521-永安區漁會">521 - 永安區漁會</option> 
                                    <option value="521-興達港區漁">521 - 興達港區漁會</option> 
                                    <option value="521-林園區漁會">521 - 林園區漁會</option> 
                                    <option value="523-東港漁會">523 - 東港漁會</option> 
                                    <option value="523-琉球區漁會">523 - 琉球區漁會</option> 
                                    <option value="523-林邊區漁會">523 - 林邊區漁會</option> 
                                    <option value="524-新港區漁會">524 - 新港區漁會</option> 
                                    <option value="525-澎湖區漁會">525 - 澎湖區漁會</option> 
                                    <option value="605-高雄市農會">605 - 高雄市農會</option> 
                                    <option value="612-豐原市農會">612 - 豐原市農會</option> 
                                    <option value="612-神岡鄉農會">612 - 神岡鄉農會</option> 
                                    <option value="613-名間農會">613 - 名間農會</option> 
                                    <option value="614-彰化地區農會">614 - 彰化地區農會</option> 
                                    <option value="616-雲林地區農會">616 - 雲林地區農會</option> 
                                    <option value="617-嘉義地區農會">617 - 嘉義地區農會</option> 
                                    <option value="618-台南地區農會">618 - 台南地區農會</option> 
                                    <option value="619-高雄地區農會">619 - 高雄地區農會</option> 
                                    <option value="620-屏東地區農會">620 - 屏東地區農會</option> 
                                    <option value="621-花蓮地區農會">621 - 花蓮地區農會</option> 
                                    <option value="622-台東地區農會">622 - 台東地區農會</option> 
                                    <option value="624-澎湖農會">624 - 澎湖農會</option> 
                                    <option value="625-台中市農會">625 - 台中市農會</option> 
                                    <option value="627-連江縣農會">627 - 連江縣農會</option> 
                                    <option value="700-中華郵政">700 - 中華郵政</option> 
                                    <option value="803-聯邦商業銀行">803 - 聯邦商業銀行</option> 
                                    <option value="805-遠東銀行">805 - 遠東銀行</option> 
                                    <option value="806-元大銀行">806 - 元大銀行</option> 
                                    <option value="807-永豐銀行">807 - 永豐銀行</option> 
                                    <option value="808-玉山銀行">808 - 玉山銀行</option> 
                                    <option value="809-萬泰銀行">809 - 萬泰銀行</option> 
                                    <option value="810-星展銀行">810 - 星展銀行</option> 
                                    <option value="812-台新銀行">812 - 台新銀行</option> 
                                    <option value="814-大眾銀行">814 - 大眾銀行</option> 
                                    <option value="815-日盛銀行">815 - 日盛銀行</option> 
                                    <option value="816-安泰銀行">816 - 安泰銀行</option> 
                                    <option value="822-中國信託">822 - 中國信託</option> 
                                    <option value="901-大里市農會">901 - 大里市農會</option> 
                                    <option value="903-汐止農會">903 - 汐止農會</option> 
                                    <option value="904-新莊農會">904 - 新莊農會</option> 
                                    <option value="910-財團法人農漁會聯合資訊中心">910 - 財團法人農漁會聯合資訊中心</option> 
                                    <option value="912-冬山農會">912 - 冬山農會</option> 
                                    <option value="916-草屯農會">916 - 草屯農會</option> 
                                    <option value="922-台南市農會">922 - 台南市農會</option> 
                                    <option value="928-板橋農會">928 - 板橋農會</option> 
                                    <option value="951-北農中心">951 - 北農中心</option> 
                                    <option value="954中南部地區農漁會-">954 - 中南部地區農漁會</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-horizontal" style="overflow: auto;">
                            <label for="bank_branch" class="col-md-4 control-label">分行名稱：</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="bank[branch]" id="bank_branch">
                            </div>
                        </div>
                        <div class="form-group form-horizontal" style="overflow: auto;">
                            <label for="bank_account" class="col-md-4 control-label">帳       號：</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control required digits" maxlength="18" name="bank[account]" id="bank_account">
                                <span class="help-block" style="color: grey;">請直接輸入數字部分，最多18位</span>
                            </div>
                        </div>
                        <div class="form-group form-horizontal" style="overflow: auto;">
                            <label for="bank_username" class="col-md-4 control-label">戶       名：</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="bank[username]" id="bank_username">
                            </div>
                        </div>
                        <div class="form-group form-horizontal" style="overflow: auto;">
                            <label for="bank_book_cover" class="col-md-4 control-label">存摺封面：</label>
                            <div class="col-md-8">
                                <input type="file" class="form-control" name="bank[bank_book_cover]" id="bank_book_cover">
                                <span class="help-block" style="color: grey;">請提供.pdf, .jpg或.png檔案</span>
                            </div>
                        </div>
                        <div class="text-center">
                            <a href="/member/account" class="btn btn-primary">下次再輸入</a>
                            <input type="submit" value="確認送出" class='btn btn-primary'>
                        </div>
                    </form>
            </div>
            <div class="modal-footer announcement" style="text-align:left;">               
                    <ul>
                        <li>為保障您的申請匯款安全，本項目一經開通無法更改，若需要更改請Email至客服。</li>
                        <li>匯款以您輸入的資料為主，影本為資料留存。</li>
                    </ul>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<script type="text/javascript">

$("#bank_account").validate({
  errorPlacement: function(error, element){
    $(element).parent('div').addClass('has-error');
  },
  success: function(label, element){
    $(element).parent('div').removeClass('has-error');
  }
});


    <? if(!$bank_info['title'] || !$bank_info['branch'] || !$bank_info['account'] || !$bank_info['code']): ?>
    $('#enter').modal({
        keyboard: false,
        backdrop: 'static'
    });

    $('form', '#enter')
        .delegate('#bank_account', 'keypress', function(e){
            var key_code = e.keyCode;
            if(key_code < 48 || key_code > 57){
                return false;
            }
            return true;
        })
        .bind('submit', function(e){
            try{
                _.each(e.target, function(input){
                    if(_.isEmpty($(input).val())){
                        alert('請填妥所有欄位');
                        throw 'break';
                    }
                });
                return true;
            } catch(err){
                e.preventDefault();
                return false;
            }
        });
    <? endif; ?>
    var account_balance = <?= $account['balance'] ?>

    $('#amount')
        .bind('keypress', function(e){
            var key_code = e.keyCode;
            if(key_code < 48 || key_code > 57){
                return false;
            }
            return true;
        })
        .bind('change', function(e){
            var value = $(this).val();
            
            if(_.isEmpty(value)){
                return $('#real_amount').val(value);
            }

            if(value > account_balance){
                $(this).val(account_balance);
            }

            value = $(this).val();

            if(value < 15){
                return $('#real_amount').val('');
            }
            $('#real_amount').val(value-15);
        });

    $('#form_cash_output').bind('submit', function(e){
        e.preventDefault();

        if(_.isEmpty($('#amount').val())){
            alert('請填入您想要匯出的金額，');
            return false;
        }
        else if($('#amount').val() <= 15){
            alert('很抱歉!!提出的金額要超過$15喔！');
            return false;
        }

        (function(){
            var deferrd = $.Deferred();

            $('<div class="alert alert-danger alert-dismissible fade in" role="alert" style="position: fixed; top: 50%; left: 50%; margin-top: -49.5px; margin-left: -153.1955px;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><p style="white-space: nowrap;">請再次確認您的帳號：<?= $bank_info["title"] ?> <?= $bank_info["branch"] ?> <?= $bank_info["account"] ?>.</p><p><button type="button" class="btn btn-danger yes">是</button><button type="button" class="btn btn-default no">否</button></p></div>')
                .delegate('button.yes', 'click', function(e){
                    $(e.target).parents('div.alert').alert('close');
                    deferrd.resolve();
                })
                .delegate('button.no', 'click', function(e){
                    $(e.target).parents('div.alert').alert('close');
                    deferrd.reject();
                }).appendTo($('#form_cash_output')).alert();

            return deferrd;
        }()).then(function(){
            var deferrd = $.Deferred();

            $('<div class="alert alert-danger alert-dismissible fade in" role="alert" style="position: fixed; top: 50%; left: 50%; margin-top: -48.5px; margin-left: -285.789px;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><p style="white-space: nowrap;">我們將扣除銀行匯款手續費15元後，將帳戶內的現金匯出給您，您確定要匯出帳戶？</p><p><button type="button" class="btn btn-danger yes">是</button><button type="button" class="btn btn-default no">否</button></p></div>')
                .delegate('button.yes', 'click', function(e){
                    $(e.target).parents('div.alert').alert('close');
                    deferrd.resolve();
                })
                .delegate('button.no', 'click', function(e){
                    $(e.target).parents('div.alert').alert('close');
                    deferrd.reject();
                }).appendTo($('#form_cash_output')).alert();

            return deferrd;
        }).then(function(){
            $('#form_cash_output').unbind('submit');
            $('#submit').click();
        });
    });
</script>
