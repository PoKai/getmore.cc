
            <div class='member_order_list_title'>
              <img src='/assets/images/order_list.png' />
              <span>購物訂單</span>
            </div>
            <table class='table'>
                <tr>
                    <th>訂單編號</th>
                    <th>訂購日期</th>
                    <th>訂購件數</th>
                    <th>消費金額</th>
                    <th>訂單狀態</th>
                    <th>訂單過期倒數</th>
                    <th></th>
                </tr>
                <?php
foreach ($orders as $order) {
	?>
                      <tr>
                          <td><?php echo $order['order_number']?></td>
                          <td><?php echo $order['created_at_label']?></td>
                          <td><?php echo $order['items_count']?></td>
                          <td><?php echo $order['total_price']?></td>
                          <td>
                          <? if(isset($order['payable']) && !$order['payable']): ?>
                            已過期
                          <? else: ?>
                            <?=$order['status_label'];?>
                          <? endif; ?>
                          </td>
                          <? if(isset($order['payable']) && $order['payable'] && $order['status']==='1'): ?>
                          <td>
                              <?php if ($order['payment_method'] === 'credit-cart'): ?>
                                <span data-expired='<?php echo $order['expired_at']?>' class="defaultCountdown"></span>
                                <form action='<?=payment_url();?>checkout/restart_for_pay?redirect=<?=urlencode(base_url());?>' method='post'>
                                    <input type='hidden' name='order_number' value='<?php echo $order['order_number']?>' />
                                    <input type='hidden' name='payment_method' value='credit-cart' />
                                    <input type='submit' value='立即付款' class='btn btn-primary btn-xs' />
                                </form>
                              <?php endif;?>
                              <?php if ($order['payment_method'] === 'atm'): ?>
                                <span data-expired='<?php echo $order['expired_at']?>' class="atmCountdown"></span>
                                <form action='/member/view_order' method='post'>
                                    <input type='hidden' name='order_number' value='<?php echo $order['order_number']?>' />
                                            <input type='submit' value='立即付款' class='btn btn-primary btn-xs' />
                                </form>
                              <?php endif;?>
                          </td>
                          <? else: ?>
                          <td></td>
                          <? endif; ?>
                          <td>
                              <?php if ($order['status'] !== '-1' && !isset($order['payable']) || isset($order['payable']) && $order['payable']): ?>
                              <form action='/member/view_order' method='post'>
                                  <input type='hidden' name='order_number' value='<?php echo $order['order_number']?>' />
                                  <input type='submit' value='查看' class='btn btn-xs margintop-3' />
                              </form>
                              <?php endif;?>
                          </td>
                      </tr>
                <?php
}
?>

            </table>

<script type="text/javascript">
    $(function () {
        $('.defaultCountdown').each(function(){
            var expired_at_string = $(this).data('expired');
            var expired_at = new Date(expired_at_string);
            $(this).countdown({ until: expired_at,
                                                  layout: '{mn}:{sn}',
                                                  expiryUrl: '/member/order'
                                             });
        });

        $('.atmCountdown').each(function(){
            var expired_at_string = $(this).data('expired');
            var expired_at = new Date(expired_at_string);
            $(this).countdown({ until: expired_at,
                                                  layout: '{dn}:{hn}:{mn}:{sn}',
                                                  expiryUrl: '/member/order'
                                             });
        });

    });
</script>
<style>
    form{
        display: inline;
    }
</style>
