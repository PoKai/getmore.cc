<form id="step-4" action="/" method="GET" role="form">
<!-- this is a dumb form for router.js to execute some codes on this page
    search "step-4" in router.js for more information
-->
</form>
<div class="alert alert-info" style="padding:20px 20px 30px 20px;">
<p><b>請點選下方按鈕列印申請表並貼於外箱。</b></p>
<p><b>若不方便列印, 也可按下按鈕，將顯示的所有資訊抄下後貼於外箱!</b></p>
<p><b>請務必黏貼申請表, 物流士會至您的取貨地點收貨。</b></p><br />
<p style="font-size:10px;">※ 請注意，GetMore 會通知物流到您指定的地點取件，請將代售衣服用<u>適當大小</u>的包材裝成「1袋/箱」，並確實封口。</p>
<p style="font-size:10px;">※ 請注意，委託派件是透過 GetMore 通知黑貓宅急便至您府上取件，將由黑貓宅急便物流士直接以電話或是按電鈴通知您，您可直接將已經包裝好商品交給物流士，或是與物流士另外約定取貨時間。請務必保持手機暢通，在取件過程中，GetMore 無法追蹤到您與宅急便物流士聯繫狀況，若未接到物流士來電，或是無法完成取件的任何狀況，請來信 GetMore客服，我們將協助您完成收件。</p>
<center><a id="prints" class="btn btn-lg btn-info" style="border-radius:5px;margin:15px 0;"href="#" onclick="window.open('/sellorder/prints/<?php echo $order['order_number']; ?>');">
<i class="fa fa-print fa-2x pull-left"></i>
列印託運資訊<br>GetMore</a></center>
</div>


					<?php /*
					<div class='col-md-9' id='upper_container'>
						<div id='instruction' class="col-md-12">
							<div class="col-md-6">
								<h3><span id='congratulation'>恭喜您</span>完成二手衣代售申請!</h3>
								<p>請點選右方按鈕列印申請表並貼於外箱。</p>
								<p>若不方便列印, 也可將下列所有資訊抄下後貼於外箱!</p>
								<p>請務必黏貼申請表, 物流士會至您的取貨地點收貨。</p>
							</div>
							<div class="col-md-6">

							

							<a id="prints" href="#" onclick="window.open('/sellorder/prints/<?php echo $order['order_number']; ?>');">
								                      <i class="fa fa-print fa-6x"></i>
								<!--<img id='print' src="/assets/images/secondhand_management/print.png" alt="" />-->
							</a>
							</div>
						</div>
					</div>

					<div id='receipt' class="col-md-10 pull-right">
						<div class="row" id='first_row'>
							<div id='sender' class="col-md-8 bordered_div">
									<div id="sender_name" class='info_div'>
										寄件人:<span id="sender_name_value"><?php echo $order['user_name'] ?></span>
										<hr class="dashed_hr"/>
									</div>
									<!--<div id='sender_zip_code' class='info_div'>
										郵遞區號:<span id="sender_zip_code_value"><?php //echo $user_address['zipcode']; ?></span>
										<hr class="dashed_hr"/>
									</div>-->
									<div id="sender_address" class='info_div'>
										寄件人地址:<span id="sender_address_value"><?php echo $order['user_address'] ?></span>
										<hr class="dashed_hr"/>
									</div>
									<div class="blank_rectangel"></div>
							</div>
							<div id="production_number_code" class='col-md-4 bordered_div'>
								<p>貨單編號</p>
                <p><?php echo $order['order_number'] ?></p>
						<!--<p><img id="barcode" src="/assets/images/barcode/<?php echo $ordernumber; ?>.jpg" /></p>-->
							</div>
						</div>
						<div id="receiver" class='col-md-12 bordered_div'>
							<!--<div id="receiver_zip_code" class='info_div'>
								郵遞區號:<span id="receiver_zip_code_value"><?php //echo $delivery_address['zipcode']; ?></span>
								<hr class="dashed_hr"/>
							</div>-->
							<div id="receiver_address" class='info_div'>
								到貨地址:<span id="receiver_address_value">106 台北市大安區新生南路一段165巷18號</span>
								<hr class="dashed_hr"/>
							</div>
							<div id="receiver_name" class='info_div'>
								收件人:<span id="receiver_name_value">GetMore 二次時尚</span>
								<hr class="dashed_hr"/>
							</div>
							<div class="blank_rectangel"></div>
						</div>
						<div id="serial_numbers" class='bordered_div'>
							<div id="production_number" class='info_div'>
								貨單編號:<span id="production_number_value"><?php echo $order['order_number'] ?></span>
								<hr class="dashed_hr"/>
							</div>
							<!--<div id="transport_number" class='info_div'>
								貨單編號:<span id="transport_number_value"></span>
								<hr class="dashed_hr"/>
							</div>-->
							<div class="blank_rectangel"></div>
						</div>
						<div id="content_info" class='bordered_div'>
							<div id="content" class='info_div'>
								內容物:<span id="content_value"> 小衣<?php echo $small; ?> 件/ 大衣<?php echo $big; ?>件/  羽絨衣<?php echo $fly; ?>件</span>
								<hr class="dashed_hr"/>
							</div>
							<div id="unit" class='info_div'>
								總件數:<span id="unit_value"><?php echo $total; ?></span>
								<hr class="dashed_hr"/>
							</div>
							<div class="blank_rectangel"></div>
						</div>
					</div>   */?>

