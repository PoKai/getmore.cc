            <div class='member_order_list_title'>
              <img src='/assets/images/little_clothing.png' />
              <span>販售管理</span>
            </div>

<form class="form-horizontal" role="form">
  <div class="form-group">
    <div class="col-sm-12">
        <p class='form-control-static margintop20 marginbottom10'>
          未上架二手衣<span class="getmoregreen marginright5 marginleft10"><?php echo count($clothings->receiving) ?></span>筆<span class='marginright5 marginleft10'><strong>|</strong></span>
          銷售中二手衣<span class="getmoregreen marginright5 marginleft10"><?php echo count($clothings->selling) ?></span>筆<span class='marginright5 marginleft10'><strong>|</strong></span>
          買家已下訂二手衣<span class="getmoregreen marginright5 marginleft10"><?php echo count($clothings->sent) ?></span>筆<span class='marginright5 marginleft10'><strong>|</strong></span>
          確定售出二手衣<span class="getmoregreen marginright5 marginleft10"><?php echo count($clothings->finished) ?></span>筆
        </p>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">顯示</label>
    <div class="col-sm-4">
        <select id='select_table' class='form-control'>
            <option value='receiving_table'>未上架二手衣</option>
            <option value='selling_table' selected='selected'>銷售中二手衣</option>
            <option value='sent_table'>買家已下訂二手衣</option>
            <option value='finished_table'>確定售出二手衣</option>
        </select>            
    </div>
  </div>
</form>
<div class='alert alert-info price_notice'>
    二手價請設定在100元以上。
    <?php 
      $suggestion_exist = false;
      foreach($clothings->selling as $item){
          if ( !empty($item->suggested_price) && ($item->suggested_price != $item->product_sell_price) ){
              $suggestion_exist = true;
              break;
          }
      }
    ?>
    <?php if ($suggestion_exist): ?>
        <button class='btn btn-success accept_all_price'>同意全部建議售價</button>
    <?php endif; ?>
</div>

            <table id='receiving_table' class='table'>
                <tr>
                    <th class='col-md-2'>名稱</th>
                    <th class='col-md-2'>商品編號</th>
                    <th>上架日期</th>
                    <th>二手價</th>
                    <th>銷售所得</th>
                </tr>
                <?php foreach($clothings->receiving as $item): ?>
                    <tr>
                        <td><?php echo $item->product_title ?></td>
                        <td><?php echo $item->product_item_number ?></td>
                        <td>尚未上架</td>
                        <td id='<?php echo $item->id ?>' class='edit'><?php echo $item->product_sell_price ?></td>
                        <td><?php echo floor($item->product_sell_price*0.75) ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>


            
            <table id='selling_table' class='table'>
                <tr>
                    <th class='col-md-2'>名稱</th>
                    <th class='col-md-2'>商品編號</th>
                    <th class='col-md-2'>上架日期</th>
                    <th class='col-md-1'>二手價</th>
                    <th class='col-md-1'>銷售所得</th>
                    <th class='col-md-2'>建議售價</th>
                    <th>修改金額</th>
                </tr>
                <?php foreach($clothings->selling as $item): ?>
                    <tr>
                        <td><?php echo $item->product_title ?></td>
                        <td><a href='/clothing/view/<?php echo $item->id ?>' target='_blank'><?php echo $item->product_item_number ?></a></td>
                        <td><?php echo date('Y-m-d', $item->add_time) ?></td>
                        <td id='<?php echo $item->id ?>' class='edit'><?php echo $item->product_sell_price ?></td>
                        <td><?php echo floor($item->product_sell_price*0.75) ?></td>
                        <td>
                          <?php if ( !empty($item->suggested_price) && ($item->suggested_price != $item->product_sell_price) ): ?>
                              <span style='color: red;'><?php echo $item->suggested_price ?></span>
                              <span data-price='<?php echo $item->suggested_price ?>' class='btn btn-xs btn-primary accept_price'>同意</span>
                          <?php else: ?>
                              無
                          <?php endif; ?>
                        </td>
                        <td><span class='btn btn-xs btn-primary button_edit'>修改</span></td>
                    </tr>
                <?php endforeach; ?>
            </table>


            <table id='sent_table' class='table'>
                <tr>
                    <th class='col-md-2'>名稱</th>
                    <th class='col-md-2'>商品編號</th>
                    <th>上架日期</th>
                    <th>二手價</th>
                    <th>銷售所得</th>
                </tr>
                <?php foreach($clothings->sent as $item): ?>
                    <tr>
                        <td><?php echo $item->product_title ?></td>
                        <td><a href='/clothing/view/<?php echo $item->id ?>' target='_blank'><?php echo $item->product_item_number ?></a></td>
                        <td><?php echo date('Y-m-d', $item->add_time) ?></td>
                        <td><?php echo $item->product_sell_price ?></td>
                        <td><?php echo floor($item->product_sell_price*0.75) ?></td>

                    </tr>
                <?php endforeach; ?>
            </table>


            <table id='finished_table' class='table'>
                <tr>
                    <th class='col-md-2'>名稱</th>
                    <th class='col-md-2'>商品編號</th>
                    <th>上架日期</th>
                    <th>二手價</th>
                    <th>銷售所得</th>
                </tr>
                <?php foreach($clothings->finished as $item): ?>
                    <tr>
                        <td><?php echo $item->product_title ?></td>
                        <td><a href='/clothing/view/<?php echo $item->id ?>' target='_blank'><?php echo $item->product_item_number ?></a></td>
                        <td><?php echo date('Y-m-d', $item->add_time) ?></td>
                        <td><?php echo $item->product_sell_price ?></td>
                        <td><?php echo floor($item->product_sell_price*0.75) ?></td>

                    </tr>
                <?php endforeach; ?>
            </table>
            
<script>
    $(document).ready(function(){
        $('#select_table').change(function(){
            var table_id = $(this).val();
            $('table').not('#' + table_id).hide();
            $('#' + table_id).show();
            if (table_id == 'selling_table'){
                $('.price_notice').show();
            }
            else{
                $('.price_notice').hide();
            }
        });
        $('#select_table').trigger('change');

        $('.button_edit').click(function(){
            $editable = $(this).parent().parent().find('.edit');
            $editable.editable('/clothing/update_price', {
                 type      : 'text',
                 cancel    : '<button class="btn btn-xs btn-defualt">取消</button>',
                 submit    : '<button type="submit" class="btn btn-xs btn-warning">確定</button>',
                 indicator : '<img src="/admin/assets/images/loading.gif">',
                 callback  : function(){
                      var new_price = $editable.html();
                      $editable.next().html(Math.floor(new_price*0.75));
                 }
            });
            $editable.trigger('click');
            $editable.off('click');
        });
        
        $('.accept_price').click(function(){
            var result = confirm('您同意將售價調整為' + $(this).data('price') + '元？');
            if (!result){
                return;
            }
            var suggested_price = $(this).data('price');
          
            $editable = $(this).parent().parent().find('.edit');
            $editable.editable('/clothing/update_price', {
                 type      : 'text',
                 cancel    : '<button class="btn btn-xs btn-defualt">取消</button>',
                 submit    : '<button type="submit" class="btn btn-xs btn-warning">確定</button>',
                 indicator : '<img src="/admin/assets/images/loading.gif">',
                 callback  : function(){
                      var new_price = $editable.html();
                      $editable.next().html(Math.floor(new_price*0.75));
                      if (new_price == suggested_price){
                          $editable.next().next().html('無');
                      }
                 }
            });
            $editable.trigger('click');
            $editable.off('click');
            $editable.find('input').val($(this).data('price'));
            $editable.find('button').first().click();
            
        });

        $('.accept_all_price').click(function(){
            var result = confirm('您同意將全部商品調整為建議售價？');
            if (!result){
                return;
            }
            $('.accept_price').each(function(/*index, item*/){

                var suggested_price = $(this).data('price');
                // alert(suggested_price);
                
                $editable = $(this).parent().parent().find('.edit');
                $editable.editable('/clothing/update_price', {
                     type      : 'text',
                     cancel    : '<button class="btn btn-xs btn-defualt">取消</button>',
                     submit    : '<button type="submit" class="btn btn-xs btn-warning">確定</button>',
                     indicator : '<img src="/admin/assets/images/loading.gif">',
                     callback  : function(){

                     }
                });
                $editable.trigger('click');
                $editable.off('click');
                $editable.find('input').val($(this).data('price'));
                $editable.find('button').first().click();

                
            });
            
        });

    });
    
</script>
<style>
    .accept_all_price{
//        margin-bottom: 20px;
    }
</style>
