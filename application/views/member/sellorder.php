
<div class='member_order_list_title'>
  <img src='/assets/images/little_clothing.png' />
  <span>二手衣申請進度查詢</span>
</div>
<table class='table'>
    <tr>
        <th>申請單號</th>
        <th>申請日期</th>
        <th>申請筆數</th>
        <th>處理進度</th>
        <th></th>
    </tr>
    <?php
        foreach($orders as $order){
    ?>
          <tr>
              <td><?php echo $order['order_number'] ?></td>
              <td><?php echo $order['date_purchased'] ?></td>
              <td><?php echo $order['how_many'] ?></td>
              <td>
                  <?php echo get_sellorder_status_label($order['status_id']) ?>
                  <?php if  ( ($order['status_id']==='1') && ( in_array($order['payment_method'], ['credit-cart', 'web-atm']) ) )  : ?>
                      <form action='<?= payment_url(); ?>checkout/restart_for_pay?redirect=<?= urlencode(site_url("/sellorder/steps/4")); ?>' method='post'>
                          <input type='hidden' name='order_number' value='<?php echo $order['order_number'] ?>' />
                          <input type='submit' value='立即付款' class='btn btn-primary btn-xs marginleft5 margintop-3' />
                      </form>
                  <?php endif; ?>                          

                      <?php if ( ($order['status_id']==='1') && ( $order['payment_method']==='atm' )): ?>
                          <form action='/member/' method='post'>
                              <input type='hidden' name='order_number' value='<?php echo $order['order_number'] ?>' />
                                      <input type='submit' value='立即付款' class='btn btn-primary btn-xs marginleft5 margintop-3' />
                          </form>
                      <?php endif; ?>
                  
              </td>
              <td>
                <form action='/member/view_sellorder' method='post'>
                    <input type='hidden' name='order_number' value='<?php echo $order['order_number'] ?>' />
                    <input type='submit' value='查看' class='btn btn-xs margintop-3' />
                </form>
              </td>
          </tr>
    <?php
          }
    ?>
</table>

<style>
      form{
            display: inline;
      }
</style>
