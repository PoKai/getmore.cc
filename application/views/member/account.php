            <div class='member_order_list_title'>
              <img src='/assets/images/my_account.png' />
              <span>我的帳戶</span>
            </div>
            <h3>您累計的帳戶金額共<?php echo $account->balance ?>元</h3>
            <?php if ($account->balance > 0): ?>
                <a href='/member/withdraw' class='btn btn-primary pull-right marginbottom10'>匯出現金給我</a>
            <?php endif; ?>
            <table class='table'>
                <tr>
                    <th class='col-md-2'>日期</th>
                    <th class='col-md-2'>異動名目</th>
                    <th class='col-md-2'>商品/貨單/訂單編號</th>
                    <th>新增金額</th>
                    <th>扣除金額</th>
                    <th>餘額</th>
                    <th>備註</th>
                </tr>
                <?php $balance = $account->balance ?>
                <?php foreach($transactions as $trans): ?>
                    <tr>
                        <td><?php echo date('Y-m-d', strtotime($trans->created_at)) ?></td>
                        <td><?php echo $trans->name ?></td>
                        <?php if (empty($trans->clothing_id)): ?>
                            <td><?php echo $trans->related_number ?></td>
                        <?php else: ?>
                            <td><a href='/clothing/view/<?php echo $trans->clothing_id ?>' target='_blank'><?php echo $trans->related_number ?></a></td>
                        <?php endif; ?>
                        <?php if ($trans->amount > 0): ?>
                            <td><?php echo $trans->amount ?></td>
                            <td></td>
                        <? else: ?>
                            <td></td>
                            <td><?php echo -1 * $trans->amount ?></td>
                        <? endif; ?>
                        <td><?php echo $balance; $balance -= $trans->amount; ?></td>
                        <td><?php echo $trans->note ?></td>
                    </tr>
                <?php endforeach; ?>

            </table>
