<?= validation_errors(); ?>
<center> 
<div>
    <img class="banner-s" id="top" src="/assets/images/event/tedxdachi.jpg">
</div>
</center>
<hr>
<div class="eventcontent">
    <h2>
        12/26 TEDxDachi - VIP 專屬免費索票
    </h2>
    <p>
        感謝您喜歡 GetMore，</br>
        歲末到了，在年底 12/26 晚上，在松菸誠品有一場 TEDxDachi 活動，</br>
        誠摯邀請 GetMore會員一同參與。</br>
    </p>
    <p>
        GetMore提供限量公關票供VIP會員一同參與(價值$200)。</br>
        票數有限，請於12/15填寫下列資料，將由活動通系統發送電子入場券至您的Email。</br>
    </p>    
    <p>
        祝 二次時尚生活愉快</br>
        GetMore 團隊
    </p>
</div>
<hr style="display: none;">
<div>
<center><h3>索票已結束，敬請準時出席</h3></center>
<form class="form-horizontal" role="form" method="post" action="edm" style="display: none;">
    <div class="form-group">
        <label for="username" class="col-sm-2 control-label">姓名</label>
        <div class="col-sm-2">
            <input type="text" class="form-control" id="username" name="username" placeholder="姓名" value="<?= $user['user_name']?$user['user_name']:set_value('username'); ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-4">
            <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?= $user['email']?$user['email']:set_value('email'); ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label for="mobile_phone" class="col-sm-2 control-label">行動電話</label>
        <div class="col-sm-3">
            <input type="tel" class="form-control" id="mobile_phone" name="mobile_phone" placeholder="行動電話" value="<?= $user['mobile_phone']?$user['mobile_phone']:set_value('mobile_phone'); ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">性別</label>
        <div class="col-sm-3">
            <label class="radio-inline">
                <input type="radio" name="gender" value="1" <? if($user['sex'] && isset($user['sex'])): ?>checked="checked"<? else: ?><?= set_radio('gender', '1'); ?><? endif; ?>> 男
            </label>
            <label class="radio-inline">
                <input type="radio" name="gender" value="0" <? if(!$user['sex'] && isset($user['sex'])): ?>checked="checked"<? else: ?><?= set_radio('gender', '0'); ?><? endif; ?>> 女
            </label>
        </div>
    </div>
    <div class="form-group">
        <label for="company" class="col-sm-2 control-label">公司名稱</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" id="company" name="company" placeholder="公司名稱" value="<?= set_value('company');?>" required>
        </div>
    </div>
    <div class="form-group">
        <label for="industry" class="col-sm-2 control-label">產業</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" id="industry" name="industry" placeholder="產業" value="<?= set_value('industry');?>" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-3">
            <button type="submit" class="btn btn-default">送出</button>
        </div>
    </div>
</form>
</div >
<center>
<div>
    <img class="banner-s" id="top" src="/assets/images/event/tedxdachi-1.png">
</div>
<div>
    <img class="banner-s" id="top" src="/assets/images/event/tedxdachi-2.png">
</div>
<a href="http://www.accupass.com/go/tedxdachi1226" target="_blank">其他詳細資訊</a>
</center>



<? if($success): ?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="myModalLabel">申請成功</h3>
      </div>
      <div class="modal-body">
        <h4 class="text-center">恭喜您已申請成功 會由主辦單位寄電子入場券至您的信箱</h4>
      </div>
      <div class="modal-footer" style="text-align: center;">
        <a href="/" class="btn btn-success btn-lg">知道了</a>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $('#myModal').modal("show");
</script>
<? endif; ?>
