<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Add your site or application content here -->
        <p>親愛的會員 您好：</p>
        <p>感謝您的購買！您在Getmore的訂單已建立，將為您保留24小時，請您於付款時限內完成轉帳匯款，訂單才算成立。</p>
        <p>訂單編號：<?= $order_number ?></p>
        <p>訂單金額：<?= $total_price ?></p>
        <p>付款方式：ATM轉帳-未付款</p>
        <p>訂購商品：</p>
        <ul>
            <? foreach ($SubOrders as $sub_order): ?>
                <? foreach ($sub_order->products as $product): ?>
                <li><?= $product->product_title ?></li>
                <? endforeach; ?>
            <? endforeach; ?>
        </ul>
        
        <p>每件商品都是獨一無二只有一件，因此我們設定付款時間限制，逾期請勿轉帳！匯款轉帳資訊請至【會員中心】→【購物訂單】查看。</p>
        
        <p>※基於匯款安全，Getmore並不會於Email及簡訊中透露匯款帳號，請至官方網站會員中心查詢。</p>
        <p><a href="<?php echo site_url('member/order') ?>"><?php echo site_url('member/order') ?></a></p>
        <p>● 本信函為系統自動發出，請勿直接回覆。</p>
        <p>Getmore祝您時尚生活愉快！</p>
    </body>
</html>
