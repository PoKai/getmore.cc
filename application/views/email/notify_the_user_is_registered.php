<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Add your site or application content here -->
        <p>親愛的會員 您好：</p>
        <p>您在「Getmore」註冊成功！</p>
        <p>Getmore祝您時尚生活愉快！</p>
        <p>● 本信函為系統自動發出，請勿直接回覆。</p>
        <p>● 若您未在GetMore二次時尚註冊，請以本信箱聯絡客服( service@getmore.cc )。</p>
    </body>
</html>
