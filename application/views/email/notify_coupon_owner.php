        <?php echo $mail_content ?>

        <p>您的消費禮券編號是「<?php echo $coupon->code ?>」，面額「<?php echo $coupon->amount ?>」元，最低消費限制為「<?php echo $coupon->minimum_charge ?>」元，使用期限為「<?php echo $coupon->expired_at ?>」之前。</p>
        <p>● 本信函為系統自動發出，請勿直接回覆。</p>
        <p>GetMore祝您二次時尚的生活愉快！</p>
