
        <p>親愛的會員 您好：</p>
        <p>您在「Getmore時尚」的代售商品，經衣管家評估，建議調整價格如下：</p>
        <?php foreach ($clothings as $clothing): ?>
            <p>建議將「<?php echo $clothing->product_title ?>」調整為「<?php echo $clothing->suggested_price ?>元」</p>        
        <?php endforeach; ?>
        <p>調整價格後，在分類頁將得到「再次降價！」的標記，可增加曝光，讓買家知道商品又更划算囉！</p>        
        <p>詳細資訊可以到會員中心查看。</p>
        <p><a href="<?php echo site_url('member/clothing_management') ?>"><?php echo site_url('member/clothing_management') ?></a></p>
        <p>● 本信函為系統自動發出，請勿直接回覆。</p>
        <p>Getmore祝您時尚生活愉快！</p>
