<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Add your site or application content here -->
        <p>親愛的會員：</p>
        <p>您在Getmore 的訂單已經完成付款，正準備出貨囉！祝您時尚生活愉快！</p>
        <p></p>
        <p>訂單編號：<?= $order_number ?></p>
        <p>付款金額：<?= $total_price ?></p>
        <p>付款方式：<?= $payment_method ?></p>
        <p>您訂購的衣服如下：</p>
        <ul>
            <? foreach ($SubOrders as $sub_order): ?>
                <? foreach ($sub_order->products as $product): ?>
                <li><?= $product->product_title ?></li>
                <? endforeach; ?>
            <? endforeach; ?>
        </ul>
        

        <p>購買人：<?= $buyer_name ?></p>
        <p>購買人電話：<?= $buyer_phone_number ?></p>
        <p>收件人：<?= $receiver_name ?></p>
        <p>收件人電話：<?= $receiver_phone_number ?></p>
        <p>收件地址：<?= $receiver_address ?></p>
        <p>付款狀態：已付款</p>

        <p>*您的商品若包括賣家自行出貨商品，有可能分別送達，請您保持手機暢通，物流將跟您聯繫</p>
        
        <p style="text-align: center;"><a href="<?php echo site_url('member/order') ?>">查看訂單</a></p>
        <p>相關資訊：</p>
        <p><a href="http://support.getmore.cc/section/buyer">消費者相關政策</a></p>
        <p>此Email為系統自動發信，有問題請洽Email至 service@getmore.cc</p>
    </body>
</html>
