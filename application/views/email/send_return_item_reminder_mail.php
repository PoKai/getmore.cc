<p>親愛的Getmore會員您好：</p>
<p>
您的訂單<?php echo $order['order_number'] ?>，申請退貨商品「<?php echo $item['product_title'] ?>」已完成退款手續。詳細退款方式請參考 <a href='http://getmore.cc/info/buy#return'>http://getmore.cc/info/buy#return</a></p>
<p>謝謝您的耐心！</p>
<p>● 本信函為系統自動發出，請勿直接回覆。</p>
<p>Getmore祝您時尚生活愉快！</p>
