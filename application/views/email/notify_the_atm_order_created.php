<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Add your site or application content here -->
        <p>親愛的會員 您好：</p>
        <p>感謝您的購買！您在「Getmore」的貨單已經成立！</p>
        <p>貨單編號：<?php echo $order['order_number'] ?></p>
        <p>付款金額：<?php echo $order['final_price'] ?></p>
                
        <p>若選擇ATM轉帳，匯款資訊可以到會員中心查看。</p>
        <p>※基於匯款安全，Getmore並不會於Email及簡訊中透露匯款帳號，請至官方網站會員中心查詢。</p>
        <p><a href="<?php echo site_url('member/sellorder') ?>"><?php echo site_url('member/sellorder') ?></a></p>
        <p>● 本信函為系統自動發出，請勿直接回覆。</p>
        <p>Getmore祝您時尚的生活愉快！</p>
    </body>
</html>
