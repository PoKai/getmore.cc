<p>親愛的Getmore會員您好：</p>
<p>您申請代售的衣物已送達，貨單編號「<?php echo $order->order_number ?>」，工作人員正在進行逐件驗收，若無異常，即進入洗衣程序，並陸續上架；若商品不符合GetMore規範，客服人員會以電話或信件與您溝通聯繫！</p>
<p>後續可參考網站<a href=<?php echo site_url('/member/clothing_management') ?>>【會員中心】→【販售管理】</a>，清楚掌握衣服上架販售狀況！</p>
<p>謝謝您的耐心等候！</p>
<p>● 本信函為系統自動發出，請勿直接回覆。</p>
<p>Getmore祝您時尚生活愉快！</p>







