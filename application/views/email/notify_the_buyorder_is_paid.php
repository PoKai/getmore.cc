<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Add your site or application content here -->
        <p>親愛的會員 您好：</p>
        <p>感謝您的購買！您在Getmore的訂單已完成付款，將於近日出貨。</p>
        <p>訂單編號：<?php echo $order['order_number'] ?></p>
        <p>訂單金額：<?php echo $order['total_price'] ?></p>
        <p>訂購商品：</p>
        <?php foreach ($clothings as $clothing): ?>
            <p><?php echo $clothing['product_title'] ?></p>        
        <?php endforeach; ?>
        <p> **您的商品若包括賣家自行出貨商品，有可能分開送達，請您務必保持手機暢通，物流士將會與您聯繫。</p>
        <p><a href="<?php echo site_url('member/order') ?>">查看訂單</a></p>
        <p>相關資訊：<br>
            <a href="http://support.getmore.cc/section/buyer/">消費者相關政策</a>
        </p>
        <p>● 本信函為系統自動發出，請勿直接回覆。</p>
        <p>Getmore祝您時尚生活愉快！</p>
    </body>
</html>
