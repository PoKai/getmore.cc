<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <p>Hi <?=$User->user_name?>，</p>
        <p>您在Getmore 收到新訂單，買家已經以<? switch ($payment_method) {
            case 'atm':
                echo 'atm';
                break;
            case 'credit-cart':
                echo '信用卡';
                break;
            case 'pay-at-home':
                echo '貨到付款';
                break;
            default:
                echo '全額抵扣';
                break;
        } ?>付款，請妥善包裝並盡快出貨。</p>

        <p>訂單編號：<?=$sub_order_number?></p>
        <ul style="list-style-type: none;">
            <? foreach ($products as $product): ?>
            <li>
                <img src="http://s3.getmore.cc/clothing/<?=explode(',', $product->files)[0];?>/medium.jpg" style="width: 100px;">
                上架商品「<a href="<?=site_url('/clothing/' . get_product_type_by_product_item_number($product->product_item_number) . '/' . $product->id);?>"><?=$product->product_title;?></a>」
            </li>
            <? endforeach; ?>
        </ul>

        <p>購買人：<?=$buyer_name?></p>
        <p>購買人電話：<?=$buyer_phone_number?></p>
        <p>收件人：<?=$receiver_name?></p>
        <p>收件人電話：<?=$receiver_phone_number?></p>
        <p>收件地址：<?=$receiver_address?></p>
        <p>付款狀態：已付款</p>

        <p style="text-align: center;"><a href="<?php echo site_url(get_product_type_by_product_item_number($product->product_item_number) . '/orderlist/2')?>">查看訂單</a></p>
        <p>相關資訊：</p>
        <p>此Email為系統自動發信，有問題請洽Email至 service@getmore.cc</p>
    </body>
</html>
