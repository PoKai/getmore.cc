
        <p>親愛的會員 您好：</p>
        <p>「Getmore」系統收到您重新設定密碼的申請</p>
        <p>請於申請 12 小時內開啟以下連結，進行密碼重設</p>
        <p><a href="<?php echo site_url('auth/forgotten_password_code?code=' . $code) ?>"><?php echo site_url('auth/forgotten_password_code?code=' . $code) ?></a></p>
        <p>若您並未啟用忘記密碼程序，請忽略此封信，謝謝您。</p><br>
        <p>● 本信函為系統自動發出，請勿直接回覆。</p>
        <p>● 為維護資訊安全，本連結將於12小時內失效。</p>
        <p>● 若無法點擊連結，可複製網址貼入瀏覽器欄位中。</p>
        <p>● 請務必保管此信件，切勿將網址外流。</p><br>
        <p>GetMore祝您二次時尚的生活愉快！</p>
