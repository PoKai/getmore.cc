<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Add your site or application content here -->
        <p>親愛的會員 您好：</p>
        <p>感謝您的購買！您在Getmore的訂單已建立。</p>
        <p>訂單編號：<?= $order_number ?></p>
        <p>訂單金額：<?= $total_price ?></p>
        <p>您訂購的衣服如下：</p>
        <ul>
            <? foreach ($SubOrders as $sub_order): ?>
                <? foreach ($sub_order->products as $product): ?>
                <li><?= $product->product_title ?></li>
                <? endforeach; ?>
            <? endforeach; ?>
        </ul>
        <ol>
            感謝您的購買，您選擇的是貨到付款。
            <li>貨到時，物流士將與您電話聯繫，來電為一般手機號碼，請勿拒接，以免錯過配送。</li>
            <li>物流士將著「黑貓宅急便」的制服，確認商品後，將貨款直接交由物流士。</li>
            <li>提醒您！連續配送3次無法成功，您的訂單將被取消，並且有可能被取消使用貨到付款的權利。</li>
        </ol>

        <p>訂單詳細資訊可以到會員中心查看。</p>
        <p><a href="<?php echo site_url('member/order') ?>"><?php echo site_url('member/order') ?></a></p>
        <p>● 本信函為系統自動發出，請勿直接回覆。</p>
        <p>Getmore祝您時尚生活愉快！</p>
    </body>
</html>
