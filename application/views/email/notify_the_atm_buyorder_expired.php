<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <!-- Add your site or application content here -->
        <p>親愛的會員 您好：</p>
        <p>您在 Getmore 的訂單因為超過系統ATM開放付款時間，系統已轉為過期訂單。</p>
        <p>訂單編號：<?php echo $order['order_number'] ?></p>
        <p>下列商品已轉為「開放購買」狀態，供全部會員購買。</p>
        <?php foreach ($clothings as $clothing): ?>
            <p><?php echo $clothing['product_title'] ?></p>        
        <?php endforeach; ?>
        <p>由於每件商品都只有一件，因此我們設定了付款時間的限制。</p>
        <p>若要再次選購，請直接將商品加入購物車。</p>
        <p>期待您再次訂購，謝謝您。</p>
        <p>● 本信函為系統自動發出，請勿直接回覆。</p>
        <p>Getmore祝您時尚生活愉快！</p>
    </body>
</html>
