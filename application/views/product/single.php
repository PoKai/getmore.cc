<div id="content">
    <div style="padding: 0px 10px;">
        <div class="nav"><a href="/">首頁</a> &#8250; <a href="/subject/lists/all">所有商品</a> &#8250; <a href="/subject/lists/<?php echo $category_id; ?>"><?php echo $category_name; ?></a> &#8250; <a href="/subject/single/<?php echo $subject_id; ?>"><?php echo $subject_name; ?></a> &#8250; <?php echo $title; ?></div>
        <div class="product_frame">
            <div class="product_left">
                <div class="product_imgblock">
                    <?php
                        for($i=0; $i<$image_list_count; $i++):
                            $image_list[$i]['file_name'] = (isset($image_list[$i]['file_name'])) ? $image_list[$i]['file_name'] : '';
                    ?>
                        <?php if($i == 0): ?>
                        <div class="product_img">
                            <a rel="gal1" href="<?php echo site_url('upload/' . $image_list[$i]['file_name']); ?>" id="main_pic" title="<?php echo $title; ?>">
                                <img src="<?php echo image_url($image_list[$i]['file_name'], 370, 350); ?>" />
                            </a>
                        </div>
                        <?php endif; ?>
                        <div class="product_imgnav">
                            <a href='javascript:void(0);' rel="{gallery: 'gal1', smallimage: '/upload/thumbs/370_350_<?php echo $image_list[$i]['file_name']; ?>',largeimage: '/upload/<?php echo $image_list[$i]['file_name']; ?>'}">
                                <img data-src="<?php echo $image_list[$i]['file_name']; ?>" src="<?php echo image_url($image_list[$i]['file_name'], 82, 82); ?>" />
                            </a>
                        </div>
                    <?php endfor;?>
                </div>
            </div>
            <div class="product_right">
                <div class="product_introblock">
                    <div class="product_back">&#8250; 回到<a href="/subject/single/<?php echo $subject_id; ?>"><?php echo $subject_name; ?></a></div>
                    <div class="product_title"><span class="title"><?php echo $title; ?></span></div>
                    <div class="product_intro">
                        <?php echo $information; ?>
                    </div>
                </div>
                <div class="product_price">
                    <div class="product_price_block">
                        <div class="product_price_org">$<?php echo $original_price;?> up</div>
                        <div class="product_price_special">$ <?php echo $sell_price;?></div>
                    </div>
                    <div class="product_price_block">
                        <div class="product_amont">
                            <?php if ($status): ?>
                            數量&nbsp;<?php echo $order_count; ?>
                            <?php else: ?>
                            <?php echo $message; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if (!(empty($inventory_amount) or $inventory_amount <= '0')):?>
                    <div class="product_price_block">
                        <div class="product_price_carticon"><img src="/assets/images/product/cart_icon.png" /></div>
                        <div class="product_price_cartadd"><img src="/assets/images/product/add_cart.png" data-price="<?php echo $sell_price;?>" data-id="<?php echo $id; ?>" data-type="product" class="add-cart" /></div>
                    </div>
                    <?php endif;?>
                </div>
            </div>
            <p class="clear">&nbsp;</p>
        </div>

        <div class="product_frame">
            <div class="product_left">
                <?php echo $introduction; ?>
            </div>
            <div class="product_right">
                <div class="product_topic">
                    <div class="product_topic_black">產品訊息</div>
                </div>
                <div class="product_info">
                    <?php foreach($detail as $row):?>
                    <div class="product_info_list">
                        <div class="product_info_formname"><?php echo $row->name; ?></div>
                        <div class="product_info_formvalue"><?php echo $row->value; ?></div>
                    </div>
                    <?php endforeach;?>
                    <div class="product_info_bottom"><a href="mailto:lab@getmore.cc" TARGET="_blank">任何問題歡迎您寄信到getmore信箱<img src="/assets/images/product/mail.png"></a></div>
              </div>
                <div class="product_topic">
                    <div class="product_topic_black">運送須知</div>
                </div>
                <div class="product_info">
                    <?php foreach($delivery as $row):?>
                    <div class="product_info_list">
                        <div class="product_info_formname"><?php echo $row->name; ?></div>
                        <div class="product_info_formvalue"><?php echo $row->value; ?></div>
                    </div>
                    <?php endforeach;?>
                </div>
                <div class="product_persist">
                    <div class="product_persist_title">getmore 風格</div>
                    <span class="black">getmore想要帶給大家的是一種生活態度與品味，</span><br>
                    我們將這些理念轉換成獨家設計品，也呈現在精選品牌商品上，<br>
                    希望讓<span class="red">美學</span>、<span class="red">美感</span>在生活中實現。
                </div>
                <div class="product_price">
                    <div class="product_price_block">
                        <div class="product_price_org">$<?php echo $original_price;?> up</div>
                        <div class="product_price_special">$ <?php echo $sell_price;?></div>
                    </div>
                    <div class="product_price_block">
                      <div class="product_amont">數量&nbsp;<?php echo $order_count; ?></div>
                    </div>
                    <div class="product_price_block">
                        <div class="product_price_carticon"><img src="/assets/images/product/cart_icon.png"></div>
                        <div class="product_price_cartadd"><img src="/assets/images/product/add_cart.png" data-price="<?php echo $sell_price;?>" data-id="<?php echo $id; ?>" data-type="product" class="add-cart" /></div>
                    </div>
                </div>
            </div>
            <p class="clear">&nbsp;</p>
        </div>
    </div>
</div>
