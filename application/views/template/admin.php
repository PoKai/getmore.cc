<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--><html class="no-js" lang="<?php echo $lang; ?>"> <!--<![endif]-->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $meta_charset; ?>"/>
		<title><?php echo $site_title; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="format-detection" content="telephone=no">
    <!-- add meta tag added by howtomakeaturn -->
  
		<link rel="shortcut icon" href="/assets/images/favicon.ico">

		<link rel="stylesheet" href="/assets/css/bootstrap.css"/>
    
        <script src="/assets/vendor/jquery/jquery.js"></script>
        <script src="/assets/javascript/bootstrap.js"></script>        

    </head>

	<body>
    <?php echo $content ?>
    
		</body>
	</html>

