<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--><html class="no-js" lang="<?php echo $lang; ?>"> <!--<![endif]-->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $meta_charset; ?>"/>
		<title><?php echo $site_title; ?></title>
        <meta name="description" content="<?php echo $site_description; ?>" />
        <meta name="keywords" content="<?php echo $site_keywords; ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?php echo $meta_tag; ?>

    <!-- add meta tag added by howtomakeaturn -->
     <meta property="og:image" content="http://beta.getmore.cc/assets/images/fb-01.jpg" />
  
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="stylesheet" href="/assets/css/bootstrap.css"/>
		<link rel="stylesheet" href="/assets/css/getmore_bootstrap.css"/>
		<link rel="stylesheet" href="/assets/css/home.css"/>
		<link rel="stylesheet" href="/assets/css/clothing.css"/>
		<link rel="stylesheet" href="/assets/css/checkout.css"/>
		<link rel="stylesheet" href="/assets/css/member.css"/>
		<link rel="stylesheet" href="/assets/css/sellorder.css"/>
    
    <!-- ugly structure by howtomakeaturn. it's a workaroud for managing javascript files
            with buying and selling pages.
     -->
    <?php if (($this->uri->segment(1)=='clothing')||($this->uri->segment(1)=='checkout')){ ?>
        <script src="/assets/vendor/jquery/jquery.js"></script>
        <script src="/assets/javascript/bootstrap.js"></script>
        <script src="/assets/vendor/underscore-amd/underscore.js"></script>
        <script src="/assets/vendor/backbone-amd/backbone.js"></script>

        <script src="/assets/javascript/howtomakeaturn/backbone.localStorage.js"></script>
        <script src="/assets/javascript/libs/jquery/jquery.twzipcode-1.4.1.js"></script>
          
        <script src="/assets/javascript/howtomakeaturn/main.js"></script>
        <script src="/assets/javascript/howtomakeaturn/buyer_cart.js"></script>
    <?php }else{ ?>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="/assets/vendor/jquery/jquery.min.js"></script>
        <script type="text/javascript">
            var getmore = getmore || {}; // Backbone namespace
            var root_path = '.';
        </script>        
        <script data-main="/assets/javascript/main" src="/assets/vendor/requirejs/require.js"></script>
    <?php }//end else ?>
  
  
<!--<script src="/assets/javascript/bootstrap.js"></script>
	<script src='/assets/javascript/home.js'></script>-->
<!--	<script data-main="/assets/javascript/main-built" src="/assets/vendor/requirejs/require.js"></script>-->

<!-- new google analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46722397-1', 'getmore.cc');
  ga('send', 'pageview');

</script>

<!-- old google analytics 
        <script type="text/javascript">
        
          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-26930222-1']);
          _gaq.push(['_setDomainName', 'getmore.cc']);
          _gaq.push(['_trackPageview']);
        
          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
        
        </script>   
        --> 
	</head>

	<body>
		<div class='container'>
			<div class='header'>
				<nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
				  <div class="navbar-header">
				    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				      <span class="sr-only">Toggle navigation</span>
				      <span class="icon-bar"></span>
				      <span class="icon-bar"></span>
				      <span class="icon-bar"></span>
				    </button>
				  </div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a data-toggle="modal" href="/"><img src="/assets/images/home.png" alt="" id='log_in'/></a></li>
						<li><a data-toggle="modal" href='/sellorder/info/pricing/'>費用說明</a></li>
						<li><a data-toggle="modal" href="http://bit.ly/1cHbuPX">開始網路二手衣代售服務！</a></li>
					</ul>
          
          
                      <ul class="nav navbar-nav navbar-right">
                        <?php if (($this->uri->segment(1)=='clothing')/*||($this->uri->segment(1)=='checkout')*/){ ?>
                          <!-- shopping cart start -->
                          <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                              <img src="/assets/images/cart.png">
                            </a>
                            <ul class="dropdown-menu" id="shopping-cart-menu">
                              <li>
                                <span>Loading ... </span>
                              </li>
                            </ul>
                          </li>
                          <!-- shopping cart end -->
                          <?php } ?>
                        
                          <?php if(!$logined): ?>
                              <li><a data-toggle="modal" href="#logInModal"><img src="/assets/images/login.png" alt="" id='log_in'/> Login</a></li>
                          <?php else: ?>
                             <li><a href="/member/order">會員中心</a></li>                         
                              <li><a data-toggle="modal" href="/auth/logout?redirect_url=<?php echo urlencode(current_url()); ?>"><img src="/assets/images/logout.png" alt="" id='log_in'/> Logout</a></li>
                          <?php endif; ?>
                      </ul>
				</div><!-- /.navbar-collapse -->
				</nav>
				
				<div class="modal fade" id='signUpModal'>
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">註冊會員</h4>
							</div>
							<div class="modal-body">
								<div class="fb_login">
									<h4>GetMore 是亞洲第一個二手衣時尚購物網站</h4>
									<a class='fb_login_btn btn btn-primary' href="<?php echo site_url('/auth/facebook?redirect_url=' . urlencode(current_url()))?>">用facebook登入</a> 快使用facebook快速登入!
									<hr class="dashed_hr"/>
								</div>
								<div id="getmore_login">
									<div class="row">
										<div class="col-md-4">
											<h4>註冊會員</h4> 
										</div>
										<div class="col-md-8 pull-right announcement getmore_instruction">
											還不是GetMore的會員嗎？ 免費加入！
										</div>
									</div>
									<form method="POST" action="/auth/register" name="dig_register">
										<div class="form-group">
											<label for="sign_up_email">信箱</label><div class='col-md-5 pull-right description'>此為您的會員登入帳號</div>
											<input type="email" class="form-control" id="email" name="email" placeholder="email">
										</div>
										<div class="form-group">
											<label for="nickname">暱稱</label> <div class='col-md-5 pull-right description'>14字元以內</div>
											<input type="text" class="form-control" id="user_name" name="user_name" placeholder="暱稱">
										</div>
										<div class="form-group">
											<label for="new_password">密碼</label>
											<input type="password" class="form-control" id="new_password" name="password" placeholder="Password">
										</div>
										<div class="form-group">
											<label for="password_confirm">確認密碼</label>
											<input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm Password">
										</div>
										<div class="form-group">
											<input type="checkbox" value="1" name="aggree" style="width:20px;">我已閱讀，並同意<a href="/terms">getmore 服務條款</a>
										</div>
										<a id='start_to_getmore' href="javascript:document.forms['dig_register'].submit();" class='btn btn-primary'>開始GetMore</a>
										<div id='registration' class="col-md-4 pull-right">
											已是會員？ <a id='logIn' href="#logInModal" data-toggle="modal">由此登入！</a>
										</div>
									</form>
								</div>
							</div>
							<div class="modal-footer">
								<p class='announcement'>已成為「會員」或是點擊「用facebook 登入」按鈕的同時,表示您已詳讀並同意GetMore的<a href="/sellorder/info/userpolicy/">使用者條款</a>。</p>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				<div class="modal fade" id='logInModal'>
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">會員入口</h4>
							</div>
							<div class="modal-body">
								<div class="fb_login">
									<h4>GetMore 是亞洲第一個二手衣時尚購物網站</h4>
									<a class='fb_login_btn btn btn-primary' href="<?php echo site_url('/auth/facebook?redirect_url=' . urlencode(current_url()))?>">用facebook登入</a> 快使用facebook快速登入!
									<hr class="dashed_hr"/>
								</div>
								<div id="getmore_login">
									<div class="row">
										<div class="col-md-4">
											<h4>會員登入</h4> 
										</div>
										<div class="getmore_instruction col-md-8 pull-right announcement">
											若已是GetMore的會員,請在此登入!
										</div>
									</div>
									<form name="login-form" action="/auth/login" method="POST">
										<div class="form-group">
											<label for="email">信箱</label>
											<input type="email" class="form-control" id="email" name="identity" placeholder="email">
										</div>
										<div class="form-group">
											<label for="password">密碼</label> <a id='forget_password' class="pull-right" href="#">忘記密碼？</a>
											<input type="password" name="password" class="form-control" id="password" placeholder="Password">
										</div>
										<a id='start_to_getmore' href="javascript:document.forms['login-form'].submit();" class='btn btn-info'>開始GetMore</a>
 										<input type="hidden" name="redirect_url" value="<?php echo urlencode(current_url());?>" />
                                        <div id='registration' class="col-md-4 pull-right">
											還不是會員? <a id='register' href="#signUpModal" data-toggle="modal">來註冊!</a>
										</div>
									</form>
								</div>
							</div>
							<div class="modal-footer">
								<p class='announcement'>已成為「會員」或是點擊「用facebook 登入」按鈕的同時,表示您已詳讀並同意GetMore的<a href="/sellorder/info/userpolicy/">使用者條款</a>。</p>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				<div class="modal fade" id='videomodel'>
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">GetMore 理念</h4>
							</div>
							<div class="modal-body">
								<iframe src="//player.vimeo.com/video/74211715" width="580" height="422" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
							</div>
							<div class="modal-footer">
								<p class='announcement'>GetMore二次時尚，重新定義衣服的價值。</p>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				<div class="modal fade" id='modalwomen'>
					<div class="modal-dialog-800">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">尺寸參考表-WONEN</h4>
							</div>
							<div class="modal-body">
								<div class="modal-center">
								<img src="/assets/images/content/buy/womensize.png">
								</div>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				<div class="modal fade" id='modalmen'>
					<div class="modal-dialog-800">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">尺寸參考表-MEN</h4>
							</div>
							<div class="modal-body">
								<div class="modal-center">
								<img src="/assets/images/content/buy/mensize.png">
								</div>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				<div class="modal fade" id='modalkids'>
					<div class="modal-dialog-800">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">尺寸參考表-KIDS/BABY</h4>
							</div>
							<div class="modal-body">
								<div class="modal-center">
								<img src="/assets/images/content/buy/kidssize.png">
								</div>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				<div class="modal fade" id='modalbtn-04'>
					<div class="modal-dialog-800">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">創造更多，得到更多</h4>
							</div>
							<div class="modal-body">
								<a href='http://goo.gl/5IVDHw'><img src="/assets/images/banner/btn-04.png"></a>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				<div id='header' class='row'>
					<div id='logo' class='col-md-12'>
						<a href='http://getmore.cc'><img src='/assets/images/getmore.png' /></a>
					</div>
					<div class='col-md-7'>
<!--						<ul class='nav navbar-nav noimgtags'>-->
						<ul class='nav navbar-nav tags'>
<!--							<li onclick="location.href='/sellorder/info/dannyhurryup/';" style="cursor: pointer">→ 好衣服，預備開始!!!</li>-->
<!--							<li onclick="location.href='/clothing/category';" style="cursor: pointer">NEW</li>-->
							<li onclick="location.href='/';" style="cursor: pointer">首頁</li>
							<li><a href="/clothing">買衣服首頁(開發中)</a></li> 

						</ul>
					</div>
					<div id='header_right_section' class='col-md-5'>
						<ul class='nav navbar-nav tags'>
							<li onclick="location.href='/sellorder/info/sellintro/';" style="cursor: pointer">新手上路</li>
						<ul class='nav navbar-nav redtags'>	
							<li onclick="location.href='/sellorder/steps/1';" style="cursor: pointer">賣衣服</li>
						</ul>
							<li onclick="location.href='/sellorder/info/intro/';" style="cursor: pointer">About</li>
						</ul>
					</div>
				</div>

			</div><!-- header -->
			<div class='main'>
            	<?php if (isset($is_home)): ?>
				<div id="main-carousel" class="carousel slide" data-ride="carousel">
					<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<div class="item active">
							<a href='http://goo.gl/lsCFZY'><img src="/assets/images/banner/banner-01.jpg"></a>
						</div>
						<div class="item">
							<a data-toggle="modal" href="#modalbtn-04"><img src="/assets/images/banner/banner-02.jpg"></a>
						</div>
						<div class="item">
							<a href='http://goo.gl/5IVDHw'><img src="/assets/images/banner/banner-03.jpg"></a>
						</div>
						<div class="item">
							<a href='http://goo.gl/3fASiE' target="_blank"><img src="/assets/images/banner/banner-04.jpg"></a>
						</div>
						<div class="item">
							<a href='http://goo.gl/JSbCYR'><img src="/assets/images/banner/banner-05.jpg"></a>
						</div>
						<div class="item">
							<a href="http://goo.gl/gxsJt9"><img src="/assets/images/banner/banner-06.jpg"></a>
						</div>
					</div>

					<!-- Controls -->
					<a class="left carousel-control" href="#main-carousel" data-slide="prev">
						<span class="icon-prev"></span>
					</a>
					<a class="right carousel-control" href="#main-carousel" data-slide="next">
						<span class="icon-next"></span>
					</a>
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#main-carousel" data-slide-to="0" class="active"></li>
						<li data-target="#main-carousel" data-slide-to="1"></li>
						<li data-target="#main-carousel" data-slide-to="2"></li>
						<li data-target="#main-carousel" data-slide-to="3"></li>
						<li data-target="#main-carousel" data-slide-to="4"></li>
						<li data-target="#main-carousel" data-slide-to="5"></li>
					</ol>
				</div>
                <?php endif;?>

                <?php if (isset($is_home)): ?>
             <div class="homead">
				<div class="panel-body">
					<div class="homead-box">
					<div class="col-md-4">
	            		<a href="http://goo.gl/PBdiu2" target="_blank"><img src="/assets/images/ad/ad-01.png" alt="">
	          			</div>	
						
						<div class="col-md-4">
	            		 <a data-toggle="modal" href="#videomodel"><img src="/assets/images/ad/ad-02.png" alt=""></a>
	          			</div>		
						
						<div class="col-md-4">
	            		  <a href="http://goo.gl/6shD7v" target="_blank"><img src="/assets/images/ad/ad-03.png" alt=""></a>
	          			</div>	
					</div>
				</div>
			</div>
			<div class="homead-text"><h3>使用GetMore二手衣代售服務，<br>只要上網填單，GetMore通通幫你搞定！</h3></div>
			<img src="/assets/images/ad/sellprocess-home.jpg" alt="">
			<div class="sellstart"><p class="text-center"><a href="http://bit.ly/190TqRp"><img src="/assets/images/ad/sellstart.png"></a></p></div>

                <?php endif;?>

            <?php if (isset($is_home)===false) echo $content; ?>
            <?php //if (isset($is_home) || isset($show_foot)): ?>
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="panel-box">
					<div class="col-md-2">
						<h5>關於GetMore</h5>
						<div class="panel-link">
							<a href="/sellorder/info/intro/">
							<h6>GetMore簡介</h6>
							</a>
							<a href="/sellorder/info/energy/">
							<h6>GetMore團隊</h6>
							</a>
							<a href="/sellorder/info/idea/">
							<h6>GetMore理念</h6>
							</a>
							<a href="/sellorder/info/coper/">
							<h6>合作夥伴</h6>
							</a>
						</div>
					</div>
					<div class='col-md-2'>
						<h5>開始GetMore</h5>
						<div class="panel-link">
							<a href="/sellorder/info/sellintro/">
							<h6>我要賣衣服</h6>
							</a>
							<a href="/sellorder/info/buyintro/">
							<h6>我要買衣服</h6>
							</a>
							<a href="/sellorder/info/washintro/">
							<h6>保證嚴格清洗</h6>
							</a>
							<a href="/sellorder/info/userpolicy/">
							<h6>使用條款</h6>
							</a>
							<a href="/sellorder/info/sellpolicy/">
							<h6>賣衣服細節</h6>
							</a>
						</div>
					</div>
					<div class='col-md-2'>
						<h5>購買說明</h5>
						<div class="panel-link">
							<a href="/sellorder/info/paynfeepolicy/">
							<h6>付款與運費</h6>
							</a>
							<a href="/sellorder/info/deliverintro/">
							<h6>配送方式</h6>
							</a>
							<a href="/sellorder/info/returnpolicy/">
							<h6>關於退貨</h6>
							</a>
						</div>
					</div>
					<div class='col-md-2'>
						<h5>和世界一起</h5>
						<div class="panel-link">
							<a href="/sellorder/info/getmorelove/">
							<h6>GetMore Love</h6>
							</a>
							<a href="#">
							<h6>超級VIP(準備中)</h6>
							</a>
						</div>
					</div>
					<div class='col-md-2'>
						<h5>客服中心</h5>
						<div class="panel-link">
							<a href="/sellorder/info/qalist/">
							<h6> Q&A </h6>
							</a>
							<h4> Contact us <br> service@getmore.cc </h4>
							<a href="http://on.fb.me/1e3k3Up" target="_blank"><img src="/assets/images/likeicon/fb.png" alt="" class='panel-like'/></a>
							<a href="http://www.youtube.com/channel/UCLQtRAJ28v2e-ymeKJR-yXw/videos/" target="_blank"><img src="/assets/images/likeicon/youtube.png" alt="" class='panel-like'/></a>
							<a href="http://bit.ly/1g9Jt4t" target="_blank" ><img src="/assets/images/likeicon/blog.png" alt="" class='panel-like'/> </a>
						</div>
					</div>
					</div>
				</div>
				<div class="panel-footer">
					<hr>
					<p>Copyright © 2013 GetMore.cc INC.</p> <p>All rights reserved.</p>
				</div>
			</div>
            <?php //endif;?>
		</div>
        <?php echo $scripts_footer; ?>
		<?php (isset($debug)) && _log($_SESSION);?>
		</body>
	</html>

