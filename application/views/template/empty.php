<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--><html class="no-js" lang="<?php echo $lang; ?>"> <!--<![endif]-->
<head>
<meta charset="<?php echo $meta_charset; ?>">
<title><?php echo $site_title; ?></title>
<meta name="description" content="<?php echo $site_description; ?>" />
<meta name="keywords" content="<?php echo $site_keywords; ?>" />
<?php echo $meta_tag; ?>
<!-- CSS -->
<link rel="shortcut icon" href="favicon.ico" />
<!-- hide this due to the weird code issue. this may affect the sellorder step4 i guess.
<link rel="stylesheet" type="text/css" href="/assets/css/screen.css" media="screen, projection" />
-->
<link rel="stylesheet" type="text/css" href="/assets/css/slideshow.css" media="screen, projection" />
<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css" media="screen, projection" />

<!--[if IE]>
    <link href="/assets/css/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
<![endif]-->
<?php echo $styles; ?>
<!-- JS -->
<?php echo $scripts_header; ?>

<!--[if lt IE 9]>
    <link rel="stylesheet" href="assets/css/ie.css" type="text/css" media="screen" />
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script type="text/javascript">
var getmore = getmore || {}; // Backbone namespace
var root_path = '.';
</script>
<script data-main="/assets/javascript/main" src="/assets/javascript/libs/require/require.js"></script>
</head>
<body>
<?php echo $content; ?>
<?php echo $scripts_footer; ?>
<?php (isset($debug)) && _log($_SESSION);?>
</body>
</html>
