<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $meta_charset; ?>"/>
		<title><?php echo $site_title; ?></title>
    <meta name="viewport" content="width=1024">
    <meta name="description" content="<?php echo $site_description; ?>" />
    <meta name="keywords" content="<?php echo $site_keywords; ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php echo $meta_tag; ?>
      <meta name="format-detection" content="telephone=no">
    <!-- add meta tag added by howtomakeaturn -->
    <meta property="fb:app_id" content="239678122794926">
    <meta property="og:url" content="<?php echo current_url() ?>" />
    <?php if (( $this->uri->segment(1) === 'closet' ) ): ?>
        <meta property="og:image" content="<?php echo $closet['profile_pic_url_big'] ?>" />
        <meta property="og:title" content="<?php echo $closet['title'] ?>" />
        <meta property="og:description" content="<?php echo $closet['description'] ?>" />
    <?php elseif (( $this->uri->segment(1) === 'clothing' ) && ( $this->uri->segment(2) === 'view' ) ): ?>
        <meta property="og:image" content="<?php echo clothing_image_url(explode(',', $clothing['files'])[0], 'large') ?>" />
        <meta property="og:title" content="<?php echo $clothing['product_title'] ?>" />
        <meta property="og:description" content="<?php echo $site_description ?>" />
    <?php elseif (( $this->uri->segment(1) === 'blog' ) && ( $this->uri->segment(2) === 'view' ) ): ?>
        <meta property="og:image" content="<?php echo $article->feature_pic_path ?>" />
        <meta property="og:title" content="<?php echo $article->title ?>" />
        <meta property="og:description" content="<?php echo $article->getIntro() ?>" /> 
    <?php else: ?>
        <meta property="og:image" content="http://beta.getmore.cc/assets/images/fb-01.png" />
    <?php endif; ?>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">  
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/core.css">

		<link rel="shortcut icon" href="/assets/images/favicon.ico">
<!--    <link rel="stylesheet" href="/assets/css/typeahead.css">
		<link rel="stylesheet" href="/assets/css/bootstrap.css"/>
		<link rel="stylesheet" href="/assets/css/getmore_bootstrap.css"/>
		<link rel="stylesheet" href="/assets/css/home.css"/>-->
	<!--	<link rel="stylesheet" href="/assets/css/clothing.css"/> -->
		<link rel="stylesheet" href="/assets/css/checkout.css"/>
		<link rel="stylesheet" href="/assets/css/member.css"/>
		<link rel="stylesheet" href="/assets/css/sellorder.css"/>
    
    <!-- ugly structure by howtomakeaturn. it's a workaroud for managing javascript files
            with buying and selling pages.
     -->
    <?php if (($this->uri->segment(1)=='clothing')||($this->uri->segment(1)=='member')||($this->uri->segment(1)=='checkout')||($this->uri->segment(1)=='')||($this->uri->segment(1)=='info')){ ?>
        <script src="/assets/vendor/jquery/jquery.js"></script>
        <script src="/assets/javascript/bootstrap.js"></script>
        <script src="/assets/vendor/underscore-amd/underscore.js"></script>
        <script src="/assets/vendor/backbone-amd/backbone.js"></script>

        <script src="/assets/javascript/howtomakeaturn/backbone.localStorage.js"></script>
        <script src="/assets/javascript/libs/jquery/jquery.twzipcode-1.4.1.js"></script>
          
        <script src="/assets/javascript/libs/jquery/jquery.browser.js"></script>
        <script src="/assets/javascript/libs/jquery/jqzoom/js/jquery.jqzoom-core.js"></script>
        <link rel="stylesheet" href="/assets/javascript/libs/jquery/jqzoom/css/jquery.jqzoom.css" type="text/css">          
          
        <script src="/assets/javascript/howtomakeaturn/main.js"></script>
       <!-- <script src="/assets/javascript/howtomakeaturn/buyer_cart.js"></script> -->
        <script src="/assets/javascript/howtomakeaturn/jquery.lunZhuan.js"></script>
        <script src="/assets/javascript/howtomakeaturn/jquery.countdown.js"></script>
        <script src="/assets/javascript/howtomakeaturn/jquery.simplePagination.js"></script>
        <script src="/assets/javascript/howtomakeaturn/jquery.jeditable.js"></script>
        
        <script type="text/javascript">
            var getmore = getmore || {}; // Backbone namespace
            getmore.free_shipping = <?php echo $free_shipping ?>;
            var root_path = '.';
        </script>                
<!--<link rel="stylesheet" href="/assets/css/simplePagination.css"/>-->
    <?php } else if ( $this->uri->segment(1)=='closet' ) { ?>
        <script src="/assets/vendor/jquery/jquery.js"></script>
        <script src="/assets/javascript/bootstrap.js"></script>
        <script src="/assets/javascript/howtomakeaturn/jquery.form.min.js"></script>

        <script src="/assets/vendor/underscore-amd/underscore.js"></script>
        <script src="/assets/vendor/backbone-amd/backbone.js"></script>
        <script src="/assets/javascript/howtomakeaturn/backbone.localStorage.js"></script>
        <script src="/assets/javascript/howtomakeaturn/main.js"></script>

    <?php } else if ( $this->uri->segment(1)=='verify' ) { ?>
        <script src="/assets/vendor/jquery/jquery.js"></script>
        <script src="/assets/javascript/howtomakeaturn/jquery.cookie.js"></script>
        <script src="/assets/javascript/libs/jquery/bday-picker.min.js"></script>
        <script type="text/javascript" src="/assets/vendor/jquery-twzipcode/jquery.twzipcode.min.js"></script>
    <?php } else if ( $this->uri->segment(1)=='sell' ) { ?>
        <script src="/assets/vendor/jquery/jquery.js"></script>
        <script src="/assets/javascript/bootstrap.js"></script>
        <script src="/assets/javascript/howtomakeaturn/jquery.form.min.js"></script>
        <script type="text/javascript" src="/assets/vendor/jquery-twzipcode/jquery.twzipcode.min.js"></script>
        <script src="/assets/vendor/underscore-amd/underscore.js"></script>
        <script src="/assets/vendor/backbone-amd/backbone.js"></script>
        <script src="/assets/javascript/howtomakeaturn/backbone.localStorage.js"></script>
        <script src="/assets/javascript/sell/validator.js"></script>
        <script src="/assets/javascript/sell/account.js"></script>
        <script src="/assets/javascript/sell/model_and_collection.js"></script>
        <script src="/assets/javascript/sell/cart_view.js"></script>
        <script src="/assets/javascript/sell/order_view.js"></script>
        <script src="/assets/javascript/sell/main.js"></script>
    <?php }else{ ?>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="/assets/vendor/jquery/jquery.min.js"></script>
        <script src="/assets/javascript/bootstrap.js"></script>
        <script type="text/javascript">
            var getmore = getmore || {}; // Backbone namespace
            var root_path = '.';
        </script>        
<!-- <script data-main="/assets/javascript/main" src="/assets/vendor/requirejs/require.js"></script>-->
    <?php }//end else ?>


<script src="/assets/javascript/howtomakeaturn/search.js"></script>
  
  
<!-- <script src="/assets/javascript/bootstrap.js"></script>
	<script src='/assets/javascript/home.js'></script>-->
<!-- <script data-main="/assets/javascript/main-built" src="/assets/vendor/requirejs/require.js"></script>-->

<!-- new google analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46722397-1', 'getmore.cc');
  ga('send', 'pageview');

</script>
  
<!-- Facebook Conversion Code for 買衣服成交 -->  
  <?php if (( $this->uri->segment(1) === 'checkout' ) && ( $this->uri->segment(2) === 'fourth' ) ): ?>
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6020121664894', {'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6020121664894&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>

   <?php endif; ?>
<!--End 買衣服成交-->

<!-- Facebook Conversion Code for 買衣服結帳第一步 -->
   <?php if (( $this->uri->segment(1) === 'checkout' ) && ( $this->uri->segment(2) === 'first' ) ): ?>
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6020121730694', {'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6020121730694&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
             
   <?php endif; ?>
<!--End 買衣服結帳第一步-->        

        <!--Start of Zopim Live Chat Script-->
        <!--<script type="text/javascript">
            window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
            d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
            _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
            $.src='//v2.zopim.com/?2CzTS30m2Lrz7FqqNDx0kz5JG84zmbBd';z.t=+new Date;$.
            type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
        </script>-->
         <!--  End of Zopim Live Chat Script-->        

<!-- Facebook Conversion Code for 全網站 -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6022979096894', {'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6022979096894&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
<script>
var originalLeave = $.fn.popover.Constructor.prototype.leave;
$.fn.popover.Constructor.prototype.leave = function(obj){
  var self = obj instanceof this.constructor ?
    obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
  var container, timeout;

  originalLeave.call(this, obj);

  if(obj.currentTarget) {
    container = $(obj.currentTarget).siblings('.popover')
    timeout = self.timeout;
    container.one('mouseenter', function(){
      //We entered the actual popover – call off the dogs
      clearTimeout(timeout);
      //Let's monitor popover content instead
      container.one('mouseleave', function(){
        $.fn.popover.Constructor.prototype.leave.call(self, self);
      });
    })
  }
};

$(function () {
    $('#menu-popover').popover({
        trigger: 'hover click',
        delay:{show: 50, hide: 400},
        'placement': 'bottom',
        'show': true,
        'content': '<div class="popoverstyle"><a href="/clothing/category/new/">NEW / 最新上架</a> <br /> <a href="/clothing/category/women/">WOMEN / 女裝</a> <br /><a href="/clothing/category/men/">MEN / 男裝</a><br /> <a href="/clothing/category/kids/" target="_blank">KIDS / 童裝</a> </div>',
        'html': true
    });
})

</script>
<style>
.popoverstyle{
  font-size: 12px;
  padding:10px 0px; 
  width:120px;
  line-height:25px;
}
.popoverstyle a{
  color: #000000;
}
.popoverstyle a:hover, a:focus{
    text-decoration: underline;
}
.Headerbadge {
  top: -8px;
  margin-left: -5px;
  position: absolute;
  background-color: #dd0017;
  float: left;
}
.newnavbar{
  color:#fff; 
  margin-top:10px;
}

.newnavbar a{
  color:#fff;
  text-decoration:none;
}

.newnavbar a:hover, a:focus{
  color:#bbb;
}

.newnavbar .dropdown-menu .divider {
  height: 0px;
}
</style>
    </head>

	<body>
    <header id='newheader'>
     <div id='header-containerbg'>
      <div id='header-container'>
        <ul class="list-inline pull-right newnavbar">
          <? if ( in_array("Seller", $this->session->userdata('user_groups') ) ):?>
          <li class="dropdown">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <i class="fa fa-cog fa-lg"></i>&nbsp;<span class="caret" style="border-top:4px solid #fff;"></span></a>
          <ul class="dropdown-menu" role="menu" style="min-width:60px;border-radius:0px;">
            <li><a href="/marketplace/orderlist/2">我的訂單管理</a></li>
            <li><a href="/closet/my/marketplace">狂熱市集衣櫃</a></li>
            <li><a href="/marketplace/manage">狂熱市集管理平台</a></li>
          </ul>
          &nbsp;&nbsp;</li>
          |<? endif;?><li >&nbsp;&nbsp;&nbsp;&nbsp;<a href="/closet/my_favorite"><i class="fa fa-gratipay fa-lg"></i></a>&nbsp;&nbsp;&nbsp;</li>
          |<li class="dropdown">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user fa-lg"></i>&nbsp;<span class="caret" style="border-top:4px solid #fff;"></span></a>
          <ul class="dropdown-menu" role="menu" style="min-width:60px;border-radius:0px;">
            <li><a href="/member/order">購物訂單</a></li>
            <li><a href="/closet/my">我的衣櫃</a></li>
            <li><a href="/member/account">線上帳戶</a></li>
            <li><a href="/member/information">會員中心</a></li>
          </ul>
          &nbsp;&nbsp;</li>
          |<li style="position:relative;">&nbsp;&nbsp;&nbsp;&nbsp;<a href='/message/fin/box'><i class="fa fa-envelope fa-lg"><? if ( count_notReadPost() != 0 ): ?><span class="badge Headerbadge"><? echo count_notReadPost(); ?></span><? endif; ?>&nbsp;&nbsp;&nbsp;</i></a>
          |<li class="dropdown">&nbsp;&nbsp;&nbsp;<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-shopping-cart fa-lg"><span class="badge Headerbadge" id ="cart_badge_Headerbadge"></span></i></a>
          <ul class="dropdown-menu" role="menu" id="shopping-cart-menu" style="color:#000;">
            <li>
              <span>Loading ... </span>
            </li>
          </ul>
          &nbsp;&nbsp;&nbsp;</li>
          <?php if (($this->uri->segment(1)=='clothing')||($this->uri->segment(1)=='member')||($this->uri->segment(1)=='')||($this->uri->segment(1)=='closet')){ ?>
            <div id='checkout-modal' class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <center>
                        <div class="modal-header">
                            <h4 class="modal-title">結帳提醒</h4>                                          
                        </div>
                        <div class="modal-body">
                            <p>
                                請問是否確認過這些衣服尺寸有符合您的身型嗎？<br />
                                商品照片下方有詳細尺寸表。<br />
                                （愛護地球，避免退貨可節省不必要的浪費喔！）<br />
                            </p>
                            <p><span class="prmary-btn-a"><a class='btn btn-primary' href='/checkout/first'>我確認過了</a></span> <a class='btn btn-default' data-dismiss="modal">再檢查看看</a></p>
                        </div>
                    </center>
                </div>
              </div>
            </div>
          <?php } ?>
            <?php if(!$logined): ?>
                |<li>&nbsp;&nbsp;&nbsp;<a data-toggle="modal" href="#logInModal">login</a>&nbsp;&nbsp;&nbsp;</li>
            <?php else: ?>
                |<li>&nbsp;&nbsp;&nbsp;<a data-toggle="modal" href="/auth/logout?redirect_url=<?php echo urlencode(current_url()); ?>">logout</a>&nbsp;&nbsp;&nbsp;</li>
            <?php endif; ?>
        </ul>
       </div>
    </div>
      <div id="navigation">
        <div class='row logoarea'>
          <div class='pull-left'>
              <a href="/"><img src='/assets/images/logos/logogreen.png'></a>
          </div>
      <!--      <div class="pull-right logoarea-right">
            <a class="headhighlight" href='http://blog.getmore.cc/about-getmore/' target="_blank">什麼是GetMore?</a>
            <a class="headhighlight" href="http://blog.getmore.cc" target="_blank">GetMore時尚誌</a>
            <a class="headhighlight" href="/info/sellchoice" target="_blank">賣衣服</a>
          </div> -->
        </div>
      </div>
  <!--  <div class='container planpopover boxmenu'>
        <div class="boxmenu-btn">        
            <a href="/marketplace" class="btn btn-primary planbtn boxmenu-btn-a">狂熱市集 / Marketplace ></a>
        </div>-->
<!--        <div class="boxmenu-btn">
            <a href="/clothing/category/women" class="btn btn-primary planbtn boxmenu-btn-a planbtn-hightlight" data-toggle="popover" >品牌代售 / Brands only ></a>
        </div>  -->
    <!--    <div class="boxmenu-btn">
            <a href="/clothing/category/women" class="btn btn-primary planbtn boxmenu-btn-a planbtn-hightlight">品牌代售 / Brands only ></a>
        </div>
        <div class="boxmenu-btn" style="margin-left:0px; margin-right:0px;">
            <a href="/clothing/dsner" class="btn btn-primary planbtn boxmenu-btn-a">設計師品牌 / Designers ></a>
        </div>
      </div>
        <?php if ($this->uri->segment(2) !== 'mkplace' && $this->uri->segment(2) !== 'dsner'): ?>
        <div class='menuarea row font-size14' style="width:980px;margin:0 auto;">
            <ul class='headmenu-left font-size12'>
              <li><a href="/clothing/category/new/">NEW</a></li>
              <li><a href="/clothing/category/women/"><span class="red">WOMEN</span></a></li>
              <li><a href="/clothing/category/men/">MEN</a></li>
              <li><a href="/clothing/category/kids/" target='_blank'>KIDS</a></li>
              <li>|</li>
              <li><a href="http://blog.getmore.cc/category/getmore-lookbook/" target="_blank">LOOKBOOK人物誌</a></li>
            </ul>
            
            <ul class='headmenu-right'>
              <li style="margin-right:15px;"><input type="text" name="href" id="href" class="form-control head-search" placeholder="Search"></li>
              <li><img class="head-search-btn" src="/assets/images/search.png"></li>
            </ul>
        </div>
        <? endif; ?>
-->

<!--         <div class='menuarea row font-size14'>
            <ul class='headmenu-left font-size12'>
              <li><a href="/clothing/category/new/">NEW</a></li>
              <li><a href="/clothing/category/women/">WOMEN</a></li>
              <li><a href="/clothing/category/men/">MEN</a></li>
              <li><a href="/clothing/category/kids/" target='_blank'>KIDS</a></li>
              <li>|</li>
              <li><a href="http://blog.getmore.cc/">Blog</a></li>
            </ul>
            
            <ul class='headmenu-right'>
              <li class='headmenu-highlight'><a href="/info/sellchoice">賣衣服</a></li>

              <li>|</li>
              <li><a href="/clothing/category/new">買衣服</a></li>
              <li><input type="text" name="href" id="href" class="form-control head-search" placeholder="Search"></li>
              <li><img class="head-search-btn" src="/assets/images/search.png"></li>
            </ul>
        </div> -->
      </div>
    </header>

		<div class='container' style="margin-top:5px;">
				 <!-- <div class="header-bar">
					<ul class="right-menu">
                        <?php if (($this->uri->segment(1)=='clothing')||($this->uri->segment(1)=='member')||($this->uri->segment(1)=='')||($this->uri->segment(1)=='closet')){ ?>

                          <div id='checkout-modal' class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                              <div class="modal-content">
                                  <center>
                                      <div class="modal-header">
                                          <h4 class="modal-title">結帳提醒</h4>                                          
                                      </div>
                                      <div class="modal-body">
                                          <p>
                                              請問是否確認過這些衣服尺寸有符合您的身型嗎？<br />
                                              商品照片下方有詳細尺寸表。<br />
                                              （愛護地球，避免退貨可節省不必要的浪費喔！）<br />
                                          </p>
                                          <p><a class='btn btn-primary' href='/checkout/first'>我確認過了</a> <a class='btn btn-default' data-dismiss="modal">再檢查看看</a></p>
                                      </div>
                                  </center>
                              </div>
                            </div>
                          </div>

                          
                          <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                              <img class='navbar-icon' src="/assets/images/home20/cart.png"> 購物車
                            </a>
                            <ul class="dropdown-menu" id="shopping-cart-menu">
                              <li>
                                <span>Loading ... </span>
                              </li>
                            </ul>
                          </li>
                          

                          <?php } ?>
                        
                          <?php if(!$logined): ?>
                              <li><a data-toggle="modal" href="#logInModal"><img src="/assets/images/home20/login.png" alt="" id='log_in'/> login</a></li>
                          <?php else: ?>
	                       	  <li><a href="/closet/my"><img class='navbar-icon' src="/assets/images/home20/mycloset.png"> 我的衣櫃</a></li> 
                              <li><a href="/member/order"><img class='navbar-icon' src="/assets/images/home20/vipcenter.png"> 會員中心</a></li>                         
                              <li><a data-toggle="modal" href="/auth/logout?redirect_url=<?php echo urlencode(current_url()); ?>"><img src="/assets/images/home20/logout.png" alt="" id='log_in'/> logout</a></li>
                          <?php endif; ?>
					</ul>
				</div>	-->				
					<!-- modals -->
				<div class="modal fade" id='signUpModal'>
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">註冊會員</h4>
							</div>
							<div class="modal-body">
								<div class="fb_login">
									<h4>GetMore 是亞洲第一個二手衣時尚購物網站</h4>
									<a class='fb_login_btn btn btn-primary' href="<?php echo site_url('/auth/facebook?redirect_url=' . urlencode(current_url()))?>">用facebook登入</a> 快使用facebook快速登入!
									<hr class="dashed_hr"/>
								</div>
								<div id="getmore_login">
									<div class="row">
										<div class="col-md-4">
											<h4>註冊會員</h4> 
										</div>
										<div class="col-md-8 pull-right announcement getmore_instruction">
											還不是GetMore的會員嗎？ 免費加入！
										</div>
									</div>
									<form method="POST" action="/auth/register" name="dig_register">
										<div class="form-group">
											<label for="sign_up_email">信箱</label><div class='col-md-5 pull-right description'>此為您的會員登入帳號</div>
											<input type="email" class="form-control" id="email" name="email" placeholder="email">
										</div>
										<div class="form-group">
											<label for="nickname">暱稱</label> <div class='col-md-5 pull-right description'>14字元以內</div>
											<input type="text" class="form-control" id="user_name" name="user_name" placeholder="暱稱">
										</div>
										<div class="form-group">
											<label for="new_password">密碼</label>
											<input type="password" class="form-control" id="new_password" name="password" placeholder="Password">
										</div>
										<div class="form-group">
											<label for="password_confirm">確認密碼</label>
											<input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm Password">
										</div>
										<div class="form-group">
											<input type="checkbox" value="1" name="aggree" style="width:20px;">我已閱讀，並同意<a href="/info/buy#buypolicy">GetMore 服務條款</a>
										</div>
										<a id='start_to_getmore' href="javascript:document.forms['dig_register'].submit();" class='btn btn-primary'>開始GetMore</a>
										<div id='registration' class="col-md-4 pull-right"> 
											已是會員？ <a id='logIn' href="#logInModal" data-toggle="modal" onClick='$("#signUpModal").modal("hide");'>由此登入！</a>
										</div>
									</form>
								</div>
							</div>
							<div class="modal-footer">
								<p class='announcement text-center'>已成為「會員」或是點擊「用facebook 登入」按鈕的同時,<br>表示您已詳讀並同意GetMore相關的<a href="/info/buy#buypolicy" target='_blank'>購買條款</a>以及<a href="/info/sell#sellpolicy" target='_blank'>代洗代售服務條款</a>。</p>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				<div class="modal fade" id='logInModal'>
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">會員入口</h4>
							</div>
							<div class="modal-body">
								<div class="fb_login">
									<h4>GetMore 是亞洲第一個二手衣時尚購物網站</h4>
									<a class='fb_login_btn btn btn-primary' href="<?php echo site_url('/auth/facebook?redirect_url=' . urlencode(current_url()))?>">用facebook登入</a> 快使用facebook快速登入!
									<hr class="dashed_hr"/>
								</div>
								<div id="getmore_login">
									<div class="row">
										<div class="col-md-4">
											<h4>會員登入</h4> 
										</div>
										<div class="getmore_instruction col-md-8 pull-right announcement">
											若已是GetMore的會員,請在此登入!
										</div>
									</div>
									<form name="login-form" action="/auth/login" method="POST">
										<div class="form-group">
											<label for="email">信箱</label>
											<input type="email" class="form-control" id="email" name="identity" placeholder="email">
										</div>
										<div class="form-group">
											<label for="password">密碼</label> <a target='_blank' id='forget_password' class="pull-right" href="/auth/forget_password">忘記密碼？</a>
											<input type="password" name="password" class="form-control" id="password" placeholder="Password">
										</div>
										<a id='start_to_getmore' href="javascript:document.forms['login-form'].submit();" class='btn btn-primary'>開始GetMore</a>
 										<input type="hidden" name="redirect_url" value="<?php echo urlencode(current_url());?>" />
                                        <div id='registration' class="col-md-4 pull-right">
											還不是會員? <a id='register' href="#signUpModal" data-toggle="modal" onClick='$("#logInModal").modal("hide");'>來註冊!</a>
										</div>
									</form>
								</div>
							</div>
							<div class="modal-footer">
								<p class='announcement text-center'>已成為「會員」或是點擊「用facebook 登入」按鈕的同時,<br>表示您已詳讀並同意GetMore相關的<a href="/info/buy#buypolicy" target='_blank'>購買條款</a>以及<a href="/info/sell#sellpolicy" target='_blank'>代洗代售服務條款</a>。</p>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
          <script type="text/javascript">
            $(function() {
              $("form[name='login-form'] input").keypress(function(event){       
                if (event.keyCode == 13) $("form[name='login-form']").submit();
              });
            });
          </script>
				</div><!-- /.modal -->

        <!-- move this to clothing/index.php
				<div class="modal fade" id='videomodel'>
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">GetMore 理念</h4>
							</div>
							<div class="modal-body">
								<iframe src="//player.vimeo.com/video/74211715" width="580" height="422" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
							</div>
							<div class="modal-footer">
								<p class='announcement'>GetMore二次時尚，重新定義衣服的價值。</p>
							</div>
						</div>
					</div>
				</div>
        -->
				<div class="modal fade" id='modalwomen'>
					<div class="modal-dialog-800">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">尺寸參考表-WOMEN</h4>
							</div>
							<div class="modal-body">
								<div class="modal-center">
								<img src="/assets/images/content/buy/womensize.png">
								</div>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				<div class="modal fade" id='modalmen'>
					<div class="modal-dialog-800">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">尺寸參考表-MEN</h4>
							</div>
							<div class="modal-body">
								<div class="modal-center">
								<img src="/assets/images/content/buy/mensize.png">
								</div>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				<div class="modal fade" id='modalkids'>
					<div class="modal-dialog-800">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">尺寸參考表-KIDS/BABY</h4>
							</div>
							<div class="modal-body">
								<div class="modal-center">
								<img src="/assets/images/content/buy/kidssize.png">
								</div>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				<div class="modal fade" id='modalbtn-04'>
					<div class="modal-dialog-800">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">創造更多，得到更多</h4>
							</div>
							<div class="modal-body">
								<a href='http://getmore.cc/sellorder/info/sellpolicy/'><img src="/assets/images/banner/btn-04.png"></a>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				<!-- modals end -->

        <?php if ($this->uri->segment(2)!=='getmorex'): ?>
				<!-- header start -->
				<?php /*<div id='header' class='row'>
				<div class='row logoarea'>
					<div class='pull-left'>
						<a href="/"><img src='/assets/images/home20/logo.png'></a>
					</div>
					<div class="pull-right logoarea-right">
						<strong><a href='/clothing/suggested'>官方，強力推薦</a>
						<a href="http://bit.ly/1cHbuPX">網路二手衣代售服務</a></strong>
					</div>
				</div>
						<hr class='pokai-hr'>
				<div class='menuarea row font-size12'>
						<ul class='headmenu-left'>
							<li><a href="/clothing/category/new/">NEW</a></li>
							<li><a href="/clothing/category/women/">WOMEN</a></li>
							<li><a href="/clothing/category/men/">MEN</a></li>
							<li><a href="/clothing/category/kids/">KIDS</a></li>
						</ul>
						
						<ul class='headmenu-right'>
							<li><a href="/info/sell#howtosell">新手上路</a></li>
							<li class='headmenu-highlight'><a href="/info/startsell">賣衣服</a></li>
							<li><a href="http://blog.getmore.cc" target="_blank">Blog</a></li>
						</ul>
				</div>
				</div><!-- header --> */ ?>
        <?php else: ?>
        <a href='/clothing/getmorex/index'><img id='getmorex_logo' src='/assets/images/getmorex/GetMoreX.png' /></a>
        <header>
            <a href='/clothing/getmorex/index'><img id='fashion_collection' src='/assets/images/getmorex/FashionCollection.png'></a>
            <div id='search_container'>
                <input type='txt' placeholder='找品牌' class='form-control' id='search_input' />
                <button class='btn btn-primary' id='search_button'>確定</button>
            </div>        
            <h6>Famous Brand & Designer Sytle</h6>
            <div class='stretch' id='hr_stretch'>
                <hr />
            </div>
            <div id='hr_shadow'>
                <!-- this div is overlaid by the banner. notice the banner is in absolute position -->
            </div>            
            
            <ul class='getmorex_nav'>
                <li class='nav_category'><a href='/clothing/getmorex/new'>New</a></li>
                <li class='nav_category' id='hoverme'>
                  <a href='#' style='cursor: default;'>Women</a>
                  <ul class='women_dropdown'>
                      <li ><a target='_blank' href='/clothing/category/women/#type-women&page-1&keyword-iROO&subtype-1'>iROO</a></li>
                      <li ><a target='_blank' href='/clothing/category/women/#type-women&page-1&keyword-Arnold Palmer&subtype-1'>ARNOLD PALMER</a></li>
                      <li ><a target='_blank' href='/clothing/category/women/#type-women&page-1&keyword-OZOC&subtype-1'>OZOC</a></li>
                      <li ><a target='_blank' href='/clothing/category/women/#type-women&page-1&keyword-SISLEY&subtype-1'>SISLEY</a></li>
                      <li ><a target='_blank' href='/clothing/category/women/#type-women&page-1&keyword-U'db&subtype-15'>U'db</a></li>
                      <li ><a target='_blank' href='/clothing/category/women/#type-women&page-1&keyword-Mango&subtype-1'>Mango</a></li>
                      <li ><a target='_blank' href='/clothing/category/women/#type-women&page-1&keyword-得華新意&subtype-1'>得華新意</a></li>
                  </ul>       
                  
                </li>
                <li class='nav_other'><a href='/sellorder/steps/1'>Sell</a></li>
                <li class='nav_other'><a href='/clothing/getmorex/aboutx'>About <img src='/assets/images/getmorex/aboutX.png' /></a></li>
                <li class='nav_other'><a href='/'>Home</a></li>
            </ul>                        
        </header>

        <script>
            $(document).ready(function(){
                $('#search_button').click(function(){
                    var keyword = $('#search_input').val();
                    if (keyword != ''){
                        var location = '/clothing/category/women/#keyword-' + keyword;
                        //window.location = location;
                        window.open(location, '_blank');
                        
                    }
//                    alert(keyword);
                });
                $("#search_input").keyup(function (e) {
                    if (e.keyCode == 13) {
                        var keyword = $('#search_input').val();
                        if (keyword != ''){
                            var location = '/clothing/category/women/#keyword-' + keyword;
                            //window.location = location;
                            window.open(location, '_blank');
                        }
                    }
                }); 
                
                $('.women_dropdown').hide();                
                $('#hoverme').hover(function(){
                    $('.women_dropdown').fadeIn();
                }, function(){
                    $('.women_dropdown').fadeOut();
                });
                
                               
            });
        </script>
        <?php endif; ?>

			<div class='main'>
        
<?php if ($this->uri->segment(1)=='closet'): ?>

<div class='closet-top'>
        <div class='closet-profile-photo'>
            <div style='width: 165px; height: 165px; overflow: hidden;'>
                <img style='width: 165px;' src='<?php echo $closet['profile_pic_url'] ?>' />
            </div>
            <?php if ($is_owner): ?>
                <button class='btn btn' onclick='$("#profile-photo-file").click();'>更換照片</button>
                <form action='/restful/closet/file/<?= $closet['user_id']; ?>' method='POST' enctype="multipart/form-data">
                    <input type='hidden' name='type' value='profile' />
                    <input type='file' name='userfile' id='profile-photo-file' style='display: none;' />
                </form>
            <?php endif; ?>
        </div>
        
        <div class='closet-profile-title'>
            <div>
                <?php echo $closet['title'] ?>
<!--            <?php if ($is_owner): ?>
                    <button class='btn btn-xs' style='display: none;' onclick="$('#edit-title-modal').modal('show');">編輯</button>
                <?php endif; ?>
-->            
            </div>
        </div>
        <div class='closet-profile-description'>
            <div>
                <?php echo $closet['description_tag'] ?>
 <!--   <?php if ($is_owner): ?>            
             <button class='btn btn-xs' style='display: none;' onclick="$('#edit-description-modal').modal('show');">編輯</button>
        <?php endif; ?>
-->
            </div>
        </div>
<!--        <div class="follower_count" style="position: absolute;top: 0;right: 0;">
          有<?= $closet['follower_count'];?>人關注
        </div>
-->
  <div class="closet-share-button">
        <?php if ($is_owner): ?>
        <div class="btn-group">
        <a data-toggle="modal" href="#edit-description-modal">
          <button class='btn btn-primary'>編輯敘述</button>
        </a>
        </div>
        <?php endif; ?>
        <?php if ($this->uri->segment(2)=='view' || $this->uri->segment(2) == 'marketplace'): ?>
          <?php if(!$is_owner):?>
          <?php if($closet['had_followed']):?>
        <div class="btn-group">
          <a target='_self' href='/closet/disfollow/<?=$closet['user_id']+9881;?>'>
            <button class="btn btn-default">
              取消追蹤
            </button>
          </a>
        </div>
          <?php else: ?>
        <div class="btn-group">
          <a target='_self' href='/closet/follow/<?=$closet['user_id']+9881;?>'>
            <button class="btn btn-primary">
              追蹤
            </button>
          </a>
        </div>
          <?php endif;?>
          <?php endif;?>
        <div class="btn-group">
        <a target='_blank' href='http://www.facebook.com/sharer.php?u=<?php echo current_url() ?>'>
          <button class='btn btn-primary'>
            分享
          </button>
        </a>        


        <?php if ( !$this->session->userdata('user_id') ): ?>
        <a data-toggle="modal" href="#logInModal"><button type="button" class="btn btn-primary btn-m">留言</button></a>
        <?php endif;?>

        <?php if ( $this->session->userdata('user_id') && !$is_owner): ?>
        <?= post_user_message_modal_for_layout_without_link_bootstrap($closet['user_id'], current_url());?>

        <a data-toggle="modal" href="#userpostmsg"><button type="button" class="btn btn-primary btn-m">留言</button></a>
        <script>
          $('#post_submit').click(function(event){
            event.preventDefault();
            var actionUrl = "/restful/post";

            var tempData, i , tempReceiver;
            tempReceiver = $( '#owner_id' ).val();
            //$( '#receiver' ).val( "999" );
            var tempData = $( '#message_post' ).serialize();

//////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////// check content
   var message_title = ($('#message_title').val()).replace(/\s/gm,'');
   var message_content = ($('#message_content').val()).replace(/\s/gm,'');
   document.getElementById("message_title").style.background = ( message_title == "" ) ? "#FFFFCC" : "#FFFFFF";
   document.getElementById("message_content").style.background = ( message_content == "" ) ? "#FFFFCC" : "#FFFFFF";

   var alert_message = "系統偵測到可能是私下交易的文字。為了交易安全以及平台的權益，嚴禁私下交易喔！";
   var re = /[0-9]{6,}|電話|私下交易|面交|手續費|[零一二三四五六七八九]{6,}|[零壹貳參肆伍陸柒捌玖]{6,}|直接跟你買|直接聯絡|傳真|e[-]{0,1}mail|E[-]{0,1}mail|Line|line|一手交錢一手交貨|歡迎來店選購/;
   if (re.test(message_title) || re.test(message_content)){
    alert( alert_message );
    return;
   }

   re =/^([a-z0-9_\.-]+\@[\da-z\.-]+\.[a-z\.]{2,6})$/gm; // mail
   if (re.test(message_title) || re.test(message_content)){
    alert( alert_message );
    return;
   }
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

            var formData;
                formData = new FormData(document.getElementById("message_post"));

                $.ajax({
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    url: actionUrl,
                    data: formData,
                    dataType: 'json'
                }).done(function(post){
                    formData.append("post_id", post['_id']);

                    $.ajax({
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        url: "/restful/post/message",
                        data: formData,
                        dataType: 'json'
                    }).done(function(post){
                        $.ajax({
                            type: 'POST',
                            url: '/post/change_message_value',
                            data: {post_id: post['post_id'], read: 'true'},
                            dataType: 'json'
                         });
                        console.log("success");
                        location.reload();
                        // form.reset();
                        // var html = message_template(post);
                        // $(form).before(html);
                    }).fail(function(error){
                        console.log(error);
                    });

                    console.log();
                }).error(function(){
                    console.log("error");
                });
          });
        </script>
        <?php endif; ?> 

        <?php endif; ?>        
        </div>
  </div>
</div> <!--END closet-top-->
        <!-- 衣櫃bar-->
    <div class="closetbar">
        <ul id="closetnav">
          <li class="rel_btn" id="rel_follow">追蹤: <span><?= $closet['follow_count'];?></span></li>
          <li class="rel_btn" id="rel_follower">粉絲: <span><?= $closet['follower_count'];?></span></li>
          <li>減少CO2排放: <span><?= $co2_amount; ?></span></li>
          <!-- <li class="views">衣櫃瀏覽次: <span><?= $closet['view_count']; ?></span></li> -->
        </ul>
    </div>



<script src="/assets/javascript/howtomakeaturn/clothing.js"></script>


<div class='closet-nav-box'>
    <ul class="nav nav-tabs">
        <li class='closet-li-view <?php if ($this->uri->segment(2)=='view'): ?>active<? endif; ?>'><a href='/closet/view/<?php echo $closet['url_code'] ?>'>品牌代售衣櫃</a></li>
        <li class='closet-li-view <?php if ($this->uri->segment(2)=='marketplace'): ?>active<? endif; ?>'><a href='/closet/marketplace/<?php echo $closet['url_code'] ?>'>狂熱市集衣櫃</a></li>
        <?php if($is_owner): ?><li class='closet-li-favorite <?php if ($this->uri->segment(2)=='my_favorite'): ?>active<? endif; ?>'><a href='/closet/my_favorite'>我的欲望清單</a></li><? endif; ?>
        <?php if($is_owner): ?><li class='closet-li-advertise <?php if ($this->uri->segment(2)=='advertise'): ?>active<? endif; ?>'><a href='/closet/advertise'>宣傳我的衣櫥</a></li><? endif; ?>
        <?php if($is_owner): ?><li class='closet-li-followlist <?php if ($this->uri->segment(2)=='followlist'): ?>active<? endif; ?>'><a href='/closet/followlist'>我的關注對象</a></li><? endif; ?>
    </ul>
</div> 
    <script>
        $(document).ready(function(){
            $('.closet-profile-photo').hover(function(){
                console.log('in');
                $('.closet-profile-photo button').show();
            }, function(){
                console.log('out');
                $('.closet-profile-photo button').hide();
            });
            
            $('.closet-banner-photo').hover(function(){
                console.log('in');
                $('.closet-banner-photo button').show();
            }, function(){
                console.log('out');
                $('.closet-banner-photo button').hide();
            });
            
            $('.closet-profile-title').hover(function(){
                console.log('in');
                $('.closet-profile-title button').show();
            }, function(){
                console.log('out');
                $('.closet-profile-title button').hide();
            });

            $('.closet-profile-description').hover(function(){
                console.log('in');
                $('.closet-profile-description button').show();
            }, function(){
                console.log('out');
                $('.closet-profile-description button').hide();
            });
            

            // initialize the jquery form plugin for sending ajax with input file
            $('#banner-photo-file').parent().ajaxForm(function(res) { 
                var item = $.parseJSON(res).upload_data;
                console.log(item);
                var params = { file_id: item.id, photo_type: 'banner' }
                post_to_url('/closet/change_pic', params);
            });
            
            $('#banner-photo-file').change(function(){
                if ($(this).val() == "") {
                    return;
                }
                $(this).parent().submit();            
                $(this).val("");
            })
            
            // initialize the jquery form plugin for sending ajax with input file
            $('#profile-photo-file').parent().ajaxForm(function(res) {
                var file_id = $.parseJSON(res);
                console.log(file_id);
                var params = { file_id: file_id, photo_type: 'profile' }
                post_to_url('/closet/change_pic', params);
            });
            
            $('#profile-photo-file').change(function(){
                if ($(this).val() == "") {
                    return;
                }
                $(this).parent().submit();            
                $(this).val("");
            })
            
            
        });
        
        function post_to_url(path, params, method) {
            method = method || "post"; // Set method to post by default if not specified.

            // The rest of this code assumes you are not using a library.
            // It can be made less wordy if you use one.
            var form = document.createElement("form");
            form.setAttribute("method", method);
            form.setAttribute("action", path);

            for(var key in params) {
                if(params.hasOwnProperty(key)) {
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", key);
                    hiddenField.setAttribute("value", params[key]);

                    form.appendChild(hiddenField);
                 }
            }

            document.body.appendChild(form);
            form.submit();
        }    
</script>

    <div class="modal fade" id='edit-title-modal'>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">編輯標題</h4>
          </div>
          <div class="modal-body">
            <div class="modal-center">
                <form action='/closet/change_text' method='post'>
                    <input type='hidden' name='type' value='title' />
                    <input type='text' class='form-control' name='content' maxlength='16' value='<?php echo $closet['title'] ?>' />
                    <input type='submit' class='btn btn-primary' value='確定' />
                </form>
            </div>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id='edit-description-modal'>
      <div class="modal-dialog" style="width:720px;">
        <div class="modal-content" style="height:500px;">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">編輯敘述</h4>
          </div>
          <div class="modal-body" style="padding-top:40px;">
            <div class="modal-center marginbottom30" style="height:110px;">
                <p>為您的衣櫃取一個標題，可以是暱稱或是一個吸引人的名字，限定16個字內。</p>
                <form action='/closet/change_text' method='post' style="width:340px; height:28px;">
                    <input type='hidden' name='type' value='title' />
                    <input type='text' class='form-control' name='content' maxlength='16' value='<?php echo $closet['title'] ?>' /><br>
                    <input type='submit' class='btn btn-primary' value='確定修改標題' />
                </form>
            </div>
            <div class="modal-center">
                <p>為您的衣櫃寫下一段敘述，可以是個人風格或是二手衣搭配理念，4行以內，輸入框大小即是顯示大小。</p>
                <form action='/closet/change_text' method='post' style="width:600px; height:78px;">
                    <input type='hidden' name='type' value='description' />
                    <textarea name='content' class='form-control' rows='3' maxlength="158" style="width:560px; height:104px;"><?php echo $closet['description'] ?></textarea><br>
                    <input type='submit' class='btn btn-primary' value='確定修改簡介' />
                </form>
            </div>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

<?php endif; // end closet ?>
        
  <?php if (isset($is_home)): ?>
				<div id="main-carousel" class="carousel slide" data-ride="carousel">
					<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<div class="item active">
							<a href='http://getmore.cc/clothing/category'><img src="/assets/images/banner/banner-00.jpg"></a>
						</div>
            <div class="item">
              <a href='/clothing/category/new/'><img src="https://dl.dropboxusercontent.com/u/13417710/newbanner.jpg"></a>
            </div>
						<div class="item">
							<a href='http://getmore.cc/sellorder/info/bigvip'><img src="/assets/images/banner/banner-01.jpg"></a>
						</div>
						<div class="item">
							<a href='http://getmore.cc/sellorder/info/sellintro/'><img src="/assets/images/banner/banner-02.jpg"></a>
						</div>
						<div class="item">
							<a data-toggle="modal" href="#modalbtn-04"><img src="/assets/images/banner/banner-03.jpg"></a>
						</div>
						<div class="item">
							<a href='http://getmore.cc/sellorder/steps/1'><img src="/assets/images/banner/banner-04.jpg"></a>
						</div>
						<div class="item">
							<a href='http://getmore.cc/sellorder/info/idea/'><img src="/assets/images/banner/banner-05.jpg"></a>
						</div>
						<div class="item">
							<a href="http://getmore.cc/sellorder/info/washintro/"><img src="/assets/images/banner/banner-06.jpg"></a>
						</div>
					</div>
					<!-- Controls -->
					<a class="left carousel-control" href="#main-carousel" data-slide="prev">
						<span class="icon-prev"></span>
					</a>
					<a class="right carousel-control" href="#main-carousel" data-slide="next">
						<span class="icon-next"></span>
					</a>
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#main-carousel" data-slide-to="0" class="active"></li>
						<li data-target="#main-carousel" data-slide-to="1"></li>
						<li data-target="#main-carousel" data-slide-to="2"></li>
						<li data-target="#main-carousel" data-slide-to="3"></li>
						<li data-target="#main-carousel" data-slide-to="4"></li>
						<li data-target="#main-carousel" data-slide-to="5"></li>
						<li data-target="#main-carousel" data-slide-to="6"></li>
					</ol>
				</div>
                <?php endif;?>

                <?php if (isset($is_home)): ?>

			<p class="clear"></p>
			<div class="homead-slogan"><h3>使用GetMore二手衣代售服務，<br>只要上網填單，GetMore通通幫你搞定！</h3></div>
			<img src="/assets/images/ad/sellprocess-home.jpg" alt="">

			<div class="sellstart"><p class="text-center"><a href="http://bit.ly/190TqRp"><img src="/assets/images/ad/sellstart.png"></a></p></div>

                <?php endif;?>

            <?php if ($this->uri->segment(1)=='member'){ ?>
                <img src='/assets/images/banner/banner-vipcenter.jpg' />
                <div class='container'>
                    <div class='row member-row'>
                        <div class='col-xs-3'>
                            <div class='member_sidebar_title'>
                                <img src='/assets/images/left.png' />
                                <span>會員中心</span>
                            </div>
                            <div class='member_sidebar'>
                                  <ul>
                                      <li><p><a href='/member/information'><button class="btn btn-primary btn-m" type="button">會員資料</button></a></p></li>
                                      <li><p><a href='/member/order'><button class="btn btn-primary btn-m" type="button">購物訂單</button></a></p></li>
                                      <li><p><a href='/member/account'><button class="btn btn-primary btn-m" type="button">我的帳戶</button></a></p></li>
                                      <li><p><a href='/member/clothing_management'><button class="btn btn-primary btn-m" type="button">販售管理</button></a></p></li>
                                      <li><p><a href='/member/sellorder'><button class="btn btn-primary btn-m" type="button">二手衣申請進度查詢</button></a></p></li>
                                      <li><p><a href='/closet/my'><button class="btn btn-primary btn-m" type="button">我的衣櫃</button></a></p></li>
                                  </ul>
                                  <hr>
									<div class='contactus'><p> Contact us <br> service@getmore.cc </p> </div>
                            </div>
                            
                            
                        </div>
                        <div class='col-xs-9'>
                              <?php echo $content ?>
                        </div>
                    </div>
                </div>

             <?php }else{ ?>

            <?php if (isset($is_home)===false) echo $content; ?>
             <?php } ?>
            <?php //if (isset($is_home) || isset($show_foot)): ?>	
	</div><!-- /.main -->
</div><!-- /.container -->	



<!--New footer-->

<div style="margin:0 auto;">
<footer id="newfootercontainer">
  <div class="footcontainer">
    <div id="newfooterbox">
      <div id="footerabout">
        <div class="footerabouttitle">
          關於 GetMore
        </div>
        <div class="footerabout" style="margin-top:10px;">
          <p>在 GetMore 直接向時尚達人購買精選的獨立品牌以及二手時尚單品！</p>
          <p>GetMore 是一個以「共享經濟」為概念的時尚分享平台，我們的使命是挖掘世界各地的搭配達人，以及獨立品牌的時裝設計師。在這，你可以透過 GetMore 的網站，發掘，購買時尚賣家供應的獨特搭配單品。<a href="http://blog.getmore.cc/about-getmore/" target="_blank">..more</a></p>
        </div>
      </div>
      <div>
        <div class="footerlinkbox" style="margin-left:80px;">
          <div class="footerabouttitle">
            發掘
          </div>
          <div class="footerabout" style="margin-top:10px;">
            <div class="footerlink"><a href="http://getmore.cc" target="_blank">HOME 購物首頁</a></div>
            <div class="footerlink"><a href="/info/sellchoice" target="_blank" style="color:#b31217">賣衣服</a></div>
            <div class="footerlink"><a href="http://blog.getmore.cc" target="_blank">GetMore Blog</a></div>
            <div class="footerlink"><a href="http://blog.getmore.cc/category/getmore-lookbook/" target="_blank">LookBook 人物誌</a></div>
            <div class="footerlink"><a href="http://blog.getmore.cc/websitemap/" target="_blank">網站地圖</a></div>
            <div class="footerlink"><a href="http://support.getmore.cc/knowledgebase/returnpolicy/" target="_blank">退貨政策</a></div>
          </div>
        </div>
        <div class="footerlinkbox" style="margin-left:10px;">
          <div class="footerabouttitle">
            公司
          </div>
          <div class="footerabout" style="margin-top:10px;">
            <div class="footerlink"><a href="http://blog.getmore.cc/about-getmore/" target="_blank">關於 GetMore</a></div>
            <div class="footerlink"><a href="http://blog.getmore.cc" target="_blank">工作機會</a></div>
            <div class="footerlink"><a href="http://blog.getmore.cc/2015-summer-intern/" target="_blank">夏季實習計畫</a></div>
            <div class="footerlink"><a href="http://blog.getmore.cc/publicrelations/" target="_blank">新聞室</a></div>
          </div>
        </div>
        <div class="footerlinkbox" style="margin-left:10px;">
          <div class="footerabouttitle">
            聯絡
          </div>
          <div class="footerabout" style="margin-top:10px;">
            <div class="footerlink"><a href="http://support.getmore.cc/" target="_blank">說明中心</a></div>
            <div class="footerlink"><a href="http://support.getmore.cc/faqpage/" target="_blank">常見問題</a></div>
            <div class="footerlink"><a href="http://support.getmore.cc/" target="_blank">聯絡我們</a></div>
            <div class="footerlink" style="margin-top:8px;">
              <a href="http://www.facebook.com/getmore.cc" target="_blank">
                <i class="fa fa-facebook-square fa-2x"  style="color:#3b5998;"></i>
              </a>
              <a href="https://instagram.com/getmorecc/" target="_blank"> 
                <i class="fa fa-instagram fa-2x" style="color:#3F729B;"></i>
              </a>
              <a href="https://www.youtube.com/channel/UCMFEkClqi18ge_EItQa0lAg/" target="_blank">  
                <i class="fa fa-youtube-square fa-2x" style="color:#b31217;"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="newfootercopyright">
      <hr style="margin:0 0 10px 0;">
      <div class="pull-left" style="margin-left:30px;"><i class="fa fa-cc-visa fa-2x"></i>&nbsp;&nbsp;<i class="fa fa-cc-mastercard fa-2x"></i>&nbsp;&nbsp;<i class="fa fa-credit-card fa-2x"></i></div>
      <div class="pull-right" style="font-size:10px;margin-right:30px;">Copyright © 2015 GetMore.cc INC. All rights reserved.</div>
    </div>
  </div>
</footer>
<!--END of footer -->
            <?php //endif;?>
        <?php echo $scripts_footer; ?>
		<?php (isset($debug)) && _log($_SESSION);?>

  	</body>
	</html>

