<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            body{
                background-color: #bde1D6;
            }
        </style>
    </head>
    <body>
        <br />
        <br />
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style='border-spacing: 2px;'>
            <tr>
                <td align="center">
                    <div class='main' style='width: 590px; background-color: white; text-align: left;'>
                        <a href="<?php echo site_url('/') ?>">
                            <img class='getmorecenter-block' src="<?php echo site_url('/assets/images/email/automail-01.png') ?>" />
                        </a>
                        <!--[if lt IE 8]>
                            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
                        <![endif]-->
                        
                        <!-- Add your site or application content here -->
                        <div style='padding: 20px;'>
                            <?php echo $content ?>
                        </div>
                        <a href="<?php echo site_url('/') ?>">
                            <img class='getmorecenter-block' src="<?php echo site_url('/assets/images/email/automail-02-02.png') ?>" />
                        </a>
                    </div>
                </td>
            </tr>
        </table>      
      

    </body>
</html>
