<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--><html class="no-js" lang="<?php echo $lang; ?>"> <!--<![endif]-->
<head>
<meta charset="<?php echo $meta_charset; ?>">
<title><?php echo $site_title; ?></title>
<meta name="description" content="<?php echo $site_description; ?>" />
<meta name="keywords" content="<?php echo $site_keywords; ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php echo $meta_tag; ?>
<!-- CSS -->
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="/assets/css/screen.css" media="screen, projection" />
<!--[if IE]>
    <link href="/assets/css/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
<![endif]-->
<?php echo $styles; ?>
<!-- JS -->
<?php echo $scripts_header; ?>
<!--[if lt IE 9]>
    <link rel="stylesheet" href="assets/css/ie.css" type="text/css" media="screen" />
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script type="text/javascript">
var getmore = getmore || {}; // Backbone namespace
var root_path = '.';
</script>
<script data-main="/assets/javascript/main-built" src="/assets/vendor/requirejs/require.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-26930222-1']);
  _gaq.push(['_setDomainName', 'getmore.cc']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div id="wrapper">
    <div id="header">
        <div id="tool-bar">
            <ul class="left-bar">
                <li><a class="home" href="/">getmore home</a></li>
                <li>
<!--
		<div id="search-box">
			<input type="text" onblur="if(this.value=='')this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue)this.value='';" name="q" autocomplete="on" value="Search" class="text" id="search-keyword">
			<a id="button-search" href="#"></a>
		</div>
-->
            	<div id="search-box">
                    <form id="searchform" action="/" method="get">
                	<input class="field" style="height:10px;" value="Search" name="s" id="s" onblur="if(this.value=='')this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue)this.value='';" autocomplete="on" type="text">
                	<input class="submit" name="submit" id="searchsubmit" value="搜尋" type="submit">
                    </form>
            	</div>
		</li>
            </ul>
            <ul class="right-bar">
                <?php if(!$logined): ?>
                <li id="login-area" class="login">
                    <a href="auth/login?redirect_url=<?php echo urlencode(current_url()); ?>" class="hover">Login</a>
                    <div class="login-area">
                        <div class="empty login-empty"></div>
                        <div class="panel-header login-header"></div>
                        <form name="login-form" action="/auth/login" method="POST">
                            <div class="login-panel">
                                <div id="login-region">
                                    <div class="register-message">
                                        <h2>會員登入</h2>
                                        <p><input type="checkbox"  id="remember" name="remember" value="1" <?php echo set_checkbox('remember', 1, TRUE); ?> />&nbsp;自動登入</p>
                                        <a id="fb-login" href="<?php echo site_url('auth/facebook?redirect_url=' . urlencode(current_url()))?>">
                                            <div id="fb-logo"></div>
                                            <p>帳號登入</p>
                                        </a>
                                    </div>
                                    <div class="register-box">
                                        <div class="account-box box-class">
                                            <label><span>信箱</span></label>
                                            <input type="text" tabindex="1" name="identity" value="" placeholder="Enter your Email" value="<?php echo set_value('identity');?>" />
                                        </div>
                                        <div class="password-box box-class">
                                            <div class="p-class">
                                                <span>密碼</span>
                                                <span class="repeat-p-class"><a href="#">（忘記密碼？）</a></span>
                                            </div>
                                            <div id="password-frame">
                                                <input type="password" tabindex="2" name="password" value="" placeholder="Enter your Password" />
                                            </div>
                                        </div>
                                        <div class="button-confirm">
                                            <button>登入</button>
                                            <input type="hidden" name="redirect_url" value="<?php echo urlencode(current_url());?>" />
                                        </div>
                                    </div>
                                    <div id="join-member"><a href="/auth/login?redirect_url=<?php echo urlencode(current_url());?>">（還沒有getmore帳號？）</a></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </li>
                <?php else: ?>
                <li class="login logout">
                    <div class="login-area">
                        <div class="empty login-block"></div>
                        <div class="panel-header login-header logout"></div>
                    </div>
                    <a href="auth/logout?redirect_url=<?php echo urlencode(current_url()); ?>" class="hover">Logout</a>
                </li>
                <li class="login">
                    <div class="login-area">
                        <div class="empty login-block"></div>
                        <div class="panel-header login-header"></div>
                    </div>
                    <a href="/auth/edit" class="hover">會員中心</a>
                </li>
                <?php endif; ?>
                <li id="shopping-cart-area" class="shopping">
                    <div class="cart-count"></div>
                    <div class="shopping-cart-area">
                        <div class="empty shopping-cart-empty"></div>
                        <div class="panel-header shopping-cart-header"></div>
                        <div class="shopping-cart">
                            <div class="cart-content">
                                <table id="shopping-car">
                                </table>
                                <div class="cart-note">您購買了 <span class="cart-count"></span> 樣商品，結帳前還可以修改數量喔！</div>
                                <div class="cart-button">
                                    <ul>
                                        <li class="checkout"><a href="/order/steps/1">結&nbsp;&nbsp;帳</a></li>
                                        <li class="close_menu"><a href="/subject/lists/all">繼續購物</a></li>
                                    </ul>
                                    <p class="clear"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li id="account-area" class="account"><a href=#></a></li>
                <li id="collection-area" class="collection"><a href=#></a></li>
            </ul>
        </div>
        <div id="logo-bar">
            <a class="logo" href="/">getmore logo</a>
            <ul>
		<!--
                <li class="activity <?php if (isset($is_home)): ?>active<?php endif; ?>"><a href="/"><span>首&nbsp;&nbsp;頁</span></a></li>
                <li class="activity <?php if (isset($is_subject) and isset($category_id) and $category_id == 'all'): ?>active<?php endif; ?>"><a href="/subject/lists/all"><span>所有商品</span></a></li>
		-->
                <?php foreach($product_category as $row): ?>
                <li class="activity <?php if (isset($is_subject) and isset($category_id) and $category_id == $row['id']): ?>active<?php endif; ?>"><a href="/subject/lists/<?php echo $row['id']?>"><span><?php echo $row['tw_name']?></span></a></li>
                <?php endforeach ?>
		<!--
                <li class="product active"><a href="/event/lists/all"><span>活&nbsp;&nbsp;動</span></a></li>
		-->
            </ul>
            <p class="clear"></p>
        </div>
        <?php if(!isset($is_hidden_header)): ?>
        <div id="slideshow">
            <div id="headline" class="slideShow">
                <div id="top-theme">
                    <div id="volti">
                        <div id="left" class="prev">
                            <a href="javascript:void(0);">&nbsp;</a>
                        </div>
                        <div id="right" class="next">
                            <a href="javascript:void(0);">&nbsp;</a>
                        </div>
                    </div>
                    <ul class="slides">
                        <?php foreach($top_image_list as $row): ?>
                        <li class="slide"><a href="<?php echo $row['link']; ?>" target="<?php echo $row['target']; ?>"><img src="<?php echo image_url($row['image'], 900, 360); ?>" border="0" alt="" /></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="slide_float">
                    <ol class="navigation topNavigation">
                        <?php for($i=0;$i<$top_image_count;$i++): ?>
                        <li><a href="javascript:void(0);" class="page">&nbsp;</a></li>
                        <?php endfor; ?>
                    </ol>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <p class="clear"></p>
        <?php if (isset($is_home)): ?>
        <div class="line"></div>
        <?php endif;?>
    </div>
    <?php echo $content; ?>
    <div id="footer">
        <div id="share-text"><span>follow us on </span></div>
        <div id="share-button">
            <a href="https://www.facebook.com/getmore.lab" class="facebook" target="_blank"></a>
            <a href="https://plus.google.com/u/0/b/109711111072717685426/109711111072717685426/posts" class="gplus" target="_blank"></a>
            <a href="https://twitter.com/getmorelab" class="twitter" target="_blank"></a>
        </div>
        <a href="#" id="back-top">Back to top</a>
        <ul id="gray-box">
            <li><a href="/about" id="about-we"><p>關於我們</p><span class="local-icon-star"></span><p>About</p></a></li>
            <li><a href="/how" id="how-to"><p>怎麼 getmore</p><div class="local-icon-star"></div><p>How to getmore</p></a></li>
            <li><a href="/terms" id="service"><p>服務條款</p><span class="local-icon-star"></span><p>Terms of Service</p></a></li>
            <li><a href="/contact" id="contac"><p>連絡我們</p><span class="local-icon-star"></span><p>Contact Us</p></a></li>
        </ul>
        <div id="copyright">
        <div>咩斯特數位整合有限公司 Copyright &copy; 2013 GetMore.tw INC. All rights reserved.</div>
        <div><a href="#">會員使用條款</a> <a href="#">隱私權政策</a></div>
        </div>
    </div>
</div>
<?php echo $scripts_footer; ?>
<?php (isset($debug)) && _log($_SESSION);?>
</body>
</html>
