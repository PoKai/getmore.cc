<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $meta_charset; ?>" />
    <title>
        <?php echo $site_title; ?>
    </title>
    <meta name="description" content="<?php echo $site_description; ?>" />
    <meta name="keywords" content="<?php echo $site_keywords; ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php echo $meta_tag; ?>
    <meta name="format-detection" content="telephone=no">
    <!-- add meta tag added by howtomakeaturn -->
    <meta property="og:url" content="<?php echo current_url() ?>" />

    <meta property="og:image" content="http://beta.getmore.cc/assets/images/fb-01.png" />


    <link rel="shortcut icon" href="/assets/images/favicon.ico">
    <link rel="stylesheet" href="/assets/css/typeahead.css">
    <link rel="stylesheet" href="/assets/css/bootstrap.css" />
    <link rel="stylesheet" href="/assets/css/getmore_bootstrap.css" />
    <link rel="stylesheet" href="/assets/css/home.css" />
    <link rel="stylesheet" href="/assets/css/clothing.css" />
    <link rel="stylesheet" href="/assets/css/checkout.css" />
    <link rel="stylesheet" href="/assets/css/member.css" />
    <link rel="stylesheet" href="/assets/css/sellorder.css" />
    <link rel="stylesheet" href="/assets/vendor/jquery-ui/themes/smoothness/jquery-ui.min.css">
    <script src="/assets/vendor/jquery/jquery.min.js"></script>
    <script src="/assets/javascript/bootstrap.js"></script>
    <script src="/assets/vendor/underscore-amd/underscore.js"></script>
    <script type="text/javascript">
        var getmore = getmore || {}; // Backbone namespace
        var root_path = '.';
    </script>

    <script src="/assets/javascript/howtomakeaturn/search.js"></script>
    <!-- new google analytics -->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-46722397-1', 'getmore.cc');
        ga('send', 'pageview');
    </script>

    <!--Start of Zopim Live Chat Script-->
    <script type="text/javascript">
        window.$zopim || (function (d, s) {
            var z = $zopim = function (c) {
                    z._.push(c)
                },
                $ = z.s =
                d.createElement(s),
                e = d.getElementsByTagName(s)[0];
            z.set = function (o) {
                z.set.
                _.push(o)
            };
            z._ = [];
            z.set._ = [];
            $.async = !0;
            $.setAttribute('charset', 'utf-8');
            $.src = '//v2.zopim.com/?2CzTS30m2Lrz7FqqNDx0kz5JG84zmbBd';
            z.t = +new Date;
            $.
            type = 'text/javascript';
            e.parentNode.insertBefore($, e)
        })(document, 'script');
    </script>
    <!--  End of Zopim Live Chat Script-->

    <!-- Facebook Conversion Code for 全網站 -->
    <script>
        (function () {
            var _fbq = window._fbq || (window._fbq = []);
            if (!_fbq.loaded) {
                var fbds = document.createElement('script');
                fbds.async = true;
                fbds.src = '//connect.facebook.net/en_US/fbds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(fbds, s);
                _fbq.loaded = true;
            }
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(['track', '6022979096894', {
            'value': '0.00',
            'currency': 'USD'
        }]);
    </script>
    <noscript>
        <img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6022979096894&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" />
    </noscript>

</head>

<body>

    <header id='newheader'>
        <div id='header-containerbg'>
            <div id='header-container'>
                <ul class="right-menu">

                    <?php if(!$logined): ?>
                    <li>
                        <a data-toggle="modal" href="#logInModal">
                            <img src="/assets/images/home20/login.png" alt="" id='log_in' />login</a>
                    </li>
                    <?php else: ?>
                    <li>
                        <a data-toggle="modal" href="/auth/logout?redirect_url=<?php echo urlencode(current_url()); ?>">
                            <img src="/assets/images/home20/logout.png" alt="" id='log_in' />logout</a>
                    </li>
                    <?php endif; ?>
                    <li>|</li>
                    <li class='dropdown'>
                        <a href="/member/order">
                            <img class='navbar-icon' src="/assets/images/home20/vipcenter.png">會員中心</a>
                    </li>
                    <li>
                        <a href="/closet/my">
                            <img class='navbar-icon' src="/assets/images/home20/mycloset.png">我的衣櫃</a>
                    </li>

                    <!-- shopping cart start -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <img class='navbar-icon' src="/assets/images/home20/cart.png">購物車<span class='cart-count'></span>
                        </a>
                        <ul class="dropdown-menu" id="shopping-cart-menu">
                            <li>
                                <span>Loading ... </span>
                            </li>
                        </ul>
                    </li>
                    <!-- shopping cart end -->
                </ul>
            </div>
        </div>
        <div id="navigation">
            <div class='row logoarea'>
                <div class='pull-left'>
                    <a href="/">
                        <img src='/assets/images/home20/logo.png'>
                    </a>
                </div>
                <div class="pull-right logoarea-right">
                    <strong><a href='/clothing/suggested'>官方，強力推薦</a>
            <a href="/info/brandsonly">品牌限定召集</a></strong>
                    <!-- <a href="http://getmore.cc/info/sell#howtosell">網路二手衣代售服務</a></strong> -->
                </div>
            </div>
        </div>
        <hr class='pokai-hr'>
        <div class='container'>
            <div class='menuarea row font-size14'>
                <ul class='headmenu-left font-size12'>
                    <li><a href="/clothing/category/new/">NEW</a>
                    </li>
                    <li><a href="/clothing/category/women/">WOMEN</a>
                    </li>
                    <li><a href="/clothing/category/men/">MEN</a>
                    </li>
                    <li><a href="/clothing/category/kids/" target='_blank'>KIDS</a>
                    </li>
                    <li>|</li>
                    <li><a href="/blog">Blog</a>
                    </li>
                </ul>

                <ul class='headmenu-right'>
                    <li class='headmenu-highlight'><a href="/info/brandsonly">賣衣服</a>
                    </li>
                    <!--<li class='headmenu-highlight'><a href="/info/startsell">賣衣服</a></li>-->
                    <li>|</li>
                    <li><a href="/clothing/category/new">買衣服</a>
                    </li>
                    <li>
                        <input type="text" name="href" id="href" class="form-control head-search" placeholder="Search">
                    </li>
                    <li>
                        <img class="head-search-btn" src="/assets/images/search.png">
                    </li>
                </ul>
            </div>
        </div>
    </header>

    <div class='container'>

        <!-- modals -->
        <div class="modal fade" id='signUpModal'>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">註冊會員</h4>
                    </div>
                    <div class="modal-body">
                        <div class="fb_login">
                            <h4>GetMore 是亞洲第一個二手衣時尚購物網站</h4>
                            <a class='fb_login_btn btn btn-primary' href="<?php echo site_url('/auth/facebook?redirect_url=' . urlencode(current_url()))?>">用facebook登入</a> 快使用facebook快速登入!
                            <hr class="dashed_hr" />
                        </div>
                        <div id="getmore_login">
                            <div class="row">
                                <div class="col-md-4">
                                    <h4>註冊會員</h4> 
                                </div>
                                <div class="col-md-8 pull-right announcement getmore_instruction">
                                    還不是GetMore的會員嗎？ 免費加入！
                                </div>
                            </div>
                            <form method="POST" action="/auth/register" name="dig_register">
                                <div class="form-group">
                                    <label for="sign_up_email">信箱</label>
                                    <div class='col-md-5 pull-right description'>此為您的會員登入帳號</div>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="email">
                                </div>
                                <div class="form-group">
                                    <label for="nickname">暱稱</label>
                                    <div class='col-md-5 pull-right description'>14字元以內</div>
                                    <input type="text" class="form-control" id="user_name" name="user_name" placeholder="暱稱">
                                </div>
                                <div class="form-group">
                                    <label for="new_password">密碼</label>
                                    <input type="password" class="form-control" id="new_password" name="password" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <label for="password_confirm">確認密碼</label>
                                    <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm Password">
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" value="1" name="aggree" style="width:20px;">我已閱讀，並同意<a href="/terms">GetMore 服務條款</a>
                                </div>
                                <a id='start_to_getmore' href="javascript:document.forms['dig_register'].submit();" class='btn btn-primary'>開始GetMore</a>
                                <div id='registration' class="col-md-4 pull-right">
                                    已是會員？ <a id='logIn' href="#logInModal" data-toggle="modal" onClick='$("#signUpModal").modal("hide");'>由此登入！</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <p class='announcement text-center'>已成為「會員」或是點擊「用facebook 登入」按鈕的同時,
                            <br>表示您已詳讀並同意GetMore相關的<a href="/info/buy#buypolicy" target='_blank'>購買條款</a>以及<a href="/info/sell#sellpolicy" target='_blank'>代洗代售服務條款</a>。</p>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id='logInModal'>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">會員入口</h4>
                    </div>
                    <div class="modal-body">
                        <div class="fb_login">
                            <h4>GetMore 是亞洲第一個二手衣時尚購物網站</h4>
                            <a class='fb_login_btn btn btn-primary' href="<?php echo site_url('/auth/facebook?redirect_url=' . urlencode(current_url()))?>">用facebook登入</a> 快使用facebook快速登入!
                            <hr class="dashed_hr" />
                        </div>
                        <div id="getmore_login">
                            <div class="row">
                                <div class="col-md-4">
                                    <h4>會員登入</h4> 
                                </div>
                                <div class="getmore_instruction col-md-8 pull-right announcement">
                                    若已是GetMore的會員,請在此登入!
                                </div>
                            </div>
                            <form name="login-form" action="/auth/login" method="POST">
                                <div class="form-group">
                                    <label for="email">信箱</label>
                                    <input type="email" class="form-control" id="email" name="identity" placeholder="email">
                                </div>
                                <div class="form-group">
                                    <label for="password">密碼</label> <a target='_blank' id='forget_password' class="pull-right" href="/auth/forget_password">忘記密碼？</a>
                                    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                                </div>
                                <a id='start_to_getmore' href="javascript:document.forms['login-form'].submit();" class='btn btn-primary'>開始GetMore</a>
                                <input type="hidden" name="redirect_url" value="<?php echo urlencode(current_url());?>" />
                                <div id='registration' class="col-md-4 pull-right">
                                    還不是會員? <a id='register' href="#signUpModal" data-toggle="modal" onClick='$("#logInModal").modal("hide");'>來註冊!</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <p class='announcement text-center'>已成為「會員」或是點擊「用facebook 登入」按鈕的同時,
                            <br>表示您已詳讀並同意GetMore相關的<a href="/info/buy#buypolicy" target='_blank'>購買條款</a>以及<a href="/info/sell#sellpolicy" target='_blank'>代洗代售服務條款</a>。</p>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id='modalwomen'>
            <div class="modal-dialog-800">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">尺寸參考表-WOMEN</h4>
                    </div>
                    <div class="modal-body">
                        <div class="modal-center">
                            <img src="/assets/images/content/buy/womensize.png">
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id='modalmen'>
            <div class="modal-dialog-800">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">尺寸參考表-MEN</h4>
                    </div>
                    <div class="modal-body">
                        <div class="modal-center">
                            <img src="/assets/images/content/buy/mensize.png">
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id='modalkids'>
            <div class="modal-dialog-800">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">尺寸參考表-KIDS/BABY</h4>
                    </div>
                    <div class="modal-body">
                        <div class="modal-center">
                            <img src="/assets/images/content/buy/kidssize.png">
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id='modalbtn-04'>
            <div class="modal-dialog-800">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">創造更多，得到更多</h4>
                    </div>
                    <div class="modal-body">
                        <a href='http://getmore.cc/sellorder/info/sellpolicy/'>
                            <img src="/assets/images/banner/btn-04.png">
                        </a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <!-- modals end -->
        <div class='main'>
            <?php echo $content; ?>
        </div>
        <!-- /.main -->
    </div>
    <!-- /.container -->
    <div class="panel panel-default bggetmorecolor margintop40">
        <div class="container">
            <div class='panel-body bggetmorecolor'>
                <div class="panel-box bggetmorecolor">
                    <div class="footer-menu">
                        <h5>關於GetMore</h5>
                        <div class="panel-link">
                            <a class='nav-a' href="/info/about#GetMore">
                                <h6>GetMore簡介</h6>
                            </a>

                            <a class='nav-a' href="/info/joinus">
                                <h6>加入我們</h6>
                            </a>
                            <a class='nav-a' href="/info/about#vender">
                                <h6>合作夥伴</h6>
                            </a>
                            <a class='nav-a' href="/info/about#news">
                                <h6>媒體報導</h6>
                            </a>
                            <a href="http://getmore.cc/blog" target="_blank">
                                <h6>Blog</h6>
                            </a>
                        </div>
                    </div>
                    <div class='footer-menu'>
                        <h5>我要賣衣服</h5>
                        <div class="panel-link">
                            <a class='nav-a' href="/info/sell#howtosell">
                                <h6>賣衣服流程</h6>
                            </a>
                            <a class='nav-a' href="/info/brandsonly">
                                <h6><div class="red">品牌召集</div></h6>
                            </a>
                            <a class='nav-a' href="/info/sell_type">
                                <h6>GetMore搶手貨</h6>
                            </a>
                            <a class='nav-a' href="/info/startsell">
                                <h6>開始賣衣服</h6>
                            </a>
                            <a class='nav-a' href="/info/sell#wash">
                                <h6>保證嚴格清洗</h6>
                            </a>
                            <a class='nav-a' href="/info/sell#sellpolicy">
                                <h6>代洗代售條款</h6>
                            </a>
                        </div>
                    </div>
                    <div class='footer-menu'>
                        <h5>我要買衣服</h5>
                        <div class="panel-link">
                            <a href="/info/perfectbuy">
                                <h6>完美的購物體驗</h6>
                            </a>
                            <a href="/info/product">
                                <h6>商品頁說明</h6>
                            </a>
                            <a class='nav-a' href="/info/buy#paynfee">
                                <h6>購買說明</h6>
                            </a>
                            <a class='nav-a' href="/info/buy#return">
                                <h6>關於退貨</h6>
                            </a>
                            <a class='nav-a' href="/info/buy#buypolicy">
                                <h6>購買條款</h6>
                            </a>
                        </div>
                    </div>
                    <div class='footer-menu'>
                        <h5>和世界一起</h5>
                        <div class="panel-link">
                            <a href="/sellorder/info/redress">
                                <h6>redress</h6>
                            </a>
                            <a href="/info/intern2015">
                                <h6>實習計畫</h6>
                            </a>
                        </div>
                    </div>
                    <div class='footer-menu'>
                        <h5>客服中心</h5>
                        <div class="panel-link">
                            <a href="/info/qalist/">
                                <h6> Q&A </h6>
                            </a>
                            <h4> Contact us <br> service@getmore.cc </h4>
                            <a href="http://on.fb.me/1e3k3Up" target="_blank">
                                <img src="/assets/images/likeicon/fb.png" alt="" class='panel-like' />
                            </a>
                            <a href="http://www.youtube.com/channel/UCLQtRAJ28v2e-ymeKJR-yXw/videos/" target="_blank">
                                <img src="/assets/images/likeicon/youtube.png" alt="" class='panel-like' />
                            </a>
                            <a href="http://bit.ly/1g9Jt4t" target="_blank">
                                <img src="/assets/images/likeicon/blog.png" alt="" class='panel-like' />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class='container'>
            <div class="panel-footer bggetmorecolor">
                <hr>
                <p>Copyright © 2013 GetMore.cc INC.</p>
                <p>All rights reserved.</p>
            </div>
        </div>
    </div>


    <?php echo $scripts_footer; ?>
    <?php (isset($debug)) && _log($_SESSION);?>
</body>

</html>

