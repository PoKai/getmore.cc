<div id="content">
    <div class="all-new"></div>
    <div id="content-buy">
        <?php foreach ($new_list_data as $row): ?>
        <div class="article">
            <div class="article-title">
                <h3 class="<?php echo $row['icon']; ?>"><?php echo $row['category_name']; ?></h3>
            </div>
            <div class="article-thing">
                <div class="article-img">
                    <a href="<?php echo  $row['url']; ?>">
                        <div class="article-img-hover"></div>
                        <div class="article-img-hover-text">get<br />this</div>
                        <img class="lazy" src="<?php echo site_url('assets/images/white.gif'); ?>" data-original="<?php echo image_url($row['file_name'], 430, 380); ?>" width="430" height="380" />
                    </a>
                    <div class="article-explain">
                        <?php echo $row['description']; ?>
                    </div>
                    <?php if (!$row['sold']): ?>
                    <div class="sold-out"></div>
                    <?php endif; ?>
                </div>
                <div class="explain">
                    <div class="title">
                        <?php echo $row['title']; ?>
                    </div>
                    <div class="price">
                        $<?php echo $row['sell_price']; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        <p class="clear"></p>
    </div>
    <p class="line"></p>
    <div class="all-sale"></div>
    <div id="content-row-3">
        <div id="row-3">
            <?php foreach ($sale_list_data as $row): ?>
            <div class="small-article">
                <div class="small-article-title">
                    <div class="<?php echo $row['icon']; ?>"><?php echo $row['category_name']; ?></div>
                </div>
                <div class="small-article-thing">
                    <?php if (!$row['sold']): ?>
                    <div class="sold-out"></div>
                    <?php endif; ?>
                    <a href="<?php echo  $row['url']; ?>">
                        <div class="article-img-hover"></div>
                        <div class="article-img-hover-text">get<br />this</div>
                        <div class="small-article-img">
                            <img class="lazy" src="<?php echo site_url('assets/images/white.gif'); ?>" data-original="<?php echo image_url($row['file_name'], 258, 224); ?>" />
                        </div>
                        <div class="surplus-time"><?php echo $row['message']; ?></div>
                    </a>
                </div>
                <div class="small-article-theme">
                    <h3><?php echo $row['title']; ?></h3>
                    <p class="discounts"><?php echo $row['description']; ?></p>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <p class="clear" style="margin-bottom: 30px;">&nbsp;</p>
    </div>
</div>

