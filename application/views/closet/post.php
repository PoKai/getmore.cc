<div class='fresh-box'>
    <!-- post -->
    <div getmore-post>
      <? foreach ($postslist as $post): ?>
          <div class="row clr " id="<?=$post['id']?>" >
              <span class="replied"></span>
              <div class="from">
                  <a href="/closet/view/<?=$post['user_id']+9881;?>" class="avatar" target="_blank">
                      <img src="<?=$post['profile_pic_url']?>">
                  </a>
                  <a href="/closet/view/<?=$post['user_id']+9881;?>" target="_blank"><?=$post['user_name']?></a>
              </div>
              <div class="subject">
                  <div class="line">
                      <b><?=$post['title']?></b>
                      <? if($post['product_id']): ?>
                      <a style="margin-left:10px;font-size:10px;" href="/clothing/view/<?=$post['product_id']?>" target="_blank">查看商品</a> 
                      <? elseif($post['url_code']):?>
                      <a style="margin-left:10px;font-size:10px;" href="/closet/view/<?=$post['url_code']?>" target="_blank">查看衣櫥</a>
                      <? endif;?>
                  </div>
                  <p class="description magictext"><?=$post['content']?></p>
                  <div class="created"><?=$post['created_time']?></div>
                  <a class="delete" mid="<?=$post['id']?>" role="receiver">×</a>
                  <div class="hidable">
                  <?if($post['last_message']):?>
                      <div class="readmore_messages" data-post_id=<?=$post['id']?>>看其他留言</div>
                      <div class="messages">
                        <div class="reply clr">
                            <img class="avatar" src="<?=$post['last_message'][0]['profile_pic_url'];?>">
                            <div class="subject">
                                <p class="text magictext"><?=$post['last_message'][0]['content']?></p>
                                <div class="created"><?=$post['last_message'][0]['created_time']?></div>
                            </div>
                        </div>
                      </div>
                  <?endif;?>
                  </div>
              </div>
          </div>
      <? endforeach; ?>
      <? if($has_login): ?>
      <form method="POST" enctype="multipart/form-data" action="/api/posts/new_post" class="post">
        <div>
          <label>關於</label>
          <input type="text" name="title">
        </div>
        <div>
          <label>訊息內容</label>
          <textarea name="content" rows="7"></textarea>
        </div>
        <input type="hidden" name="url_code" value="<?=$closet['url_code'];?>">
        <input type="hidden" name="owner_id" value="<?=$closet['user_id'];?>">
        <div>
          <input type="reset" value="取消">
          <input type="submit" value="送出">
        </div>
      </form>
      <? else:?>
      <div>請先登入才能留言喔！</div>
      <? endif; ?>
    </div>
    <script type="text/javascript" src="/assets/javascript/grassshrimp/posts.js">
    </script>
    <div style='clear: both;'></div>    
</div>
