<div class='row' id='pagerow'>
    <div class='col-254'>
        <div class='leftcol'>
            <p>商品管理 / Sell Manage</p>
            <p>
                <a href="/closet/manage"><button type="button" class="btn btn-primary btn-m">二手衣上架</button></a>
            </p>
            <p>
                <a href="/info/joinus"><button type="button" class="btn btn-primary btn-m">修改已上架資訊</button></a>
            </p>
            <p>
                <a href="/closet/manage"><button type="button" class="btn btn-primary btn-m">設定交易政策</button></a>
            </p>
            <p>
                <a href="/closet/manage"><button type="button" class="btn btn-primary btn-m">宣傳我的衣櫃</button></a>
            </p>
        </div>
        <hr>
        <div class='contactus'><p> Contact us <br> service@getmore.cc </p> </div>
    </div>

<div class='col-726'>
    <div class="tab-content article-line">
    <div class="tab-pane active" id="GetMore">
    <div class='rightcol fontp15'>
    <div>
        <div class="planabox">

            <!-- add price -->
            <div class="item">
                <div>
                    <h3>交易政策</h3>
                    <p>確認您的交易政策，請參考<a>交易政策說明</a>。</p>
                </div>
                <div class="form-group">
                        <label for="product_comments">寄送時間</label>
                        <textarea class="form-control" id="disabledInput" name="product_comments" ng-model="product.product_comments" rows="5" placeholder=" 商品出貨時間：
 匯款確認後的 2 天內會將商品郵寄給您。
商品運送時間 (台灣)：
寄出後約 5 - 7 個工作天內送達 (不含週六日及國定假日)。">
                        </textarea>
                        <div class="planaex" >字數限制100字，還剩100字。</div>
                </div>
                <div class="form-group">
                <label for="product_title">寄送方式</label>
                    <select class="form-control width375">
                      <option>黑貓宅急便</option>
                      <option>郵局</option>
                      <option>宅配通</option>
                      <option>新竹物流</option>
                      <option>其他貨運</option>
                    </select>
                </div>
                <div class="form-group">
                        <label for="product_comments">商品運送時間</label>
                        <textarea class="form-control" id="disabledInput" name="product_comments" ng-model="product.product_comments" rows="5" placeholder="一般商品出貨時間：確認付款後的 3 天內會將商品郵寄給您。
商品運送時間 (台灣)：寄出後約 3 個工作天內送達 (不含週六日及國定假日)
商品運送時間 (海外)：根據目的地不同,而有不同送達工作天數，請務必事先詢問。">
                        </textarea>
                        <div class="planaex" >字數限制100字，還剩100字。</div>
                </div>
                <div class="form-group">
                        <label for="product_comments">退貨政策</label>
                        <textarea class="form-control" id="disabledInput" name="product_comments" ng-model="product.product_comments" rows="5" placeholder="退換貨政策：在收到商品後，請立刻檢查商品是否正確，如果有問題請於 3 天內與我連絡，並於我回覆地址後 2 天內保持完整連同原商品包裝寄回，超過時限即視為無誤。商品為二手商品只有一件，並非完美，退貨請自行負擔運費。">
                        </textarea>
                        <div class="planaex" >字數限制100字，還剩100字。</div>
                </div>
                <div class="planabtn">
                    <a class="btn btn-success"  role="button" data-target="#carousel-example-generic" data-slide-to="3">上一步</a>
                    <a class="btn btn-info" role="button" data-target="#carousel-example-generic" data-slide-to="4">下一步，確認上架資訊</a>
                </div>
            </div>
            <div class="item">
                <button type="button" class="btn btn-info" data-target="#carousel-example-generic" ng-click="submit()">完成上架！</button>
            </div>
        </form>
        </div>
        <script src="/assets/vendor/angular/angular.min.js" type="text/javascript"></script>
        <script src="/assets/vendor/ngFileReader/vendor/swfobject.js" type="text/javascript"></script>
        <script src="/assets/vendor/ngFileReader/vendor/jquery.FileReader.position.ver.js" type="text/javascript"></script>
        <script type="text/javascript">
            angular.module('closetManage', [])
                .controller('launch', ['$scope', '$element', '$http', function ($scope, $element, $http) {
                    var default_product = {
                        product_title: '',
                        product_comments: '',
                        item_class1: 1,
                        product_type: 11,
                        item_status: '全新未拆吊牌',
                        product_brand: '',
                        product_original_price: 1000,
                        product_sell_price: 0,
                        material: '',
                        location: ''
                    };

                    $scope.product = angular.copy(default_product);

                    $scope.text = {};



                    angular.element('div.filelist', $element).delegate('input[type="file"]', 'change', function(e){
                        console.log(e.target.files[0]);
                    });

                    $scope.$watch('product.item_class1', function (newValue, oldValue) {
                        $scope.text.item_class1 = angular.element('input.item_class1[value="' + newValue + '"]', $element).parent().text().trim();

                        $scope.product.product_type = newValue + '1';
                    });

                    $scope.$watch('product.product_type', function (newValue, oldValue) {
                        $scope.text.product_type = angular.element('input.product_type[value="' + newValue + '"]', $element).parent().text().trim();
                    });

                    $scope.$watch('product.product_original_price', function (newValue, oldValue) {
                        $scope.text.product_original_price = angular.element('option[value="' + newValue + '"]', $element).text().trim();
                    });

                    angular.element('input[type="file"]').fileReader({});

                    $scope.addFile = function(e){
                        var last_file_input = angular.element('input[type="file"]:last', $element);
                        var clone_file_input = last_file_input.clone();
                        angular.element(clone_file_input).fileReader({});
                        last_file_input.after(clone_file_input);
                        if(angular.element('input[type="file"]', $element).length >= 5){
                            angular.element(e.target).remove();
                        }
                    }

                    $scope.submit = function () {
                        angular.element('form', $element)[0].submit();
                    }
                }])
        </script>
        </div>
        </div>
        </div>
        </div>
</div>
</div> <!--End of row-->
