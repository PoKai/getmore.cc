
<style>
#select-lock-status{
  width: 200px;
}
</style>

<div class='fresh-box'>
    <div style="overflow:auto; padding-right: 10px;">
      <?php if (!empty($items)): ?>
        <select id="select-lock-status" class="form-control pull-right">
          <option value="-1">全部</option>
          <option value="0">銷售中123</option>
        <?php if($this->uri->segment(2) == 'view'): ?>
          <option value="1">已預定</option>
        <?php endif;?>
          <option value="2">已售出</option>
        <?php if($is_owner): ?>
          <?php if($this->uri->segment(2) == 'view'): ?>
          <option value="3">預約上架</option>
          <option value="4">草稿</option>
          <?php endif;?>
          <option value="5">已下架</option>
        <?php endif; ?>
        </select>
      <? endif; ?>
    </div>
    </br>
    <div>
      <? foreach ($items as $item): ?>
        <?php echo clothing_thumbnail_layout($item); ?>
      <? endforeach; ?>
      <?php if (empty($items)): ?>
      <div class='alert alert-info'>這個衣櫃內還沒有衣服喔！</div>
      <?php endif; ?>
      <div class='alert alert-info zero-select'>這個選項中還沒有衣服喔！</div>
    </div>
    <div style='clear: both;'></div>    
</div>
<hr>

<div class="fb-comments margintop20 marginleft15" data-href="<?php echo current_url() ?>" data-width="950" data-numposts="5" data-colorscheme="light"></div>

<script type="text/javascript">
  
    $(function () {
        LikeButtonModule.initialize();
        <?php if($logined): ?>
            LikeButtonModule.setIsLogined(true);
        <?php endif; ?>
        $('.zero-select').hide();

        if($(".lock-status-1").length == 0){
          $("option[value='1']").hide();
        }
        if($(".lock-status-3").length == 0){
          $("option[value='3']").hide();
        }
        if($(".lock-status-4").length == 0){
          $("option[value='4']").hide();
        }
        if($(".lock-status-5").length == 0){
          $("option[value='5']").hide();
        }

        $('#select-lock-status').change(function(){
          $('.zero-select').hide();
          $('.fresh-product-view').hide();
          switch ($(this).val()) {
            case '-1':
              $('.fresh-product-view').show();
              break;
            case '0':
              $('.lock-status-0').show();
              break;
            case '1':
              $('.lock-status-1').show();
              break;
            case '2':
              $('.lock-status-2').show();
              if($(".lock-status-2").length == 0){
                $('.zero-select').show();
              }
              break;
            case '3':
              $('.lock-status-3').show();
              break;
            case '4':
              $('.lock-status-4').show();
              break;
            case '5':
              $('.lock-status-5').show();
              break;
          }
        });
    });
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&appId=239678122794926&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
