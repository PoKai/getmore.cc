<?php echo alert_message($message, 'info') ?>

<div class="col-md-7">
    <h4 class='marginbottom20 getmoregreen'>方法1.透過facebook宣傳</h4>
        <a target='_blank' href='http://www.facebook.com/sharer.php?u=<?php echo site_url('/closet/view/' . $closet['url_code']) ?>'><img src="/assets/images/fb_button.png"></a><a class='btn btn-primary fbsharebtn' target='_blank' 
              href='http://www.facebook.com/sharer.php?u=<?php echo site_url('/closet/view/' . $closet['url_code']) ?>'>
            分享衣櫃給FB上的朋友
        </a>
</div>

<div class="col-md-11"><hr></div>
<div class="col-md-7">
<h4 class='marginbottom20 getmoregreen'>方法2.Email給朋友</h4>
<form role="form" action='/closet/invite_friends' method='post'>

  <div class="form-group">
    <label>寄件人名稱</label>
    <input type="text" name='sender' class="form-control" placeholder="告訴朋友你是誰，未輸入則是預設的暱稱">
  </div>

  <div class="form-group">
    <label>好友email</label>
    <input type="text" name='receiver' class="form-control" placeholder="若有多個收件人，請以逗號「,」隔開" required>
  </div>

  <div class="form-group">
    <label>主旨</label>
    <input type="text" name='title' class="form-control" value="來逛逛我在GetMore衣櫃吧！" required>
  </div>

  <div class="form-group">
    <label>內容</label>
    <textarea name='content' class='form-control' rows=10 placeholder="這是我在GetMore的衣櫃，快來來逛逛，有問題可以發問喔！" required></textarea>
    <p class="help-block">系統會自動帶入衣櫃網址，您不需要複製貼上</p>
  </div>

  <button type="submit" class="btn btn-default">送出</button>
</form>
</div>

<div class="col-md-11"><hr></div>
<div class="col-md-7">
    <h4 class='marginbottom20 getmoregreen'>方法3.我的衣櫃小貼紙</h4>
    <a target='_blank' href='<?php echo site_url('/closet/view/' . $closet['url_code']) ?>'><img src=<?php echo site_url('/assets/images/closet/sticker.png') ?> /></a>
    <p class="help-block  marginbottom20">（245 x 142）</p>
    <p>在你的部落格加入衣櫃小貼紙，可以增加衣櫃的人氣喔！</p>
    <textarea rows="4" class='form-control'>
    <a target='_blank' href='<?php echo site_url('/closet/view/' . $closet['url_code']) ?>'><img src=<?php echo site_url('/assets/images/closet/sticker.png') ?> /></a>
    </textarea>
    <p class="help-block">（貼紙程式碼）</p>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&appId=239678122794926&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
