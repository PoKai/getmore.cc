<div class='row' id='pagerow'>
    <div class='col-254'>
        <div class='leftcol'>
            <p>關於 / About</p>
            <p>
                <a class='nav-a' href="#GetMore" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">關於GetMore</button></a>
            </p>
            <!--<p>
                <a class='nav-a' href="#team" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">GetMore Team</button></a>
            </p>-->
            <p>
                <a href="/info/joinus"><button type="button" class="btn btn-primary btn-m">加入我們</button></a>
            </p>
            <p>
                <a class='nav-a' href="#vender" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">合作夥伴</button></a>
            </p>
            <p>
                <a class='nav-a' href="#news" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">媒體報導</button></a>
            </p>
        </div>
        <hr>
        <div class='contactus'><p> Contact us <br> service@getmore.cc </p> </div>
    </div>

<div class='col-726'>
    <div class="tab-content article-line">
    <div class="tab-pane active" id="GetMore">
    <div class='rightcol fontp15'>
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false" style="height: auto;" ng-app="closetManage" ng-controller="launch">
        <ul class="list-unstyled list-inline text-center">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active">step1</li>
            <li data-target="#carousel-example-generic" data-slide-to="1">step2</li>
            <li data-target="#carousel-example-generic" data-slide-to="2">step3</li>
        </ul>
        <div class="planabox">
        <form name="launchForm" method="post" action="/restful/product_clothing" class="carousel-inner form-horizontal" role="listbox" enctype="multipart/form-data">
            <div class="item active filelist">
                <div>
                    <input type="file" name="product_pic[]">
                    <a class="btn" ng-click="addFile($event)">新增檔案</a>
                </div>
                <div>
                    <a class="btn btn-info" data-target="#carousel-example-generic" data-slide-to="1">下一步</a>
                </div>
            </div>

            <div class="item">
                <div class="col-md-4">
                    <h3>概述</h3>
                    <p>顯示在商品頁面的商品名稱,品牌及敘述。</p>
                </div>

                <div class="col-md-8">
                    <div class="form-group">
                        <label for="product_title">商品名稱</label>
                        <input type="text" class="form-control" id="product_title" maxlength="20" ng-model="product.product_title" name="product_title">
                        <p class="description">ex:ZARA 紅色夏季襯衫</p>
                    </div>
                    <div class="form-group">
                        <label for="product_comments">商品敘述</label>
                        <textarea class="form-control" id="product_comments" name="product_comments" ng-model="product.product_comments" rows="5"></textarea>
                        <p class="description">請詳細描述這衣服的特徵及穿起來的感受等等</p>
                    </div>
                    <div class="form-group">
                    <label for="product_brand">品牌</label>
                    <input type="text" class="form-control ui-autocomplete-input" id="product_brand" name="product_brand" maxlength="30" autocomplete="off" ng-model="product.product_brand">
                    <p class="description">請確實輸入品牌,GetMore不收假貨喔!</p>
                    </div>
                    </hr>
                 </div>

                <div class="col-md-4">
                    <h3>類別及狀態</h3>
                    <p>網站分類，以及告知商品狀態</p>
                </div>
                <div class="col-md-8">

                </div>
                <div class="col-md-4">
                    <h3>類別及狀態</h3>
                    <p>網站分類，以及告知商品狀態</p>
                </div>
                <div class="col-md-8">
                    <div class="level_1">
                        <h6>服裝</h6>
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="product.item_class1" value="1" name="item_class1" class="item_class1 item_class">Women
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="product.item_class1" value="2" name="item_class1" class="item_class1 item_class">Men
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="product.item_class1" value="3" name="item_class1" class="item_class1 item_class">Kids / Baby
                            </label>
                        </div>
                    </div>
                </div>
            <!--
                <div class="col-md-4">
                    <h3>價格</h3>
                    <p>購買價以及想要賣的價錢</p>
                </div>

                <div class="col-md-8">
                    <div class="form-group">
                        <label for="original_price">參考原價</label>
                        <select class="form-control" id="product_original_price" name="product_original_price" ng-model="product.product_original_price">
                            <option value="1000">500元~1,000元</option>
                            <option value="2000">1,001元~2,000元</option>
                            <option value="3000">2,001元~3,000元</option>
                            <option value="4000">3,001元~4,000元</option>
                            <option value="5000">4,001元~5,000元</option>
                            <option value="6000">5,001元~6,000元</option>
                            <option value="7000">6,001元~7,000元</option>
                            <option value="8000">7,001元~8,000元</option>
                            <option value="9000">8,001元~9,000元</option>
                            <option value="10000">9,001元~10,000元</option>
                            <option value="99999">超過10,000元</option>
                        </select>
                        <p class="description">請填入當時購買的大約價格,請勿提出原價低於$500商品</p>
                    </div>
                    <div class="form-group">
                        <label for="sell_price">二手販售價格</label>
                        <input type="number" class="form-control" id="product_sell_price" name="product_sell_price" ng-model="product.product_sell_price">
                        <p class="description">想要賣出的價格,建議為原價3折以下</p>
                    </div>
                </div>


                <div class="form-group col-md-7">
                    <label for="product_title">商品名稱</label>
                    <input type="text" class="form-control" id="product_title" maxlength="20" ng-model="product.product_title" name="product_title">
                    <p class="description">ex:ZARA 紅色夏季襯衫</p>
                </div>

                <div class="col-md-10">
                    <div class="col-md-4 level_1">
                        <h6>服裝</h6>
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="product.item_class1" value="1" name="item_class1" class="item_class1 item_class">Women
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="product.item_class1" value="2" name="item_class1" class="item_class1 item_class">Men
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="product.item_class1" value="3" name="item_class1" class="item_class1 item_class">Kids / Baby
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4 level_2">
                        <h6>類別</h6>
                        <ul class="list-unstyled" ng-class="product.item_class1 == 1?'show':'hidden'">
                            <li>
                                <label>
                                    <input type="radio" ng-model="product.product_type" value="11" name="product_type" class="product_type item_class">上衣
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" ng-model="product.product_type" value="12" name="product_type" class="product_type item_class">洋裝
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" ng-model="product.product_type" value="13" name="product_type" class="product_type item_class">褲子
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" ng-model="product.product_type" value="14" name="product_type" class="product_type item_class">裙子
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" ng-model="product.product_type" value="15" name="product_type" class="product_type item_class">外套
                                </label>
                            </li>
                        </ul>
                        <ul class="list-unstyled hidden" ng-class="product.item_class1 == 2?'show':'hidden'">
                            <li>
                                <label>
                                    <input type="radio" ng-model="product.product_type" value="21" name="product_type" class="product_type item_class" ng-checked="item_class1 == 2">上衣
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" ng-model="product.product_type" value="22" name="product_type" class="product_type item_class">襯衫
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" ng-model="product.product_type" value="23" name="product_type" class="product_type item_class">褲子
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" ng-model="product.product_type" value="24" name="product_type" class="product_type item_class">外套
                                </label>
                            </li>
                        </ul>
                        <ul class="list-unstyled hidden" ng-class="product.item_class1 == 3?'show':'hidden'">
                            <li>
                                <label>
                                    <input type="radio" ng-model="product.product_type" value="31" name="product_type" class="product_type item_class" ng-checked="item_class1 == 3">連身衣
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" ng-model="product.product_type" value="32" name="product_type" class="product_type item_class">上衣
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" ng-model="product.product_type" value="33" name="product_type" class="product_type item_class">下身
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" ng-model="product.product_type" value="34" name="product_type" class="product_type item_class">洋裝
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" ng-model="product.product_type" value="35" name="product_type" class="product_type item_class">外套
                                </label>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 level_3" ng-init="product.item_status = '全新未拆吊牌'">
                        <h6>狀態</h6>
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="product.item_status" value="全新未拆吊牌" name="item_status" class="item_status item_class">全新未拆吊牌
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="product.item_status" value="幾近全新" name="item_status" class="item_status item_class">幾近全新
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="product.item_status" value="狀況良好" name="item_status" class="item_status item_class">狀況良好
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="product.item_status" value="有歲月痕跡" name="item_status" class="item_status item_class">有歲月痕跡
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-7">
                    <label for="product_brand">品牌</label>
                    <input type="text" class="form-control ui-autocomplete-input" id="product_brand" name="product_brand" maxlength="30" autocomplete="off" ng-model="product.product_brand">
                    <p class="description">請確實輸入品牌,GetMore不收假貨喔!</p>
                </div>
                <div class="form-group col-md-7">
                    <label for="original_price">參考原價</label>
                    <select class="form-control" id="product_original_price" name="product_original_price" ng-model="product.product_original_price">
                        <option value="1000">500元~1,000元</option>
                        <option value="2000">1,001元~2,000元</option>
                        <option value="3000">2,001元~3,000元</option>
                        <option value="4000">3,001元~4,000元</option>
                        <option value="5000">4,001元~5,000元</option>
                        <option value="6000">5,001元~6,000元</option>
                        <option value="7000">6,001元~7,000元</option>
                        <option value="8000">7,001元~8,000元</option>
                        <option value="9000">8,001元~9,000元</option>
                        <option value="10000">9,001元~10,000元</option>
                        <option value="99999">超過10,000元</option>
                    </select>
                    <p class="description">請填入當時購買的大約價格,請勿提出原價低於$500商品</p>
                </div>
                <div class="form-group col-md-7">
                    <label for="sell_price">二手販售價格</label>
                    <input type="number" class="form-control" id="product_sell_price" name="product_sell_price" ng-model="product.product_sell_price">
                    <p class="description">想要賣出的價格,建議為原價3折以下</p>
                </div>
                <div class="form-group col-md-7">
                    <label for="material">材質</label>
                    <input type="text" class="form-control" id="material" name="material" ng-model="product.material">
                </div>
                <div class="form-group col-md-7">
                    <label for="location">產地</label>
                    <input type="text" class="form-control" id="location" name="location" ng-model="product.location">
                </div>
-->
                <a class="btn btn-primary" data-target="#carousel-example-generic" data-slide-to="2">next</a>
            </div>

            <div class="item form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">商品名稱</label>
                    <div class="col-sm-10">
                        <p class="form-control-static" ng-bind="product.product_title"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">商品敘述</label>
                    <div class="col-sm-10">
                        <p class="form-control-static" ng-bind="product.product_comments"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">服裝</label>
                    <div class="col-sm-10">
                        <p class="form-control-static" ng-bind="text.item_class1"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">類別</label>
                    <div class="col-sm-10">
                        <p class="form-control-static" ng-bind="text.product_type"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">狀態</label>
                    <div class="col-sm-10">
                        <p class="form-control-static" ng-bind="product.item_status"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">品牌</label>
                    <div class="col-sm-10">
                        <p class="form-control-static" ng-bind="product.product_brand"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">參考原價</label>
                    <div class="col-sm-10">
                        <p class="form-control-static" ng-bind="text.product_original_price"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">二手販售價格</label>
                    <div class="col-sm-10">
                        <p class="form-control-static" ng-bind="product.product_sell_price"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">材質</label>
                    <div class="col-sm-10">
                        <p class="form-control-static" ng-bind="product.material"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">產地</label>
                    <div class="col-sm-10">
                        <p class="form-control-static" ng-bind="product.location"></p>
                    </div>
                </div>
                <button type="button" class="btn btn-success" data-target="#carousel-example-generic" ng-click="submit()">done</button>
            </div>
        </form>
        </div>
        <script src="/assets/vendor/angular/angular.min.js" type="text/javascript"></script>
        <script src="/assets/vendor/ngFileReader/vendor/swfobject.js" type="text/javascript"></script>
        <script src="/assets/vendor/ngFileReader/vendor/jquery.FileReader.position.ver.js" type="text/javascript"></script>
        <script type="text/javascript">
            angular.module('closetManage', [])
                .controller('launch', ['$scope', '$element', '$http', function ($scope, $element, $http) {
                    var default_product = {
                        product_title: '',
                        product_comments: '',
                        item_class1: 1,
                        product_type: 11,
                        item_status: '全新未拆吊牌',
                        product_brand: '',
                        product_original_price: 1000,
                        product_sell_price: 0,
                        material: '',
                        location: ''
                    };

                    $scope.product = angular.copy(default_product);

                    $scope.text = {};



                    angular.element('div.filelist', $element).delegate('input[type="file"]', 'change', function(e){
                        console.log(e.target.files[0]);
                    });

                    $scope.$watch('product.item_class1', function (newValue, oldValue) {
                        $scope.text.item_class1 = angular.element('input.item_class1[value="' + newValue + '"]', $element).parent().text().trim();

                        $scope.product.product_type = newValue + '1';
                    });

                    $scope.$watch('product.product_type', function (newValue, oldValue) {
                        $scope.text.product_type = angular.element('input.product_type[value="' + newValue + '"]', $element).parent().text().trim();
                    });

                    $scope.$watch('product.product_original_price', function (newValue, oldValue) {
                        $scope.text.product_original_price = angular.element('option[value="' + newValue + '"]', $element).text().trim();
                    });

                    angular.element('input[type="file"]').fileReader({});

                    $scope.addFile = function(e){
                        var last_file_input = angular.element('input[type="file"]:last', $element);
                        var clone_file_input = last_file_input.clone();
                        angular.element(clone_file_input).fileReader({});
                        last_file_input.after(clone_file_input);
                        if(angular.element('input[type="file"]', $element).length >= 5){
                            angular.element(e.target).remove();
                        }
                    }

                    $scope.submit = function () {
                        angular.element('form', $element)[0].submit();
                    }
                }])
        </script>
        </div>
        </div>
        </div>
        </div>
</div>
</div> <!--End of row-->
<script>
    $(document).ready(function(){
        var hash = window.location.hash;
        hash && $('.nav-a[href="' + hash + '"]').tab('show');

        $('.nav-a').click(function (e) {
          $(this).tab('show');
          var scrollmem = $('body').scrollTop();
          window.location.hash = this.hash;
          $('html,body').scrollTop(scrollmem);

        });
    });
</script>
