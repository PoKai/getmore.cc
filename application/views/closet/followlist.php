<div class='fresh-box'>

<? foreach ($followlist as $user): ?>
    <div class="fresh-product-view">
        <a href="/closet/view/<?= $user['url_code']?>">
            <div class="fresh-product-title"><?= $user['title']?></div>
        </a>
        <div style="width: 198px; height: 198px; overflow: hidden;">
            <a href="/closet/view/<?= $user['url_code']?>">
                <img src="<?= $user['profile_pic_url']?>" style="width: 198px;">
            </a>
        </div>
        <div class="article-abstract">
            <a href="/closet/view/<?= $user['url_code']?>" style="color:inherit;">
            <?=  $user['title']?>
            </a>
        </div>
        <div class="article-abstract" style='max-height:70px;'>
            <?= $user['description_tag']?>
        </div>
    </div>
<? endforeach; ?>
    <?php if (empty($followlist)): ?>
    <div class='alert alert-info'>您目前尚未追蹤任何人喔！</div>
    <?php endif; ?>
    <div style='clear: both;'></div>  


 <!--   <? foreach ($followlist as $user): ?>
        <a href="/closet/view/<?= $user['url_code']?>">
            <img src="<?= $user['profile_pic_url']?>">
            <span><?= $user['title']?></span>
        </a>
    <? endforeach; ?>
    <?php if (empty($followlist)): ?>
    <div class='alert alert-info'>您目前尚未追蹤任何人喔！</div>
    <?php endif; ?>
    <div style='clear: both;'></div>    
-->
</div>