
<!--
 <div class='closet-nav'>
     <div class='closet-nav-element'><a href='/closet/view/<?php echo $closet['url_code'] ?>'>我的衣櫃</a></div>
     <div class='closet-nav-element closet-nav-element-active'><a href='/closet/show_room/<?php echo $closet['url_code'] ?>'>SHOW ROOM</a></div>
     <div style='clear: both;'></div>    
 </div>
-->
<style>
#select-lock-status{
  width: 200px;
}
</style>

<div class='fresh-box'>
    <div style="overflow:auto; padding-right: 10px;">
      <?php if (!empty($favorites)): ?>
        <select id="select-lock-status" class="form-control pull-right">
          <option value="-1">全部</option>
          <option value="0">販售中</option>
          <option value="2">已售出</option>
          <option value="5">已下架</option>
        </select>
      <? endif; ?>
    </div>
    </br>
    <div>
    <? foreach ($favorites as $item): ?>
      <?php echo clothing_thumbnail_layout($item); ?>
    <? endforeach; ?>
    <?php if (empty($favorites)): ?>
    <div class='alert alert-info'>欲望清單內還沒有衣服喔！</div>
    <?php endif; ?>
    <div style='clear: both;'></div>    
    </div>
</div>

<script type="text/javascript">
  
    $(function () {
        LikeButtonModule.initialize();
        <?php if($logined): ?>
            LikeButtonModule.setIsLogined(true);
        <?php endif; ?>

        $('.zero-select').hide();

        if($(".lock-status-1").length == 0){
          $("option[value='1']").hide();
        }
        
        if($(".lock-status-0").length == 0){
          $("option[value='0']").hide();
        }

        if($(".lock-status-2").length == 0){
          $("option[value='2']").hide();
        }

        if($(".lock-status-5").length == 0){
          $("option[value='5']").hide();
        }
        
        $('#select-lock-status').change(function(){
          $('.zero-select').hide();
          $('.fresh-product-view').hide();
          switch ($(this).val()) {
            case '-1':
              $('.fresh-product-view').show();
              break;
            case '0':
              $('.lock-status-0').show();
              break;
            case '1':
              $('.lock-status-1').show();
              break;
            case '2':
              $('.lock-status-2').show();
              break;
            case '5':
              $('.lock-status-5').show();
              break;
          }
        });
    });
</script>
