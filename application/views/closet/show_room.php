<div class='closet-nav-box'>
    <ul class="nav nav-tabs">
      <li><a href='/closet/view/<?php echo $closet['url_code'] ?>'>我的衣櫃</a></li>
      <li class="active"><a href='/closet/show_room/<?php echo $closet['url_code'] ?>'>SHOW ROOM</a></li>
      <?php if($is_owner): ?><li><a href='/closet/my_favorite'>我的欲望清單</a></li><?php endif ?>
    </ul>
</div>
<!--
 <div class='closet-nav'>
     <div class='closet-nav-element'><a href='/closet/view/<?php echo $closet['url_code'] ?>'>我的專櫃</a></div>
     <div class='closet-nav-element closet-nav-element-active'><a href='/closet/show_room/<?php echo $closet['url_code'] ?>'>SHOW ROOM</a></div>
     <div style='clear: both;'></div>    
 </div>
-->
<div class='closet-main closet-main-show'>
    <?php if ($is_owner):?>
        <button class='btn btn-add-photo' onclick="$('#create-demo-modal').modal('show');">新增照片</button>
    <?php endif; ?>
    <? foreach ($demos as $demo): ?>
        <div class='demo' data-id='<?php echo $demo['demo_id'] ?>'>
           <?php if ($is_owner): ?>            
               <button class='btn edit_demo_button' style='display: none;'>編輯</button>
               <button class='btn btn-danger delete_demo_button' style='display: none;' data-id='<?php echo $demo['demo_id'] ?>'>刪除</button>            
           <?php endif; ?>
        
            <img src='<?php echo image_url($demo['picture_name'], 250, 375) ?>' />
            <a href='/clothing/view/<?php echo $demo['id'] ?>' target='_blank'><h3 class='demo_title'><?php echo $demo['product_title'] ?></h3></a>
            <div class='demo_description'>
               <div class='demo_description_raw' style='display: none;'><?php echo $demo['description'] ?></div>
               <div class='line-height24 font-size12'><?php echo $demo['description_tag'] ?></div>
            </div>
            <div class='demo_bottom'>
                <div class='font-size10'>發表於<?php echo date('Y-m-d', strtotime($demo['created_at'])) ?></div>
                <span class='like_information'>
                <?php if($demo['is_liked']): ?>
                    <a data-id='<?php echo $demo['demo_id'] ?>' class='like_button_disabled' href='#<?php echo $demo['demo_id'] ?>'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                    <span class='num_of_likes font-size10'><?php echo $demo['num_of_likes'] ?></span>
                <?php else: ?>
                    <a data-id='<?php echo $demo['demo_id'] ?>' class='like_button' href='#<?php echo $demo['demo_id'] ?>'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                    <span class='num_of_likes font-size10'><?php echo $demo['num_of_likes'] ?></span>
                <?php endif; ?>
                </span>
            </div>
        </div>
    <? endforeach; ?>
    <?php if (empty($demos)): ?>
    <div class='alert alert-info'>這個SHOW ROOM內還沒有照片喔！</div>
    <?php endif; ?>
    <div style='clear: both;'></div>    
</div>

<div class="modal fade" id='edit-demo-description-modal'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">編輯簡介</h4>
      </div>
      <div class="modal-body">
        <div class="modal-center">
            <form action='/files/upload' method='POST' enctype="multipart/form-data">
                <input type='hidden' name='type' value='product' />              
                <input type='hidden' name='demo_id' />
                <strong>更換照片</strong>
                <input type='file' name='userfile' />
                <strong>描述您的穿搭</strong>                
                <textarea name='edit_description' class='form-control' rows='10' maxlength="50"></textarea><br>
                <input type='submit' class='btn btn-primary' value='確定' />         
            </form>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<style>
    .demo{
        position: relative;
    }
  
    .edit_demo_button{
        position: absolute;
        right: 20px;
        top: 20px;
    }
    .delete_demo_button{
        position: absolute;
        right: 20px;
        top: 60px;
    }
    .demo_bottom{
        position: absolute;
        bottom: 0;
        padding: 10px;
        width: 250px;
    }
    .demo_bottom small{
        color: #999;
    }
    .like_information{
        position: absolute;
        right: 10px;
        top: 0;
        padding: 10px;
        
    }
</style>

<script>
    $(document).ready(function(){

        $('.edit_demo_button').click(function(){
            var content = $(this).parent().find('.demo_description_raw').html();
            $('#edit-demo-description-modal textarea').val(content); 

            var demo_id = $(this).parent().data('id');
            $('#edit-demo-description-modal input[name="demo_id"]').val(demo_id);             
                       
            $('#edit-demo-description-modal').modal('show');      
        });

        $('.delete_demo_button').click(function(){
            var result = confirm('確定刪除？')
            if (!result){
                return;
            }
            var demo_id = $(this).data('id');
            var params = { demo_id: demo_id };
            post_to_url('/closet/delete_demo', params);
        });

      
        $('.demo').hover(function(){
            // $('.demo_description button').show();
            $(this).find('button').show();
        }, function(){
            $(this).find('button').hide();
        });
    });
</script>

<div class="modal fade" id='create-demo-modal'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">新增照片</h4>
      </div>
      <div class="modal-body">
        <div class="modal-center">
            <form action='/files/upload' method='POST' enctype="multipart/form-data">
                <input type='hidden' name='type' value='product' />
                <input type='hidden' name='closet_id' value='<?php echo $closet['id'] ?>' />
                <strong>挑選您的衣服</strong>
                <select class='form-control' name='clothing_id'>
                    <?php foreach ($items as $item): ?>
                        <option value='<?php echo $item['id'] ?>'><?php echo $item['product_title'] ?></option>
                    <?php endforeach; ?>
                </select>
                <strong>上傳一張穿搭照</strong>
                <input id='demo_upload_photo' type='file' name='userfile' />
                <strong>描述您的穿搭</strong>
                <textarea name='description' class='form-control' rows='10' maxlength='50'></textarea>
                <input type='submit' class='btn btn-primary' value='確定' />                
            </form>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    function validate_form(){
        if ($('input[type=file]').val().length){
            return false;
        }
        return true;
    }
    var is_logging = false;    
    <?php if ($is_logging): ?>    
        is_logging = true;
    <?php endif; ?>
  
    $(document).ready(function(){
        $('#create-demo-modal input[type=submit]').click(function(e){
            console.log($('#demo_upload_photo').val().length);
            if ( $('#demo_upload_photo').val().length === 0 ){
                e.stopImmediatePropagation();          
                e.preventDefault();
                alert('請選擇您要上傳的照片');
            }
        })
        
        // initialize the jquery form plugin for sending ajax with input file
        $('#create-demo-modal form').ajaxForm(function(res) {
            
            var item = $.parseJSON(res).upload_data;
            var description = $('#create-demo-modal textarea[name="description"]').val();
            var clothing_id = $('select[name="clothing_id"]').val();
            var closet_id = $('input[name="closet_id"]').val();

            var params = { file_id: item.id, clothing_id: clothing_id, 
                                    description: description, closet_id: closet_id }

            post_to_url('/closet/create_closet_demo', params);
            
        });
        
        // initialize the jquery form plugin for sending ajax with input file
        $('#edit-demo-description-modal form').ajaxForm(function(res) {
            var item = $.parseJSON(res).upload_data;

            var file_id = '';
            if (item!==''){
                file_id = item.id;
            }
            
            var description = $('#edit-demo-description-modal textarea[name="edit_description"]').val();
            var demo_id = $('#edit-demo-description-modal input[name="demo_id"]').val();

            var params = { file_id: file_id, demo_id: demo_id, 
                                    description: description }
            console.log(params);
            
            post_to_url('/closet/update_demo', params);
            
        });
        
        
        
        $('.like_button').click(function(e){
            if (!is_logging){
                $('#logInModal').modal('show');
            }
            else{
                // alert($(this).data('id'));
                var demo_id = $(this).data('id');
                var params = { demo_id: demo_id };
                var $num_of_likes = $(this).next();
                var number = parseInt($num_of_likes.html());
                $num_of_likes.html(number+1);
                $(this).removeClass('like_button');
                $(this).off('click');
                $(this).addClass('like_button_disabled');
                $.ajax({
                  type: "POST",
                  url: "/closet/like",
                  data: params
                });
            }
        })
    });
    
    function post_to_url(path, params, method) {
        method = method || "post"; // Set method to post by default if not specified.

        // The rest of this code assumes you are not using a library.
        // It can be made less wordy if you use one.
        var form = document.createElement("form");
        form.setAttribute("method", method);
        form.setAttribute("action", path);

        for(var key in params) {
            if(params.hasOwnProperty(key)) {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);

                form.appendChild(hiddenField);
             }
        }

        document.body.appendChild(form);
        form.submit();
    }    
</script>
