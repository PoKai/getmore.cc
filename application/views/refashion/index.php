<h1>Let’s Refashion – GetMore徵求你的二手衣新概念</h1>
<form role="form" method='post' action='/refashion/send' enctype="multipart/form-data">
  <div class="form-group">
    <label>Q1 什麼時候擁有第一件二手衣?</label>
    <input type="text" class="form-control" name='q1' required>
  </div>

  <div class="form-group">
    <label>Q2 覺得穿二手衣的好處?</label>
    <input type="text" class="form-control" name='q2' required>
  </div>

  <div class="form-group">
    <label>Q3 如何將二手衣穿得時尚?</label>
    <input type="text" class="form-control" name='q3' required>
  </div>

  <div class="form-group">
    <label>寫下妳的二手衣小故事(最少50個字)</label>
    <textarea class="form-control" rows=10 name='story' required></textarea>
  </div>



  <div class="form-group">
    <label>上傳一張想和大家炫耀的二手衣穿搭</label>
    <input type="file" name='userfile' required>
    <p class="help-block"></p>
  </div>
  
  <div class="form-group">
    <label>你的穿搭照說明</label>
    <input type="text" class="form-control" name='description' required>
  </div>
  
  <button type="submit" class="btn btn-default">送出</button>
</form>
