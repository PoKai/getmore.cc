<div class='fresh-box'>
    <!-- sms postlist -->
    <div getmore-post>
    <? foreach ($postslist as $post): ?>
        <div class="row clr " id="<?=$post['id']?>">
            <span class="replied"></span>
            <div class="from">
                <a href="/closet/view/<?=$post['user_id']+9881;?>" class="avatar" target="_blank">
                    <img src="<?=$post['profile_pic_url']?>">
                </a>
                <a href="/closet/view/<?=$post['user_id']+9881;?>" target="_blank"><?=$post['user_name']?></a>
            </div>
            <div class="subject">
                <div class="line">
                    <b><?=$post['title']?></b>
                    <? if($post['product_id']): ?>
                    <a style="margin-left:10px;font-size:10px;" href="/clothing/view/<?=$post['product_id']?>" target="_blank">查看商品</a> 
                    <? elseif($post['url_code']):?>
                    <a style="margin-left:10px;font-size:10px;" href="/closet/post/<?=$post['url_code']?>" target="_blank">查看衣櫥</a>
                    <? endif;?>
                </div>
                <p class="description magictext"><?=$post['content']?></p>
                <div class="created"><?=$post['created_time']?></div>
                <a class="delete" mid="<?=$post['id']?>" role="receiver">×</a>
                <div class="hidable">
                    <?if($post['last_message']):?>
                    <div class="readmore_messages" data-post_id=<?=$post['id']?>>看其他留言</div>
                    <div class="messages">
                        <div class="reply clr">
                            <img class="avatar" src="<?=$post['last_message'][0]['profile_pic_url'];?>">
                            <div class="subject">
                                <p class="text magictext"><?=$post['last_message'][0]['content']?></p>
                                <div class="created"><?=$post['last_message'][0]['created_time']?></div>
                            </div>
                        </div>
                    </div>
                    <?endif;?>
                    <div class="wrap">
                        <form method="POST" enctype="multipart/form-data" action="/api/posts/new_message" class="message">
                            <textarea name="content"></textarea>
                            <input type="hidden" value="<?=$post['id']?>" name="post_id">
                            <div class="action clr">
                                <!-- <div class="file"> 附加圖片或檔案 <input name="file" type="file"></div> -->
                                <input type="submit" value="回覆訊息">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <? endforeach; ?>
    <form method="POST" enctype="multipart/form-data" action="/api/posts/new_post" class="post">
      <div>
        <label>收件者</label>
        <input type="text" name="receiver">
      </div>
      <div>
        <label>關於</label>
        <input type="text" name="title">
      </div>
      <div>
        <label>訊息內容</label>
        <textarea name="content" rows="7"></textarea>
      </div>
      <div>
        <input type="reset" value="取消">
        <input type="submit" value="送出">
      </div>
    </form>
    </div>
    <div style='clear: both;'></div>    
</div>
