<div class="content order-page">
    <div class="nav">購物車及結帳</div>
    <div class="welcome checkout">
        歡迎來到&nbsp;&nbsp;<img src="/assets/images/logo_s.png" />&nbsp;&nbsp;結帳櫃台，很高興能為您服務！
    </div>
    <div class="all-steps">
        <div class="steps active"><div class="single-step">選擇<br />付費</div></div>
        <div class="steps"><div class="single-step">填寫<br />資料</div></div>
        <div class="steps"><div class="single-step">完成<br />購物</div></div>
    </div>
    <form id="step-1" action="/order/steps/2" method="POST">
    <div class="information">
        <?php if (isset($message) and !empty($message)): ?>
        <?php echo $message; ?>
        <?php endif; ?>
        <?php if(!empty($products)): ?>
        <div class="detail">
            <table class="table table-bordered table-single-order">
                <thead>
                    <tr>
                        <th>商品清單</th>
                        <th>定價</th>
                        <th>會員價</th>
                        <th>數量</th>
                        <th>小計</th>
                        <th width="160">動作</th>
                    </tr>
                </thead>
                <tbody class="product_body">
                <?php
                    foreach($products as $row):
                ?>
                    <tr>
                        <td class="image"><a href="/product/single/<?php echo $row['id'];?>"><img src="<?php echo $row['image_url']; ?>" />&nbsp;<?php echo $row['title']; ?></a></td>
                        <td><del>$<span class="original_price"><?php echo $row['original_price']; ?></span></del></td>
                        <td>$<span class="sell_price"><?php echo $row['sell_price']; ?></span></td>
                        <td><?php echo $row['unit_menu']; ?></td>
                        <td>$<span class="total_price"><?php echo $row['total_price']; ?></span></td>
                        <td><?php if($row['show_update_btn']): ?><button class="btn btn-danger inline update-cart" data-id="<?php echo $row['id']?>" data-type="product" data-o-price="<?php echo $row['original_price']; ?>" data-price="<?php echo $row['sell_price']; ?>" data-unit="<?php echo $row['unit']; ?>">更新數量</button><?php endif;?>&nbsp;<button class="btn btn-danger inline delete-cart" data-update="1" data-id="<?php echo $row['id']?>" data-type="product">移除商品</button></td>
                    </tr>
                <?php
                    endforeach;
                ?>
                </tbody>
            </table>
        </div>
        <div class="price">
            <div class="title product">商品總金額</div>
            <div class="total-price">
                <p id="product_original_price"><?php echo $total_original_product_price; ?></p>
                <p id="product_discount_price"><?php echo $total_original_product_price - $total_product_price; ?></p>
                <p class="yellow product_total_price"><?php echo $total_product_price; ?></p>
            </div>
            <div class="total-price">
                <p>原價金額</p>
                <p>扣除金額</p>
                <p class="yellow">會員價</p>
            </div>
            <div class="clear"></div>
        </div>
        <?php endif; ?>
        <?php if(!empty($events)): ?>
        <div class="detail">
            <table class="table table-bordered table-single-order">
                <thead>
                    <tr>
                        <th>活動清單</th>
                        <th>活動時間</th>
                        <th>活動費</th>
                        <th>報名數</th>
                        <th>小計</th>
                        <th width="160">動作</th>
                    </tr>
                </thead>
                <tbody class="event_body">
                <?php
                    foreach($events as $row):
                ?>
                    <tr>
                        <td class="image"><a href="/event/single/<?php echo $row['id'];?>"><img src="<?php echo $row['image_url']; ?>" />&nbsp;<?php echo $row['title']; ?></a></td>
                        <td><?php echo $row['event_time']; ?></td>
                        <td>$<span class="unit_price"><?php echo $row['price']; ?></span></td>
                        <td><?php echo $row['unit_menu']; ?></td>
                        <td>$<span class="total_price"><?php echo $row['total_price']; ?></span></td>
                        <td><?php if($row['show_update_btn']): ?><button class="btn btn-danger inline update-cart" data-id="<?php echo $row['id']?>" data-type="event" data-price="<?php echo $row['price']; ?>" data-unit="<?php echo $row['unit']; ?>">更新人數</button><?php endif;?>&nbsp;<button class="btn btn-danger inline delete-cart" data-update="1" data-id="<?php echo $row['id']?>" data-type="event">移除活動</button></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="price">
            <div class="title activity">活動選課總金額</div>
            <div class="total-price">
                <p class="yellow event_total_price"><?php echo $total_event_price; ?></p>
            </div>
            <div class="total-price">
                <p class="yellow">活動費</p>
            </div>
            <div class="clear"></div>
        </div>
        <?php endif; ?>
        <div class="price left">
            <a href="/subject/lists/all" class="keep_product">繼續購物</a><a href="/event/lists/all" class="keep_event last-child">繼續活動</a>
        </div>
        <div class="price right">
            <div class="title total">總計</div>
            <div class="total-price">
                <?php if(!empty($total_product_price)): ?><p class="product_total_price"><?php echo $total_product_price; ?></p><?php endif;?>
                <?php if(!empty($total_event_price)): ?><p class="event_total_price"><?php echo $total_event_price; ?></p><?php endif;?>
                <p class="transportation_charge"><?php echo $transportation_charge; ?></p>
                <p class="yellow" id="total_price"><?php echo $total_price; ?></p>
            </div>
            <div class="total-price">
                <?php if(!empty($total_product_price)): ?><p class="yellow">商品總計</p><?php endif;?>
                <?php if(!empty($total_event_price)): ?><p class="yellow">活動總計</p><?php endif;?>
                <p class="yellow">運費總計</p>
                <p class="yellow">總計</p>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="information charge-type footer">
        <div class="charge-info">
            <ul>
                <li class="title">付費方式</li>
                <li class="button payment active" data-type="credit-cart">信用卡</li>
                <li class="button payment" data-type="web-atm">WEB ATM</li>
                <li class="button payment" data-type="atm">ATM 轉帳</li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
    <div class="steps">
        <a href="/" class="back">離開交易</a>
        <a href="#" class="next" data-form="step-1" id="step2">下一步</a>
    </div>
    <input type="hidden" name="payment_method" value="credit-cart">
    </form>
</div>

<div class="order-note">
    <div class="container">
        <div class="right">
            <ul>
                <li>● getmore 金流採用藍新科技解決方案，為台灣目前金流第一品牌。</li>
                <li>● 信用卡刷卡採用藍新科技信賴付款機制，受『128-bit SSL』加密保護。</li>
                <li>● WEBATM 採用第一銀行金流服務，請先準備好您的金融卡及讀卡機。</li>
                <li>● ATM 轉帳付款採用第一銀行虛擬帳號，系統將自動為您辦理入帳手續，您可不需再傳真確認。</li>
                <li>● 如果您有任何疑問，請email至lab@getmore.cc。</li>
            </ul>
        </div>
        <div class="left">
            <h3>付費須知</h3>
        </div>
        <div class="clear"></div>
    </div>
</div>
