<div class="content order-page">
    <div class="nav">購物車及結帳</div>
    <div class="welcome checkout">
        歡迎來到&nbsp;&nbsp;<img src="/assets/images/logo_s.png" />&nbsp;&nbsp;結帳櫃台，很高興能為您服務！
    </div>
    <div class="all-steps">
        <div class="steps"><div class="single-step">選擇<br />付費</div></div>
        <div class="steps"><div class="single-step">填寫<br />資料</div></div>
        <div class="steps active"><div class="single-step">完成<br />購物</div></div>
    </div>
    <div class="steps">
        <a href="/order/single/<?php echo $ordernumber; ?>" class="back">訂單資料</a>
    </div>
</div>

<div class="order-note">
    <div class="container">
        <div class="right">
            <ul>
                <li>● getmore 金流採用藍新科技解決方案，為台灣目前金流第一品牌。</li>
                <li>● 信用卡刷卡採用藍新科技信賴付款機制，受『128-bit SSL』加密保護。</li>
                <li>● WEBATM 採用第一銀行金流服務，請先準備好您的金融卡及讀卡機。</li>
                <li>● ATM 轉帳付款採用第一銀行虛擬帳號，系統將自動為您辦理入帳手續，您可不需再傳真確認。</li>
                <li>● 如果您有任何疑問，請email至lab@getmore.cc。</li>
            </ul>
        </div>
        <div class="left">
            <h3>付費須知</h3>
        </div>
        <div class="clear"></div>
    </div>
</div>
