<div class="content order-page">
    <div class="nav">購物車及結帳</div>
    <div class="welcome checkout">
        歡迎來到&nbsp;&nbsp;<img src="/assets/images/logo_s.png" />&nbsp;&nbsp;結帳櫃台，很高興能為您服務！
    </div>
    <div class="all-steps">
        <div class="steps"><div class="single-step">選擇<br />付費</div></div>
        <div class="steps active"><div class="single-step">填寫<br />資料</div></div>
        <div class="steps"><div class="single-step">完成<br />購物</div></div>
    </div>
    <form id="step-2" action="/order/steps/3" method="POST">
    <div class="information">
        <?php if(!empty($products)): ?>
        <div class="detail">
            <table class="table table-bordered table-single-order">
                <thead>
                    <tr>
                        <th>商品清單</th>
                        <th>定價</th>
                        <th>會員價</th>
                        <th>數量</th>
                        <th>小計</th>
                    </tr>
                </thead>
                <tbody class="product_body">
                <?php
                    foreach($products as $row):
                ?>
                    <tr>
                        <td class="image"><a href="/product/single/<?php echo $row['id'];?>"><img src="<?php echo $row['image_url']; ?>" />&nbsp;<?php echo $row['title']; ?></a></td>
                        <td><del>$<span class="original_price"><?php echo $row['original_price']; ?></span></del></td>
                        <td>$<span class="sell_price"><?php echo $row['sell_price']; ?></span></td>
                        <td><?php echo $row['unit']; ?></td>
                        <td>$<span class="total_price"><?php echo $row['total_price']; ?></span></td>
                    </tr>
                <?php
                    endforeach;
                ?>
                </tbody>
            </table>
        </div>
        <div class="price">
            <div class="title product">商品購物清單</div>
            <div class="total-price">
                <p id="product_original_price"><?php echo $total_original_product_price; ?></p>
                <p id="product_discount_price"><?php echo $total_original_product_price - $total_product_price; ?></p>
                <p class="yellow product_total_price"><?php echo $total_product_price; ?></p>
            </div>
            <div class="total-price">
                <p>原價金額</p>
                <p>扣除金額</p>
                <p class="yellow">會員價</p>
            </div>
            <div class="clear"></div>
        </div>
        <?php endif; ?>
        <?php if(!empty($events)): ?>
        <div class="detail">
            <table class="table table-bordered table-single-order">
                <thead>
                    <tr>
                        <th>活動清單</th>
                        <th>活動時間</th>
                        <th>活動費</th>
                        <th>報名數</th>
                        <th>小計</th>
                    </tr>
                </thead>
                <tbody class="event_body">
                <?php
                    foreach($events as $row):
                ?>
                    <tr>
                        <td class="image"><a href="/event/single/<?php echo $row['id'];?>"><img src="<?php echo $row['image_url']; ?>" />&nbsp;<?php echo $row['title']; ?></a></td>
                        <td><?php echo $row['event_time']; ?></td>
                        <td>$<span class="unit_price"><?php echo $row['price']; ?></span></td>
                        <td><?php echo $row['unit']; ?></td>
                        <td>$<span class="total_price"><?php echo $row['total_price']; ?></span></td>
                    </tr>
                <?php
                    endforeach;
                ?>
                </tbody>
            </table>
        </div>
        <div class="price">
            <div class="title activity">活動清單</div>
            <div class="total-price">
                <p class="yellow event_total_price"><?php echo $total_event_price; ?></p>
            </div>
            <div class="total-price">
                <p class="yellow">活動費</p>
            </div>
            <div class="clear"></div>
        </div>
        <?php endif; ?>
        <div class="price left">
            <a href="/subject/lists/all" class="keep_product">繼續購物</a><a href="/event/lists/all" class="keep_event last-child">繼續活動</a>
        </div>
        <div class="price right">
            <div class="title total">總計</div>
            <div class="total-price">
                <?php if(!empty($total_product_price)): ?><p class="product_total_price"><?php echo $total_product_price; ?></p><?php endif;?>
                <?php if(!empty($total_event_price)): ?><p class="event_total_price"><?php echo $total_event_price; ?></p><?php endif;?>
                <p class="transportation_charge"><?php echo $transportation_charge; ?></p>
                <p class="yellow" id="total_price"><?php echo $total_price; ?></p>
            </div>
            <div class="total-price">
                <?php if(!empty($total_product_price)): ?><p class="yellow">商品總計</p><?php endif;?>
                <?php if(!empty($total_event_price)): ?><p class="yellow">活動總計</p><?php endif;?>
                <p class="yellow">運費總計</p>
                <p class="yellow">總計</p>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="information <?php echo (empty($events)) ? 'footer' : ''; ?>">
        <div class="detail">
        <table>
            <tr>
                <td>
                    <div class="header">訂購人資訊</div>
                    <label>中文姓名</label>
                    <input name="user_name" type="text" value="<?php echo $profile['user_name']; ?>" placeholder="輸入 Email" />&nbsp;&nbsp;<input type="radio" name="user_sex" value="1" checked />&nbsp;先生&nbsp;&nbsp;<input type="radio" name="user_sex" value="0" />&nbsp;小姐
                    <label>手機號碼</label>
                    <input name="user_mobile_phone" type="text" value="<?php echo $profile['mobile_phone']; ?>" placeholder="輸入手機號碼" />
                    <label>聯絡電話</label>
                    <input name="user_telephone" type="text" value="<?php echo $profile['telephone']; ?>" placeholder="輸入家裡電話" />
                    <label>聯絡地址</label>
                    <span id="user_address" class="address"></span>
                </td>
                <td>
                    <div class="header">收件人資訊&nbsp;<input id="add_to_delivery" name="add_to_delivery" data-name="user_name" type="checkbox" value="1" /><span class="delivery">&nbsp;同訂購人</span>&nbsp;<span>※請填寫真實收件人資訊以便收件。</span></div>
                    <label>中文姓名</label>
                    <input name="delivery_name" type="text" value="" placeholder="輸入姓名" />&nbsp;&nbsp;<input class="delivery_sex_man" type="radio" name="delivery_sex" value="1" checked />&nbsp;先生&nbsp;&nbsp;<input type="radio" class="delivery_sex_woman" name="delivery_sex" value="0" />&nbsp;小姐
                    <label>手機號碼</label>
                    <input name="delivery_mobile_phone" type="text" value="" placeholder="輸入手機號碼" />
                    <label>聯絡電話</label>
                    <input name="delivery_telephone" type="text" value="" placeholder="輸入家裡電話" />
                    <label>聯絡地址</label>
                    <span id="delivery_address" class="address"></span>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; padding: 10px 0px;"><input type="radio" name="uniform_invoice_type" value="0" checked />&nbsp;&nbsp;捐贈發票(捐贈單位：創世基金會)或&nbsp;&nbsp;<input type="radio" name="uniform_invoice_type" value="1" />&nbsp;二聯式發票&nbsp;&nbsp;<input type="radio" name="uniform_invoice_type" value="2" />&nbsp;三聯式發票</td>
            </tr>
            <tr class="show_uniform_invoice" style="display:none;">
                <td colspan="2">發票抬頭&nbsp;&nbsp;<input name="uniform_invoice_title" type="text" value="" placeholder="Enter uniform invoice title" /></td>
            </tr>
            <tr class="show_uniform_invoice" style="display:none;">
                <td colspan="2">統一編號&nbsp;&nbsp;<input name="uniform_invoice_number" type="text" value="" placeholder="Enter uniform invoice number" /></td>
            </tr>
        </table>
        </div>
    </div>
    <?php if(!empty($events)): ?>
    <div class="information footer">
        <div class="detail">
            <table class="table table-bordered table-single-order">
                <thead>
                    <tr>
                        <th>圖片</th>
                        <th>活動名稱</th>
                        <th>報名資訊</th>
                        <th>報名人數</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($events as $row): ?>
                    <tr>
                        <td class="image step-2"><img src="<?php echo $row['image_url']; ?>" /></td>
                        <td class="step-2"><?php echo $row['title']; ?></td>
                        <td>
                            <?php for($i=1;$i<=$counts['event'][$row['id']];$i++): ?>
                            <div class="register"><?php echo $i; ?>.&nbsp;姓名&nbsp;<input name="user_name_<?php echo $row['id']; ?>_<?php echo $i; ?>" type="text" value="" placeholder="輸入姓名" />&nbsp;手機&nbsp;<input name="user_phone_<?php echo $row['id']; ?>_<?php echo $i; ?>" type="text" value="" placeholder="輸入電話" /></div>
                            <?php endfor; ?>
                        </td>
                        <td><?php echo $counts['event'][$row['id']]; ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php endif; ?>
    <div class="steps">
        <a href="/order/steps/1" class="back">重選付費方式</a>
        <a href="#" class="next" data-form="step-2" id="step3">下一步</a>
        <input type="hidden" name="payment_method" value="<?php echo $payment_method; ?>">
    </div>
    </form>
</div>

<div class="order-note">
    <div class="container">
        <div class="right">
            <ul>
                <li>● getmore 金流採用藍新科技解決方案，為台灣目前金流第一品牌。</li>
                <li>● 信用卡刷卡採用藍新科技信賴付款機制，受『128-bit SSL』加密保護。</li>
                <li>● WEBATM 採用第一銀行金流服務，請先準備好您的金融卡及讀卡機。</li>
                <li>● ATM 轉帳付款採用第一銀行虛擬帳號，系統將自動為您辦理入帳手續，您可不需再傳真確認。</li>
                <li>● 如果您有任何疑問，請email至lab@getmore.cc。</li>
            </ul>
        </div>
        <div class="left">
            <h3>付費須知</h3>
        </div>
        <div class="clear"></div>
    </div>
</div>
