<div class="content order-page">
    <div class="nav">首頁 &#8250; 會員中心</div>
    <div class="welcome">
        歡迎來到&nbsp;&nbsp;<img src="/assets/images/logo_s.png" />&nbsp;&nbsp;會員中心，很高興能為您服務！
    </div>
    <div class="container">
        <div class="right order">
            <?php if (isset($message) and !empty($message)): ?>
            <?php echo $message; ?>
            <?php else: ?>
            <?php echo $status; ?>
            <?php endif; ?>
            <div class="single">
                <h3>您的訂單編號是: <?php echo $order['order_number']; ?></h3>
                <?php if(!empty($product)): ?>
                <div class="detail">
                    <table class="table table-bordered table-single-order">
                        <thead>
                            <tr>
                                <th>商品名稱</th>
                                <th>定價</th>
                                <th>會員價</th>
                                <th>數量</th>
                                <th>小計</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total_sell_price = $total_original_price = 0;
                            foreach($product as $row):
                                $total_sell_price += (int) ($row['product_sell_price'] * $row['product_quantity']);
                                $total_original_price += (int) ($row['product_original_price'] * $row['product_quantity']);
                            ?>
                            <tr>
                                <td><a href="/product/single/<?php echo $row['product_id']; ?>"><?php echo $row['product_title']; ?></a></td>
                                <td><del>$<?php echo (int) $row['product_original_price']; ?></del></td>
                                <td>$<?php echo (int) $row['product_sell_price']; ?></td>
                                <td><?php echo (int) $row['product_quantity']; ?></td>
                                <td>$<?php echo (int) ($row['product_quantity'] * $row['product_sell_price']); ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="price">
                    <div class="title product">商品購物清單</div>
                    <div class="total-price">
                        <p><?php echo $total_original_price; ?></p>
                        <p><?php echo $total_original_price - $total_sell_price; ?></p>
                        <p class="yellow"><?php echo $total_sell_price; ?></p>
                    </div>
                    <div class="total-price">
                        <p>原價金額</p>
                        <p>扣除金額</p>
                        <p class="yellow">會員價</p>
                    </div>
                    <div class="clear"></div>
                </div>
                <?php endif; ?>
                <?php if(!empty($event)): ?>
                <div class="detail">
                    <table class="table table-bordered table-single-order">
                        <thead>
                            <tr>
                                <th>活動名稱</th>
                                <th>活動時間</th>
                                <th>活動費</th>
                                <th>報名人數</th>
                                <th>小計</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total_event_price = 0;
                            foreach($event as $row):
                                $total_event_price += (int) ($row['event_price'] * $row['event_quantity']);
                            ?>
                            <tr>
                                <td><a href="/event/single/<?php echo $row['event_id']; ?>"><?php echo $row['event_title']; ?></a></td>
                                <td><?php echo $row['event_time']; ?></td>
                                <td>$<?php echo (int) $row['event_price']; ?></td>
                                <td><?php echo (int) $row['event_quantity']; ?></td>
                                <td>$<?php echo (int) $row['event_price'] * $row['event_quantity']; ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="price">
                    <div class="title activity">活動選課清單</div>
                    <div class="total-price">
                        <p class="yellow"><?php echo $total_event_price; ?></p>
                    </div>
                    <div class="total-price">
                        <p class="yellow">活動費</p>
                    </div>
                    <div class="clear"></div>
                </div>
                <?php endif; ?>
                <div class="price">
                    <div class="title total">總計</div>
                    <div class="total-price">
                        <p><?php echo $total_sell_price; ?></p>
                        <p><?php echo $total_event_price; ?></p>
                        <p><?php echo intval($order['transportation_charge']); ?></p>
                        <p class="yellow"><?php echo $total_sell_price + $total_event_price + $order['transportation_charge']; ?></p>
                    </div>
                    <div class="total-price">
                        <p class="yellow">商品總計</p>
                        <p class="yellow">活動總計</p>
                        <p class="yellow">運費總計</p>
                        <p class="yellow">總計</p>
                    </div>
                    <div class="clear"></div>
                </div>
                <h3>付款方式與寄送資訊</h3>
                <div class="detail">
                    <p>付款方式&nbsp;:&nbsp;<?php echo $payment_method; ?></p>
                    <?php if ($order['payment_method'] == PAYMENT_ATM): ?>
                    <p>匯款銀行&nbsp;:&nbsp;<?php echo $order['bank_id']; ?></p>
                    <p>匯款帳戶&nbsp;:&nbsp;<?php echo $order['virtual_account']; ?></p>
                    <?php endif; ?>
                    <p>收件姓名&nbsp;:&nbsp;<?php echo $order['delivery_name']; ?></p>
                    <p>寄送地址&nbsp;:&nbsp;<?php echo $order['delivery_address']; ?></p>
                    <p>發票類型&nbsp;:&nbsp;<?php echo $uniform_invoice; ?></p>
                </div>
                <div style="margin-top: 20px; text-align: center;">
                    <?php if ($show_pay_button): ?>
                    <a class="btn btn-primary btn-large check-payment" data-form="payment" data-price="<?php echo (int) $order['total_price']?>" data-id="<?php echo $order['order_number']?>" style="color: white;"><i class="icon-ok icon-white"></i> 線上付款</a>
                    <?php endif; ?>
                    <a class="btn btn-primary btn-large" style="color: white;" href="/order/lists"><i class="icon-ok icon-white"></i> 回訂單列表</a>
                </div>
            </div>
        </div>
        <div class="left">
            <ul>
                <li class="menu">會員中心</li>
                <li>
                    <ul>
                        <li class="sub-menu"><a href="/auth/edit">會員資料</a></li>
                        <li class="sub-menu"><a href="/order/lists" class="active">訂單查詢</a></li>
                    </ul>
                </li>
                <li class="menu">聯絡我們</li>
            </ul>
        </div>
        <p class="clear"></p>
    </div>
</div>

<div class="order-note">
    <div class="container">
        <div class="right">
            <ul>
                <?php if ($order['payment_method'] == PAYMENT_ATM): ?>
                <li>請點選訂單查詢，內有匯款詳細資訊。</li>
                <li>當您匯款後，系統將自動為您辦理入帳手續，您可不需再傳真確認。</li>
                <li>提醒您，若2日內未收到您的匯款，系統將自動取消此訂單。</li>
                <?php endif; ?>
                <?php if ($order['payment_method'] == PAYMENT_WEB_ATM): ?>
                <li>感謝您的WEBATM付款。</li>
                <li>當您匯款後，系統將自動為您辦理入帳手續，您可不需再傳真確認。</li>
                <?php endif; ?>
                <?php if ($order['payment_method'] == PAYMENT_CREDIT_CARD): ?>
                <li>感謝您的信用卡付款。</li>
                <li>getmore 將儘速處理您的訂單。</li>
                <?php endif; ?>
                <li>謝謝您的惠顧，getmore仍保有決定是否接受訂單及出貨與否之權利。</li>
                <li>如果您有任何疑問，請email至lab@getmore.cc。</li>
            </ul>
        </div>
        <div class="left">
            <h3>注意事項</h3>
        </div>
        <div class="clear"></div>
    </div>
</div>
