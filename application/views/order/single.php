<div class="content order-page">
    <div class="container" style="width:100%;">
        <div class="right order" style="width:100%; margin: 0; padding: 0; background-color:transparent;">
            <div class="single">
                <h3>您的訂單編號是: <?php echo $order['order_number']; ?></h3>
                <?php if(!empty($product)): ?>
                <div class="detail">
                    <table class="table table-bordered table-single-order">
                        <thead>
                            <tr>
                                <th>商品名稱</th>
                                <th>定價</th>
                                <th>會員價</th>
                                <th>數量</th>
                                <th>小計</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total_sell_price = $total_original_price = 0;
                            foreach($product as $row):
                                $total_sell_price += (int) ($row['product_sell_price'] * $row['product_quantity']);
                                $total_original_price += (int) ($row['product_original_price'] * $row['product_quantity']);
                            ?>
                            <tr>
                                <td><?php echo $row['product_title']; ?></td>
                                <td><del>$<?php echo (int) $row['product_original_price']; ?></del></td>
                                <td>$<?php echo (int) $row['product_sell_price']; ?></td>
                                <td><?php echo (int) $row['product_quantity']; ?></td>
                                <td>$<?php echo (int) ($row['product_quantity'] * $row['product_sell_price']); ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="price">
                    <div class="title product">商品購物清單</div>
                    <div class="total-price">
                        <p><?php echo $total_original_price; ?></p>
                        <p><?php echo $total_original_price - $total_sell_price; ?></p>
                        <p class="yellow"><?php echo $total_sell_price; ?></p>
                    </div>
                    <div class="total-price">
                        <p>原價金額</p>
                        <p>扣除金額</p>
                        <p class="yellow">會員價</p>
                    </div>
                    <div class="clear"></div>
                </div>
                <?php endif; ?>
                <?php if(!empty($event)): ?>
                <div class="detail">
                    <table class="table table-bordered table-single-order">
                        <thead>
                            <tr>
                                <th>活動名稱</th>
                                <th>活動時間</th>
                                <th>活動費</th>
                                <th>報名人數</th>
                                <th>小計</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total_event_price = 0;
                            foreach($event as $row):
                                $total_event_price += (int) ($row['event_price'] * $row['event_quantity']);
                            ?>
                            <tr>
                                <td><?php echo $row['event_title']; ?></td>
                                <td><?php echo $row['event_time']; ?></td>
                                <td>$<?php echo (int) $row['event_price']; ?></td>
                                <td><?php echo (int) $row['event_quantity']; ?></td>
                                <td>$<?php echo (int) $row['event_price'] * $row['event_quantity']; ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="price">
                    <div class="title activity">活動選課清單</div>
                    <div class="total-price">
                        <p class="yellow"><?php echo $total_event_price; ?></p>
                    </div>
                    <div class="total-price">
                        <p class="yellow">活動費</p>
                    </div>
                    <div class="clear"></div>
                </div>
                <?php endif; ?>
                <div class="price">
                    <div class="title total">總計</div>
                    <div class="total-price">
                        <p><?php echo $total_sell_price; ?></p>
                        <p><?php echo $total_event_price; ?></p>
                        <p><?php echo intval($order['transportation_charge']); ?></p>
                        <p class="yellow"><?php echo $total_sell_price + $total_event_price + $order['transportation_charge']; ?></p>
                    </div>
                    <div class="total-price">
                        <p class="yellow">商品總計</p>
                        <p class="yellow">活動總計</p>
                        <p class="yellow">運費總計</p>
                        <p class="yellow">總計</p>
                    </div>
                    <div class="clear"></div>
                </div>
                <h3>付款方式與寄送資訊</h3>
                <div class="detail">
                    <p>付款方式&nbsp;:&nbsp;<?php echo $payment_method; ?></p>
                    <p>收件姓名&nbsp;:&nbsp;<?php echo $order['delivery_name']; ?></p>
                    <p>寄送地址&nbsp;:&nbsp;<?php echo $order['delivery_address']; ?></p>
                    <p>發票類型&nbsp;:&nbsp;<?php echo $uniform_invoice; ?></p>
                </div>
            </div>
        </div>
<!--
        <div class="left">
            <ul>
                <li class="menu">會員中心</li>
                <li>
                    <ul>
                        <li class="sub-menu"><a href="/auth/edit">會員資料</a></li>
                        <li class="sub-menu"><a href="/order/lists" class="active">訂單查詢</a></li>
                    </ul>
                </li>
                <li class="menu">聯絡我們</li>
            </ul>
        </div>
-->
    </div>
</div>
