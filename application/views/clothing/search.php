<script src="/assets/javascript/howtomakeaturn/clothing.js"></script>

      <img src='/assets/images/banner/search.jpg' />

    <!-- fresh product -->
    <div class="fresh">
      <span class="fresh-title">
              <?php echo $keyword ?>
      </span>
    <div class='fresh-sequence pull-right'>  
      <select class="form-control floatright sort" name='sort'>
        <option value='new'>最新上架優先</option>
        <option value='early'>最早上架優先</option>
        <option value='l'>價格由低至高</option>
        <option value='h'>價格由高至低</option>
        <option value='rec'>推薦優先</option>
      </select>
      <div class='floatright marginright10'>商品排序</div>
    </div>
    <div style="clear: both;"></div>
        <div class="fresh-box">
        <?php foreach ($clothings as $clothing):?>  
          <?php echo clothing_thumbnail_layout($clothing); ?>
        <?php endforeach;?>
        </div>
        
        <?php if(!count($clothings)): ?>
            <div class='alert alert-success'>沒有搜尋結果。</div>
            <center>
              <br>
              <h5 class="getmoregreen">可以試試分類頁左側好用的篩選器來找衣服喔！</h5><br>
              <div class="btn-group">
              <a href="http://getmore.cc/clothing/category/women" class="btn btn-primary marginleft10 marginright10">WOMEN</a>
              <a href="http://getmore.cc/clothing/category/men" class="btn btn-primary marginleft10 marginright10">MEN</a>
              <a href="http://getmore.cc/clothing/category/kids" class="btn btn-primary marginleft10 marginright10">KIDS</a>
              </div><br><br><br>
            </center>
        <?php endif; ?>
        <div style='clear: both;'></div>
    </div>
    <!-- fresh product -->
    <div style='clear: both;'></div>
<script type="text/javascript">
  
    $(function () {
        LikeButtonModule.initialize();
        <?php if($logined): ?>
            LikeButtonModule.setIsLogined(true);
        <?php endif; ?>
    });

    $(document).ready(function(){
        getParameterByName('s') && $('.sort').val(getParameterByName('s'));
      
        $('.sort').change(function(){
              var uri = 'http://' + window.location.hostname + 
                  window.location.pathname + '?s=' + $('.sort').val();
                  
              window.location = uri;

        })
      

      
    });

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

</script>


