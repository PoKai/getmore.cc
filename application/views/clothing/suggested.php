<script src="/assets/javascript/howtomakeaturn/clothing.js"></script>

      <img src='/assets/images/banner/superclothes-01.jpg' />

    <!-- fresh product -->
    <div class="fresh">
      <span class="fresh-title">
          GetMore 推薦 / SUPER CLOTHES
      </span>
        <div class="pull-right getmore-pagination"><?php echo $pagination; ?></div>        
        <div style='clear: both;'></div>
      
        <div class="fresh-box">
        <?php foreach ($clothings as $clothing):?>  
          <?php echo clothing_thumbnail_layout($clothing); ?>
        <?php endforeach;?>
        </div>
        <div style='clear: both;'></div>
        <div class="pull-right getmore-pagination"><?php echo $pagination; ?></div>        
    </div>
    <!-- fresh product -->
    <div style='clear: both;'></div>

<style>
    .getmore-pagination{
        padding-right: 20px;
        color: #19a190!important;
    }
    .getmore-pagination a{
        color: #19a190;
    }
</style>

<script type="text/javascript">
  
    $(function () {
        LikeButtonModule.initialize();
        <?php if($logined): ?>
            LikeButtonModule.setIsLogined(true);
        <?php endif; ?>
    });
</script>
