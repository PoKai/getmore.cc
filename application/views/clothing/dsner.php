<style>

.pagetitle{
  font-size: 18px;
  line-height: 200%;
  letter-spacing: 3px;
  margin-left: 30px;
  font-weight: bold;
}
.pagecentertitle{
  font-size: 18px;
  line-height: 200%;
  letter-spacing: 3px;
  font-weight: bold;
}

.fifthrow {
  overflow: auto;
  text-align: center;
  margin-top: 20px;
  padding: 20px;
  border-bottom: 1px dashed #000000;
  border-top: 1px dashed #000000;
  min-height: 160px;
}
.plandescription {
  overflow: auto;
  text-align: center;
  padding: 20px;
  border-bottom: 1px dashed #000000;
  min-height: 160px;
  margin-bottom:30px;
}
.pageabouttitle{
  margin: 0 auto;
  width: 230px;
  border-bottom: 2px solid #000000;
  letter-spacing: 5px;
  text-align: center;

}
.pageaboutbox{
  margin-top: 15px;
}
.pageaboutp{
  font-size: 14px;
  font-weight: bold;
  letter-spacing: 5px;
  line-height: 25px;
}

.sixthrow{
  overflow: auto;
  text-align: center;
  margin-top: 0px;
  margin-bottom: 20px;
  padding: 20px;
//  min-height: 160px;
  border-bottom: 2px solid #888;
}
.pageliketitle{
  margin: 0 auto;
  width: 330px;
  border-bottom: 2px solid #000000;
  letter-spacing: 5px;
  text-align: center;
}

.pagelikebox{
  overflow: auto;
  margin: 20px auto 0px auto;
  display:inline-block; 
}

.pagelikefacebook ,.pagelikeinstagram{
  float: left;
  margin-right: 20px;
}

.pagelikeyoutube{
  float: left;
}

a.pagelikea{
  color: #333;
  text-decoration: none;
}
a.pagelikea:hover{
  color: #000;
  text-decoration: none;
}

#newContainer {
  z-index: 101;
  background-color: #fff;
  width: 1000px !important;
  margin: 0 auto;
  padding-top: 10px;
}
#newfooter{
  z-index: 101;
  width: 1000px !important;
  margin: 0 auto;
  padding-top: 10px;
}
.fresh-box {
  width: 1000px;
  padding: 0 20px;
  margin-top: 20px;
  overflow: auto;
}
.sellerbanner{
  float: left;
  width: 300px;
  margin: 10px 10px;
}

</style>
<div id="newbannerContainer"style="display: block;">
    <div class="plandescription">
      <div class="pageabouttitle" style="width:400px;"><font class="pagecentertitle">設計師品牌/Designers </font></div>
      <div class="pageaboutbox">
        <div class="pageaboutp">匯集亞洲傑出時裝設計師，來自台灣、香港等地，</div>
        <div class="pageaboutp">直接向他們購買，穿出獨特。</div>
      </div>
    </div>
    <div style="height:700px;">
    <img src="http://blog.getmore.cc/wp-content/uploads/2015/05/designer_brand.jpg">
    </div>      
  </div>
  <div id="newContainer"style="display: block;">
    <div class="fifthrow">
      <div class="pageabouttitle"><font class="pagecentertitle">ABOUT GETMORE</font></div>
      <div class="pageaboutbox">
        <div class="pageaboutp">GetMore是一個以「共享經濟」為概念的時尚分享網站</div>
        <div class="pageaboutp">在這裡，你可以輕鬆享受二手衣帶來的自由、獨特 、環保等時尚樂趣</div>
        <div class="pageaboutp" style="margin-top:10px;">Enjoy Pre-owed Fashion on GetMore Now!</div>
      </div>
    </div>
    <div class='sixthrow'>
    <div class='pageliketitle'><font class='pagecentertitle'>GET MORE GETMORE ON</font></div>
    <div class='pagelikebox'>
        <div class='pagelikefacebook'><a href='https://www.facebook.com/getmore.cc' class='pagelikea' target='_blank'><i class="fa fa-facebook fa-3x"></i></a></div>
        <div class='pagelikeinstagram'><a href='https://instagram.com/getmorecc/' class='pagelikea' target='_blank'><i class="fa fa-instagram fa-3x"></i></a></div>
        <div class='pagelikeyoutube'><a href='https://www.youtube.com/channel/UCMFEkClqi18ge_EItQa0lAg/feed' class='pagelikea' target='_blank'><i class="fa fa-youtube-square fa-3x"></i></a></div>
    </div>
</div>
  </div>
 