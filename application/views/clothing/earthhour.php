<div class='eh-banner'>
    <div class='countdown_container'>
      <!--
        <div data-expired='2014-3-30 18:00:00' class="defaultCountdown"></div>
        <span class='eh-unit'>日</span>
        <span class='eh-unit'>時</span>
        <span class='eh-unit'>分</span>
        <span class='eh-unit'>秒</span>
        -->
    </div>
</div>
<div class='eh-description'>
    <div class='row'>
        <div class='col-xs-4'>
            <h4>減碳三步驟</h4><br />
        </div>    
    </div>
    
    <div class='row'>
        <div class='col-xs-4'>
          <h2>Step 1</h2><br />
          <a class='btn btn-primary' href='http://getmore.cc/clothing/category/new' style='background-color: #4DBAA9; padding: 10px 20px;' target="_blank">買二手衣</a>
        </div>
        <div class='col-xs-6'>
            <h4>擁有一件獨一無二的二手衣</h4><br />
            <p class='step-description'>
                穿二手衣不但能減少資源的浪費,還能展現獨特自我。<br />
                利用二手衣作穿搭,即是一種對於環境友善的行動。<br />
                來GetMore挑件屬於你的好衣吧!
            </p>
        </div>
    </div>
    <br />

    <div class='row'>
        <div class='col-xs-4'>
          <h2>Step 2</h2><br />
          <a class='btn btn-primary' href='https://earthhour.wwf.org.hk/' style='background-color: #1fbdd6; padding: 10px 20px;' target="_blank">Earth Hour</a>
          
        </div>
        <div class='col-xs-6'>
            <h4>為地球熄燈一小時</h4><br />
            <p class='step-description'>
                3/29(六)20:30 留住地球,捨出未來!<br />
                我們相約一起熄燈一小時,呵護你我生活的地方。<br />
                一起加入吧:)
            </p>
        </div>
    </div>
    <br />

    <div class='row'>
        <div class='col-xs-4'>
          <h2>Step 3</h2><br />
          <h4  style='color: #4DBAA9;'>Just do it!</h4>
        </div>
        <div class='col-xs-6'>
            <h4>這一步，由你自己決定！</h4><br />
            <p class='step-description'>
                開始使用環保筷、不開強冷氣、不要讓水龍頭流個不停....<br />
                改變生活小細節，地球就能更美好，<br />
                減碳生活，這一步由你開始！
            </p>
        </div>
    </div>

</div>

<style>
    body{
        background-color: black;
    }
  
    .eh-banner{
        width: 980px;
        height: 477px;
        background-image: url(/assets/banner/earthhour-02.png);
    }
    
    #header{
        background-color: black;
    }
    
    .logoarea a, .menuarea a, .eh-description{
        color: white !important;
    }
    .pokai-hr{
        border-color: white;
    }
    
    .container{
        background-color: black;
    }
    .eh-description{
        padding: 40px;
    }
    .countdown_container{
        padding-top: 130px;
        padding-left: 105px;
    }
    .eh-digit, .eh-unit{
        width: 70px;
        display: inline-block;
        font-size: 40px;
        padding: 0 10px;
        margin: 0 20px;
        color: white;
        text-align: center;
    }
    .eh-digit{
        padding-top: 30px;
        padding-bottom: 30px;
        border: 1px solid #4DBAA9;      
    }
    .eh-digit-reverse{
        border: 1px solid #4DBAA9;      
        background-color: #4DBAA9;
        color: white;
      
    }
    .eh-unit{
        margin-top: 10px;
        font-size: 30px;
        margin-right: 17px;
    }
    .step-description{
        line-height: 3em;
        font-weight: bold;
        font-size: 12px;
    }
    
</style>
<script type="text/javascript">
    $(function () {
        $('.logoarea .pull-left a img').attr('src', '/assets/images/earthhour-03.png');

        /*
        $('.defaultCountdown').each(function(){
            $(this).countdown({ until: new Date('March 29, 2014 20:30:00'), 
//                                                  layout: '{mn}:{sn}',
                                                    layout: '<span class="eh-digit eh-digit-reverse">{dn}</span>'+
                                                    '<span class="eh-digit">{hn}</span>'+
                                                    '<span class="eh-digit">{mn}</span>'+
                                                    '<span class="eh-digit">{sn}</span>',
                                                  expiryUrl: '/member/order'
                                             });
        })
        */
    });
</script>
