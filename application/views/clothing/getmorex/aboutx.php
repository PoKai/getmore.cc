<div class='stretch banner_stretch'>
    <img src='/assets/images/getmorex/aboutxbanner.png' />
</div>
<div class='banner_shadow'>

</div>

<img src='/assets/images/getmorex/aboutx/aboutx-06.png' />
<hr class='shadow_hr' />

<div class='aboutx_container'>

    <h3>環保 X 獨特 X 質感 = 二次時尚:環保時尚,忠於自我</h3>
    <p>主簡約、華麗、復古、幾何、優雅......跨越時代的流行元素與文化,自由組合、碰撞。在GetMore二次時尚,每件好衣都是獨
一無二,你可以在這裡找到各種風格、各個時代的流行元素,你可以找到屬於自己的時尚好衣,並且用環保、實惠的方式
擁有它。不必屈服於「快速時尚」、「高級時尚」的潮流下,你可以環保、自由地為自己打造「二次時尚」的生活。</p>
    <img src='/assets/images/getmorex/aboutx/aboutx-01.png' />


    <h3>專為時尚人士劃分的品牌特區</h3>
    <p>主打知名品牌與設計師款，以更高標準、條件去做篩選，並在「GetMore三個承諾」(保證清洗、12道嚴格篩選條件、輕
    度使用)保證下，全區皆為高品質時尚好衣,以提高時尚、質感、價值三方層次，更加傳達「穿二手衣是一種時尚」的
    生活態度。</p>
    <img src='/assets/images/getmorex/aboutx/aboutx-02.png' />

    <h3>知名品牌 X 完美價格 = 令人心動的購物體驗</h3>
    <p>只要用平均2.5折的價格，就能買到現下知名品牌、設計師款的時尚好衣。令你心動的品牌好衣，你可以用完美的價格買
    到它，我們會用貼心的包裝送到你手上，相信你打開看到好衣時，會有再次心動的感覺。</p>
    <img src='/assets/images/getmorex/aboutx/aboutx-03.png' />

    <h3>貼心的包裝</h3>
    <p>我們用最高規格對待您的購買，包括防塵紙、訂單明細、品牌標籤(未拆標商品)、貼紙，以及一個專屬的紙提袋。</p>
    <img src='/assets/images/getmorex/aboutx/aboutx-04.png' />

    <h3>三個承諾</h3>
    <p>GetMore以「三個承諾」–保證清洗、12道嚴格篩選條件、輕度使用,保證全區為高品質時尚好衣。</p>
    <img src='/assets/images/getmorex/aboutx/aboutx-05.png' />

    <h3>How We Choose</h3>
    <p>嚴選各大知名品牌與設計師款，包括世界名牌、百貨公司專櫃、國內知名品牌、知名設計師等等。</p>
</div>
<style>
    .shadow_hr{
        display: block;
        border: 1px dashed black;
        margin-bottom: 50px;
    }
    .aboutx_container h3, .aboutx_container p{
      padding: 20px;
    }
    .aboutx_container h3{
      font-weight: bold;
    }
    .aboutx_container p{
        font-size: 20px;
        line-height: 2;
    }
    
</style>
