<div class='stretch banner_stretch'>
    <img src='/assets/images/getmorex/brandlistbanner.png' />
</div>
<div class='banner_shadow'>

</div>

<div class='brand_list_container'>
    <h1 class='text-center'>Brand list</h1>
    <h2 class='text-center'>Famous Brand & Designer Sytle</h2>
    <hr />
    <div class='row'>
        <div class='col-md-4'>
            <ul>
                <li>0918</li>
                <li>22 OCTOBRE</li>
                <li>23區</li>
                <li>45RPM</li>
                <li>7AGE</li>
                <li>7 FOR ALL MANKIND</li>
                <li>8 HAPPY</li>
            </ul> 
       
          <h3>A</h3>
          <ul>
              <li>Abercrombie & Fitch</li>
              <li>ADIEU TRISTESSE</li>
              <li>Adriano Goldschmied (無袖需全新)</li>
              <li>Adrienne Vittadini</li>
              <li>agnes b.</li>
              <li>a la sha</li>
              <li>Alexander McQueen</li>
              <li>Alexander Wang</li>
              <li>Alice + Olivia</li>
              <li>Allibelle</li>
              <li>American Apparel</li>
              <li>AMERICAN BLUEDEER</li>
              <li>Amour Vert</li>
              <li>ANAYI</li>
              <li>Ann Taylor</li>
              <li>Anna Sui</li>
              <li>Anthropologie</li>
              <li>ANNE FONTAINE</li>
              <li>Aqua</li>
              <li>AQUASCUTUM</li>
              <li>ARCHIVES</li>
              <li>Ark & Co</li>
              <li>Armani</li>
              <li>ARNOLD PALMER</li>
              <li>Aryn K</li>
              <li>AS KNOW AS</li>
              <li>ASOS</li>
              <li>Audrey 3+1</li>
              <li>AZUR BY JAPAN</li>
          </ul>      
          <h3>B</h3>
          <ul>
              <li>Balenciaga</li>
              <li>Banana Republic(不包括短T)</li>
              <li>Barneys New York</li>
              <li>B.CLUB</li>
              <li>BCBGMAXAZRIA</li>
              <li>bebe</li>
              <li>Bergdorf Goodman</li>
              <li>Betsey Johnson</li>
              <li>Billabong(短T、無袖需全新)</li>
              <li>Blank NYC</li>
              <li>Bloomingdale’s</li>
              <li>Blu Pepper</li>
              <li>Blue Bird</li>
              <li>Boden</li>
              <li>Bottega Veneta</li>
              <li>BP by Nordstrom</li>
              <li>BREAD N BUTTER</li>
              <li>Brooks Brothers</li>
              <li>Burberry</li>
          </ul>
          <h3>C</h3>
          <ul>
              <li>C&C California(無袖、短T需全新)</li>
              <li>Calvin Klein</li>
              <li>Catherine Malandrino</li>
              <li>CARTIER</li>
              <li>Celine</li>
              <li>Chanel</li>
              <li>Charlie Jade</li>
              <li>Chaser</li>
              <li>Cheap Monday</li>
              <li>Chloe</li>
              <li>Christian Dior</li>
              <li>Christopher Kon</li>
              <li>Citizens of Humanity</li>
              <li>Classiques Entier by Nordstrom</li>
              <li>Club Monaco</li>
              <li>Coach</li>
              <li>Coldwater Creek</li>
              <li>COTELAC</li>
              <li>Collective Concepts</li>
              <li>Costa Blanca</li>
              <li>CRAIG</li>
              <li>Current Elliott</li>
              <li>Cynthia Rowley</li>
              <li>Cynthia Steffe</li>
          </ul>
          <h3>D</h3>
          <ul>
              <li>DAKS</li>
              <li>Diane Von Furstenberg</li> 
              <li>Diesel</li> 
              <li>Diffa </li>
              <li>DKNY </li>
              <li>DL1961 </li>
              <li>Dolce&Gabbana </li>
              <li>DONNA HSU </li>
              <li>Donna Karan </li>
              <li>Donna Morgan </li>
              <li>Dries Van Noten </li>
              <li>DV by Dolce Vita </li>

          </ul>
          <h3>E</h3>
          <ul>
              <li>Earnest Sewn </li>
              <li>earth music&ecology </li>
              <li>ECCO </li>
              <li>ECI by Nordstrom</li> 
              <li>Eddie Bauer </li>
              <li>E hyphen world gallery</li> 
              <li>ef-d'e </li>
              <li>Eight Sixty </li>
              <li>Eileen Fisher</li> 
              <li>Elan </li>
              <li>Elie Tahari </li>
              <li>Elizabeth and James</li> 
              <li>Ella Moss </li>
              <li>Emilio Pucci </li>
              <li>en-suey </li>
              <li>Equipment </li>
              <li>EPISODE </li>
              <li>ERMENEGILDO ZEGNA </li>
              <li>Etoile Isabel Marant</li> 
              <li>E WEAR </li>
              <li>Express </li>
              <li>EXPERIENCE </li>
          </ul>
          <h3>F</h3>
          <ul>
              <li>Fate</li>
              <li>Fendi</li>
              <li>Finders Keepers</li>
              <li>flounce tbd by Nordstrom</li>
              <li>Fossil</li>
              <li>Free People</li>
              <li>French Connection</li>
              <li>FRIULOS</li>
          </ul>
        </div>
        <div class='col-md-4'>
            <h3>G</h3>
            <ul>
                <li>Gap(x tee&tops)</li> 
                <li>Galoop(x T) </li>
                <li>Generra </li>
                <li>Genetic Denim </li>
                <li>Gentle Fawn </li>
                <li>GERARD DAREL</li> 
                <li>Givenchy </li>
                <li>GIMO'S </li>
                <li>GLAM </li>
                <li>GOZO </li>
                <li>Greylin </li>
                <li>Gucci </li>
                <li>GUESS </li>
                <li>Gypsy Junkies</li>         
            </ul>
            <h3>H</h3>
            <ul>
                <li>Halogen by Nordstrom</li>
                <li>Hazel</li>
                <li>Helmut Lang</li>
                <li>HER&HIM</li>
                <li>HERMES</li>
                <li>Hinge by Nordstrom</li>
                <li>HUANG SHU CHI 黃淑琦</li>
                <li>Hudson</li>
                <li>Hugo BOSS</li>
            </ul>
            <h3>I</h3>
            <ul>
                <li>iiMK</li>
                <li>INED</li>
                <li>INDIVI</li>
                <li>IN</li>
                <li>i prefer</li>
                <li>IRIS</li>
                <li>iROO</li>
                <li>I. Madeline</li>
                <li>INC International Concepts</li>
                <li>Isabel Marant</li>
            </ul>
            <h3>J</h3>
            <ul>
                <li>JAMEI CHEN 陳季敏</li>
                <li>J Brand</li>
                <li>J.Crew</li>
                <li>J.Jill</li>
                <li>Jaloux</li>
                <li>James Jeans</li>
                <li>James Perse</li>
                <li>Jean Paul Gaultier</li>
                <li>Jessica Simpson</li>
                <li>JIS</li>
                <li>Jill Sander</li>
                <li>JOSEPH</li>
                <li>JOSEPH&TINA</li>
                <li>Joe's Jeans</li>
                <li>Johnny Was</li>
                <li>Joie</li>
                <li>Jones New York</li>
                <li>Juicy Couture</li>
            </ul>
            <h3>K</h3>
            <ul>
                <li>Kate Spade</li>
                <li>Kate Spade Saturday</li>
                <li>Keepsake</li>
                <li>Kenneth Cole</li>
                <li>Kensie</li>
                <li>Ksubi</li>
                <li>KUT from the Kloth</li>
            </ul>
            <h3>L</h3>
            <ul>
                <li>LA COLLECTION</li>
                <li>L.A.M.B</li>
                <li>Lacoste</li>
                <li>Ladakh</li>
                <li>Lands’ End</li>
                 <li>Lanvin</li>
                <li>Laundry By Design</li>
                <li>Laundry By Shelli Segal</li>
                <li>Leith by Nordstrom</li>
                <li>LeSportsac</li>
                <li>Levi’s</li>
                 <li>Liebeskind</li>
                <li>Lilly Pulitzer</li>
                <li>Line & Dot</li>
                <li>Lord & Taylor</li>
                <li>Louis Vuitton</li>
                <li>Lovers + Friends</li>
                <li>Lucca Couture</li>
                <li>Lucky Brand</li>
            </ul>
            <h3>M</h3>
            <ul>
                <li>Madewell</li>
                <li>Maggy London</li>
                <li>MANGO</li>
                <li>Marc Jacobs</li>
                <li>MARC New York</li>
                <li>Mark & James by Badgley Mischka</li>
                <li>Marni</li>
                <li>Matty M</li>
                <li>Max Azria Collection</li>
                <li>Max Studio</li>
                <li>Michael Kors</li>
                <li>Michael Stars</li>
                <li>MiH Jeans</li>
                <li>Miilla</li>
                <li>Milly</li>
                <li>MINKPINK</li>
                <li>Missoni</li>
                <li>Miu Miu</li>
                <li>MM Couture by Miss Me</li>
                <li>Mother Jeans</li>
                <li>Mulberry</li>
            </ul>
            <h3>N</h3>
            <ul>
                <li>Nanette Lepore</li>
                <li>Neiman Marcus</li>
                <li>Nic+Zoe</li>
                <li>Nicole Miller</li>
                <li>non stop</li>
                <li>NATURALLY JO JO</li>
                <li>Nordstrom</li>
                <li>North Face</li>
                <li>Not Your Daughters Jeans</li>
            </ul>
        </div>
        <div class='col-md-4'>
            <h3>O</h3>
            <ul>
                <li>Obey</li>
                <li>Olive & Oak</li>
                <li>Olivo</li>
                <li>OLIVE des OLIVE</li>
                <li>Oscar de la Renta</li>
                <li>OZOC</li>
            </ul>
            <h3>P</h3>
            <ul>
                <li>Paige Denim</li>
                <li>Parker</li>
                <li>Patterson J. Kincaid</li>
                <li>Pendleton</li>
                <li>Pink Martini Collection</li>
                <li>Pink Stitch</li>
                <li>Plastic Island</li>
                <li>Prada</li>
                <li>Proenza Schouler</li>
                <li>P*S COMPANY</li>
            </ul>
            <h3>R</h3>
            <ul>
                <li>Rachel Pally</li>
                <li>Rachel Roy</li>
                <li>Rag & Bone</li>
                <li>Ralph Lauren</li>
                <li>Rebecca Minkoff</li>
                <li>Rebecca Taylor</li>
                <li>Reed Krakoff</li>
               <li>REICO HSU 許瑋玲</li>
                <li>Reiss</li>
                <li>Rich & Skinny</li>
                <li>Robert Rodriguez</li>
                <li>Roberto Cavalli</li>
                <li>Roots</li>
                <li>Rory Beca</li>
                <li>Roxy</li>
                <li>Rugby Ralph Lauren</li>
            </ul>
            <h3>S</h3>
            <ul>
                <li>S.Y.L.K.</li>
                <li>Saint Laurent</li>
                <li>Saks Fifth Avenue</li>
                <li>Sam & Lavi</li>
                <li>SERGIO GIROMBELLI</li>
                <li>SISLEY</li>
                <li>SCOTTISH HOUSE</li>
                <li>Sky</li>
                <li>Somedays Lovin</li>
                <li>Something Else by Natalie Wood</li>
                <li>Splendid</li>
                <li>Stella McCartney</li>
                <li>Suzi Chin for Maggy Boutique</li>
                <li>Sweaty Betty</li>
            </ul>
            <h3>T</h3>
            <ul>
                <li>Talbots</li>
                <li>Ted Baker</li>
                <li>THE EMPORIUM</li>
                <li>The Limited</li>
                <li>Theory</li>
                <li>THML</li>
                <li>THEME</li>
                <li>Three Dots</li>
                <li>Tibi</li>
                <li>Title Nine</li>
                <li>Timberland 天柏嵐</li>
                <li>Tod’s</li>
                <li>Toi et Moi</li>
                <li>Tommy Bahama</li>
                <li>Tommy Hilfiger</li>
                <li>Topshop</li>
                <li>Tory Burch</li>
                <li>TOUGH Jeansmith</li>
                <li>Tracy Reese</li>
                <li>Trina Turk</li>
                <li>Trinity</li>
                <li>Trouve</li>
                <li>True Religion</li>
                <li>Tulle</li>
                <li>TUAN TUAN COMME DES GARCONS</li>
                <li>Twelfth St by Cynthia Vincent</li>
            </ul>
            <h3>U</h3>
            <ul>
                <li>United Colors of Benetton</li>
                <li>Urban Outfitters</li>
                <li>URE            </li>
            </ul>
            <h3>V</h3>
            <ul>
                <li>Valentino</li>
                <li>Velvet</li>
                <li>Vera Wang</li>
                <li>Versace</li>
                <li>Viereck</li>
                <li>VIF</li>
                <li>Vince</li>
                <li>Vince Camuto</li>
                <li>Vivienne Westwood</li>
                <li>V_K            </li>
            </ul>
            <h3>W</h3>
            <ul>
                <li>WHIPLE</li>
                <li>White House Black Market</li>
                <li>William Rast</li>
                <li>Willow & Clay</li>
                <li>Woodleigh</li>
                <li>Wyatt     </li>       
            </ul>

            <h3>Y</h3>
            <ul>
                <li>Yigal Azrouel</li>
                <li>Young Fabulous & Broke</li>
                <li>Yves Saint Laurent </li>
            </ul>
            <h3>Z</h3>
            <ul>
                <li>Zac Zac Posen</li>
                <li>Zara(coat)              </li>
            </ul>
            <h3> </h3>
            <ul>
                <li>得華新意 </li>
            </ul>
        </div>
    </div>
</div>

<style>
    .brand_list_container h1{
        margin-top: 100px;
//        font-weight: bold;
        font-size: 60px;
    }
    .brand_list_container h2{
        font-size: 10px;
        margin-bottom: 100px;
    }
    .brand_list_container hr{
        display: block;
        border: 1px dashed black;
        margin-bottom: 50px;
    }
    .brand_list_container{
        padding-bottom: 100px;
    }
    .brand_list_container h3, .brand_list_container p, .brand_list_container a{
        padding-left: 40px;
    }
    .brand_list_container h3{
          font-weight: bold;
          color: #4DBAA9;
          font-size: 30px;
          margin-top: 30px;
    }
</style>
