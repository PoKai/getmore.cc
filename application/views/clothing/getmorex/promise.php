<div class='stretch banner_stretch'>
    <img src='/assets/images/getmorex/promisebanner.png' />
</div>
<div class='banner_shadow'>

</div>
    
<img src='/assets/images/getmorex/promise/Promise-05.png' />

<div class='row promise_container'>
    <div class='col-md-2 col-md-offset-1'>
        <img src='/assets/images/getmorex/promise/Promise-02.png' />
    </div>
    <div class='col-md-7 col-md-offset-1'>
        <p>在GetMore的每件衣服都會送到專業的洗衣公司
        嚴格清洗,全程採用環保洗劑,衣物洗滌後不殘
        留任何危害健康的化學物質。</p>
    </div>
</div>

<div class='row promise_container'>
    <div class='col-md-2 col-md-offset-1'>
        <img src='/assets/images/getmorex/promise/Promise-03.png' />
    </div>
    <div class='col-md-7 col-md-offset-1'>
        <ul>
            <li>無污漬</li>
            <li>無嚴重脫線</li>
            <li>無破損</li>
            <li>無縮水</li>
            <li>無褪色</li>
            <li>無裂紗/勾紗</li>
            <li>無裝飾脫落</li>
            <li>無圖案掉漆/龜裂</li>
            <li>無起毛球</li>
            <li>非貼身衣物</li>
            <li>非仿冒品</li>
            <li>符合GetMore品牌清單</li>
        </ul>    
        <a href='/clothing/getmorex/brand_list'>>Brand List</a>
    </div>  
  
  
</div>


<div class='promise_container'>
    <div class='col-md-2 col-md-offset-1'>
        <img src='/assets/images/getmorex/promise/Promise-04.png' />
    </div>
    <div class='col-md-7 col-md-offset-1'>
        <p>全區一律為賣家購買5年以內,幾近全新、全新、全新未拆吊牌
        好衣。</p>
    </div>
</div>
<style>
    .promise_container{
        border-top: 1px dashed black;
        padding-top: 50px;
        padding-bottom: 50px;
    }
    .promise_container p{
        font-size: 20px;
        line-height: 2;
        padding-top: 35px;
    }
    .promise_container ul{
        line-height: 2;
        font-size: 20px;
    }
    .promise_container a{
      color: #4DBAA9;
      padding: 40px;
      font-size: 20px;
    }
</style>
