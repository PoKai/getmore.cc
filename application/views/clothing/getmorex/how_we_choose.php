<div class='stretch banner_stretch'>
    <img id='howwechoose' src='/assets/images/getmorex/howwechoosebanner.png' />
</div>
<div class='banner_shadow'>

</div>

<div class='howwechoose_container'>
    <h1 class='text-center'>我們如何挑選...</h1>
    <h2 class='text-center'>HOW WE CHOOSE</h2>
    <hr />
    <h3>知名品牌 & 設計師款</h3>
    <p>嚴選各大知名品牌與設計師款,包括世界名牌、百貨公司專櫃、國內知名品牌、知名設計師等等。</p>
    <a href='/clothing/getmorex/brand_list'>>> see Brand List</a>

    <h3>篩選條件 & 衣服狀況</h3>
    <p>必須通過12道篩選條件,好衣狀況必須是幾近全新、全新、全新未拆吊牌。</p>
    <a href='/clothing/getmorex/promise'>>> GetMore Promise</a>
</div>

<style>
    .howwechoose_container h1{
        margin-top: 100px;
//        font-weight: bold;
        font-size: 40px;
    }
    .howwechoose_container h2{
        margin-bottom: 100px;
    }
    .howwechoose_container hr{
        display: block;
        border: 1px dashed black;
        margin-bottom: 50px;
    }
    .howwechoose_container{
        padding-bottom: 100px;
    }
    .howwechoose_container h3, .howwechoose_container p, .howwechoose_container a{
        padding-left: 40px;
    }
    .howwechoose_container h3{
          font-weight: bold;
          margin-top: 100px;
    }
    .howwechoose_container p{
        font-size: 20px;
        margin-top: 40px;
    }
    .howwechoose_container a{
        font-size: 60px;
        color: #4DBAA9;
        font-style: italic;
        margin-top: 100px;
        display: block;
    }
</style>
