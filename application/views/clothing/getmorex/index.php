

<div class='stretch' id='brand_stretch'>
<div id="main-carousel" class="carousel slide carousel-margin" data-ride="carousel">
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <a href="/clothing/getmorex/index"><img src="/assets/images/getmorex/brandbanner/brandbanner-01.png"></a>
    </div>

    <div class="item">
      <a href="/clothing/getmorex/index"><img src="/assets/images/getmorex/brandbanner/brandbanner-02.png"></a>
    </div>

    <div class="item">
      <a href="/clothing/getmorex/index"><img src="/assets/images/getmorex/brandbanner/brandbanner-03.png"></a>
    </div>

    <div class="item">
      <a href="/clothing/getmorex/index"><img src="/assets/images/getmorex/brandbanner/brandbanner-04.png"></a>
    </div>

    <div class="item">
      <a href="/clothing/getmorex/index"><img src="/assets/images/getmorex/brandbanner/brandbanner-05.png"></a>
    </div>

  </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#main-carousel" data-slide="prev">
      <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#main-carousel" data-slide="next">
      <span class="icon-next"></span>
    </a>
  </div>  
</div>
<div id='brand_shadow'>
    <!-- this div is overlaid by the banner. notice the banner is in absolute position -->
</div>

<img src='/assets/images/getmorex/LevelUp980.png' />
<div class='stretch'>
    <img id='perfectprice' src='/assets/images/getmorex/perfectprice1280.png' />
</div>
<div id='perfectprice_shadow'>
    <!-- this div is overlaid by the banner. notice the banner is in absolute position -->
</div>
    <img id='new_arrival' src='/assets/images/getmorex/newarrival.png'>
    
    <!-- fresh product -->
    <div class="fresh">
      <div class="fresh-box">
      <?php foreach ($clothings as $clothing):?>  
        <?php echo clothing_thumbnail_layout($clothing); ?>
      <?php endforeach;?>
      </div>
    </div>
    <!-- fresh product -->
    <div style='clear: both;'></div>
<a class='new_arrival_link' href='/clothing/getmorex/new'>more...</a>
<div class='stretch' id='adbanner_stretch'>
    <div class='link_container'>
        <!-- <a class='btn btn-primary btn-lg' href='/clothing/getmorex/special'>see more</a> -->
        <a class='btn btn-primary btn-lg' target='_blank' href='/clothing/category/women/#type-women&page-1&subtype-12&style-8'>see more</a>
    </div>
    
    <img id='adbanner' src='/assets/images/getmorex/adbanner1280.png' />
</div>
<div id='adbanner_shadow'>
    <!-- this div is overlaid by the banner. notice the banner is in absolute position -->
</div>

<div class='stretch' id='promise_stretch'>
    <div class='link_container'>
        <a class='btn btn-primary btn-lg' href='/clothing/getmorex/promise'>learn more</a>
    </div>  
    <img id='promise' src='/assets/images/getmorex/GetMorePromise1280.png' />
</div>
<div id='promise_shadow'>
    <!-- this div is overlaid by the banner. notice the banner is in absolute position -->
</div>
<div class='stretch'>
    <img id='fashion' src='/assets/images/getmorex/fashionav1280.png' />
</div>
<div id='fashion_shadow'>
    <!-- this div is overlaid by the banner. notice the banner is in absolute position -->
</div>

<div class='stretch' id='howwechoose_stretch'>
    <div class='link_container'>
        <a class='btn btn-primary' href='/clothing/getmorex/how_we_choose'>learn more</a>
    </div>  
    <img id='howwechoose' src='/assets/images/getmorex/howwechoose980.png' />
</div>
<div id='howwechoose_shadow'>
    <!-- this div is overlaid by the banner. notice the banner is in absolute position -->
</div>
<div class='stretch' id='brand_list_stretch'>
    <div class='link_container'>
        <a href='/clothing/getmorex/brand_list'>see all ></a>
    </div>  
    <img id='brand_list'src='/assets/images/getmorex/Brandlist1280.png' />
</div>
<div id='brand_list_shadow'>
    <!-- this div is overlaid by the banner. notice the banner is in absolute position -->
</div>




<style>
    /**
     * set shadow element height for getmorex index page
     * --------------------------------------------------------------------------------------
     */

    /* shadow elements are the workaround
        for absolute position overlaid problem  */
        
    #perfectprice_shadow{
        height: 585px;
    }
    #adbanner_shadow{
        height: 745px;
    }
    #promise_shadow{
        height: 645px;
    }
    #brand_shadow{
        height: 630px;
    }
    #fashion_shadow{
        height: 345px;
    }
    #howwechoose_shadow{
        height: 365px;
    }
    #brand_list_shadow{
        height: 485px;
    }
    #promise_stretch{
        background-color: #4DBAA9;      
    }
    #brand_stretch{
        height: 630px;
        background-color: #B64F8E;
    }
    #howwechoose_stretch{
        background-color: white;
    }
    #brand_list_stretch{
        background-color: white;
    }
    
    #new_arrival{
        display: block;
        margin: 0 auto;
        margin-top: 100px;
    }
    
    #adbanner_stretch .link_container{
        top: 650px;
        left: 1000px;
    }
    #adbanner_stretch .link_container a{
        background-color: #4DBAA9;
    }
    
    #promise_stretch .link_container{
        top: 580px;
        left: 580px;
    }
    #promise_stretch .link_container a{
        background-color: #4DBAA9;
        border: 1px solid white;
    }
    
    #howwechoose_stretch .link_container{
        top: 230px;
        left: 800px;
    }
    #howwechoose_stretch .link_container a{
        background-color: #4DBAA9;
        border: 1px solid white;
    }
    
    #brand_list_stretch .link_container{
        top: 350px;
        left: 780px;
    }
    #brand_list_stretch .link_container a{
        font-size: 60px;
        color: black;
        font-weight: normal;
    }
    .new_arrival_link{
        display: block;
        text-align: center;
        padding: 50px;
        font-weight: bold;
        font-size: 20px;
        color: black;
    }    
    

    
</style>
