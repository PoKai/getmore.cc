<script src="/assets/javascript/howtomakeaturn/clothing.js"></script>
<!-- view -->
<link rel="stylesheet" href="/assets/css/clothing_view.css">
<link rel="stylesheet" href="/assets/font-awesome-4.0.3/css/font-awesome.min.css">

<div id='clothing-main'>
    <div class='go-left'>
        <a href='/clothing/view/<?php echo $previous_clothing_id ?>'><img src='/assets/images/productleft.png' /></a>
    </div>
    <div class='go-right'>
        <a href='/clothing/view/<?php echo $next_clothing_id ?>'><img src='/assets/images/productright.png' /></a>
    </div>   
    <div class="product-view" >

        <div class="product-image">
            <a href='<?= image_url($clothing['files'][0], 'large');?>' class="product-main-image" title='<?=$clothing['product_title'];?>' rel="gal1">
                <img src="<?= image_url($clothing['files'][0], 'medium');?>" id="product-main-image" title='<?=$clothing['product_title'];?>' />
            </a>
            <div style='clear: both;'></div>
            
            <?php
              foreach ($clothing['files'] as $file_id){
            ?>
              <a class="product-other-image" href="javascript:void(0);" rel="{gallery: 'gal1', smallimage: '<?= image_url($file_id, 'medium');?>',largeimage: '<?= image_url($file_id, 'large');?>'}">
                <img src="<?= image_url($file_id, 'medium');?>" />
              </a>
            <?php
              }
            ?>
            
            <a data-toggle="modal" href="#size-modal" class="product-size-image">
                <img src="<?php echo $clothing['measure_img_url'] ?>" />
            </a>
        
        
         </div><!-- END the left part.-->

    <div id='clothing-information'>
        <strong>
          <p class="product-breadcrumb">
            <a href="/">首頁</a>
               > 
              <?php if (substr($clothing['product_type'], 0, 1) == '1'): ?>
                  <a href="/clothing/category/women">WOMEN</a>
                  <?php $href = "/clothing/category/women"; ?>
              <?php endif; ?>
              <?php if (substr($clothing['product_type'], 0, 1) == '2'): ?>
                  <a href="/clothing/category/men">MEN</a>
                  <?php $href = "/clothing/category/men"; ?>
              <?php endif; ?>
              <?php if (substr($clothing['product_type'], 0, 1) == '3'): ?>
                  <a href="/clothing/category/kids">KIDS</a>
                  <?php $href = "/clothing/category/kids"; ?>
              <?php endif; ?>
              >
              <?php
                  $subtype = substr($clothing['product_type'], 0, 2);
                  $hash = '#subtype-' . $subtype;
                  $subtype_name_mapping = array(
                      '11' => '上衣',
                      '12' => '洋裝',
                      '15' => '外套',
                      '13' => '褲子',
                      '14' => '裙子',

                      '21' => '上衣',
                      '22' => '襯衫',
                      '24' => '外套',
                      '23' => '褲子',
                      
                      '31' => '連身衣',
                      '32' => '上衣',
                      '33' => '下身',
                      '34' => '洋裝',
                      '35' => '外套'                      
                  );
              ?>
              <a href="<?php echo $href . $hash ?>"><?php echo $subtype_name_mapping[$subtype] ?></a>
           </p>
        </strong>
          <p id='clothing_json_data' class='hidden'><?php echo json_encode($clothing) ?></p>
          <h2>
              <?=$clothing['product_title'];?>
              <?php if ($clothing['is_suggested']): ?>
              &nbsp;&nbsp;<img class='recommendation_icon' src="/assets/images/superrecommend.png">    
              <?php endif; ?>
              <section class='social-funcs'>
                  <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo current_url() ?>" target='_blank' class="new-social-share-button facebook">
                      <i class="fa fa-facebook"></i>
                  </a>
                  <a href="https://twitter.com/intent/tweet?url=<?php echo current_url() ?>&amp;text=♥<?php echo $clothing['product_title'] ?>" target='_blank' class="new-social-share-button twitter">
                      <i class="fa fa-twitter"></i>
                  </a>
                  <a target='_blank' href="http://pinterest.com/pin/create/button/?media=<?php echo clothing_image_url(explode(',', $clothing['files'])[0], 'large') ?>&amp;url=<?php echo current_url() ?>&amp;description=♥<?php echo $clothing['product_title'] ?>" class="new-social-share-button pinterest">
                      <i class="fa fa-pinterest"></i>
                  </a>
                  <div class='fbiframe floatright'>
                  <iframe src="//www.facebook.com/plugins/like.php?href=<?php echo current_url() ?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21&amp;appId=239678122794926" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:70px; height:21px;" allowTransparency="true"></iframe>
                  </div>
              </section>
          </h2>

          <section id='highlight-section'>
              <p class='highlight-information'>
                  Size：<?=$clothing['size_type_major'];?>
                  <?=$clothing['size'];?>
                  <?php if($clothing['size_number_minor'] != ''): ?>
                      /
                      <?=$clothing['size_type_minor'];?>
                      <?=$clothing['size_number_minor'];?>
                  <?php endif; ?>


                  <?php if($clothing['size_comment'] != ''): ?>
                      (<?=$clothing['size_comment'];?>)
                  <?php endif; ?>
                  &nbsp;&nbsp;&nbsp;


                  <?php if (substr($clothing['product_type'], 0, 1) == '1'): ?>
                      <a data-toggle="modal" href="#sizetable-modal" class='learn-more'><img class="qmark-adjust" src='/assets/images/icon-qmark_light.png' />&nbsp;Learn more</a>
                  <?php endif; ?>
                  <?php if (substr($clothing['product_type'], 0, 1) == '2'): ?>
                      <a data-toggle="modal" href="#sizetable-modal" class='learn-more'><img class="qmark-adjust" src='/assets/images/icon-qmark_light.png' />&nbsp;Learn more</a>
                  <?php endif; ?>
                  <?php if (substr($clothing['product_type'], 0, 1) == '3'): ?>
                  
                  <?php endif; ?>
                  
                  
              </p>
              <p class='highlight-information'>品牌：<?=$clothing['product_brand'];?></p>
          </section>
          
          <p class='product-condition'>              
              狀態驗收：<?php echo $clothing['item_status']?>
              <span class='item_status_comment'><?php echo get_item_status_comment($clothing['item_status']) ?></span>
          </p>
          <?php if($clothing['admin_comment'] != ''): ?>              
              <p class='product-additional'>點評： <?=$clothing['admin_comment'];?></p>                        
          <?php endif; ?>

          <?php if($clothing['admin_remark'] != ''): ?>
              <p class='product-additional'>備註： <?=$clothing['admin_remark'];?></p>                        
          <?php endif; ?>          
          
          <?php foreach ($clothing['additional_fields'] as $key => $value):?>
              <p class='product-additional'><?php echo $key ?>： <?php echo $value ?></p>          
          <?php endforeach; ?>


          <section id='checkout-section'>

              <section id='checkout-prices'>
                  <h3>NT$<?=$clothing['product_sell_price'];?></h3>
                  <p>參考原價：<?= get_price_label($clothing['product_original_price']); ?>元</p>
              </section>

              <section id='checkout-cart'>
                  <div>

                      <?php if ( !$this->session->userdata('user_id') ): ?>
                      <?= post_user_message_modal($owner_id, current_url());?>
                      <a data-toggle="modal" href="#logInModal"><button type="button" class="btn product-view-buy" style="width:54px;">留言</button></a>
                      <?php endif?>
                      <?php if ( $this->session->userdata('user_id') && $this->session->userdata('user_id') != $owner_id ): ?>
                      <?= post_user_message_modal($owner_id, current_url());?>
                      <a data-toggle="modal" href="#userpostmsg"><button type="button" class="btn product-view-buy" style="width:54px;">留言</button></a>
                      <?php endif ?>

                      <?php if($clothing['lock_status'] == '4' || $clothing['publication_time'] > date('c')): ?>
                        <a class="btn product-view-buy" disabled='disabled'>準備上架中</a>
                      <?php elseif($clothing['lock_status'] == '0'): ?>
                          <button id="product-view-buy" class="btn product-view-buy">Add to Cart</button>
                      <?php elseif ($clothing['lock_status'] == '2'): ?>
                          <button id="product-view-buy" class="btn product-view-buy" disabled='disabled'>已售出</button>                    
                      <?php elseif ($clothing['lock_status'] == '1'): ?>
                          <button id="product-view-buy" class="btn product-view-buy" disabled='disabled'>已預訂</button> 
                      <?php elseif ($clothing['lock_status'] == '3'): ?>
                          <button id="product-view-buy" class="btn product-view-buy" disabled='disabled'>已下架</button>
                      <?php endif ?>
                  </div>
                  
                  <div id='add_to_favorite'>
                      <?php if($clothing['isLike']): ?>
                          <a class='liked' href='#' data-id='<?php echo $clothing['id'];?>'>已加入欲望清單！</a>
                      <?php else: ?>
                          <a class='to_like' href='#' data-id='<?php echo $clothing['id'];?>'>加入我的欲望清單！</a>
                      <?php endif; ?>
                      <?if($clothing['likes_count']):?><span class="likes_count">(<em><?=$clothing['likes_count'];?>人已加入</em>)</span><?endif;?>
                  </div>
                  <div id='checkoutcloset'>
                  &nbsp;&nbsp;&nbsp;<a href='/closet/view/<?php echo $owner_id + 9881 ?>' target="_blank">看看這位賣家的衣櫃！</a>
                  </div>
              </section>
          </section>
          
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" id='clothing-reminding'>
            <li class="active"><a href="#home" data-toggle="tab">商品說明</a></li>
            <li><a href="#profile" data-toggle="tab">配送方式</a></li>
            <li><a href="#messages" data-toggle="tab">貼心小叮嚀</a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content" id='other-information'>
            <div class="tab-pane active" id="home">
                <div class='row'>
                    <div class='col-md-6 clothing-reminding-detail'>
                        <strong><p>商品編號： <?=$clothing['product_item_number'];?></p>
                        <p>
                            款式： 
                            <?php if (empty($clothing['pattern_name'])): ?>
                                <!-- only kids 洋裝、連身衣 -->
                                <?php echo $subtype_name_mapping[$subtype] ?>
                            <?php else: ?>
                                <?php echo $clothing['pattern_name'] ?>
                            <?php endif; ?>          
                        
                        </p>                
                        <p> <?php if (!empty($clothing['material'])) echo '材質：' . $clothing['material'] ?></p>
                        <p> <?php if (!empty($clothing['location']))  echo '產地：' . $clothing['location'] ?></p>
                        <p> <?php if (!empty($clothing['color']))  echo '顏色：' . '<img src="/admin/assets/images/colorbox/' . $clothing['color'].'.png">' ?></p>                
                        <?php if(!empty($clothing['style_names'])): ?>
                            <p>風格： 
                            <?php foreach($clothing['style_names'] as $style_name){
                                echo '<span class="product-view-special product-style">' . $style_name . '</span>';
                              }
                            ?>
                            </p>
                        <?php  endif; ?>
                        </strong>
      
                            <div style='clear: both;'></div>
                          <ul class='product-view-ul font-size10'>
                            <li>尺寸數據會因其布料彈性、水洗處理、測量起訖點等因素而出現些微誤差; 誤差值在±2公分內視為合理範圍。</li>
                            <li>每件商品都是在專業攝影棚拍攝，相當於陽光下採光，且在不同螢幕上可能產生色差，一律以實品顏色為準。</li>
                          </ul>
                        
                    </div>
                    <div class='col-md-6'>
                        <div class='alert alert-info bggetmorecolor'>
                            <div class='row'>
                            <a class='pull-right' data-toggle="modal" href="#size-modal"><img src='/assets/images/icon-qmark_light.png' /></a>
                            </div>
                            <div class='row'>
                                <div class='col-md-6'>
                                    <p class='size-modal-text'>詳細尺寸</p>
                                    <br />
                                    <p class='size-modal-text'>尺寸為單面平放測量，都是GetMore仔細丈量的喔！</p>
                                </div>
                               <strong> 
                                <div class='col-md-6'>

                                    <?php $measure_labels = get_semantic_measure_name($clothing['size_code'], $clothing['measure']) ?>                                        
                                    <?php  foreach($measure_labels as $key => $label): ?>
                                        <p class='size-modal-text'><?php  echo $key . '：' . $label ?>cm</p>
                                    <?php  endforeach; ?>                                    
                                </div>
                              </strong>
                            </div>                        
                        </div>
                    </div>
                </div>
            </div><!-- 商品說明 -->
            
            <div class="tab-pane" id="profile">
            
                <p>每筆訂單皆由國內最專業的黑貓宅急便進行配送。</p>
                <ul class='font-size12 product-view-ul-p'>
                <li>配送範圍：目前配送範圍僅限台灣本島，不便之處敬請見諒。</li>
                <li>配送時間：常態的出貨時間為：今天下單，隔天出貨，後天就可以收到您訂購的衣物。
                <br/> (您可至【會員中心】—>【訂單查詢】來查看訂單處理進度喔!)</li>
                <li>運費計算：宅急便配送到府：單筆消費金額未滿千元，運費一律80元。
                 <br/>單筆消費金額1,000元以上，免運費。</li>
                 <ul>
            
            </div>
            <div class="tab-pane" id="messages">
                
                <p class='getmore-color'>二次時尚的生活，從擁有一件二手好衣開始 ：）</p>
                <ul class='font-size12 product-view-ul-p'>
                <li>為了避免因商品尺寸不合，耽誤您寶貴的時間，請在訂購前務必先確認各款尺寸。</li>
                <li>在GetMore，所有衣服都是獨一無二，請留給真心喜歡的人，減少退貨，也是節能減碳愛護地球的一種方式喔！  </li> 
                </ul>         
                <img class='pull-right' src='/assets/images/content/buy/product-07.png' />
                <div style='clear: both;'></div>
            </div>
          </div>
        </div>
    </div>
    <div style='clear: both;'></div>
</div>

    <div style="clear: both;"></div>
  <img src="/assets/images/content/buy/product-06.png" />
  <?php if (!empty($clothings)): ?>
  <div id='similar_suggestion_title'>看看同款式的衣服</div>      
  <div class='suggested-box'>
    <ul id='target'>
        <?php foreach ($clothings as $item):?>
            <li>
                <a target='_blank' href='/clothing/view/<?= $item->id; ?>'>
                    <div class='suggested-item-original-image'><img src="<?= image_url($item->files[0]); ?>" /></div>
                    <div class='suggested-item-title'><?= $item->product_title; ?></div>
                    <div class='suggested-item-original-price'>NT$ <?= get_price_label($item->product_original_price); ?></div>
                    <div class='suggested-item-selling-price'><span>NT$ <?= $item->product_sell_price; ?></span></div>
                </a>
            </li>
        <?php endforeach;?>
    </ul>
  </div>
  <?php endif; ?>
  <img class='getmorecenter-block' src="/assets/images/content/sell/getmoreheart.jpg" />
        <script>
          $('#post_submit').click(function(event){
            event.preventDefault();
            var actionUrl = "/restful/post";

            var tempData, i , tempReceiver;
            tempReceiver = $( '#owner_id' ).val();
            //$( '#receiver' ).val( "999" );
            var tempData = $( '#message_post' ).serialize();
//////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////// check content
   
   var message_title = $('#message_title').val();
   var message_content = $('#message_content').val();
   document.getElementById("message_title").style.background = ( message_title == "" ) ? "#FFFFCC" : "#FFFFFF";
   document.getElementById("message_content").style.background = ( message_content == "" ) ? "#FFFFCC" : "#FFFFFF";

   var alert_message = "系統偵測到可能是私下交易的文字。為了交易安全以及平台的權益，嚴禁私下交易喔！";
   var re = /[0-9]{6,}|電話|私下交易|面交|手續費|[零一二三四五六七八九]{6,}|[零壹貳參肆伍陸柒捌玖]{6,}|直接跟你買|直接聯絡|傳真|e[-]{0,1}mail|E[-]{0,1}mail|Line|line|一手交錢一手交貨|歡迎來店選購/;
   if (re.test(message_title) || re.test(message_content)){
    alert( alert_message );
    return;
   }

   re =/^([a-z0-9_\.-]+\@[\da-z\.-]+\.[a-z\.]{2,6})$/gm; // mail
   if (re.test(message_title) || re.test(message_content)){
    alert( alert_message );
    return;
   }
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
            var formData;
                formData = new FormData(document.getElementById("message_post"));

                $.ajax({
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    url: actionUrl,
                    data: formData,
                    dataType: 'json'
                }).done(function(post){
                    formData.append("post_id", post['_id']);

                    $.ajax({
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        url: "/restful/post/message",
                        data: formData,
                        dataType: 'json'
                    }).done(function(post){
                        $.ajax({
                            type: 'POST',
                            url: '/post/change_message_value',
                            data: {post_id: post['post_id'], read: 'true'},
                            dataType: 'json'
                         });
                        console.log("success");
                        location.reload();
                        // form.reset();
                        // var html = message_template(post);
                        // $(form).before(html);
                    }).fail(function(error){
                        console.log(error);
                    });

                    console.log();
                }).error(function(){
                    console.log("error");
                });
          });
        </script>

    <script type="text/javascript" src="/assets/javascript/grassshrimp/posts.js"></script>
    <script>        
        $(document).ready(function(){
            $('#target').lunZhuan({
                itemClass: 'suggested-item',
                buttonClass: 'suggested-item-button'
            });
            
            $('.new-social-share-button').click(function(e){
                e.preventDefault();
                window.open($(this).attr('href'), 'just_new', config='height=600,width=600');
            });
        });
    </script>

    <style>
        .go-left{
            position: absolute;
            top: 200px;
            margin-left: -70px;
        }
        .go-right{
            position: absolute;
            top: 200px;
            right: -70px;
        }
                      
        .new-social-share-button {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-transition: all .15s ease-in-out;
            -moz-transition: all .15s ease-in-out;
            transition: all .15s ease-in-out;
            display: inline-block;
            text-align: center;
            padding-top: 2.5px;
            width: 24px;
            height: 24px;
            font-size: 15px;
            border-radius: 99px;
            background: #f5f5f5;
            color: #333
        }

        .new-social-share-button.facebook:hover {
            background: #45629d
        }

        .new-social-share-button.twitter:hover {
            background: #55acee
        }

        .new-social-share-button.tumblr:hover {
            background: #529ecc
        }

        .new-social-share-button.pinterest:hover {
            background: #cb2027
        }

        .new-social-share-button:hover{
            color: white;
        }
        
        .item_status_comment{
            font-size: 12px;
            color: #bbbbbb;
        }
        
    </style>
        
</div>
      <script>
          $(".product-other-image").click(function(){
              var src = $(this).children().attr("src");
              $("#product-main-image").attr("src", src);
              console.log(src);
          });
          
$(document).ready(function(){  
    var options = {  
            zoomType: 'standard',  
            lens:true,  
            preloadImages: true,  
            alwaysOn:false,  
            zoomWidth: 500,  
            zoomHeight: 400,  
            xOffset:90,  
          //  yOffset:30,  
            position:'left',
            showEffect: 'fadein'
            // hideEffect: 'fadeout'
            //...MORE OPTIONS  
    };  
    $('.product-main-image').jqzoom(options);  
});            
      </script>

<script type="text/javascript">
  
    $(function () {
        LikeButtonModule.setupLink();
        <?php if($logined): ?>
            LikeButtonModule.setIsLogined(true);
        <?php endif; ?>
    });
</script>


<div class="modal fade" id='size-modal'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">詳細尺寸</h4>
      </div>
      <div class="modal-body" id='size-modal-body'>
        <div class="modal-center">
              <center>
              <div id="measure_pic">
                <img src="<?php echo $clothing['measure_img_url'] ?>" id="measure_img" />
              </div>
              <div id='measure_column'>
                <div class='measure_unit'><?php if ($clothing['measure'][1]!=""){ echo $clothing['measure'][1] . "cm"; }else{} ?></div>
                <div class='measure_unit'><?php if ($clothing['measure'][2]!=""){ echo $clothing['measure'][2] . "cm"; }else{} ?></div>
                <div class='measure_unit'><?php if ($clothing['measure'][3]!=""){ echo $clothing['measure'][3] . "cm"; }else{} ?></div>
                <div class='measure_unit'><?php if ($clothing['measure'][4]!=""){ echo $clothing['measure'][4] . "cm"; }else{} ?></div>
                <div class='measure_unit'><?php if ($clothing['measure'][5]!=""){ echo $clothing['measure'][5] . "cm"; }else{} ?></div>
                <div class='measure_unit'><?php if ($clothing['measure'][6]!=""){ echo $clothing['measure'][6] . "cm"; }else{} ?></div>
                <div class='measure_unit'><?php if ($clothing['measure'][7]!=""){ echo $clothing['measure'][7] . "cm"; }else{} ?></div>
                <div class='measure_unit'><?php if ($clothing['measure'][8]!=""){ echo $clothing['measure'][8] . "cm"; }else{} ?></div>
                <div class='measure_unit'><?php if ($clothing['measure'][9]!=""){ echo $clothing['measure'][9] . "cm"; }else{} ?></div>
                <div class='measure_unit'><?php if ($clothing['measure'][10]!=""){ echo $clothing['measure'][10] . "cm"; }else{} ?></div>
              </div>
              </center>

              <div style='clear: both'></div>
            <br />
              <ul class='product-view-ul'>
                <li>測量出的尺寸數據會因為其布料彈性、水洗處理、測量起訖點等因素而出現些微誤差; 誤差值在+-2公分內視為合理範圍。</li>
                <li>為了避免因商品尺寸不合，耽誤您寶貴的時間，請在訂購前務必先確認各款尺寸。</li>
                <li>在GetMore，所有衣服都是獨一無二，請留給真心喜歡的人，減少退貨，也是節能減碳愛護地球的一種方式。  </li> 
              </ul>  
            <br />

            <?php if (substr($clothing['product_type'], 0, 1) == '2'): ?>
                <a class='pull-right' data-dismiss="modal" data-toggle="modal" href="#modalmen" class="product-size-image">？如何測量</a>
            <?php endif; ?>
            <?php if (substr($clothing['product_type'], 0, 1) == '1'): ?>
                <a class='pull-right' data-dismiss="modal" data-toggle="modal" href="#modalwomen" class="product-size-image">？如何測量</a>            
            <?php endif; ?>
            <?php if (substr($clothing['product_type'], 0, 1) == '3'): ?>
                <a class='pull-right' data-dismiss="modal" data-toggle="modal" href="#modalkids" class="product-size-image">？如何測量</a>            
            <?php endif; ?>

        </div>
      </div>
    </div><!-- /.modal-content -->

  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id='sizetable-modal'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">
              <?php if (substr($clothing['product_type'], 0, 1) == '1'): ?>
                  女裝尺碼對照參考
              <?php endif; ?>
              <?php if (substr($clothing['product_type'], 0, 1) == '2'): ?>
                  男裝尺碼對照參考
              <?php endif; ?>
          
          </h4>
      </div>
      <div class="modal-body">
        <div class="modal-center">
          <center>
            <table id='sizetable'>
                <?php if (substr($clothing['product_type'], 0, 1) == '1'): ?>
                <tr>
                    <th>Regular</th>
                    <th>XXS</th>
                    <th>XS</th>
                    <th>S</th>
                    <th>M</th>
                    <th>L</th>
                    <th>XL</th>
                    <th>XXL</th>
                </tr>
                <tr>
                    <td>US</td>
                    <td>0</td>
                    <td>2</td>
                    <td>4</td>
                    <td>6</td>
                    <td>8</td>
                    <td>10</td>
                    <td>12</td>
                </tr>
                <tr>
                    <td>UK</td>
                    <td>4</td>
                    <td>6</td>
                    <td>8</td>
                    <td>10</td>
                    <td>12</td>
                    <td>14</td>
                    <td>16</td>
                </tr>
                <tr>
                    <td>EUR/FR</td>
                    <td>32</td>
                    <td>34</td>
                    <td>36</td>
                    <td>38</td>
                    <td>40</td>
                    <td>42</td>
                    <td>44</td>
                </tr>
                <tr>
                    <td>JP 1</td>
                    <td></td>
                    <td></td>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>JP 2</td>
                    <td>3</td>
                    <td>5</td>
                    <td>7</td>
                    <td>9</td>
                    <td>11</td>
                    <td>13</td>
                    <td>15</td>
                </tr>
                <tr>
                    <td>牛仔褲</td>
                    <td>22</td>
                    <td>23</td>
                    <td>24.25</td>
                    <td>26.27</td>
                    <td>28.29</td>
                    <td>30</td>
                    <td>31</td>
                </tr>
                <?php endif; ?>
                <?php if (substr($clothing['product_type'], 0, 1) == '2'): ?>
                <tr>
                    <th>Regular</th>
                    <th>XS</th>
                    <th>S</th>
                    <th>M</th>
                    <th>L</th>
                    <th>XL</th>
                    <th>XXL</th>
                </tr>                
                <tr>
                    <td>US</td>
                    <td></td>
                    <td>14½</td>
                    <td>15</td>
                    <td>15½</td>
                    <td>16</td>
                    <td>16½</td>
                </tr>
                <tr>
                    <td>UK</td>
                    <td></td>
                    <td>14½</td>
                    <td>15</td>
                    <td>15½</td>
                    <td>16</td>
                    <td>16½</td>
                </tr>
                <tr>
                    <td>EUR/FR</td>
                    <td></td>
                    <td>37</td>
                    <td>38</td>
                    <td>39</td>
                    <td>40</td>
                    <td>41</td>
                </tr>
                <tr>
                    <td>JP 1</td>
                    <td>S</td>
                    <td>M</td>
                    <td>L</td>
                    <td>XL</td>
                    <td>XXL</td>
                    <td></td>
                </tr>
                <tr>
                    <td>JP 2</td>
                    <td>36-37</td>
                    <td>38-39</td>
                    <td>40-41</td>
                    <td>42-43</td>
                    <td>44</td>
                    <td></td>
                </tr>
                <tr>
                    <td>牛仔褲</td>
                    <td>28-29</td>
                    <td>30-31</td>
                    <td>32-34</td>
                    <td>35-37</td>
                    <td>38-40</td>
                    <td>41-43</td>
                </tr>
                <?php endif; ?>


            </table>
          </center>
            <hr />
        <ul class='product-view-ul'>
        <li>F：One Size / Free Size。</li>
        <li>吊牌/布標遺失、無尺碼，我們會依據丈量尺寸結果，給予衣服一個近似尺碼。<br/>
         ex. Fits L：表示這件好衣尺寸近似 L 。</li>
        <li>上表尺碼對照為普遍情形，可能因各個品牌不同而有所差異，僅供參考。 </li>
        </ul>            
        </div>
      </div>
    </div><!-- /.modal-content -->

  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
