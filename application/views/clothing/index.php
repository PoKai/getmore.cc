<script src="/assets/javascript/howtomakeaturn/clothing.js"></script>
<!--fontawesome added-->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!-- newpage -->
<link rel="stylesheet" href="/assets/css/getmorepage/newpage.css"/>

<!--<img style='position: fixed; right: 20px; top: 180px;' src='/assets/banner/coldcold-01.jpg'>-->

<!--modal start-->
<div class="modal fade" id='videomodel'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">GetMore 理念</h4>
      </div>
      <div class="modal-body">
        <iframe width="580" height="326" src="https://www.youtube.com/embed/OGBxLxGuQ3c?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
      </div>
      <div class="modal-footer">
        <p class='announcement'>GetMore二次時尚，重新定義衣服的價值。</p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id='videomodelTVBS'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">新聞採訪</h4>
      </div>
      <div class="modal-body">
        <iframe width="580" height="326" rc="//www.youtube.com/embed/wO7FvfhxGqk?rel=0" frameborder="0" allowfullscreen></iframe>
      </div>
      <div class="modal-footer">
        <p class='announcement'>GetMore二次時尚，重新定義衣服的價值。</p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id='TEDJessi'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">勇敢做自己，穿自己喜歡的衣服</h4>
      </div>
      <div class="modal-body">
        <iframe width="580" height="326" src="//www.youtube.com/embed/Xsmhu4V13h4" frameborder="0" allowfullscreen></iframe>      
      </div>
      <div class="modal-footer">
        <p class='announcement'>GetMore二次時尚，重新定義衣服的價值。</p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id='ask_for_email_modal'>
  <div class="modal-dialog-500">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">請輸入常用email</h4>
      </div>
      <div class="modal-body">
        
          <form role="form" method='POST' action='/member/information'>
            <div class="form-group">
              <label >常用email</label>
                <input type="text" id='active_email' class="form-control" name='active_email'  value='<?php echo $user->active_email ?>'>
                <p class="help-block">請輸入您常用的email。<span class='btn btn-default btn-xs' id='fill_in_my_registration_email' data-registration_email='<?php echo $user->email; ?>'>填入我的註冊信箱</span></p>
            </div>
            
            <div class="form-group">
                <button type="submit" id='submit' class="btn btn-primary" >確定</button>
            </div>
          </form>
        
      </div>

    </div>
  </div>
</div>
<!-- modal END -->

<div id="main-carousel" class="carousel slide carousel-margin" data-ride="carousel">
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <!--
    <div class="item active">
      <a href='/clothing/category/new/'><img src="/assets/banner/newbanner.jpg"></a>
    </div>
    -->
    
    <!--
    <div class='item active'>
        <a href='/clothing/earthhour'>
            <div class='eh-banner'>
                <div class='countdown_container'>
                    <div data-expired='2014-3-30 18:00:00' class="defaultCountdown"></div>
                    <span class='eh-unit'>日</span>
                    <span class='eh-unit'>時</span>
                    <span class='eh-unit'>分</span>
                    <span class='eh-unit'>秒</span>
                </div>
            </div>
        </a>
    </div>
    -->
    <!--
    <div class="item active">
      <a href='/clothing/earthhour'><img src="/assets/banner/earthhour-06.jpg"></a>
    </div>
    -->    
    <?php foreach ($banners as $index => $banner): ?>
        <?php if ($index == 0): ?>
        <div class="item active">
          <a href='<?php echo $banner->href ?>' <?php if ($banner->open_in_new_tab){echo 'target="_blank"'; } ?> ><img src="<?php echo $banner->image_url ?>"></a>
        </div>    
        <?php else: ?>
        <div class="item">
          <a href='<?php echo $banner->href ?>' <?php if ($banner->open_in_new_tab){echo 'target="_blank"'; } ?> ><img src="<?php echo $banner->image_url ?>"></a>
        </div>    
        <?php endif; ?>
    <?php endforeach; ?>
  </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#main-carousel" data-slide="prev">
      <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#main-carousel" data-slide="next">
      <span class="icon-next"></span>
    </a>
    <!-- Indicators 
    <ol class="carousel-indicators">
      <li data-target="#main-carousel" data-slide-to="0" class="active"></li>
      <li data-target="#main-carousel" data-slide-to="1"></li>
      <li data-target="#main-carousel" data-slide-to="2"></li>
      <li data-target="#main-carousel" data-slide-to="3"></li>
      <li data-target="#main-carousel" data-slide-to="4"></li>
      <li data-target="#main-carousel" data-slide-to="5"></li>
      <li data-target="#main-carousel" data-slide-to="6"></li>
      <li data-target="#main-carousel" data-slide-to="7"></li>
    </ol>
    -->
  </div>

<!--banner2 start -->  
<div class="homead">
    <div class="homead-box">
            <a href="http://www.facebook.com/getmore.cc" target="_blank"><img src="/assets/images/ad/ad-01.jpg" alt=""></a>
    </div> 
    <div class="homead-box">
            <a data-toggle="modal" href='#TEDJessi'><img src="/assets/images/ad/ad-021.jpg" alt=""></a>
    </div>
    <div class="homead-box">
            <a href='/info/perfectbuy'><img src="/assets/images/ad/ad-03.jpg" alt=""></a>
    </div>            
    <div class="homead-box-right">
            <a data-toggle="modal" href="#videomodel"><img src="/assets/images/ad/ad-04.jpg" alt=""></a>
    </div>      
</div>
<!-- banner2 END -->
<p class="clear"></p>

<!-- suggested items -->
<div>
    <span class="fresh-title">
      GetMore 推薦 / SUPER CLOTHES
    </span>
</div>
<div class='suggested-box'>
  <ul id='target'>
      <?php foreach ($suggested_items as $item):?>
          <?php
              /* 
               * handle logic here is actually bad practice
               * move it sometime
               */
              $soldout_tag = '';
              if ($item['lock_status'] == '2'){
                  $soldout_tag = "<div class='soldoutsticker180'><img src='/assets/images/home/soldout180.png'/></div>";
              }
              else if ($item['lock_status'] == '1'){
                  $soldout_tag = "<div class='soldoutsticker180'><img src='/assets/images/home/reserved180.png'/></div>";
              }            
          ?>
      
          <li>
              <?php echo $soldout_tag; ?>
              <div class='buy-me-tag'><a target='_blank' href='/clothing/brandsonly/<?php echo $item['clothing_id']; ?>'><img src='/assets/images/buyme125x165.png' /></a></div>
              <a target='_blank' href='/clothing/brandsonly/<?php echo $item['clothing_id']; ?>'>
                  <div class='suggested-item-original-image'><img src="<?php echo image_url(explode(',', $item['files'])[0]); ?>" /></div>
                  <div class='suggested-item-title'><?php echo $item['product_title']; ?></div>
                  <div class='suggested-item-original-price'><?php echo get_price_label($item['product_original_price']); ?></div>
                  <div class='suggested-item-selling-price'>
                      <span> NT$ <?php echo $item['product_sell_price']; ?></span>
                      <?php if ($item['reduction_again']): ?>
                          <span style='color:#4dbbaa'>再次降價↓</span>
                      <?php endif; ?>
                  </div>
              </a>
          </li>
      <?php endforeach;?>
  </ul>
</div>
  <a class='more_suggested_clothing' href='/clothing/suggested' target='_blank'>看更多推薦好衣...</a>
    <script>        
        $(document).ready(function(){
            <?php if (!empty($user) && empty($user->active_email)): ?>
                $('#ask_for_email_modal').modal('show');
                $('#fill_in_my_registration_email').click(function(){
                    console.log('click');
                    var registration_email = $(this).data('registration_email');
                    console.log(registration_email);
                    $('#active_email').val(registration_email);
                })
            <?php endif; ?>
            $('#target').lunZhuan({
                itemClass: 'suggested-item',
                buttonClass: 'suggested-item-button'
            });
            
            $('.suggested-item').hover(
                function(){ // hover in
                    $(this).find('.buy-me-tag').css({display: 'block'});
                },
                function(){ // hover out
                    $(this).find('.buy-me-tag').css({display: 'none'});                  
                }
            ); // end hover function
            
        });
    </script>

    <!-- fresh product -->
    <div class="fresh">
      <span class="fresh-title">
        新鮮貨 / NEW LOOK
      </span>
      <div class="fresh-box">
      <?php foreach ($clothings as $clothing):?>  
        <?php echo clothing_thumbnail_layout($clothing); ?>
      <?php endforeach;?>
      </div>
    </div>
    <!-- fresh product -->

    <div style='clear: both;'></div>
    <div class="gotonew text-center"><a href="/clothing/category/new" class="btn btn-default enterplanbtn" href="#" role="button" style='width:920px;'>到最新上架一件一件逛 >></a></div>
   <!-- <div class="sellstart"><p class="text-center"><a href="http://bit.ly/1dfjycB"><button type="button" class="btn btn-primary">到最新上架一件一件逛 >></button></a></p></div> -->
<div class='fourthrow'>
  <div class='lookbooktopic'>
    <div class='lookbootshowimg'><a href='http://blog.getmore.cc/category/getmore-lookbook/' target='_blank'><img src="/assets/images/lookbook/weblookbook03-01.png"></a></div>
  </div>
  <div class='entertobuy'>
    <div class='enterplanB'>
      <font class='enterplantitle'>品牌限定</font><br>
      <font>BRANDS ONLY</font><br>
      <font>／</font><br>
      <font class='enterplanp'>國際品牌限定，每件商品都經過第三方專業清洗整燙，由GetMore統一出貨，2折起，輕鬆上手精品服飾！</font><br>
      <a class="btn btn-default enterplanbtn" href="/clothing/category/women" role="button" target='_blank'>SHOP >></a>
    </div>
    <div class='enterplanA'>
      <font class='enterplantitle'>狂熱市集</font><br>
      <font>MARKETPLACE</font><br>
      <font>／</font><br>
      <font class='enterplanp'>這裏聚集了許多對服飾狂熱份子，分享他們的穿搭品味，你在這可以玩上一整天，享受這場時尚市集饗宴。</font><br>
      <a href='/clothing/mkplace' class="btn btn-default enterplanbtn" href="#" role="button" target='_blank'>SHOP >></a>
    </div>
  </div>
  <div class='otherentery'>
    <div class='otherrow1'><a href="/clothing/reduction" target="_blank"><img src="/assets/images/newpage/11-01.png"></a></div>
    <div class='otherrow2'><a href="/clothing/category/women/#type-women&page-1&price_to-300&price_from-0" target="_blank"><img src="/assets/images/newpage/12-01.png"></a></div>
    <div class='otherrow3'><a href="http://blog.getmore.cc/" target="_blank"><img src="/assets/images/newpage/13-01.png"></a></div>
  </div>
</div>
<div class='fifthrow'>
  <div class='pageabouttitle'><font class='pagecentertitle'>ABOUT GETMORE</font></div>
  <div class='pageaboutbox'>
    <div class='pageaboutp'>GetMore是一個以「共享經濟」為概念的時尚分享網站</div>
    <div class='pageaboutp'>在這裡，你可以輕鬆享受二手衣帶來的自由、獨特 、環保等時尚樂趣</div>
    <div class='pageaboutp' style="margin-top:10px;">Enjoy Pre-owed Fashion on GetMore Now!</div>
  </div>
</div>
<div class='sixthrow'>
  <div class='pageliketitle'><font class='pagecentertitle'>GET MORE GETMORE ON</font></div>
  <div class='pagelikebox'>
      <div class='pagelikefacebook'><a href='https://www.facebook.com/getmore.cc' class='pagelikea' target='_blank'><i class="fa fa-facebook fa-3x"></i></a></div>
      <div class='pagelikeinstagram'><a href='https://instagram.com/getmorecc/' class='pagelikea' target='_blank'><i class="fa fa-instagram fa-3x"></i></a></div>
      <div class='pagelikeyoutube'><a href='https://www.youtube.com/channel/UCMFEkClqi18ge_EItQa0lAg/feed' class='pagelikea' target='_blank'><i class="fa fa-youtube-square fa-3x"></i></a></div>
  </div>
</div>

<div id="fb-root"></div>


    <div class="marginbottom30">
      <span style="width:500px;">
        <div class="fb-page" data-href="https://www.facebook.com/getmore.cc" data-width="500" data-height="300" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><!--<blockquote cite="https://www.facebook.com/getmore.cc"><a href="https://www.facebook.com/getmore.cc">GetMore</a></blockquote>--></div></div>
      </span> 
          <!--<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fgetmore.cc&amp;width=980&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true&amp;appId=207910605914306" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:980px; height:258px;" allowTransparency="true"></iframe>-->
      <div style="float:right;width:450px;height:214px;" class="bookinggetmore">
        <form action="http://www.newsleopard.com/mailverify.htm?x=8a383d3a4e26ae89014e2e9128552da7"  target="popupwindow" method="post" onsubmit="window.open('', 'popupwindow', 'scrollbars=yes,width=350,height=115');return true">
          <div style="padding:20px;margin-top:100px;float:right;">
            <p>
              <span><input style="width:260px;border:3px solid #000;"  class="form-control" placeholder="請輸入Email" name="email" type="text"/></span></center>
            </p>
            <input style="float:right;"class="btn" value="立即訂閱" type="submit"/>
          </div>
        </form>
      </div>
      <div class="clear"></div> 
    </div>

<script>
  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.3&appId=239678122794926";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<script type="text/javascript">
    $(function () {
        LikeButtonModule.initialize();
        <?php if($logined): ?>
            LikeButtonModule.setIsLogined(true);
        <?php endif; ?>
    });
</script>

<style>
.bookinggetmore{
  background-image:url("/assets/images/bookinggetmore.jpg");
  background-repeat: no-repeat; 
}
</style>
