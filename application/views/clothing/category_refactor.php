<script src="/assets/javascript/howtomakeaturn/clothing.js"></script>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>

<div id='category_banner_container'>
    <?php if ($this->uri->segment(3)=='new'): ?>
        <img src='/assets/banner/new.jpg' />
    <?php endif;?>
    <?php if ($this->uri->segment(3)=='men'): ?>
        <img src='/assets/banner/men.jpg' />
    <?php endif;?>
    <?php if ($this->uri->segment(3)=='women'): ?>
        <img src='/assets/banner/women.jpg' />
    <?php endif;?>
    <?php if ($this->uri->segment(3)=='kids'): ?>
        <img src='/assets/banner/kids.jpg' />
    <?php endif;?>
</div>
<div style='clear: both;'></div>
    <?php if ($this->uri->segment(3)=='women'): ?>
<div class='category-container'>
    <ul class="subtype nav nav-tabs">
        <li class='active'><a href="#" data-subtype='1'>全部商品</a></li>
        <li><a href="#" data-subtype='11'>上衣</a></li>
        <li><a href="#" data-subtype='12'>洋裝</a></li>
        <li><a href="#" data-subtype='15'>外套</a></li>
        <li><a href="#" data-subtype='13'>褲子</a></li>
        <li><a href="#" data-subtype='14'>裙子</a></li>
    </ul>

    <?php endif;?>
    <?php if ($this->uri->segment(3)=='men'): ?>
<div class='category-container'>    
    <ul class="subtype nav nav-tabs">
        <li class='active'><a href="#" data-subtype='2'>全部商品</a></li>
        <li><a href="#" data-subtype='21'>上衣</a></li>
        <li><a href="#" data-subtype='22'>襯衫</a></li>
        <li><a href="#" data-subtype='24'>外套</a></li>
        <li><a href="#" data-subtype='23'>褲子</a></li>
    </ul>
    <?php endif;?>
    <?php if ($this->uri->segment(3)=='kids'): ?>
<div class='category-container'>    
    <ul class="subtype nav nav-tabs">
        <li class='active'><a href="#" data-subtype='3'>全部商品</a></li>
        <li><a href="#" data-subtype='31'>連身衣</a></li>
        <li><a href="#" data-subtype='32'>上衣</a></li>
        <li><a href="#" data-subtype='33'>下身</a></li>
        <li><a href="#" data-subtype='34'>洋裝</a></li>
        <li><a href="#" data-subtype='35'>外套</a></li>
    </ul>
    <?php endif;?>

<div class='clothing_category_menu'>

    <div class='search_container factor_container'>
        <input type='text' class='form-control search_input' placeholder='找品牌' />
        <button class='btn search_button btn-sm btn-default'>搜尋</button>
    </div>


    <div class='pattern_container factor_container' style='display: none;'>
        <h3 class='pattern_title factor_title'>款式</h3>  
        <div class='pattern_checkboxes_container'>
        </div>  
    </div>

    <div class='item_long_container factor_container' style='display: none;'>
        <h3 class='item_long_title factor_title'>長度</h3>  
        <div class='item_long_checkboxes_container'>
        </div>  
    </div>

    <div class='factor_container'>
        <h3 class='factor_title'>顏色</h3>  
        <div>
            <label class='color_label'><input type="checkbox" value="black" class='color'> <img src="/admin/assets/images/colorbox/black.png"></label>
            <label class='color_label'><input type="checkbox" value="white" class='color'> <img src="/admin/assets/images/colorbox/white.png"></label>
            <label class='color_label'><input type="checkbox" value="gray" class='color'> <img src="/admin/assets/images/colorbox/gray.png"></label>
            <label class='color_label'><input type="checkbox" value="red" class='color'> <img src="/admin/assets/images/colorbox/red.png"> </label>
            <label class='color_label'><input type="checkbox" value="green" class='color'> <img src="/admin/assets/images/colorbox/green.png"></label>
            <label class='color_label'><input type="checkbox" value="brown" class='color'> <img src="/admin/assets/images/colorbox/brown.png"></label>
            <label class='color_label'><input type="checkbox" value="blue" class='color'> <img src="/admin/assets/images/colorbox/blue.png"></label>
            <label class='color_label'><input type="checkbox" value="orange" class='color'> <img src="/admin/assets/images/colorbox/orange.png"> </label>
            <label class='color_label'><input type="checkbox" value="purple" class='color'> <img src="/admin/assets/images/colorbox/purple.png"></label>
            <label class='color_label'><input type="checkbox" value="pink" class='color'> <img src="/admin/assets/images/colorbox/pink.png"></label>
            <label class='color_label'><input type="checkbox" value="yellow" class='color'> <img src="/admin/assets/images/colorbox/yellow.png"></label>
            <label class='color_label'><input type="checkbox" value="color" class='color'> <img src="/admin/assets/images/colorbox/color.png"></label>
        </div>  
    </div>
    <div class='factor_container'>
        <h3 class='factor_title'>風格</h3>  
        <div>
            <?php foreach ($styles as $key => $value): ?>       
                <label class='style_label'><input type="checkbox" value="<?php echo $key ?>" class='style'><?php echo $value ?></label>
            <?php endforeach; ?>
        </div>  
    </div>
    
    
    
    
    
    <div class='factor_container'>
        <h3 class='factor_title'>季節</h3>
        <label class='season_label'><input type='checkbox' value='1' class='season' />春夏</label>
        <label class='season_label'><input type='checkbox' value='2' class='season' />秋冬</label>
        <label class='season_label'><input type='checkbox' value='3' class='season' />無季節</label>
    </div>

    <div class='factor_container'>
        <h3 class='factor_title'>尺寸</h3>
            <label class='size_label'><input type="checkbox" value="XXS" class='size'>XXS</label>
            <label class='size_label'><input type="checkbox" value="XS" class='size'>XS</label>
            <label class='size_label'><input type="checkbox" value="S" class='size'>S</label>
            <label class='size_label'><input type="checkbox" value="M" class='size'>M</label>
            <label class='size_label'><input type="checkbox" value="L" class='size'>L</label>
            <label class='size_label'><input type="checkbox" value="XL" class='size'>XL</label>
            <label class='size_label'><input type="checkbox" value="XXL" class='size'>XXL</label>
    </div>

    
</div>
<div class='clothing_category_content'>
    <div id='loading'></div>

    <!-- fresh product -->
    <div class="fresh nopadding">
    <!--
      <span class="fresh-title">
          <?php if ($this->uri->segment(3)=='new'): ?>
              新鮮貨 / NEW LOOK
          <?php endif;?>
          <?php if ($this->uri->segment(3)=='men'): ?>
              男裝 / MEN LOOK
          <?php endif;?>
          <?php if ($this->uri->segment(3)=='women'): ?>
              女裝 / WOMEN LOOK
          <?php endif;?>
          <?php if ($this->uri->segment(3)=='kids'): ?>
              童裝 / KIDS LOOK
          <?php endif;?>
      </span> -->
        <div class="pull-right getmore-pagination"><?php // echo $pagination; ?></div>
        <div style='clear: both;'></div>
        <div class="fresh-box-ajax">
          
        <?php foreach ($clothings as $clothing):?>  
          <?php // echo clothing_thumbnail_layout($clothing); ?>
        <?php endforeach;?>
        </div>
        <div style='clear: both;'></div>
        <div class="pull-right getmore-pagination"><?php // echo $pagination; ?></div>
    </div>
    <!-- fresh product -->
    <div style='clear: both;'></div>
</div>
<div style='clear: both;'></div>
</div>

<script>
    function AjaxWithHashTool(){
      
        var _params = {};
        this.set = function(key, value){
            _params[key] = value;
        }
        this.get = function(key){
            if (!( key in _params)){
                // this is too strict for just manipulating elements
                // throw new Error('undefined key: ' + key + '. set the key before getting value from it.');
                return '';
            }
            return _params[key];
        }
        var _beforeCallback = function(){}
        var _successCallback = function(){}

        this.setBeforeCallback = function(callback){
            _beforeCallback = callback;
        }
        this.setSuccessCallback = function(callback){
            _successCallback = callback;
        }

        var _hash = '';

        var self = this;
        
        var refresh_hash = function(){
            var hash = '';
            var noValueInHash = true;
            for ( key in _params){
                if (_params[key].length !== 0){
                    if (noValueInHash){
                        hash = hash + key + '-' + _params[key];
                        noValueInHash = false;
                    }
                    else{
                        hash = hash + '&' + key + '-' + _params[key];
                    }
                }
            }
            _hash = hash;
            window.location.hash = hash;
        }
        
        this.send = function(){
            console.log(_params);

            $.ajax({
                url: '/clothing/category_via_ajax',
                data: _params,
                method: 'post',
                dataType: 'json',
                beforeSend: function(jqXHR, settings) {
                    _beforeCallback();
                },
                success: function(res){
                    _successCallback(res);
                    refresh_hash();                    
                }
            });
        }
        this.setParamsFromUrl = function(){
            var hash = window.location.hash.substr(1);
            if (hash.length!==0){
                var parts = hash.split('&');
                $.each(parts, function(index, part){
                    var key = part.split('-')[0];
                    var value = part.split('-')[1];
                    self.set(key, value);                
                });
            }
        }        
    }
    
    $(document).ready(function(){
      
        $('.getmore-pagination').pagination({
            items: 100,
            itemsOnPage: 15,
            selectOnClick: false,
            edges: 1,
            displayedPages: 3,
            prevText: '<',
            nextText: '>'
            // cssStyle: 'compact-theme'
        });      
        $('.getmore-pagination ul').addClass('pagination pagination-sm');
    
    // if it's 'NEW' category, make it four items per row and remove selection menu.
    // put PHP code in js code is very BAD i know .. @@"
    <?php if ($this->uri->segment(3)=='new'): ?>
        $('.clothing_category_menu').hide();
        $('.clothing_category_content').css('width', '980px');
        $('.fresh-box-ajax').css('width', '980px');
        $('.getmore-pagination').pagination('updateItemsOnPage', 20);        
    <?php endif;?>
    

    <?php if ($this->uri->segment(3)=='women'): ?>
        // a dirty and quick way to set proper styles
        $('.style[value=17]').parent().hide();
        $('.style[value=18]').parent().hide();
    <?php endif;?>
    
    <?php if ($this->uri->segment(3)=='men'): ?>
        // a dirty and quick way to set proper styles
        $('.style[value=13]').parent().hide();
        $('.style[value=14]').parent().hide();
        $('.style[value=15]').parent().hide();
        $('.style[value=16]').parent().hide();
        $('.style[value=17]').parent().hide();
        $('.style[value=18]').parent().hide();
        $('.style[value=19]').parent().hide();
        $('.style[value=3]').parent().hide();
        $('.style[value=5]').parent().hide();
    <?php endif;?>
    
    <?php if ($this->uri->segment(3)=='kids'): ?>
        // a dirty and quick way to set proper styles
        $('.style[value=13]').parent().hide();
        $('.style[value=14]').parent().hide();
        $('.style[value=15]').parent().hide();
        $('.style[value=16]').parent().hide();
        $('.style[value=19]').parent().hide();
        $('.style[value=3]').parent().hide();
        $('.style[value=1]').parent().hide();
        $('.style[value=8]').parent().hide();
        $('.style[value=2]').parent().hide();
        $('.style[value=4]').parent().hide();

    <?php endif;?>
    
    
        var awhTool = new AjaxWithHashTool();    
        awhTool.set('type', '<?php echo $this->uri->segment(3) ?>');    
        awhTool.set('page', '1');    
        awhTool.setBeforeCallback(setupBeforeSendingAjax);
        awhTool.setSuccessCallback(setupAfterReceivingAjaxResponse);
        awhTool.setParamsFromUrl();
        awhTool.send();

        $('.subtype li').click(function(e){
            e.preventDefault();
            var subtype = $(this).find('a').data('subtype');
            //alert(subtype);
            $('.subtype li').removeClass('active');
            $(this).addClass('active');
            awhTool.set('subtype', subtype);
            awhTool.set('page', 1); // to prevent clothing not enough for the previous offset
            awhTool.set('pattern', '');
            awhTool.set('item_long', '');
            awhTool.set('color', '');
            awhTool.set('style', '');
            awhTool.set('season', '');
            awhTool.set('keyword', '');
            awhTool.send();
        });
        
        $('.color').change(function (){
            var colors = [];
            $('.color').each(function(){
                if ($(this).prop('checked')){
                    colors.push($(this).val());
                }
            })
            awhTool.set('color', colors.toString());
            
            awhTool.set('page', 1); // to prevent clothing not enough for the previous offset
            awhTool.send();
        });

        $('.style').change(function (){
            var styles = [];
            $('.style').each(function(){
                if ($(this).prop('checked')){
                    styles.push($(this).val());
                }
            })
            awhTool.set('style', styles.toString());
            
            awhTool.set('page', 1); // to prevent clothing not enough for the previous offset
            awhTool.send();
        });
        
        $('.season').change(function (){
            var seasons = [];
            $('.season').each(function(){
                if ($(this).prop('checked')){
                    seasons.push($(this).val());
                }
            })
            awhTool.set('season', seasons.toString());
            
            awhTool.set('page', 1); // to prevent clothing not enough for the previous offset
            awhTool.send();
        });

        $(".search_button").click(function (e) {
            var keyword = $('.search_input').val();
            console.log(keyword);
            awhTool.set('keyword', keyword);
            awhTool.set('page', 1); // to prevent clothing not enough for the previous offset
            awhTool.set('pattern', '');
            awhTool.set('item_long', '');
            awhTool.set('color', '');
            awhTool.set('style', '');
            awhTool.set('season', '');            
            awhTool.send();
        });
        

        $(".search_input").keyup(function (e) {
            if (e.keyCode == 13) {
                var keyword = $(this).val();
                console.log(keyword);
                awhTool.set('keyword', keyword);
                awhTool.set('page', 1); // to prevent clothing not enough for the previous offset
                awhTool.set('pattern', '');
                awhTool.set('item_long', '');
                awhTool.set('color', '');
                awhTool.set('style', '');
                awhTool.set('season', '');            
                awhTool.send();
            }
        });
        /*
        $('.search_input').change(function (){
            var keyword = $(this).val();
            console.log(keyword);
            awhTool.set('keyword', keyword);
            awhTool.set('page', 1); // to prevent clothing not enough for the previous offset
            awhTool.set('pattern', '');
            awhTool.set('item_long', '');
            awhTool.set('color', '');
            awhTool.set('style', '');
            awhTool.set('season', '');            
            awhTool.send();
        });
*/
        function render_pattern_checkboxes(matched_patterns, pattern_name_mapping){
            $.each(matched_patterns, function(index, value){
                var pattern_name = pattern_name_mapping[value];
                var pattern_box = $('<input type="checkbox" class="pattern" value="' + value + '" />');
                $label = $('<label class="pattern_label">');
                $label.append(pattern_box);
                $label.append(pattern_name);
                $('.pattern_checkboxes_container').append($label);
            });
        }

        function render_item_long_checkboxes(matched_item_longs, item_long_name_mapping){
            $.each(matched_item_longs, function(index, value){
                var item_long_name = item_long_name_mapping[value];
                var item_long_box = $('<input type="checkbox" class="item_long" value="' + value + '" />');
                $label = $('<label class="item_long_label">')
                $label.append(item_long_box);
                $label.append(item_long_name);                
                $('.item_long_checkboxes_container').append($label);
            });
        }
        
        function initialize_menu(color, style, season, subtype, pattern, item_long){
            var colors = color.split(',');
            $('input.color').prop('checked', false);
            $.each(colors, function( index, value ) {
                $('input[value=' + value + '].color').prop('checked', true);
            });
            var styles = style.split(',');
            $('input.style').prop('checked', false);
            $.each(styles, function( index, value ) {
                $('input[value=' + value + '].style').prop('checked', true);
            });
            var seasons = season.split(',');
            $('input.season').prop('checked', false);
            $.each(seasons, function( index, value ) {
                $('input[value=' + value + '].season').prop('checked', true);
            });
            $('.subtype li').removeClass('active');
            $('.subtype li').each(function(){
                if ($(this).find('a').data('subtype') == subtype){
                    $(this).addClass('active');
                }
            })
            var patterns = pattern.split(',');
            $.each(patterns, function( index, value ) {
                $('input[value=' + value + '].pattern').prop('checked', true);
            });
            var item_longs = item_long.split(',');
            $.each(item_longs, function( index, value ) {
                $('input[value=' + value + '].item_long').prop('checked', true);
            });
        }

        function setupBeforeSendingAjax(){
            $('#loading').show();
        }

        function setupAfterReceivingAjaxResponse(res){
            if (awhTool.get('subtype') && awhTool.get('subtype').length!=0){
                $('#category_banner_container img').attr('src', '/assets/banner/subtype/' + awhTool.get('subtype') + '.jpg');
            }
            $('.pattern_checkboxes_container').empty();
            $('.pattern_container').hide();
            if ( (res.matched_patterns!=null)&&(res.matched_patterns.length != 0) ){
                $('.pattern_container').show();
                render_pattern_checkboxes(res.matched_patterns, res.pattern_name);
                // initialize_menu(_color, _style, _season, _subtype, _pattern);
                initialize_menu( awhTool.get('color'), awhTool.get('style'),
                    awhTool.get('season'), awhTool.get('subtype'),
                    awhTool.get('pattern'), awhTool.get('item_long') );
              
                $('.pattern').change(function (){
                    var patterns = [];
                    $('.pattern').each(function(){
                        if ($(this).prop('checked')){
                            patterns.push($(this).val());
                        }
                    })
                    awhTool.set('pattern', patterns.toString());
                    awhTool.set('page', 1); // to prevent clothing not enough for the previous offset
                    awhTool.send();
                });
            }

            $('.item_long_checkboxes_container').empty();
            $('.item_long_container').hide();
            if ( (res.matched_item_longs!=null)&&(res.matched_item_longs.length != 0) ){
                $('.item_long_container').show();
                // quick and dirty way to set item long title
                if (res.matched_item_longs[0].charAt(1)=='s'){
                    $('.item_long_title').html('袖長');
                } else if (res.matched_item_longs[0].charAt(1)=='p'){
                    $('.item_long_title').html('褲長');
                } else if (res.matched_item_longs[0].charAt(1)=='b'){
                    $('.item_long_title').html('長度');
                }
                render_item_long_checkboxes(res.matched_item_longs, res.item_long_name);
                initialize_menu( awhTool.get('color'), awhTool.get('style'),
                    awhTool.get('season'), awhTool.get('subtype'),
                    awhTool.get('pattern'), awhTool.get('item_long') );
                $('.item_long').change(function (){
                    var item_longs = [];
                    $('.item_long').each(function(){
                        if ($(this).prop('checked')){
                            item_longs.push($(this).val());
                        }
                    })
                    awhTool.set('item_long', item_longs.toString());
                    awhTool.set('page', 1); // to prevent clothing not enough for the previous offset
                    awhTool.send();
                });                
            }
            
             $('.getmore-pagination').hide();
            
            $('.getmore-pagination').pagination('updateItems', res.total_rows);
            $('.getmore-pagination').pagination('selectPage', awhTool.get('page'));
            
            $('.getmore-pagination').pagination('redraw');
            $('.getmore-pagination ul').addClass('pagination pagination-sm');
            
            $('.fresh-box-ajax').hide();
            
            // $('.getmore-pagination').html(res.pagination);
            $('.fresh-box-ajax').html(res.clothing_list_page);

            $('.getmore-pagination').fadeIn();
            $('.fresh-box-ajax').fadeIn();
            
            $('.getmore-pagination li a').click(function(e){
                e.preventDefault();
                var href = $(this).attr('href');
                var hash = href.split('#')[1];
                var page = get_value_by_key_from_hash('page', hash);
                awhTool.set('page', page);
                awhTool.send();
            });
            $('#loading').hide();
            
            LikeButtonModule.initialize();
            <?php if($logined): ?>
                LikeButtonModule.setIsLogined(true);
            <?php endif; ?>            
        }
    });
    // this is a reserved private function
    // most of the time, you won't need it. but I guess it may help in some conditions.
    function get_value_by_key_from_hash(key, hash){
        if (hash.indexOf(key) === -1){
            return '';
        }
        var re = new RegExp( '.*'+ key + '-');
        var value = hash.replace(re, '');
        value = value.split('&')[0];

        return value;
    }

</script>

<script>
    $(document).ready(function(){

      
        /*
        alert('ho!!');
        
        console.log($('.fresh-product-view'));
        $('.fresh-product-view').hover(
            function(){ // hover in
                console.log('hey');
                //$(this).find('.like_tag').css({display: 'block'});
            },
            function(){ // hover out
                console.log('hoo');
                //$(this).find('.like_tag').css({display: 'none'});                  
            }
        ); // end hover function      
        */
    });

</script>

<style>
    #loading {
        display: none;
        // position: absolute;
        // top: 550px;
        // left: 650px;
        position: fixed;
        top: 50%;
        left: 50%;
        width: 64px;
        height: 64px;
        margin-left: 68px;
        margin-top: -32px;
        z-index: 1000;
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        border-radius: 8px;
        background-color: #eee0c8;
        background-color: rgba(255, 255, 255, 0.6);
        -webkit-box-shadow: 0 0 5px #999999;
        -moz-box-shadow: 0 0 5px #999999;
        box-shadow: 0 0 5px #999999;
        background-image: url("/admin/assets/images/loading.gif");
        background-position: center center;
        background-repeat: no-repeat;
    }  
  
    .clothing_category_menu{
        // background-color: #EEE;
        width: 245px;
        float: left;        
    }
  
    .clothing_category_content{
        // background-color: pink;
        width: 735px;
        float: left;
    }
    .fresh-box-ajax{
        width: 735px;
    }
    .getmore-pagination{
        padding-right: 20px;
        color: #19a190!important;
    }
    .getmore-pagination a{
        color: #19a190;
    }
    .factor_container{
        // background-color: hotpink;
        padding: 10px 30px 10px 0;
        width: 240px;
        overflow: auto;
    }
    .factor_title{
        padding-bottom: 10px;
        border-bottom: 1px solid grey;
        font-size: 16px;
    }
    
    .pattern_label, .item_long_label, .color_label, .style_label, .season_label{
        display: block;
    }
 
    .pattern_label, .color_label, .style_label{
        float: left;    
    }
    
    .pattern_label, .style_label{
        width: 90px;
    }
    .color_label, .size_label{
        width: 60px;
    }
</style>

  <script>
      $(function() {
          var availableTags = [
              <?php foreach($brands as $brand): ?>
                  "<?php echo $brand['name'] ?>",
              <?php endforeach; ?>
          ];
          $( ".search_input" ).autocomplete({
//              source: availableTags
              source: function(req, responseFn) {
                  var re = $.ui.autocomplete.escapeRegex(req.term);
                  var matcher = new RegExp( "^" + re, "i" );
                  var a = $.grep( availableTags, function(item,index){
                      return matcher.test(item);
                  });
                  responseFn( a );
              }    
          });
      });
  </script>
