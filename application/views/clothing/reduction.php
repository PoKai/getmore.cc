<script src="/assets/javascript/howtomakeaturn/clothing.js"></script>

      <img src='/assets/images/banner/reduction.jpg' />
      <div class="alert alert-warning alert-dismissable alert-adjust">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  衣服上架後，「GetMore二次時尚」衣管家會向賣家提出建議售價，若賣家降價，將得到「再次降價」的標示，就代表買二手衣又更划算了！！
      </div>
    <!-- fresh product -->
    <div class="fresh">
      <span class="fresh-title">
              再次降價
      </span>
        <div class="fresh-box">
        <?php foreach ($clothings as $clothing):?>  
          <?php echo clothing_thumbnail_layout($clothing); ?>
        <?php endforeach;?>
        </div>
        <div style='clear: both;'></div>
    </div>
    <!-- fresh product -->
    <div style='clear: both;'></div>
<script type="text/javascript">
  
    $(function () {
        LikeButtonModule.initialize();
        <?php if($logined): ?>
            LikeButtonModule.setIsLogined(true);
        <?php endif; ?>
    });
</script>
