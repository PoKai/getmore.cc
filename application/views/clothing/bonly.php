<style>

.pagetitle{
  font-size: 18px;
  line-height: 200%;
  letter-spacing: 3px;
  margin-left: 30px;
  font-weight: bold;
}
.pagecentertitle{
  font-size: 18px;
  line-height: 200%;
  letter-spacing: 3px;
  font-weight: bold;
}

.fifthrow {
  overflow: auto;
  text-align: center;
  margin-top: 20px;
  padding: 20px;
  border-bottom: 1px dashed #000000;
  border-top: 1px dashed #000000;
  min-height: 160px;
}
.plandescription {
  overflow: auto;
  text-align: center;
  padding: 20px;
  border-bottom: 1px dashed #000000;
  min-height: 160px;
  margin-bottom:30px;
}
.pageabouttitle{
  margin: 0 auto;
  width: 230px;
  border-bottom: 2px solid #000000;
  letter-spacing: 5px;
  text-align: center;

}
.pageaboutbox{
  margin-top: 15px;
}
.pageaboutp{
  font-size: 14px;
  font-weight: bold;
  letter-spacing: 5px;
  line-height: 25px;
}

.sixthrow{
  overflow: auto;
  text-align: center;
  margin-top: 0px;
  margin-bottom: 20px;
  padding: 20px;
//  min-height: 160px;
  border-bottom: 2px solid #888;
}
.pageliketitle{
  margin: 0 auto;
  width: 330px;
  border-bottom: 2px solid #000000;
  letter-spacing: 5px;
  text-align: center;
}

.pagelikebox{
  overflow: auto;
  margin: 20px auto 0px auto;
  display:inline-block; 
}

.pagelikefacebook ,.pagelikeinstagram{
  float: left;
  margin-right: 20px;
}

.pagelikeyoutube{
  float: left;
}

a.pagelikea{
  color: #333;
  text-decoration: none;
}
a.pagelikea:hover{
  color: #000;
  text-decoration: none;
}

#newContainer {
  z-index: 101;
  background-color: #fff;
  width: 1000px !important;
  margin: 0 auto;
  padding-top: 10px;
}
#newfooter{
  z-index: 101;
  width: 1000px !important;
  margin: 0 auto;
  padding-top: 10px;
}
.fresh-box {
  width: 1000px;
  padding: 0 20px;
  margin-top: 20px;
  overflow: auto;
}
.sellerbanner{
  float: left;
  width: 300px;
  margin: 10px 10px;
}

</style>
<div id="newbannerContainer"style="display: block;">
    <div class="plandescription">
      <div class="pageabouttitle" style="width:400px;"><font class="pagecentertitle">品牌代售/BRANDS ONLY</font></div>
      <div class="pageaboutbox">
        <div class="pageaboutp">國際品牌限定的二手精品專區，每件商品都經過第三方專業清洗整燙，</div>
        <div class="pageaboutp">由GetMore統一出貨，2折起，輕鬆上手精品服飾！</div>
      </div>
    </div>
        <div class="productcontainer">
      <span class="fresh-title">
        熱門推薦 / HOT PICKS
      </span>
      <div class="fresh-box">
        
        <a target="_blank" href="http://getmore.cc/clothing/view/6259"><div class="fresh-product-view"><div class="like_tag to_like" data-id="6259" style="display: none;"><img src="/assets/GetMore_HomePage_files/to_like.png"></div><img src="/assets/GetMore_HomePage_files/small(47).jpg"><div class="fresh-product-title">A/T 卡其雙排釦外套</div><div class="fresh-product-price"><span class="is-deleted">NT$8,001~9,000</span><span class="fresh-product-sale">NT$1790</span></div><span class="size-tag">Size Fits L</span></div></a>        
        <a target="_blank" href="http://getmore.cc/clothing/view/6372"><div class="fresh-product-view"><div class="like_tag to_like" data-id="6372" style="display: none;"><img src="/assets/GetMore_HomePage_files/to_like.png"></div><img src="/assets/GetMore_HomePage_files/small(48).jpg"><div class="fresh-product-title">羊毛菱格紋洋裝</div><div class="fresh-product-price"><span class="is-deleted">NT$6,001~7,000</span><span class="fresh-product-sale">NT$2300</span></div><span class="size-tag">Size M</span></div></a>        
        <a target="_blank" href="http://getmore.cc/clothing/view/6371"><div class="fresh-product-view"><div class="like_tag to_like" data-id="6371" style="display: none;"><img src="/assets/GetMore_HomePage_files/to_like.png"></div><img src="/assets/GetMore_HomePage_files/small(49).jpg"><div class="fresh-product-title">GARCIA針織毛背心</div><div class="fresh-product-price"><span class="is-deleted">NT$3,001~4,000</span><span class="fresh-product-sale">NT$1499</span></div><span class="size-tag">Size XS</span></div></a>        
        <a target="_blank" href="http://getmore.cc/clothing/view/6370"><div class="fresh-product-view"><div class="like_tag to_like" data-id="6370" style="display: none;"><img src="/assets/GetMore_HomePage_files/to_like.png"></div><img src="/assets/GetMore_HomePage_files/small(50).jpg"><div class="fresh-product-title">艷藍色T恤</div><div class="fresh-product-price"><span class="is-deleted">NT$ 500~1,000</span><span class="fresh-product-sale">NT$300</span></div><span class="size-tag">Size S</span></div></a>        
        <a target="_blank" href="http://getmore.cc/clothing/view/6366"><div class="fresh-product-view"><div class="like_tag to_like" data-id="6366" style="display: none;"><img src="/assets/GetMore_HomePage_files/to_like.png"></div><div class="recommended_tag"><img src="/assets/GetMore_HomePage_files/superrecommend.png"></div><img src="/assets/GetMore_HomePage_files/small.jpg"><div class="fresh-product-title">SportMax 背後網狀條紋T</div><div class="fresh-product-price"><span class="is-deleted">NT$3,001~4,000</span><span class="fresh-product-sale">NT$350</span></div><span class="size-tag">Size M</span></div></a>        
        <a target="_blank" href="http://getmore.cc/clothing/view/6369"><div class="fresh-product-view"><div class="like_tag to_like" data-id="6369" style="display: none;"><img src="/assets/GetMore_HomePage_files/to_like.png"></div><img src="/assets/GetMore_HomePage_files/small(51).jpg"><div class="fresh-product-title">針織蝴蝶結扣外套 </div><div class="fresh-product-price"><span class="is-deleted">NT$2,001~3,000</span><span class="fresh-product-sale">NT$1300</span></div><span class="size-tag">Size </span></div></a>        
        <a target="_blank" href="http://getmore.cc/clothing/view/6364"><div class="fresh-product-view"><div class="like_tag to_like" data-id="6364" style="display: none;"><img src="/assets/GetMore_HomePage_files/to_like.png"></div><img src="/assets/GetMore_HomePage_files/small(52).jpg"><div class="fresh-product-title">COCOLULU 帥氣外套</div><div class="fresh-product-price"><span class="is-deleted">NT$2,001~3,000</span><span class="fresh-product-sale">NT$800</span></div><span class="size-tag">Size M</span></div></a>        
        <a target="_blank" href="http://getmore.cc/clothing/view/6363"><div class="fresh-product-view"><div class="like_tag to_like" data-id="6363" style="display: none;"><img src="/assets/GetMore_HomePage_files/to_like.png"></div><img src="/assets/GetMore_HomePage_files/small(53).jpg"><div class="fresh-product-title">條紋雪紡露背上衣</div><div class="fresh-product-price"><span class="is-deleted">NT$2,001~3,000</span><span class="fresh-product-sale">NT$800</span></div><span class="size-tag">Size EU38/US6</span></div></a>            </div>
    </div>
    <hr />
    <div class="productcontainer">
      <span class="fresh-title">
        熱門專櫃 / NEW LOOK
      </span>
      <div class="fresh-box">
        <div class="sellerbanner"><img src="/assets/GetMore_HomePage_files/300122.png"></div>
        <div class="sellerbanner"><img src="/assets/GetMore_HomePage_files/300122.png"></div>
        <div class="sellerbanner"><img src="/assets/GetMore_HomePage_files/300122.png"></div>
        <div class="sellerbanner"><img src="/assets/GetMore_HomePage_files/300122.png"></div>
        <div class="sellerbanner"><img src="/assets/GetMore_HomePage_files/300122.png"></div>
        <div class="sellerbanner"><img src="/assets/GetMore_HomePage_files/300122.png"></div>
        <div class="sellerbanner"><img src="/assets/GetMore_HomePage_files/300122.png"></div>
        <div class="sellerbanner"><img src="/assets/GetMore_HomePage_files/300122.png"></div>
        <div class="sellerbanner"><img src="/assets/GetMore_HomePage_files/300122.png"></div>
      </div>
    </div>
        <div class="gotonew text-center"><a href="/clothing/category/marketplace" class="btn btn-default enterplanbtn" role="button" style="width:920px;">到最新上架一件一件逛 &gt;&gt;</a></div>
  </div>
  <div id="newContainer"style="display: block;">
    <div class="fifthrow">
      <div class="pageabouttitle"><font class="pagecentertitle">ABOUT GETMORE</font></div>
      <div class="pageaboutbox">
        <div class="pageaboutp">GetMore是一個以「共享經濟」為概念的時尚分享網站</div>
        <div class="pageaboutp">在這裡，你可以輕鬆享受二手衣帶來的自由、獨特 、環保等時尚樂趣</div>
        <div class="pageaboutp" style="margin-top:10px;">Enjoy Pre-owed Fashion on GetMore Now!</div>
      </div>
    </div>

      <div class="pageliketitle"><font class="pagecentertitle">GET MORE GETMORE ON</font></div>

  </div>
 