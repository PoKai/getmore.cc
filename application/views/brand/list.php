<article class="module">
<h3>搜尋清單</h3>
<?php foreach($approved_brands as $brand): ?>
<span class='brand-box'>
    <?php echo $brand['name'] ?>
    <?php echo button_to( 'btn btn-danger btn-mini','/api/brand/remove', 
        ['id'=>$brand['id']], '刪除', '確定要刪除？') ?>
</span>
<?php endforeach; ?>
<hr />
<h3>新增品牌</h3>
<form action='/api/brand/add' method='post'>
    <input type='text' name='name' class='form-control' required />
    是
    <select name="category_id">
        <? foreach ($brand_categories as $category): ?>
        <option value="<?= $category['id']; ?>"><?= $category['name']; ?></option>
        <? endforeach; ?>
        <option value="0">其他:</option>
    </select>
    <input type="text" name="category_name" style="display: none;">
    <button type='submit' class='btn btn-success'>加入</button>
</form>
<hr/>

<h3>待批准加入清單</h3>
<?php foreach($unapproved_brands as $brand): ?>
    <div class="well well-small">
        <?php echo $brand['name'] ?>
        是
        <form action="/api/brand/approve" method="post" style='display: inline;'>
            <select name="category_id">
                <? foreach ($brand_categories as $category): ?>
                <option value="<?= $category['id']; ?>"><?= $category['name']; ?></option>
                <? endforeach; ?>
                <option value="0">其他:</option>
            </select>
            <input type="text" name="category_name" style="display: none;">
            <input type="hidden" value="<?=$brand['id']?>" name="id">
            <button class="btn btn-success">加入</button>
        </form>
        <?php echo button_to('btn btn-danger', '/api/brand/remove', ['id'=>$brand['id']], '刪除', '確定要刪除？') ?>
    </div>
<?php endforeach; ?>
<script src="/assets/vendor/jquery/jquery.js"></script>
<script type="text/javascript">
    $('article').delegate('select[name=category_id]', 'change', function(e){
        if(parseInt($(this).val())){
            return $('input[name="category_name"]', $(e.target).parents('form')).hide();
        }
        $('input[name="category_name"]', $(e.target).parents('form')).show();
    });
</script>

</article>

<style>
    .brand-box{
        border: 1px solid black;
        padding: 5px;
        margin: 5px;
        display: inline-block;
    }
</style>
