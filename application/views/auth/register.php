<div id="content" style="margin:-4px 0 0 0;border-top:none;">

	<div class="nav">首頁 &#8250; 會員註冊</div>

    <div class="member_frame">
    	您已經是getmore會員了嗎？　<span class="member_reg_login"><a href="<?php echo site_url('auth/login'); ?>"><img src="/assets/images/member/login.jpg"></a></span>
    </div>
    <?php echo form_open("auth/register");?>
    <div class="member_frame">
    	【成為會員】<br>
        ※ 成為ＧetMore會員，將得到第一手生活品味、以及優質活動資訊。<br>
        <div id="infoMessage"><?php echo $message;?></div>
   	  	<div class="member_register">
        	<div class="member_reg_left">
            	<div class="memberregister_form">
                    <div class="memberregister_formname">輸入您的Email</div>
                    <div class="memberregister_formvalue"><?php echo form_input($email);?></div>
                </div>
                <div class="memberregister_form">
                    <div class="memberregister_formname">設定密碼</div>
                    <div class="memberregister_formvalue"><?php echo form_input($password);?></div>
                </div>
                <div class="memberregister_form">
                    <div class="memberregister_formname">確認密碼</div>
                    <div class="memberregister_formvalue"><?php echo form_input($password_confirm);?></div>
                </div>
                <div class="memberregister_form">
                	<div class="memberregister_formname"></div>
                    <div class="memberregister_formvalue"><?php echo form_checkbox('aggree', '1', FALSE);?>&nbsp;我已閱讀且同意getmore的服務條款</div>
                </div>
                <div class="memeberregister_reg_button"><input type="image" src="/assets/images/member/accept.jpg" alt="Submit" /></div>
            </div>
            <div class="member_reg_right">
            	<div class="memberregister_fb"><a href="<?php echo site_url('auth/facebook'); ?>"><img src="/assets/images/member/fb_login.png"></a></div>
            </div>
        </div>
        <br><br>
        <div class="subscribe_gototop"><span>回最上面</span></div>
    </div>
    <?php echo form_close();?>    
</div>
