<div class="content profile-page">
    <div class="nav">首頁 &#8250; 會員中心</div>
    <div class="welcome">
        歡迎來到&nbsp;&nbsp;<img src="/assets/images/logo_s.png" />&nbsp;&nbsp;會員中心，很高興能為您服務！
    </div>
    <div class="container">
        <?php if(isset($message) and !empty($message)): ?>
        <div class="alert alert-success"><a class="close" data-dismiss="alert" type="button">×</a><?php echo $message; ?></div>
        <?php endif; ?>
        <form name="edit" action="/auth/edit" method="POST">
        <div class="right profile">
            <div class="panel">
                <h3>帳號資訊</h3>
                <label>E-mail</label>
                <input name="email" type="text" value="<?php echo $profile->email; ?>" placeholder="Enter your Email" />
                <?php if(!empty($profile->password)): ?>
                <label>舊密碼</label>
                <input name="old_password" type="password" placeholder="Enter old password" />&nbsp;&nbsp;修改密碼時請輸入，不更改免填
                <?php endif; ?>
                <label>新密碼</label>
                <input name="new_password" type="password" placeholder="Enter new password" />&nbsp;&nbsp;須區分大小寫，請用5個字元以上，不更改免填
                <label>再一次輸入新密碼</label>
                <input name="password_confirm" type="password" placeholder="Repeat your new password" />&nbsp;&nbsp;不更改免填
            </div>
            <div class="panel">
                <h3>個人資訊</h3>
                <label>姓名</label>
                <input name="user_name" type="text" value="<?php echo $profile->user_name; ?>" placeholder="Enter your name" />&nbsp;&nbsp;<input type="radio" name="sex" value="1" <?php echo ($profile->sex == '1') ? 'checked="true"' : ''; ?>/>&nbsp;先生&nbsp;&nbsp;<input type="radio" name="sex" value="0" <?php echo ($profile->sex == '0') ? 'checked="true"' : ''; ?>/>&nbsp;小姐
                <label>生日</label>
                <?php echo $user_birthday; ?>
                <label>手機號碼</label>
                <input name="mobile_phone" type="text" value="<?php echo $profile->mobile_phone; ?>" placeholder="Enter your mobile phone" />
                <label>聯絡地址</label>
                <span id="p_address"></span>
            </div>
            <button class="submit">修改資料</button>
        </div>
        </form>
        <div class="left">
            <ul>
                <li class="menu">會員中心</li>
                <li>
                    <ul>
                        <li class="sub-menu"><a href="/auth/edit" class="active">會員資料</a></li>
                        <li class="sub-menu"><a href="/order/lists">訂單查詢</a></li>
                        <li class="sub-menu"><a href="/sellorder/lists">二手衣販售管理</a></li>
                    </ul>
                </li>
                <li class="menu">聯絡我們</li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
</div>
