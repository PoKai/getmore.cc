<!--<img class='banner-s' src="/assets/images/sellorder/banner-login.jpg">-->
<div class="content login-page">
    <div class="row">
    <div class='loginpageleft'>
        <img src="/assets/images/welcome/welcome05.jpg">
    </div>
    <div class='loginpageright'>
            <h3>歡迎使用Getmore的服務，請先登入！</h3></br>
     <div class="fb_login">
            <h4>Getmore 新銳設計與時尚二手線上市集</h4>
<!-- comment by howtomakeaturn
        temporily redirect it to sell page.
 -->                                    
<!--                                    <a class='fb_login_btn btn btn-primary' href="<?php echo site_url('/auth/facebook?redirect_url=' . urlencode(current_url()))?>">用facebook登入</a> 快使用facebook快速登入!-->
                                    <a class='fb_login_btn btn btn-primary' href="<?php echo site_url('/auth/facebook?redirect_url=' . urlencode($this->input->get('redirect_url')))?>">用facebook登入</a> 快使用facebook快速登入!                                    
                                    
                                    <hr class="dashed_hr"/>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <h4>會員登入</h4> 
                                        </div>
                                        <div class="getmore_instruction col-md-8 pull-right announcement">
                                            若已是Getmore的會員,請在此登入!
                                        </div></br></br>
                                    </div>

            <form name="login" action="/auth/login" method="POST">
            <div class="input-form member-login-form">
                <div class="right">
                    <div class="controlls"><input type="text" id="identity" name="identity" placeholder="請輸入電子郵件" value="<?php echo set_value('identity');?>" /></div>
                    <div class="controlls">

                        <input type="password" id="password" name="password" placeholder="請輸入密碼" />&nbsp&nbsp&nbsp<button class="btn btn-info btn-xs">登入</button><input type="hidden" name="redirect_url" value="<?php echo $redirect_url; ?>" /></div>
                    
                    <div class="controlls"><input type="checkbox" id="remember" name="remember" value="1" <?php echo set_checkbox('remember', 1, TRUE); ?> />&nbsp;記住我的帳號&nbsp;<a target='_blank' href="/auth/forget_password">忘記密碼</a></div>
                </div>
                <?php if(isset($message) and !empty($message)): ?>
                    <?php echo $message; ?>
                <?php endif; ?>
                <p class="clear"></p>
            </div>
            </form></br>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <h4>註冊會員</h4> 
                                        </div>
                                        <div class="col-md-8 pull-right announcement getmore_instruction">
                                            還不是Getmore的會員嗎？ 免費加入！
                                        </div></br></br>
                                    </div>

            <form name="register" action="/auth/register" method="POST">
            <div class="input-form member-register-form">
                <div class="right">
                    <div class="controlls"><input type="text" id="user_name" name="user_name" placeholder="請輸入全名" value="<?php echo set_value('user_name');?>" /></div>
                    <div class="controlls"><input type="text" id="email" name="email" placeholder="請輸入電子郵件" value="<?php echo set_value('email');?>" /></div>
                    <div class="controlls"><input type="password" id="new_password" name="password" placeholder="請輸入密碼" /></div>
                    <div class="controlls"><input type="password" id="password_confirm" name="password_confirm" placeholder="請再次確認密碼" />&nbsp&nbsp&nbsp<button class="btn btn-info btn-xs">註冊</button><input type="hidden" name="redirect_url" value="<?php echo $redirect_url; ?>" /></div>
                    <div class="controlls"><input type="checkbox" name="aggree" value="1" />&nbsp;我已閱讀，並同意<a href="/info/buy#buypolicy" target='_blank'>購買條款</a>以及<a href="/info/sell#sellpolicy" target='_blank'>代洗代售服務條款</a>&nbsp;</div>
                </div>
                <?php if(isset($reg_message) and !empty($reg_message)): ?>
                    <?php echo $reg_message; ?>
                <?php endif; ?>
                <p class="clear"></p>
            </div>
            </form>
        </div>
    </div>
    </div>
</div>
