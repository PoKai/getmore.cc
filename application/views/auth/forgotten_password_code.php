<div class='row'>
  <div class='contentbox width500 boxcenter getmorecenter-block'>
    <div class="col-md-offset-1">
        <?php if ($this->session->flashdata('auth_message')): ?>
            <div class='alert alert-danger'>
                <?php echo $this->session->flashdata('auth_message') ?>
            </div>
        <?php endif; ?>      
        <h3>請輸入您的新密碼</h3>
        <form role="form" method='POST' action='/auth/forgotten_password_code'>
          <input type="hidden" name='code' value='<?php echo $code; ?>' />
          <div class="form-group">
            <label >輸入您的新密碼，8位數以上</label>
              <input type="password" id='new_password' class="form-control" name='new_password' />
          </div>
          <div class="form-group">
            <label >請再次輸入您的新密碼</label>
              <input type="password" id='confirm_password' class="form-control" name='confirm_password' />
          </div>
          <div class="form-group">
              <button type="submit" id='submit' class="btn btn-success" >確定</button>
          </div>
        </form>
    </div>
  </div>
</div>
<script>
    $(document).ready(function(){
        $('#submit').click(function(e){            
            if ($('#email_addr').val() == ''){
                e.preventDefault();
            }
        });
    });
</script>
