<div id="content">
    <div style="padding: 0px 10px;">
        <div class="nav"><a href="/">首頁</a> &#8250; <a href="/event/lists/all">所有活動</a> &#8250; <?php echo $category_name;?></div>
        <div class="event_frame">
            <div class="event_left">
                <div class="event_img">
                    <?php
                        $image_list[1]['file_name'] = (isset($image_list[1]['file_name'])) ? $image_list[1]['file_name'] : '';
                    ?>
                    <img class="lazy" src="<?php echo site_url('assets/images/white.gif'); ?>" data-original="<?php echo image_url($image_list[1]['file_name'], 540, 385); ?>" width="540" height="385" />
                </div>
            </div>
            <div class="event_right">
                <div class="event_back">&#8250; 回到<a href="/event/lists/all">所有活動</a></div>
                <div class="event_cate"><span><?php echo $category_name;?></span></div>
                <div class="event_title"><span class="title"><?php echo $title; ?></span></div>
                <div class="event_price">活動費用: <span class="price"><?php echo $price; ?></span></div>
                <div class="event_join">
                    <?php if ($status): ?>
                    <?php echo $order_count; ?>&nbsp;<img data-price="<?php echo $price;?>" data-id="<?php echo $id; ?>" data-type="event" src="/assets/images/event/join.jpg" class="add-cart" />
                    <?php else:?>
                    <?php echo $message; ?>
                    <?php endif;?>
                </div>
                <div class="event_intro">
                    <?php foreach($detail as $row):?>
                    <?php echo $row->name . '　' . $row->value . '<br />';?>
                    <?php endforeach;?>
                </div>
                <div class="event_sharebutton">
                    <div class="date"><a href="http://getmore.cc/calendar"><img src="/assets/images/event/date.png"></a></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="event_frame" style="margin-top:20px;">
        <div class="event_left">
            <div class="event_mainintro">
                <?php echo $content;?>
            </div>
        </div>
        <div class="event_right">
            <div class="maybe">你可能也會喜歡...</div>
            <?php
                foreach($product_list as $row):
                    $row['image_list'][0]['file_name'] = (isset($row['image_list'][0]['file_name'])) ? $row['image_list'][0]['file_name'] : '';
            ?>
            <div class="maybe_block">
                <div class="maybe_left">
                    <a href="<?php echo site_url('product/single/' . $row['id']); ?>">
                        <img class="lazy" src="<?php echo site_url('assets/images/white.gif'); ?>" data-original="<?php echo image_url($row['image_list'][0]['file_name'], 140, 130); ?>" width="140" height="130" />
                    </a>
                    <div class="maybe_caption"><?php echo $row['description'];?></div>
                </div>
                <div class="maybe_right">
                    <div class="maybe_title"><?php echo $row['title'];?></div>
                    <div class="maybe_price_org">$<?php echo $row['original_price']; ?> up</div>
                    <div class="maybe_price_special">$<?php echo $row['sell_price']; ?> up</div>
                    <div class="maybe_inventory">庫存<span><?php echo $row['inventory_amount']; ?></span>個</div>
                    <div class="maybe_seeit"><a href="<?php echo site_url('product/single/' . $row['id']); ?>"><img src="/assets/images/event/seeit.jpg"></a></div>
                </div>
            </div>
            <?php endforeach;?>
        </div>
        <p class="clear">&nbsp;</p>
    </div>
</div>
