<div id="content">
    <div id="content-row-3" data-module="event" data-limit="9">
        <div id="row-3">
            <?php for($i=0;$i<$count;$i++):?>
            <div class="small-article">
                <div class="small-article-title">
                    <div class="fun-icon"><?php echo $output[$i]['category_name']; ?></div>
                </div>
                <div class="small-article-thing">
                    <?php if (!$output[$i]['sold']): ?>
                    <div class="sold-out"></div>
                    <?php endif; ?>
                    <a href="/event/single/<?php echo  $output[$i]['id']; ?>">
                        <div class="article-img-hover"></div>
                        <div class="article-img-hover-text">get<br />this</div>
                        <div class="small-article-img">
                            <img class="lazy" src="/assets/images/white.gif" data-original="<?php echo $output[$i]['image_path']; ?>" />
                        </div>
                        <div class="surplus-time">
                            <?php echo $output[$i]['message']; ?>
                        </div>
                    </a>
                </div>
                <div class="small-article-theme">
                    <h3><?php echo $output[$i]['title']; ?></h3>
                    <p class="discounts"><?php echo $output[$i]['description']; ?></p>
                </div>
            </div>
            <?php endfor; ?>
        </div>
        <p class="clear" style="margin-bottom: 30px;">&nbsp;</p>
    </div>
</div>
