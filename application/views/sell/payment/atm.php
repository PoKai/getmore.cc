<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!-->
<html> <!--<![endif]-->
    <head>
        <meta charset="utf8">
        <script src="/assets/vendor/jquery/jquery.js"></script>
        <script src="/assets/javascript/bootstrap.js"></script>
        <script src="/assets/javascript/howtomakeaturn/jquery.form.min.js"></script>
        <script src="/assets/javascript/libs/jquery/jquery.twzipcode-1.4.1.js"></script>
        <script src="/assets/vendor/underscore-amd/underscore.js"></script>
        <script src="/assets/vendor/backbone-amd/backbone.js"></script>
        <script src="/assets/javascript/howtomakeaturn/backbone.localStorage.js"></script>
        <script src="/assets/javascript/sell/validator.js"></script>
        <script src="/assets/javascript/sell/account.js"></script>
        <script src="/assets/javascript/sell/model_and_collection.js"></script>
        <script src="/assets/javascript/sell/cart_view.js"></script>
        <script src="/assets/javascript/sell/order_view.js"></script>
        <script src="/assets/javascript/sell/main.js"></script>
    </head>
<body>
    
    <form action="/member/view_sellorder" method="POST" id="payment">
        <input type=hidden name="order_number" value="<?php echo $order_number; ?>">
    </form>
    <script>
        $(document).ready(function(){

            var items = new SellApp.ItemCollection;
            items.fetch();

            _(items.length).times(function(n){ 
                var model = items.models[0]
                model.destroy();
            });
        
            console.log('now the cart is CLEAR.');                
          
            document.getElementById("payment").submit();          
        })
    </script>

</body>
</html>
