<h3><span id='congratulation'>恭喜您</span>完成品牌二手衣代售申請!</h3>
<p>請將您的代售貨品折疊並簡易包裝，並保持手機暢通，宅急便物流士將與您聯繫。</p>
<a class="btn btn-primary" href="http://getmore.cc/member/sellorder">檢視貨單</a>

 <?php /* <div class='row' id='pagerow'>
		<div class='col-md-9' id='upper_container'>
			<h4><img id='clothe_icon' src="/assets/images/secondhand_management/picture_clothes.png" alt="" /><span id="selling">開始賣衣服</span> / <span id="pending_list">待付款貨單狀態</span> / <span id="status">二手衣販售狀態</span></h4>
			<hr class="bold_hr" />
			<img id='flow' src="/assets/images/secondhand_management/flow4.png" alt="" />
			<div id='instruction' class="col-md-12">
				<div class="col-md-6">
					<h3><span id='congratulation'>恭喜您</span>完成二手衣代售申請!</h3>
					<!--<p>請點選右方按鈕列印申請表並貼於外箱。</p>
					<p>若不方便列印, 也可將下列所有資訊抄下後貼於外箱!</p>
					<p>請務必黏貼申請表, 物流士會至您的取貨地點收貨。</p>-->

				</div>
				<div class="col-md-6">
				<a id="prints" href="#" onclick="window.open('/sellorder/prints/<?php echo $order['order_number']; ?>');">
					<img id='print' src="/assets/images/secondhand_management/print.png" alt="" />
				</a>
				</div>
			</div>
		</div>
			<hr id='center_spliter' class="dashed_hr"/>
					<div id='receipt' class="col-md-10 pull-right">
						<div class="row" id='first_row'>
							<div id='sender' class="col-md-8 bordered_div">
									<div id="sender_name" class='info_div'>
										寄件人:<span id="sender_name_value"><?php echo $order['user_name']; ?></span>
										<hr class="dashed_hr"/>
									</div>
									<div id="sender_address" class='info_div'>
										寄件人地址:<span id="sender_address_value"><?php echo $order['user_address']; ?></span>
										<hr class="dashed_hr"/>
									</div>
									<div class="blank_rectangel"></div>
							</div>
							<div id="production_number_code" class='col-md-4 bordered_div'>
								<p>貨單編號</p>
						<p><img id="barcode" src="/assets/images/barcode/<?php echo $order['order_number']; ?>.jpg" /></p>
							</div>
						</div>
						<div id="receiver" class='col-md-12 bordered_div'>
							<div id="receiver_address" class='info_div'>
								到貨地址:<span id="receiver_address_value">106 台北市大安區新生南路一段165巷18號</span>
								<hr class="dashed_hr"/>
							</div>
							<div id="receiver_name" class='info_div'>
								收件人:<span id="receiver_name_value">GetMore 二次時尚</span>
								<hr class="dashed_hr"/>
							</div>
							<div class="blank_rectangel"></div>
						</div>
						<div id="serial_numbers" class='bordered_div'>
							<div id="production_number" class='info_div'>
								貨單編號:<span id="production_number_value"><?php echo $order['order_number']; //$ordernumber; ?></span>
								<hr class="dashed_hr"/>
							</div>
							<!--<div id="transport_number" class='info_div'>
								貨單編號:<span id="transport_number_value"></span>
								<hr class="dashed_hr"/>
							</div>-->
							<div class="blank_rectangel"></div>
						</div>
						<div id="content_info" class='bordered_div'>
							<div id="content" class='info_div'>
								內容物:<span id="content_value"> 小衣<?php echo $small; ?> 件/ 大衣<?php echo $big; ?>件/  羽絨衣<?php echo $fly; ?>件</span>
								<hr class="dashed_hr"/>
							</div>
							<div id="unit" class='info_div'>
								總件數:<span id="unit_value"><?php echo $total; ?></span>
								<hr class="dashed_hr"/>
							</div>
							<div class="blank_rectangel"></div>
						</div>
					</div>
</div>
*/?>

<!-- Facebook Conversion Code for 賣衣服成交 -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6020121709494', {'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6020121709494&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>

<!-- Google Code for google ads&#36067;&#34915;&#26381; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1013794543;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "NQqMCPHqiggQ74214wM";
var google_conversion_value = 0;
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1013794543/?value=0&amp;label=NQqMCPHqiggQ74214wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
