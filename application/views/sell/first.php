<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>


<input type='hidden' id='type_name' value='<?php echo json_encode($type_name) ?>' />
<input type='hidden' id='type_price' value='<?php echo json_encode($type_price) ?>' />

<img class='banner-s' src="/assets/images/banner-s/sellorder.jpg">
<div class='row' id='pagerow'>

		<div style=" width:680px;margin:0 auto; overflow:auto;">
      <div><center><img src="/assets/images/sellorder/sell-01.jpg" style="margin-top:15px;margin-bottom:15px;"></center></div>
			<h4><img id='clothe_icon' src="/assets/images/secondhand_management/picture_clothes.png" alt="" /><span id="selling">開始賣衣服</span></h4>
			<hr class="bold_hr" />
			<h3>請輸入二手衣資訊</h3>
				
			<form id="step-1" action="/sellorder/steps/2" method="POST" role="form">
				<div class="form-group col-md-7">
          <label for="product_desc">商品敘述</label>
					<input type="text" class="form-control" id="product_desc" maxlength="20">
					<p class='description'>ex: MJ紅色春秋襯衫</p>
				</div>
				<div class="col-md-10">
					<input type="hidden" value="" id="product_type">
						<div class="col-md-4 level_1">
                <h6>服裝</h6>
						</div>
						<div class="col-md-4 level_2">
                <h6>類別</h6>
						</div>
						<div class="col-md-4 level_3">
                <h6>類別2</h6>
						</div>
				</div>
        <input type="hidden" id="shipping_price">
        <div class="form-group col-md-7">
          <label for="product_brand">品牌</label>
					<input type="text" class="form-control" id="product_brand" maxlength='30'>
          <p class='description'>請參照<a href="/info/brandsonly" target="_blank">品牌列表</a>確實輸入品牌名稱，不符合品牌列表會被退回喔！</p>
        </div>
        <div class="form-group col-md-7">
          <label for="">參考原價</label>
	   <select class="form-control" id="original_price">
          <?= price_area(); ?>  
          </select>
          <p class='description'>請填入當時購買的大約價格,請勿提出原價低於$500商品</p>
        </div>
        <div class="form-group col-md-7">
          <label for="sell_price">二手販售價格</label>
          <input type="text" class="form-control" id="sell_price" maxlength='5' onkeyup="this.value=this.value.replace(/[^\d]/,'');">
          <p class='description'>想要賣出的價格,建議為原價3折以下</p>
        </div>
        <div class="form-group col-md-7">
          <label>期滿送回資訊</label>
          <div class="radio">
            <label><input type="radio" value="1" name="donative" class='donative'>期滿後若未售出請幫我捐出，不用還我喔！</label>
          </div>

          <div class="radio">
            <label><input type="radio" value="0" name="donative" class='donative' checked="checked">請寄還給我。</label>
          </div>
          <p class='description'>下一步可輸入送回地址；</br>若選擇捐出 GetMore 將於期滿後協助您送出愛心！</p>
        </div>
        <div class="form-group col-md-7">
          <label for="note">備註</label>
          <input type="text" class="form-control" id="product_note" maxlength='20'>
          <p class='description'>您認為需要備註的事項;限定20字元以內。ex:全新沒拆吊牌。</p>
        </div>
        <div id='buttons' class="col-md-7">
          <button type="button" class="btn btn-info add-sellcart" id ="new_sell_product">新增一筆</button>
          <span id = 'over20clothing_message'></span>
          <button type="button" class="test-sellcart hidden">TEST</button>
        </div>
			</form>		
		

    <script type="text/template" id="items_list_template">
        <table class='table table-single-order'>
          <thead>
              <tr>
                <td>商品描述</td>
                <td>服飾 / 類別</td>
                <td>品牌</td>
                <td>原價 / 二手售價</td>
                <td>期滿</td>
                <td>備註</td>
                <td>修改</td>
                <td>刪除</td>
              </tr>
          </thead>
            <tbody>

            <% _.each(items, function(item) { %>           
                <tr>
                    <td><span class="title"><%= item.get('product_title') %></span></td>
                    <td><span class="product_type">
                        <%= SellApp.Helper.getTypeName(item.get('product_type').substr(0,1)) %> / 
                        <%= SellApp.Helper.getTypeName(item.get('product_type')) %>
                    </span></td>
                    <td><span class="product_brand"><%= item.get('product_brand') %></span></td>
                    <td><span class="original_price"><%= SellApp.Helper.getPriceLabel(item.get('product_original_price')) %> / 
                    $<%= item.get('product_sell_price') %></span></td>
                    <td><span><%= SellApp.Helper.getReturnName(item.get('donative')) %></span></td>
                    <td><span class="product_note"><%= item.get('comment') %></span></td>
                    <td><button class="btn inline edit-sellcart  btn-xs" data-update="1" data-id="<%= item.get('id') %>" data-type="product">修改</button></td>
                    <td><button class="btn btn-danger inline delete-sellcart  btn-xs" data-update="1" data-id="<%= item.get('id') %>" data-type="product">移除</button></td>
                </tr>
            <% }); %>

            </tbody>
        </table>


    </script>


<div id='items_list' class='row'>    
		<table class='table table-single-order'>
			<thead>
					<tr>
						<td>商品描述</td>
						<td>服飾 / 類別</td>
						<td>品牌</td>
						<td>原價 / 二手售價</td>
						<td>期滿</td>
						<td>備註</td>
						<td>修改</td>
						<td>刪除</td>
					</tr>
			</thead>
				<tbody>
				</tbody>
		</table>
</div>

		<hr class="dashed_hr"/>
    <div>
      <!--<a class='marginright5 marginleft15 getmoregreen' data-toggle="modal" href="#sellnoticemodal">--><a target="_blank" href="http://support.getmore.cc/?p=317">費用說明</a> /
      <!--<a class='marginright5 getmoregreen' data-toggle="modal" href="#sellpricemodal">--><a target="_blank" href="http://support.getmore.cc/?p=507">注意事項</a>/
      <!--<a class='marginright5 getmoregreen'  data-toggle="modal" href="#brandsonlymodal">--><a target="_blank" href="http://getmore.cc/info/brandsonly">品牌限定</a>
    </div>
		<div id="info" class='col-md-8'>

				<div class="row">
					<div class="col-md-12">
						總件數：<span id="all_num_value"></span><span class='font-size10 getmoregreen'>&nbsp;(單次申請最多30件)</span>
					</div>
					<!-- 
                                 <div class="col-md-4" id='small_num'>
						<span id="small_num_value"></span> 小衣
					</div>
					<div class="col-md-4" id='big_num'>
						<span id="big_num_value"></span> 大衣
					</div>
					<div class="col-md-4" id='fly_num'>
						<span id="fly_num_value"></span> 羽絨衣
					</div> 
                                -->
				</div>

				<hr />
				<div class="row">
					<div class="col-md-10">
						洗衣費小計<span class='font-size10 getmoregreen'>&nbsp;(不分類別，單件$99)</span>
					</div>
					<div class="col-md-2" id='washing_fee'>

					</div>
				</div>
				<div class="row">
					<div class="col-md-10">
						運費小計<span class='font-size10 getmoregreen'>&nbsp;(運費均一價$120)</span>
					</div>
					<div class="col-md-2" id='transport_fee'>

					</div>
				</div>

				<hr />
				<div class="row">
					<div class="col-md-10">
						總計
					</div>
					<div class="col-md-2" id='total_fee'>
					<?php echo $total_shipping_price; ?>
					</div>
				</div>
		</div>
			<div class='col-md-8'><a href="/sell/second" class="btn btn-info go-second" data-form="step-1" id="step2">下一步</a> 
			</div>
		</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">修改商品</h4>
      </div>
      <div class="modal-body">                
        <input type="hidden" id="edit_product_id" />
				<div class="form-group col-md-7">
          		<label for="edit_product_desc">商品敘述</label>
					<input type="text" class="form-control" id="edit_product_desc" maxlength="20">
					<p class='description'>辨別您的代售商品 ex:miu miu鐵灰絲質上衣</p>
				</div>        

				<div class="col-md-10">
<!--					<input type="hidden" value="" id="product_type">-->
          <div class="col-md-4 level_1">
          </div>
          <div class="col-md-4 level_2">
          </div>
          <div class="col-md-4 level_3">
          </div>
				</div>
        
        <div class="form-group col-md-7">
          <label for="edit_product_brand">品牌</label>
					<input type="text" class="form-control" id="edit_product_brand" maxlength='30'>
          <p class='description'>請確實輸入品牌,GetMore不收假貨喔!</p>
        </div>
                
        <div class="form-group col-md-7">
          <label for="edit_original_price">參考原價</label>
		  <select class="form-control" id="edit_original_price">
            <?= price_area(); ?>
          </select>
          <p class='description'>請填入當時購買的大約價格,原價必須大於$500元喔!</p>
        </div>                
                
                
        <div class="form-group col-md-7">
          <label for="edit_sell_price">二手販售價格</label>
          <input type="text" class="form-control" id="edit_sell_price" maxlength='5' onkeyup="this.value=this.value.replace(/[^\d]/,'');">
          <p class='description'>想要賣出的價格,建議為原價3~5折</p>
        </div>
        <div class="form-group col-md-8">
          <label>期滿送回資訊</label>
          <div class="radio">
            <label><input type="radio" value="1" name="edit_donative" class='edit_donative'>期滿後若未售出請幫我捐出，不用還我喔！</label>
          </div>
          <div class="radio">
            <label><input type="radio" value="0" name="edit_donative" class='edit_donative'>請寄還給我。</label>
          </div>
          <p class='description'>下一步可輸入送回地址；</br>若選擇捐出 GetMore 將於期滿後協助您送出愛心！</p>
        </div>        
        <div class="form-group col-md-9">
          <label for="edit_product_note">備註</label>
          <input type="text" class="form-control" id="edit_product_note" maxlength='20'>
          <p class='description'>您認為需要備註的事項;限定20字元以內。ex:全新沒拆吊牌。</p>
        </div>
       <div style='clear: both'></div>
      </div><!-- /.modal-body -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-info save-sellcart">確認</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

       <div class="modal fade" id='brandsonlymodal'>
          <div class="modal-dialog-800">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">品牌限定召集</h4>
              </div>
              <div class="modal-body">
                <div class="modal-center">
                <img src="/assets/images/content/buy/brandsonly.png">
                </div>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

       <div class="modal fade" id='sellpricemodal'>
          <div class="modal-dialog-800">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">注意事項</h4>
              </div>
              <div class="modal-body">
                <div class="modal-center">
                <img src="/assets/images/content/buy/presell-02.png">
                </div>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id='sellnoticemodal'>
          <div class="modal-dialog-800">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">清洗費用與運費</h4>
              </div>
              <div class="modal-body">
                <div class="modal-center">
                <img src="/assets/images/content/buy/presell-03.png">
                </div>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<!-- Facebook Conversion Code for 賣衣服第一步 -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6020121732294', {'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6020121732294&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>

  <script>
      $(function() {
          var availableTags = [
              <?php foreach($brands as $brand): ?>
                  "<?php echo $brand['name'] ?>",
              <?php endforeach; ?>
          ];
          $( "#product_brand" ).autocomplete({
//              source: availableTags
              source: function(req, responseFn) {
                  var re = $.ui.autocomplete.escapeRegex(req.term);
                  var matcher = new RegExp( "^" + re, "i" );
                  var a = $.grep( availableTags, function(item,index){
                      return matcher.test(item);
                  });
                  responseFn( a );
              }              
          }).bind('keypress blur', function(e){
            if(e.type === 'keypress' && e.keyCode !== 13) return;
            var reg = new RegExp('^'+$(this).val()+'$', "i");
            var brand = _.find(availableTags, function(tag){
              return reg.test(tag);
            });

            if(brand){
              $(this).val(brand);
            }
            
          })
      });
  </script>

          <style>
        #brandsonly{
          position: fixed;
          z-index: 1002;
          margin-top: -30px;
          margin-left: 115px;
        }
          </style>

