<img class='banner-s' src="/assets/images/banner-s/sellorder.jpg">
	<div class='row' id='pagerow'>
	<div style=" width:680px;margin:0 auto; overflow:auto;">
      <div><center><img src="/assets/images/sellorder/sell-02.jpg" style="margin-top:15px;margin-bottom:15px;"></center></div>
			<h4><img id='clothe_icon' src="/assets/images/secondhand_management/picture_clothes.png" alt="" /><span id="selling">開始賣衣服</span> </h4>
			<hr class="bold_hr" />
			<h3>請輸入取件資訊</h3>
				<form id="step-2" action="/sellorder/steps/3" method="POST" role="form">

				<div class="form-group col-md-7">
					<label for="name">姓名</label>
					<input type="text" class="form-control" id="seller_name" name="seller_name">
					<p class='description'>ex:王小咩</p>
				</div>
				<div class="form-group col-md-7">
					<label for="address">取件地址</label><br>
		    			<div id="seller_location">
		    				<div data-role="county"
				                         data-name="seller_county"
				                         data-value="台北市"
				                         data-style="form-control" style="float: left;">
				                    </div>
				                    <div data-role="district"
				                         data-name="seller_district"
				                         data-value="大安區"
				                         data-style="form-control" style="float: left;">
				                    </div>
				                    <div data-role="zipcode"
				                         data-name="seller_zipcode"
				                         data-value="160"
				                         data-style="form-control" style="float: left;width: 173.6px">
				                    </div>
				                    <div>
				                        <input type="text" name="seller_address" class="form-control">
				                    </div>
		    			</div>                
				</div>
				<div class="form-group col-md-7">
					<label for="phone">聯絡電話</label>
					<input type="text" class="form-control" id="seller_phone" name="seller_phone" />
					<p class='description'>請留下您的手機號碼,宅配人員將會與您聯繫。</p>
				</div>
				<div class="form-group col-md-7">
					<label for="">發票開立</label><br>
					<select name="uniform_invoice_type" id="sel_invoice_type">
						<option value='0'>捐贈（捐贈單位：創世基金會）</option>
						<option value='1'>二聯式發票</option>
						<option value='2'>三聯發票</option>
					</select>
					<p class='description'>採用電子發票</p>
					<input type="text" name="uniform_invoice_number" class='form-control' id="uniform_invoice_number" placeholder='統一編號' />
					<input type="text" name="uniform_invoice_title" class='form-control' id="uniform_invoice_title" placeholder='發票抬頭' />
				</div>
				<div id="returning_info" class='col-md-11'>
				<h3>期滿送回資訊</h3>
				<hr />
				<p id='returning_description'>請填入期滿寄回地址；若您選擇捐出，GetMore將於期滿後協助您處理！</p><br>
				<div>
					<label>
						<input name="no_return" type="radio" value="1" />
      					我全部衣服都選擇了期滿後捐贈。
					</label>
				</div>
				<div>
					<label>
						<input type="radio" name="no_return" value="0" <?php if($no_return==0){ ?>checked=""<?php } ?> >
      					我選擇"寄回"的衣服請送到下列地址。
					</label>
				</div>
				<div>
					<label>
						<input type="checkbox" id="same_as" <?php if($no_return!=0){ ?>disabled=""<?php } ?> >
						同取件資訊
					</label>
				</div>
				</div>
				<div class="form-group col-md-7">
					<label for="name">姓名</label>
					<input type="text" class="form-control" id='receiver_name' name="receiver_name">
					<p class='description'>ex:王小咩</p>
				</div>
				<div class="form-group col-md-7">
					<label for="address" class='pokai-address'>送回地址</label><br>
					<div id="receiver_location">
						<div data-role="county"
                         data-name="receiver_county"
                         data-value="台北市"
                         data-style="form-control" style="float: left;">
                    </div>
                    <div data-role="district"
                         data-name="receiver_district"
                         data-value="大安區"
                         data-style="form-control" style="float: left;">
                    </div>
                    <div data-role="zipcode"
                         data-name="receiver_zipcode"
                         data-value="160"
                         data-style="form-control" style="float: left;width: 173.6px">
                    </div>
                    <div>
                        <input type="text" name="receiver_address" class="form-control">
                    </div>
					</div>
				</div>
				<div class="form-group col-md-7">
					<label for="phone">聯絡電話</label>
					<input type="text" class="form-control" id='receiver_phone' name="receiver_phone" />
					<p class='description'>請留下您的手機號碼,宅配人員將會與您聯繫。</p>
				</div>
      <!--
					<div class="form-group col-md-7">
						<label for="time">配送時間</label>
						<select name="time" id="">
							<option value='any'>不指定</option>
							<option value='12'>中午前</option>
							<option value='17'>12時～17時</option>
							<option value='20'>17時～20時</option>
						</select>
					</div>
      -->
				</form>
				<div id='buttons' class="col-md-7">
					<button type="button" class='btn btn-default' id='prev'>上一步</button>
					<button type="button" class="btn btn-info" id='next'>下一步</button>
				</div>
		</div>
	</div>
