<input type='hidden' id='type_price' value='<?php echo json_encode($type_price) ?>' />
<script src="/assets/javascript/libs/jquery/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/plugin/jquery.datetimepicker.css"/>

<img class='banner-s' src="/assets/images/banner-s/sellorder.jpg">
	<div class='row' id='pagerow'>
		<div style=" width:680px;margin:0 auto; overflow:auto;">
    		<div><center><img src="/assets/images/sellorder/sell-03.jpg" style="margin-top:15px;margin-bottom:15px;"></center></div>

				<h4><img id='clothe_icon' src="/assets/images/secondhand_management/picture_clothes.png" alt="" /><span id="selling">開始賣衣服</span> / <span id="pending_list">待付款貨單狀態</span> / <span id="status">二手衣販售狀態</span></h4>
				<hr class="bold_hr" />
				<h3>本次二手衣販售</h3>
				<hr class="dashed_hr"/>
				<div id="info" class='col-md-8'>
					<div class="row">
						<div class="col-md-12">
							總件數：<span id="all_num_value"></span> 
						</div>
						<!-- <div class="col-md-4" id='small_num'>
							<span id="small_num_value"></span> 小衣
						</div>
						<div class="col-md-4" id='big_num'>
							<span id="big_num_value"></span> 大衣
						</div>
						<div class="col-md-4" id='small_num'>
							<span id="fly_num_value"></span> 羽絨衣
						</div> -->
					</div>
					<hr />
					<div class="row">
						<div class="col-md-10">
							洗衣費小計
						</div>
						<div class="col-md-2" id='washing_fee'>

						</div>
					</div>
					<div class="row">
						<div class="col-md-10">
							運費小計
						</div>
						<div class="col-md-2" id='transport_fee'>
						<?php echo $tran_price; ?>
						</div>
					</div>
      
      <?php if ( $account->balance>0): ?>							
      				<div class="row">
						<div class="col-md-10">
							帳戶餘額
						</div>
						<div class="col-md-2" id='account_balance'><?php echo $account->balance ?></div>
					</div>
      
					<div class="row">
						<div class="col-md-10">
							本次扣抵
						</div>
						<div class="row" id='discount'> 
				            <div class="col-md-6"><input style='width: 200px' type='text' name='deduction_amount' class='deduction_amount form-control' value='0' onkeyup="this.value=this.value.replace(/[^\d]/,'');" /> </div>               
				            <div class="col-md-6"><div class='btn btn-info sure-btn'>確定</div></div>
				        </div>
					</div>
      <?php endif; ?>

					<div class="row">
						<div class="col-md-10">
							輸入洗衣券
						</div>
						<div class="row">
				            <div class="col-md-6"><input style='width: 200px' type='text' name='coupon_code' class='coupon_code form-control' /></div>                
				            <div class="col-md-6"><div class='btn btn-info' >確定</div></div>
				        </div>
					</div>
					
      <hr />
					<div class="row">
						<div class="col-md-10">
							應付餘額
						</div>
						<div class="col-md-2" id='total_fee'></div>
					</div>
				</div>
            
            
            
						<div id='getting_info'>
							<hr class="dashed_hr"/>
							<h3>取件資訊</h3>
							<p>您的姓名：<span id="seller_name"></span></p>
							<p>取件地址：<span id="seller_location"></span></p>
							<p>聯絡電話：<span id="seller_phone"></span></p>
						</div>
						<div id="returning_info">
							<hr class='dashed_hr'/>
							<h3>送回資訊<span class='is_donative'>【我全部衣服都選擇了期滿後捐贈】</span></h3>

							<p class='is_not_donative'>您的姓名：<span id="receiver_name"></span></p>
							<p class='is_not_donative'>送回地址：<span id="receiver_location"></span></p>
							<p class='is_not_donative'>聯絡電話：<span id="receiver_phone"></span></p>

						</div>
						<div id="paying_method">
							<hr class='dashed_hr' />
							<h3>請選擇洗衣費付款方式</h3>
							<p id='ssl_description'>Getmore直接與國泰世華銀行合作，採用銀行端信賴付款機制,受『128-bit SSL』加密保護,請您安心使用!</p>
							<div style="padding-left:10px;">
							<div class='is-not-deduction'>
								<label>
									<input type="radio" class='payment_method' name="payment_method" value="credit-cart" data-type="credit-cart">
									信用卡
								</label>
							</div>
							<div class='is-not-deduction'>
								<label>
									<input type="radio" class='payment_method' name="payment_method" value="atm" data-type="atm">
									ATM轉帳
								</label>
				                <br />
				                <div id='atm_fields'>
				                    <label>匯款帳號後5碼：<input id='last_five_code' name='last_five_code' type='text' maxlength='5' onkeypress='return event.charCode >= 48 && event.charCode <= 57' /></label><br />
				                    <label>預計匯款時間：<input type='text' id='datetimepicker' name='datetimepicker' /></label>
				                </div>
							</div>

							<div class='is-deduction'>
								<label>
									<input type="radio" class='payment_method' id='deduction' name="payment_method" value="deduction">
									全額抵扣
								</label>
							</div>
							</div>
						</div>
						<div id="uniform">
							<hr class='dashed_hr'/>
							<h3>發票資訊</h3>
              
              <h5 class='invoice invoice_0'>捐贈（捐贈單位：創世基金會）</h5>
              <h5 class='invoice invoice_1'>二聯式發票</h5>
              <h5 class='invoice invoice_2'>三聯發票</h5>
              
              
              <p></p>

							<p class='invoice invoice_1 invoice_2'>統一編號：<span id="uniform_invoice_number"></span></p>
							<p class='invoice invoice_2'>發票抬頭：<span id="uniform_invoice_title"></span></p>

						</div>
						<hr class='dashed_hr' />
						<div id='buttons' class="col-md-7">
							<button type="button" class='btn btn-default' onclick="location.href='/sell/second';">上一步</button>
							<button type="button" class="btn btn-info next"  id="confirm-button">確認付款</button>
              <form method='post' action='/sell/fourth' id='final-order'>
                  <input type='hidden' name='order' id='order' />
                  <input type='hidden' name='items' id='items' />              
              </form>
              
              
						</div>

					</div>
	</div>

<script>
    /*
     * The following code are useless form now on.
     * Still keep here as a memorial.
     * They show the bad practice about over-defensive and non-OOD.
     *
     */
  
    /*
    $(document).ready(function(){
      
          var total_price = $('#total_fee').html();
          var account_balance = $('#account_balance').html();      
          
          $('.deduction_amount').change(function(){
              var deduction_amount = $(this).val();
                
              
              if (validateDeductionAmount(deduction_amount)){
                  var final_price = total_price - deduction_amount;
                  
                  if( validateAccountBalance(account_balance, deduction_amount) )
                  {
                      if ( !validateFinalPrice(final_price) ){
                          $(this).val(total_price);
                          //alert("FUCK")
                          // alert(total_price);
                          $('#total_fee').html(0);
                          $("#dumb").prop('checked', true);     
                          $('#paying_method').hide();
                      }else{
               //           alert(final_price);
                          $('#total_fee').html( final_price );
                          $("#dumb").prop('checked', false);     
                          $('#paying_method').show();
                      }
                  }
                  else{
                      $('.deduction_amount').val(0);
                  }
              }
          });
          
            
      });

function isPositiveInteger(n) {
    return n >>> 0 === parseFloat(n);
}

function validateDeductionAmount(deduction_amount){
    console.log('validateDeductionAmount: ' + deduction_amount);
    if ( isNaN(deduction_amount) ){
        alert('請輸入數字');
        return false;
    }

    if ( deduction_amount < 0 ){
        alert('請輸入正整數');
        return false;
    }
    if ( !isPositiveInteger(deduction_amount) ){
        alert('請輸入正整數。');
        return false;              
    }
    
    return true;
}

function validateAccountBalance(account_balance, deduction_amount){
    account_balance = parseInt(account_balance);
    deduction_amount = parseInt(deduction_amount);
    console.log('validateAccountBalance: ' + account_balance + ', ' + deduction_amount);
    if ( deduction_amount > account_balance ){
        alert('您的帳戶餘額不足。');
        return false;              
    }
    
    return true;
  
}

function validateFinalPrice(final_price){
    console.log('validateFinalPrice: ' + final_price);
    if ( isNaN(final_price) ){
        throw new Error('final_price should be number');
    }

    if ( final_price < 0 ){
        // alert('應付餘額不可小於零');
        return false;
    }
    
    if ( final_price == 0 ){
       // alert('應付餘額不可為零');
        return false;
    }
    
    return true;
}
    */
</script>
