<div id="content">
    <div style="padding: 0px 10px;">
        <div class="nav"><a href="/">首頁</a> &#8250; <a href="/subject/lists/all">所有商品</a> &#8250; <a href="/subject/lists/<?php echo $category_id; ?>"><?php echo $category_name; ?></a> &#8250; <?php echo $title; ?></div>
        <div id="project">
            <div class="project-cover">
                <img class="lazy" src="<?php echo site_url('assets/images/white.gif'); ?>" data-original="<?php echo image_url($image_list[1]['file_name'], 350, 350); ?>" width="350" height="350" />
            </div>
            <div class="project-cotent">
                <div class="project-article">
                    <h2><?php echo $title; ?></h2>
                    <?php echo $content; ?>
                </div>
                <div class="project-teacher">
                    <?php
                        $image_list[2]['file_name'] = (isset($image_list[2]['file_name'])) ? $image_list[2]['file_name'] : '';
                    ?>
                    <img src="<?php echo image_url($image_list[2]['file_name'], 500, 120); ?>" />
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div id="content-row-3">
        <div id="row-3">
            <?php foreach($products as $row):?>
            <div class="small-article">
                <div class="small-article-title">
                    <div class="buy-icon"></div>
                </div>
                <div class="small-article-thing">
                    <?php if ($row['inventory_amount'] <= 0): ?>
                    <div class="sold-out"></div>
                    <?php endif; ?>
                    <a href="<?php echo site_url('product/single/' . $row['id']); ?>">
                        <div class="article-img-hover"></div>
                        <div class="article-img-hover-text">get<br />this</div>
                        <div class="small-article-img">
                            <img class="lazy" src="<?php echo site_url('assets/images/white.gif'); ?>" data-original="<?php echo image_url($row['file_name'], 258, 224); ?>" />
                        </div>
                    </a>
                </div>
                <div class="small-article-theme">
                    <h3><?php echo $row['title']; ?></h3>
                    <p class="discounts"><?php echo $row['description']; ?></p>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <p class="clear" style="margin-bottom: 30px;">&nbsp;</p>
    </div>
</div>
