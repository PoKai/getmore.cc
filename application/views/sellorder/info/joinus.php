				<img class='banner-s' src="/assets/images/banner-s/news.jpg">
				<div class='row' id='pagerow'>
					<div class='rightcol padding0'>
						<center>
						<h2>加入GetMore</h2></br>
						<p>一同改變時尚產業吧！/ Join us and change the fashion industry</p>
						<hr>
						</center>
					<div class='col-254'>
						<div class='leftcol'>
						<p>職缺 / Jobs</p>
						<p>
  							<a href="#marketing" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">網路行銷</button></a>
						</p>
						<p>
  							<a href="#engineer" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">軟體工程師</button></a>
						</p>
						<p>
  							<a href="#designer" data-toggle="tab"><button type="button" class="btn btn-primary btn-m">設計師</button></a>
						</p>
						</div>
						<hr>
						<div class='contactus'><p> Contact us <br> service@getmore.cc </p> </div>
					</div>
					<div class='col-726'>
						<div class="tab-content article-line">
							<div class="tab-pane active" id="marketing">
							<div class="floatright">
							<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fgetmore.cc%2Fsellorder%2Finfo%2Fjoinus%2F&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=35&amp;appId=239678122794926" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:35px;" allowTransparency="true"></iframe>
							</div>
							<h3>網路行銷 / Product Marketing</h3><br>
							<p>工作內容：</p>
							<p>行銷企劃、異業合作、贊助洽談、網路活動規劃與執行<p>

							<p>妳（你）是否?</p>		
							<ol>
							<li>擅於文字編輯，有網路內容經營的經驗</li>
							<li>認識很多服裝品牌，瞭解國際流行趨勢，注重穿衣的品質</li>
							<li>覺得打扮美美外出是一種禮貌</li>
							<li>樂於溝通，活潑樂觀</li>
							<li>能應付各種突發狀況</li>
							<li>對於新創公司或創新商業模式有興趣</li>
							</ol><br>

							<p>符合上述條件，快與我們聯繫。</p><br>

							<p>以下條件非必要，但是若符合，妳（你）一定會愛死這裡</p>
							<ol>
							<li>認為喝咖啡是一種享受</li>
							<li>喜歡甜點，烘焙</li>
							<li>覺得逛二手衣市集、穿二手衣好處多多</li></br>
							</ol>
							<br>
							<p>GetMore 是亞洲第一個「二次時尚」購物網站，並提供網路二手衣代售服務。
							<br>團隊主要來自交大、成大、台大，在2013年，得到台北市政府SBIR創新補助。</p><br>

							<p>我們正在尋找一位行銷經理，若妳（你）具備上述條件，請儘速與我們聯絡。</p>
							<p>行銷有機會參與海外擴展的計劃。</p><br>
							<p>數位時代報導：</p>
							</p><a href="http://www.bnext.com.tw/article/view/id/31468">http://www.bnext.com.tw/article/view/id/31468</a></p><br>
							<p>若妳（你）具備上述條件，請儘速與我們聯絡。</p>
							<p>Apply Information</p>
							<p>請將履歷Email至 <a mailto="pokai@getmore.cc">pokai@getmore.cc</a></p>				
							</div>

							<div class="tab-pane" id="engineer">
							<div class="floatright">
							<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fgetmore.cc%2Fsellorder%2Finfo%2Fjoinus%2F&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=35&amp;appId=239678122794926" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:35px;" allowTransparency="true"></iframe>
							</div>
							<h3>軟體工程師 / Soft Engineer</h3><br>
							<p>工作內容:</p>
							<ol>
							<li>使用php按照原本網站核心進行延續開發，使用Linux作業系統</li>
							<li>有web開發經驗,可獨立完成工作</li>
							<li>使用git版本控制佳</li>
							<li>使用framework佳</li>
							<li>前端工程略懂佳</li>
							<li>使用coffeescript，backbone佳</li>
							</ol>

							<p>GetMore 是亞洲第一個「二次時尚」購物網站，並提供網路二手衣代售服務。
							<br>團隊主要來自交大、成大、台大，在2013年，得到台北市政府SBIR創新補助。</p><br>

							<p>數位時代報導：</p>
							</p><a href="http://www.bnext.com.tw/article/view/id/31468" target="_blank">http://www.bnext.com.tw/article/view/id/31468</a></p>
							<p>若你具備上述條件，請儘速與我們聯絡。</p><br>
							
							<p>Apply Information</p>
							<p>請將履歷Email至 <a mailto="pokai@getmore.cc">pokai@getmore.cc</a></p>
							</div>

							<div class="tab-pane" id="designer">
							<div class="floatright">
							<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fgetmore.cc%2Fsellorder%2Finfo%2Fjoinus%2F&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=35&amp;appId=239678122794926" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:35px;" allowTransparency="true"></iframe>
							</div>
							<h3>設計師 / Designer</h3><br>
							</p>工作內容:</p>
							<ol>
							<li>網站設計，前端切板</li>
							<li>視覺及品牌形象設計</li>
							<li>平面設計及行銷設計</li>
							<li>攝影及修圖</li>
							</ol>

							<p>你是否?</p>
							<ol>
							<li>網站Layout設計經驗一年以上，熟DIV CSS切版</li>
							<li>能與同事溝通配合、主動積極</li>
							<li>對網站前端工程(HTML5/CSS3/Responsive)有興趣者為佳</li>
							<li>喜歡攝影</li>
							<li>樂於溝通，主動樂觀</li>
							<li>能應付各種突發狀況</li>
							<li>對於新創公司或創新商業模式有興趣</li>
							</ol><br>

							<p>符合上述條件，快與我們聯繫。</p><br>

							<p>以下條件非必要，但是若符合，妳（你）一定會愛死這裡</p>
							<ol>
							<li>認為喝咖啡是一種享受</li>
							<li>喜歡甜點，烘焙</li>
							<li>覺得逛二手衣市集、穿二手衣好處多多</li>
							</ol></br>

							<p>GetMore 是亞洲第一個「二次時尚」購物網站，並提供網路二手衣代售服務。
							<br>團隊主要來自交大、成大、台大，在2013年，得到台北市政府SBIR創新補助。</p><br>

							<p>數位時代報導：</p>
							</p><a href="http://www.bnext.com.tw/article/view/id/31468" target="_blank">http://www.bnext.com.tw/article/view/id/31468</a></p>
							<p>若你具備上述條件，請儘速與我們聯絡。</p>
							<p>Apply Information</p>
							<p>請將履歷Email至 <a mailto="pokai@getmore.cc">pokai@getmore.cc</a></p>
							</div>
					</div>
					</div>
				</div>
				</div>