				<div class='row' id='pagerow'>
						<img src="/assets/images/info/redress01.jpg">
						<div class="margintop40 marginbottom40">
							<h4>Get Redressed in Taiwan：GetMore x Redress</h4>
						</div>
						<ul class="nav nav-tabs">
							  <li class="active"><a href="#redress" data-toggle="tab">Introduction</a></li>
							  <li><a href="#getredressed" data-toggle="tab">Get Redressed</a></li>
						</ul>
						<div class="tab-content article-line">
							<div class="tab-pane active" id="redress">
								<div class="margintop40 marginbottom40">
								<p>Redress 是一個國際非政府組織，總部位於香港，<br>創辦人 Christina是一位英國人，致力透過減少浪費紡織品、污染、節約用水及節省能源，推動時裝業的可持續發展。</p>
								<p>GetMore 團隊在2014年3月前往香港拜訪 Redress，未來會有更多跨地合作，一起推廣「綠時尚」的理念。<br>Redress 和 GetMore的第一項合作就是「365挑戰」</p>
								<p><a href="http://redress.com.hk/" target="_blank">（http://redress.com.hk/）</a></p>
								</div>
								<div class="marginbottom40">
								<h4>365挑戰 / The 365 Challenge 2013</h4>
								</div>
								<div class="margintop40  marginbottom20">
								<h4>「重新配搭，不要丟棄」/ Redress it, don’t bin it</h4>
								</div>
								<p>Redress的創辦人Christina Dean在2013年，全年只穿著被丟棄或捐贈所得的二手衣物，身體力行宣傳「重新配搭，不要丟棄」的理念。每一天，Christina 都會穿著從夥伴機構香港地球之友所得的二手衣物，並由12位時裝達人幫助搭配造型，展示365種充滿啟發性的造型，讓紡織廢料留在時裝圈內，而非丟棄。這個挑戰幫助消費者領悟到延長衣物壽命的祕訣，進而避免把仍然有用及時尚的服飾丟棄。除此之外，這活動亦能提高大眾對龐大紡織廢料的意識，讓他們在購買衣物時以環保為前提。</p>
								<div class="margintop40  marginbottom20">
								<h4>為什麼要重新配搭？</h4>
								</div>
								<p>今日，我們花在時裝上的消費比10年前增加了大約百分之60，導致每年愈來愈多紡織廢料被丟棄。單在2011年，香港人每天平均丟棄至3個掩埋場的衣物達217噸。在英國，每年送往掩埋場的衣物達350,000噸，估計價值1億4千萬英鎊。</p>
								<p>不能被生物分解的棄置衣物對環境構成極沉重的負擔，不但為堆填區帶來龐大壓力，同時亦產生各種改變氣候的化學物質。然而，幾乎所有紡織品皆可循環再用。透過重新配搭、修補、重新構造等方式，衣物能創造出既別具創意，又時尚經濟的造型。衣物亦能透過循環再用、升級再造或降級回收獲得「新生命」，減低對原材料和堆填區的壓力。</p>
								<p>延長衣物壽命、選購二手服裝對環境有著正面的影響。舉例說，循環再用1公斤的二手衣物，相比購買新衣物能減省達3.6公斤的二氧化碳排放量、6000升水、0.3公斤的肥料及0.2公斤的殺蟲劑。 (哥本哈根大學，2008)</p>

								<div class="margintop40 marginbottom20">
								<h4>Get Redressed挑戰2014</h4>
								</div>
								<p>Get Redressed挑戰是一項為期一年的消費者挑戰，於2014年1月至12月舉行，由世界各地人士穿著365套可持續服裝啟發人心。這項挑戰旨在教育消費者他們的服飾、衣物護理及衣櫃管理對環境的正面影響，並推廣「重新配搭，不要丟棄」的理念。</p>
								<p>他們會在全年涵蓋12個可持續時裝主題，包括「小黑裙」、潮換、DIY、潮流、洗滌護理、重新造型、修補及重新構造，為消費者提供意念及資訊延長衣物壽命。這些行動的影響甚廣；在英國，延長衣物平均壽命9個月可節省用於供應、洗滌及棄置衣物的資源，價值高達50億英鎊。(資料來源：Valuing our Clothes, WRAP 2012)</p>
								</p>GetMore現在一同加入，將理念推廣至台灣，為時尚與環境的永續平衡做努力。如有興趣想一同加入計劃或合作的朋友，也歡迎聯絡我們喔！</p>

								<p>（service@getmore.cc）</p>

								<p class="text-right">資料來源：Redress</p>
								<img src="/assets/images/info/redress02.png">
								
							</div>
							<div class="tab-pane" id="getredressed">
								<div class="margintop40 marginbottom40">
								<p>Get Redressed for April-縫補 : 從衣櫃挑選一件已破損的衣物，給它多一點關愛</p>
								<p>以下是 GetMore 團隊或是會員一同參與的計畫，同時刊登於 Redress 的 instagram </p>
								<p><a href="http://instagram.com/GetRedressed/" target="_blank">（http://instagram.com/GetRedressed/）</a></p>
								<div class="container">
								  <div class="row">
								  	<div class="getregressed-photo">
								    <a href="http://blog.getmore.cc/?p=954" target="_blank"><img src="/assets/images/info/getredressed1.png"></a>
									</div>
									<div class="getregressed-photo">
								    <a href="http://blog.getmore.cc/?p=958" target="_blank"><img src="/assets/images/info/getredressed2.png"></a>
									</div>
									<div class="getregressed-photo">
								    <a href="http://blog.getmore.cc/?p=966" target="_blank"><img src="/assets/images/info/getredressed3.png"></a>
									</div>
									<div class="getregressed-photo">
								    <a href="http://blog.getmore.cc/?p=970" target="_blank"><img src="/assets/images/info/getredressed4.png"></a>
									</div>
								  </div>
								  <div class="row">
								  	<div class="getregressed-photo">
								    <a href="http://blog.getmore.cc/?p=954" target="_blank">Vivian’s special wool cape</a>
									</div>
									<div class="getregressed-photo">
								    <a href="http://blog.getmore.cc/?p=958" target="_blank">Kuan‘s sweet gift</a>
									</div>
									<div class="getregressed-photo">
								    <a href="http://blog.getmore.cc/?p=966" target="_blank">Connie’s knit cardigan</a>
									</div>
									<div class="getregressed-photo">
								    <a href="http://blog.getmore.cc/?p=970" target="_blank">Anna’s life style with second clothing</a>
									</div>
								  </div>
								</div>
								</div>
							</div>
						</div>

				</div>

				<style>

				.getregressed-photo{
					margin-right:30px;
					margin-left:30px;
					margin-top: 30px;
					float: left; 
					width: 185px;
				}

				.getregressed-photo a{
					color:#333333;
				}
				</style>