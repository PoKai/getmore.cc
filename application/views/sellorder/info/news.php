				<img class='banner-s' src="/assets/images/banner-s/news.jpg">
				<div class='row' id='pagerow'>
						<div class='rightcol padding0'>
						<center>
						<h2>新聞室</h2></br>
						<p>Hi，各位媒體朋友，</p></br>

						<p>對我們的網站有興趣嗎？想要了解更多關於GetMore二次時尚嗎? </p>
						<p>請參考我們的新聞稿，或直接與我們聯繫。</p></br>
						<p><a href="mailto:service@getmore.cc" target='_blank'>service@getmore.cc</a></br>
						02 23259881</p></br>
						<hr>
						</center>
						<h4>媒體報導</h4><br>
						<img src="/assets/images/info/report.jpg">
						<p><a href="http://www.bnext.com.tw/article/view/id/31468" target="_blank">● [Meet創業之星] GetMore二次時尚－二手衣代售服務，讓無用衣物發揮新價值！</a></p>
						<p><a href="http://www.bnext.com.tw/article/view/id/31842" target="_blank">● 從生活中找尋創業新點子</a></p>
						<p><a href="http://www.chinatimes.com/realtimenews/20140327004155-260402" target="_blank">● 自身經驗萌想法 年輕人創業</a></p>
						<hr>
						<h4>新聞稿</h4><br>
						<p>● 網路二手衣代售服務：亞洲第一個二次時尚購物網站，正式上線</p></br>
						<p>GetMore 是亞洲第一個「二次時尚」購物網站，並且提供雲端二手衣代售服務。</p></br>
						<p>鎖定「衣櫃塞爆」的時尚族群，利用GetMore二次時尚的雲端系統，簡單輸入衣服資料，線上支付洗衣費後，
						會有物流人員上門收件，之後送洗、拍照、丈量、上架、包裝出貨的事情，網站業者通通幫你搞定! 
						GetMore二次時尚同時也是新型態網路服飾商店，上千件品牌二手衣，平均折扣2.5折起，
						標榜每一件衣服都是經過嚴格清洗，且獨一無二，只有一件。</p><br>
						
						<p>● 創造時尚新文化運動：二次時尚</p></br>
						<p>面對當今服飾產業的潮流「快速時尚」，ＧetMore反其道而行，提出新概念「二次時尚」。</p></br>
						<p>二次時尚所表達的含義，不只是二手衣的時尚，而是時尚的再次創造、再次定義、再次思考，以及重視質感、
							環保、獨特的精神。相對於「快速時尚」追求流行、快速丟棄所造成的浪費，
							ＧetMore二次時尚相信服飾的價值是可以再被創造的，提倡環保、注重品質、忠於自我的時尚態度。
							二次時尚ＧetMore想傳達的是，穿二手衣是一種時尚、一種生活態度。</p><br>

						<p>● 延續歐美「分享經濟」概念，讓好衣服有再次被愛的機會</p></br>
						<p>近年來，歐美開始興起「分享經濟」，這是一個足以改變我們熟悉的各種經濟生活的革命。ＧetMore二次時尚將這樣的經濟概
						   念運用在現在的服飾產業，服飾某部分的來源出自於大家的衣櫃，這些我們平時不常穿的好衣，可以以合理的價格分享出去，
						   讓好衣服有再次被愛的機會。</p></br>

						 <p>● 品質嚴格把關，一律送洗最安心</p></br>
						<p>ＧetMore二次時尚想打造的是一個環保、獨特、注重服飾質感的時尚文化，因此對於二手衣的代售條件也相對嚴謹許多。
							例如：ＧetMore只收全新或九成新的衣服、不能有瑕疵、建議大家拿有品牌或是高質感的衣服等等。最特別的是，
							在ＧetMore的每件衣服，一律會送到專業的洗衣公司送洗，因為即使是全新的衣服，也要經過清洗才能保證安全。
							GetMore透過這樣的方式，維持一貫的品質與保證，讓買賣家都能安心。</p></br>

						<img src="/assets/images/content/news/nt-04.jpg"></br>
						</div>
				</div>