				<img class='banner-s' src="/assets/images/banner-s/about/partner.jpg">
				<div class='row' id='pagerow'>
					<div class='col-254'>
						<div class='leftcol'>
						<p>
  							<a href='/sellorder/info/sellintro/'><button type="button" class="btn btn-primary btn-lg">我要賣衣服</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/buyintro/'><button type="button" class="btn btn-primary btn-lg">我要買衣服</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/washintro/'><button type="button" class="btn btn-primary btn-lg">保證嚴格清洗</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/userpolicy/'><button type="button" class="btn btn-primary btn-lg">使用條款</button></a>
						</p>
						</div>
						<hr>
						<div class='contactus'><p> Contact us <br> service@getmore.cc </p> </div>
					</div>
					<div class='col-726'>
						<div class='rightcol'>
						<h2>使用GetMore好處多!</h2></br>
						<h4>使用 GetMore 賣衣服的好處多多，來跟你（妳）說：</h4></br>
						<p>1. 只要在網站上輸入一些訊息，其他 GetMore 都幫您包了。</p>
						<p>以往在拍賣平台上賣衣服，要經歷繁複的流程，好不容易上架了，還要處理寄送面交等問題….</p>
						<p>使用GetMore 賣衣服，從洗衣、整燙、拍照、丈量、上架、寄送，都由專人幫您服務，省時又省力。</p></br>
						<p>2. 只要在網站上輸入一些訊息，其他 GetMore 都幫您包了。</p>
						<p>GetMore 與臻穎專業洗衣工廠合作，是全台灣最厲害的精緻洗衣（全家潔衣家合作工廠），全站保證每一件衣服都是清洗整燙過的，上架採
						用獨創的自動化系統，買家購買安心，成交率高！</p></br>
						<p>3. 精打細算，用 GetMore 省運費，賣出才收手續費，真划算！</p>
						<p>換季時，想把衣櫃裡穿不到的衣服出售，一筆成交就必須付一筆運費，大失血外還要每筆自己寄送。在GetMore，賣出才收手續費，還包含運
						費，若期限內沒賣出，送洗過的乾淨衣服送回，怎麼算，還是 GetMore 划算！</p></br>
						<p>4. 為什麼一定要洗衣？</p>
						<p>洗衣的流程也是 GetMore 的好處之一，以往在拍賣平台上賣衣服，不洗的衣服沒人買，洗了買家又不相信，在 GetMore，每一件衣服都是保
						證被洗過的，而且重點是大家都相信。</p></br>
						<h2>使用GetMore好處多!</h2></br>
						<h4><a href="http://getmore.cc/sellorder/steps/1/"><font color='#008195'>→ 開始賣衣服！</font></a></h4>
						<img src="/assets/images/content/benefit/benefitinfo.jpg">
						</br>
						</div>
					</div>
				</div>
