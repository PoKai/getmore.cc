				<div id="main-carousel" class="carousel slide">
					<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<div class="item active">
							<img src="/assets/images/banner/banner-02.jpg">
						</div>
						<div class="item">
							<img src="/assets/images/banner/banner-03.jpg">
						</div>
						<div class="item">
							<img src="/assets/images/banner/banner-05.jpg">
						</div>
						<div class="item">
							<img src="/assets/images/banner/banner-06.jpg">
						</div>
						<div class="item">
							<img src="/assets/images/banner/banner-07.jpg">
						</div>
						<div class="item">
							<img src="/assets/images/banner/banner-08	.jpg">
						</div>
					</div>

					<!-- Controls -->
					<a class="left carousel-control" href="#main-carousel" data-slide="prev">
						<span class="icon-prev"></span>
					</a>
					<a class="right carousel-control" href="#main-carousel" data-slide="next">
						<span class="icon-next"></span>
					</a>
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#main-carousel" data-slide-to="0" class="active"></li>
						<li data-target="#main-carousel" data-slide-to="1"></li>
						<li data-target="#main-carousel" data-slide-to="2"></li>
						<li data-target="#main-carousel" data-slide-to="3"></li>
						<li data-target="#main-carousel" data-slide-to="4"></li>
						<li data-target="#main-carousel" data-slide-to="5"></li>
						<li data-target="#main-carousel" data-slide-to="6"></li>
						<li data-target="#main-carousel" data-slide-to="7"></li>
					</ol>
				</div>

				<div class="newproduct">
						<div class='hometitle'><h4>新鮮貨</h4>
							 <hr>
						</div>
						<div class='row'>
							<div class='col-md-3'>
								<img src="/assets/images/product/cc-01.png">
							</div>
							<div class='col-md-3'>
								<img src="/assets/images/product/cc-02.png">
							</div>
							<div class='col-md-3'>
								<img src="/assets/images/product/cc-03.png">
							</div>
							<div class='col-md-3'>
								<img src="/assets/images/product/cc-04.png">
							</div>
						</div>
				</div>