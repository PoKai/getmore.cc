				<img class='banner-s' src="/assets/images/banner-s/about/intro.jpg">
				<div class='row' id='pagerow'>
					<div class='col-254'>
						<div class='leftcol'>
						<p>
  							<a href='/sellorder/info/intro/'><button type="button" class="btn btn-primary btn-lg">GetMore簡介</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/energy/'><button type="button" class="btn btn-primary btn-lg">GetMore Energy</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/idea/'><button type="button" class="btn btn-primary btn-lg">GetMore理念</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/coper/'><button type="button" class="btn btn-primary btn-lg">合作夥伴</button></a>
						</p>
						</div>
						<hr>
						<div class='contactus'><p> Contact us <br> service@getmore.cc </p> </div>
					</div>
					<div class='col-726'>
						<div class='rightcol'>
						<h2>GetMore 簡介</h2></br>
						<h4><font color='#52AF7E'>電子商務＋環保＋時尚＝ GetMore二次時尚</font></h4></br>
						<p>「GetMore二次時尚」是一個為時尚生活打造的二手服飾買賣網站</p> 
						<p>用環保、時尚、獨特的方式打造「二次時尚」的生活，實踐SLOW FASHION。</p> 
						<p>以電子商務為核心，透過 web 技術串起的服務，GetMore 讓服飾的價值被重新定義。</p></br>
						<a data-toggle="modal" href="#modalbtn-04"><img src='/assets/images/content/intro/btn.jpg'></a><br><br>
						<p>擁有電子商務、環保、時尚三大基因，GetMore二次時尚想要讓每個人用最簡單、有趣的方式，享受二手衣的時尚生活。透過網路，隨時隨地，進入二次時尚的網站，可以輕鬆挑選網站上獨一無二的好衣服，用可以負擔的好價格買到，每一件都是經過清洗整燙，且享有七天的鑑賞期。GetMore也提供二手衣的代售服務，許多時尚人士因為工作或是購物方式等原因，常有汰換衣物的需求，透過代售服務，閒置在衣櫃裡的好衣服，經過清洗，有機會找到新的主人，時尚人士則透過這個流程，換取現金，增加收入同時也增加空間。</p>
						<p>這是一個顛覆以往服飾工業供應鏈流程的創新服務。</p></br>
						<h4>從台灣開始，一群有志青年，打造顛覆傳統服飾工業的二手衣買賣服務！</h4><br>
						<p>GetMore二次時尚的創意來源，來自創辦人温博凱，因為女友有衣櫃塞爆的問題，許多品牌衣服甚至連吊牌都沒拆，認為可以有更好的方式來管理衣服，因而有創立二次時尚的想法。</p>
						<p>在瞭解歐美時尚圈引領的二手衣或古著衣風潮後，決定付諸行動。</p>
						<p>GetMore 成立於 2011年9月，總部位於台北大安區巷弄內的老公寓一樓，在2013年，獲得台北市政府地方產業創新研發推動計畫（SBIR）的輔導企業。</p></br>
						<ul>
						<li>2011年9月成立</li>
						<li>100年度教育部U-START計畫專案</li>
						<li>進駐交通大學創新育成中心</li>
						<li>進駐政大創立方</li>
						<li>2012年10月，我們的工作室，你的咖啡館，getmore lab 開幕</li>
						<li>102年度台北市SBIR創新研發計劃</li>
						<li>2013年12月，GetMore Storage成立，專業二手衣倉櫥空間</li>
						<li>2013年12月底，亞洲第一個二次時尚購物網站兼二手衣代售服務-GetMore，正式上線</li>
						<li>2014年1月，推出「超級VIP計畫·衣管家到你家」服務，迴響熱烈</li>
						<li>2014年2月，入圍數位時代第5屆綠色品牌</li>

						</ul>
						</div>
						<p>來看看<a href='/sellorder/info/energy/'> GetMore的團隊。</a></p></br></br>
						<h4><font color='#52AF7E'>【品牌理念】</font></h4></br>
						<p>GetMore從生活經驗出發，不管是自身，或是與人交談之中，我們發現了一個問題：衣櫃裡有些衣服，買了卻很少穿或根本沒穿過，不穿浪費，但丟了又可惜，總是令人感到尷尬。因此，我們想幫助大家「解決衣櫃裡尷尬的衣服」，並且讓這件事能延伸出更大的意義，如同我們品牌的名稱「GetMore」，我們希望能創造更多價值。</p>
						</br></br>
						<p><B>GetMore< 讓好衣服有再次被愛的機會></b></p>
						<p>你的衣櫃裡，總是有些不常穿，或一次也沒穿過的衣服，</p>
						<p>也許是你衝動之下買的、或者是買回家發現不適合你，</p>
						<p>還是買回來過一段時間後，你改變了風格不穿了。</p>
						<p>雖然這些衣服不再適合自己，</p>
						<p>但它們仍然保持光鮮的樣貌，</p>
						<p>靜靜地躺在你的衣櫃等待。</p>
						<p>別再蹉跎漠視，</p>
						<p>讓它們再次站上舞台，</p>
						<p>尋找更愛它的主人。</p>
						</br></br>
						<p><B>GetMore< 二次時尚></b></p>
						<p>簡約、華麗、復古、幾何、優雅……</p>
						<p>跨越時代的流行元素與文化，</p>
						<p>自由組合、碰撞，</p>
						<p>在這裡，</p>
						<p>品牌與價格無法限制甚麼，</p>
						<p>時尚的決定權在於你自己。</p>
					</div>
				</div>
