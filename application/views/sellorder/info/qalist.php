				<img class='banner-s' src="/assets/images/banner-s/userpolicy.jpg">
				<div class='row' id='pagerow'>
					<div class='col-254'>
						<div class='leftcol'>
						<p>
  							<a href='/sellorder/info/qalist/'><button type="button" class="btn btn-primary btn-lg">Q&A</button></a>
						</p>
						</div>
						<hr>
						<div class='contactus'><p> Contact us <br> service@getmore.cc </p> </div>
					</div>
					<div class='col-726'>
					<div class='rightcol'>
					
					<h2>Q&A-賣二手衣</h2></br>
						
						<div class="panel-group" id="accordion">
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q001">
						        Q1：什麼樣的衣服可以拿去賣？
						        </a>
						      </h4>
						    </div>
						    <div id="q001" class="panel-collapse collapse">
						      <div class="panel-body">
								A1：根據統計，大部份的人在80%的時間只穿衣櫃裡20%的衣服。也就是說，有80%的衣服很少穿到甚至幾乎沒有穿過，其中不乏9成新或是全新吊牌未拆的衣服，這些衣服因為一些原因妳 / 你都沒有去穿它，這就是我們要找的衣服！
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q002">
						         Q2：所以是我不要 / 不喜歡的舊衣服都可以拿出來賣嗎？
						        </a>
						      </h4>
						    </div>
						    <div id="q002" class="panel-collapse collapse">
						      <div class="panel-body">
						        A2：不是！其實是妳 / 你仍然喜歡，總想著有一天會穿，但都沒有穿或是很少穿到的衣服，因此它的狀態應該是全新、9成新的(lightly used)。把它拿到GetMore上賣，讓不會冷落這件衣服的人有機會擁有它。只要妳 / 你試著割愛，就能讓好衣服有再次被愛的機會！至於陳年的舊衣服或是破損、髒污的衣服就交給舊衣回收桶或是垃圾桶吧！
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q003">
						          Q3：我是直接將衣服賣給GetMore嗎？
						        </a>
						      </h4>
						    </div>
						    <div id="q003" class="panel-collapse collapse">
						      <div class="panel-body">
						          A3：不是！GetMore是提供代售服務，衣服的物權仍歸您所有。GetMore在代售保管期間（衣服上架到銷售至消費者，或是到代售期終止這段期間），會將妳 / 你的衣服經由專業洗衣公司清洗整燙，並透過系統化的上架流程，拍照、丈量、驗收狀態，完整地將妳 / 你的衣物透過我們的網站呈現給消費者。
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q004">
						         Q4：賣衣服需要負擔任何費用嗎？
						        </a>
						      </h4>
						    </div>
						    <div id="q004" class="panel-collapse collapse">
						      <div class="panel-body">
								 A4：需要，我們分成兩階段收取費用。第一階段是洗衣服的費用， 為保障會員權益，所有在GetMore上架的衣物皆需經由我們驗收後交給洗衣公司清洗，以確保品質，因此會先向您收取洗衣費用（洗衣費用說明）。第二階段是衣服賣出後，GetMore會扣除衣服銷售金額的25%，做為上架代售手續費，剩餘的75%會入到您的線上帳戶；若衣服未賣出，則不會收取手續費。（ps. GetMore負責洗衣、上架、行銷等流程，但並不保證您一定能獲利。建議您提供符合GetMore理念與規範的衣物，提高成交率。）
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q005">
						        Q5：我要如何開始賣衣服呢？
						        </a>
						      </h4>
						    </div>
						    <div id="q005" class="panel-collapse collapse">
						      <div class="panel-body">
								A5：若您也認同GetMore的理念，就趕緊將衣櫃中的好衣服拿出來吧！只要在網站首頁右上方點選『賣衣服』，按照賣衣4步驟輕鬆填寫相關資訊，待物流士到府收件後，其餘的麻煩事就交給GetMore處理吧！
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q006">
						        Q6：有限定哪些品牌才能上架販賣嗎？
						        </a>
						      </h4>
						    </div>
						    <div id="q006" class="panel-collapse collapse">
						      <div class="panel-body">
						        A6：沒有，只要您認為它是件好衣服、有特色且狀況良好（9成新、全新）且願意負擔清洗的費用都可以交給我們賣。但是，GetMore不收仿冒品，請確實填寫品牌名稱，若不是真品，請不要提出寄售申請。ps.有品牌的衣服由於設計、材質等因素相對較有二手價值，在市場上詢問度也比較高喔！
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q007">
								Q7：有不能賣的款式嗎？
						        </a>
						      </h4>
						    </div>
						    <div id="q007" class="panel-collapse collapse">
						      <div class="panel-body">
								A7：以下狀況的衣物請勿提出申請</br>
    						   【貼身衣物】 基於保障消費者個人衛生，GetMore只賣外衣(outerwear)，貼身衣物（內睡 衣、泳衣、貼身內搭褲）無論新舊程度， 一律不賣。
                               【衣服有瑕疵】污漬、破損、嚴重脫線、縮水、裂紗/勾紗、褪色、裝飾脫落、圖案掉漆/龜裂、起毛球、保存不良(包含纖維老化、纖維脫落、布料鬆弛等狀況都會造成衣服明顯陳舊)。(衣服上若有污漬，請自行判斷是否能清洗掉。如清洗完畢後，汙漬仍殘留，一概以退回處理。）
                               【 其它狀況】
                                1.真皮、麂皮衣物暫時不收，待日後開放，敬請見諒!
                                2. GetMore 拒收仿冒品，若不是真品，請不要提出寄售申請。
                                3. 由於某些品牌過於普及且平價(原價)，較不利於販售，為了您的權益，GetMore不收以下品牌： lativ、Hang Ten、Net、 Pazzo、東京著衣、A&D。
                                4. 衣服原價低於500元。

						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q008">
						        Q8：請問二手衣的售價是我自己決定嗎？
						        </a>
						      </h4>
						    </div>
						    <div id="q008" class="panel-collapse collapse">
						      <div class="panel-body">
						        A8：二手衣的售價由賣家自行決定，依照衣服的狀況訂定合理的價格，建議為原價的3折～6折（因賣家需負擔些許成本，售價請不要低於150元，即衣服原價要在500元以上）。
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q009">
						        Q9：我真的不記得衣服原價多少錢怎麼辦？
						        </a>
						      </h4>
						    </div>
						    <div id="q009" class="panel-collapse collapse">
						      <div class="panel-body">
								A9：您可以參考該品牌同類型的商品售價大約落在哪個價位區間，請盡量照實填寫，若與事實不符，GetMore保留修改權利。
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q010">
						        Q10：可以賣真皮皮衣嗎？
						        </a>
						      </h4>
						    </div>
						    <div id="q010" class="panel-collapse collapse">
						      <div class="panel-body">
								A10：真皮皮衣的清洗需要特殊處理，目前暫時不提供此服務。但請放心，我們已經與專業清洗皮衣的公司接洽，相關服務即將上線！								      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q011">
						        Q11：我的衣服是全新的 / 已經洗過的，可以不要送洗直接上架嗎？
						        </a>
						      </h4>
						    </div>
						    <div id="q011" class="panel-collapse collapse">
						      <div class="panel-body">
								A11：為保障所有會員權益，所有在GetMore上架的衣物皆需經由我們驗收後交給專業洗衣公司清洗或做整新處理，以確保品質。								      </div>
						    </div>
						  </div>						  
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q012">
						        Q12：請問GetMore收到我的衣服後會怎麼處理呢？
						        </a>
						      </h4>
						    </div>
						    <div id="q012" class="panel-collapse collapse">
						      <div class="panel-body">
								A12：我們收到您的衣服後，首先會驗點數量、品項與您提出的申請是否相符，並針對每件衣服檢查是否符合上架規定，為保障您的權益，衣物從拆封到驗收全程均會錄影，若有瑕疵會拍照，並與您聯繫告知不予上架，您可以選擇不洗或是仍要送洗。
        						</br>GetMore完成初步驗收後會將您心愛的衣物送至洗衣公司，再進行第二次高規格的檢查，為您的衣物把關（高規格23項衣物檢查項目為：縮水、褪色/變色，破損、移染、汙漬、變形/變質、起毛、脫絨、起紗/勾紗、自黏裡襯剝離、配件脫落損壞、硬化、裡襯膠滲出/硬化、伸長、亮片脫落/褪色、皮革/拉鏈染色、整燙失當、鬆緊布料鬆弛、逆汙染、金(亮)粉脫落、摺痕消失、墊肩變形）。若檢查出上述瑕疵，如同第一階段會與您聯繫告知不予上架，您一樣可以選擇不洗或是仍要送洗。但若您決定送洗，清洗後原瑕疵惡化造成衣服損壞，GetMore恕不負責。								      </div>
						    </div>
						  </div>						 
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q013">
						        Q13：我接到客服電話反應衣服破損不能上架，但是我已經付洗衣費了怎麼辦？
						        </a>
						      </h4>
						    </div>
						    <div id="q013" class="panel-collapse collapse">
						      <div class="panel-body">
								A13：請放心！未送洗衣服，其洗衣費會退到您的線上帳戶，但會先扣除您應負擔的手續費後一併退還。為避免您額外支出不必要的手續費，以及耽誤您其他衣物上架的時間，請在提出申請前仔細檢視您的所有的衣物是否符合GetMore的規範。								      </div>
						     </div>
						  </div>						 
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q014">
						        Q14：我提出申請的所有衣服都可以上架販賣嗎？
						        </a>
						      </h4>
						    </div>
						    <div id="q014" class="panel-collapse collapse">
						      <div class="panel-body">
								A14：若您的衣物都符合GetMore的規範，就會幫您全數上架；除上述不符規範的衣物不能上架外，若您的衣物經洗衣公司驗收後認定清洗會有損毀風險，經聯繫說明後，賣家仍堅持要清洗，洗後若有損壞則不予上架。</div>
						     </div>
						  </div>

						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q015">
						        Q15：請詳細說明若衣服不符合規範需要負擔的費用。
						        </a>
						      </h4>
						    </div>
						    <div id="q015" class="panel-collapse collapse">
						      <div class="panel-body">
								A15：我們收到您寄售的好衣，在送洗前會經過人員詳細檢驗；送洗後、上架前也會再次檢驗。若衣服不符合上架規範賣家需要負擔的費用以下分為兩個階段做說明：</br>
                                    【送洗前】若經人員檢驗發現您的衣物有不符合上架規範的狀況，我們會將不予上架的衣物寄還給您，並退還洗衣費用。但您需要負擔返還運費80元，若不符規範的衣服超過2件，第3件起，每件加收20元處理費，處理費金額以300元為上限。</br>
                                    【送洗後】若您的衣服在送洗後有污漬無法清除等不能上架的狀況，我們也會將不予上架的衣物寄還給您，且您需要負擔返還運費80元。
                             </div>
						  	</div>
						  </div>

						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q016">
						        Q16：如果是洗衣公司疏失把我的衣服洗壞了怎麼辦？
						        </a>
						      </h4>
						    </div>
						    <div id="q016" class="panel-collapse collapse">
						      <div class="panel-body">
								A16：將依照經濟部商業司頒布的《洗衣定型化契約》載明之規定進行賠償。
							 </div>
						  	</div>
						  </div>

						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q017">
						        Q17：若衣服在代售期間內我不想賣了，可以拿回來嗎？
						        </a>
						      </h4>
						    </div>

						    <div id="q017" class="panel-collapse collapse">
						      <div class="panel-body">
								A17：可以，但您需要負擔返還運費80元，以及上架途中領回處理費100元/件。
							  </div>
						    </div>
						   </div> 

						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q018">
						        Q18：衣服賣出去後我就可以拿到錢嗎？
						        </a>
						      </h4>
						    </div>
						    <div id="q018" class="panel-collapse collapse">
						      <div class="panel-body">
								A18：您的衣服售出後，因買家享有七天鑑賞期之時間，為避免買家退貨造成金額浮動，您賣衣服所得將於出貨後15～30天內入到「線上帳戶」（GetMore固定為每月的15、30號撥款）。</br>
								範例：</br>
								買家已下訂，且於9月16日～9月30日出貨之二手衣，款項則於10月15日匯入。</br>
								買家已下訂，且於10月1日～10月15日出貨之二手衣，款項則於10月30日匯入。</br>
								以此類推。
							  </div>
						    </div>
						   </div> 

						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q019">
						        Q19：如果衣服沒有賣掉就要退還給我嗎？有沒有其他選擇？
						        </a>
						      </h4>
						    </div>
						    <div id="q019" class="panel-collapse collapse">
						      <div class="panel-body">
								A19：在您開始申請賣衣時，就會徵詢您的意願，若衣服在GetMore上架六個月後沒有賣出，您可以選擇退回指定地址，或是全數捐贈給公益團體。
							  </div>
						    </div>
						  </div>

						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#q020">
						        Q20：什麼是『一時貿易所得』？
						        </a>
						      </h4>
						    </div>
						    <div id="q020" class="panel-collapse collapse">
						      <div class="panel-body">
								A20：一時貿易所得是指「臨時、非常態性」的買賣收入，因此您的衣服在GetMore上售出則適用之。根據稅捐機關規定，商家需兩個月申報一次，您可來信詢問相關資料。另外，GetMore會於每年報稅前寄發扣繳憑單至您指定的地址。
							  </div>
						    </div>
						  </div>
						</div>
						
					</div>
					</div>
				</div>