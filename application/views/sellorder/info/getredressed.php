				<div class='row' id='pagerow'>
						<img src="/assets/images/info/redress01.jpg">
						<div class="margintop40">
							<h2>Get Redressed in Taiwan：GetMore x Redress</h2>
						</div>
						<ul class="nav nav-tabs">
							  <li><a href="/sellorder/info/redress">Introduction</a></li>
							  <li class="active"><a href="/sellorder/info/getredressed">Get Redressed</a></li>
						</ul>

							<h4>Get Redressed for April-縫補 : 從衣櫃挑選一件已破損的衣物，給它多一點關愛</h4>
							<h4><a href="http://redress.com.hk/" target="_blank">（http://redress.com.hk/）</a></h4>
							<>365挑戰 / The 365 Challenge 2013

							「重新配搭，不要丟棄」/ Redress it, don’t bin it

							<p>Redress的創辦人Christina Dean從2013年起，只穿著被丟棄或捐贈所得的二手衣物，身體力行宣傳「重新配搭，不要丟棄」的理念。每一天，Christina 都會穿著從夥伴機構香港地球之友所得的二手衣物，並由12位時裝達人幫助搭配造型，展示365種充滿啟發性的造型，讓紡織廢料留在時裝圈內，而非堆填區。這個挑戰幫助消費者領悟到延長衣物壽命的祕訣，從而避免把仍然有用及時尚的服飾丟至堆填區。除此之外，這活動亦能提高大眾對龐大紡織廢料的意識，讓他們在購買衣物時以環保為前提。</p>

							<h4>為什麼要重新配搭？</h4>

							<p>今天，我們花在時裝上的消費比10年前增加了大約百分之六十，導致每年愈來愈多紡織廢料被送往堆填區。單在2011年，香港人每天平均丟棄至3個堆填區的衣物達217噸。在英國，每年送往堆填區的衣物達350,000噸，估計價值1億4千萬英鎊。</p>

							<p>不能被生物分解的棄置衣物對環境構成極沉重的負擔，不但為堆填區帶來龐大壓力，同時亦產生各種改變氣候的化學物質。然而，幾乎所有紡織品皆可循環再用。透過重新配搭、修補、重新構造等方式，衣物能創造出既別具創意，又時尚經濟的造型。衣物亦能透過循環再用、升級再造或降級回收獲得「新生命」，減低對原材料和堆填區的壓力。</p>

							<p>延長衣物壽命、選購二手服裝對環境有著正面的影響。舉例說，循環再用1公斤的二手衣物，相比購買新衣物能減省達3.6公斤的二氧化碳排放量、6000升水、0.3公斤的肥料及0.2公斤的殺蟲劑。 (哥本哈根大學，2008)</p>

							<h4>Get Redressed挑戰2014</h4>

							<p>Get Redressed挑戰是一項為期一年的消費者挑戰，於2014年1月至12月舉行，由世界各地人士穿著365套可持續服裝啟發人心。這項挑戰旨在教育消費者他們的服飾、衣物護理及衣櫃管理對環境的正面影響，並推廣「重新配搭，不要丟棄」的理念。</p>

							</p>他們會在全年涵蓋12個可持續時裝主題，包括「小黑裙」、潮換、DIY、潮流、洗滌護理、重新造型、修補及重新構造，為消費者提供意念及資訊延長衣物壽命。這些行動的影響甚廣；在英國，延長衣物平均壽命9個月可節省用於供應、洗滌及棄置衣物的資源，價值高達50億英鎊。(資料來源：Valuing our Clothes, WRAP 2012)<p>

							</p>GetMore現在一同加入，將理念推廣至台灣，為時尚與環境的永續平衡做努力。如有興趣想一同加入計劃或合作的朋友，也歡迎聯絡我們喔！</p>

							<p>（service@getmore.cc）</p>

							<p class="text-right">資料來源：Redress</p>
						<img src="/assets/images/info/redress02.png">
				</div>