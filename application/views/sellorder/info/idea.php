				<img class='banner-s' src="/assets/images/banner-s/about/idea.jpg">
				<div class='row'>
					<div class='col-254'>
						<div class='leftcol'>
						<p>
  							<a href='/sellorder/info/intro/'><button type="button" class="btn btn-primary btn-lg">GetMore簡介</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/energy/'><button type="button" class="btn btn-primary btn-lg">GetMore Energy</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/idea/'><button type="button" class="btn btn-primary btn-lg">GetMore理念</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/coper/'><button type="button" class="btn btn-primary btn-lg">合作夥伴</button></a>
						</p>
						</div>
						<hr>
						<div class='contactus'><p> Contact us <br> service@getmore.cc </p> </div>
					</div>
					<div class='col-726'>
						<div class='rightcol'>
							<h2>GetMore 理念</h2></br>
							<img class='banner-s' src="/assets/images/content/about/idea.jpg">
					
							<h3><font color='#000000'>穿二手衣的理由：一種更感性、更快樂的生活方式</font></h3></br>
							<p>Enjoy fashion of lifestyle at GetMore.</p>

							<h4><font color='#52AF7E'>裝扮個性，彰顯自我風格</font></h4></br>
	                        <p>二手衣是能展現個人風格與特色的生活方式。因為在逛二手衣的時候，你想的不是要買一件當季流行的洋裝，或是尋找時下必備不可或缺的款式。你想的是找一件自己喜歡、穿在身上能讓自己看起來很特別的衣服。不用再為追求流行，買了一堆不合適、不自在的衣服。</p></br>

							<h4><font color='#52AF7E'>不可替代、獨一無二</font></h4></br>
                            <p>逛二手衣有一種挖寶的樂趣，每件都是獨一無二、不同風格、不同年代，隨時都可能發現驚喜。從逛二手衣到買回家穿搭，讓生活有了不同的樂趣與體驗。</p></br>

							<h4><font color='#52AF7E'>時尚環保、環保時尚</font></h4></br>
							<p>穿二手衣不僅能擁有自己的時尚風格，對我們的環境更有正面的影響。從時尚的角度做環保，也用環保的方式打造時尚。</p></br>
							 
					</div>
				</div>
			</div>
