				<img class='banner-s' src="/assets/images/banner-s/buydetail.jpg">
				<div class='row' id='pagerow'>
					<div class='col-254'>
						<div class='leftcol'>	
						<p>
  							<a href='/sellorder/info/paynfeepolicy/'><button type="button" class="btn btn-primary btn-lg">付款與運費</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/deliverintro/'><button type="button" class="btn btn-primary btn-lg">配送方式</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/returnpolicy/'><button type="button" class="btn btn-primary btn-lg">關於退貨</button></a>
						</p>
						</div>
						<hr>
						<div class='contactus'><p> Contact us <br> service@getmore.cc </p> </div>
					</div>
					<div class='col-726'>
						<div class='rightcol'>
						<h2>關於退貨</h2></br>
						<p>「我們不喜歡衝動購買造成的輕易退貨」- 雖然妳可以放心的在7日鑑賞期內在家試穿。</p>
						<p>很多購物網站或是大型量販告訴我們，「先買回家，不喜歡再來退就好」，讓我們養成了「衝動購買」或是「過度消費」的習慣，刺激了消費，但卻有可能傷害環境，堆積如山的無用商品反而降低了生活品質。</p>
						<p>GetMore的理念是提供一個管道，讓好衣服有再次被愛的機會，讓賣家可以賺取合理利潤；也讓買家可用完美價格買到心愛好衣。我們無法也不會拒絕客人退貨，但GetMore的每一件衣服都是獨一無二的，我們在幫她尋找再次被愛的機會，因此希望每位買家下訂前，可以理性考慮過衣服的尺寸、版型是否適合自己，甚至是能否與自己現有的衣服做搭配。當然，這不是件容易的事情，但還是希望大家在GetMore上避免衝動購買，輕易退貨的消費習慣。畢竟，不輕言退貨也是節能減碳、愛護地球的方式之一，也是GetMore一直提倡的理念。</p>
						<p>承如上述，GetMore提供一個平台，希望賣家與買家雙贏，因此我們只在衣服成交後收取25%做為代售手續費，買家輕易就能退貨，但運費必須由GetMore負擔，坦白說是有些辛苦的。</p>
						<p>感謝您的支持，GetMore會持續努力為您找好衣服，網站功能也會持續改善，提供您更好的購物環境。</p></br>
						<hr>
						<h4>只退不換</h4></br>
						<p>GetMore上的每一件衣服都是獨一無二的，若您收到的衣服尺寸不合或不如預期，很抱歉我們也無法提供相同款式的衣服供您更換。</p></br>
						<h4>退貨方式</h4></br>
						<p>GetMore提供拆單退貨的服務（若您購買5件衣服中其中僅1件不合穿，只要退回不合穿的1件，其餘心愛的衣服都可以留下喔）。請於7日鑑賞期內填妥【<a href='http://dl.dropboxusercontent.com/u/33336146/return_apply.doc'>退貨申請單</a>】，email(service@getmore.cc)通知我們，同時將要退貨的衣物連同購物發票完整包裝並妥善密封，物流士將於您提出申請後3個工作日內到府收件，退貨運費將由GetMore為您負擔（若退貨後訂單金額未達免運標準，則將加計公告運費）。
						請注意，鑑賞期非試用期，收到衣服後若擔心不適合，請先“在家”正常試穿（請留意臉上的粉妝不要沾到衣物上），並請不要拆下洗衣標與GetMore吊牌，更請不要穿到室外或穿著用餐。若正常試穿後仍決定退貨，請保留衣服原包裝與所有配件，連同購物發票與退貨申請單一併包好交給物流士，GetMore感謝您的配合！</p></br>
						<h4>退款流程</h4></br>
						<p>依據您的付款方式，GetMore的退款流程如下：</p></br>
						<img src="/assets/images/content/return/return.jpg">
						</div>
					</div>
				</div>