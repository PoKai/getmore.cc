				<img class='banner-s' src="/assets/images/banner-s/buyintro.jpg">
				<div class='row' id='pagerow'>
					<div class='col-254'>
						<div class='leftcol'>
						<p>
  							<a href='/sellorder/info/sellintro/'><button type="button" class="btn btn-primary btn-lg">我要賣衣服</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/pricing/'><button type="button" class="btn btn-primary btn-lg">費用說明</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/buyintro/'><button type="button" class="btn btn-primary btn-lg">我要買衣服</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/washintro/'><button type="button" class="btn btn-primary btn-lg">保證嚴格清洗</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/userpolicy/'><button type="button" class="btn btn-primary btn-lg">使用條款</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/sellpolicy/'><button type="button" class="btn btn-primary btn-lg">賣衣服細節</button></a>
						</p>
						</div>
						<hr>
						<div class='contactus'><p> Contact us <br> service@getmore.cc </p> </div>
					</div>
					<div class='col-726'>
						<div class='rightcol'>
						<h2>我要買衣服</h2></br>
						<h4>購買流程</h4></br>
						<img src="/assets/images/content/buy/shopping.jpg">
						<h4>尺寸表參考圖</h4></br>
						<div  class='col-md-4'>
							<a data-toggle="modal" href="#modalwomen"><img src="/assets/images/content/buy/sizeicon-women.png"></a>
						</div>
						<div  class='col-md-4'>
							<a data-toggle="modal" href="#modalmen"><img src="/assets/images/content/buy/sizeicon-men.png"></a>
						</div>
						<div  class='col-md-4'>
							<a data-toggle="modal" href="#modalkids"><img src="/assets/images/content/buy/sizeicon-kids.png"></a>
						</div>
						</div>
					</div>
				</div>