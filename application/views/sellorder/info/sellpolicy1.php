				<img class='banner-s' src="/assets/images/banner-s/sellintro.jpg">
				<div class='row' id='pagerow'>
					<div class='col-254'>
						<div class='leftcol'>
						<p>
  							<a href='/sellorder/info/sellintro/'><button type="button" class="btn btn-primary btn-lg">我要賣衣服</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/pricing/'><button type="button" class="btn btn-primary btn-lg">費用說明</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/buyintro/'><button type="button" class="btn btn-primary btn-lg">我要買衣服</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/washintro/'><button type="button" class="btn btn-primary btn-lg">保證嚴格清洗</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/userpolicy/'><button type="button" class="btn btn-primary btn-lg">使用條款</button></a>
						</p>
						<p>
  							<a href='/sellorder/info/sellpolicy/'><button type="button" class="btn btn-primary btn-lg">賣衣服細節</button></a>
						</p>
						</div>
						<hr>
						<div class='contactus'><p> Contact us <br> service@getmore.cc </p> </div>
					</div>
					<div class='col-726'>
						<div class='rightcol'>
						<h2>賣衣服細節</h2></br>
						<h4>賣衣服流程</h4></br>
					<div class="sell-process">	
						<div class="row">
							<div class="row-img"><img src="/assets/images/content/sell/sellstep-01.png"></div>
							<div class="row-text-right">
							<h4>整理</h4><p>衣櫃裡總是有些衣服，買了卻很少穿或根本沒穿過，不穿浪費，但丟了又可惜，因而令你尷尬嗎？現在開始，
							把這些衣服整理出來，交給GetMore，讓GetMore幫你增加收入。
							</p>
							</div>
						</div>
						
						<div class="row">
							<div class="row-img"><img src="/assets/images/content/sell/sellstep-02.png"></div>
							<div class="row-text-right">
								<h4>洗衣</h4><p>為保證品質，在GetMore 的每一件衣服，都是必須經過洗衣公司嚴格清洗整新的，即使是全新的衣服也是。當您準備好想要賣的衣服，系統完成線上流程後，將會有物流士到您的指定地點收送，經過檢查，即進入洗衣的流程。
								</p>
							</div>
						</div>

						<div class="row"><div class="row-img"><img src="/assets/images/content/sell/sellstep-03.png"></div>
							<div class="row-text-right">
								<h4>上架</h4><p>洗完的衣服，會進行上架流程，我們擁有先進的攝影上架系統，且由專人丈量衣服尺寸，透過這個流程，開始在 GetMore 的網站販售！
								<br><p>消費者可以輕鬆地在 GetMore 的網站上購物，自由挑選。</p>
								</p>
							</div>
						</div>

						<div class="row"><div class="row-img"><img src="/assets/images/content/sell/sellstep-04.png"></div>
							<div class="row-text-right">
								<h4>入帳</h4><p>販售的款項，扣除相關費用後，我們將在每個月匯入您在GetMore的帳戶，可以拿來買更多喜歡的衣服，或是申請領回。
								</p>
							</div>
						</div>
					</div>

						<h4>清洗費用與運費</h4></br>
						<img src="/assets/images/content/sell/wash.jpg">
						<img src="/assets/images/content/sell/refuse.jpg">
						<h4>上架及代售</h4></br>
						<ul>
						<li>上架程序：經過清洗後的衣服，進入上架以及代售程序。</li>
						<li>售出手續費：在 GetMore，代售手續費用為代售申請時填入售價之25%(5%為營業稅)，售出後直接扣除佣金，若約定期間內無售出，則不收取手續費用。
						</li>
						<li>商品理賠：GetMore於洗衣及代售保管期間（收到衣服到銷售至消費者此段期間），若有毁損或遺失，依照洗衣定型化契約進行理賠。
						</li>
						<li>銷售發票：二手衣銷售依國稅局規定由GetMore統一開立發票。並且依照賣家二手衣銷售所得，寄發「個人一時貿易資料申報表」。
						</li>
						<li>
						結帳方式： 二手衣銷售所得扣除佣金後為賣家所得， 將匯入賣家會員中心的「我的帳戶」。如個人戶欲匯出現金至銀行帳戶， 每筆匯款需扣除手續費15元；公司戶匯至公司銀行帳戶除須扣除15元手續費外，需先依請款金額開立發票或收據(免用 統一發票收據)給咩斯特數位整合有限公司。
						</li>
						<li>
						委售期限：從 GetMore 將賣家的商品上架起，6個月內委由 GetMore 進行販售，除網站外，包含不定期二手市集。委售期滿GetMore將依照賣家當時填入之期滿送回資料宅配送回，不收運費，若當時選擇不送回，我們將代為捐贈給需要的團體。(
						如委售期未滿的狀況下申請領回，運費須自付)
						</li>
						</ul>
						<img src="/assets/images/content/sell/getmoreheart.jpg">
						<div class='text-center'>
							<div class="sellstart"><p class="text-center"><a href="http://bit.ly/190TqRp"><img src="/assets/images/ad/sellstart.png"></a></p></div>
						</div>
						</div>
					</div>
				</div>
