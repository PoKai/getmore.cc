<img class='banner-s' src="/assets/images/banner-s/sellorder.jpg">
<div class='row' id='pagerow'>
		<div class='col-md-3'>
			<div class="leftcol">
				<img src="/assets/images/sellorder/sprocess-01.png">
			</div>
		</div>

		<div class='col-md-9'>
			<h4><img id='clothe_icon' src="/assets/images/secondhand_management/picture_clothes.png" alt="" /><span id="selling">開始賣衣服</span></h4>
			<hr class="bold_hr" />
			<h3>請輸入二手衣資訊</h3>
				
			<form id="step-1" action="/sellorder/steps/2" method="POST" role="form">
				<div class="form-group col-md-7">
          <label for="product_desc">商品敘述</label>
					<input type="text" class="form-control" id="product_desc" maxlength="10">
					<p class='description'>限定10字元以內。ex:ZARA 紅色夏季襯衫</p>
				</div>
				<div class="col-md-10">
					<input type="hidden" value="" id="product_type">
						<div class="col-md-4 level_1">
						</div>
						<div class="col-md-4 level_2">
						</div>
						<div class="col-md-4 level_3">
						</div>
				</div>
        <input type="hidden" id="shipping_price">
        <div class="form-group col-md-7">
          <label for="product_brand">品牌</label>
					<input type="text" class="form-control" id="product_brand" maxlength='30'>
          <p class='description'>請確實輸入品牌,GetMore不收假貨喔!</p>
        </div>
        <div class="form-group col-md-7">
          <label for="">參考原價</label>
		  <select class="form-control" id="original_price">
            <option value="1000">500元~1,000元</option>
            <option value="2000">1,001元~2,000元</option>
            <option value="3000">2,001元~3,000元</option>
            <option value="4000">3,001元~4,000元</option>
            <option value="5000">4,001元~5,000元</option>
            <option value="6000">5,001元~6,000元</option>
            <option value="7000">6,001元~7,000元</option>
            <option value="8000">7,001元~8,000元</option>
            <option value="9000">8,001元~9,000元</option>
            <option value="10000">9,001元~10,000元</option>
            <option value="99999">超過10,000元</option>
          </select>
          <p class='description'>請填入當時購買的大約價格,原價必須大於$500元喔!</p>
        </div>
        <div class="form-group col-md-7">
          <label for="sell_price">二手販售價格</label>
          <input type="text" class="form-control" id="sell_price" maxlength='5' onkeyup="this.value=this.value.replace(/[^\d]/,'');">
          <p class='description'>想要賣出的價格,建議為原價3~5折</p>
        </div>
        <div class="form-group col-md-7">
          <label>期滿送回資訊</label>
          <div class="radio">
            <label><input type="radio" value="1" name="donative">期滿後若未售出請幫我捐出，不用還我喔！</label>
          </div>

          <div class="radio">
            <label><input type="radio" value="0" name="donative" checked="checked">請寄還給我。</label>
          </div>
          <p class='description'>下一步可輸入送回地址；</br>若選擇捐出 GetMore 將於期滿後協助您送出愛心！</p>
        </div>
        <div class="form-group col-md-7">
          <label for="note">備註</label>
          <input type="text" class="form-control" id="product_note" maxlength='20'>
          <p class='description'>您認為需要備註的事項;限定20字元以內。ex:全新沒拆吊牌。</p>
        </div>
        <div id='buttons' class="col-md-7">
          <button type="button" class="btn btn-info add-sellcart">新增一筆</button>
          <button type="button" class="test-sellcart hidden">TEST</button>
        </div>
			</form>		
		

		<table class='table table-single-order'>
			<thead>
					<tr>
						<td>商品描述</td>
						<td>服飾 / 類別</td>
						<td>品牌</td>
						<td>原價 / 二手售價</td>
						<td>期滿</td>
						<td>備註</td>
						<td>修改</td>
						<td>刪除</td>
					</tr>
			</thead>
				<tbody>
				</tbody>
		</table>

		<hr class="dashed_hr"/>
    <div><a class='marginright5 marginleft15 getmoregreen' data-toggle="modal" href="#sellpricemodal">費用說明</a> <a class='marginright5 getmoregreen' data-toggle="modal" href="#sellnoticemodal">注意事項</a></div>
		<div id="info" class='col-md-8'>

				<div class="row">
					<div class="col-md-12">
						總件數：<span id="all_num_value"></span>
					</div>
					<div class="col-md-4" id='small_num'>
						<span id="small_num_value"></span> 小衣
					</div>
					<div class="col-md-4" id='big_num'>
						<span id="big_num_value"></span> 大衣
					</div>
					<div class="col-md-4" id='fly_num'>
						<span id="fly_num_value"></span> 羽絨衣
					</div>
				</div>

				<hr />
				<div class="row">
					<div class="col-md-10">
						洗衣費小計
					</div>
					<div class="col-md-2" id='washing_fee'>

					</div>
				</div>
				<div class="row">
					<div class="col-md-10">
						運費小計<font class='font-size12 getmoregreen'>  (洗衣費$600以上免運費)</font>
					</div>
					<div class="col-md-2" id='transport_fee'>

					</div>
				</div>
				<div class="row hidden">
					<div class="col-md-10">
						帳戶餘額
					</div>
					<div class="col-md-2" id='account_balance'>

					</div>
				</div>
				<div class="row hidden">
					<div class="col-md-10">
						本次扣抵
					</div>
					<div class="col-md-2" id='discount'>

					</div>
				</div>
				<hr />
				<div class="row">
					<div class="col-md-10">
						總計
					</div>
					<div class="col-md-2" id='total_fee'>
					<?php echo $total_shipping_price; ?>
					</div>
				</div>
		</div>
			<div class='col-md-8'><a href="#" class="btn btn-info" data-form="step-1" id="step2">下一步</a> 
			</div>
		</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">修改商品</h4>
      </div>
      <div class="modal-body">                
        <input type="hidden" id="edit_product_id" />
				<div class="form-group col-md-7">
          		<label for="edit_product_desc">商品敘述</label>
					<input type="text" class="form-control" id="edit_product_desc" maxlength="10">
					<p class='description'>限定10字元以內。ex:ZARA 紅色夏季襯衫</p>
				</div>        

				<div class="col-md-10">
<!--					<input type="hidden" value="" id="product_type">-->
          <div class="col-md-4 level_1">
          </div>
          <div class="col-md-4 level_2">
          </div>
          <div class="col-md-4 level_3">
          </div>
				</div>
        
        <div class="form-group col-md-7">
          <label for="edit_product_brand">品牌</label>
					<input type="text" class="form-control" id="edit_product_brand" maxlength='30'>
          <p class='description'>請確實輸入品牌,GetMore不收假貨喔!</p>
        </div>
                
        <div class="form-group col-md-7">
          <label for="edit_original_price">參考原價</label>
		  <select class="form-control" id="edit_original_price">
            <option value="1000">500元~1,000元</option>
            <option value="2000">1,001元~2,000元</option>
            <option value="3000">2,001元~3,000元</option>
            <option value="4000">3,001元~4,000元</option>
            <option value="5000">4,001元~5,000元</option>
            <option value="6000">5,001元~6,000元</option>
            <option value="7000">6,001元~7,000元</option>
            <option value="8000">7,001元~8,000元</option>
            <option value="9000">8,001元~9,000元</option>
            <option value="10000">9,001元~10,000元</option>
            <option value="99999">超過10,000元</option>
          </select>
          <p class='description'>請填入當時購買的大約價格,原價必須大於$500元喔!</p>
        </div>                
                
                
        <div class="form-group col-md-7">
          <label for="edit_sell_price">二手販售價格</label>
          <input type="text" class="form-control" id="edit_sell_price" maxlength='5' onkeyup="this.value=this.value.replace(/[^\d]/,'');">
          <p class='description'>想要賣出的價格,建議為原價3~5折</p>
        </div>
        <div class="form-group col-md-8">
          <label>期滿送回資訊</label>
          <div class="radio">
            <label><input type="radio" value="1" name="edit_donative">期滿後若未售出請幫我捐出，不用還我喔！</label>
          </div>
          <div class="radio">
            <label><input type="radio" value="0" name="edit_donative" checked="checked">請寄還給我。</label>
          </div>
          <p class='description'>下一步可輸入送回地址；</br>若選擇捐出 GetMore 將於期滿後協助您送出愛心！</p>
        </div>        
        <div class="form-group col-md-9">
          <label for="edit_product_note">備註</label>
          <input type="text" class="form-control" id="edit_product_note" maxlength='20'>
          <p class='description'>您認為需要備註的事項;限定20字元以內。ex:全新沒拆吊牌。</p>
        </div>
       <div style='clear: both'></div>
      </div><!-- /.modal-body -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-info save-sellcart">確認</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

       <div class="modal fade" id='sellpricemodal'>
          <div class="modal-dialog-800">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">清洗費用與運費</h4>
              </div>
              <div class="modal-body">
                <div class="modal-center">
                <img src="/assets/images/content/buy/presell-02.png">
                </div>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id='sellnoticemodal'>
          <div class="modal-dialog-800">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">注意事項</h4>
              </div>
              <div class="modal-body">
                <div class="modal-center">
                <img src="/assets/images/content/buy/presell-03.png">
                </div>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<!-- Facebook Conversion Code for 開始賣衣服 -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6020121732294', {'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6020121732294&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>