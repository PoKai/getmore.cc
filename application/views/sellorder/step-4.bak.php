<div class="content order-page">
    <div class="nav">二手衣販售管理</div>
    <div class="welcome checkout">
        歡迎來到&nbsp;&nbsp;<img src="/assets/images/logo_s.png" />&nbsp;&nbsp;結帳櫃台，很高興能為您服務！
    </div>
    <div class="all-steps">
        <div class="steps"><div class="single-step">輸入<br />衣服資訊</div></div>
        <div class="steps"><div class="single-step">輸入<br />取件資訊</div></div>
        <div class="steps"><div class="single-step">線上<br />洗衣付費</div></div>
        <div class="steps active"><div class="single-step">列印<br />標籤</div></div>
    </div>
    <div class="steps">
        <p>恭喜您完成二手衣上架申請，<br />
        請點選列印按鈕，列印申請單後，貼於外箱。<br />
        若不方便列印，也可將下列所有資訊抄下，貼於外箱。<br />
        請務必填寫申請單！宅配人員將會至您的取貨地點收貨。</p>
        <a id="prints" href="#" onclick="window.open('/sellorder/prints/<?php echo $ordernumber; ?>');" class="back">列印申請單</a>
        <a href="/order/single/<?php echo $ordernumber; ?>" class="back">訂單資料</a>
    </div>
</div>

<div class="order-note">
    <div class="container">
        <div class="right">
            <ul>
                <li>● getmore 金流採用藍新科技解決方案，為台灣目前金流第一品牌。</li>
                <li>● 信用卡刷卡採用藍新科技信賴付款機制，受『128-bit SSL』加密保護。</li>
                <li>● WEBATM 採用第一銀行金流服務，請先準備好您的金融卡及讀卡機。</li>
                <li>● ATM 轉帳付款採用第一銀行虛擬帳號，系統將自動為您辦理入帳手續，您可不需再傳真確認。</li>
                <li>● 如果您有任何疑問，請email至lab@getmore.cc。</li>
            </ul>
        </div>
        <div class="left">
            <h3>付費須知</h3>
        </div>
        <div class="clear"></div>
    </div>
</div>
