<?php 
    function barcode($bartext = '') {
        $test = Zend_Barcode::draw('code39', 'image', array('text' => $bartext), array());
        imagejpeg($test, './assets/images/barcode/'.$bartext.'.jpg', 100);
    }
    barcode($order['order_number']);
?>
<style>
body {visibility:hidden;}
.single {visibility:visible;}
</style>
<div class="content order-page">
    <div class="container" style="width:100%;">
        <div class="right order" style="width:100%; margin: 0; padding: 0; background-color:transparent;">
            <div class="single">
                <h3>貨單編號: <?php echo $order['order_number']; ?></h3>
                <div class="detail">
                    <table>
                        <tbody>
                            <tr>
                                <td>寄件人地址&nbsp;:&nbsp;<?php echo $order['user_address']; ?></td>
                                <th rowspan=4><!--<img id="barcode" src="/assets/images/barcode/<?php echo $order['order_number']; ?>.jpg" />--></th>
                            </tr>
                            <tr>
                                <td>寄件人&nbsp;:&nbsp;<?php echo $order['user_name']; ?></td>
                            </tr>
                            <tr>
                                <td><br /></td>
                            </tr>
                            <tr>
                                <td>到貨地址&nbsp;:&nbsp;106 台北市大安區新生南路一段139巷15號B1</td>
                            </tr>
                            <tr>
                                <td>收件人&nbsp;:&nbsp;GetMore二次時尚</td>
                            </tr>
                            <tr>
                                <td><br /></td>
                            </tr>
                            <tr>
                                <td>貨單編號&nbsp;:&nbsp;<?php echo $order['order_number']; ?></td>
                            </tr>  
                            <tr>
                                <td>託運編號&nbsp;:&nbsp;<?php echo $order['order_number']; ?></td>
                            </tr>
                            <tr>
                                <td><br /></td>
                            </tr>
                            <tr>
                                <td>內容物：小衣 <?php echo $count_small; ?> 件/ 大衣<?php echo $count_medium; ?>件/ 羽絨衣 <?php echo $count_big; ?> 件 </td>
                            </tr>
                            <tr>
                                <td><p style="font-size:10px;width:400px;">※ 請注意，委託派件是透過 GetMore 通知黑貓宅急便至您府上取件，將由黑貓宅急便物流士直接以電話或是按電鈴通知您，您可直接將已經包裝好商品交給物流士，或是與物流士另外約定取貨時間。請務必保持手機暢通，在取件過程中，GetMore 無法追蹤到您與宅急便物流士聯繫狀況，若未接到物流士來電，或是無法完成取件的任何狀況，請來信 GetMore客服 service@getmore.cc，我們將協助您完成收件。</p>
</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <br />
                <a id="prints" href="#" onclick="window.print();" class="btn btn-large btn-primary">列印</a>
            </div>
        </div>
        <!--
        <div class="left">
            <ul>
                <li class="menu">會員中心</li>
                <li>
                    <ul>
                        <li class="sub-menu"><a href="/auth/edit">會員資料</a></li>
                        <li class="sub-menu"><a href="/order/lists" class="active">訂單查詢</a></li>
                    </ul>
                </li>
                <li class="menu">聯絡我們</li>
            </ul>
        </div>
        <p class="clear"></p>
-->
    </div>
</div>
