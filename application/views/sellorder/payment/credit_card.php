<form action="<?php echo $payment_url; ?>" method="POST" id="payment">
    <input type=hidden name="MerchantNumber" value="<?php echo $MerchantNumber; ?>">
    <input type=hidden name="OrderNumber"    value="<?php echo $OrderNumber; ?>">
    <input type=hidden name="Amount"         value="<?php echo $Amount; ?>">
    <input type=hidden name="OrgOrderNumber" value="<?php echo $OrgOrderNumber; ?>">
    <input type=hidden name="ApproveFlag"    value="<?php echo $ApproveFlag; ?>">
    <input type=hidden name="DepositFlag"    value="<?php echo $DepositFlag; ?>">
    <input type=hidden name="Englishmode"    value="<?php echo $Englishmode; ?>">
    <input type=hidden name="iphonepage"     value="<?php echo $iphonepage; ?>">
    <input type=hidden name="OrderURL"       value="<?php echo $OrderURL; ?>">
    <input type=hidden name="ReturnURL"      value="<?php echo $ReturnURL; ?>">
    <input type=hidden name="checksum"       value="<?php echo $checksum; ?>">
    <input type=hidden name="op"             value="<?php echo $op; ?>">
</form>
<script>
    document.getElementById("payment").submit();
</script>
