<div class="content order-page">
    <div class="nav">二手衣販售管理</div>
    <div class="welcome checkout">
        歡迎來到&nbsp;&nbsp;<img src="/assets/images/logo_s.png" />&nbsp;&nbsp;二手衣販售管理，很高興能為您服務！
    </div>
    <div class="all-steps">
        <div class="steps active"><div class="single-step">輸入<br />衣服資訊</div></div>
        <div class="steps"><div class="single-step">輸入<br />取件資訊</div></div>
        <div class="steps"><div class="single-step">線上<br />洗衣付費</div></div>
        <div class="steps"><div class="single-step">列印<br />標籤</div></div>
    </div>
    <form id="step-1" action="/sellorder/steps/2" method="POST">
    <div class="information">
        <?php if (isset($message) and !empty($message)): ?>
        <?php echo $message; ?>
        <?php endif; ?>
        <div class="sell_info">
            <h3>請輸入二手衣資訊</h3>
            <label>商品敘述</label>
            <input id="product_desc" type="text" value="" placeholder="" />&nbsp;&nbsp;ex: ZARA紅色夏季襯衫
            <label>類別</label>
            <input id="product_type" type="text" value="" placeholder="" />&nbsp;&nbsp;選一個類別，詳細參考<a href="#">類別表</a>
            <label>品牌</label>
            <input id="product_brand" type="text" value="" placeholder="" />&nbsp;&nbsp;請確實輸入品牌，GetMore不收假貨喔！
            <label>原價</label>
            <input id="original_price" type="text" value="" placeholder="" />&nbsp;&nbsp;原價
            <label>二手售價</label>
            <input id="sell_price" type="text" value="" placeholder="" />&nbsp;&nbsp;想要邁出的價格，建議為原價3-5折
            <label>衣物大小</label>
            <ul>
                <li>小衣75元／件（T恤、POLO衫、上衣、襯衫、領巾、套頭毛衣、裙子、褲子）</li>
                <li>大衣120元／件（西裝、外套、洋裝、夾克、前開襟毛衣、大衣、連身服）</li>
                <li>羽絨衣150元／件（羽絨衣：一般羽絨衣、羽絨背心）</li>
            </ul>
            <select id="shipping_price" name="shipping_price">
                <option value="<?php echo $shipping_price_small; ?>" selected="selected">小衣</option>
                <option value="<?php echo $shipping_price_medium; ?>">大衣</option>
                <option value="<?php echo $shipping_price_big; ?>">羽絨衣</option>
            </select>
            <label>備註</label>
            <textarea id="product_note"></textarea>
        </div>
        <a href="#" class="btn btn-danger inline add-sellcart">新增一筆</a>
        <div class="detail">
            <table class="table table-bordered table-single-order">
                <thead>
                    <tr>
                        <th>您的二手衣販售清單</th>
                    </tr>
                    <tr>
                        <th>商品敘述</th>
                        <th>類別</th>
                        <th>品牌</th>
                        <th>參考原價</th>
                        <th>二手售價</th>
                        <th>洗衣費</th>
                        <th width="160">備註</th>
                        <th>刪除</th>
                    </tr>
                </thead>
        <?php if(!empty($products)): ?>
                <tbody class="product_body">
                <?php
                    foreach($products as $row):
                ?>
                    <tr>
                        <td><span class="title"><?php echo $row['title']; ?></span></td>
                        <td><span class="product_type"><?php echo $row['product_type']; ?></span></td>
                        <td><span class="product_brand"><?php echo $row['product_brand']; ?></span></td>
                        <td>$<span class="original_price"><?php echo $row['original_price']; ?></span></td>
                        <td>$<span class="sell_price"><?php echo $row['sell_price']; ?></span></td>
                        <td>$<span class="shipping_price"><?php echo $row['shipping_price']; ?></span></td>
                        <td><span class="product_note"><?php echo $row['product_note']; ?></span></td>
                        <td><button class="btn btn-danger inline delete-sellcart" data-update="1" data-id="<?php echo $row['id']?>" data-type="product">移除商品</button></td>
                    </tr>
                <?php
                    endforeach;
                ?>
                </tbody>
        <?php endif; ?>
            </table>
        </div>
        <div class="price">
            <div class="title product">洗衣費總金額</div>
            <div class="total-price">
                <p id="total_shipping_price"><?php echo $total_shipping_price; ?></p>
            </div>
            <div class="total-price">
                <p></p>
            </div>
            <div class="clear"></div>
        </div>
        <div class="price left">
            <a href="/subject/lists/all" class="keep_product">繼續購物</a><a href="/event/lists/all" class="keep_event last-child">繼續活動</a>
        </div>
        <div class="price right">
            <div class="title total">總計</div>
            <div class="total-price">
                <?php if(!empty($total_product_price)): ?><p class="product_total_price"><?php echo $total_product_price; ?></p><?php endif;?>
                <?php if(!empty($total_event_price)): ?><p class="event_total_price"><?php echo $total_event_price; ?></p><?php endif;?>
                <p class="transportation_charge"><?php echo $transportation_charge; ?></p>
                <p class="yellow" id="total_price"><?php echo $total_price; ?></p>
            </div>
            <div class="total-price">
                <?php if(!empty($total_product_price)): ?><p class="yellow">商品總計</p><?php endif;?>
                <?php if(!empty($total_event_price)): ?><p class="yellow">活動總計</p><?php endif;?>
                <p class="yellow">運費總計</p>
                <p class="yellow">總計</p>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="steps">
        <a href="/" class="back">離開交易</a>
        <a href="#" class="next" data-form="step-1" id="step2">下一步</a>
    </div>
    <!-- <input type="hidden" name="payment_method" value="credit-cart"> -->
    </form>
</div>

<div class="order-note">
    <div class="container">
        <div class="right">
            <ul>
                <li>● getmore 金流採用藍新科技解決方案，為台灣目前金流第一品牌。</li>
                <li>● 信用卡刷卡採用藍新科技信賴付款機制，受『128-bit SSL』加密保護。</li>
                <li>● WEBATM 採用第一銀行金流服務，請先準備好您的金融卡及讀卡機。</li>
                <li>● ATM 轉帳付款採用第一銀行虛擬帳號，系統將自動為您辦理入帳手續，您可不需再傳真確認。</li>
                <li>● 如果您有任何疑問，請email至lab@getmore.cc。</li>
            </ul>
        </div>
        <div class="left">
            <h3>付費須知</h3>
        </div>
        <div class="clear"></div>
    </div>
</div>
