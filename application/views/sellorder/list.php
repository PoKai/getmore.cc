<div class="content order-page">
    <div class="nav">首頁 &#8250; 會員中心</div>
    <div class="welcome">
        歡迎來到&nbsp;&nbsp;<img src="/assets/images/logo_s.png" />&nbsp;&nbsp;會員中心，很高興能為您服務！
    </div>
    <div class="container">
        <div class="right order">
            <?php if (isset($message) and !empty($message)): ?>
            <?php echo $message; ?>
            <?php endif; ?>
            <table class="table table-bordered table-order">
                <thead>
                    <tr>
                        <th>訂購日期</th>
                        <th>訂單編號</th>
                        <th>付款方式</th>
                        <th>款項總額</th>
                        <th>處理進度</th>
                        <th>貨單</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($outputs as $row): ?>
                    <tr>
                        <td><?php echo $row['date_purchased']; ?></td>
                        <td><?php echo $row['order_number']; ?></td>
                        <td><?php echo $row['payment_method']; ?></td>
                        <td><?php echo $row['total_price']; ?></td>
                        <td><?php echo $row['status']; ?></td>
                        <td>
                            <a class="btn btn-primary" style="color: white;" href="/sellorder/prints/<?php echo $row['order_number']; ?>"><i class="icon-ok icon-white"></i> 檢視貨單</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="left">
            <ul>
                <li class="menu">會員中心</li>
                <li>
                    <ul>
                        <li class="sub-menu"><a href="/auth/edit">會員資料</a></li>
                        <li class="sub-menu"><a href="/order/lists">訂單查詢</a></li>
                        <li class="sub-menu"><a href="/sellorder/lists" class="active">二手衣販售管理</a></li>
                    </ul>
                </li>
                <li class="menu">聯絡我們</li>
            </ul>
        </div>
        <p class="clear"></p>
    </div>
</div>
