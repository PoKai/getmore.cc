<img class='banner-s' src="/assets/images/banner-s/sellorder.jpg">
	<div class='row' id='pagerow'>
		<div class='col-md-3'>
			<div class="leftcol">
				<img src="/assets/images/sellorder/sprocess-02.png">
			</div>
		</div>

		<div class='col-md-9'>
			<h4><img id='clothe_icon' src="/assets/images/secondhand_management/picture_clothes.png" alt="" /><span id="selling">開始賣衣服</span> </h4>
			<hr class="bold_hr" />
			<h3>請輸入取件資訊</h3>

						<form id="step-2" action="/sellorder/steps/3" method="POST" role="form">
							<input type="hidden" name="usercounty" id="usercounty">
							<input type="hidden" name="userdistrict" id="userdistrict">
							<input type="hidden" name="userzipcode" id="userzipcode">
							<input type="hidden" name="useraddress" id="useraddress">
							<input type="hidden" name="deliverycounty" id="deliverycounty">
							<input type="hidden" name="deliverydistrict" id="deliverydistrict">
							<input type="hidden" name="deliveryzipcode" id="deliveryzipcode">
							<input type="hidden" name="deliveryaddress" id="deliveryaddress">
							<div class="form-group col-md-7">
								<label for="name">姓名</label>
								<input type="text" class="form-control" id="name" name="user_name" value="<?php if(empty($profile['user_name'])) echo $user_name; else echo $profile['user_name']; ?>">
								<p class='description'>ex:王小咩</p>
							</div>
							<div class="form-group col-md-7">
								<label for="address">取件地址</label><br>
			<?php
			if(empty($user_address)){
			    $user_zipcode=$profile['p_address']['zipcode'];
			    $user_addr=$profile['p_address']['address'];
			}else{
			    list($user_zipcode, $user_addr)=explode(' ',$user_address,2);
			    $user_addr=explode(' ',$user_addr,2)[1];
			}
			if(!empty($delivery_address)){
			    list($delivery_zipcode, $delivery_addr)=explode(' ',$delivery_address,2);
			    $delivery_addr=explode(' ',$delivery_addr,2)[1];
			}
			?>
		    <input type="hidden" id="obj_zipcode" value="<?php echo $user_zipcode;  ?>">
		    <input type="hidden" id="obj_address" value="<?php echo $user_addr; ?>">
		    <input type="hidden" id="dev_zipcode" value="<?php echo $delivery_zipcode;  ?>">
		    <input type="hidden" id="dev_address" value="<?php echo $delivery_addr; ?>">
								<div id="twzipcode"></div>
							</div>
							<div class="form-group col-md-7">
								<label for="phone">聯絡電話</label>
								<input type="text" class="form-control" id="phone" value="<?php if(empty($user_telephone)) echo $profile['telephone']; else echo $user_telephone ?>" name="user_telephone" />
								<p class='description'>請留下您的手機號碼,宅配人員將會與您聯繫。</p>
							</div>
							<div class="form-group col-md-7">
								<label for="">發票種類</label><br>
								<select name="uniform_invoice_type" id="sel_invoice_type">
									<option value='0' <?php if($uniform_invoice_type==0){ ?>selected=""<?php } ?>>捐贈（捐贈單位：創世基金會）</option>
									<option value='1' <?php if($uniform_invoice_type==1){ ?>selected=""<?php } ?>>二聯式發票</option>
									<option value='2' <?php if($uniform_invoice_type==2){ ?>selected=""<?php } ?>>三聯發票</option>
								</select>
								<input type="text" name="uniform_invoice_number" id="uniform_invoice_number" value="<?php echo $uniform_invoice_number; ?>" placeholder='統一編號' <?php if($uniform_invoice_type<1){ ?>style="display:none;"<?php } ?> />
								<input type="text" name="uniform_invoice_title" id="uniform_invoice_title" value="<?php echo $uniform_invoice_title; ?>" placeholder='發票抬頭' <?php if($uniform_invoice_type<2){ ?>style="display:none;"<?php } ?> />
							</div>
						<div id="returning_info" class='col-md-11'>
							<h3>期滿送回資訊</h3>
							<hr />
							<p id='returning_description'>請填入期滿寄回地址；若您選擇捐出，GetMore將於期滿後協助您送出愛心！</p><br>
							<div>
								<label>
									<input name="no_return" type="radio" value="1" <?php if($no_return!=0){ ?>checked=""<?php } ?> />
<!--									期滿後，未售出之商品全數捐給慈善機構，不用還我！-->
                  我全部衣服都選擇了期滿後捐贈。
								</label>
							</div>
							<div>
								<label>
									<input type="radio" name="no_return" value="0" <?php if($no_return==0){ ?>checked=""<?php } ?> >
<!--									請寄還給我。-->
                  我選擇"寄回"的衣服請送到下列地址。
								</label>
							</div>
							<div>
								<label>
									<input type="checkbox" id="same_as" <?php if($no_return!=0){ ?>disabled=""<?php } ?> >
									同取件資訊
								</label>
							</div>
						</div>
							<div class="form-group col-md-7">
								<label for="name">姓名</label>
								<input type="text" class="form-control" name="delivery_name" value="<?php echo $delivery_name; ?>">
								<p class='description'>ex:王小咩</p>
							</div>
							<div class="form-group col-md-7">
								<label for="address" class='pokai-address'>送回地址</label><br>
								<div id="twzipcode2"></div>
							</div>
							<div class="form-group col-md-7">
								<label for="phone">聯絡電話</label>
								<input type="text" class="form-control" name="delivery_telephone" value="<?php echo $delivery_telephone; ?>" />
								<p class='description'>請留下您的手機號碼,宅配人員將會與您聯繫。</p>
							</div>
							<div class="form-group col-md-7">
								<label for="time">配送時間</label>
								<select name="time" id="">
									<option value='any'>不指定</option>
									<option value='12'>中午前</option>
									<option value='17'>12時～17時</option>
									<option value='20'>17時～20時</option>
								</select>
							</div>
						</form>
				<div id='buttons' class="col-md-7">
					<button type="button" onclick="location.href='/sellorder/steps/1';" class='btn btn-default'>上一步</button>
					<button type="button" data-form="step-2" id="step3" class="btn btn-info next">下一步</button>
				</div>
		</div>
	</div>
