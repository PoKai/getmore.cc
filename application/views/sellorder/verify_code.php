<img class='banner-s' src="/assets/images/banner-s/sellorder.jpg">
<div class='row' id='pagerow'>
		<div class='col-md-3'>
			<div class="leftcol">
				<img src="/assets/images/sellorder/sprocess-01.png">
			</div>
		</div>
					<div class='col-md-9'>
						<form id="verify_code" action="/sellorder/verify_code" method="POST" role="form">
							<h4><img id='key_icon' src="/assets/images/secondhand_management/bottom_key.png" alt="" />權限開通</h4>
							<hr class="bold_hr" />
						<div id='input_code'>
							請輸入您手機收到的驗證碼:<input type="text" name="verification_code" id="verification_code"/>
							<button data-form="verify_code" type="button" class="btn btn-info btn-xs" id="btnverify_code" >確認</button>
<span id="error"></span><!-- 認證失敗!您輸入的認證碼不正確,請重新輸入! -->
						</div>
						<input type="hidden" value="<?php echo $outputs['phone']; ?>" name="phone" id="phone">
						<input type="hidden" value="<?php echo $outputs['id']; ?>" name="id" id="id">
						<input type="hidden" value="<?php echo $outputs['verify_status']; ?>" id="stat">
						<div id='resend_code'>
							如果您沒有收到或遺失認證碼簡訊：<button type="button" class="btn btn-xs" >重新認證</button>
						</div>
						</form>
					</div>
</div>
<script type="text/javascript">
	var vstatus=document.getElementById('stat').value;
	if(vstatus=="yes"){
	alert('簡訊驗證成功!');
	document.location.href="/sellorder/steps/1"
	}	
</script>
