<div class="content order-page">
    <div class="nav">二手衣販售管理</div>
    <div class="welcome checkout">
        歡迎來到&nbsp;&nbsp;<img src="/assets/images/logo_s.png" />&nbsp;&nbsp;結帳櫃台，很高興能為您服務！
    </div>
    <div class="all-steps">
        <div class="steps"><div class="single-step">輸入<br />衣服資訊</div></div>
        <div class="steps"><div class="single-step">輸入<br />取件資訊</div></div>
        <div class="steps active"><div class="single-step">線上<br />洗衣付費</div></div>
        <div class="steps"><div class="single-step">列印<br />標籤</div></div>
    </div>
    <form id="step-3" action="/sellorder/steps/4" method="POST">
    <div class="information">
        <div class="price">
            <div class="title product">本次二手衣販售</div>
            <div class="total-price">
                <p class="yellow" id="total_price"><?php echo $total_price; ?></p>
            </div>
            <div class="total-price">
                <p class="yellow">總計</p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="information <?php echo (empty($events)) ? 'footer' : ''; ?>">
        <div class="detail">
        <table>
            <tr>
                <td>
                    <div class="header">取件資訊</div>
                    <label>取件人</label>
                    <?php
                        echo $this->input->get_post("user_name");
                    ?>
                    <label>手機號碼</label>
                    <?php
                        echo $this->input->get_post("user_mobile_phone");
                    ?>
                    <label>聯絡電話</label>
                    <?php
                        echo $this->input->get_post("user_telephone");
                    ?>
                    <label>取件地址</label>
                    <?php  
                        $user_address_json = array(
                            'county' => $this->input->post('user_county'),
                            'district' => $this->input->post('user_district'),
                            'zipcode' => $this->input->post('user_zipcode'),
                            'address' => $this->input->post('user_address')
                        );
                        echo $user_address_json['zipcode'].$user_address_json['county'].$user_address_json['district'].$user_address_json['address']; 
                    ?>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <div class="header">送回資訊&nbsp;(若期滿未賣出)</div>
                    <label>收件人</label>
                    <?php
                        echo $this->input->get_post("delivery_name");
                    ?>
                    <label>手機號碼</label>
                    <?php
                        echo $this->input->get_post("delivery_mobile_phone");
                    ?>
                    <label>聯絡電話</label>
                    <?php
                        echo $this->input->get_post("delivery_telephone");
                    ?>
                    <label>收件地址</label>
                    <?php  
                        $delivery_address_json = array(
                            'county' => $this->input->post('delivery_county'),
                            'district' => $this->input->post('delivery_district'),
                            'zipcode' => $this->input->post('delivery_zipcode'),
                            'address' => $this->input->post('delivery_address')
                        );  
                        echo $delivery_address_json['zipcode'].$delivery_address_json['county'].$delivery_address_json['district'].$delivery_address_json['address']; 
                    ?>
                </td>
            </tr>
        </table>
        </div>
    </div>
    <div class="information charge-type footer">
        <div class="charge-info">
            <ul>
                <li class="title">請選擇洗衣費付費方式</li>
                <li class="button payment active" data-type="credit-cart">信用卡</li>
                <li class="button payment" data-type="web-atm">WEB ATM</li>
                <li class="button payment" data-type="atm">ATM 轉帳</li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
    <div class="steps">
        <a href="/sellorder/steps/2" class="back">上一步</a>
        <a href="#" class="next" data-form="step-3" id="step4">確認付款</a>
        <input type="hidden" name="payment_method" value="credit-cart">
    </div>
    </form>
</div>

<div class="order-note">
    <div class="container">
        <div class="right">
            <ul>
                <li>● getmore 金流採用藍新科技解決方案，為台灣目前金流第一品牌。</li>
                <li>● 信用卡刷卡採用藍新科技信賴付款機制，受『128-bit SSL』加密保護。</li>
                <li>● WEBATM 採用第一銀行金流服務，請先準備好您的金融卡及讀卡機。</li>
                <li>● ATM 轉帳付款採用第一銀行虛擬帳號，系統將自動為您辦理入帳手續，您可不需再傳真確認。</li>
                <li>● 如果您有任何疑問，請email至lab@getmore.cc。</li>
            </ul>
        </div>
        <div class="left">
            <h3>付費須知</h3>
        </div>
        <div class="clear"></div>
    </div>
</div>
