<script src="/assets/javascript/libs/jquery/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/plugin/jquery.datetimepicker.css"/>

<img class='banner-s' src="/assets/images/banner-s/sellorder.jpg">
	<div class='row' id='pagerow'>
		<div class='col-md-3'>
			<div class="leftcol">
				<img src="/assets/images/sellorder/sprocess-03.png">
			</div>
		</div>	

					<div class='col-md-9'>
					        <form id="step-3" action="/sellorder/steps/4" method="POST">
						<input type="hidden" id="show" name="show" value="yes">
						<h4><img id='clothe_icon' src="/assets/images/secondhand_management/picture_clothes.png" alt="" /><span id="selling">開始賣衣服</span> / <span id="pending_list">待付款貨單狀態</span> / <span id="status">二手衣販售狀態</span></h4>
						<hr class="bold_hr" />
						<h3>本次二手衣販售</h3>
						
						<hr class="dashed_hr"/>
						<div id="info" class='col-md-8'>
							<div class="row">
								<div class="col-md-12">
									總件數：<span id="total_num_value"><?php echo $total_num; ?></span> 
								</div>
								<div class="col-md-4" id='small_num'>
									<span id="small_num_value"><?php echo $small_num; ?></span> 小衣
								</div>
								<div class="col-md-4" id='big_num'>
									<span id="big_num_value"><?php echo $big_num; ?></span> 大衣
								</div>
								<div class="col-md-4" id='small_num'>
									<span id="fly_num_value"><?php echo $fly_num; ?></span> 羽絨衣
								</div>
							</div>
							<hr />
							<div class="row">
								<div class="col-md-10">
									洗衣費小計
								</div>
								<div class="col-md-2" id='washing_fee'>
								<?php echo $wash_price; ?>
								</div>
							</div>
							<div class="row">
								<div class="col-md-10">
									運費小計
								</div>
								<div class="col-md-2" id='transport_fee'>
								<?php echo $tran_price; ?>
								</div>
							</div>
              
              <?php if ( $account->balance>0): ?>							
              <div class="row">
								<div class="col-md-10">
									帳戶餘額
								</div>
								<div class="col-md-2" id='account_balance'><?php echo $account->balance ?></div>
							</div>
							<div class="row">
								<div class="col-md-10">
									本次扣抵
								</div>
								<div class="col-md-2" id='discount'> 
                    <input style='width: 200px' type='text' name='deduction_amount' class='deduction_amount form-control' value='0' onkeypress='return event.charCode >= 48 && event.charCode <= 57' />                
                    <div class='btn btn-info' onclick=''>確定</div>
                </div>
							</div>
              <?php endif; ?>
							
              <hr />
							<div class="row">
								<div class="col-md-10">
									應付餘額
								</div>
								<div class="col-md-2" id='total_fee'><?php echo $total_all_price; ?></div>
							</div>
						</div>
            
            
            
						<div id='getting_info'>
							<hr class="dashed_hr"/>
							<h3>取件資訊</h3>
							<p>您的姓名：<span id="name"><?php echo $this->input->get_post("user_name"); ?></span></p>
							<p>取件地址：<span id="address"><?php echo $user_address; ?></span></p>
							<p>聯絡電話：<span id="phone"><?php echo $user_telephone; ?></span></p>
						</div>
						<div id="returning_info">
							<hr class='dashed_hr'/>
							<h3>送回資訊<?php if($no_return) echo '【我全部衣服都選擇了期滿後捐贈】'; ?></h3>
              <?php if(!$no_return){ ?>
							<p>您的姓名：<span id="name"><?php echo $this->input->get_post("delivery_name"); ?></span></p>
							<p>送回地址：<span id="address"><?php echo $delivery_address; ?></span></p>
							<p>聯絡電話：<span id="phone"></span><?php echo $delivery_telephone; ?></p>
              <?php } ?>
						</div>
						<div id="paying_method">
							<hr class='dashed_hr' />
							<h3>請選擇洗衣費付款方式</h3>
							<p id='ssl_description'>GetMore採用藍新科技信賴付款機制,受『128-bit SSL』加密保護,請您安心使用!</p>
							<div class="radio" class='col-md-6'>
								<label>
									<input type="radio" class='payment_method' name="payment_method" value="credit-cart" data-type="credit-cart">
									信用卡
								</label>
							</div>
							<div class="radio" class='col-md-6'>
								<label>
									<input type="radio" class='payment_method' name="payment_method" value="web-atm" data-type="web-atm">
									webATM
								</label>
							</div>

							<div class="radio" class='col-md-6'>
								<label>
									<input type="radio" class='payment_method' name="payment_method" value="atm" data-type="atm">
									ATM轉帳
								</label>
                <br />
                <div id='atm_fields'>
                    <label>匯款帳號後5碼：<input id='last_five_code' name='last_five_code' type='text' maxlength='5' onkeypress='return event.charCode >= 48 && event.charCode <= 57' /></label><br />
                    <label>預計匯款時間：<input type='text' id='datetimepicker' name='datetimepicker' /></label>
                </div>
							</div>

              
							<div class="radio" class='col-md-6' style='visibility: hidden; color: red;'>
								<label>
									<input type="radio" name="payment_method" id='dumb' value="dumb" data-type="dumb">
									dumb
								</label>
							</div>
						</div>
						<div id="uniform">
							<hr class='dashed_hr'/>
							<h3>發票資訊</h3>
              <?php if ($uniform_invoice_type==0)
                              echo "<h5>捐贈（捐贈單位：創世基金會）</h5>";
                          else if ($uniform_invoice_type==1)
                              echo "<h5>二聯式發票</h5>";
                          else if ($uniform_invoice_type==2)
                              echo "<h5>三聯發票</h5>";
              ?>              
              
              <p></p>
              <?php if ($uniform_invoice_type!=0){ ?>
							<p>統一編號：<span id="uniform_invoice_number"><?php echo $uniform_invoice_number; ?></span></p>
							<p>發票抬頭：<span id="uniform_invoice_title"><?php echo $uniform_invoice_title; ?></span></p>
              <?php } ?>
						</div>
						<hr class='dashed_hr' />
						<div id='buttons' class="col-md-7">
							<button type="button" class='btn btn-default' onclick="location.href='/sellorder/steps/2';">上一步</button>
							<button type="button" class="btn btn-info next" data-form="step-3" id="step4">確認付款</button>
						</div>
						</form>
					</div>
	</div>

<script>
    $(document).ready(function(){
        $('#datetimepicker').datetimepicker();
      
          var total_price = $('#total_fee').html();
          var account_balance = $('#account_balance').html();      
          
          $('.deduction_amount').change(function(){
              var deduction_amount = $(this).val();
                
              
              if (validateDeductionAmount(deduction_amount)){
                  var final_price = total_price - deduction_amount;
                  
                  if( validateAccountBalance(account_balance, deduction_amount) )
                  {
                      if ( !validateFinalPrice(final_price) ){
                          $(this).val(total_price);
                          //alert("FUCK")
                          // alert(total_price);
                          $('#total_fee').html(0);
                          $("#dumb").prop('checked', true);     
                          $('#paying_method').hide();
                      }else{
               //           alert(final_price);
                          $('#total_fee').html( final_price );
                          $("#dumb").prop('checked', false);     
                          $('#paying_method').show();
                      }
                  }
                  else{
                      $('.deduction_amount').val(0);
                  }
              }
          });

          $('#atm_fields').hide();
            
          $('.payment_method').change(function(){
              if ($(this).val()=='atm'){
                  $('#atm_fields').show();
              }
              else{
                  $('#atm_fields').hide();
              }
          });
      });

function isPositiveInteger(n) {
    return n >>> 0 === parseFloat(n);
}

function validateDeductionAmount(deduction_amount){
    console.log('validateDeductionAmount: ' + deduction_amount);
    if ( isNaN(deduction_amount) ){
        alert('請輸入數字');
        return false;
    }

    if ( deduction_amount < 0 ){
        alert('請輸入正整數');
        return false;
    }
    if ( !isPositiveInteger(deduction_amount) ){
        alert('請輸入正整數。');
        return false;              
    }
    
    return true;
}

function validateAccountBalance(account_balance, deduction_amount){
    account_balance = parseInt(account_balance);
    deduction_amount = parseInt(deduction_amount);
    console.log('validateAccountBalance: ' + account_balance + ', ' + deduction_amount);
    if ( deduction_amount > account_balance ){
        alert('您的帳戶餘額不足。');
        return false;              
    }
    
    return true;
  
}

function validateFinalPrice(final_price){
    console.log('validateFinalPrice: ' + final_price);
    if ( isNaN(final_price) ){
        throw new Error('final_price should be number');
    }
    /*
    if ( !isPositiveInteger(deduction_amount) ){
        throw new Error('final_price should be number');         
    }
    */
    if ( final_price < 0 ){
        // alert('應付餘額不可小於零');
        return false;
    }
    
    if ( final_price == 0 ){
       // alert('應付餘額不可為零');
        return false;
    }
    
    return true;
}

</script>
