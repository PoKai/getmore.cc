<div class="content order-page">
    <div class="nav">二手衣販售管理</div>
    <div class="welcome checkout">
        歡迎來到&nbsp;&nbsp;<img src="/assets/images/logo_s.png" />&nbsp;&nbsp;結帳櫃台，很高興能為您服務！
    </div>
    <div class="all-steps">
        <div class="steps"><div class="single-step">輸入<br />衣服資訊</div></div>
        <div class="steps active"><div class="single-step">輸入<br />取件資訊</div></div>
        <div class="steps"><div class="single-step">線上<br />洗衣付費</div></div>
        <div class="steps"><div class="single-step">列印<br />標籤</div></div>
    </div>
    <form id="step-2" action="/sellorder/steps/3" method="POST">
    <div class="information <?php echo (empty($events)) ? 'footer' : ''; ?>">
        <div class="detail">
        <table>
            <tr>
                <td>
                    <div class="header">請輸入取件資訊</div>
                    <label>中文姓名</label>
                    <input name="user_name" type="text" value="<?php echo $profile['user_name']; ?>" placeholder="輸入姓名" />&nbsp;&nbsp;<input type="radio" name="user_sex" value="1" checked />&nbsp;先生&nbsp;&nbsp;<input type="radio" name="user_sex" value="0" />&nbsp;小姐
                    <label>手機號碼</label>
                    <input name="user_mobile_phone" type="text" value="<?php echo $profile['mobile_phone']; ?>" placeholder="輸入手機號碼" />
                    <label>聯絡電話</label>
                    <input name="user_telephone" type="text" value="<?php echo $profile['telephone']; ?>" placeholder="輸入家裡電話" />
                    <label>聯絡地址</label>
                    <span id="user_address" class="address"></span>
                </td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td>
                    <div class="header">期滿送回資訊&nbsp;<input id="add_to_delivery" name="add_to_delivery" data-name="user_name" type="checkbox" value="1" /><span class="delivery">&nbsp;同取件資訊</span>&nbsp;<span>※請填寫真實收件人資訊以便收件。</span></div>
                    <p>您可以選擇不同於取件的地址，或是選擇將您的衣服捐出，GetMore將於期滿後協助處理。</p>
                    <input id="no_return" name="no_return" data-name="user_name" type="checkbox" value="1" />&nbsp;期滿後未售出商品全數捐贈慈善機構，不用送回給我了。&nbsp;
                    <label>中文姓名</label>
                    <input name="delivery_name" type="text" value="" placeholder="輸入姓名" />&nbsp;&nbsp;<input class="delivery_sex_man" type="radio" name="delivery_sex" value="1" checked />&nbsp;先生&nbsp;&nbsp;<input type="radio" class="delivery_sex_woman" name="delivery_sex" value="0" />&nbsp;小姐
                    <label>手機號碼</label>
                    <input name="delivery_mobile_phone" type="text" value="" placeholder="輸入手機號碼" />
                    <label>聯絡電話</label>
                    <input name="delivery_telephone" type="text" value="" placeholder="輸入家裡電話" />
                    <label>聯絡地址</label>
                    <span id="delivery_address" class="address"></span>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; padding: 10px 0px;"><input type="radio" name="uniform_invoice_type" value="0" checked />&nbsp;&nbsp;捐贈發票(捐贈單位：創世基金會)或&nbsp;&nbsp;<input type="radio" name="uniform_invoice_type" value="1" />&nbsp;二聯式發票&nbsp;&nbsp;<input type="radio" name="uniform_invoice_type" value="2" />&nbsp;三聯式發票</td>
            </tr>
            <tr class="show_uniform_invoice" style="display:none;">
                <td colspan="2">發票抬頭&nbsp;&nbsp;<input name="uniform_invoice_title" type="text" value="" placeholder="Enter uniform invoice title" /></td>
            </tr>
            <tr class="show_uniform_invoice" style="display:none;">
                <td colspan="2">統一編號&nbsp;&nbsp;<input name="uniform_invoice_number" type="text" value="" placeholder="Enter uniform invoice number" /></td>
            </tr>
        </table>
        </div>
    </div>
    <div class="steps">
        <a href="/sellorder/steps/1" class="back">上一步</a>
        <a href="#" class="next" data-form="step-2" id="step3">下一步</a>
    </div>
    </form>
</div>

<div class="order-note">
    <div class="container">
        <div class="right">
            <ul>
                <li>● getmore 金流採用藍新科技解決方案，為台灣目前金流第一品牌。</li>
                <li>● 信用卡刷卡採用藍新科技信賴付款機制，受『128-bit SSL』加密保護。</li>
                <li>● WEBATM 採用第一銀行金流服務，請先準備好您的金融卡及讀卡機。</li>
                <li>● ATM 轉帳付款採用第一銀行虛擬帳號，系統將自動為您辦理入帳手續，您可不需再傳真確認。</li>
                <li>● 如果您有任何疑問，請email至lab@getmore.cc。</li>
            </ul>
        </div>
        <div class="left">
            <h3>付費須知</h3>
        </div>
        <div class="clear"></div>
    </div>
</div>
