<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('generate_order_number')){
    function generate_order_number($from_product = false){
        
        $instance = & get_instance();
        
        $instance->load->helper('date');
        $instance->load->library('lib_sellorder');
        $instance->load->model('Product_clothing_model');
        // $datestring = "Year: %Y Month: %m Day: %d - %h:%i %a";
        $datestring = "%Y%m%d";
        $time = time();
        // $time = $instance->_time;

        // date('Ymd') == date('Ymd', strtotime($timestamp));
        // update order status
        // $instance->db->set($data)->where('orders.order_number', $ordernumber)->update('orders');
        // query order_number

        $order_num = "S";
        if($_SERVER['REMOTE_ADDR'] == '127.0.0.1' ){
            $order_num = "T";
        }
        
        $time_str = mdate($datestring, $time);
        
        if(!$from_product){
            // select count(*), DATE_FORMAT(date_purchased,"%Y-%m-%d") as created_day FROM sellorders GROUP BY created_day
            $result = $instance->lib_sellorder->select('count(*), DATE_FORMAT(date_purchased,"%Y%m%d") as created_day')
                 ->group_by('created_day')
                 ->orders()
                 ->result_array();
        }
        else {
            // select count(*), DATE_FORMAT(date_purchased,"%Y-%m-%d") as created_day FROM sellorders GROUP BY created_day
            $result = $instance->Product_clothing_model->db->select('count(*), FROM_UNIXTIME(add_time,"%Y%m%d") as created_day', FALSE)
                 ->group_by('created_day')
                 ->get('product_clothing')
                 ->result_array();
        }
        
        foreach ($result as $row) {
            if($time_str == $row['created_day']){
                $count_today = intval($row['count(*)']);
                
                if(!$count_today || $count_today==0)
                    $count_today = 1;
                else  
                    $count_today++;
                
                break;
            }else{
                $count_today = 1;
            }
        }
        
        $sn_today = $count_today;
        $sn_today = str_pad($sn_today,4,'0',STR_PAD_LEFT);
        
        $time_str = substr($time_str, 2);
        $order_num .= $time_str.$sn_today;
        
        // return strtoupper($instance->system->generate_code('1', 'word')) . $instance->system->generate_code('10', 'digit');
        return $order_num;
    }
}
?>