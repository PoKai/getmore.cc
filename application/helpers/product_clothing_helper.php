<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('sort_out_by_closet')){
    function sort_out_by_closet($clothings){
        return array_reduce($clothings, function($result, $clothing){
            $closet_id = $clothing['launch_way'] == "2"?0:$clothing['closet_id'];
            $closet_title = $clothing['launch_way'] == "2"?'getmore':$clothing['closet_title'];
            if(!isset($result[$closet_id])){
                $result[$closet_id] = array(
                    "title" => $closet_title,
                    "items" => array($clothing),
                    "total_price" => (int)$clothing['product_sell_price'],
                    "user_id" => $clothing["user_id"],
                    "closet_id" => $closet_id
                );
            }
            else {
                array_push($result[$closet_id]["items"], $clothing);
                $result[$closet_id]["total_price"] += (int)$clothing['product_sell_price'];
            }
            return $result;
        }, array());
    }
}

if (!function_exists('calculate_prices')){
    function calculate_prices($closets = array()){
        $prices = array_reduce($closets, function($result, $closet){
            $result["items_price"] += $closet["total_price"];
            return $result;
        }, array("items_price" => 0));

        $prices["shipping_price"] = !isset($closets[0])?0:$closets[0]["total_price"] >= 1000?0:80;

        $prices["total_price"] = $prices["shipping_price"] + $prices["items_price"];

        return $prices;
    }
}

?>
