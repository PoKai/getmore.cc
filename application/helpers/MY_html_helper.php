<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package     CodeIgniter
 * @author      ExpressionEngine Dev Team
 * @copyright   Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license     http://codeigniter.com/user_guide/license.html
 * @link        http://codeigniter.com
 * @since       Version 1.0
 * @filesource
 */
// ------------------------------------------------------------------------

/**
 * CodeIgniter HTML Helpers
 *
 * @package     CodeIgniter
 * @subpackage  Helpers
 * @category    Helpers
 * @author      ExpressionEngine Dev Team
 * @link        http://codeigniter.com/user_guide/helpers/html_helper.html
 */
// ------------------------------------------------------------------------

/**
 * script_tag
 *
 * Generates an javascript heading tag. First param is the data.
 *
 * @access  public
 * @param   string
 * @return  string
 */
if (!function_exists('script_tag')) {
	function script_tag($src = NULL) {
		if (isset($src) and !empty($src)) {
			return '<script src="' . base_url($src) . '" type="text/javascript"></script>';
		}

		return "";
	}
}

if (!function_exists('_log')) {
	function _log($data) {
		if (is_array($data)) {
			echo "<pre>";
			print_r($data);
			echo "</pre>";
		} else if (is_object($data)) {
			echo "<pre>";
			var_dump($data);
			echo "</pre>";
		} else {
			echo "<div style='color: red'>log message: " . $data . "</div>";
		}
	}
}

if (!function_exists('image_url')) {
	function image_url($file_id = null, $size = 'medium', $type = 'clothing') {
		switch ($type) {
		case 'clothing':
		case 'profile':
			$image_path = 'http://s3.getmore.cc/' . $type . '/' . $file_id . '/' . $size . '.jpg';
			break;
		case 'banner':
		case 'blog':
			$image_path = 'http://s3.getmore.cc/' . $type . '/' . $file_id . '.jpg';
			break;
		default:
			# code...
			break;
		}

		return $image_path;
	}
}

if (!function_exists('banner_url')) {
	function banner_url($id) {
		$s3_path = 'https://s3-ap-southeast-1.amazonaws.com/getmore/banner/';

		return $s3_path . $id . '.jpg';
	}
}

if (!function_exists('user_banner_url')) {
	function user_banner_url($id) {
		$s3_path = 'https://s3-ap-southeast-1.amazonaws.com/getmore/user_banner/';

		return $s3_path . $id . '.jpg';
	}
}

if (!function_exists('user_photo_url')) {
	function user_photo_url($id, $type = 'small') {
		$s3_path = 'https://s3-ap-southeast-1.amazonaws.com/getmore/profile/';

		return $s3_path . $id . '/' . $type . '.jpg';
	}
}

if (!function_exists('clothing_image_url')) {
	function clothing_image_url($id, $type = 'medium') {
		$s3_path = 'https://s3-ap-southeast-1.amazonaws.com/getmore/clothing/';

		return $s3_path . $id . '/-' . $type . '.jpg';
	}
}

/* because this partial view appear in many places(index, category, even 'suggested items' in the future)
 * i cant find good solution to fit it in the view or template system.
 * for DRY principle, i use helper as the workaround. although i know put html in php is ugly.
 *
 * @author howtomakeaturn
 */
if (!function_exists('clothing_thumbnail_layout')) {
	function clothing_thumbnail_layout($clothing) {
		$clothing = (object) $clothing;
		$id = $clothing->id;
		if (is_array($clothing->files)) {
			$img_src = image_url($clothing->files[0]);
		} else {
			$img_src = image_url(explode(',', $clothing->files)[0]);
		}
		$product_brand = $clothing->product_brand;
		$product_title = $clothing->product_title;
		$product_original_price = $clothing->product_original_price;
		$product_original_price_label = get_price_label($clothing->product_original_price);
		$product_sell_price = $clothing->product_sell_price;

		$recommended_tag = '';
		$is_recommended = $clothing->is_recommended;
		if ($is_recommended) {
			$recommended_tag = '<div class="recommended_tag"><img src="/assets/images/superrecommend.png"></div>';
		}

		$soldout_tag = '';
		if ($clothing->lock_status == '2') {
			$soldout_tag = "<div class='soldoutsticker'><img src='/assets/images/home/soldout.png'/></div>";
		} else if ($clothing->lock_status == '1') {
			$soldout_tag = "<div class='soldoutsticker'><img src='/assets/images/home/reserved.png'/></div>";
		} else if ($clothing->lock_status == '5') {
			$soldout_tag = "<div class='soldoutsticker'><img src='/assets/images/home/status50.png'/></div>";
		}
		$reduction_tag = '';
		$reduction_again = $clothing->reduction_again;
		if ($reduction_again) {
			$reduction_tag = "<span class='pricedown-tag'>再次降價↓</span>";
		}

		//change to new like  style
		$like_tag = '<div class="likeslideup like-fade">
                      <div class="likeslideupcontent" data-id=' . $clothing->id . '>
                        <i class="fa fa-gratipay fa-2x"></i><div>LIKE</div>
                    </div>
                    </div>';

		$size_tag = '';
		$size_tag = ($clothing->size_type_major == 'Fits') ?
		$clothing->size_type_major . ' ' .
		$clothing->size :
		$clothing->size_type_major .
		$clothing->size;

		// if($clothing->size_number_minor != ''){
		//     $size_tag = ($clothing->size_type_minor == 'Fits') ?
		//         $size_tag . '/' . $clothing->size_type_minor . ' ' .
		//         $clothing->size_number_minor :
		//         $size_tag . '/' . $clothing->size_type_minor .
		//         $clothing->size_number_minor;
		// }

		$lock_status = "lock-status-" . $clothing->lock_status;

		if ($clothing->launch_way == 1) {
			$href = '/clothing/marketplace/' . $id;
		} else {
			$href = '/clothing/brandsonly/' . $id;
		}
		$plain_html = '' .
			"<a target='_blank' href='{$href}'>" .
			"<div class='fresh-product-view {$lock_status}'>" .
			$recommended_tag .
			$soldout_tag .
			"<img src='{$img_src}' />" .
			"<div class='product-brand'>" .
			$product_brand .
			"</div>" .
			"<div class='fresh-product-title'>" .
			$product_title .
			"</div>" .
			"<span class='size-tag'>" .
			"Size " . $size_tag .
			"</span>" .
			"<div class='fresh-product-price'>" .
			"<span class='is-deleted'>" .
			"{$product_original_price_label}" .
			"</span>" .
			"<span class='fresh-product-sale'>" .
			"NT$" . "{$product_sell_price}" .
			"</span>" .
			"</div>" .
			$reduction_tag .
			$like_tag .
			"</div>" .
			"</a>";
		return $plain_html;
	}
}

/* transform original price value from the one in database to a price period
 * @author howtomakeaturn
 */
if (!function_exists('get_price_label')) {
	function get_price_label($price_value_in_database) {
		$price_label = '';
		$price_value_in_database = (int) $price_value_in_database;
		if ($price_value_in_database <= 1000) {
			$price_label = 'NT$ 500~1,000';
		} else if ($price_value_in_database <= 10000) {
			$price_label = 'NT$' . (preg_replace('/(\d+)(\d{3})$/', '$1,$2', $price_value_in_database - 1000 + 1)) . "~" . (preg_replace('/(\d+)(\d{3})$/', '$1,$2', $price_value_in_database));
		} else if ($price_value_in_database == 99999) {
			$price_label = '超過10,000';
		} else if ($price_value_in_database <= 50000) {
			$limit_price = $price_value_in_database - 5000;
			$point1 = (int) ($limit_price % 10000) / 1000;
			$point2 = (int) ($price_value_in_database % 10000) / 1000;
			$price_label = 'NT$' . (int) ($limit_price / 10000) . ($point1 ? "." . $point1 : '') . "萬~" . (int) ($price_value_in_database / 10000) . ($point2 ? "." . $point2 : '') . "萬";
		} else if ($price_value_in_database <= 100000) {
			$limit_price = $price_value_in_database - 10000;
			$price_label = 'NT$' . (int) ($limit_price / 10000) . "萬~" . (int) ($price_value_in_database / 10000) . "萬";
		} else {
			$price_label = '超過10萬';
		}
		return $price_label;
	}
}

if (!function_exists('get_sellorder_status_label')) {
	function get_sellorder_status_label($status_id) {
		$status_label = '';
		if ($status_id == '-1') {
			$status_label = '已取消';
		} else if ($status_id == '1') {
			$status_label = '待付款';
		} else if ($status_id == '1.5') {
			$status_label = 'ATM待付款';
		} else if ($status_id == '2') {
			$status_label = '待收貨';
		} else if ($status_id == '3') {
			$status_label = '收貨中';
		} else if ($status_id == '4') {
			$status_label = '驗收中';
		} else if ($status_id == '5') {
			$status_label = '已送洗';
		} else if ($status_id == '6') {
			$status_label = '已送洗';
		} else {
			$status_label = $status_id;
		}

		return $status_label;
	}
}

if (!function_exists('get_product_type_label')) {
	function get_product_type_label($type) {
		$CI = &get_instance();
		$CI->config->load('clothing_class');
		$class_name = $CI->config->item('class_name');

		return $class_name[$type];
	}
}

if (!function_exists('get_product_type_by_product_item_number')) {
	function get_product_type_by_product_item_number($item_number) {
		switch (substr($item_number, 0, 1)) {
		case 'S':
			return 'brandsonly';
			break;
		case 'A':
			return 'marketplace';
			break;
		case 'D':
			return 'designer';
			break;
		}
	}
}

if (!function_exists('get_product_rough_type_label')) {
	function get_product_rough_type_label($type) {
		if ($type[0] == 1) {
			return 'Women';
		} else if ($type[0] == 2) {
			return 'Men';
		} else if ($type[0] == 3) {
			return 'Kids';
		} else {
			return $type;
		}

	}
}

function get_semantic_measure_name($size_code, $measure) {
	$json_str = '[{"labels": [{"code": "1", "label": "\u80a9\u5bec"}, {"code": "3", "label": "\u8863\u9577"}, {"code": "4", "label": "\u80f8\u5bec"}, {"code": "5", "label": "\u8896\u9577"}, {"code": "9", "label": "\u8896\u53e3\u5bec"}, {"code": "10", "label": "\u4e0b\u64fa\u5bec"}], "code": "11ls"}, {"labels": [{"code": "1", "label": "\u80a9\u5bec"}, {"code": "3", "label": "\u8896\u650f"}, {"code": "5", "label": "\u80f8\u5bec"}, {"code": "6", "label": "\u8863\u9577"}, {"code": "8", "label": "\u4e0b\u64fa\u5bec"}], "code": "11ns"}, {"labels": [{"code": "1", "label": "\u80a9\u5bec"}, {"code": "2", "label": "\u8896\u9577"}, {"code": "5", "label": "\u8896\u53e3\u5bec"}, {"code": "6", "label": "\u80f8\u5bec"}, {"code": "7", "label": "\u8863\u9577"}, {"code": "8", "label": "\u4e0b\u64fa\u5bec"}], "code": "11ss"}, {"labels": [{"code": "1", "label": "\u80a9\u5bec"}, {"code": "2", "label": "\u8896\u9577"}, {"code": "3", "label": "\u8863\u9577"}, {"code": "4", "label": "\u80f8\u5bec"}, {"code": "5", "label": "\u8170\u5bec"}, {"code": "6", "label": "\u81c0\u5bec"}, {"code": "8", "label": "\u8896\u53e3\u5bec"}, {"code": "9", "label": "\u4e0b\u64fa\u5bec"}], "code": "12ls"}, {"labels": [{"code": "1", "label": "\u80a9\u5bec"}, {"code": "3", "label": "\u8896\u650f"}, {"code": "5", "label": "\u80f8\u5bec"}, {"code": "6", "label": "\u8170\u5bec"}, {"code": "7", "label": "\u81c0\u5bec"}, {"code": "8", "label": "\u8863\u9577"}, {"code": "9", "label": "\u4e0b\u64fa\u5bec"}], "code": "12ns"}, {"labels": [{"code": "1", "label": "\u80a9\u5bec"}, {"code": "2", "label": "\u8896\u9577"}, {"code": "4", "label": "\u8896\u53e3\u5bec"}, {"code": "5", "label": "\u80f8\u5bec"}, {"code": "6", "label": "\u8170\u5bec"}, {"code": "7", "label": "\u81c0\u5bec"}, {"code": "8", "label": "\u8863\u9577"}, {"code": "9", "label": "\u4e0b\u64fa\u5bec"}], "code": "12ss"}, {"labels": [{"code": "1", "label": "\u8170\u5bec"}, {"code": "2", "label": "\u81c0\u5bec"}, {"code": "4", "label": "\u8932\u64cb"}, {"code": "5", "label": "\u5927\u817f\u5bec"}, {"code": "7", "label": "\u8932\u9577"}, {"code": "8", "label": "\u8932\u53e3\u5bec"}], "code": "13lp"}, {"labels": [{"code": "2", "label": "\u8170\u5bec"}, {"code": "4", "label": "\u81c0\u5bec"}, {"code": "5", "label": "\u8932\u64cb"}, {"code": "6", "label": "\u5927\u817f\u5bec"}, {"code": "7", "label": "\u8932\u9577"}, {"code": "8", "label": "\u8932\u53e3\u5bec"}], "code": "13sp"}, {"labels": [{"code": "3", "label": "\u8170\u5bec"}, {"code": "5", "label": "\u81c0\u5bec"}, {"code": "6", "label": "\u88d9\u9577"}, {"code": "8", "label": "\u4e0b\u64fa\u5bec"}], "code": "14"}, {"labels": [{"code": "2", "label": "\u80a9\u5bec"}, {"code": "4", "label": "\u8896\u9577"}, {"code": "5", "label": "\u80f8\u5bec"}, {"code": "6", "label": "\u8863\u9577"}, {"code": "8", "label": "\u4e0b\u64fa\u5bec"}, {"code": "10", "label": "\u8896\u53e3\u5bec"}], "code": "15"}, {"labels": [{"code": "3", "label": "\u80a9\u5bec"}, {"code": "4", "label": "\u8896\u9577"}, {"code": "5", "label": "\u80f8\u5bec"}, {"code": "6", "label": "\u8863\u9577"}, {"code": "9", "label": "\u8896\u53e3\u5bec"}, {"code": "10", "label": "\u4e0b\u64fa\u5bec"}], "code": "21ls"}, {"labels": [{"code": "1", "label": "\u80a9\u5bec"}, {"code": "3", "label": "\u8896\u650f"}, {"code": "5", "label": "\u80f8\u5bec"}, {"code": "6", "label": "\u8863\u9577"}, {"code": "9", "label": "\u4e0b\u64fa\u5bec"}], "code": "21ns"}, {"labels": [{"code": "1", "label": "\u80a9\u5bec"}, {"code": "2", "label": "\u8896\u9577"}, {"code": "5", "label": "\u8896\u53e3\u5bec"}, {"code": "6", "label": "\u80f8\u5bec"}, {"code": "7", "label": "\u8863\u9577"}, {"code": "8", "label": "\u4e0b\u64fa\u5bec"}], "code": "21ss"}, {"labels": [{"code": "1", "label": "\u9818\u570d"}, {"code": "4", "label": "\u80a9\u5bec"}, {"code": "5", "label": "\u80f8\u5bec"}, {"code": "6", "label": "\u8863\u9577"}, {"code": "7", "label": "\u8896\u9577"}, {"code": "9", "label": "\u8896\u53e3\u5bec"}, {"code": "10", "label": "\u4e0b\u64fa\u5bec"}], "code": "22ls"}, {"labels": [{"code": "1", "label": "\u9818\u570d"}, {"code": "2", "label": "\u80a9\u5bec"}, {"code": "3", "label": "\u8896\u650f"}, {"code": "6", "label": "\u80f8\u5bec"}, {"code": "7", "label": "\u8863\u9577"}, {"code": "9", "label": "\u4e0b\u64fa\u5bec"}], "code": "22ns"}, {"labels": [{"code": "1", "label": "\u9818\u570d"}, {"code": "2", "label": "\u80a9\u5bec"}, {"code": "3", "label": "\u8896\u9577"}, {"code": "6", "label": "\u8896\u53e3\u5bec"}, {"code": "7", "label": "\u80f8\u5bec"}, {"code": "8", "label": "\u8863\u9577"}, {"code": "9", "label": "\u4e0b\u64fa\u5bec"}], "code": "22ss"}, {"labels": [{"code": "1", "label": "\u8170\u5bec"}, {"code": "2", "label": "\u81c0\u5bec"}, {"code": "4", "label": "\u8932\u64cb"}, {"code": "6", "label": "\u5927\u817f\u5bec"}, {"code": "7", "label": "\u8932\u9577"}, {"code": "8", "label": "\u8932\u53e3\u5bec"}], "code": "23lp"}, {"labels": [{"code": "2", "label": "\u8170\u5bec"}, {"code": "3", "label": "\u81c0\u5bec"}, {"code": "4", "label": "\u8932\u64cb"}, {"code": "5", "label": "\u5927\u817f\u5bec"}, {"code": "7", "label": "\u8932\u9577"}, {"code": "8", "label": "\u8932\u53e3\u5bec"}], "code": "23sp"}, {"labels": [{"code": "2", "label": "\u80a9\u5bec"}, {"code": "5", "label": "\u8896\u9577"}, {"code": "6", "label": "\u80f8\u5bec"}, {"code": "7", "label": "\u8863\u9577"}, {"code": "8", "label": "\u4e0b\u64fa\u5bec"}, {"code": "10", "label": "\u8896\u53e3\u5bec"}], "code": "24"}, {"labels": [{"code": "2", "label": "\u80a9\u5bec"}, {"code": "3", "label": "\u8896\u9577"}, {"code": "4", "label": "\u80f8\u5bec"}, {"code": "5", "label": "\u8863\u9577"}, {"code": "7", "label": "\u8896\u53e3\u5bec"}, {"code": "9", "label": "\u8932\u53e3\u5bec"}, {"code": "10", "label": "\u81c0\u5bec"}], "code": "31"}, {"labels": [{"code": "2", "label": "\u80a9\u5bec"}, {"code": "3", "label": "\u8896\u9577"}, {"code": "4", "label": "\u80f8\u5bec"}, {"code": "6", "label": "\u8863\u9577"}, {"code": "8", "label": "\u8896\u53e3\u5bec"}, {"code": "9", "label": "\u4e0b\u64fa\u5bec"}], "code": "32ls"}, {"labels": [{"code": "2", "label": "\u80a9\u5bec"}, {"code": "4", "label": "\u8896\u650f"}, {"code": "6", "label": "\u80f8\u5bec"}, {"code": "7", "label": "\u8863\u9577"}, {"code": "8", "label": "\u4e0b\u64fa\u5bec"}], "code": "32ns"}, {"labels": [{"code": "1", "label": "\u80a9\u5bec"}, {"code": "2", "label": "\u8896\u9577"}, {"code": "5", "label": "\u8896\u53e3\u5bec"}, {"code": "6", "label": "\u80f8\u5bec"}, {"code": "7", "label": "\u8863\u9577"}, {"code": "8", "label": "\u4e0b\u64fa\u5bec"}], "code": "32ss"}, {"labels": [{"code": "2", "label": "\u8170\u5bec"}, {"code": "4", "label": "\u81c0\u5bec"}, {"code": "5", "label": "\u8932\u64cb"}, {"code": "6", "label": "\u5927\u817f\u5bec"}, {"code": "8", "label": "\u8932\u9577"}, {"code": "9", "label": "\u8932\u53e3\u5bec"}], "code": "33lp"}, {"labels": [{"code": "3", "label": "\u8170\u5bec"}, {"code": "5", "label": "\u81c0\u5bec"}, {"code": "6", "label": "\u88d9\u9577"}, {"code": "8", "label": "\u4e0b\u64fa\u5bec"}], "code": "33skirt"}, {"labels": [{"code": "3", "label": "\u8170\u5bec"}, {"code": "4", "label": "\u8932\u9577"}, {"code": "5", "label": "\u81c0\u5bec"}, {"code": "6", "label": "\u8932\u64cb"}, {"code": "7", "label": "\u5927\u817f\u5bec"}, {"code": "10", "label": "\u8932\u53e3\u5bec"}], "code": "33sp"}, {"labels": [{"code": "1", "label": "\u80a9\u5bec"}, {"code": "3", "label": "\u80f8\u5bec"}, {"code": "4", "label": "\u8896\u9577"}, {"code": "5", "label": "\u8863\u9577"}, {"code": "7", "label": "\u8896\u53e3\u5bec"}, {"code": "8", "label": "\u4e0b\u64fa\u5bec"}], "code": "34ls"}, {"labels": [{"code": "1", "label": "\u80a9\u5bec"}, {"code": "3", "label": "\u8896\u650f"}, {"code": "5", "label": "\u80f8\u5bec"}, {"code": "7", "label": "\u8863\u9577"}, {"code": "8", "label": "\u4e0b\u64fa\u5bec"}], "code": "34ns"}, {"labels": [{"code": "1", "label": "\u80a9\u5bec"}, {"code": "2", "label": "\u8896\u9577"}, {"code": "4", "label": "\u8896\u53e3\u5bec"}, {"code": "5", "label": "\u80f8\u5bec"}, {"code": "7", "label": "\u8863\u9577"}, {"code": "8", "label": "\u4e0b\u64fa\u5bec"}], "code": "34ss"}, {"labels": [{"code": "2", "label": "\u80a9\u5bec"}, {"code": "3", "label": "\u8896\u9577"}, {"code": "4", "label": "\u8863\u9577"}, {"code": "5", "label": "\u80f8\u5bec"}, {"code": "7", "label": "\u8896\u53e3\u5bec"}, {"code": "8", "label": "\u4e0b\u64fa\u5bec"}], "code": "35"}]';
	$mapping = json_decode($json_str, true);
	foreach ($mapping as $map) {
		if ($map['code'] == $size_code) {
			$meta = $map['labels'];
		}
	}

	$temp = [];
	foreach ($measure as $key => $value) {
		$temp[$key] = $value;
	}

	$result = array();
	foreach ($meta as $value) {
		$result[$value['label']] = $temp[$value['code']];
	}

	return $result;
}

if (!function_exists('alert_message')) {
	function alert_message($message, $type = 'danger') {
		if (empty($message)) {
			return '';
		}
		$html = "<div class='alert alert-$type'>$message</div>";
		return $html;
	}
}

function get_item_status_comment($item_status) {
	$result = '';
	if ($item_status == '全新未拆吊牌') {
		$result = '商品附原吊牌，且沒有使用痕跡。';
	} else if ($item_status == '幾近全新') {
		$result = '商品可能被穿過，但幾乎看不出使用痕跡。';
	} else if ($item_status == '狀況良好') {
		$result = '商品看得出使用痕跡，但整體狀況佳，沒有明顯的瑕疵。';
	} else if ($item_status == '有歲月痕跡') {
		$result = '由外觀可看出被使用過數次，但整體並無重大瑕疵。';
	}

	return $result;
}

if (!function_exists('post_message_modal')) {
	function post_message_modal($post = array('owner_id' => 2, 'user_id' => 2, 'photo' => "")) {
		$closet_id = $post['user_id'] + 9881;
		$html = "<div class=\"modal fade\" id=\"postmsg\">
                    <div class=\"modal-dialog\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                <h4 class=\"modal-title\">留下訊息</h4>
                            </div>
                            <div class=\"modal-body\">

                                <div class=\"fb_login\">
                                    <div class=\"postmsgfrom\">
                                        <a href=\"/closet/view/{$closet_id}\"  target=\"_blank\">
                                            <img class=\"avatar\" src=\"{$post['photo']}\">
                                        </a>
                                        <font>有商品的疑問嗎？直接問衣櫃主人</font>
                                    </div>
                                    <hr class=\"dashed_hr\"/>
                                </div>

                                <div id=\"getmore_login\">
                                    <div class=\"row\">
                                        <div>
                                            關於
                                        </div>
                                        <div>
                                            <input type=\"msgtitle\" class=\"form-control\" id=\"msgtitle\" name=\"identity\" placeholder=\"主旨\">
                                        </div>
                                        <div class=\"margintop20\">
                                            訊息內容
                                        </div>
                                        <div>
                                            <input type=\"hidden\" value=\"{$post['owner_id']}\" name=\"owner_id\" />
                                            <textarea name=\"content\" class=\"form-control\" rows=\"6\" placeholder=\"留下訊息\"></textarea>
                                              <div>
                                                <div class=\"margintop10\">
                                                  <button type=\"submit\" class=\"btn btn-default btn-xs\">送出訊息</button>
                                                </div>
                                              </div>
                                        </div>
                                    </div>
                                    <div>
                                        <font class=\"font-size12\">您的購買帳款或先由GetMore保管，完成交易後才交由賣家，私下交易無法追蹤交易紀錄，也容易引起買賣糾紛，對您來說極無保障，且不符合GetMore的二次時尚精神。因此，<font class=\"red\">請不要在刊登內容中加入任何引導私下交易的內容</font>。<u>若違反規則，您可能遭到停權，賣家商品或衣櫃也可能遭管理人員刪除。</u></font>
                                    </div>
                                </div>
                            </div><!-- /.modal-body -->
                                <div class=\"modal-footer\">
                                    <p class=\"announcement text-center\">更多交易安全及網站交易規範,<br>請閱讀<a href=\"/info/buy#buypolicy\" target=\"_blank\">購買條款</a>以及<a href=\"/info/sell#sellpolicy\" target=\"_blank\">代洗代售服務條款</a>。</p>
                                </div>
                        </div><!-- /.modal-content -->
                     </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <script src=\"/assets/javascript/bootstrap.js\"></script>";
		return $html;
	}
}

if (!function_exists('post_message_form')) {
	/*send message to db*/
	function post_message_form($i) {
		$html = "<div class=\"replypost wrap\">
                                <form method=\"POST\" enctype=\"multipart/form-data\" action=\"/restful/post/message\" class=\"form-horizontal message\"id = \"{$i}{$i}\">
                                    <div>
                                        <textarea name=\"content\" class=\"form-control\" rows=\"3\" maxlength=\"100\"></textarea>
                                        <input type=\"hidden\" value=\"{$i}\" name=\"post_id\">
                                        <div>
                                            <div class=\"margintop10 marginbottom20\">
                                                <input type=\"file\" id=\"file\" name=\"file\" style=\"margin-bottom:10px;font-size:10px;\">
                                                <button type=\"submit\" class=\"btn product-view-buy\">回覆訊息</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                </div>";
		return $html;
	}
}

if (!function_exists('price_area')) {
	function price_area() {

		$html = '<option value="0"></option>';
		$html .= '<option value="1000">500元~1,000元</option>';

		for ($price = 1000; $price < 10000; $price += 1000) {
			$html .= '<option value="' . ($price + 1000) . '">' . number_format($price + 1) . '元~' . number_format($price + 1000) . '元</option>';
		}

		for ($price = 10000; $price < 50000; $price += 5000) {
			$html .= '<option value="' . ($price + 5000) . '">' . number_format($price + 1) . '元~' . number_format($price + 5000) . '元</option>';
		}

		for ($price = 50000; $price < 100000; $price += 10000) {
			$html .= '<option value="' . ($price + 10000) . '">' . number_format($price + 1) . '元~' . number_format($price + 10000) . '元</option>';
		}

		$html .= '<option value="999999">超過100,000元</option>';

		return $html;
	}
}

if (!function_exists('payment_method_label')) {
	function payment_method_label($payment_method) {
		switch ($payment_method) {
		case 'deduction':
			return '全額抵扣';
		case 'atm':
			return 'ATM匯款';
		case 'credit-cart':
			return '信用卡付款';
		case 'pay-at-home':
			return '貨到付款';
		case 'line_pay':
			return 'Line Pay';
		default:
			return '';
		}
	}
}

if (!function_exists('transfer_utc')) {
	function transfer_utc($utc, $format = 'Y-m-d H:i:s') {
		$unix_time = strtotime($utc);

		return date($format, $unix_time);
	}
}

if (!function_exists('get_expiration_time')) {
	function get_expiration_time($created_time, $type = 'credit-cart') {
		$CI = &get_instance();
		$CI->config->load('checkout');
		$unix_time = strtotime($created_time);

		if ($type === 'credit-cart') {
			$active_time = $CI->config->item('active_time');
			$unix_time += $active_time * 60;
		} else if ($type === 'atm') {
			$active_time = $CI->config->item('atm_active_time');
			$unix_time += $active_time * 60 * 60;
		}

		return new DateTime(date('Y-m-d H:i:s', $unix_time));
	}
}

if (!function_exists('count_notReadPost')) {
	function count_notReadPost() {

		$CI = get_instance();
		$post["user_id"] = $CI->session->userdata('user_id');
		if (!$post["user_id"]) {
			return 0;
		}

		$select = 'box';
		$data = get_post($select);
		// $data = get_post('box');
		$data = json_decode($data);
		$i = 0;

		foreach ($data as $post) {
			if ($post->user_id == intval($CI->session->userdata('user_id'))) {
				if (!$post->user_read) {
					++$i;
				}

			} // if
			else {
				if (!$post->owner_read) {
					++$i;
				}

			} // else
		}

		return $i;
	}
}

if (!function_exists('post_messages')) {
	function post_messages($select) {

		$CI = get_instance();
		$post["user_id"] = $CI->session->userdata('user_id');

		$data = get_post($select);
		$data = json_decode($data);
		$i = 0;

		foreach ($data as $post) {
			$html_post_message = post_message($i, $select);
			$html_message = message($post->_id);
			$data_temp = json_decode(get_post($select));
			$html_post_message_form = post_message_form($post->_id);

			$html .= "<div class=\"panel panel-default\">
                            <div class=\"panel-heading\" role=\"tab\" id=\"headingOne\" data-toggle=\"collapse\" data-parent=\"#accordion\" data-target=\"#{$post->_id}\" aria-expanded=\"false\" aria-controls=\"collapseOne\" target=\"{$post->_id}\">
                            " . $html_post_message . "
                            </div>
                            <div id=\"{$post->_id}\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingOne\">
                              <div class=\"panel-body\">
                                " . $html_message . "
                                " . $html_post_message_form . "
                              </div>
                            </div>
                          </div>";
			$i++;
		}

		return $html;
	}
}

if (!function_exists('get_post')) {
	function get_post($select) {
		$ch = curl_init();
		$CI = get_instance();
		$CI->load->helper('url');
		if (strcmp($select, "box") == 0) {
			$url = base_url("/restful/post?user_id={$CI->session->userdata('user_id')}");
		} else if (strcmp($select, "sent") == 0) {
			$url = base_url("/restful/post/user/{$CI->session->userdata('user_id')}");
		}

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_COOKIE, http_build_query($_COOKIE, '', ';'));
		$file = curl_exec($ch);
		curl_close($ch);
		return $file;
	}
}
// get_post

if (!function_exists('post_message')) {
	function post_message($i, $select) {
		//$closet_id = 9881+$post["user_id"];

		$CI = get_instance();
		$CI->load->database();
		$post["user_id"] = $CI->session->userdata('user_id');
		// $CI->load->model('Post_model');
		// $data = $CI->Post_model->get_posts_by_ownerId($post["user_id"]);
		$post = $data[$i];

		$data = get_post($select);
		$data = json_decode($data);
		$post = $data[$i];
		$time = (string) $post->created_time;
		$time = date("Y-m-d H:i:s", strtotime($time));
		$closet_id = $CI->db->from('closet')->select('url_code')->where('user_id', $post->owner_id)->get()->row_array()['url_code'];
		$photo = "http://s3.getmore.cc/profile/$post->user_id/small.jpg";
		$send_from_nickname = $CI->db->from('users')->where('id', $post->user_id)->get()->row_array()['nickname'];
		$receiver_nickname = $CI->db->query("select * from users where id = " . $post->owner_id)->result_array()[0]['nickname'];

		// Check sender is admin
		$user_group_id = $CI->db->query("select * from users_groups where user_id = " . $post->user_id)->result_array();

		if (!intval($post->user_id)) {
			$user_nickname = "<span style='color: #ff0000'>管理員</span>";
			$photo = "/assets/images/home/logosquare.png";
		}

		//foreach ($data as $post) {
		$read_color = "gray";
		$user_personal_post = 0;

		if ($post->user_id == intval($CI->session->userdata('user_id'))) {
			if (!$post->user_read) {
				$read_color = "#ff0000";
			}

			$user_personal_post = 1;
		} // if
		else {
			if (!$post->owner_read) {
				$read_color = "#ff0000";
			}

		} // else
		// if ( !$post->owner_read ) {
		//     $read_color = "#ff0000;";
		// }
		// else
		//     $read_color = "gray";

		$html = "<div class=\"row margintop10\" target=\"{$post->_id}\" data-id =\"{$user_personal_post}\" style=\"color:{$read_color};\">
                                    <div class=\"msgfrom\" target=\"{$post->_id}\" data-id =\"{$user_personal_post}\">
                                        <a href=\"/closet/view/{$closet_id}\"  target=\"_blank\">
                                            <img class=\"avatar\" src=\"{$photo}\">
                                        </a>
                                        <a href=\"/closet/view/{$closet_id}\" target=\"_blank\">{$post->user_name}</a>
                                    </div>
                                    <div class=\"subject\" target=\"{$post->_id}\" data-id =\"{$user_personal_post}\">
                                        <div class=\"msgbox\" target=\"{$post->_id}\" data-id =\"{$user_personal_post}\">
                                            <p target=\"{$post->_id}\" data-id =\"{$user_personal_post}\">
                                                <b style=\"font-size:15px;\" target=\"{$post->_id}\" data-id =\"{$user_personal_post}\">{$post->title}</b>
                                            <a style=\"margin-left:10px;font-size:10px;\" href=\"{$post->url_code}\" target=\"_blank\" >查看該商品</a>
                                            </p>
                                            <div>{$post->content}</div>
                                        </div>
                                        <div class=\"msgtime\" target=\"{$post->_id}\" data-id =\"{$user_personal_post}\">來自 {$send_from_nickname} 寄給 {$receiver_nickname} {$time}</div>
                                    </div>
                               </div>";
		return $html;
	}
}

if (!function_exists('message')) {
	function message($id) {

		// $CI = get_instance();
		// $CI->load->model('Post_model');
		// $data = $CI->Post_model->get_messages_of_post($id);
		$CI = get_instance();
		$CI->load->database();

		$data = get_message_from_post($id);
		$data = json_decode($data);
		foreach ($data as $message) {
			$time = (string) $message->created_time;
			$time = date("Y-m-d H:i:s", strtotime($time));
			$user_id = $message->user_id;
			$user_name = $CI->db->query("Select * from users where id = " . $user_id)->result_array()[0]['user_name'];
			$closet_id = $CI->db->from('closet')->select('url_code')->where('user_id', $user_id)->get()->row_array()['url_code'];
			$photo = "http://s3.getmore.cc/profile/$message->user_id/small.jpg";
			$img = "";
			$color = "#444";

			// Check sender is admin
			$user_group_id = $CI->db->query("select * from users_groups where user_id = " . $user_id)->result_array();

			if (!intval($user_id)) {
				$user_name = "<span style='color: #ff0000'>管理員</span>";
				$photo = "/assets/images/home/logosquare.png";
			}

			if (isset($message->file)) {
				$img = "<img src=\"http://s3.getmore.cc/messages/$message->file.jpg\"><p></p><p></p>";
			}

			$html .= "<div class=\"reply\">
            <a href=\"/closet/view/{$closet_id}\" target=\"_blank\"><img class=\"avatar\" src=\"{$photo}\"></a>
            <div class=\"replysubject\">"
				. $img .
				"<p style=\"line-height:1.5; font-size:13px; color:{$color}; word-break:break-all;\">
                    {$message->content}
                </p>
                <div class=\"msgfrom\">來自 {$user_name}</div>
                <div class=\"msgtime\">{$time}</div>
            </div>
            </div>";
		}

		/*
			$html = "<div class=\"reply\">
			<img class=\"avatar\" src=\"{$message['photo']}\">
			<div class=\"replysubject\">
			<p style=\"line-height:1.5; font-size:13px; color:#444;\">
			{$message['content']}
			</p>
			<div class=\"msgtime\">{$message['created_time']}</div>
			</div>
			</div>";
		*/

		return $html;
	}
}

if (!function_exists('get_message_from_post')) {
	function get_message_from_post($post_id) {
		if (isset($post_id)) {
			$ch = curl_init();
			$CI = get_instance();
			$CI->load->helper('url');
			$url = base_url("/restful/post/" . $post_id . "/messages");
			curl_setopt($ch, CURLOPT_URL, $url);

			//curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
			//curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:multipart/form-data")); // 啟用POST
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_COOKIE, http_build_query($_COOKIE, '', ';'));

			//curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			$file = curl_exec($ch);
			curl_close($ch);
			return $file;
		}
		// else {
		//     echo "false";
		// }
	}
}

if (!function_exists('user_in_group')) {
	function user_in_group($group) {
		$CI = get_instance();

		return in_array($group, $CI->session->userdata('user_groups'));
	}
}

if (!function_exists('post_user_message_modal')) {
	function post_user_message_modal($localsite, $url_code) {
		$closet_id = $post['user_id'] + 9881;
		$owner_id = $localsite;
		$html = "<div class=\"modal fade\" id=\"userpostmsg\">
                    <div class=\"modal-dialog\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                <h4 class=\"modal-title\">發送訊息</h4>
                            </div>
                            <div class=\"modal-body\">

                                <div class=\"fb_login\">
                                    <div class=\"postmsgfrom\">
                                        <font>有商品的疑問嗎？直接問衣櫃主人</font>
                                    </div>
                                    <hr class=\"dashed_hr\"/>
                                </div>

                                <div id=\"getmore_login\">
                                    <div class=\"row\">
                        <form method=\"POST\" enctype=\"multipart/form-data\" action=\"/restful/post\" class=\"post\" id = \"message_post\" nate = \"post\">
                            <div style=\"display:none;\">
                                <label>收件者</label>
                                <input class = \"form-control\" type=\"text\" id = \"owner_id\" name=\"owner_id\" value=\"{$owner_id}\"placeholder=\"1234....\">
                                <input class = \"form-control\" type=\"text\" id = \"url_code\" name=\"url_code\" value=\"{$url_code}\"placeholder=\"1234....\">
                            </div>
                            <div>
                                <label>關於</label>
                                <input class = \"form-control\" type=\"text\" id =\"message_title\" name=\"title\">
                            </div>
                            <div>
                                <label>訊息內容</label>
                                <textarea class = \"form-control\" id =\"message_content\" name=\"content\" rows=\"7\"></textarea>
                            </div>
                            <div>
                                <input type=\"file\" id=\"file\" name=\"file\" style=\"margin-bottom:10px;\" >
                                <input type=\"reset\" value=\"取消\">
                                <input type=\"submit\" value=\"送出\"  class=\"post_submit\" id=\"post_submit\" >
                            </div>
                        </form>
                                    </div>
                                    <div>
                                        <font class=\"font-size12\">您的購買帳款或先由GetMore保管，完成交易後才交由賣家，私下交易無法追蹤交易紀錄，也容易引起買賣糾紛，對您來說極無保障，且不符合GetMore的二次時尚精神。因此，<font class=\"red\">請不要在刊登內容中加入任何引導私下交易的內容</font>。<u>若違反規則，您可能遭到停權，賣家商品或衣櫃也可能遭管理人員刪除。</u></font>
                                    </div>
                                </div>
                            </div><!-- /.modal-body -->
                                <div class=\"modal-footer\">
                                    <p class=\"announcement text-center\">更多交易安全及網站交易規範,<br>請閱讀<a href=\"/info/buy#buypolicy\" target=\"_blank\">購買條款</a>以及<a href=\"/info/sell#sellpolicy\" target=\"_blank\">代洗代售服務條款</a>。</p>
                                </div>
                        </div><!-- /.modal-content -->
                     </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <script src=\"/assets/javascript/bootstrap.js\"></script>";
		return $html;
	}
}

if (!function_exists('post_user_message_modal_for_layout_without_link_bootstrap')) {
	function post_user_message_modal_for_layout_without_link_bootstrap($localsite, $url_code) {
		$owner_id = $localsite;
		$html = "<div class=\"modal fade\" id=\"userpostmsg\">
                    <div class=\"modal-dialog\" style=\"z-index: 1100;\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                <h4 class=\"modal-title\">發送訊息</h4>
                            </div>
                            <div class=\"modal-body\">

                                <div class=\"fb_login\">
                                    <div class=\"postmsgfrom\">
                                        <font>有商品的疑問嗎？直接問衣櫃主人</font>
                                    </div>
                                    <hr class=\"dashed_hr\"/>
                                </div>

                                <div id=\"getmore_login\">
                                    <div class=\"row\">
                        <form method=\"POST\" enctype=\"multipart/form-data\" action=\"/restful/post\" class=\"post\" id = \"message_post\" nate = \"post\">
                            <div style=\"display:none;\">
                                <label>收件者</label>
                                <input class = \"form-control\" type=\"text\" id = \"owner_id\" name=\"owner_id\" value=\"{$owner_id}\"placeholder=\"1234....\">
                                <input class = \"form-control\" type=\"text\" id = \"url_code\" name=\"url_code\" value=\"{$url_code}\"placeholder=\"1234....\">
                            </div>
                            <div>
                                <label>關於</label>
                                <input class = \"form-control\" type=\"text\" id =\"message_title\" name=\"title\">
                            </div>
                            <div>
                                <label>訊息內容</label>
                                <textarea class = \"form-control\" id =\"message_content\" name=\"content\" rows=\"7\"></textarea>
                            </div>
                            <div>
                                <input type=\"file\" id=\"file\" name=\"file\" style=\"margin-bottom:10px;\" >
                                <input type=\"reset\" value=\"取消\">
                                <input type=\"submit\" value=\"送出\"  class=\"post_submit\" id=\"post_submit\" >
                            </div>
                        </form>
                                    </div>
                                    <div>
                                        <font class=\"font-size12\">您的購買帳款或先由GetMore保管，完成交易後才交由賣家，私下交易無法追蹤交易紀錄，也容易引起買賣糾紛，對您來說極無保障，且不符合GetMore的二次時尚精神。因此，<font class=\"red\">請不要在刊登內容中加入任何引導私下交易的內容</font>。<u>若違反規則，您可能遭到停權，賣家商品或衣櫃也可能遭管理人員刪除。</u></font>
                                    </div>
                                </div>
                            </div><!-- /.modal-body -->
                                <div class=\"modal-footer\">
                                    <p class=\"announcement text-center\">更多交易安全及網站交易規範,<br>請閱讀<a href=\"/info/buy#buypolicy\" target=\"_blank\">購買條款</a>以及<a href=\"/info/sell#sellpolicy\" target=\"_blank\">代洗代售服務條款</a>。</p>
                                </div>
                        </div><!-- /.modal-content -->
                     </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->";
		return $html;
	}
}

if (!function_exists('_followlist_html')) {
	function _followlist_html($designerlist, $userlist) {
		$html = '';
		$empty_msg = "<div class='alert alert-info' style='margin-top:30px;'>您目前尚未追蹤任何人喔！</div>";

		$html .= '
            <div style="margin-top:30px;">
                <span class="fresh-title">
                    追蹤的設計師和品牌
                </span>';
		if (!empty($designerlist)) {
			$html .= '
                <button type="button" class="btn btn-primary edit-list" id="edit_designer" data-toggle="tooltip" data-html="true" title="點選可重新排列<br/>或取消追隨">編輯</button>
                <div class="followdsnerbox">
                    <ul class="sortable grid" id="designer_list">';
			foreach ($designerlist as $designer) {
				$html .= '
                        <li>
                            <div class="followdsnerbanner" id="' . $designer->followed . '">
                                <div class="user-disfollow" data-toggle="modal" data-target="#user-disfollow-modal">
                                    <a href="#"><span class="fa fa-times-circle-o fa-2x fa-inverse"></span></a>
                                </div>
                                <a href="/shop/designer/' . $designer->info['url_code'] . '" target="_blank">
                                    <img src="http://s3.getmore.cc/designer/signboard/' . $designer->info['user_id'] . '/small.jpg">
                                    <div class="followbrand">' . $designer->info['title'] . '</div>
                                </a>
                            </div>
                        </li>';
			}
			$html .= '</ul>
                </div>';
		} else {
			$html .= $empty_msg;
		}
		$html .= '<hr/></div>';
		$html .= '
            <div style="margin-top:30px;">
                <span class="fresh-title">追蹤的達人
                </span>';
		if (!empty($userlist)) {
			$html .= '
                <button type="button" class="btn btn-primary edit-list" id="edit_user" data-toggle="tooltip" data-html="true" title="點選可重新排列<br/>或取消追隨">編輯</button>
                <div class="followdsnerbox">
                    <ul class="sortable grid" id="user_list">';
			foreach ($userlist as $user) {
				$html .= '
                        <li>
                            <div class="followmktbanner" id="' . $user['user_id'] . '">
                                <div class="user-disfollow" data-toggle="modal" data-target="#user-disfollow-modal">
                                    <a href="#"><span class="fa fa-times-circle-o fa-2x fa-inverse"></span></a>
                                </div>
                                <a href="/closet/tabs/' . $user['url_code'] . '" target="_blank">
                                    <img src="' . $user['profile_pic_url'] . '">
                                    <div class="followbrand">' . $user['title'] . '</div>
                                </a>
                            </div>
                        </li>';
			}
			$html .= '
                    </ul>
                </div>
            </div>';
		} else {
			$html .= $empty_msg;
			$html .= '</div>';
		}

		return $html;
	}
}

if (!function_exists('_new_clothing_thumbnail_html')) {
	function _new_clothing_thumbnail_html($item, $type) {
		$CI = get_instance();
		$CI->load->model('Clothing_model');
		$item_html = '';

		if ($type == 'designer_products') {
			$link_type = 'designer';
			$item_type = '3';
		} else if ($type == 'marketplace') {
			$link_type = 'marketplace';
			$item_type = '1';
		} else {
			$link_type = 'brandsonly';
			$item_type = '2';
		}

		if ($CI->Clothing_model->_item_is_liked($item->id, $item_type)) {
			$is_liked = 'liked';
		} else {
			$is_liked = 'to_like';
		}

		$colors_div = '';
		foreach ($item->colors as $color) {
			$colors_div .=
				'<div style="width: 0;height: 0;border-width: 5px;border-style: solid;border-color: ' . $color . ';display: inline-block;margin-right:0px;" class="ng-scope"></div>';
		}

		if (is_array($item->files)) {
			$img_src = image_url($item->files[0]);
		} else {
			$img_src = image_url(explode(',', $item->files)[0]);
		}

		if ($item->lock_status == 2) {
			$sold_out_text = '
                <p class="sold-out-text text-center" style="position:absolute;top:0;left:0;bottom:0;right:0;padding-top:125px;font-weight:bold;font-size:1.4em;background-color:#ffffff;opacity:0.7;">
                    SOLD<br/>OUT
                </p>';
		} else {
			$sold_out_text = '';
		}

		$item_html .=
		'<a href="/clothing/' . $link_type . '/' . $item->id . '" target="_blank">
          <div class="fresh-product-view lock-status-' . $item->lock_status . '">' .
		$sold_out_text
		. '<img src="' . $img_src . '">
            <div class="product-brand">' . $item->product_brand . '</div>
            <div class="fresh-product-title">' . $item->product_title . '</div>
            <div class="fresh-product-price" style="overflow:hidden;">
              <span class="is-deleted">NT$ ' . $item->product_original_price . '</span>
              <span class="fresh-product-sale">NT$ ' . $item->product_sell_price . '</span>
            </div>' .
		$colors_div .
		'<div class="likeslideup like-fade">
              <div class="likeslideupcontent ' . $is_liked . '" data-id="' . $item->id . '"><i class="fa fa-gratipay fa-2x"></i>
                <div>' .
			($is_liked ? 'SAVED' : 'SAVE')
			. '</div>
                <input type="hidden" class="like-item-type" value="' . $item_type . '">
              </div>
            </div>
          </div>
        </a>';
		return $item_html;
	}
}

if (!function_exists('FirstBuy')) {
	function FirstBuy() {
		$CI = get_instance();
		$CI->load->database();
		$buy_count = $CI->session->userdata('buy_count');

		if ($buy_count == 0 || empty($buy_count)) {
			return true;
		} else {
			return false;
		}
	}
}

// get_message_from_post
/* End of file html_helper.php */
/* Location: ./application/helpers/MY_html_helper.php */
