<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Some common utilities for use in checkout procedure.
 * 
 * you may need to use ->format('Ymd') in further step
 * 
 * @author howtomakeaturn
 * @param string with format Y-m-d H:i:s
 * @return date object
 */
if ( ! function_exists('get_atm_duedate'))
{
    function get_atm_duedate($datetime_string=''){
        $ci = & get_instance();
        $ci->load->config('checkout');
        if (empty($datetime_string)){
            $datetime = new DateTime();        
        }
        else{
            $datetime = new DateTime($datetime_string);
        }
        
        $duedate = $datetime->modify("+".$ci->config->item('atm_active_time')." hours");
        return $duedate;
    }
}
