<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * support some common buyorder utilities
 * 
 * @author howtomakeaturn
 */
if ( ! function_exists('invoice_information'))
{
    function invoice_information($receipt_to = '', $uniform_invoice_type = ''){
        $result = receipt_to_label($receipt_to) . ' ' . uniform_invoice_type_label($uniform_invoice_type);
        
        if ($receipt_to=='3'){
            $result = receipt_to_label($receipt_to);
        }      
      
        return $result;    
    }
}



if ( ! function_exists('uniform_invoice_type_label'))
{
	function uniform_invoice_type_label($uniform_invoice_type = ''){
      $result = $uniform_invoice_type;
      
      if ($uniform_invoice_type === 'two'){
          $result = '二聯式發票';        
      }
      else if ($uniform_invoice_type === 'three'){
          $result = '三聯式發票';        
      }
      
      return $result;    
	}
}


if ( ! function_exists('receipt_to_label'))
{
	function receipt_to_label($receipt_to = ''){
      $result = $receipt_to;
      
      if ($receipt_to === '1'){
          $result = '寄給訂購人';        
      }
      else if ($receipt_to === '2'){
          $result = '寄給收件人';        
      }
      else if ($receipt_to === '3'){
          $result = '捐贈創世基金會';                
      }
      
      return $result;    
	}
}




if ( ! function_exists('delivery_time_label'))
{
	function delivery_time_label($delivery_time = ''){
      $result = $delivery_time;
      
      if ($delivery_time === 'any'){
          $result = '不指定';        
      }
      else if ($delivery_time === '0-12'){
          $result = '中午前';        
      }
      else if ($delivery_time === '12-17'){
          $result = '12時～17時';                
      }
      else if ($delivery_time === '17-20'){
          $result = '17時～20時';                
      }
      
      return $result;    
	}
}


if ( ! function_exists('payment_method_label'))
{
	function payment_method_label($payment_method = ''){
      $result = $payment_method;
      if ($payment_method === 'credit-cart'){
          $result = '信用卡';        
      }
      else if ($payment_method === 'pay-at-home'){
          $result = '貨到付款';        
      }
      else if ($payment_method === 'atm'){
          $result = 'ATM轉帳';                
      }
      else if ($payment_method === 'deduction'){
          $result = '全額抵扣';                
      }
      else if ($payment_method === 'cash'){
          $result = '現場購買-現金付款';
      }
      
      return $result;    
	}
}

if ( ! function_exists('status_label'))
{
	function status_label($status = ''){
      $result = $status;
      if ($status == '-1'){
          $result = '已過期';
      }
      else if ($status == '6'){
          $result = '已退貨';        
      }
      else if ($status == '1'){
          $result = '待付款';        
      }
      // this is useless. we don't use 1.25 for atm anymore.
      /*
      else if ($status == '1.25'){
          $result = 'ATM轉帳-未付款';        
      }
      */
      else if ($status == '1.5'){
          $result = '貨到付款-未付款';        
      }
      else if ($status == '2'){
          $result = '待出貨';        
      }
      else if ($status == '3'){
          $result = '已出貨';        
      }
      else if ($status == '4'){
          $result = '已到貨';        
      }
      else if ($status == '5'){
          $result = '已完成';        
      }
      
      return $result;    
	}
}

/*
 * Render the payment information FOR CUSTOMER
 * based on payment_method and order status
 * 
 * @author howtomaketurn
 */
if ( ! function_exists('payment_information')){
    function payment_information($payment_method='', $status = ''){
        $result = payment_method_label($payment_method) . ' ' . status_label($status);
        
        if ($payment_method=='deduction'){
            $result = payment_method_label($payment_method);
        }
        else if ($payment_method=='pay-at-home'){
            $result = payment_method_label($payment_method);
        }
        else if ( ($payment_method=='atm') && ($status>=2) ){
            $result = payment_method_label($payment_method) . ' ' . '已完成付款';
        }
        else if ( ($payment_method=='credit-cart') && ($status>=2) ){
            $result = payment_method_label($payment_method) . ' ' . '已完成付款';
        }
        
        return $result;
    }
}
