<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * Global functions helping development
 */

// A quick way to observe the variables.
// Allow chinese characters in a dirty way.
function look($var){
    exit('<meta charset="utf8">' . var_export($var));
}
