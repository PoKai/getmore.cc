<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

if (!function_exists('discount')) {
	function discount($original_price, $sell_price) {
		$discount = intval(round($sell_price / $original_price, 2) * 100);
		if ($discount <= 95) {
			if ($discount >= 10) {
				return str_replace('0', '', strval($discount));
			} else {
				return '1';
			}

		} else {
			return null;
		}
	}
}

if (!function_exists('size_translate')) {
	function size_translate($size) {
		switch ($size) {
		case 'custom_made':
			return '量身訂做';
			break;
		default:
			return $size;
			break;
		}
	}
}

if (!function_exists('accessories_semantic_measure_name')) {
	function accessories_semantic_measure_name($key, $group) {
		$shoes_measure_label_mapping = [
			'length' => '鞋長',
			'width' => '鞋寬',
			'heel' => '跟高/鞋高',
			'platform' => '防水台高度',
			'top' => '筒長',
			'round' => '筒圍',
		];
		$bags_measure_label_mapping = [
			'height' => '高度(Height)',
			'width' => '寬度(Width)',
			'depth' => '深度(Depth)',
			'weight' => '重量(Weight)',
			'strap drop' => '背帶長(Strap drop)',
		];
		$accessories_measure_label_mapping = [
			'height' => '高度(Height)',
			'width' => '寬度(Width)',
			'length' => '長度(Length)',
			'depth' => '深度(Depth)',
			'weight' => '重量(Weight)',
		];
		switch ($group) {
		case 'Shoes':
			if ($shoes_measure_label_mapping[$key]) {
				return $shoes_measure_label_mapping[$key];
			} else {
				return $key;
			}
			break;
		case 'Bags':
			if ($bags_measure_label_mapping[$key]) {
				return $bags_measure_label_mapping[$key];
			} else {
				return $key;
			}
			break;
		case 'Accessories':
			if ($accessories_measure_label_mapping[$key]) {
				return $accessories_measure_label_mapping[$key];
			} else {
				return $key;
			}
			break;
		default:
			return $key;
			break;
		}
	}
}

if (!function_exists('order_status_label')) {
	function order_status_label($order_status) {
		switch ($order_status) {
		case -2:
			return '已經取消';
			break;
		case -1:
			return '已經過期';
			break;
		case 1:
			return '尚未付款';
			break;
		case 1.5:
			return '貨到付款';
			break;
		case 2:
			return '等待出貨';
			break;
		case 3:
			return '已經出貨';
			break;
		case 4:
			return '已經到貨';
			break;
		case 5:
			return '已經完成';
			break;
		case 6:
			return '完全退貨';
			break;
		}
	}
}

if (!function_exists('flashdata')) {
	function flashdata($key) {
		$ci = &get_instance();
		$ci->load->library('session');
		return $ci->session->flashdata($key);
	}
}

if (!function_exists('measure_img')) {
	function measure_img($product_type, $item_long, $pattern) {
		$url = '/admin/assets/images/clothing_table/' . $product_type;

		if (!($product_type == "14" || $product_type == "15" || $product_type == "24" || $product_type == "31" || $product_type == "35")) {
			if (!($product_type == "33" && $pattern == "20")) {
				$url .= $item_long;
			} else {
				$url .= 'skirt';
			}
		}

		return $url . '.png';
	}
}

if (!function_exists('product_link')) {
	function product_link($product_type, $product_id) {
		$link = '/clothing/';
		switch ($product_type) {
		case 1:
			$link .= 'marketplace/';
			break;
		case 2:
			$link .= 'brandsonly/';
			break;
		case 3:
			$link .= 'designer/';
			break;
		case 4:
			$link .= 'buyer/';
			break;
		}
		$link .= $product_id;
		return $link;
	}
}

if (!function_exists('reason_label')) {
	function reason_label($reason_value) {
		$label_list = ['狀態過舊', '明顯污漬', '破損', '勾紗/裂紗', '款式陳舊', '貼身衣物', '嚴重起毛球', '嚴重脫線', '縮水/褪色/染色', '掉飾/掉圖', '掉釦', '斑駁/龜裂', '不符品牌限制', '剪標商品', '品牌疑慮', '未收到', '其他'];
		return $label_list[$reason_value - 1];
	}
}

if (!function_exists('season_label')) {
	function season_label($season_value) {
		switch ($season_value) {
		case 1:
			return '春夏';
			break;
		case 2:
			return '秋冬';
			break;
		case 3:
			return '無季節';
			break;
		}
	}
}

if (!function_exists('split_files')) {
	function split_files($files = null) {
		$words = explode(',', $files);
		return $words;
	}
}

if (!function_exists('s3_url')) {
	function s3_url($file_id = null, $size = 'medium', $type = 'clothing') {

		switch ($type) {
		case 'clothing':
		case 'profile':
			$image_path = 'http://s3.getmore.cc/' . $type . '/' . $file_id . '/' . $size . '.jpg';
			break;
		case 'banner':
		case 'blog':
			$image_path = 'http://s3.getmore.cc/' . $type . '/' . $file_id . '.jpg';
			break;
		default:
			$image_path = 'http://s3.getmore.cc/white.gif';
			break;
		}

		return $image_path;
	}
}

if (!function_exists('object_to_array')) {
	function object_to_array($object) {
		return (array) $object;
	}
}

if (!function_exists('product_type')) {
	function product_type($type) {
		$ci = &get_instance();
		$ci->load->spark('curl');

		$product_type = (array) json_decode($ci->curl->simple_get('/restful/product_clothing/product_type/'));

		foreach ($product_type as $key => $value) {
			if ($key === $type) {
				return $value;
			}
		}
	}
}

if (!function_exists('get_price_range')) {
	function get_price_range($price) {
		$ci = &get_instance();
		$ci->load->spark('curl');

		$price_range = (array) json_decode($ci->curl->simple_get('/restful/product_clothing/product_original_price/'));

		foreach ($price_range as $key => $value) {
			if ((int) $key === $price) {
				return $value;
			}
		}
	}
}

if (!function_exists('get_price_range')) {
	function price_area() {

		$html = '<option value="1000">500元~1,000元</option>';

		for ($price = 1000; $price < 10000; $price += 1000) {
			$html .= '<option value="' . ($price + 1000) . '">' . number_format($price + 1) . '元~' . number_format($price + 1000) . '元</option>';
		}

		for ($price = 10000; $price < 50000; $price += 5000) {
			$html .= '<option value="' . ($price + 5000) . '">' . number_format($price + 1) . '元~' . number_format($price + 5000) . '元</option>';
		}

		for ($price = 50000; $price < 100000; $price += 10000) {
			$html .= '<option value="' . ($price + 10000) . '">' . number_format($price + 1) . '元~' . number_format($price + 10000) . '元</option>';
		}

		$html .= '<option value="999999">超過100,000元</option>';

		return $html;
	}
}

if (!function_exists('Breadcrumb')) {
	function Breadcrumb($prodyct_type) {
		$str = '';
		$gender = strval($prodyct_type)[0];

		switch ($gender) {
		case '1':
			$str .= '<a href="/clothing/dsnerview?type=women">WOMEN</a> > ';
			break;
		case '2';
			$str .= '<a href="/clothing/dsnerview?type=men">MEN</a> > ';
			break;
		case '3':
			$str .= '<a href="/clothing/dsnerview?type=kids">KIDS</a> > ';
			break;
		}

		$ci = &get_instance();
		$ci->load->config('clothing_class');

		$str .= $ci->config->item('class_name')[$prodyct_type];

		return $str;
	}
}