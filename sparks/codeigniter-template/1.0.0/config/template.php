<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * default layout
 * Location: application/views/
 */
$config['template_layout'] = 'template/layout';

/**
 * default css
 */
$config['template_css'] = array(
);

/**
 * default javascript
 * load javascript on header: FALSE
 * load javascript on footer: TRUE
 * example:
 * '/assets/javascript/libs/jquery/jquery.min.js' => false
 */
$config['template_js'] = array(
);

/**
 * default variable
 */
$config['template_vars'] = array(
    'site_description'  => 'GetMore 是亞洲第一個二手衣時尚買賣網站，提供穿二手衣的生活新理念，輕鬆享受二手衣獨特、環保、時尚的二次生活。',
    'site_keywords'     => '時尚,衣著,服飾,時裝,流行,褲子,買二手衣,賣二手衣,環保,電子商務,startup,二次時尚'
);

/**
 * default site title
 */
$config['base_title'] = 'GetMore - 二手衣時尚購物網站/綠色時尚/買二手衣/賣二手衣';

/**
 * default title separator
 */
$config['title_separator'] = ' | ';
