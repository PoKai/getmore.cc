
.PHONY: all admin

all:
	@git submodule init
	@git submodule update
	@test -f .htaccess || cp -r .htaccess.ubuntu .htaccess
	@test -f application/config/database.php || cp -r application/config/database.php.sample application/config/database.php
	@test -f application/config/images.php || cp -r application/config/images.php.sample application/config/images.php
	@test -f application/config/facebook.php || cp -r application/config/facebook.php.sample application/config/facebook.php
	@bower install
	@echo
	@echo "Please modified database.php, images.php and facebook.php files"
	@echo

template:
	cd admin && handlebars assets/templates/*.handlebars -m -f assets/templates/template.js -k each -k if -k unless
	#handlebars assets/templates/*.handlebars -m -f assets/templates/template.js -k each -k if -k unless

admin: template
	cd admin && node ../assets/javascript/r.js -o name=main out=assets/javascript/main-built.js baseUrl=assets/javascript/ paths.order="libs/require/order"
	#cd admin && r.js -o name=main out=assets/javascript/main-built.js baseUrl=assets/javascript/ paths.order="libs/require/order"

front: template
	node assets/javascript/r.js -o assets/javascript/app.build.js
	#r.js -o assets/javascript/app.build.js
