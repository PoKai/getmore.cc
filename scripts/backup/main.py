import urllib2, json, os, urllib
var_from = raw_input('from:')
var_to = raw_input('to:')

response = urllib2.urlopen(
    'http://localhost/backup/clothing_files?' + 
    'token=getmorebackup' + 
    '&from=' + var_from + '&to=' + var_to)
html = response.read()
response.close()

file_names = json.loads(html)

if not os.path.exists('upload'):
    os.makedirs('upload')

for name in file_names:
    url = 'http://localhost/upload/' + name
    if os.path.isfile('upload/' + name):
        continue
    try:
        resp = urllib2.urlopen(url)
        f = open('upload/' + name, 'wb')
        f.write(resp.read())
        f.close()
        print 'download one image successfully.'
    except urllib2.HTTPError:
        pass

print('scanned ' + str(len(file_names)) + ' files.')
