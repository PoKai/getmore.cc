import urllib2, json, os, urllib

def download_thumbs(name):
    pre_fix = ['200_300_', '250_375_', '800_1200_']

    for pre in pre_fix:
        url = 'http://localhost/upload/thumbs/' + pre + name
        if os.path.isfile('upload/thumbs/' + pre + name):
            continue
        try:
            resp = urllib2.urlopen(url)
            f = open('upload/thumbs/' + pre + name, 'wb')
            f.write(resp.read())
            f.close()
            print 'download one image successfully.'
        except urllib2.HTTPError:
            pass

var_from = raw_input('from:')
var_to = raw_input('to:')

response = urllib2.urlopen(
    'http://localhost/backup/clothing_files?' + 
    'token=getmorebackup' + 
    '&from=' + var_from + '&to=' + var_to)
html = response.read()
response.close()

file_names = json.loads(html)

if not os.path.exists('upload/thumbs'):
    os.makedirs('upload/thumbs')

for name in file_names:
    download_thumbs(name)
print('scanned ' + str(len(file_names)) + ' files.')

