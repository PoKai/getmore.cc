import re

def parse_size_code(line):
    result = ''
    for char in line:
        if char not in ['<', ' ', '>', '\n']:
            result += char
    return result

def parse_part_code(line):
    m=re.search('(\w+) (\w*)',line)
    return m.group(1)

def parse_part_name(line):
    m=re.search('\w+\s(\S*)',line)
    return m.group(1)

result = []
lines = tuple(open('raw', 'r'))
index = -1
for line in lines:
    if line[0] == '<':
        key = parse_size_code(line)
        result.append({ 'code': key, 'labels': [] })
        index = index + 1
    else:
        code = parse_part_code(line)
        label = parse_part_name(line)
        result[index]['labels'].append({ 'label': label, 'code': code })

print result

import json

f = open('json','w')
json.dump(result, f)
f.close()
