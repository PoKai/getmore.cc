// Filename: main.js

require({
  paths: {
    loader: 'loader',
    jquery: 'libs/jquery/jquery',
    underscore: 'libs/underscore/underscore',
    backbone: 'libs/backbone/backbone',
    // Require.js plugins
    text: 'libs/require/text',
    order: 'libs/require/order',
    templates: '../templates'
  }
},
['require', 'loader'], function(require, loader) {
    require(['app'], function(App){
      App.initialize();
    });
  }
);
