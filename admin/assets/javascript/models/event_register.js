/*
 * Event Register Model
 */
define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone, CollectionEvents){
    RT.ModelEventRegister = Backbone.Model.extend({

        initialize: function() {

        },

        url: function() {
            return RT.API.getEventRegisters + '?id=' + this.id;
        },

        parse: function(response) {
            var is_edit = true;
            $.extend(response.item, {"is_edit": is_edit});
            return response.item;
        }
    });
    return RT.ModelEventRegister;
});
