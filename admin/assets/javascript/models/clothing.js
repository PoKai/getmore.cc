/*
 * Clothing Model
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'collections/clothing'
    ], function($, _, Backbone, CollectionClothing) {
    RT.ModelClothing = Backbone.Model.extend({

        initialize: function() {
            this.lists = new CollectionClothing();
            this.lists.url = RT.API.getAllClothing;
        },
        
        product_item_number: '',
        
        url: function() {
            return "/api/clothing/getOneByProductItemNumber" + '?product_item_number=' + this.product_item_number;
        },           
        
        parse: function(response){
            return response.item;
        }        
        
    });
    return RT.ModelClothing;
});
