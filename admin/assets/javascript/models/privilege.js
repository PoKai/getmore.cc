/*
 * A user
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'collections/privileges'
    ], function($, _, Backbone, CollectionPrivileges) {
    RT.ModelPrivilege = Backbone.Model.extend({

        initialize: function() {
            this.lists = new CollectionPrivileges;
            this.lists.url = RT.API.getAcls;
        },

        url: function() {
            return RT.API.getAcls;
        },

        parse: function(response) {
            return response;
        },
        /*
      save: function(attributes) {
        // Backbone.Model.save() sends the complete representation to the server, 
        // which we don't support, so we have to override it.
        var model = this;
        _.each(attributes, function (value, key) {
          model.set(key, value);
        });
        $.ajax({
          type: 'PUT',
          url: this.url(),
          data: attributes,
          success: function(response) {
            model.change();
          }
        });
      }
      */
    });
    return RT.ModelPrivilege;
});