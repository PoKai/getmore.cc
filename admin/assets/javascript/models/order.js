/*
 * Order Model
 */
define([
  'jquery',
  'underscore',
  'backbone',
  'collections/orders',
  'collections/status'
], function($, _, Backbone, CollectionOrders, CollectionStatus){
    RT.ModelProduct = Backbone.Model.extend({

        initialize: function() {
            this.lists = new CollectionOrders;
            this.lists.url = RT.API.getOrders;
            this.status = new CollectionStatus;
            this.status.url = RT.API.getOrderStatus;
        },

        set_params: function(params) {
            this.lists.url = RT.API.getOrders + '?' + $.param(params);
        },

        url: function() {
            return RT.API.getOrder + '?id=' + this.id;
        },

        parse: function(response) {
            var is_edit = true;
            var edit_time = moment.unix(response.item.edit_time).format('YYYY-MM-DD HH:mm:ss');
            $.extend(response.item, {"is_edit": is_edit, "edit_time": edit_time});
            return response.item;
        }
    });
    return RT.ModelProduct;
});
