/*
 * Category Model
 */
define([
  'jquery',
  'underscore',
  'backbone',
  'collections/categories'
], function($, _, Backbone, CollectionCategories){
    RT.ModelCategory = Backbone.Model.extend({

        initialize: function() {
            this.lists = new CollectionCategories;
            this.lists.url = RT.API.getCategories + '?type=' + this.get('type');
        },

        set_params: function(params) {
            this.lists.url = RT.API.getCategories + '?' + $.param(params);
        },

        url: function() {
            return RT.API.getCategory + '?id=' + this.id;
        },

        parse: function(response) {
            var is_edit = true;
            var edit_time = moment.unix(response.item.edit_time).format('YYYY-MM-DD HH:mm:ss');
            $.extend(response.item, {"is_edit": is_edit, "edit_time": edit_time});
            return response.item;
        }
    });
    return RT.ModelCategory;
});
