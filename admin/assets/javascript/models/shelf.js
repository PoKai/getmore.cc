/*
 * Config Model
 */
define([
    'jquery',
    'underscore',
    'backbone'
    ], function($, _, Backbone) {
    RT.ModelShelf = Backbone.Model.extend({

        initialize: function() {},

        url: function() {
            return RT.API.getConfigs;
        },

        parse: function(response) {
            var is_edit = true;
            $.extend(response.item, {
                "is_edit": is_edit
            });
            return response.item;
        }
/*
	pleace_item_info: function(itemData){
	    console.log(itemData);
	}
*/
    });
    return RT.ModelShelf;
});
