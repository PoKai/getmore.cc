/*
 * Event Model
 */
define([
  'jquery',
  'underscore',
  'backbone',
  'collections/events'
], function($, _, Backbone, CollectionEvents){
    RT.ModelEvent = Backbone.Model.extend({

        initialize: function() {
            this.lists = new CollectionEvents;
            this.lists.url = RT.API.getEvents;
        },

        set_params: function(params) {
            this.lists.url = RT.API.getEvents + '?' + $.param(params);
        },

        url: function() {
            return RT.API.getEvent + '?id=' + this.id;
        },

        parse: function(response) {
            var is_edit = true;
            var edit_time = moment.unix(response.item.edit_time).format('YYYY-MM-DD HH:mm:ss');
            $.extend(response.item, {"is_edit": is_edit, "edit_time": edit_time});
            return response.item;
        }
    });
    return RT.ModelEvent;
});
