/*
 * A user
 */
define([
  'jquery',
  'underscore',
  'backbone',
  'collections/users'
], function($, _, Backbone, CollectionUsers){
    RT.ModelUser = Backbone.Model.extend({

        initialize: function() {
            this.lists = new CollectionUsers();
            this.lists.url = RT.API.getUsers;
        },

        set_params: function(params) {
            this.lists.url = RT.API.getUsers + '?' + $.param(params);
        },

        url: function() {
            return RT.API.getUser + '?id=' + this.id;
        },
        
        parse: function(response) {
            var is_edit = true;
            var edit_time = moment.unix(response.item.edit_time).format('YYYY-MM-DD HH:mm:ss');
            $.extend(response.item, {"is_edit": is_edit, "edit_time": edit_time});
            return response.item;
        }
    });
    return RT.ModelUser;
});
