/*
 * Subject Model
 */
define([
  'jquery',
  'underscore',
  'backbone',
  'collections/subjects'
], function($, _, Backbone, CollectionSubjects){
    RT.ModelSubject = Backbone.Model.extend({

        initialize: function() {
            this.lists = new CollectionSubjects();
            this.lists.url = RT.API.getSubjects;
            // for product menu options
            this.menus = new CollectionSubjects();
            this.menus.url = RT.API.getSubjectMenus;
        },

        set_params: function(params) {
            this.lists.url = RT.API.getSubjects + '?' + $.param(params);
            this.menus.url = RT.API.getSubjects + '?' + $.param(params);
        },

        url: function() {
            return RT.API.getSubject + '?id=' + this.id;
        },

        parse: function(response) {
            var is_edit = true;
            var edit_time = moment.unix(response.item.edit_time).format('YYYY-MM-DD HH:mm:ss');
            $.extend(response.item, {"is_edit": is_edit, "edit_time": edit_time});
            return response.item;
        }
    });
    return RT.ModelSubject;
});
