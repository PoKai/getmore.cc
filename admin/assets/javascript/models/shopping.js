/*
 * Shopping Model
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'collections/shopping'
    ], function($, _, Backbone, CollectionShopping) {
    RT.ModelShopping = Backbone.Model.extend({

        initialize: function() {
            this.lists = new CollectionShopping();
            this.lists.url = RT.API.getShopping;
        },

        set_params: function(params) {
            this.lists.url = RT.API.getShopping + '?' + $.param(params);
        },

        url: function() {
            return RT.API.getShopping + '?id=' + this.id;
        },

        parse: function(response) {
            var is_edit = true,
                edit_time = moment.unix(response.item.edit_time).format('YYYY-MM-DD HH:mm:ss'),
                add_time = moment.unix(response.item.edit_time).format('YYYY-MM-DD HH:mm:ss');
            $.extend(response.item, {
                'is_edit': is_edit,
                'add_time': add_time,
                'edit_time': edit_time
            });
            return response.item;
        }
    });
    return RT.ModelShopping;
});
