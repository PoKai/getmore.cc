/*
 * Static URL Model
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'collections/static_url'
    ], function($, _, Backbone, CollectionStaticURLs) {
    RT.ModelStaticUrl = Backbone.Model.extend({

        initialize: function() {
            this.lists = new CollectionStaticURLs();
            this.lists.url = RT.API.getStaticUrls;
        },

        set_params: function(params) {
            this.lists.url = RT.API.getStaticUrls + '?' + $.param(params);
        },

        url: function() {
            return RT.API.getStaticUrl + '?id=' + this.id;
        },

        parse: function(response) {
            var is_edit = true,
                edit_time = moment.unix(response.item.edit_time).format('YYYY-MM-DD HH:mm:ss'),
                add_time = moment.unix(response.item.edit_time).format('YYYY-MM-DD HH:mm:ss');
            $.extend(response.item, {
                'is_edit': is_edit,
                'add_time': add_time,
                'edit_time': edit_time
            });
            return response.item;
        }
    });
    return RT.ModelStaticUrl;
});