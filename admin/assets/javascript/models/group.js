/*
 * Group Model
 */
define([
  'jquery',
  'underscore',
  'backbone',
  'collections/groups'
], function($, _, Backbone, CollectionGroups){
    RT.ModelGroup = Backbone.Model.extend({

        initialize: function() {
            this.lists = new RT.CollectionGroups;
            this.lists.url = RT.API.getGroups;
        },

        url: function() {
            return RT.API.getGroup + '?id=' + this.id;
        },

        parse: function(response) {
            var is_edit = true;
            var data = $.extend(response.item,{"is_edit": is_edit});
            return data;
        },
      /*
      save: function(attributes) {
        // Backbone.Model.save() sends the complete representation to the server,
        // which we don't support, so we have to override it.
        var model = this;
        _.each(attributes, function (value, key) {
          model.set(key, value);
        });
        $.ajax({
          type: 'PUT',
          url: this.url(),
          data: attributes,
          success: function(response) {
            model.change();
          }
        });
      }
      */
    });
    return RT.ModelGroup;
});
