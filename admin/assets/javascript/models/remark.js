/*
 * Category Model
 */
define([
  'jquery',
  'underscore',
  'backbone',
  'collections/remarks'
], function($, _, Backbone, CollectionRemarks){
    RT.ModelRemark = Backbone.Model.extend({

        initialize: function() {
            this.lists = new CollectionRemarks;
            this.lists.url = RT.API.getRemarks + '?type=' + this.get('type') + '&r_id=' + this.get('r_id');
        },

        url: function() {
            return RT.API.getRemark+ '?id=' + this.id;
        },

        parse: function(response) {
            var is_edit = true;
            $.extend(response.item, {"is_edit": is_edit});
            return response.item;
        }
    });
    return RT.ModelRemark;
});
