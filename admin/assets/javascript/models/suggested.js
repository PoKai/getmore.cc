/*
 * Clothing Model
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'collections/suggested'
    ], function($, _, Backbone, CollectionSuggested) {
    RT.ModelSuggested = Backbone.Model.extend({

        initialize: function() {
            this.lists = new CollectionSuggested();
            this.lists.url = "/api/suggested/getAll";
        },
                
    });
    return RT.ModelSuggested;
});
