/*
 * Product Model
 */
define([
  'jquery',
  'underscore',
  'backbone',
  'collections/products'
], function($, _, Backbone, CollectionProducts){
    RT.ModelProduct = Backbone.Model.extend({

        initialize: function() {
            this.lists = new CollectionProducts;
            this.lists.url = RT.API.getProducts;
        },

        set_params: function(params) {
            this.lists.url = RT.API.getProducts + '?' + $.param(params);
        },

        url: function() {
            return RT.API.getProduct + '?id=' + this.id;
        },

        parse: function(response) {
            var is_edit = true;
            var edit_time = moment.unix(response.item.edit_time).format('YYYY-MM-DD HH:mm:ss');
            $.extend(response.item, {"is_edit": is_edit, "edit_time": edit_time});
            return response.item;
        }
    });
    return RT.ModelProduct;
});
