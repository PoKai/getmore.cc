/*
 * Clothing Model
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'collections/buyorder'
    ], function($, _, Backbone, CollectionBuyorder) {
    RT.ModelBuyorder = Backbone.Model.extend({

        initialize: function() {
            this.lists = new CollectionBuyorder();
            // this.lists.url = RT.API.getAllClothing;
            this.lists.basicUrl = "/api/buyorder/getAll";
        },
        
        url: function() {
            return "/api/buyorder/getOneByOrderNumber" + '?order_number=' + this.order_number;
        },        
        
        
        order_number: '',
        
        parse: function(response){
            return response.item;
        }
        
    });
    return RT.ModelBuyorder;
});
