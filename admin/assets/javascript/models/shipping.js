/*
 * Shipping Model
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'collections/shipping'
    ], function($, _, Backbone, CollectionShipping) {
    RT.ModelShipping = Backbone.Model.extend({

        initialize: function() {
            this.lists = new CollectionShipping();
            this.lists.url = RT.API.getShipping;
        },

        set_params: function(params) {
            this.lists.url = RT.API.getShipping + '?' + $.param(params);
        },

        url: function() {
            return RT.API.getShipping + '?id=' + this.id;
        },

        parse: function(response) {
            var is_edit = true,
                edit_time = moment.unix(response.item.edit_time).format('YYYY-MM-DD HH:mm:ss'),
                add_time = moment.unix(response.item.edit_time).format('YYYY-MM-DD HH:mm:ss');
            $.extend(response.item, {
                'is_edit': is_edit,
                'add_time': add_time,
                'edit_time': edit_time
            });
            return response.item;
        }
    });
    return RT.ModelShipping;
});
