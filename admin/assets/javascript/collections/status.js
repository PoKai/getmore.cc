/*
 * Order Status Collection
 */
define([
  'jquery',
  'underscore',
  'backbone'
], function($, _, Backbone){
    RT.CollectionStatus = Backbone.Collection.extend({
        parse: function(response) {
            return response.items;
        }
    });
    return RT.CollectionStatus;
});
