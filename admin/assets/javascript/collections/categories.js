/*
 * Category List
 */
define([
  'jquery',
  'underscore',
  'backbone'
], function($, _, Backbone){
    RT.CollectionCategories = Backbone.Collection.extend({
        parse: function(response) {
            return response.item;
        }
    });
    return RT.CollectionCategories;
});
