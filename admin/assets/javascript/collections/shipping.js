/*
 * Shipping Collection
 */
define([
    'jquery',
    'underscore',
    'backbone'
    ], function($, _, Backbone) {
    RT.CollectionShipping = Backbone.Collection.extend({
        parse: function(response) {
            return response.items;
        }
    });
    return RT.CollectionShipping;
});
