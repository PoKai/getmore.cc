/*
 * Remark List
 */
define([
  'jquery',
  'underscore',
  'backbone'
], function($, _, Backbone){
    RT.CollectionRemarks = Backbone.Collection.extend({
        parse: function(response) {
            return response.item;
        }
    });
    return RT.CollectionRemarks;
});
