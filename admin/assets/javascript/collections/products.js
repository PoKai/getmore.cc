/*
 * Product Collection
 */
define([
  'jquery',
  'underscore',
  'backbone'
], function($, _, Backbone){
    RT.CollectionProducts = Backbone.Collection.extend({
        parse: function(response) {
            return response.items;
        }
    });
    return RT.CollectionProducts;
});
