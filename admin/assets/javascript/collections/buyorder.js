/*
 * Clothing Collection
 */
define([
    'jquery',
    'underscore',
    'backbone'
    ], function($, _, Backbone) {
    RT.CollectionBuyorder = Backbone.Collection.extend({
        parse: function(response) {
            return response.items;
        },
        setStatus: function(status){
            this.url = this.basicUrl + '/' + status;
            return this;
        }
    });
    return RT.CollectionBuyorder;
});
