/*
 * Static Url Collection
 */
define([
    'jquery',
    'underscore',
    'backbone'
    ], function($, _, Backbone) {
    RT.CollectionStaticURLs = Backbone.Collection.extend({
        parse: function(response) {
            return response.items;
        }
    });
    return RT.CollectionStaticURLs;
});