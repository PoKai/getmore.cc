/*
 * Shopping Collection
 */
define([
    'jquery',
    'underscore',
    'backbone'
    ], function($, _, Backbone) {
    RT.CollectionShopping = Backbone.Collection.extend({
        parse: function(response) {
            return response.items;
        }
    });
    return RT.CollectionShopping;
});
