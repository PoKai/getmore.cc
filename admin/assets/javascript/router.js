/*
 * Maps out the URLs in the app
 */

var unix_to_time = function(unixtime, hour) {
    hour = hour || 0;
    unixtime = parseInt(unixtime, 10) + (hour * 3600);
    date = new Date(unixtime * 1000);
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    month = (month.toString().length == 1) ? '0' + month : month;
    var day = (date.getDate().toString().length == 1) ? '0' + date.getDate() : date.getDate();
    var hours = (date.getHours().toString().length == 1) ? '0' + date.getHours() : date.getHours();
    var minutes = (date.getMinutes().toString().length == 1) ? '0' + date.getMinutes() : date.getMinutes();
    var seconds = (date.getSeconds().toString().length == 1) ? '0' + date.getSeconds() : date.getSeconds();

    var formattedTime = year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;
    return formattedTime;
};

/*
 * Flags
 */
RT.Flags = {
    _IS_INDEX: 0x01,
    _IS_ENABLED: 0x02,
    _IS_UP: 0x04,
    _IS_ACTIVE: 0x08
};

/*
 * API List
 */
RT.API = {
    'me': root_path + '/API/Users/me',
    'getGroups': root_path + '/API/Users/getGroups',
    'getGroup': root_path + '/API/Users/getGroup',
    'groupAdd': root_path + '/API/Users/groupAdd',
    'groupEdit': root_path + '/API/Users/groupEdit',
    'groupDelete': root_path + '/API/Users/groupDelete',
    'getUsers': root_path + '/API/Users/getUsers',
    'getUser': root_path + '/API/Users/getUser',
    'userAdd': root_path + '/API/Users/userAdd',
    'userEdit': root_path + '/API/Users/userEdit',
    'userDelete': root_path + '/API/Users/userDelete',
    'getAcls': root_path + '/API/Acl/getAcls',
    'aclAdd': root_path + '/API/Acl/aclAdd',
    'aclEdit': root_path + '/API/Acl/aclEdit',
    'aclDelete': root_path + '/API/Acl/aclDelete',
    'getCategory': root_path + '/API/Category/getCategory',
    'getCategories': root_path + '/API/Category/getCategories',
    'categoryAdd': root_path + '/API/Category/categoryAdd',
    'categoryDelete': root_path + '/API/Category/categoryDelete',
    'categoryEdit': root_path + '/API/Category/categoryEdit',
    'getRemark': root_path + '/API/Remark/getRemark',
    'getRemarks': root_path + '/API/Remark/getRemarks',
    'remarkAdd': root_path + '/API/Remark/remarkAdd',
    'remarkDelete': root_path + '/API/Remark/remarkDelete',
    'remarkEdit': root_path + '/API/Remark/remarkEdit',
    'getSubject': root_path + '/API/Subject/getSubject',
    'getSubjects': root_path + '/API/Subject/getSubjects',
    'getSubjectMenus': root_path + '/API/Subject/getSubjectMenus',
    'subjectAdd': root_path + '/API/Subject/subjectAdd',
    'subjectDelete': root_path + '/API/Subject/subjectDelete',
    'subjectEdit': root_path + '/API/Subject/subjectEdit',
    'getProduct': root_path + '/API/Product/getProduct',
    'getProducts': root_path + '/API/Product/getProducts',
    'productAdd': root_path + '/API/Product/productAdd',
    'productDelete': root_path + '/API/Product/productDelete',
    'productEdit': root_path + '/API/Product/productEdit',
    'getEvent': root_path + '/API/Event/getEvent',
    'getEvents': root_path + '/API/Event/getEvents',
    'getEventRegisters': root_path + '/API/Event/getEventRegisters',
    'eventAdd': root_path + '/API/Event/eventAdd',
    'eventDelete': root_path + '/API/Event/eventDelete',
    'eventEdit': root_path + '/API/Event/eventEdit',
    'fileDelete': root_path + '/files/ajax',
    'getConfigs': root_path + '/API/Config/getConfigs',
    'configEdit': root_path + '/API/Config/configEdit',
    'getOrder': root_path + '/API/Order/getOrder',
    'getOrders': root_path + '/API/Order/getOrders',
    'getOrderStatus': root_path + '/API/Order/getOrderStatus',
    'orderEdit': root_path + '/API/Order/orderEdit',
    'orderDeliveryNumber': root_path + '/API/Order/orderDeliveryNumber',
    'getStaticUrl': root_path + '/API/Url/getUrl',
    'getStaticUrls': root_path + '/API/Url/getUrls',
    'urlAdd': root_path + '/API/Url/urlAdd',
    'urlDelete': root_path + '/API/Url/urlDelete',
    'urlEdit': root_path + '/API/Url/urlEdit',
    'getShipping': root_path + '/API/Shipping/getShipping',
    'getShippingItems': root_path + '/API/Shipping/getShippingItems',
    'getShopping': root_path + '/API/Shipping/getShopping',
    'userinfoEdit': root_path + '/API/Shipping/userinfoEdit',
    'iteminfoEdit': root_path + '/API/Shipping/iteminfoEdit',
    'setShippingStatus': root_path + '/API/Shipping/setShippingStatus',
    'generateXML': root_path + '/API/Shipping/generateXML',
    'delShippingItems': root_path + '/API/Shipping/delShippingItems',
    'getClothingSetting': root_path + '/API/Shipping/getClothingSetting',
    'addClothingitem': root_path + '/API/Shipping/addClothingItem',
    'getAllClothing': root_path + '/api/clothing/getAll',
    'getDraftClothing': root_path + '/api/clothing/getDraft',
    'launchDraftClothing': root_path + '/api/clothing/launchDraft'
};

define([
    'jquery',
    'underscore',
    'backbone',
    // define Model
    'models/me',
    'models/user',
    'models/privilege',
    'models/group',
    'models/category',
    'models/subject',
    'models/product',
    'models/event',
    'models/event_register',
    'models/config',
    'models/order',
    'models/static_url',
    'models/shipping',
    'models/shopping',
    'models/shelf',
    'models/clothing',
    'models/buyorder',
    'models/suggested',
    // define View
    'views/view',
    'views/users/list',
    'views/users/edit',
    'views/categories/list',
    'views/groups/list',
    'views/privileges/list',
    'views/subjects/list',
    'views/subjects/edit',
    'views/subjects/add',
    'views/products/list',
    'views/products/edit',
    'views/products/add',
    'views/events/list',
    'views/events/edit',
    'views/events/add',
    'views/events/register',
    'views/configs/edit',
    'views/orders/list',
    'views/orders/edit',
    'views/static_urls/list',
    'views/static_urls/edit',
    'views/shipping/list',
    'views/shopping/list',
    'views/shelf/scan',
    'views/clothing/clothing',
    'views/clothing/list',
    'views/clothing/draft',
    'views/clothing/edit',
    'views/suggested/index',
    'views/buyorder/list',
    'views/buyorder/edit',
    'views/vendor/vendor',
    'views/sellcoupon/sellcoupon',
    'views/account/account',
    'libs/jquery/jquery-ui-timepicker-addon',
    'libs/moment/moment.min'
    ], function($, _, Backbone, ModelMe, ModelUser, ModelPrivilege, ModelGroup, ModelCategory, ModelSubject, ModelProduct, ModelEvent, ModelEventRegister, ModelConfig, ModelOrder, ModelStaticUrl, ModelShipping, ModelShopping,  ModelShelf, ModelClothing, ModelBuyorder, ModelSuggested, View, ViewUsersList, ViewUser, ViewCategories, ViewGroups, ViewPrivileges, ViewSubjectsList, ViewSubject, ViewSubjectAdd, ViewProductsList, ViewProduct, ViewProductAdd, ViewEventsList, ViewEvent, ViewEventAdd, ViewEventRegister, ViewConfig, ViewOrdersList, ViewOrder, ViewStaticUrls, ViewStaticUrl, ViewShipping, ViewShopping, ViewShelf, ViewClothing, ViewClothingList, ViewDraftList, ViewClothingEdit, ViewSuggested, ViewBuyorder, ViewBuyorderEdit, ViewVendor, ViewSellcoupon, ViewAccount /*, */) {
    var AppRouter = Backbone.Router.extend({
    
        site_name: 'getmore.cc 後台管理',

        routes: {
            '': 'home',
            '!/': 'home',
            '!/home': 'home',
            '!/user/:action': 'user',
            '!/user/:action/:id': 'user',
            '!/group/:action': 'group',
            '!/group/:action/:id': 'group',
            '!/privilege/:action': 'privilege',
            '!/privilege/:action/:id': 'privilege',
            '!/category/:action': 'category',
            '!/category/:action/:id': 'category',
            '!/subject/:action': 'subject',
            '!/subject/:action/:id': 'subject',
            '!/product/:action': 'product',
            '!/product/:action/:id': 'product',
            '!/event/:action': 'event',
            '!/event/:action/:id': 'event',
            '!/config/:action': 'config',
            '!/order/:action': 'order',
            '!/order/:action/:id': 'order',
            '!/static_url/:action': 'static_url',
            '!/static_url/:action/:id': 'static_url',
            '!/shipping/:action': 'shipping',
            '!/shipping/:action/:id': 'shipping',
            '!/shopping/:action': 'shopping',
            '!/shelf/:action': 'shelf',
            '!/clothing/:action': 'clothing',
            '!/clothing/:action/:id': 'clothing',
            '!/buyorder/:action': 'buyorder',
            '!/buyorder/:action/:status_id': 'buyorder',
            '!/suggested/index': 'suggested',
            '!/physical_selling/:action': 'physical_selling',            
            '!/physical_selling/:action/:id': 'physical_selling',            
            '!/account/:action': 'account',            
            '!/account/:action/:id': 'account',            
            '!/withdraw/:action': 'withdraw',            
            '!/other/:action': 'other', 
            '!/sys_config': 'sys_config',
            '!/sms_handle': 'sms_handle', 
            '!/coupon/:action': 'coupon',
            '!/coupon/:action/:id': 'coupon',
            '!/vendor/:action': 'vendor',
            '!/vendor/:action/:id': 'vendor',
            '!/sellcoupon/:action': 'sellcoupon',
            '!/sellcoupon/:action/:id': 'sellcoupon',             
            '!/article/:action': 'article',
            '!/article/:action/:id': 'article',
            '!/brand/:action': 'brand',            
            '!/return_request/:action': 'return_request',
            '!/report/:action': 'report'
        },

        initialize: function() {
            this.me = new ModelMe();
            this.me.on("change", this.update_user, this);
            this.me.fetch();

            // load model
            if (!this.subject_model) {
                this.subject_model = new ModelSubject();
            }

            if (!this.category_model) {
                this.category_model = new ModelCategory();
            }

            if (!this.config_model) {
                this.config_model = new ModelConfig();
            }

            if (!this.privilege_model) {
                this.privilege_model = new ModelPrivilege();
            }

            if (!this.event_model) {
                this.event_model = new ModelEvent();
            }

            if (!this.event_register_model) {
                this.event_register_model = new ModelEventRegister();
            }

            if (!this.product_model) {
                this.product_model = new ModelProduct();
            }

            if (!this.user_model) {
                this.user_model = new ModelUser();
            }

            if (!this.order_model) {
                this.order_model = new ModelOrder();
            }

            if (!this.group_model) {
                this.group_model = new ModelGroup();
            }

            if (!this.static_url_model) {
                this.static_url_model = new ModelStaticUrl();
            }

            if (!this.shipping_model) {
                this.shipping_model = new ModelShipping();
            }

            if (!this.shopping_model) {
                this.shopping_model = new ModelShopping();
            }

            if (!this.shelf_model) {
                this.shelf_model = new ModelShelf();
            }

            if (!this.clothing_model) {
                this.clothing_model = new ModelClothing();
            }

            if (!this.buyorder_model) {
                this.buyorder_model = new ModelBuyorder();
            }

            if (!this.suggested_model) {
                this.suggested_model = new ModelSuggested();
            }

        },

        update_title: function(title) {
            if (title) {
                document.title = this.site_name + '::' + title;
                $(".section_title").text(title);
            } else {
                document.title = this.site_name;
                $(".section_title").text('');
            }
        },

        category: function(action, id) {
            RT.dialogs.loading('open');
            $("#main").html("");
            var data = {},
                title;
            switch (id) {
            case 'product':
                title = '產品';
                data = {
                    type: 'product',
                    title: title,
                    is_add: true
                };
                break;
            case 'activity':
                title = '活動';
                data = {
                    type: 'activity',
                    title: title,
                    is_add: true
                };
                break;
            }

            // set type
            this.category_model.set_params({
                'type': id
            });

            switch (action) {
            case 'list':
                this.update_title(title + '分類列表');

                if (!this.view_category_list) {
                    this.view_category_list = new ViewCategories({
                        el: '#main',
                        collection: this.category_model.lists,
                        model_name: this.category_model
                    });
                }

                this.view_category_list.options.data = data;
                this.view_category_list.render();
                break;
            case 'add':
                this.update_title('新增' + title + '主題分類');

                if (!this.view_category_add) {
                    this.view_category_add = new View({
                        template_name: 'category_edit',
                        el: '#main'
                    });
                }

                this.view_category_add.options.data = data;
                this.view_category_add.render();
                break;
            case 'edit':
                this.update_title('修改主題分類');

                if (!this.view_category) {
                    this.view_category = new View({
                        template_name: 'category_edit',
                        el: '#main',
                        model: this.category_model
                    });
                } else {
                    // trigger change event if model is not changed
                    if (!this.category_model.hasChanged()) {
                        this.category_model.trigger('change');
                    }
                }

                this.category_model.id = id;
                this.category_model.fetch();
                break;
            }
        },

        config: function(action, id) {
            RT.dialogs.loading('open');
            $("#main").html("");

            switch (action) {
            case 'list':
                this.update_title('系統設定');

                if (!this.view_config) {
                    this.view_config = new ViewConfig({
                        el: '#main',
                        model: this.config_model
                    });
                } else {
                    // trigger change event if model is not changed
                    if (!this.config_model.hasChanged()) {
                        this.config_model.trigger('change');
                    }
                }

                this.config_model.fetch();
                break;
            }
        },

	shelf: function(action, id) {
            RT.dialogs.loading('open');
            $("#main").html("");

            switch (action) {
            case 'scan':
                this.update_title('商品上架');

                if (!this.view_shelf) {
                    this.view_shelf = new ViewShelf({
                        el: '#main',
                        model: this.shelf_model
                    });
                } else {
                    // trigger change event if model is not changed
                    if (!this.shelf_model.hasChanged()) {
                        this.shelf_model.trigger('change');
                    }
                }

                this.shelf_model.fetch();
                break;
            }
        },

        privilege: function(action, id) {
            RT.dialogs.loading('open');
            $("#main").html("");
            switch (action) {
            case 'list':
                this.update_title('權限列表');

                if (!this.view_privilege_list) {
                    this.view_privilege_list = new ViewPrivileges({
                        el: '#main',
                        collection: this.privilege_model.lists
                    });
                }

                this.privilege_model.lists.fetch();
                break;
            case 'add':
                this.update_title('新增權限');

                if (!this.view_privilege) {
                    this.view_privilege = new View({
                        template_name: 'privilege_edit',
                        el: '#main',
                        model: this.privilege_model
                    });
                } else {
                    // trigger change event if model is not changed
                    if (!this.privilege_model.hasChanged()) {
                        this.privilege_model.trigger('change');
                    }
                }

                this.privilege_model.fetch();
                break;
            }
        },

        subject: function(action, id) {
            RT.dialogs.loading('open');
            $("#main").html("");

            // set type
            this.category_model.set_params({
                'type': 'product'
            });

            switch (action) {
            case 'list':
                this.update_title('產品主題');

                // new view object
                if (!this.view_subject_list) {
                    this.view_subject_list = new ViewSubjectsList({
                        el: '#main',
                        collection: this.subject_model.lists,
                        model_name: this.subject_model
                    });
                }

                this.subject_model.lists.fetch();
                break;
            case 'add':
                this.update_title('新增主題');

                // new view object
                if (!this.view_subject_add) {
                    this.view_subject_add = new ViewSubjectAdd({
                        el: '#main',
                        category_model: this.category_model.lists
                    });
                }

                // call render
                this.category_model.set_params({
                    type: 'product'
                });
                this.view_subject_add.render();

                break;
            case 'edit':
                this.update_title('修改主題');

                // new view object
                if (!this.view_subject) {
                    this.view_subject = new ViewSubject({
                        el: '#main',
                        model: this.subject_model,
                        category_model: this.category_model.lists
                    });
                }

                this.subject_model.id = id;
                this.subject_model.fetch();
                break;
            }
        },

        event: function(action, id) {
            RT.dialogs.loading('open');
            $("#main").html("");
            switch (action) {
            case 'list':
                this.update_title('活動列表');

                if (!this.view_event_list) {
                    this.view_event_list = new ViewEventsList({
                        el: '#main',
                        collection: this.event_model.lists,
                        model_name: this.event_model
                    });
                }

                this.event_model.lists.fetch();
                break;
            case 'add':
                this.update_title('新增活動');

                if (!this.view_event_add) {
                    this.view_event_add = new ViewEventAdd({
                        el: '#main',
                        category_collection: this.category_model.lists
                    });
                }

                this.category_model.set_params({
                    type: 'activity'
                });
                this.view_event_add.render();
                break;
            case 'edit':
                this.update_title('修改活動');

                if (!this.view_event) {
                    this.view_event = new ViewEvent({
                        el: '#main',
                        model: this.event_model,
                        category_collection: this.category_model.lists
                    });
                }

                this.category_model.set_params({
                    type: 'activity'
                });
                this.event_model.id = id;
                this.event_model.fetch();
                break;

                // show register list
            case 'register':
                this.update_title('活動學員');

                if (!this.view_event_register) {
                    this.view_event_register = new ViewEventRegister({
                        el: '#main',
                        model: this.event_register_model
                    });
                } else {
                    // trigger change event if model is not changed
                    if (!this.event_register_model.hasChanged()) {
                        this.event_register_model.trigger('change');
                    }
                }

                this.event_register_model.id = id;
                this.event_register_model.fetch();
                break;
            }
        },

        product: function(action, id) {
            RT.dialogs.loading('open');
            $("#main").html("");
            switch (action) {
            case 'list':
                this.update_title('產品列表');

                if (!this.view_product_list) {
                    this.view_product_list = new ViewProductsList({
                        el: '#main',
                        collection: this.product_model.lists,
                        model_name: this.product_model
                    });
                }

                this.product_model.lists.fetch();
                break;
            case 'add':
                this.update_title('新增產品');

                if (!this.view_product_add) {
                    this.view_product_add = new ViewProductAdd({
                        el: '#main',
                        subject_collection: this.subject_model.menus
                    });
                }

                this.view_product_add.render();
                break;
            case 'edit':
                this.update_title('修改產品');

                if (!this.view_product) {
                    this.view_product = new ViewProduct({
                        el: '#main',
                        model: this.product_model,
                        subject_collection: this.subject_model.menus
                    });
                }

                this.product_model.id = id;
                this.product_model.fetch();
                break;
            }
        },

        /*
         * Order controller
         * Order's list and single view
         *
         * @author appleboy
         */
        order: function(action, id) {
            RT.dialogs.loading('open');
            $("#main").html("");
            switch (action) {
            case 'list':
                this.update_title('訂單列表');

                if (!this.view_order_list) {
                    this.view_order_list = new ViewOrdersList({
                        el: '#main',
                        collection: this.order_model.lists,
                        model_name: this.order_model
                    });
                }

                this.order_model.lists.fetch();
                break;
            case 'edit':
                this.update_title('檢視訂單');

                if (!this.view_order) {
                    this.view_order = new ViewOrder({
                        el: '#main',
                        model: this.order_model,
                    });
                }

                this.order_model.id = id;
                this.order_model.fetch();
                break;
            }
        },

        user: function(action, id) {
            RT.dialogs.loading('open');
            $("#main").html("");
            switch (action) {
            case 'list':
                this.update_title('帳號列表');

                if (!this.view_user_list) {
                    this.view_user_list = new ViewUsersList({
                        el: '#main',
                        collection: this.user_model.lists,
                        model_name: this.user_model
                    });
                }

                this.user_model.lists.fetch();
                break;
            case 'add':
                this.update_title('新增帳號');

                if (!this.view_user_add) {
                    this.view_user_add = new View({
                        template_name: 'user_edit',
                        el: '#main'
                    });
                }

                this.view_user_add.render();
                break;
            case 'edit':
                this.update_title('修改帳號');

                if (!this.view_user_edit) {
                    this.view_user_edit = new ViewUser({
                        template_name: 'user_edit',
                        el: '#main',
                        model: this.user_model
                    });
                }

                this.user_model.id = id;
                this.user_model.fetch();
                break;
            }
        },

        group: function(action, id) {
            RT.dialogs.loading('open');
            $("#main").html("");
            switch (action) {
            case 'list':
                this.update_title('權限群組列表');

                if (!this.view_group_list) {
                    this.view_group_list = new ViewGroups({
                        el: '#main',
                        collection: this.group_model.lists
                    });
                }

                this.group_model.lists.fetch();
                break;

            case 'add':
                this.update_title('新增群組');

                if (!this.view_group_add) {
                    this.view_group_add = new View({
                        template_name: 'group_edit',
                        el: '#main'
                    });
                }

                this.view_group_add.render();
                break;
            case 'edit':
                this.update_title('修改群組');

                if (!this.view_group_edit) {
                    this.view_group_edit = new View({
                        template_name: 'group_edit',
                        el: '#main',
                        model: this.group_model
                    });
                } else {
                    // trigger change event if model is not changed
                    if (!this.group_model.hasChanged()) {
                        this.group_model.trigger('change');
                    }
                }

                this.group_model.id = id;
                this.group_model.fetch();
                break;
            }
        },

        static_url: function(action, id) {
            RT.dialogs.loading('open');
            $("#main").html("");
            switch (action) {
            case 'list':
                this.update_title('靜態頁面列表');

                if (!this.view_static_url_list) {
                    this.view_static_url_list = new ViewStaticUrls({
                        el: '#main',
                        collection: this.static_url_model.lists,
                        model_name: this.static_url_model
                    });
                }

                this.static_url_model.lists.fetch();
                break;

            case 'add':
                this.update_title('新增靜態頁面');

                if (!this.view_static_url_add) {
                    this.view_static_url_add = new View({
                        template_name: 'static_url_edit',
                        el: '#main'
                    });
                }

                this.view_static_url_add.render();
                break;
            case 'edit':
                this.update_title('修改靜態頁面');

                if (!this.view__static_url_edit) {
                    this.view__static_url_edit = new View({
                        template_name: 'static_url_edit',
                        el: '#main',
                        model: this.static_url_model
                    });
                } else {
                    // trigger change event if model is not changed
                    if (!this.static_url_model.hasChanged()) {
                        this.static_url_model.trigger('change');
                    }
                }

                this.static_url_model.id = id;
                this.static_url_model.fetch();
                break;
            }
        },

        shipping: function(action, id) {
            RT.dialogs.loading('open');
            $("#main").html("");
            var data = {},
                title;
            switch (id) {
            case 'disabled': 
                // set type
                this.shipping_model.set_params({
                    'type': 'disabled'
                });
                title = '已取消';
                data = {
                    type: 'disabled',
                    title: title,
                    is_add: true
                };
                break;
            case 'notpay':
                // set type
                this.shipping_model.set_params({
                    'type': 'notpay'
                });
                title = '待付款';
                data = {
                    type: 'notpay',
                    title: title,
                    is_add: true
                };
                break;
            case 'atmnotpay':
                // set type
                this.shipping_model.set_params({
                    'type': 'atmnotpay'
                });
                title = 'ATM待付款';
                data = {
                    type: 'atmnotpay',
                    title: title,
                    is_add: true
                };
                break;
            case 'nottake':
                // set type
                this.shipping_model.set_params({
                    'type': 'nottake'
                });
                title = '待收貨';
                data = {
                    type: 'nottake',
                    title: title,
                    is_add: true
                };
                break;
            case 'taking':
                // set type
                this.shipping_model.set_params({
                    'type': 'taking'
                });
                title = '收貨中';
                data = {
                    type: 'taking',
                    title: title,
                    is_add: true
                };
                break;
            case 'checking':
                // set type
                this.shipping_model.set_params({
                    'type': 'checking'
                });
                title = '驗收中';
                data = {
                    type: 'checking',
                    title: title,
                    is_add: true
                };
                break;
            case 'washed':
                // set type
                this.shipping_model.set_params({
                    'type': 'washed'
                });
                title = '已送洗';
                data = {
                    type: 'washed',
                    title: title,
                    is_add: true
                };
                break;
            case 'done':
                // set type
                this.shipping_model.set_params({
                    'type': 'done'
                });
                title = '已完成';
                data = {
                    type: 'done',
                    title: title,
                    is_add: true
                };
                break;
            }
            switch (action) {
            case 'list':
                this.update_title(title + '貨單列表');

                if (!this.view_shipping) {
                    this.view_shipping = new ViewShipping({
                        el: '#main',
                        collection: this.shipping_model.lists,
                        model_name: this.shipping_model
                    });
                }

                this.shipping_model.lists.fetch();
                break;
            }
	    this.view_shipping.setShippingType(id);

        },

        shopping: function(action) {
            RT.dialogs.loading('open');
            $("#main").html("");
            var data = {},
                title;
            switch (action) {
            case 'list':
                this.update_title('已送洗商品列表');

                if (!this.view_shopping) {
                    this.view_shopping = new ViewShopping({
                        el: '#main',
                        collection: this.shopping_model.lists,
                        model_name: this.shopping_model
                    });
                }

                this.shopping_model.lists.fetch();
                break;
            }
        },

        update_user: function() {
            new View({
                template_name: 'user_me',
                el: '#display_username',
                model: this.me
            }).render();
            
            if (!this.me.get('logged_in')) {
                var redirect_url = 'http://' + window.location.hostname + '/admin' + window.location.hash;
                window.location = '../auth/login?redirect_url=' + encodeURIComponent(redirect_url);
            }

            if ( _.indexOf(this.me.get('user_groups'), 'Admin') >= 0) {
                $('.func').hide();
                $('body').show();
                $('.func-admin').show();
            }
            else if ( _.indexOf(this.me.get('user_groups'), 'Intern') >= 0) {
                $('.func').hide();
                $('body').show();
                $('.func-intern').show();
            }
            else if ( _.indexOf(this.me.get('user_groups'), 'Blogger') >= 0) {
                $('.func').hide();
                $('body').show();
                $('.func-blogger').show();
            }
            else if ( _.indexOf(this.me.get('user_groups'), 'Operation') >= 0) {
                $('.func').hide();
                $('body').show();
                $('.func-operation').show();
            }
            else {
                var redirect_url = 'http://' + window.location.hostname + '/admin' + window.location.hash;
                window.location = '../auth/login?redirect_url=' + encodeURIComponent(redirect_url);
            }

            // check permission (Admin or User or None)
            if (this.me.has('user_groups')) {
                // show admin menu
                if (_.indexOf(this.me.get('user_groups'), 'None') >= 0) {
                    $('.none').show(500);
                }
            }
        },

        home: function() {
            this.update_title('管理首頁');
            RT.dialogs.loading('close');
        },

        /*
         * @author howtomakeaturn
         */

        clothing: function(action, id) {
            if (!this.view_clothing) {
                this.view_clothing = new ViewClothing;
            }

            RT.dialogs.loading('open');
            $("#main").html("");
            var data = {},
                title;
            switch (action) {
            
            case 'off_shelf_list':
                this.view_clothing.off_shelf_list();
                RT.dialogs.loading('close');
                break;              
            case 'list':
                this.update_title('已上架衣服列表');

                if (!this.view_clothing_list) {
                    this.view_clothing_list = new ViewClothingList({
                        el: '#main',
                        collection: this.clothing_model.lists,
                        model_name: this.clothing_model
                    });
                }
                else {
                    this.view_clothing_list.initialize();
                }
                this.clothing_model.lists.url = RT.API.getAllClothing;
                this.clothing_model.lists.fetch();
                break;
            case 'draft':
                this.update_title('草稿');
                if (!this.view_draft_list) {
                    this.view_draft_list = new ViewDraftList({
                        el: '#main',
                        collection: this.clothing_model.lists,
                        model_name: this.clothing_model
                    });
                }
                else {
                    this.view_draft_list.initialize();
                }
                this.clothing_model.lists.url = RT.API.getDraftClothing;
                this.clothing_model.lists.fetch();
                break;
            case 'enter':
                this.view_clothing.enter();
                RT.dialogs.loading('close');
                break;              
            case 'edit':
                this.update_title("編輯已上架衣服");
                
                if (!this.view_clothing_edit) {
                    this.view_clothing_edit = new ViewClothingEdit({
                        el: '#main',
                        model: this.clothing_model
                    });
                }

                this.clothing_model.product_item_number = id;
                 this.clothing_model.fetch();
                // console.log(this.clothing_model);
                
                // console.log('prodcut item number is ' + id);
                break;                
            }
        },

        /*
         * @author howtomakeaturn
         */

        buyorder: function(action, status_id) {
            RT.dialogs.loading('open');
            $("#main").html("");
            switch (action) {
                case 'list':
                    var status_name = '';
                    if (status_id == '1')
                        status_name = "待付款";
                    else if (status_id == '2')
                        status_name = "待出貨";
                    else if (status_id == '3')
                        status_name = "已出貨";
                    else if (status_id == '4')
                        status_name = "已到貨";
                    else if (status_id == '5')
                        status_name = "已完成";
                    else if (status_id == '-1')
                        status_name = "已過期";
                        
                    this.update_title(status_name + '訂單列表');

                    if (!this.view_buyorder) {
                        this.view_buyorder = new ViewBuyorder({
                            el: '#main',
                            collection: this.buyorder_model.lists,
                            model_name: this.buyorder_model
                        });
                    }

                    this.view_buyorder.setStatus(status_id);
                    this.buyorder_model.lists.setStatus(status_id).fetch();
                    break;
                case 'enter':
                    this.update_title("編輯訂單");
                    var quick_html = ""+
                        '<article class="module width_full">'+
                            '<div style="width:99%;margin:0 auto;">'+
                            '</div>'+
                            '<h1>請輸入訂單編號</h1>'+
                            '<p><input type="text" id="order_number" /></p>'+
                            '<p><button class="btn" id="submit_order_number">確定</button></p>'+
                        '</article>';
                                                
                    $("#main").hide().html(quick_html).fadeIn("slow");
                    $("#submit_order_number").click(function(){
                        var order_number = $("#order_number").val();
                        window.location.hash = '!/buyorder/edit/' + order_number;
                    });
                    RT.dialogs.loading('close');                    
                    break;
                case 'edit':
                    this.update_title("編輯訂單");
                    if (!this.view_buyorder_edit) {
                        this.view_buyorder_edit = new ViewBuyorderEdit({
                            el: '#main',
                            model: this.buyorder_model
                        });
                    }
                    this.buyorder_model.order_number = status_id;
                    this.buyorder_model.fetch();
                    console.log(this.buyorder_model.order_number);
                    // this.view_buyorder_edit.render();
                    // RT.dialogs.loading('close');                    
                    break;
            }
        },

 
        
        suggested: function() {
            RT.dialogs.loading('open');
            $("#main").html("");

            this.update_title('推薦衣服');

            if (!this.view_suggested) {
                this.view_suggested = new ViewSuggested({
                    el: '#main',
                    collection: this.suggested_model.lists,
                    model: this.suggested_model
                });
            }
            this.suggested_model.lists.fetch();
        },

        physical_selling: function(action, order_id) {
            RT.dialogs.loading('open');
            $("#main").html("");
            
            switch (action) {
                case 'list':
                    var self = this;
                    this.update_title('訂單列表');
                    $.ajax({
                        type: 'GET',
                        url: "/api/physical_selling/getPhysicalOrders?format=json",
                        dataType: 'json',
                        success: function(res){
                            $("#main").hide().html(Handlebars.templates.physical_selling_list(res)).fadeIn("slow");
                            $('#view_physical_buyorder').click(function(){
                                RT.dialogs.loading('open');
                                var data = {};
                                data.order_number = $('#physical_buyorder_list').val();
                                console.log(data);
                                $.ajax({
                                    type: 'POST',
                                    url: "/api/physical_selling/viewOrder.json",
                                    data: data,
                                    dataType: 'json',
                                    success: function(res){
                                        self.update_title('販售衣服');
                                        $("#main").hide().html(
                                            Handlebars.templates.physical_selling_view(res)).fadeIn("slow");                                      
                                            $('#put_one_into_physical_buyorder_button').click(function(){
                                                var data = {};
                                                data.product_item_number = $('#product_item_number').val();
                                                data.order_number = res.complete_order.order.order_number;                                
                                                if (!data.product_item_number.trim()) {
                                                    // is empty or whitespace
                                                    return false;
                                                }
                                                $.ajax({
                                                    type: 'POST',
                                                    url: "/api/physical_selling/putOneIntoPhysicalBuyorder.json",
                                                    data: data,
                                                    dataType: 'json',
                                                    beforeSend: function(){
                                                        RT.dialogs.loading('open');

                                                    },
                                                    success: function(res){
                                                        console.log(res);
                                                        alert('success!');
                                                        $('#clothings_in_physical_order').empty();
                                                        $.each(res.clothings, function(index, clothing){
                                                            var $li = $('<li>');
                                                            $li.html(clothing.product_title);
                                                            $('#clothings_in_physical_order').append($li);
                                                        })
                                                    },
                                                    error: function(res){
                                                        alert('failed, please try again');
                                                    },
                                                    complete: function(){
                                                        RT.dialogs.loading('close');  
                                                    }
                                                });
                                            });
                                    },
                                    error: function(res){
                                        alert('failed, please try again');
                                    },
                                    complete: function(){
                                        RT.dialogs.loading('close');                                    
                                    }
                                });
                                
                            });
                        },
                        error: function(res){
                            alert("failed, please try again");
                        },
                        complete: function(res){
                            RT.dialogs.loading('close');
                        }
                    });                                        
                    break;
                case 'new':
                    this.update_title("新增實體訂單");
                    $.ajax({
                        type: 'GET',
                        url: "/api/physical_selling/getAdminList?format=json",
                        dataType: 'json',
                        success: function(res){
                            $("#main").hide().html(Handlebars.templates.physical_selling_new()).fadeIn("slow");
                            $.each(res.users, function(index, user){
                                var optionElement = '' + 
                                '<option value="' + user.id + '">' + user.nickname + '</option>';
                                $('#admin_list').append(optionElement);
                            })
                            $('#create_physical_buyorder').click(function(){
                                var data = {};
                                data.user_id = $('#admin_list').val();
                                data.buyer_name = $('#buyer_name').val();                                
                                if (!data.buyer_name.trim()) {
                                    // is empty or whitespace
                                    return false;
                                }
                                $.ajax({
                                    type: 'POST',
                                    url: "/api/physical_selling/createOrder.json",
                                    data: data,
                                    dataType: 'json',
                                    success: function(res){
                                        window.location.hash = '!/physical_selling/list';
                                    },
                                    error: function(res){
                                        alert('failed, please try again');
                                    }
                                });
                            });
                        },
                        error: function(res){
                            alert("failed, please try again");
                        },
                        complete: function(res){
                            RT.dialogs.loading('close');
                        }
                    });                    
                    break;
                case 'view':
                    break;
            }
        
        
        },

        account: function(action, id) {

            if (!this.view_account) {
                this.view_account = new ViewAccount();
            }
            
            switch (action) {
                case 'enter':
                    this.view_account.enter();
                    break;
                case 'view':
                    this.view_account.view(id);
                    break              
                case 'list':
                    this.view_account.list();
                    break;
                case 'new':
                    this.view_account.new();
                    break;
            }// end switch
        },// end account
        
        withdraw: function(action) {
            RT.dialogs.loading('open');
            $("#main").html("");
            
            switch (action) {
                case 'list':
                    var self = this;
                    this.update_title('匯出申請');
                    $.ajax({
                        type: 'GET',
                        url: "/api/withdraw/getWithdraws?format=json",
                        dataType: 'json',
                        success: function(res){
                            $("#main").hide().html(Handlebars.templates.withdraw_list(res)).fadeIn("slow");
                                console.log(res);
                        },
                        error: function(res){
                            alert("failed, please try again");
                        },
                        complete: function(res){
                            RT.dialogs.loading('close');
                        }
                    });                                        
                    break;
            }// end switch
        },// end withdraw

        other: function(action) {
            RT.dialogs.loading('open');
            $("#main").html("");
            
            switch (action) {
                case 'emails':
                    var self = this;
                    this.update_title('會員信箱');
                    $.ajax({
                        type: 'GET',
                        url: "/api/users/getEmails?format=json",
                        dataType: 'json',
                        success: function(res){
                            $.each(res.emails, function(key, value){
                                var $para = $('<p>');
                                if (value.type=='active'){
                                    $para.css('color', 'red');
                                }
                                $para.append(value.address);
                                $('#main').append($para);                              
                            })
                                console.log(res);
                        },
                        error: function(res){
                            alert("failed, please try again");
                        },
                        complete: function(res){
                            RT.dialogs.loading('close');
                        }
                    });                                        
                    break;
                case 'buyer_emails':
                    var self = this;
                    this.update_title('買家信箱');
                    $.ajax({
                        type: 'GET',
                        url: "/api/users/getBuyerEmails?format=json",
                        dataType: 'json',
                        success: function(res){
                            $.each(res.emails, function(key, value){
                                var $para = $('<p>');
                                if (value.type=='active'){
                                    $para.css('color', 'red');
                                }
                                $para.append(value.address);
                                $('#main').append($para);                              
                            })
                                console.log(res);
                        },
                        error: function(res){
                            alert("failed, please try again");
                        },
                        complete: function(res){
                            RT.dialogs.loading('close');
                        }
                    });                                        
                    break;                    
                case 'seller_emails':
                    var self = this;
                    this.update_title('賣家信箱');
                    $.ajax({
                        type: 'GET',
                        url: "/api/users/getSellerEmails?format=json",
                        dataType: 'json',
                        success: function(res){
                            $.each(res.emails, function(key, value){
                                var $para = $('<p>');
                                if (value.type=='active'){
                                    $para.css('color', 'red');
                                }
                                $para.append(value.address);
                                $('#main').append($para);                              
                            })
                                console.log(res);
                        },
                        error: function(res){
                            alert("failed, please try again");
                        },
                        complete: function(res){
                            RT.dialogs.loading('close');
                        }
                    });                                        
                    break;                         
            }// end switch
        },// end other

        sys_config: function() {
            RT.dialogs.loading('open');
            $("#main").html("");
            
            var self = this;
            this.update_title('系統設定');

            $.ajax({
                type: 'GET',
                url: "/api/sys_config/getFreeShipping?format=json",
                dataType: 'json',
                success: function(res){
                    $("#main").hide().html(Handlebars.templates.sys_config(res)).fadeIn("slow");
                        console.log(res);
                },
                error: function(res){
                    alert("failed, please try again");
                },
                complete: function(res){
                    RT.dialogs.loading('close');
                }
            });
        },// end sys_config

        sms_handle: function(){
            RT.dialogs.loading('open');
            $("#main").html("");
            
            var self = this;
            this.update_title('訊息處理');

            $.ajax({
                type: 'GET',
                url: "/api/posts/get_all_post",
                dataType: 'html',
                success: function(res){
                    $("#main").hide().html(res).fadeIn('slow');

                    $(function(){
                        var root = $('[getmore-post]');
                        var message_template = _.template('<div class="reply clr"><img class="avatar" src="<%=profile_pic_url%>"><div class="subject"><p class="text magictext"><%=content%></p><div class="created"><%=created_time%></div></div></div>');
                        var post_template = _.template('<div class="row clr" style="margin:20px;" id="<%=id%>"><span class="replied"></span><div class="from"><a href="/closet/view/<%=parseInt(user_id)+9881%>" class="avatar" target="_blank"><img src="<%=profile_pic_url%>"></a><a href="/closet/view/<%=parseInt(user_id)+9881%>" target="_blank"><%=user_name%></a></div><div class="subject"><div class="line"><b><%=title%></b><a style="margin-left:10px;font-size:10px;" href="/closet/post/<%= url_code %>" target="_blank">查看衣櫥</a></div><p class="description magictext"><%=content%></p><div class="created"><%=created_time%></div><a class="delete" mid="<%=id%>" role="receiver">×</a><div class="hidable"><div class="wrap"><form method="POST" enctype="multipart/form-data" action="/api/posts/new_message" class="message"><textarea name="content"></textarea><input type="hidden" value="<%=id%>" name="post_id"><div class="action clr"><!-- <div class="file"> 附加圖片或檔案 <input name="file" type="file"></div> --><input type="submit" value="回覆訊息"></div></form></div></div></div></div>');
                        root
                            .delegate('div.wrap form.message', 'submit', function(e){
                                e.preventDefault();
                                var form = this;
                                $.ajax({
                                    type: $(this).attr('method'),
                                    url: $(this).attr('action'),
                                    data: $(this).serialize(),
                                    dataType: 'json'
                                }).done(function(message){
                                    form.reset();
                                    var html = message_template(message);
                                    if($('div.reply.clr', $(form).parents('div.hidable')).length){
                                        $('div.reply.clr:last').after(html);
                                    }
                                    else {
                                        $(form).parent('div.wrap').before($('<div>').addClass('messages').append(html));
                                    }
                                }).error(function(response){
                                    console.log(response);
                                });
                            })
                            .delegate('.readmore_messages', 'click', function(e){
                                $.ajax({
                                    type: 'GET',
                                    url: '/api/posts/get_all_message_of_post',
                                    data: {'post_id': $(this).data('post_id')},
                                    dataType: 'json'
                                }).done(function(messages){
                                    var html = '';
                                    _.each(messages, function(message){
                                        html += message_template(message);
                                    });
                                    $(e.target).toggleClass('readmore_messages readless_messages').html('讀較少留言').next('div.messages').html(html);
                                }).error(function(response){
                                    console.log(response);
                                });
                            })
                            .delegate('.readless_messages', 'click', function(e){
                                $('div.reply.clr:not(:last)', $(e.target).parent()).remove();
                                $(e.target).toggleClass('readmore_messages readless_messages').html('看其他留言')
                            })
                            .delegate('form.post', 'submit', function(e){
                                e.preventDefault();
                                var form = this;
                                $.ajax({
                                    type: $(this).attr('method'),
                                    url: $(this).attr('action'),
                                    data: $(this).serialize(),
                                    dataType: 'json'
                                }).done(function(post){
                                    form.reset();
                                    if(_.isArray(post)){
                                        var html = '';
                                        _.each(post, function(sub_post){
                                            html += post_template(sub_post);
                                        });
                                    }
                                    else {
                                        var html = post_template(post);
                                    }
                                    $(form).before(html);
                                }).error(function(response){
                                    console.log(response);
                                });
                            });
                    })
                },
                error: function(res){
                    alert("failed, please try again");
                },
                complete: function(res){
                    RT.dialogs.loading('close');
                }
            });
        },

        coupon: function(action, id) {
            RT.dialogs.loading('open');
            $("#main").html("");
            
            switch (action) {
                case 'list':
                    var self = this;
                    this.update_title('禮券列表');
                    $.ajax({
                        type: 'GET',
                        url: "/api/coupon/getCoupons?format=json",
                        dataType: 'json',
                        success: function(res){
                            $("#main").hide().html(Handlebars.templates.coupon_list(res)).fadeIn("slow");
                            $('.write_email').click(function(){
                                console.log('click');
                                var data = {};
                                data.ids = [];
                                $('.mail_to:checked').each(function(index){
                                    data.ids.push($(this).val());
                                });
                                if ( data.ids.length == 0 ){
                                    alert('請選擇要發通知信的禮券！');
                                    return false;
                                }
                                window.location.hash = '!/coupon/mailto/' + data.ids.join();                                  
                            });
                        },
                        error: function(res){
                            alert("failed, please try again");
                        },
                        complete: function(res){
                            RT.dialogs.loading('close');
                        }
                    });                                        
                    break;
                case 'mailto':
                    var self = this;
                    this.update_title('禮券通知信');
                    $("#main").hide().html(Handlebars.templates.coupon_mailto()).fadeIn("slow");
                    RT.dialogs.loading('close');
                    $('.send_button').click(function(){
                        var result = confirm('確定送出？');
                        if (!result){
                            return false;
                        }
                                              
                        var content = $('.mail_content').val();
                        var data = { ids: id, content: content };
                        RT.dialogs.loading('open');                        
                        $.post('/api/coupon/notifyCoupons?format=json', data, function(res){
                            RT.dialogs.loading('close');
                            alert('寄信完成');
                            window.location.hash = '!/coupon/list';     
                        }, 'json');
                    });
                    break;
                case 'view':
                    var self = this;
                    this.update_title('編輯禮券');
                    $.ajax({
                        type: 'GET',
                        url: "/api/coupon/getCoupon?format=json",
                        data: { id: id},
                        dataType: 'json',
                        success: function(res){
                            $("#main").hide().html(Handlebars.templates.coupon_view(res.coupon)).fadeIn("slow");
                            $('#delete_coupon_button').click(function(){
                                var result = confirm('確定要刪除禮券？');
                                if (!result){
                                    return false;
                                }
                              
                                RT.dialogs.loading('open');
                              
                                var data = {};
                                data.id = $('#id').val();
                                $.ajax({
                                    type: 'POST',
                                    url: "/api/coupon/deleteCoupon?format=json",
                                    data: data,
                                    dataType: 'json',
                                    success: function(res){
                                        window.location.hash = '!/coupon/list';
                                    },
                                    error: function(res){
                                        alert("failed, please try again");
                                    },
                                    complete: function(res){
                                        RT.dialogs.loading('close');
                                    }
                                });
                            });
                            $('#update_coupon_button').click(function(){
                                var result = confirm('確定要修改禮券？');
                                if (!result){
                                    return false;
                                }
                                RT.dialogs.loading('open');
                              
                                var data = {};
                                data.id = $('#id').val();
                                data.name = $('#name').val();
                                data.code = $('#code').val();
                                data.amount = $('#amount').val();
                                data.times_available = $('#times_available').val();
                                data.minimum_charge = $('#minimum_charge').val();
                                data.expired_at = $('#expired_at').val();
                                data.dedicated_user_id = $('#dedicated_user_id').val();
                                $.ajax({
                                    type: 'POST',
                                    url: "/api/coupon/updateCoupon?format=json",
                                    data: data,
                                    dataType: 'json',
                                    success: function(res){
                                        window.location.hash = '!/coupon/list';
                                    },
                                    error: function(res){
                                        alert("failed, please try again");
                                    },
                                    complete: function(res){
                                        RT.dialogs.loading('close');
                                    }
                                });
                            });
                            console.log(res);
                        },
                        error: function(res){
                            alert("failed, please try again");
                        },
                        complete: function(res){
                            RT.dialogs.loading('close');
                        }
                    });                                        
                    break;
                case 'new':
                    var self = this;
                    this.update_title('新增禮券');
                    $("#main").hide().html(Handlebars.templates.coupon_new()).fadeIn("slow");
                    RT.dialogs.loading('close');
                    
                    $('#create_coupon_button').click(function(){
                        RT.dialogs.loading('open');
                      
                        var data = {};
                        data.name = $('#name').val();
                        data.code = $('#code').val();
                        data.amount = $('#amount').val();
                        data.times_available = $('#times_available').val();
                        data.minimum_charge = $('#minimum_charge').val();
                        data.expired_at = $('#expired_at').val();
                        data.dedicated_user_id = $('#dedicated_user_id').val();
                        $.ajax({
                            type: 'POST',
                            url: "/api/coupon/addCoupon?format=json",
                            data: data,
                            dataType: 'json',
                            success: function(res){
                                window.location.hash = '!/coupon/list';
                            },
                            error: function(res){
                                alert("failed, please try again");
                            },
                            complete: function(res){
                                RT.dialogs.loading('close');
                            }
                        });
                    });

                    $("form#data").submit(function(){

                        var formData = new FormData($(this)[0]);
                        console.log(formData);

                        $.ajax({
                            url: "/api/coupon/uploadCoupon?format=json",
                            type: 'POST',
                            data: formData,
                            async: false,
                            dataType: 'json',
                            success: function (data) {
                                alert('成功新增' + data.csv.length + '筆禮券');
                                console.log(data);
                                window.location.hash = '!/coupon/mailto/' + data.ids;
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });

                        return false;
                    });

                    break;
            }// end switch
        },

        vendor: function(action, id) {
            if (!this.view_vendor) {
                this.view_vendor = new ViewVendor();
            }

            switch (action) {
                case 'list':
                    this.view_vendor.list();
                    break;
                case 'add':
                    this.view_vendor.add();                 
                    break;
                case 'view':
                    this.view_vendor.view(id);
                    break;
            }// end switch
        },

        sellcoupon: function(action, id) {
            if (!this.view_sellcoupon) {
                this.view_sellcoupon = new ViewSellcoupon();
            }

            switch (action) {
                case 'list':
                    this.view_sellcoupon.list();
                    break;
                case 'view':
                    this.view_sellcoupon.view(id);
                    break;                
                case 'new':
                    this.view_sellcoupon.add();
                    break;
            } // end switch
        },
        
        article: function(action, id) {
          /*
            if (!this.view_sellcoupon) {
                this.view_sellcoupon = new ViewSellcoupon();
            }
          */
            RT.dialogs.loading('open');
            switch (action) {
                case 'list':
                    $.get('/api/articleapi/getAll?format=json', {}, function(res){
                        $("#main").hide().html(Handlebars.templates.article_list(res)).fadeIn("slow");                      
                        RT.dialogs.loading('close');
                    }, 'json');
                    break;
                case 'edit':
                    $.get('/api/articleapi/getOne?format=json', {id: id}, function(res){
                        $("#main").hide().html(Handlebars.templates.article_edit(res)).fadeIn("slow");                      
                        RT.dialogs.loading('close');
                    }, 'json');
                    break;
                case 'new':
                    $.get('/api/articleapi/getAllCategories?format=json', {}, function(res){
                        $("#main").hide().html(Handlebars.templates.article_new(res)).fadeIn("slow");                      
                        RT.dialogs.loading('close');
                    }, 'json');
                    break;                
                case 'category':
                    $.get('/api/articleapi/getAllCategories?format=json', {}, function(res){
                        $("#main").hide().html(Handlebars.templates.article_category(res)).fadeIn("slow");                      
                        RT.dialogs.loading('close');
                    }, 'json');
                    break;
            } // end switch
        },

        brand: function(action, id) {
          /*
            if (!this.view_sellcoupon) {
                this.view_sellcoupon = new ViewSellcoupon();
            }
          */
            RT.dialogs.loading('open');
            switch (action) {
                case 'list':
                    $.get('/api/brand/getAll?format=html', {}, function(res){
                        $("#main").hide().html(res).fadeIn("slow");                      
                        RT.dialogs.loading('close');
                    }, 'html');
                    break;
            } // end switch
        },

        return_request: function(action) {
            RT.dialogs.loading('open');
            switch (action) {
                case 'list':
                    $.get('/api/return_request/getAll?format=json', {}, function(res){
                        $("#main").hide().html(Handlebars.templates.return_request_list(res)).fadeIn("slow");
                        RT.dialogs.loading('close');
                    }, 'json');
                    break;
            } // end switch
        },

        report: function(action){
            switch(action){
                case 'buy_order':
                    this.update_title('報表::已完成訂單');
                    $("#main").hide().html(Handlebars.templates.report_buy_order()).fadeIn("slow");
                    break;
                case 'return_request':
                    this.update_title('報表::退貨');
                    $("#main").hide().html(Handlebars.templates.report_return_request()).fadeIn("slow");
                    break;
                case 'sellorders': 
                    this.update_title('報表::貨單');
                    $("#main").hide().html(Handlebars.templates.report_sellorders()).fadeIn("slow");
                    break;
                case 'product_clothing':
                    this.update_title('報表::庫內商品');
                    $("#main").hide().html(Handlebars.templates.report_product_clothing()).fadeIn("slow");
                    break;
                case 'selled_products':
                    this.update_title('報表::售出商品');
                    $("#main").hide().html(Handlebars.templates.report_selled_products()).fadeIn("slow");
                    break;
            }
        }
    });

    var initialize = function() {
        RT.Router = new AppRouter;
        Backbone.history.start();
    };
    return {
        initialize: initialize
    };
});
