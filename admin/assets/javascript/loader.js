define([
    'order!libs/jquery/jquery.min',
    'order!libs/jquery/jquery-ui-1.9.0.custom.min',
    'order!libs/jquery/jquery.serialize',
    'order!libs/jquery/bootstrap-modal',
    'order!libs/tablesorter/jquery.tablesorter.min',
    'order!libs/jquery/swfobject',
    'order!libs/jquery/jquery.uploadify.v2.1.4.min',
    'order!libs/jquery/jquery.equalHeight',
    'order!libs/console.log',
    'order!libs/twitter/bootstrap-tab',
    'order!libs/handlebars/handlebars.runtime-1.0.rc.1',
    'order!libs/handlebars_helper',
    'order!../templates/template',
    'order!libs/underscore/underscore-min',
    'order!libs/backbone/backbone-min',
//    'order!libs/editable/jquery.jeditable.js' comment by howtomakeaturn. this line cause admin site failed. it's weird. fine.
    'order!assets/javascript/libs/editable/jquery.jeditable.js',
    'order!/assets/javascript/howtomakeaturn/jquery.form.min.js'
    ], function() {
    return {
        Backbone: Backbone.noConflict(),
        _: _.noConflict(),
        $: jQuery.noConflict()
    };
});
