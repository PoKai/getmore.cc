/*
 * RT script
 */

"use strict";

// Functions to run even before DOMReady that required jQuery
(function($) {

    RT.templates = {};

    // define API Request.
    var ajaxSettings = {
        dataType: 'json'
    };

    var api_req = function(name, callback, settings) {
            settings = (!settings) ? {} : settings;

            return $.ajax(
            $.extend({}, ajaxSettings, {
                url: name,
                type: (settings.data) ? 'POST' : 'GET',
                beforeSend: function(jqXHR, settings) {
                    RT.dialogs.loading('open');
                },
                success: callback,
                error: function(xhr, status, errorThrown) {
                    var message = 'Unknown error. Please try again later.';
                    switch (status) {
                        case 'timeout':
                            message = 'Server timeout. Please try again later.';
                            break;
                        case 'error':
                        case 'parsererror':
                            message = 'Server experienced some difficulty. Please try again later.';
                            break;
                        case 'abort':
                            message = 'Aborted.';
                            break;
                    }

                    if (xhr.status === 403) {
                        message = "You don't have permission to access it.";
                    }

                    console.log(message);
                    RT.show_message('form', 'alert-error', message);
                    RT.dialogs.loading('close');
                }
            }, settings));
        };

    RT.api_req = {
        GET: function(path, data, callback, settings) {
            settings = settings || {};
            data = data || {};
            return api_req(
            path, callback, $.extend({
                type: 'GET',
                data: data
            }, settings));
        },
        POST: function(path, data, callback, settings) {
            settings = settings || {};
            data = data || {};
            return api_req(
            path, callback, $.extend({
                type: 'POST',
                data: data
            }, settings));
        },
        PUT: function(path, data, callback, settings) {
            settings = settings || {};
            data = data || {};
            return api_req(
            path, callback, $.extend({
                type: 'PUT',
                data: data
            }, settings));
        },
        DELETE: function(path, data, callback, settings) {
            settings = settings || {};
            data = data || {};
            return api_req(
            path, callback, $.extend({
                type: 'DELETE',
                data: data
            }, settings));
        }
    };


    RT.dialogs = {
        loading: function(action) {
            if (action !== 'close') {
                $('#loading').show();
            } else {
                $('#loading').hide();
            }
        }
    };

    $('script[type="text/x-mustache-template"]').each(

    function(index, element) {
        RT.templates[$(this).data('id')] = $(this).html();
        $(this).remove();
    });

    $(".tablesorter").tablesorter({
        // pass the headers argument and assing a object
        headers: {
            // assign the secound column (we start counting zero)
            0: {
                // disable it by setting the property sorter to false
                sorter: false
            }
        }
    });

    //When page loads...
    $(".tab_content").hide(); //Hide all content
    $("ul.tabs li:first").addClass("active").show(); //Activate first tab
    $(".tab_content:first").show(); //Show first tab content
    //On Click Event
    $("ul.tabs li").click(function() {
        $("ul.tabs li").removeClass("active"); //Remove any "active" class
        $(this).addClass("active"); //Add "active" class to selected tab
        $(".tab_content").hide(); //Hide all tab content
        var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
        $(activeTab).fadeIn(); //Fade in the active ID content
        return false;
    });

    RT.show_message = function(id, type, message, location) {
        location = location || 'insertBefore';
        var html = '<div class="alert" style="display:none;"><a class="close" data-dismiss="alert">×</a><span class="message"></span></div>';
        type = type || 'alert-success', message = message || '';

        $(".alert").remove();
        if (message === '' || id === undefined) return false;
        switch (location) {
            case 'prepend':
                $(id).prepend(html);
            break;
            default:
                $(html).insertBefore(id);
        }
        $(".message").text(message);
        $(".alert").addClass(type).fadeIn("slow", function(e) {
            setTimeout(function(e) {
                $(".alert").fadeOut("slow");
            }, 10000);
        });
    };

    RT.update_table = function() {
        $(".tablesorter").tablesorter({
            // pass the headers argument and assing a object
            headers: {
                // assign the secound column (we start counting zero)
                0: {
                    // disable it by setting the property sorter to false
                    sorter: false
                }
            }
        });
        $("table").trigger("update");
        $("#sidebar").equalHeight();
    };

    RT.generateSerial = function(len) {
        var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
        var string_length = (len === undefined || isNaN(parseInt(len)) || len == "") ? 32 : parseInt(len);
        var randomstring = '';

        for (var x = 0; x < string_length; x++) {

            var letterOrNumber = Math.floor(Math.random() * 2);
            if (letterOrNumber === 0) {
                var newNum = Math.floor(Math.random() * 9);
                randomstring += newNum;
            } else {
                var rnum = Math.floor(Math.random() * chars.length);
                randomstring += chars.substring(rnum, rnum + 1);
            }
        }
        return randomstring;
    };

    RT.loadEditor = function(id) {
        var instance = CKEDITOR.instances[id];
        if (instance) {
            CKEDITOR.remove(instance);
        }
        CKEDITOR.config.contentsCss = ['/assets/css/screen.css'];

        CKEDITOR.replace(id, {
            toolbar: [
                ['Source', '-', 'NewPage', 'Preview'],
                ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Scayt'],
                ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'], '/', ['Styles', 'Format', 'FontSize'],
                ['Bold', 'Italic', 'Strike'],
                ['TextColor', 'BGColor'], '/', ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote'],
                ['Link', 'Unlink', 'Anchor'],
                ['Maximize', '-', 'About'],
                ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],

                ]
        });
    };

    // delete button
    $(document).on('mouseenter', '.privilege', function(ev) {
        ev.preventDefault();
        var id = $(this).data("id");
        var action = $(this).data("action");
        if (action == 'deny') {
            $(this).removeClass("btn-danger").addClass("btn-primary");
            $(this).text("允許");
        } else {
            $(this).removeClass("btn-primary").addClass("btn-danger");
            $(this).text("禁止");
        }
    }).on('mouseleave', '.privilege', function(ev) {
        ev.preventDefault();
        var id = $(this).data("id");
        var action = $(this).data("action");
        if (action == 'deny') {
            $(this).removeClass("btn-primary").addClass("btn-danger");
            $(this).text("禁止");
        } else {
            $(this).removeClass("btn-danger").addClass("btn-primary");
            $(this).text("允許");
        }
    }).on('click', '.privilege', function(ev) {
        ev.preventDefault();
        var id = $(this).data("id");
        var priview_action = $(this).data("action");
        var current_action = (priview_action == 'deny') ? 'allow' : 'deny';
        var form_info = {
            'id': id,
            'action': current_action
        };
        var form_id = $(this).data('form');

        if (priview_action == 'deny') {
            $(this).data("action", "allow");
        } else {
            $(this).data("action", "deny");
        }

        $.ajax({
            url: RT.API.aclEdit,
            dataType: 'json',
            type: 'POST',
            data: form_info,
            success: function(response) {
                if (response.error_text) {
                    RT.show_message(form_id, 'alert-error', '修改失敗');
                    RT.dialogs.loading('close');
                }
                if (response.success_text) {
                    RT.show_message(form_id, 'alert-success', '修改成功');
                }
            }
        });
    }).on('click', '.auto_generate', function(ev) {
        ev.preventDefault();
        var name = $(this).data("field");
        var random_serial = RT.generateSerial(32);
        $(":input[name=" + name + "]").val(random_serial);
    }).on('click', '.close', function(ev) {
        ev.preventDefault();
        $(this).parent().fadeOut('slow');
    }).on('click', '.toggle > li > a', function(ev) {
        // click left menu list
        //RT.dialogs.loading('open');
    }).on('click', '.check_all', function(ev) {
        ev.preventDefault();
        $("input[type=checkbox]").each(

        function() {
            var checked = $(this).attr('checked');
            if (checked) $(this).attr("checked", false);
            else $(this).attr("checked", true);
        });
    });

    $('.column').equalHeight();
    $(window).trigger('hashchange');
}(jQuery));