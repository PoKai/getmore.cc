(function($){
    var file_name = ($("input[name=upload_file_name]").val()) ? $("input[name=upload_file_name]").val() : 'file_list';
    var type = $('#file_upload').data('type') || '';
    $('#file_upload').uploadify({
        'uploader': 'assets/upload/uploadify.swf',
        'script': '/restful/product_clothing/file/' + type,
        'cancelImg': 'assets/upload/cancel.png',
        'folder': '/upload/',
        'auto': true,
        'buttonText': 'Select Upload File',
        'displayData': true,
        'fileDataName': 'userfile',
        'removeCompleted': false,
        'multi': true,
        'onComplete': function(event, ID, fileObj, response, data) {
            var html, file_id;
            file_id = jQuery.parseJSON(response);
            $('#file_upload' + ID).slideUp('1000', function() {
                return $(this).remove();
            });
            html = '<li class="span2" style="text-align:center"><a href="#" class="thumbnail"><img src="http://s3.getmore.cc/clothing/' + file_id + '/medium.jpg" alt=""></a><input type="hidden" name="' + file_name + '[]" value="' + file_id + '"><button type="button" data-id="' + file_id + '" class="btn btn-danger delete_file">刪除檔案</button></li>';
            $(".thumbnails").prepend(html);
        },
        'onError': function(event, ID, fileObj, response){
            console.log(arguments);
            /*var html = $("#message").html();
            $("#message").html(html + obj.error);*/
        }
    });
    $(".thumbnails").sortable();
    $(".thumbnails").disableSelection();
}(jQuery));
