// Filename: app.js
define([
    'jquery',
    'underscore',
    'backbone',
    'router',
    'assets/javascript/ckeditor/ckeditor.js',
    'assets/javascript/libs/hideshow.js',
    'assets/javascript/libs/script.js'
    ], function($, _, Backbone, Router){
    var initialize = function(){
        // Pass in our Router module and call it's initialize function
        Router.initialize();
    }

    return {
        initialize: initialize
    };
});
