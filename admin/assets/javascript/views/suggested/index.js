define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewSuggested = View.extend({

        events: _.extend({
            'click .delete-suggested-item': 'deleteSuggestedItem',
            'click #submit_product_item_number': 'addItem',
            'click #update_suggested_sequence': 'updateSequence'
        }, View.prototype.events),


        initialize: function() {
            var parent_view = this;
            if (this.model) {
                this.model.on("reset", this.render, this);
            }
            if (this.collection) {
                this.collection.on("reset", this.render, this);
            }            
            
        },

        updateSequence: function(){
            var sortedIDs = $( "#sortable" ).sortable( "toArray" , { attribute: 'data-id' });
            var self = this;
            var data = { 'sequences': JSON.stringify(sortedIDs) };
            console.log(data);
            $.ajax({
                url: '/api/suggested/updateSequences',
                type: 'POST',
                dataType: 'json',
                data: data,
                beforeSend: function(jqXHR, settings) {
                    RT.dialogs.loading('open');
                },
                success: function(response) {
                    self.model.lists.fetch();
                },
                error: function(){
                    alert('新增失敗');
                    RT.dialogs.loading('close');
                }
            });
            
        },

        
        addItem: function(){
            var self = this;
            var data = {'product_item_number': $('#product_item_number').val()};
            console.log(data);
            $.ajax({
                url: '/api/suggested/add',
                type: 'POST',
                dataType: 'json',
                data: data,
                beforeSend: function(jqXHR, settings) {
                    RT.dialogs.loading('open');
                },
                success: function(response) {
                    self.model.lists.fetch();
                },
                error: function(){
                    alert('新增失敗');
                    RT.dialogs.loading('close');
                }
            });
            
            // window.location.hash = '!/suggested/index';
        },
        
        deleteSuggestedItem: function(e){
            var self = this;
            var suggested_id = $(e.currentTarget).parent().data('id');          
            var data = {suggested_id: suggested_id};
            $.ajax({
                url: '/api/suggested/delete',
                type: 'POST',
                dataType: 'json',
                data: data,
                beforeSend: function(jqXHR, settings) {
                    RT.dialogs.loading('open');
                },
                success: function(response) {
                    self.model.lists.fetch();
                },
                error: function(){
                    alert('刪除失敗');
                    RT.dialogs.loading('close');
                }
            });
        },

        // implement render function
        render: function() {
            var parent_view = this;
            $(parent_view.el).empty();
            
            var data = {};
            data.items = [];

            this.collection.each(function(item) {
                data.items.push(item.attributes);
            });
            
            $(parent_view.el).hide().html(Handlebars.templates.suggested_index(data)).fadeIn("slow");            

            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewSuggested;
});
