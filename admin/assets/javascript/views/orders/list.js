define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewProducts = View.extend({

        events: _.extend({
            'click .search_order': 'search',
            'click .change_status': 'change_status',
            'click .delivery_number': 'delivery_number',
            'click .print_order_list': 'print_order_list',
            'click .print_product_list': 'print_product_list'
        }, View.prototype.events),

        params: {},

        initialize: function() {
            var parent_view = this;
            var model_name = parent_view.options.model_name || parent_view.model_name;
            parent_view.status = model_name.status;

            if (this.model) {
                this.model.on("reset", this.render, this);
            }
            if (this.collection) {
                this.collection.on("reset", this.render, this);
            }
            if (parent_view.status) {
                parent_view.status.on("reset", this.insert_option, this);
            }
        },

        print_product_list: function(e) {
            e.preventDefault();
            var parent_view = this;
            var form_id = $(e.currentTarget).data('form'),
                model = $(e.currentTarget).data('model'),
                form_info = $(form_id).serializeObject(),
                length = $(form_id + " input:checked").length;

            if (length == 0) {
                RT.show_message(form_id, 'alert-error', '尚未勾選項目');
                return false;
            }

            // set order_id value
            $('input[name=order_id]').val(form_info.id);
            // change form action value
            $(form_id).attr('action', '/print/output/order/products/' + model);
            $(form_id).submit();
        },

        print_order_list: function(e) {
            e.preventDefault();
            var parent_view = this;
            var form_id = $(e.currentTarget).data('form'),
                model = $(e.currentTarget).data('model'),
                form_info = $(form_id).serializeObject(),
                length = $(form_id + " input:checked").length;

            if (length == 0) {
                RT.show_message(form_id, 'alert-error', '尚未勾選項目');
                return false;
            }

            // set order_id value
            $('input[name=order_id]').val(form_info.id);
            // change form action value
            $(form_id).attr('action', '/print/output/order/list/' + model);
            $(form_id).submit();
        },

        insert_option: function(e) {
            var parent_view = this;
            var options = '';
            parent_view.status.each(function(item) {
                var key = item.get('id');
                var value = item.get('name');
                options += '<option value="' + key + '">' + value + '</optiion>';
            });
            $("#s_status_id, #c_status_id").append(options);
        },

        change_status: function(e) {
            var parent_view = this;
            var value = $("#c_status_id").val(),
                text = $("#c_status_id :selected").text(),
                form_id = $(e.currentTarget).data('form'),
                form_info = $(form_id).serializeObject(),
                length = $(form_id + " input:checked").length;
            if (value == '' || length == 0) {
                RT.show_message(form_id, 'alert-error', '尚未選擇狀態或尚未勾選項目');
                return false;
            }

            $.ajax({
                url: RT.API.orderEdit,
                dataType: 'json',
                type: 'POST',
                data: {
                    'id': form_info.id,
                    'status_id': value
                },
                beforeSend: function(jqXHR, settings) {
                    RT.dialogs.loading('open');
                },
                success: function(response) {
                    if (response.error_text) {
                        RT.show_message(form_id, 'alert-error', '更新失敗');
                        RT.dialogs.loading('close');
                    }
                    if (response.success_text) {
                        RT.show_message(form_id, 'alert-success', '更新成功');
                        $(form_id + " input:checked").each(function() {
                            $(this).parent().parent().find('.status_text').html(text);
                        });
                        RT.dialogs.loading('close');
                    }
                }
            });
        },

        delivery_number: function(e) {
            var parent_view = this;
            var delivery_prefix = $("#delivery_prefix").val(),
                delivery_count = $("#delivery_count").val(),
                delivery_number = $("#delivery_number").val(),
                form_id = $(e.currentTarget).data('form'),
                form_info = $(form_id).serializeObject(),
                length = $(form_id + " input:checked").length;
            if (delivery_prefix == '' || length == 0 || delivery_count == '' || delivery_number == '') {
                RT.show_message(form_id, 'alert-error', '尚未勾選項目');
                return false;
            }

            $.ajax({
                url: RT.API.orderDeliveryNumber,
                dataType: 'json',
                type: 'POST',
                data: {
                    'id': form_info.id,
                    'delivery_prefix': delivery_prefix,
                    'delivery_count': delivery_count,
                    'delivery_number': delivery_number
                },
                beforeSend: function(jqXHR, settings) {
                    RT.dialogs.loading('open');
                },
                success: function(response) {
                    if (response.error_text) {
                        RT.show_message(form_id, 'alert-error', '更新失敗');
                        RT.dialogs.loading('close');
                    }
                    if (response.success_text) {
                        // reload page
                        parent_view.collection.fetch();
                    }
                }
            });
        },

        search: function(e) {
            (this.debug) && console.log('search');
            e.preventDefault();
            var parent_view = this,
                form_id = $(e.currentTarget).data('form');
            this.params = $(form_id).serializeObject();

            parent_view.options.model_name.set_params(this.params);
            parent_view.collection.fetch();
            e.stopImmediatePropagation();
            return false;
        },

        // implement render function
        render: function() {
            var parent_view = this;
            var data = this.options.data || {};
            $(parent_view.el).empty();
            data.items = [];
            this.collection.each(function(item) {
                var total_price = parseInt(item.get('total_price'));
                item.set('total_price', total_price, {
                    silent: true
                });
                data.items.push(item.attributes);
            });

            $.extend(data, this.params);
            $(parent_view.el).hide().html(Handlebars.templates.order_list(data)).fadeIn("slow");

            // get status select
            parent_view.status.fetch();

            RT.update_table();
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewProducts;
});