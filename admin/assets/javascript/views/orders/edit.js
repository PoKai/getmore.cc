define([
    'jquery',
    'underscore',
    'backbone',
    'views/view',
    'models/remark'
    ], function($, _, Backbone, View, ModelRemark) {
    RT.ViewOrder = View.extend({

        initialize: function() {
            if (this.model) {
                this.model.on("change", this.render, this);
            }

            if (this.model.status) {
                this.model.status.on("reset", this.insert_option, this);
            }
        },

        insert_option: function(e) {
            var parent_view = this;
            var options = '';
            parent_view.model.status.each(function(item) {
                var key = item.get('id');
                var value = item.get('name');
                var selected = (parent_view.model.attributes.status_id == key) ? 'selected' : '';
                options += '<option value="' + key + '" ' + selected + '>' + value + '</optiion>';
            });
            $("#status_id").html('');
            $("#status_id").append(options);
        },

        render: function(e) {
            var parent_view = this,
                data = {};
            $(parent_view.el).empty();

            $.each(parent_view.model.attributes.products, function(key, value) {
                $.extend(value, {
                    id: key + 1
                });
            });

            $.extend(data, parent_view.model.attributes);
            $(parent_view.el).hide().html(Handlebars.templates.order_edit(data)).fadeIn("slow");

            parent_view.model.status.fetch();
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewOrder;
});