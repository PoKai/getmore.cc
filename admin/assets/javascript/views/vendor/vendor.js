define([
        'jquery',
        'underscore',
        'backbone'
    ], function($, _, Backbone){
    var view = Backbone.View.extend({

        el: '#main',

        events: {
            'click #create_button': 'create',
            'click #add_to_order': 'add_to_order',
            'click #checkout_order': 'checkout_order',
            'click .remove_from_order': 'remove_from_order'
        },
        
        initialize: function() {
            // this excutes once while the script is running
            // no-op for now
        },

        create: function(){
            if (!confirm('確定新增一張Pop-up Shop訂單？')){
                return false;
            }
            
            RT.dialogs.loading('open');
          
            var customer_id = $('#user_id').val();
            var self = this;
            $.post('/api/vendor/vendor_create.json', { user_id: customer_id }, function(res){
                window.location.hash = '!/vendor/view/' + res.order_number;
                RT.dialogs.loading('close');                
            }, 'json');
        },

        add_to_order: function(){
            if (!confirm('確定加入這件衣服？')){
                return false;
            }
            var self = this;

            var washing_id = $('#washing_id').val();
            var order_number = this.order_number;
            
            RT.dialogs.loading('open');
            $.ajax({
                type: 'POST',
                url: "/api/vendor/add_to_order?format=json",
                data: { washing_id: washing_id, order_number: order_number },
                dataType: 'json',
                success: function(res){
                    self.view(self.order_number);
                },
                error: function(res){
                    alert("failed, please try again");
                },
                complete: function(res){
                    RT.dialogs.loading('close');
                }
            });                                        
        },

        remove_from_order: function(e){
            if (!confirm('確定取消這件衣服？')){
                return false;
            }
            var self = this;
            
            var clothing_id = $(e.currentTarget).data("id");
            
            RT.dialogs.loading('open');
            $.ajax({
                type: 'POST',
                url: "/api/vendor/remove_from_order?format=json",
                data: { clothing_id: clothing_id },
                dataType: 'json',
                success: function(res){
                    self.view(self.order_number);
                },
                error: function(res){
                    alert("failed, please try again");
                },
                complete: function(res){
                    RT.dialogs.loading('close');
                }
            });                                        
            
        },


        checkout_order: function(){
            if (!confirm('確定結帳？')){
                return false;
            }
            var self = this;

            var order_number = this.order_number;
            
            RT.dialogs.loading('open');
            $.ajax({
                type: 'POST',
                url: "/api/vendor/checkout_order?format=json",
                data: { order_number: order_number },
                dataType: 'json',
                success: function(res){
                    self.view(self.order_number);
                },
                error: function(res){
                    alert("failed, please try again");
                },
                complete: function(res){
                    RT.dialogs.loading('close');
                }
            });                                        
        },

        // implement render function
        render: function(template_name, data, smooth) {
          
            // set smooth default value to true
            smooth = typeof smooth !== 'undefined' ? smooth : true;
            
            this.$el.html('');
            if (smooth){
                this.$el.hide().html(Handlebars.templates[template_name](data)).fadeIn("slow");
            }
            else{
                this.$el.html(Handlebars.templates[template_name](data));
            }
            return this;
        },
        
        list: function(){
            RT.Router.update_title('Pop-up Shop訂單列表');
            RT.dialogs.loading('open');
            
            var self = this;
            $.get('/api/vendor/vendor_list.json', {}, function(res){
                self.render('vendor_list', res);
                RT.dialogs.loading('close');
            }, 'json');
        },

        add: function(){
            RT.Router.update_title('新增Pop-up Shop訂單');
            RT.dialogs.loading('open');
          
            this.render('vendor_add', {});

            RT.dialogs.loading('close');

        },
        
        view: function(order_number){
            RT.Router.update_title('管理Pop-up Shop訂單');
            RT.dialogs.loading('open');
            this.order_number = order_number;
            var self = this;
            $.get('/api/vendor/vendor_view.json', {order_number: order_number}, function(res){
                self.render('vendor_view', res, true);
                RT.dialogs.loading('close');
            }, 'json');

        }
        
        
    });
    return view;
});
