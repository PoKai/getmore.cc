define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewSubjectAdd = View.extend({
        initialize: function() {
            if (this.options.category_model) {
                this.options.category_model.on("reset", this.insert_option, this);
            }
        },

        insert_option: function() {
            var parent_view = this;
            var category_model = this.options.category_model || this.category_model;
            var category_option = '';
            category_model.each(function(item) {
                var key = item.get('id');
                var value = item.get('tw_name');
                category_option += '<option value="' + key + '">' + value + '</optiion>';
            });
            $("#category_id").html('');
            $("#category_id").append(category_option);
        },

        render: function(e) {
            var parent_view = this;
            var data = this.options.data || {};
            $(parent_view.el).empty();

            $.extend(data, {
                is_edit: false
            });
            $(parent_view.el).hide().html(Handlebars.templates.subject_edit(data)).fadeIn("slow");

            // get category options
            this.options.category_model.fetch();
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewSubjectAdd;
});