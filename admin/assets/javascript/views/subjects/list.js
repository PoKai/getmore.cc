define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewSubjects = View.extend({

        params: {},

        events: _.extend({
            'click .add_index': 'add_index',
            'click .remove_index': 'remove_index',
            'click .enable_all': 'enable_all',
            'click .disable_all': 'disable_all',
            'click .search_subject': 'search'
        }, View.prototype.events),

        search: function(e) {
            (this.debug) && console.log('search');
            e.preventDefault();
            var parent_view = this,
                form_id = $(e.currentTarget).data('form');
            this.params = $(form_id).serializeObject();

            parent_view.options.model_name.set_params(this.params);
            parent_view.collection.fetch();
            e.stopImmediatePropagation();
            return false;
        },

        add_index: function(e) {
            console.log('add_index');
            e.preventDefault();
            var parent_view = this;
            var form_id = $(e.currentTarget).data('form');
            var form_info = $(form_id).serializeObject();
            $.extend(form_info, {
                is_index: 1
            });
            parent_view.form_info = form_info;
            parent_view.form_id = form_id;
            parent_view.check_select();
            $("input:checked").each(

            function() {
                $(this).parent().parent().find('.is_index').html('<span style="color:blue">是</span>');
            });
            $("input:checked").length > 0 && parent_view.update_status();
            e.stopImmediatePropagation();
            return false;
        },

        remove_index: function(e) {
            console.log('remove_index');
            e.preventDefault();
            var parent_view = this;
            var form_id = $(e.currentTarget).data('form');
            var form_info = $(form_id).serializeObject();
            $.extend(form_info, {
                is_index: 0
            });
            parent_view.form_info = form_info;
            parent_view.form_id = form_id;
            parent_view.check_select();
            $("input:checked").each(

            function() {
                $(this).parent().parent().find('.is_index').html('<span style="color:red">否</span>');
            });
            $("input:checked").length > 0 && parent_view.update_status();
            e.stopImmediatePropagation();
            return false;
        },

        enable_all: function(e) {
            console.log('enable_all');
            e.preventDefault();
            var parent_view = this;
            var form_id = $(e.currentTarget).data('form');
            var form_info = $(form_id).serializeObject();
            parent_view.form_info = form_info;
            parent_view.form_id = form_id;
            parent_view.check_select();
            $.extend(form_info, {
                is_enabled: 1
            });
            $("input:checked").each(

            function() {
                $(this).parent().parent().find('.is_enabled').html('<span style="color:blue">是</span>');
            });
            $("input:checked").length > 0 && parent_view.update_status();
            e.stopImmediatePropagation();
            return false;
        },

        disable_all: function(e) {
            console.log('disable_all');
            e.preventDefault();
            var parent_view = this;
            var form_id = $(e.currentTarget).data('form');
            var form_info = $(form_id).serializeObject();
            $.extend(form_info, {
                is_enabled: 0
            });
            parent_view.form_info = form_info;
            parent_view.form_id = form_id;
            parent_view.check_select();
            $("input:checked").each(

            function() {
                $(this).parent().parent().find('.is_enabled').html('<span style="color:red">否</span>');
            });
            $("input:checked").length > 0 && parent_view.update_status();
            e.stopImmediatePropagation();
            return false;
        },

        check_select: function() {
            var parent_view = this;
            var length = $("input:checked").length;
            if (length == 0) {
                RT.show_message(this.form_id, 'alert-error', '尚未選取任何項目');
                return false;
            }
        },

        update_status: function(e) {
            var parent_view = this;
            $.ajax({
                url: RT.API.subjectEdit,
                dataType: 'json',
                type: 'POST',
                data: parent_view.form_info,
                beforeSend: function(jqXHR, settings) {
                    RT.dialogs.loading('open');
                },
                success: function(response) {
                    if (response.error_text) {
                        RT.show_message(parent_view.form_id, 'alert-error', '更改失敗');
                        RT.dialogs.loading('close');
                    }
                    if (response.success_text) {
                        RT.show_message(parent_view.form_id, 'alert-success', '更改成功');
                        RT.dialogs.loading('close');
                    }
                }
            });
        },
        // implement render function
        render: function() {
            var parent_view = this;
            var data = this.options.data || {};
            var boxes = [];
            $(parent_view.el).empty();
            data.items = [];
            this.collection.each(function(item) {
                var is_index = +item.get('is_index');
                var is_enabled = +item.get('is_enabled');
                item.set('is_index', is_index, {
                    silent: true
                });
                item.set('is_enabled', is_enabled, {
                    silent: true
                });
                data.items.push(item.attributes);
            });
            $.extend(data, this.params);

            $(parent_view.el).hide().html(Handlebars.templates.subject_list(data)).fadeIn("slow");
            RT.update_table();
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewSubjects;
});