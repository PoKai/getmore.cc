define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewSubject = View.extend({
        initialize: function() {
            if (this.model) {
                this.model.on("change", this.render, this);
            }
            if (this.options.category_model) {
                this.options.category_model.on("reset", this.insert_option, this);
            }
        },

        insert_option: function(e) {
            var parent_view = this,
                category_id = parent_view.model.attributes.category_id,
                category_option = '',
                key, value, selected;
            this.options.category_model.each(function(item) {
                key = item.get('id');
                value = item.get('tw_name');
                selected = (key == category_id) ? 'selected' : '';
                category_option += '<option value="' + key + '" ' + selected + '>' + value + '</optiion>';
            });
            $("#category_id").html('');
            $("#category_id").append(category_option);
        },

        render: function(e) {
            var parent_view = this;
            var category_model = this.options.category_model || this.category_model;
            var data = this.options.data || {};
            var user_id = parent_view.model.attributes.id;
            $(parent_view.el).empty();

            parent_view.model.attributes.is_index = +parent_view.model.attributes.is_index;
            parent_view.model.attributes.is_enabled = +parent_view.model.attributes.is_enabled;
            $.extend(data, parent_view.model.attributes);
            console.log(data);
            $(parent_view.el).hide().html(Handlebars.templates.subject_edit(data)).fadeIn("slow");
            // get category
            category_model.fetch();
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewSubject;
});