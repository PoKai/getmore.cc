define([
        'jquery',
        'underscore',
        'backbone',
        'views/tony_view'
    ], function($, _, Backbone, TonyView){
    var view = TonyView.extend({

        el: '#main',

        events: {
            'click #update_coupon_button': 'update_coupon',
            'click #delete_coupon_button': 'delete_coupon'
        },
        
        initialize: function() {
            // this excutes once while the script is running
            // no-op for now
        },
        
        list: function(){
            RT.Router.update_title('洗衣券列表');
            RT.dialogs.loading('open');
            
            var self = this;
            $.get('/api/sellcoupon/getCoupons?format=json', {}, function(res){
                self.render('sellcoupon_list', res);
                RT.dialogs.loading('close');
            }, 'json');
        },
        view: function(id){
            var self = this;
            RT.Router.update_title('編輯洗衣券');
            RT.dialogs.loading('open');

            $.get('/api/sellcoupon/getCoupon?format=json', { id: id }, function(res){
                self.render('sellcoupon_view', res.coupon);
                RT.dialogs.loading('close');
            }, 'json');
        },
        
        add: function(){
            var self = this;
            RT.Router.update_title('新增洗衣券');
            this.render('sellcoupon_new');

            $('#create_coupon_button').click(function() {
                RT.dialogs.loading('open');

                var data = {};
                data.name = $('#name').val();
                data.code = $('#code').val();
                data.amount = $('#amount').val();
                data.times_available = $('#times_available').val();
                data.minimum_charge = $('#minimum_charge').val();
                data.expired_at = $('#expired_at').val();
                data.dedicated_user_id = $('#dedicated_user_id').val();
                $.ajax({
                    type: 'POST',
                    url: "/api/sellcoupon/addCoupon?format=json",
                    data: data,
                    dataType: 'json',
                    success: function(res) {
                        window.location.hash = '!/sellcoupon/list';
                    },
                    error: function(res) {
                        alert("failed, please try again");
                    },
                    complete: function(res) {
                        RT.dialogs.loading('close');
                    }
                });
            });          
        },
        
        update_coupon: function(){
            var result = confirm('確定要修改洗衣券？');
            if (!result) {
                return false;
            }
            RT.dialogs.loading('open');

            var data = {};
            data.id = $('#id').val();
            data.name = $('#name').val();
            data.code = $('#code').val();
            data.amount = $('#amount').val();
            data.times_available = $('#times_available').val();
            data.minimum_charge = $('#minimum_charge').val();
            data.expired_at = $('#expired_at').val();
            $.ajax({
                type: 'POST',
                url: "/api/sellcoupon/updateCoupon?format=json",
                data: data,
                dataType: 'json',
                success: function(res) {
                    window.location.hash = '!/sellcoupon/list';
                },
                error: function(res) {
                    alert("failed, please try again");
                },
                complete: function(res) {
                    RT.dialogs.loading('close');
                }
            });
        },
        
        delete_coupon: function(){
            var result = confirm('確定要刪除洗衣券？');
            if (!result) {
                return false;
            }
            self = this;
            RT.dialogs.loading('open');
            var data = {};
            data.id = $('#id').val();
            $.ajax({
                type: 'POST',
                url: "/api/sellcoupon/deleteCoupon?format=json",
                data: data,
                dataType: 'json',
                success: function(res) {
                    window.location.hash = '!/sellcoupon/list';
                },
                error: function(res) {
                    alert("failed, please try again");
                },
                complete: function(res) {
                    RT.dialogs.loading('close');
                }
            });          
        }
                
    });
    return view;
});
