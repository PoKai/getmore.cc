define([
        'jquery',
        'underscore',
        'backbone',
        'views/tony_view'
    ], function($, _, Backbone, TonyView){
    var view = TonyView.extend({

        el: '#main',
        
        initialize: function() {
            // this excutes once while the script is running
            // no-op for now
        },
        
        list: function(){
            RT.dialogs.loading('open');
            var self = this;
            RT.Router.update_title('帳戶異動列表');
            $.ajax({
                type: 'GET',
                url: "/api/account/getTransactions?format=json",
                dataType: 'json',
                success: function(res){
                    self.render('account_list', res)
                },
                error: function(res){
                    alert("failed, please try again");
                },
                complete: function(res){
                    RT.dialogs.loading('close');
                }
            });
        },
        
        'new': function(){
            RT.dialogs.loading('open');
            var self = this;
            RT.Router.update_title('新增異動');
            self.render('account_new');
            RT.dialogs.loading('close');
            
            $('#create_transaction_button').click(function(){
                RT.dialogs.loading('open');
              
                var data = {};
                data.user_id = $('#user_id').val();
                data.name = $('#name').val();
                data.related_number = $('#related_number').val();
                data.amount = $('#amount').val();
                data.note = $('#note').val();
                $.ajax({
                    type: 'POST',
                    url: "/api/account/addTransaction?format=json",
                    data: data,
                    dataType: 'json',
                    success: function(res){
                        window.location.hash = '!/account/list';
                    },
                    error: function(res){
                        alert("failed, please try again");
                    },
                    complete: function(res){
                        RT.dialogs.loading('close');
                    }
                });
            });
          
        },
        
        view: function(id){
            RT.dialogs.loading('open');
            var self = this;
            $.ajax({
                type: 'GET',
                url: "/api/account/getByUserId?format=json",
                data: { user_id: id },
                dataType: 'json',
                success: function(res){
                    self.render('account_view', res);
                },
                error: function(res){
                    alert("failed, please try again");
                },
                complete: function(res){
                    RT.dialogs.loading('close');
                }
            });

        },
        
        enter: function(){
            RT.Router.update_title("檢視會員帳戶");
            var quick_html = ""+
                '<article class="module width_full">'+
                    '<div style="width:99%;margin:0 auto;">'+
                    '</div>'+
                    '<h1>請輸入會員ID</h1>'+
                    '<p><input type="text" id="user_id" /></p>'+
                    '<p><button class="btn" id="submit_user_id">確定</button></p>'+
                '</article>';
                                        
            $("#main").hide().html(quick_html).fadeIn("slow");
            $("#submit_user_id").click(function(){
                var user_id = $("#user_id").val();
                window.location.hash = '!/account/view/' + user_id;
            });
        }
        
    });
    return view;
});
