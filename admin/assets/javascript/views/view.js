/*
 * The main portion of a page
 *
 * Each RT.View should define the following properties:
 *   el: a CSS path or the parent element
 *   template_name: the data-id of the Mustache template to use for displaying the model or collection
 * Each RT.View should also specify one of the following for default rendering:
 *   model: a single object that the view will display using the template
 *   collection: a set of objects, each of which will be displayed using the template
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'models/subject',
    'models/product',
    'models/event'
    ], function($, _, Backbone, ModelSubject, ModelProduct, ModelEvent) {
    RT.View = Backbone.View.extend({

        initialize: function() {
            this.folding_menu();
            if (this.model) {
                this.model.on("change", this.render, this);
                this.model.on("error", this.ajax_error, this);
            }
            if (this.collection) {
                this.collection.on("reset", this.render, this);
                this.collection.on("error", this.ajax_error, this);
            }
            this.debug = false;
        },

        events: {
            'click .add': 'add',
            'click .edit': 'edit',
            'click .delete': 'delete_item',
            'click .delete_all': 'delete_all',
            'click .delete_file': 'delete_file',
            'click .delete_clothing_file': 'delete_clothing_file',
            'click .delete_remarks': 'delete_remarks',
            'click .delete_all_remarks': 'delete_all_remarks',
            'click .add_remarks': 'add_remarks',
            'click .edit_remarks': 'edit_remarks',
            'click .save_remarks': 'save_remarks',
            'click .add_detail': 'add_detail',
            'click .delete_detail': 'delete_detail',
            //'click input:checkbox': 'folding_menu',
            'keyup .price': 'calculate',
            'keyup .event_price': 'calculate_event'
        },

        folding_menu: function(){
            $('#sidebar h3').toggle(function(){
                $this = $(this);
                $this.next().show();
            },function(){
                $this = $(this);
                $this.next().hide();
            }
);
        },

        ajax_error: function(model, error) {
            var message = 'Unknown error. Please try again later.';
            if (error.status === 403) {
                message = "You don't have permission to access it.";
            }
            console.log(message);
            RT.show_message('#main', 'alert-error', message, 'prepend');
            RT.dialogs.loading('close');
        },

        edit_remarks: function(e) {
            console.log('edit_remarks');
            e.preventDefault();
            e.stopPropagation();
            var parent_view = this;
            var html = $(e.currentTarget).parent().parent().find(".remarks").html();
            var id = $(e.currentTarget).data('id');
            CKEDITOR.instances.remarks.setData(html);
            $(".add_remarks").text('編輯備註').removeClass('add_remarks').addClass('save_remarks').data('id', id);
            e.stopImmediatePropagation();
            return false;
        },

        save_remarks: function(e) {
            console.log('save_remarks');
            e.preventDefault();
            var parent_view = this,
                form_id = $(e.currentTarget).data('form'),
                id = $(e.currentTarget).data('id'),
                form_info = $(form_id).serializeObject(),
                value = CKEDITOR.instances['remarks'].getData(),
                data = {
                    id: id,
                    remarks: value
                };

            RT.api_req.POST(RT.API.remarkEdit, $.extend(form_info, data), function(response) {
                if (response.error_text) {
                    RT.show_message(form_id, 'alert-error', '刪除失敗');
                    RT.dialogs.loading('close');
                }
                if (response.success_text) {
                    RT.show_message(form_id, 'alert-success', '修改成功');
                    $(".save_remarks").text('新增備註').removeData('id').removeClass('save_remarks').addClass('add_remarks');
                    CKEDITOR.instances.remarks.setData('');
                    // set html to remarks
                    $(form_id).find("td." + id).html(value);
                    RT.dialogs.loading('close');
                }
            });
            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },

        calculate: function(e) {
            // calculate product price
            var sell_price = parseInt($("input[name='sell_price']").val()),
                stock_price = parseInt($("input[name='stock_price']").val()),
                original_price = parseInt($("input[name='original_price']").val()),
                number_1 = Math.floor(100 - sell_price / original_price * 100) || 0,
                number_2 = Math.floor((sell_price - stock_price) / stock_price * 100) || 0;

            $("input[name='number_1']").val(number_1);
            $("input[name='number_2']").val(number_2);
        },

        calculate_event: function(e) {
            // calculate product price
            var price = parseInt($("input[name='price']").val()),
                peoples = parseInt($("input[name='peoples']").val()),
                participants = parseInt($("input[name='participants']").val()),
                costs = parseInt($("input[name='costs']").val()),
                number_1 = Math.floor(peoples * price) || 0,
                number_2 = Math.floor((number_1 - costs) / costs * 100) || 0,
                number_3 = Math.floor(participants * price) || 0,
                number_4 = Math.floor((number_3 - costs) / costs * 100) || 0;

            $("input[name='number_1']").val(number_1);
            $("input[name='number_2']").val(number_2);
            $("input[name='number_3']").val(number_3);
            $("input[name='number_4']").val(number_4);
        },

        add_detail: function(e) {
            console.log('add_detail');
            e.preventDefault();
            var parent_view = this;
            var data = {};
            var form_id = $(e.currentTarget).data('form');
            var form_info = $(form_id).serializeObject();
            var model = $(e.currentTarget).data('model');
            switch (model) {
            case "detail":
                data = {
                    model: model,
                    name: $("input[name=detail_name]").val(),
                    value: $("input[name=detail_value]").val()
                };
                $("input[name=detail_name]").val('');
                $("input[name=detail_value]").val('');
                $("input[name=detail_name]").focus();
                $("#detail_list").append(Handlebars.templates.product_detail(data));
                break;
            case "delivery":
                data = {
                    model: model,
                    name: $("input[name=delivery_name]").val(),
                    value: $("input[name=delivery_value]").val()
                };
                $("input[name=delivery_name]").val('');
                $("input[name=delivery_value]").val('');
                $("input[name=delivery_name]").focus();
                $("#delivery_list").append(Handlebars.templates.product_detail(data));
                break;
            }
            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },

        delete_detail: function(e) {
            console.log('delete_detail');
            e.preventDefault();
            if (confirm("確定刪除資料?")) {
                $(e.currentTarget).parent().fadeOut("slow", function() {
                    $(this).remove();
                });
            }
            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },

        delete_file: function(e) {
            console.log('delete_file');
            e.preventDefault();
            var id = $(e.currentTarget).data("id");
            if (confirm("確定刪除資料?")) {
                $.ajax({
                    url: '/restful/product_clothing/file/'+id,
                    dataType: 'json',
                    type: 'DELETE',
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        console.log('刪除成功');
                        RT.show_message('form', 'alert-success', '刪除成功');
                        
                        $(e.currentTarget).parent().fadeOut("slow", function() {
                            $(this).remove();
                        });
                        
                        RT.dialogs.loading('close');
                    },
                    error: function(){
                        RT.show_message('form', 'alert-error', '刪除失敗');
                        RT.dialogs.loading('close');
                    }
                });
            }
            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },
        delete_clothing_file: function(e){
            $.ajax({
                url: '/restful/product_clothing/file/'+$(e.currentTarget).data('id'),
                type: 'DELETE'
            }).then(function(){
                $(e.currentTarget).parent().fadeOut("slow", function() {
                    $(this).remove();
                });
            });
        },
        delete_remarks: function(e) {
            console.log('delete_apps');
            e.preventDefault();
            var parent_view = this,
                form_id = $(e.currentTarget).data('form'),
                id = $(e.currentTarget).data("id");
            if (confirm("確定刪除資料?")) {
                $.ajax({
                    url: RT.API.remarkDelete,
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        'id': id
                    },
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '刪除失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', '刪除成功');
                            $(e.currentTarget).parent().parent().fadeOut("slow", function() {
                                $(e.currentTarget).remove();
                            });
                            RT.dialogs.loading('close');
                        }
                    }
                });
            }
            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },
        delete_all_remarks: function(e) {
            console.log('delete_apps');
            e.preventDefault();
            var parent_view = this,
                form_id = $(e.currentTarget).data('form'),
                form_info = $(form_id).serializeObject(),
                length = $(form_id + " input:checked").length;
            if (length == 0) {
                RT.show_message(form_id, 'alert-error', '尚未選取任何項目');
                e.stopImmediatePropagation();
                return false;
            }
            if (confirm("確定刪除選取資料?")) {
                $.ajax({
                    url: RT.API.remarkDelete,
                    dataType: 'json',
                    type: 'POST',
                    data: form_info,
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '刪除失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', '刪除成功');
                            // remove checkbox tag
                            $(form_id + " input:checked").each(

                            function() {
                                $(this).parent().parent().fadeOut("slow", function() {
                                    $(this).remove();
                                });
                            });
                            RT.dialogs.loading('close');
                        }
                    }
                });
            }
            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },
        add_remarks: function(e) {
            console.log('add_apps');
            e.preventDefault();
            var parent_view = this;
            var form_id = $(e.currentTarget).data('form');
            var model = $(e.currentTarget).data('model');
            var form_info = $(form_id).serializeObject();

            if (confirm("確定新增備註?")) {
                var value = CKEDITOR.instances['remarks'].getData();
                var data = {
                    remarks: value
                };
                $.ajax({
                    url: RT.API.remarkAdd,
                    dataType: 'json',
                    type: 'POST',
                    data: $.extend(form_info, data),
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '新增失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', '新增成功');
                            $.extend(form_info, response.item);
                            $(model + ' > tbody').append('<tr>' + Handlebars.templates.remark_table(form_info) + '</tr>');
                            CKEDITOR.instances.remarks.setData('');
                            RT.update_table();
                            RT.dialogs.loading('close');
                        }
                    }
                });
            }
            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },

        delete_item: function(e) {
            console.log('delete');
            e.preventDefault();
            var id = $(e.currentTarget).data("id"),
                model = $(e.currentTarget).data('model'),
                el;
            if (confirm("確定刪除此筆資料?")) {
                switch (model) {
                case "static_url":
                    el = "#static_url_list_form";
                    $.ajax({
                        url: RT.API.urlDelete,
                        dataType: 'json',
                        type: 'POST',
                        data: {
                            'id': id
                        },
                        beforeSend: function(jqXHR, settings) {
                            RT.dialogs.loading('open');
                        },
                        success: function(response) {
                            if (response.error_text) {
                                RT.show_message(el, 'alert-error', '刪除失敗');
                                RT.dialogs.loading('close');
                            }
                            if (response.success_text) {
                                RT.show_message(el, 'alert-success', '刪除成功');
                                $(e.currentTarget).parent().parent().remove();
                                RT.update_table();
                                RT.dialogs.loading('close');
                            }
                        }
                    });
                    break;
                case "privilege":
                    el = "#privilege_list_form";
                    $.ajax({
                        url: RT.API.aclDelete,
                        dataType: 'json',
                        type: 'POST',
                        data: {
                            'id': id
                        },
                        beforeSend: function(jqXHR, settings) {
                            RT.dialogs.loading('open');
                        },
                        success: function(response) {
                            if (response.error_text) {
                                RT.show_message(el, 'alert-error', '刪除失敗');
                                RT.dialogs.loading('close');
                            }
                            if (response.success_text) {
                                RT.show_message(el, 'alert-success', '刪除成功');
                                $(e.currentTarget).parent().parent().remove();
                                RT.update_table();
                                RT.dialogs.loading('close');
                            }
                        }
                    });
                    break;
                case "user":
                    el = "#user_list";
                    $.ajax({
                        url: RT.API.userDelete,
                        dataType: 'json',
                        type: 'POST',
                        data: {
                            'id': id
                        },
                        beforeSend: function(jqXHR, settings) {
                            RT.dialogs.loading('open');
                        },
                        success: function(response) {
                            if (response.error_text) {
                                RT.show_message(el, 'alert-error', '刪除失敗');
                                RT.dialogs.loading('close');
                            }
                            if (response.success_text) {
                                RT.show_message(el, 'alert-success', '刪除成功');
                                $(e.currentTarget).parent().parent().remove();
                                RT.update_table();
                                RT.dialogs.loading('close');
                            }
                        }
                    });
                    break;
                case "group":
                    el = "#group_list";
                    $.ajax({
                        url: RT.API.groupDelete,
                        dataType: 'json',
                        type: 'POST',
                        data: {
                            'id': id
                        },
                        beforeSend: function(jqXHR, settings) {
                            RT.dialogs.loading('open');
                        },
                        success: function(response) {
                            if (response.error_text) {
                                RT.show_message(el, 'alert-error', '刪除失敗');
                                RT.dialogs.loading('close');
                            }
                            if (response.success_text) {
                                RT.show_message(el, 'alert-success', '刪除成功');
                                $(e.currentTarget).parent().parent().remove();
                                RT.update_table();
                                RT.dialogs.loading('close');
                            }
                        }
                    });
                    break;
                case "category":
                    el = "#category_list";
                    $.ajax({
                        url: RT.API.categoryDelete,
                        dataType: 'json',
                        type: 'POST',
                        data: {
                            'id': id
                        },
                        beforeSend: function(jqXHR, settings) {
                            RT.dialogs.loading('open');
                        },
                        success: function(response) {
                            if (response.error_text) {
                                RT.show_message(el, 'alert-error', '刪除失敗');
                                RT.dialogs.loading('close');
                            }
                            if (response.success_text) {
                                RT.show_message(el, 'alert-success', '刪除成功');
                                $(e.currentTarget).parent().parent().remove();
                                RT.update_table();
                                RT.dialogs.loading('close');
                            }
                        }
                    });
                    break;
                case "event":
                    el = "#event_list";
                    $.ajax({
                        url: RT.API.eventDelete,
                        dataType: 'json',
                        type: 'POST',
                        data: {
                            'id': id
                        },
                        beforeSend: function(jqXHR, settings) {
                            RT.dialogs.loading('open');
                        },
                        success: function(response) {
                            if (response.error_text) {
                                RT.show_message(el, 'alert-error', '刪除失敗');
                                RT.dialogs.loading('close');
                            }
                            if (response.success_text) {
                                RT.show_message(el, 'alert-success', '刪除成功');
                                $(e.currentTarget).parent().parent().remove();
                                RT.update_table();
                                RT.dialogs.loading('close');
                            }
                        }
                    });
                    break;
                case "subject":
                    el = "#subject_list";
                    $.ajax({
                        url: RT.API.subjectDelete,
                        dataType: 'json',
                        type: 'POST',
                        data: {
                            'id': id
                        },
                        beforeSend: function(jqXHR, settings) {
                            RT.dialogs.loading('open');
                        },
                        success: function(response) {
                            if (response.error_text) {
                                RT.show_message(el, 'alert-error', '刪除失敗');
                                RT.dialogs.loading('close');
                            }
                            if (response.success_text) {
                                RT.show_message(el, 'alert-success', '刪除成功');
                                $(e.currentTarget).parent().parent().remove();
                                RT.update_table();
                                RT.dialogs.loading('close');
                            }
                        }
                    });
                    break;
                case "product":
                    el = "#product_list";
                    $.ajax({
                        url: RT.API.productDelete,
                        dataType: 'json',
                        type: 'POST',
                        data: {
                            'id': id
                        },
                        beforeSend: function(jqXHR, settings) {
                            RT.dialogs.loading('open');
                        },
                        success: function(response) {
                            if (response.error_text) {
                                RT.show_message(el, 'alert-error', '刪除失敗');
                                RT.dialogs.loading('close');
                            }
                            if (response.success_text) {
                                RT.show_message(el, 'alert-success', '刪除成功');
                                $(e.currentTarget).parent().parent().remove();
                                RT.update_table();
                                RT.dialogs.loading('close');
                            }
                        }
                    });
                    break;
                }
            }
            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },

        delete_all: function(e) {
            console.log('delete all');
            e.preventDefault();
            var form_id = $(e.currentTarget).data('form'),
                form_info = $(form_id).serializeObject(),
                model = $(e.currentTarget).data('model'),
                length = $("input:checked").length;
            if (length == 0) {
                RT.show_message(form_id, 'alert-error', '尚未選取任何項目');
                e.stopImmediatePropagation();
                return false;
            }

            if (confirm("確定刪除選取資料?")) {
                switch (model) {
                case "privilege":
                    $.ajax({
                        url: RT.API.aclDelete,
                        dataType: 'json',
                        type: 'POST',
                        data: form_info,
                        beforeSend: function(jqXHR, settings) {
                            RT.dialogs.loading('open');
                        },
                        success: function(response) {
                            if (response.error_text) {
                                RT.show_message(form_id, 'alert-error', '刪除失敗');
                                RT.dialogs.loading('close');
                            }
                            if (response.success_text) {
                                RT.show_message(form_id, 'alert-success', '刪除成功');
                                // remove checkbox tag
                                $("input:checked").each(

                                function() {
                                    $(this).parent().parent().remove();
                                });
                                RT.update_table();
                                RT.dialogs.loading('close');
                            }
                        }
                    });
                    break;
                case "user":
                    $.ajax({
                        url: RT.API.userDelete,
                        dataType: 'json',
                        type: 'POST',
                        data: form_info,
                        beforeSend: function(jqXHR, settings) {
                            RT.dialogs.loading('open');
                        },
                        success: function(response) {
                            if (response.error_text) {
                                RT.show_message(form_id, 'alert-error', '刪除失敗');
                                RT.dialogs.loading('close');
                            }
                            if (response.success_text) {
                                RT.show_message(form_id, 'alert-success', '刪除成功');
                                // remove checkbox tag
                                $("input:checked").each(

                                function() {
                                    $(this).parent().parent().remove();
                                });
                                RT.update_table();
                                RT.dialogs.loading('close');
                            }
                        }
                    });
                    break;
                case "group":
                    $.ajax({
                        url: RT.API.groupDelete,
                        dataType: 'json',
                        type: 'POST',
                        data: form_info,
                        beforeSend: function(jqXHR, settings) {
                            RT.dialogs.loading('open');
                        },
                        success: function(response) {
                            if (response.error_text) {
                                RT.show_message(form_id, 'alert-error', '刪除失敗');
                                RT.dialogs.loading('close');
                            }
                            if (response.success_text) {
                                RT.show_message(form_id, 'alert-success', '刪除成功');
                                // remove checkbox tag
                                $("input:checked").each(

                                function() {
                                    $(this).parent().parent().remove();
                                });
                                RT.update_table();
                                RT.dialogs.loading('close');
                            }
                        }
                    });
                    break;
                case "category":
                    $.ajax({
                        url: RT.API.categoryDelete,
                        dataType: 'json',
                        type: 'POST',
                        data: form_info,
                        beforeSend: function(jqXHR, settings) {
                            RT.dialogs.loading('open');
                        },
                        success: function(response) {
                            if (response.error_text) {
                                RT.show_message(form_id, 'alert-error', '刪除失敗');
                                RT.dialogs.loading('close');
                            }
                            if (response.success_text) {
                                RT.show_message(form_id, 'alert-success', '刪除成功');
                                // remove checkbox tag
                                $("input:checked").each(

                                function() {
                                    $(this).parent().parent().remove();
                                });
                                RT.update_table();
                                RT.dialogs.loading('close');
                            }
                        }
                    });
                    break;
                case "subject":
                    $.ajax({
                        url: RT.API.subjectDelete,
                        dataType: 'json',
                        type: 'POST',
                        data: form_info,
                        beforeSend: function(jqXHR, settings) {
                            RT.dialogs.loading('open');
                        },
                        success: function(response) {
                            if (response.error_text) {
                                RT.show_message(form_id, 'alert-error', '刪除失敗');
                                RT.dialogs.loading('close');
                            }
                            if (response.success_text) {
                                RT.show_message(form_id, 'alert-success', '刪除成功');
                                // remove checkbox tag
                                $("input:checked").each(

                                function() {
                                    $(this).parent().parent().remove();
                                });
                                RT.update_table();
                                RT.dialogs.loading('close');
                            }
                        }
                    });
                    break;
                case "product":
                    $.ajax({
                        url: RT.API.productDelete,
                        dataType: 'json',
                        type: 'POST',
                        data: form_info,
                        beforeSend: function(jqXHR, settings) {
                            RT.dialogs.loading('open');
                        },
                        success: function(response) {
                            if (response.error_text) {
                                RT.show_message(form_id, 'alert-error', '刪除失敗');
                                RT.dialogs.loading('close');
                            }
                            if (response.success_text) {
                                RT.show_message(form_id, 'alert-success', '刪除成功');
                                // remove checkbox tag
                                $("input:checked").each(

                                function() {
                                    $(this).parent().parent().remove();
                                });
                                RT.update_table();
                                RT.dialogs.loading('close');
                            }
                        }
                    });
                    break;
                case "static_url":
                    $.ajax({
                        url: RT.API.urlDelete,
                        dataType: 'json',
                        type: 'POST',
                        data: form_info,
                        beforeSend: function(jqXHR, settings) {
                            RT.dialogs.loading('open');
                        },
                        success: function(response) {
                            if (response.error_text) {
                                RT.show_message(form_id, 'alert-error', '刪除失敗');
                                RT.dialogs.loading('close');
                            }
                            if (response.success_text) {
                                RT.show_message(form_id, 'alert-success', '刪除成功');
                                // remove checkbox tag
                                $("input:checked").each(
                                function() {
                                    $(this).parent().parent().remove();
                                });
                                RT.update_table();
                                RT.dialogs.loading('close');
                            }
                        }
                    });
                case "event":
                    $.ajax({
                        url: RT.API.eventDelete,
                        dataType: 'json',
                        type: 'POST',
                        data: form_info,
                        beforeSend: function(jqXHR, settings) {
                            RT.dialogs.loading('open');
                        },
                        success: function(response) {
                            if (response.error_text) {
                                RT.show_message(form_id, 'alert-error', '刪除失敗');
                                RT.dialogs.loading('close');
                            }
                            if (response.success_text) {
                                RT.show_message(form_id, 'alert-success', '刪除成功');
                                // remove checkbox tag
                                $("input:checked").each(
                                function() {
                                    $(this).parent().parent().remove();
                                });
                                RT.update_table();
                                RT.dialogs.loading('close');
                            }
                        }
                    });
                    break;
                }
            }
            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },

        add: function(e) {
            console.log('add');
            e.preventDefault();
            var form_id = $(e.currentTarget).data('form'),
                form_info = $(form_id).serializeObject(),
                model = $(e.currentTarget).data('model'),
                name, data;
            $('.control-group').removeClass('error');
            $('.help-inline').text('');
            switch (model) {
            case 'static_url':
                form_id = "#static_url_form";
                form_info = $(form_id).serializeObject();
                data = {
                    content: CKEDITOR.instances['content'].getData()
                };
                $.ajax({
                    url: RT.API.urlAdd,
                    dataType: 'json',
                    type: 'POST',
                    data: $.extend(form_info, data),
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '新增失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', form_info.title + ' 新增成功');
                            RT.dialogs.loading('close');
                        }
                        window.location = "#!/static_url/list";
                    }
                });
                break;
            case 'privilege':
                form_id = "#privilege_form";
                form_info = $(form_id).serializeObject();
                for (name in form_info) {
                    if (!$.trim(form_info[name])) {
                        $('input[name=' + name + ']').parent().addClass('error');
                        RT.show_message(form_id, 'alert-error', '紅色欄位務必填寫');
                        e.stopImmediatePropagation();
                        return false;
                    }
                }
                var title = $(':input[name=parent_id]').find('option:selected').data('module') + '/' + form_info.resource_name;
                $.ajax({
                    url: RT.API.aclAdd,
                    dataType: 'json',
                    type: 'POST',
                    data: form_info,
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '新增失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', title + ' 新增成功');
                            RT.dialogs.loading('close');
                        }
                    }
                });
                break;
            case 'category':
                form_id = "#category_form";
                form_info = $(form_id).serializeObject();
                for (name in form_info) {
                    if (!$.trim(form_info[name])) {
                        $('input[name=' + name + ']').parent().addClass('error');
                        RT.show_message(form_id, 'alert-error', '紅色欄位務必填寫');
                        e.stopImmediatePropagation();
                        return false;
                    }
                }

                $.ajax({
                    url: RT.API.categoryAdd,
                    dataType: 'json',
                    type: 'POST',
                    data: form_info,
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '新增失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', form_info.tw_name + ' 新增成功');
                            RT.dialogs.loading('close');
                            window.location = "#!/category/list/" + form_info.type;
                        }
                    }
                });
                break;
            case 'group':
                form_id = "#group_form";
                form_info = $(form_id).serializeObject();
                for (name in form_info) {
                    if (!$.trim(form_info[name])) {
                        $('input[name=' + name + ']').parent().addClass('error');
                        RT.show_message(form_id, 'alert-error', '紅色欄位務必填寫');
                        e.stopImmediatePropagation();
                        return false;
                    }
                }

                RT.api_req.POST(RT.API.groupAdd, form_info, function(response) {
                    if (response.error_text) {
                        RT.show_message(form_id, 'alert-error', '新增失敗');
                        RT.dialogs.loading('close');
                    }
                    if (response.success_text) {
                        RT.show_message(form_id, 'alert-success', form_info.name + ' 新增成功');
                        RT.dialogs.loading('close');
                    }
                });
                break;
            case 'user':
                form_id = "#user_add_form";
                form_info = $("#user_add_form").serializeObject();
                if (!$.trim(form_info.email) || !$.trim(form_info.password)) {
                    for (name in form_info) {
                        if (!$.trim(form_info[name])) {
                            $(form_id + ' input[name=' + name + ']').parent().parent().addClass('error');
                            RT.show_message(form_id, 'alert-error', '紅色欄位務必填寫');
                        }
                    }
                    e.stopImmediatePropagation();
                    return false;
                }
                if ($.trim(form_info.password) != $.trim(form_info.confirm_password)) {
                    $(form_id + ' input[name=password]').parent().parent().addClass('error');
                    $(form_id + ' input[name=confirm_password]').parent().parent().addClass('error');
                    RT.show_message(form_id, 'alert-error', '登入密碼跟確認密碼必須相等');
                    e.stopImmediatePropagation();
                    return false;
                }

                $.ajax({
                    url: RT.API.userAdd,
                    dataType: 'json',
                    type: 'POST',
                    data: form_info,
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '帳號新增失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.user_id) {
                            RT.show_message(form_id, 'alert-success', form_info.email + ' 帳號新增成功');
                            $('input[type=text], input[type=password]').val('');
                            RT.dialogs.loading('close');
                            window.location = "#!/user/list";
                        }
                    }
                });
                break;
            case 'subject':
                var value = CKEDITOR.instances['content'].getData();
                data = {
                    content: value
                };
                if (!$.trim(form_info.title) || !$.trim(form_info.description)) {
                    for (name in form_info) {
                        if (!$.trim(form_info[name])) {
                            $(form_id + ' input[name=' + name + ']').parent().parent().addClass('error');
                            RT.show_message(form_id, 'alert-error', '紅色欄位務必填寫');
                        }
                    }
                    e.stopImmediatePropagation();
                    return false;
                }
                $.ajax({
                    url: RT.API.subjectAdd,
                    dataType: 'json',
                    type: 'POST',
                    data: $.extend(form_info, data),
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '新增失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', form_info.title + ' 新增成功');
                            RT.dialogs.loading('close');
                            window.location = "#!/subject/list";
                        }
                    }
                });
                break;
            case 'event':
                var content = CKEDITOR.instances['content'].getData();
                data = {
                    content: content
                };
                if (!$.trim(form_info.title) || !$.trim(form_info.description)) {
                    for (name in form_info) {
                        if (!$.trim(form_info[name])) {
                            $(form_id + ' input[name=' + name + ']').parent().parent().addClass('error');
                            RT.show_message(form_id, 'alert-error', '紅色欄位務必填寫');
                        }
                    }
                    e.stopImmediatePropagation();
                    return false;
                }
                $.ajax({
                    url: RT.API.eventAdd,
                    dataType: 'json',
                    type: 'POST',
                    data: $.extend(form_info, data),
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '新增失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', form_info.title + ' 新增成功');
                            RT.dialogs.loading('close');
                            window.location = "#!/event/list";
                        }
                    }
                });
                break;
            case 'product':
                var introduction = CKEDITOR.instances['introduction'].getData();
                var information = CKEDITOR.instances['information'].getData();
                data = {
                    information: information,
                    introduction: introduction
                };
                if (!$.trim(form_info.title) || !$.trim(form_info.description)) {
                    for (name in form_info) {
                        if (!$.trim(form_info[name])) {
                            $(form_id + ' input[name=' + name + ']').parent().parent().addClass('error');
                            RT.show_message(form_id, 'alert-error', '紅色欄位務必填寫');
                        }
                    }
                    e.stopImmediatePropagation();
                    return false;
                }
                $.ajax({
                    url: RT.API.productAdd,
                    dataType: 'json',
                    type: 'POST',
                    data: $.extend(form_info, data),
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '新增失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', form_info.title + ' 新增成功');
                            RT.dialogs.loading('close');
                            window.location = "#!/product/list";
                        }
                    }
                });
                break;
            }
            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },

        edit: function(e) {
            console.log('edit');
            e.preventDefault();
            var form_id = $(e.currentTarget).data('form'),
                form_info = $(form_id).serializeObject(),
                model = $(e.currentTarget).data('model'),
                name, data, value, introduction, content, information;
            $('.control-group').removeClass('error');
            $('.help-inline').text('');
            switch (model) {
            case 'static_url':
                form_id = "#static_url_form";
                form_info = $(form_id).serializeObject();
                data = {
                    content: CKEDITOR.instances['content'].getData()
                };

                $.ajax({
                    url: RT.API.urlEdit,
                    dataType: 'json',
                    type: 'POST',
                    data: $.extend(form_info, data),
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        console.log(response);
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '修改失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', form_info.title + ' 修改成功');
                            RT.dialogs.loading('close');
                        }
                    }
                });
                break;
            case 'category':
                form_id = "#category_form";
                form_info = $(form_id).serializeObject();
                for (name in form_info) {
                    if (!$.trim(form_info[name])) {
                        $('input[name=' + name + ']').parent().addClass('error');
                        RT.show_message(form_id, 'alert-error', '紅色欄位務必填寫');
                        e.stopImmediatePropagation();
                        return false;
                    }
                }

                $.ajax({
                    url: RT.API.categoryEdit,
                    dataType: 'json',
                    type: 'POST',
                    data: form_info,
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        console.log(response);
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '修改失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', form_info.tw_name + ' 修改成功');
                            RT.dialogs.loading('close');
                        }
                    }
                });
                break;
            case 'config':
                $.ajax({
                    url: RT.API.configEdit,
                    dataType: 'json',
                    type: 'POST',
                    data: form_info,
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        console.log(response);
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '修改失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', '修改成功');
                            RT.dialogs.loading('close');
                        }
                    }
                });
                break;
            case 'group':
                form_id = "#group_form";
                form_info = $(form_id).serializeObject();
                for (name in form_info) {
                    if (!$.trim(form_info[name])) {
                        $('input[name=' + name + ']').parent().addClass('error');
                        RT.show_message(form_id, 'alert-error', '紅色欄位務必填寫');
                        e.stopImmediatePropagation();
                        return false;
                    }
                }

                $.ajax({
                    url: RT.API.groupEdit,
                    dataType: 'json',
                    type: 'POST',
                    data: form_info,
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '修改失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', form_info.name + ' 修改成功');
                            RT.dialogs.loading('close');
                        }
                    }
                });
                break;
            case 'user':
                form_id = "#user_add_form";
                form_info = $(form_id).serializeObject();
                form_info.p_address = JSON.stringify({
                    county: form_info.county[0],
                    district: form_info.district[0],
                    zipcode: form_info.zipcode[0],
                    address: form_info.address[0]
                });
                form_info.s_address = JSON.stringify({
                    county: form_info.county[1],
                    district: form_info.district[1],
                    zipcode: form_info.zipcode[1],
                    address: form_info.address[1]
                });
                if ($.trim(form_info.password) !== '' && $.trim(form_info.password) !== $.trim(form_info.confirm_password)) {
                    $(form_id + ' input[name=password]').parent().addClass('error');
                    $(form_id + ' input[name=confirm_password]').parent().addClass('error');
                    RT.show_message(form_id, 'alert-error', '修改密碼跟確認密碼必須相等');
                    e.stopImmediatePropagation();
                    return false;
                }
                $.ajax({
                    url: RT.API.userEdit,
                    dataType: 'json',
                    type: 'POST',
                    data: form_info,
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '修改失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', form_info.email + ' 修改成功');
                            RT.dialogs.loading('close');
                        }
                    }
                });
                break;
            case 'subject':
                value = CKEDITOR.instances['content'].getData();
                data = {
                    content: value
                };
                if (!$.trim(form_info.title) || !$.trim(form_info.description)) {
                    for (name in form_info) {
                        if (!$.trim(form_info[name])) {
                            $(form_id + ' input[name=' + name + ']').parent().parent().addClass('error');
                            RT.show_message(form_id, 'alert-error', '紅色欄位務必填寫');
                        }
                    }
                    e.stopImmediatePropagation();
                    return false;
                }

                $.ajax({
                    url: RT.API.subjectEdit,
                    dataType: 'json',
                    type: 'POST',
                    data: $.extend(form_info, data),
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '修改失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', form_info.title + ' 修改成功');
                            RT.dialogs.loading('close');
                        }
                    }
                });
                break;
            case 'event':
                content = CKEDITOR.instances['content'].getData();
                data = {
                    content: content
                };
                if (!$.trim(form_info.title) || !$.trim(form_info.description)) {
                    for (name in form_info) {
                        if (!$.trim(form_info[name])) {
                            $(form_id + ' input[name=' + name + ']').parent().parent().addClass('error');
                            RT.show_message(form_id, 'alert-error', '紅色欄位務必填寫');
                        }
                    }
                    e.stopImmediatePropagation();
                    return false;
                }

                $.ajax({
                    url: RT.API.eventEdit,
                    dataType: 'json',
                    type: 'POST',
                    data: $.extend(form_info, data),
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '修改失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', form_info.title + ' 修改成功');
                            RT.dialogs.loading('close');
                        }
                    }
                });
                break;
            case 'product':
                introduction = CKEDITOR.instances['introduction'].getData();
                information = CKEDITOR.instances['information'].getData();
                data = {
                    information: information,
                    introduction: introduction
                };
                if (!$.trim(form_info.title) || !$.trim(form_info.description)) {
                    for (name in form_info) {
                        if (!$.trim(form_info[name])) {
                            $(form_id + ' input[name=' + name + ']').parent().parent().addClass('error');
                            RT.show_message(form_id, 'alert-error', '紅色欄位務必填寫');
                        }
                    }
                    e.stopImmediatePropagation();
                    return false;
                }

                $.ajax({
                    url: RT.API.productEdit,
                    dataType: 'json',
                    type: 'POST',
                    data: $.extend(form_info, data),
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '修改失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', form_info.title + ' 修改成功');
                            RT.dialogs.loading('close');
                        }
                    }
                });
                break;

            case 'order':
                if (!$.trim(form_info.user_name) || !$.trim(form_info.delivery_number)) {
                    for (name in form_info) {
                        if (!$.trim(form_info[name])) {
                            $(form_id + ' input[name=' + name + ']').parent().parent().addClass('error');
                            RT.show_message(form_id, 'alert-error', '紅色欄位務必填寫');
                        }
                    }
                    e.stopImmediatePropagation();
                    return false;
                }

                $.ajax({
                    url: RT.API.orderEdit,
                    dataType: 'json',
                    type: 'POST',
                    data: form_info,
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            RT.show_message(form_id, 'alert-error', '修改失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            RT.show_message(form_id, 'alert-success', '修改成功');
                            RT.dialogs.loading('close');
                        }
                    }
                });
                break;
            }
            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },

        render: function() {
            var parent_view = this;
            if (!this.options.el) {
                $('body').append(this.el);
            }
            var template_name = this.options.template_name || this.template_name;
            var data = this.options.data || {};
            var is_table = this.options.is_table || false;
            var is_append = this.options.is_append || false;
            var display = (this.options.display === false) ? false : true;
            if (!template_name) {
                console.log("Error: Could not find template");
                return;
            }
            if (this.model && this.collection) {
                console.log("Both model and collection provided. Please override render() or mustache()");
            } else if (this.model) {
                $.extend(this.model.attributes, data);
                if (is_append) {
                    $(this.el).append(Handlebars.templates[template_name](this.model.attributes));
                } else {
                    $(this.el).html(Handlebars.templates[template_name](this.model.attributes));
                }
            } else if (this.collection) {
                $(this.el).empty();
                var parent_view = this;
                this.collection.each(function(item) {
                    var child_view = new RT.View({
                        tagName: (is_table) ? 'tr' : '',
                        template_name: template_name,
                        model: item
                    });
                    $(parent_view.el).append(child_view.render().el);
                });
            } else {
                $(this.el).hide().html(Handlebars.templates[template_name](data));
            }

            if (is_table) RT.update_table();
            if (display) this.$el.fadeIn("slow");
            if (display) RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.View;
});
