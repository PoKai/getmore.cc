define([
    'jquery',
    'underscore',
    'backbone',
    'views/view',
    'models/remark'
    ], function($, _, Backbone, View, ModelRemark) {
    RT.ViewProduct = View.extend({

        initialize: function() {
            if (this.model) {
                this.model.on("change", this.render, this);
            }
            if (this.options.subject_collection) {
                this.options.subject_collection.on("reset", this.insert_option, this);
            }
        },

        insert_option: function(e) {
            var parent_view = this,
                subject_id = parent_view.model.attributes.subject_id,
                subject_option = '',
                key, value, selected;
            this.options.subject_collection.each(function(item) {
                key = item.get('id');
                value = item.get('title');
                selected = (key == subject_id) ? 'selected' : '';
                subject_option += '<option value="' + key + '" ' + selected + '>' + value + '</optiion>';
            });
            $("#subject_id").html('');
            $("#subject_id").append(subject_option);
        },

        render: function(e) {
            var parent_view = this;
            var subject_collection = this.options.subject_collection || this.subject_collection;
            var data = this.options.data || {};
            var product_id = parent_view.model.attributes.id;
            $(parent_view.el).empty();

            // calculate price
            var sell_price = parent_view.model.attributes.sell_price;
            var stock_price = parent_view.model.attributes.stock_price;
            var original_price = parent_view.model.attributes.original_price;
            var number_1 = Math.floor(100 - sell_price / original_price * 100);
            var number_2 = Math.floor((sell_price - stock_price) / stock_price * 100);

            parent_view.model.attributes.is_enabled = (parent_view.model.attributes.options & RT.Flags._IS_ENABLED) ? true : false;
            $.extend(data, {
                number_1: number_1,
                number_2: number_2
            });
            $.extend(data, parent_view.model.attributes);
            $(parent_view.el).hide().html(Handlebars.templates.product_edit(data)).fadeIn("slow");
            // get subject list
            subject_collection.fetch();
            // product remarks
            parent_view.remark = new ModelRemark({
                type: 'product',
                r_id: product_id
            });
            parent_view.remarks = parent_view.remark.lists;
            parent_view.remarks.fetch();
            new View({
                is_table: true,
                template_name: 'remark_table',
                el: '#product_remarks > tbody',
                collection: parent_view.remarks
            });
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewProduct;
});