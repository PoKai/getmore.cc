define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewProducts = View.extend({

        events: _.extend({
            'click .enable_all': 'enable_all',
            'click .disable_all': 'disable_all',
            'click .search_product': 'search'
        }, View.prototype.events),

        params: {},

        search: function(e) {
            (this.debug) && console.log('search');
            e.preventDefault();
            var parent_view = this,
                form_id = $(e.currentTarget).data('form');
            this.params = $(form_id).serializeObject();

            parent_view.options.model_name.set_params(this.params);
            parent_view.collection.fetch();
            e.stopImmediatePropagation();
            return false;
        },

        enable_all: function(e) {
            console.log('enable_all');
            e.preventDefault();
            var parent_view = this;
            var form_id = $(e.currentTarget).data('form');
            var form_info = $(form_id).serializeObject();
            parent_view.form_info = form_info;
            parent_view.form_id = form_id;
            parent_view.check_select();
            $.extend(form_info, {
                is_enabled: 1
            });
            $("input:checked").each(

            function() {
                $(this).parent().parent().find('.is_enabled').html('<span style="color:blue">是</span>');
            });
            $("input:checked").length > 0 && parent_view.update_status();
            e.stopImmediatePropagation();
            return false;
        },

        disable_all: function(e) {
            console.log('disable_all');
            e.preventDefault();
            var parent_view = this;
            var form_id = $(e.currentTarget).data('form');
            var form_info = $(form_id).serializeObject();
            $.extend(form_info, {
                is_enabled: 0
            });
            parent_view.form_info = form_info;
            parent_view.form_id = form_id;
            parent_view.check_select();
            $("input:checked").each(

            function() {
                $(this).parent().parent().find('.is_enabled').html('<span style="color:red">否</span>');
            });
            $("input:checked").length > 0 && parent_view.update_status();
            e.stopImmediatePropagation();
            return false;
        },

        check_select: function() {
            var parent_view = this;
            var length = $("input:checked").length;
            if (length == 0) {
                RT.show_message(this.form_id, 'alert-error', '尚未選取任何項目');
                return false;
            }
        },

        update_status: function(e) {
            var parent_view = this;
            $.ajax({
                url: RT.API.productEdit,
                dataType: 'json',
                type: 'POST',
                data: parent_view.form_info,
                beforeSend: function(jqXHR, settings) {
                    RT.dialogs.loading('open');
                },
                success: function(response) {
                    if (response.error_text) {
                        RT.show_message(parent_view.form_id, 'alert-error', '更改失敗');
                        RT.dialogs.loading('close');
                    }
                    if (response.success_text) {
                        RT.show_message(parent_view.form_id, 'alert-success', '更改成功');
                        RT.dialogs.loading('close');
                    }
                }
            });
        },
        // implement render function
        render: function() {
            var parent_view = this;
            var data = this.options.data || {};
            $(parent_view.el).empty();
            var boxes = [];
            data.items = [];
            this.collection.each(function(item) {
                var is_enabled = +item.get('is_enabled');
                item.set('is_enabled', is_enabled, {
                    silent: true
                });
                data.items.push(item.attributes);
            });

            $.extend(data, this.params);
            $(parent_view.el).hide().html(Handlebars.templates.product_list(data)).fadeIn("slow");
            RT.update_table();
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewProducts;
});