define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewProductAdd = View.extend({

        initialize: function() {
            if (this.options.subject_collection) {
                this.options.subject_collection.on("reset", this.insert_option, this);
            }
        },

        insert_option: function() {
            var parent_view = this,
                subject_option = '',
                key, value;
            parent_view.options.subject_collection.each(function(item) {
                key = item.get('id');
                value = item.get('title');
                subject_option += '<option value="' + key + '">' + value + '</optiion>';
            });
            $("#subject_id").html('');
            $("#subject_id").append(subject_option);
        },

        render: function(e) {
            var parent_view = this;
            var data = this.options.data || {};
            $(parent_view.el).empty();

            $.extend(data, {
                is_add: true
            });
            $(parent_view.el).hide().html(Handlebars.templates.product_edit(data)).fadeIn("slow");

            // get subject options
            this.options.subject_collection.fetch();
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewProductAdd;
});