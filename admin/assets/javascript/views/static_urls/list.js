define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewStaticUrls = View.extend({

        events: _.extend({
            'click .search_static_url': 'search'
        }, View.prototype.events),

        params: {},

        initialize: function() {
            var parent_view = this;
            var model_name = parent_view.options.model_name || parent_view.model_name;

            if (this.model) {
                this.model.on("reset", this.render, this);
            }
            if (this.collection) {
                this.collection.on("reset", this.render, this);
            }
        },

        search: function(e) {
            (this.debug) && console.log('search');
            console.log('search');
            e.preventDefault();
            var parent_view = this,
                form_id = $(e.currentTarget).data('form');
            this.params = $(form_id).serializeObject();

            parent_view.options.model_name.set_params(this.params);
            parent_view.collection.fetch();
            e.stopImmediatePropagation();
            return false;
        },

        // implement render function
        render: function() {
            var parent_view = this,
                data = this.options.data || {};
            $(parent_view.el).empty();
            data.items = [];
            this.collection.each(function(item) {
                data.items.push(item.attributes);
            });

            $.extend(data, this.params);
            $(parent_view.el).hide().html(Handlebars.templates.static_url_list(data)).fadeIn("slow");

            RT.update_table();
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewStaticUrls;
});