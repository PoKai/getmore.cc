define([
    'jquery',
    'underscore',
    'backbone',
    'views/view',
    'models/static_url'
    ], function($, _, Backbone, View, ModelStaticUrl) {
    RT.ViewStaticUrl = View.extend({

        initialize: function() {
            if (this.model) {
                this.model.on("change", this.render, this);
            }

            if (this.model.status) {
                this.model.status.on("reset", this.insert_option, this);
            }
        },

        render: function(e) {
            var parent_view = this,
                data = {};
            $(parent_view.el).empty();

            $.extend(data, parent_view.model.attributes);
            $(parent_view.el).hide().html(Handlebars.templates.static_url_edit(data)).fadeIn("slow");

            parent_view.model.status.fetch();
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewStaticUrl;
});