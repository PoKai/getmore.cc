define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    var view  = View.extend({

        events: _.extend({
            'click .size': 'click_size',
            'change .other_size_type': 'change_other_size_type',
            'change .other_size_number': 'change_other_size_number',
            'click .add_size': 'add_size',            
            'click .size_clear1': 'size_clear1',
            'click .size_clear2': 'size_clear2'            
        }, View.prototype.events),
        
        add_size: function(e){
            e.preventDefault();
            console.log('add size');
            // Maximum is two sizes
            if ( ($('.size_type2').val() !== '') || ($('.size_number2').val() !== '') ){
                alert('最多只能填兩種尺寸。');
                return false;
            }
            
            // Already set size 1
            if ($('.size_standard1').val() !== ''){
                $('.size_type2').val($('.size_type').html());
                $('.size_number2').val($('.size_number').html());                            
            }
            // Not set size 1 yet
            else{
                $('.size_type1').val($('.size_type').html());
                $('.size_number1').val($('.size_number').html());                                          
                $('.size_standard1').val($('.size_standard').html());                                          
            }
            
            $('.size_type').html('');
            $('.size_number').html('');
            $('.size_standard').html('');
            
        },
        
        size_clear1: function(e){
            e.preventDefault();
            $('.size_type1').val('');
            $('.size_number1').val('');
            $('.size_standard1').val('');            
        },
        
        size_clear2: function(e){
            e.preventDefault();
            $('.size_type2').val('');
            $('.size_number2').val('');
        },
        
        click_size: function(e){
            e.preventDefault();
            var $this = $(e.currentTarget);
            if ($this.data('type') !== undefined){
                $('.size_type').html($this.data('type'));
            }
            if ($this.data('number') !== undefined){
                $('.size_number').html($this.data('number'));
            }
            if ($this.data('standard') !== undefined){
                $('.size_standard').html($this.data('standard'));
            }            
        },
        change_other_size_type: function(e){
                $('.size_type').html($('.other_size_type').val());          
        },
        change_other_size_number: function(e){
                $('.size_number').html($('.other_size_number').val());                    
        }        
        
    });
    
    return view;
    
});
