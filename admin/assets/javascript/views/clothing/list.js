define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewShopping = View.extend({

        events: _.extend({
        }, View.prototype.events),

        initialize: function() {
            var parent_view = this;
            var model_name = parent_view.options.model_name || parent_view.model_name;

            if (this.model) {
                this.model.on("reset", this.render, this);
            }
            if (this.collection) {
                this.collection.on("reset", this.render, this);
            }
        },

        // implement render function
        render: function() {
            var parent_view = this,
                data = this.options.data || {};
            $(parent_view.el).empty();
            data.items = [];
            this.collection.each(function(item) {

                // add some labels for displaying
                if ((item.attributes.lock_status)=='0'){
                    item.attributes.lock_status_label = "未售出";
                }
                else if ((item.attributes.lock_status)=='1'){
                    item.attributes.lock_status_label = "待付款";
                }
                else if ((item.attributes.lock_status)=='2'){
                    item.attributes.lock_status_label = "已售出";
                }
                
                var date = new Date(item.attributes.add_time*1000);
                item.attributes.add_time_tag  = date.getFullYear() + '-' 
                + (date.getMonth()+1) + '-' + date.getDate() + ' '+
                date.getHours() + ':' + date.getMinutes();
                
                data.items.push(item.attributes);
            });

            $.extend(data, this.params);
            $(parent_view.el).hide().html(Handlebars.templates.clothing_list(data)).fadeIn("slow");
            RT.update_table();
            RT.dialogs.loading('close');
            $("#shopping_list").tablesorter(); 

            $(".barinfo").html("共 "+data.items.length+" 件商品已上架");
	
            return this;
        }
    });
    return RT.ViewShopping;
});
