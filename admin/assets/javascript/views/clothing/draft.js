define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewShopping = View.extend({

        events: _.extend({
            'click #launch': 'launch',
            'click #toggleall': 'toggleall'
        }, View.prototype.events),

        initialize: function() {
            var parent_view = this;
            var model_name = parent_view.options.model_name || parent_view.model_name;
            
            if (this.model) {
                this.model.on("reset", this.render, this);
            }
            if (this.collection) {
                this.collection.on("reset", this.render, this);
            }
        },
        launch: function(e){
            if(!$('input.ready:checked').length) return false;
            var ready_for_launch = $('input.ready:checked');
            var draft_ids = $.map(ready_for_launch, function(input){
                return $(input).val();   
            });
            $.ajax({
                url: RT.API.launchDraftClothing,
                data: {'draft_ids': draft_ids},
                type: 'POST',
                success: function(response){
                    ready_for_launch.parents('tr').remove();
                    $(".barinfo").html("共 "+$('input.ready').length+" 件商品待上架");
                }
            });
        },
        toggleall: function(e){
            if(!$('input.ready:not(:checked)').length) {
                $('input.ready').removeAttr('checked');
            }
            else {
                $('input.ready').attr('checked', 'checked');
            }
        },
        // implement render function
        render: function() {
            var parent_view = this,
                data = this.options.data || {};
            $(parent_view.el).empty();
            data.items = [];
            this.collection.each(function(item) {
                var date = new Date(item.attributes.add_time*1000);
                item.attributes.add_time_tag  = date.getFullYear() + '-' 
                + (date.getMonth()+1) + '-' + date.getDate() + ' '+
                date.getHours() + ':' + date.getMinutes();
                
                data.items.push(item.attributes);
            });

            $.extend(data, this.params);
            $(parent_view.el).hide().html(Handlebars.templates.clothing_draft(data)).fadeIn("slow");
            RT.update_table();
            RT.dialogs.loading('close');
            $("#shopping_list").tablesorter({ headers: { 0: { sorter: false} } }); 

            $(".barinfo").html("共 "+data.items.length+" 件商品待上架");
    
            return this;
        }
    });
    return RT.ViewShopping;
});
