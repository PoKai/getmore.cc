define([
        'jquery',
        'underscore',
        'backbone'
    ], function($, _, Backbone){
    var view = Backbone.View.extend({

        el: '#main',

        off_shelf_list: function(){
            console.log('off shelf list');
            RT.Router.update_title('已下架衣服列表');
            var parent_view = this;
            
            $.get('/api/clothing/get_all_off_shelf', {}, function(res){
                console.log(res);
                $(parent_view.el).hide().html(Handlebars.templates.off_shelf_clothing_list(res)).fadeIn("slow");              
                $(".barinfo").html("共 "+res.items.length+" 件商品已下架");
            }, 'json');
	
            return this;
            
        },
        
        enter: function(){
            RT.Router.update_title("編輯已上架衣服");
            var quick_html = ""+
                '<article class="module width_full">'+
                    '<div style="width:99%;margin:0 auto;">'+
                    '</div>'+
                    '<h1>請輸入商品編號</h1>'+
                    '<p><input type="text" id="product_item_number" /></p>'+
                    '<p><button class="btn" id="submit_product_item_number">確定</button></p>'+
                '</article>';
                                        
            $("#main").hide().html(quick_html).fadeIn("slow");
            $("#submit_product_item_number").click(function(){
                var product_item_number = $("#product_item_number").val();
                window.location.hash = '!/clothing/edit/' + product_item_number;
            });
        }
        
    });
    return view;
});
