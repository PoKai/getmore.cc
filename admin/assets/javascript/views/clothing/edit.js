define([
    'jquery',
    'underscore',
    'backbone',
    'views/size_table_view'
    ], function($, _, Backbone, SizeTableView) {
    RT.ViewClothingEdit = SizeTableView.extend({

        events: _.extend({
          "click #edit_clothing": 'editClothing',
          'click #add_field': 'add_field',
          'click .delete_field': 'delete_field',
          'change #item_status_select': 'item_status_select',
          'change #draft': 'draft'
        }, SizeTableView.prototype.events),

        initialize: function() {
            var parent_view = this;
            //this.render();
            
            if (this.model) {
                // this.model.on("reset", this.render, this);
                //this.model.on("change", this.render, this);
                //this.model.on("error", this.render, this);
                this.model.on("sync", this.render, this);
            }
        },
        
        item_status_select: function(){
            $('#item_status').val($('#item_status_select').val());
        },
        
        editClothing: function(){
            // use orginial files if user doesnt upload
            var files = $("input[name='files']").val();
            if (jQuery('input[name="banner_list[]"]').length>0){
                files = jQuery('input[name="banner_list[]"]').map(function() {return this.value;}).get().join();
            }

            // measure
            var measure={
              1: jQuery('#measure1').val(),
              2: jQuery('#measure2').val(),
              3: jQuery('#measure3').val(),
              4: jQuery('#measure4').val(),
              5: jQuery('#measure5').val(),
              6: jQuery('#measure6').val(),
              7: jQuery('#measure7').val(),
              8: jQuery('#measure8').val(),
              9: jQuery('#measure9').val(),
              10:jQuery('#measure10').val()
            };
            
            var measure_json = JSON.stringify(measure);

            var styles = $('input[name=style]:checked').map(function(){
                                    return $(this).val();
                               }).get().join(',');

            var $descript={};
            jQuery('.descript_key').each(function(i,item){
              var key=jQuery(this).val();
              $descript[key]=
              jQuery('.descript_value:eq('+i+')').val();
            });
            
            var data = {
                product_item_number: $("input[name='product_item_number']").val(), 
                product_title: $("input[name='product_title']").val(), 
                product_brand: $("input[name='product_brand']").val(),
                files: files,

                size_type_major: $('.size_type1').val(),
                size_type_minor: $('.size_type2').val(),
                size: $('.size_number1').val(),
                size_number_minor: $('.size_number2').val(),
                size_standard: $('.size_standard1').val(),      

                size_comment: $("input[name='size_comment']").val(), 
                item_status: $("input[name='item_status']").val(),
                measure: measure_json,
                
                color: $('input[name=r_color]:checked').val(),
                
                style: styles,
                descript: JSON.stringify($descript),

                admin_comment: $("input[name='admin_comment']").val(), 
                admin_remark: $("input[name='admin_remark']").val(),
                
                publication_time: $("#publication_time").val(), 
                draft: ($('#draft').length && $('#publication_time').val() === "")?$('#draft').is(':checked'):undefined
            };
            console.log(data);
            $.ajax({
                url: '/restful/product_clothing/'+this.id,
                type: 'PUT',
                dataType: 'json',
                data: data,
                beforeSend: function(jqXHR, settings) {
                    RT.dialogs.loading('open');
                },
                success: function(response) {
                    alert('修改成功');
                    RT.dialogs.loading('close');
                },
                error: function(){
                    alert('修改失敗');
                    RT.dialogs.loading('close');
                }
            });
        },
        draft: function(e){
            if($(e.target).is(':checked')){
                $('#publication_time').attr('disabled', 'disabled').val("");
            }
            else {
                $('#publication_time').removeAttr('disabled');
            }
        },
        // implement render function
        render: function() {
            var parent_view = this;
            $(parent_view.el).empty();
            
            var data = {};
            data.item = {};
            
            $.extend(data.item, parent_view.model.attributes);
            this.id = data.item.id;
            data.item.can_changlock = true;

            if(data.item.lock_status === "1" || data.item.lock_status === "2"){
                data.item.can_changlock = false;
            }
            else if(data.item.lock_status === "4"){
                data.item.draft = true
            }

            var t = data.item.product_type.substr(0,2);
            var measure_image_path = "/admin/assets/images/clothing_table/" + t;
            
            if(!(t=="14" || t=="15" || t=="24" || t=="31" || t=="35")){
                if(!(t=="33" && data.item.pattern == "20"))
                        measure_image_path+=data.item.item_long;
                else
                        measure_image_path+='skirt';
            }

            measure_image_path+=".png";
            data.item.measure_image_path = measure_image_path;
            
            // notice that the object-form and array-form may be confusing

            var measures = JSON.parse(data.item.measure);

            var measures_array = _.values(measures);
            
            data.item.pics = data.item.files.split(',');

            data.item.measures_array = measures_array;

            Handlebars.partials = Handlebars.templates;                    
            $(parent_view.el).hide().html(Handlebars.templates.clothing_edit(data)).fadeIn("slow");
            
            $('#publication_time').datetimepicker({ dateFormat: 'yy-m-d' });    
            
            var $radios = $('input:radio[name=r_color]');
            $radios.filter('[value=' + data.item.color + ']').prop('checked', true);            
            
            // check the style checkboxes
            var style_list = data.item.style.split(',');
            $.each(style_list, function(index, value){
                $('input[value=' + value + '][name=style]').prop('checked', true);
            });
            
            $.each(JSON.parse(data.item.descript), function(index, value){
                var $para = $('<p>');
                var $key_field = $('<input type="text" class="descript_key form-control">');
                var $value_field = $('<input type="text" class="descript_value form-control">');
                var $delete_button = $('<button class="btn btn-danger delete_field">刪除</button>');
                $key_field.val(index);
                $value_field.val(value);
                $para.append($key_field);
                $para.append('：');
                $para.append($value_field);
                $para.append($delete_button);
                
                $('#descript_container').append($para);
            });


            // Only show matched size selection table
            if (data.item.product_type.substr(0,1)=='1'){
                $('.men-size').hide();
                $('.women-size').show();
            }
            else if (data.item.product_type.substr(0,1)=='2'){
                $('.women-size').hide();              
                $('.men-size').show();              
            }
            else{
                $('.men-size').hide();
                $('.women-size').hide();              
            }
            
            var level_list = data.item.level.split(',');
            $.each(level_list, function(index, value){
                $('input[value=' + value + '].level').prop('checked', true);
            });
     
            RT.dialogs.loading('close');
            return this;
        },
        
        add_field: function(){
            var $para = $('<p>');
            var $key_field = $('<input type="text" class="descript_key form-control">');
            var $value_field = $('<input type="text" class="descript_value form-control">');
            var $delete_button = $('<button class="btn btn-danger delete_field">刪除</button>');

            $para.append($key_field);
            $para.append('：');
            $para.append($value_field);
            $para.append($delete_button);

            $('#descript_container').append($para);
        },
        
        delete_field: function(e){
            $(e.currentTarget).parent().remove();            
        }
    });
    return RT.ViewClothingEdit;
});
