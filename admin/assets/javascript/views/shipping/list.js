define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewShipping = View.extend({

        events: _.extend({
            'click .search_static_url': 'search',
            'click #change_status': 'change_status',
            'click .changeStatus': 'change_one_status',
            'click .click_td': 'show_detail_info',
            'click .trheader': 'sort_shipping_list',
            'click .recount_price': 'reset_price'
        }, View.prototype.events),

        params: {},
        clothingClass: {},

        initialize: function() {
            var parent_view = this;
            var model_name = parent_view.options.model_name || parent_view.model_name;

            if (this.model) {
                this.model.on("reset", this.render, this);
            }
            if (this.collection) {
                this.collection.on("reset", this.render, this);
            }

            $.ajaxSetup({
              statusCode: {
                301: function(xhr){
                    var url = JSON.parse(xhr.responseText).toString();
                    location.replace(url);
                }
              }
            });
        },

        show_detail_info: function(e) {
            (this.debug) && console.log('show_detail_info');
            self=this;
            var temp,
            $nextUL,
            $nextTR=$(e.currentTarget).parent().next();

            if ($nextTR.is(":visible")) {
                $nextTR.hide();
            }
            else{
                $nextTR.show();
                if($nextTR.find('li').size()<1){
                    $('.edit_info').editable(RT.API.userinfoEdit, {
                        indicator : 'Saving...',
                        tooltip   : 'Click to edit...'//,
                        //onblur : 'submit' 
                    }); // end editable
                
                    var washId,
                    shipping_type=this.options.shipping_type;
                    $.ajax({
                        type: "GET",
                        url: RT.API.getShippingItems,
                        //data: "order_id=" + $(e.currentTarget).val(),
                        data: "order_id=" + $(e.currentTarget).prev().find("input").val(),
                        dataType: "json",
                        success: function(data){
                            $nextUL=$nextTR.find('ul');
                            $.each(data.items, function(i, item) {
                                temp='  <li class="clr">';
                                iconName='/admin/assets/images/clothing_icon/'+item.product_type.substr(0,2)+'.png';
                                temp+='    <img src="'+iconName+'">';
                                temp+='    <div data-productclass="'+item.product_type+'">';
                                if(item.shipping_wash_id==null)
                                    washId=''; 
                                else  
                                    washId=item.shipping_wash_id;
                                if(shipping_type==4){
                                    temp+='洗衣編號: <input type="text" class="wash_id_input" data-id="'+item.id+'"  data-remove="'+item.remove+'" style="margin-bottom:0px;" value="'+ washId+'"> ';
                                    if(item.remove==1)
                                        temp+='<span style="color:red;">[已撤單]</span><br>';
                                    else
                                        temp+='<a style="color: white;" href="javascript:void(0);" data-id="'+item.id+'" class="btn btn-danger delete_item delShippingItem"><i class="icon-edit icon-white"></i> 撤單</a><br>';
                                }
                                else if(shipping_type==5){
                                    temp+='洗衣編號: '+ washId+'';
                                    if(item.remove==1)
                                        temp+='<span style="color:red;">[已撤單]</span><br>';
                                    else
                                        temp+='<a style="color: white;" href="javascript:void(0);" data-id="'+item.id+'" class="btn btn-danger delete_item delShippingItem"><i class="icon-edit icon-white"></i> 撤單</a><br>';
                                }
                                else if(shipping_type==6){
                                    temp+='洗衣編號: '+ washId+' <br>';
                                    if(item.remove==1)
                                        temp+='<span style="color:red;">[已撤單]</span><br>';
                                }
                                
                                
                                temp+='商品編號: '+item.product_item_number;
                                temp+='<input type="hidden" name="itemid" value="'+item.product_item_number+'"><br>';
                                temp+='商品敘述: '+item.product_title+' <br>';
                                temp+= '品牌: <div style="min-width: 200px; height: 20px; border: 1px solid black; display: inline-block;" class="edit_iteminfo" id="product_brand!'+item.id+'">'+item.product_brand+'</div><span style="color: #0088CC;padding-left: 20px;"></span><br />';
                                temp+='參考售價: '+self.convert_original_price(item.product_original_price)+' <br>';
                                temp+='建議售價: '+item.product_sell_price+' <br>';
                                temp+='類別: <div class="selarea"></div> ';
                                if(shipping_type==4){
                                    if(item.remove!=1)
                                        temp+='<a style="color: white;" href="javascript:void(0);" data-id="'+item.id+'" class="btn btn-primary changeClass"><i class="icon-edit icon-white"></i> 更新</a> ';
                                }
                                temp+='<br>';
                                temp+='洗衣金額: <span class="shipping_price">'+item.shipping_price+'</span> 元 <br>';
                                temp = (item.donative=='1') ? temp + '期滿: <span>'+'捐贈'+'</span><br>' : temp + '期滿: <span>'+'寄還'+'</span><br>';                                
                                temp+= '配件: <div style="min-width: 200px; height: 20px; border: 1px solid black; display: inline-block;" class="edit_iteminfo" id="has_accessory!'+item.id+'">'+item.has_accessory+'</div><span style="color: #0088CC;padding-left: 20px;"></span><br />';
//                                temp+= '吊牌: <div style="min-width: 200px; height: 20px; border: 1px solid black; display: inline-block;" class="edit_iteminfo_bool" id="has_tag!'+item.id+'">'+item.has_tag+'</div><span style="color: #0088CC;padding-left: 20px;"></span><br />';                                
//                                temp+= '吊牌: <span data-id="' + item.id + '"><label style="display: inline"><input type="radio" value="0" name="has_tag-' + item.id + '" class="has_tag" />無</label><label style="display: inline"><input type="radio" value="1" name="has_tag-' + item.id + '" class="has_tag" />有</label></span><span class="has_tag_ajax_status"></span><br />';
                                temp += (item.has_tag!='0') ?
                                    '吊牌: <span data-id="' + item.id + '"><label style="display: inline"><input type="radio" value="0" name="has_tag-' + item.id + '" class="has_tag" />無</label><label style="display: inline"><input type="radio" value="1" name="has_tag-' + item.id + '" class="has_tag" checked="checked" />有</label></span><span class="has_tag_ajax_status"></span><br />' :
                                    '吊牌: <span data-id="' + item.id + '"><label style="display: inline"><input type="radio" value="0" name="has_tag-' + item.id + '" class="has_tag" checked="checked" />無</label><label style="display: inline"><input type="radio" value="1" name="has_tag-' + item.id + '" class="has_tag" />有</label></span><span class="has_tag_ajax_status"></span><br />';
                                temp+='備註: <div style="min-width: 200px; height: 20px; border: 1px solid black; display: block;" class="edit_iteminfo" id="comments!'+item.id+'">'+item.comments+'</div><span style="color: #0088CC;padding-left: 20px;"></span>';
                                temp+='    </div>';
                                temp+='  </li>';
                                $nextUL.append(temp);
                                setDropDownList(item.product_type);
                            }); // end $.each

                            if(shipping_type==4){
                                $nextUL.find('.delShippingItem').click(function(){
                                    if (window.confirm("確定撤銷此項目?")) {
                                        $this=$(this);
                                        $.ajax({
                                            type: "POST",
                                            url: RT.API.iteminfoEdit,
                                            data: {id:"remove!" + $this.data("id"), value:"1"},
                                            dataType: "json",
                                            success: function(){
                                                $this.parent().find('.changeClass').hide();
                                                var $itemcount=$this.parent().parent().parent().parent().parent().parent().parent().prev().find('.items_count');
                                                $itemcount.text($itemcount.text()-1);
                                                $('<span style="color:red;">[已撤單]</span>').insertAfter($this);
                                                $this.remove();
                                            }
                                        });// end ajax
                                    }// end if
                                });// end click

                                $nextUL.find('.wash_id_input').blur(function(){
                                    $this=$(this);
                                    //var val=($this.val().length>0)?$this.val():null;
                                    $.ajax({
                                        type: "POST",
                                        url: RT.API.iteminfoEdit,
                                        data: {id: "shipping_wash_id!"+$this.data("id"),value:$this.val()},
                                        dataType: "json",
                                        success: function(){}
                                    });// end ajax			
                                });// end blur
                              
                                $("ul select").change(function(){
                                    changeDropDownList($(this));
                                });// end change

                                $nextUL.find('.changeClass').click(function(){
                                    var items_class=$('.barinfo').data("items_class");
                                    $this=$(this);
                                    var sel=$this.prev();
                                    var class_value=null;
                                    var w_price=0,msg;

                                    if(sel.find('.level_3').val()!=null)
                                        class_value=sel.find('.level_3').val();
                                    else if(sel.find('.level_2').val()!=null)
                                        class_value=sel.find('.level_2').val();

                                    if(class_value!=null){
                                        iconName='/admin/assets/images/clothing_icon/'+sel.find('.level_2').val()+'.png';
                                        $this.parent().parent().find('img').attr('src',iconName);
                                        if(class_value.length==2)
                                            w_price=items_class.price[sel.find('.level_1').val()][class_value.substr(0,2)];
                                        else if(class_value.length==3)
                                            w_price=items_class.price[sel.find('.level_1').val()][class_value.substr(0,2)][class_value];

                                        $.ajax({
                                            type: "POST",
                                            url: RT.API.iteminfoEdit,
                                            data: {id: "shipping_price!"+$this.data("id"),value:w_price},
                                            dataType: "json",
                                            success: function(){
                                                var $p=$this.parent();
                                                var orderId=$p.parent().parent().parent().data("id");
                                                msg="類別修改 : 商品編號"+$p.find("input[name=itemid]").val()+",原類別id-"+$p.data("productclass");
                                                msg+=" , 原洗衣費("+$p.find('.shipping_price').text()+"), 更新類別id-"+class_value+" , 更新洗衣費("+w_price+")";
                                                logChange(orderId, msg);
                                                $p.find('.shipping_price').text(w_price);
                                            }// end success
                                        });// end ajax
                                        $.ajax({
                                            type: "POST",
                                            url: RT.API.iteminfoEdit,
                                            data: {id: "product_type!"+$this.data("id"),value:class_value},
                                            dataType: "json",
                                            success: function(){
                                                alert("更新類別項目完成 !");
                                            }
                                        });// end ajax
                                    }
                                    else
                                        alert("請選擇次類別項目 !");
                                });// end click

                                $('.edit_iteminfo').editable(RT.API.iteminfoEdit, {
                                    indicator : 'Saving...',
                                    tooltip   : 'Click to edit...',
                                    width: "250px"
                                }); // end editable

                                $('.has_tag').change(function(e){
                                    $(e.currentTarget).parent().parent().next().text('更新中...');
                                    var data = {
                                        id: "has_tag!" + $(this).parent().parent().data('id'),
                                        value: $(this).val()
                                    };
                                    console.log(data);
                                    $.post(RT.API.iteminfoEdit, data, function(res){
                                        $(e.currentTarget).parent().parent().next().text('成功。');                                        
                                    });
                                });

                            }// end if(shipping_type==4)

                            if(shipping_type==5){
                                $nextUL.find('.delShippingItem').click(function(){
                                    if (window.confirm("確定撤銷此項目?")) {
                                        $this=$(this);
                                        $.ajax({
                                            type: "POST",
                                            url: RT.API.iteminfoEdit,
                                            data: {id:"remove!" + $this.data("id"), value:"1"},
                                            dataType: "json",
                                            success: function(){
                                                $this.parent().find('.changeClass').hide();
                                                var $itemcount=$this.parent().parent().parent().parent().parent().parent().parent().prev().find('.items_count');
                                                $itemcount.text($itemcount.text()-1);
                                                $('<span style="color:red;">[已撤單]</span>').insertAfter($this);
                                                $this.remove();
                                            }
                                        });// end ajax
                                    }// end if
                                });// end click

                                $('.edit_iteminfo').editable(RT.API.iteminfoEdit, {
                                    indicator : 'Saving...',
                                    tooltip   : 'Click to edit...',
                                    width: "250px"
                                }); // end editable                                
                                
                            }// end if(shipping_type==5)


                        },// end success
                        error: function(jqXHR, textStatus, errorThrown){
                            console.log("FETCH FAILED: " + errorThrown);
                        }
                    });// end ajax

                    var tmp = $nextTR.find('.log_info').text();
                    tmp = tmp.replace(/\<br\>/g," ");
                    tmp = tmp.replace(/\]/g,"\]\<br\>\<br\>");
                    $nextTR.find('.log_info').html(tmp)
		
                }  //if($nextTR.find('li').size()<1)
            } //if ($nextTR.is(":visible"))

            function setDropDownList(classId) {
                //classId='352';
                var option;
                var items_class=$('.barinfo').data("items_class");

                var level_1 = $('<select class="level_1">');
                if (typeof(items_class.price) !== "undefined") { 
                    $.each(items_class.price, function(i, item) {
                        option = $('<option>').val(i).append(items_class.name[i]);
                              level_1.append(option);
                    });
                }

                var level_2 = $('<select class="level_2">');
                if (classId.length>0 && typeof(items_class.price[classId.substr(0,1)]) !== "undefined") { 
                    level_1.val(classId.substr(0,1));
                    $.each(items_class.price[classId.substr(0,1)], function(i, item) {
                        option = $('<option>').val(i).append(items_class.name[i]);
                              level_2.append(option);
                    });
                }

                var level_3 = $('<select class="level_3">');
                if (classId.length>1 && typeof(items_class.price[classId.substr(0,1)][classId.substr(0,2)]) !== "undefined") { 
                    level_2.val(classId.substr(0,2));
                    $.each(items_class.price[classId.substr(0,1)][classId.substr(0,2)], function(i, item) {
                        option = $('<option>').val(i).append(items_class.name[i]);
                              level_3.append(option);
                    });
                }

                if(classId.length>2)
                    level_3.val(classId.substr(0,3));  
                $('.selarea').append(level_1).append(level_2).append(level_3).removeClass('selarea').addClass("seted");
            }// end setDropDownList

            function changeDropDownList($this) {
                var items_class=$('.barinfo').data("items_class");
                var value=$this.val();
                var option;
                if(value.length==1){
                    $this.next().find('option').remove()
                    $this.next().next().find('option').remove()

                    if (typeof(items_class.price[value]) !== "undefined") {
                        $.each(items_class.price[value], function(i, item) {
                            option = $('<option>').val(i).append(items_class.name[i]);
                            $this.next().append(option);
                        });
                    }
  
                }else if(value.length==2){
                    $this.next().find('option').remove()

                    if (typeof(items_class.price[$this.prev().val()][value]) !== "undefined") {
                        $.each(items_class.price[$this.prev().val()][value], function(i, item) {
                            option = $('<option>').val(i).append(items_class.name[i]);
                            $this.next().append(option);
                        });
                    }

                }
            }// end changeDropDownList

            function logChange(orderId, msg) {
                var $loginfo;
                $('.items_info').each(function(i,item){
                    $this=$(this);
                    if($this.data("id")==orderId)
                        $loginfo=$this.prev().find('.log_info');
                });
                if(typeof($loginfo)=='undefined')
                    return;

                var org_log=$loginfo.text();
                if(org_log==null)
                    org_log='';

                msg=org_log+" [ "+msg+" ] ";
                msg=msg.replace(/\<br\>/g," ");

                $.ajax({
                    type: "POST",
                    url: RT.API.userinfoEdit,
                    data: {id: "logs!"+orderId,value:msg},
                    dataType: "json",
                    complete: function(){
                        $loginfo.html(msg.replace(/\]/g,"\]\<br\>\<br\>"));
                    }
                });

            }//end changeDropDownList

        }, // end show_detail_info
        format: function(number){
            return number.toString().replace(/(\d+)(\d{3})$/g, "$1,$2");
        },
        convert_original_price: function(o_price){
            if(parseInt(o_price) <= 1000)
                show = "500元~1,000元";
            else if(parseInt(o_price) <= 10000){
                show = this.format(o_price-1000+1)+"元~"+this.format(o_price)+"元";
            }
            else if(parseInt(o_price) == 99999){
                show = "超過10,000元";
            }
            else if(parseInt(o_price) <= 50000){
                show = this.format(o_price-5000+1)+"元~"+this.format(o_price)+"元";
            }
            else if(parseInt(o_price) <= 100000){
                show = this.format(o_price-10000+1)+"元~"+this.format(o_price)+"元";
            }
            else {
                show = "超過100,000元";
            }
            
            return show;
        },// end convert_original_price

	reset_price: function(e){
      var $this=$(e.currentTarget);
      $.post('/api/shipping/recalculate', {order_id: $this.data("id")}, function(res){
          // Debugging message.
          // alert(JSON.stringify(res));

          // don't want to implement ajax refresh, I think it's unnecessary  
          window.location.reload();
      }, 'json');
      
      /*
      $.each($items_info, function(i, item) {
          if($(item).find('.delShippingItem').length > 0)
          total+=parseInt($(item).find('.shipping_price').text());
      });
      if(total<400)
          tran=items_class.transport.less;
      else if(total<600)
          tran=items_class.transport.more;
      else 
          tran=0;

      var all=(total+tran);
      var tmp=total+" (洗衣費) + "+tran+" (運費)<br>="+all+"<br>";
      $this.parent().find('.recount_info').append(tmp);
      $.ajax({
          type: "POST",
          url: RT.API.userinfoEdit,
          data: {id: "transportation_charge!"+$this.data("id"),value:tran},
          dataType: "json",
          complete: function(){
              var $p=$this.parent();
              var orderId=$this.data("id");
              msg="重新計算洗衣費 : 原洗衣費記錄 "+$p.find(".item_washprice").text()+" (洗衣費) + "+$p.find(".item_transport").text()+" (運費)="+(parseInt($p.find(".item_washprice").text())+parseInt($p.find(".item_transport").text())) ;
              logChange(orderId, msg);
          }
      });

      $.ajax({
          type: "POST",
          url: RT.API.userinfoEdit,
          data: {id: "total_price!"+$this.data("id"),value:total},
          dataType: "json",
          complete: function(){
              $(e.currentTarget).parent().parent().parent().parent().prev().find("td:eq(5)").text(total);
          }
      });

      function logChange(orderId, msg) {
          var $loginfo;
          $('.items_info').each(function(i,item){
              $this=$(this);
              if($this.data("id")==orderId)
                  $loginfo=$this.prev().find('.log_info');
          });
          var org_log=$loginfo.html();
          if(org_log==null)
              org_log='';
          msg=org_log+" [ "+msg+" ] ";
          $.ajax({
              type: "POST",
              url: RT.API.userinfoEdit,
              data: {id: "logs!"+orderId,value:msg},
              dataType: "json",
              complete: function(){
                  $loginfo.html(msg);
              }
          });
      }
      */
	},

	sort_shipping_list: function(e) {
            e.preventDefault();
            var parent_view = this,
                shipping_type = this.options.shipping_type,
		sort_id = $(e.currentTarget).data('sort');;
            this.params = {type: shipping_type,sort_by: sort_id};

            parent_view.options.model_name.set_params(this.params);
            parent_view.collection.fetch();
            e.stopImmediatePropagation();
            return false;
        },

        search: function(e) {
            (this.debug) && console.log('search');
            console.log('search');
            e.preventDefault();
            var parent_view = this,
                form_id = $(e.currentTarget).data('form');
            this.params = $(form_id).serializeObject();

            parent_view.options.model_name.set_params(this.params);
            parent_view.collection.fetch();
            e.stopImmediatePropagation();
            return false;
	},

        change_status: function(e) {
            (this.debug) && console.log('change_status');
            console.log('change_status');
            e.preventDefault();
	    
            if (this.options.shipping_type==3 || window.confirm("確定更改狀態，進入下一階段?")) {
                var checkedItems = [],
                $chked=$('input[type=checkbox]:checked');
                $chked.each(function() {
                    checkedItems.push($(this).val());
                }); 

                if(checkedItems.length >0 ){
                    if(this.options.shipping_type==2){
                        var url=checkedItems.join(',');
                        url=RT.API.generateXML+"?items="+url;
                        window.open(url, '_blank');
                    }

                    $.ajax({
                        type: "POST",
                        url: RT.API.setShippingStatus,
                        data: {chkitems:checkedItems, type:this.options.shipping_type},
                        dataType: "json",
                        success: function(data){
                            $chked.parent().parent().next().remove();
                            $chked.parent().parent().remove();
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            console.log("FETCH FAILED: " + errorThrown);
                        }
                    });
                }// end if
            }
            e.stopImmediatePropagation();
            return false;
        },
        change_one_status: function(e) {
            (this.debug) && console.log('change_one_status');
            console.log('change_one_status');
            e.preventDefault();
            var $this=$(e.currentTarget),
            $thisTr=$this.parent().parent(),
            flag=true;
	    
            if (this.options.shipping_type==4) {
                var $all_wash=$thisTr.next().find('.wash_id_input');	
                $all_wash.each(function() {
                    if(( $(this).val().length<=0) && ($(this).data('remove')=='0') ){
                        alert('請檢查洗衣編號 !');
                        flag = false;
                        return flag;
                    }
                });
            }
            if(flag){
                if (this.options.shipping_type==3 || window.confirm("確定更改狀態，進入下一階段?")) {
                    var checkedItems = $this.data("id");

                    $.ajax({
                        type: "POST",
                        url: RT.API.setShippingStatus,
                        data: {chkitems:checkedItems, type:this.options.shipping_type},
                        dataType: "json",
                        success: function(data){
                            $thisTr.next().remove();
                            $thisTr.remove();
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            console.log("FETCH FAILED: " + errorThrown);
                        }
                    });// end ajax
                }// end if
            }// end if
            e.stopImmediatePropagation();
            return false;
        },// end change_one_status

        setShippingType: function(id) {
            switch (id) {
                case 'disabled':
                    id="-1";
                    break;
                case 'notpay':
                    id="1";
                    break; 
                case 'atmnotpay':
                    id="1.5";
                    break; 
                case 'nottake':
                    id="2";
                    break; 
                case 'taking':
                    id="3";
                    break; 
                case 'checking':
                    id="4";
                    break; 
                case 'washed':
                    id="5";
                    break; 
                case 'done':
                    id="6";
                    break; 
            }
	    this.options.shipping_type=id;
        },

	setClothingClassJSON: function(){
	    $.ajax({
       	        type: "GET",
		async: false,
       	        url: RT.API.getClothingSetting,
       	        dataType: "json",
        	success: function(data){
		    $('.barinfo').data("items_class",data);
		}
            });
	},

        // implement render function
        render: function() {
            var parent_view = this,
                data = this.options.data || {};
            $(parent_view.el).empty();
            data.items = [];
            this.collection.each(function(item) {
                if ((item.attributes.uniform_invoice_type)=='0'){
                    item.attributes.uniform_invoice_type_label = "捐贈創世基金會";
                }
                else if ((item.attributes.uniform_invoice_type)=='1'){
                    item.attributes.uniform_invoice_type_label = "二聯式發票";
                }
                else if ((item.attributes.uniform_invoice_type)=='2'){
                    item.attributes.uniform_invoice_type_label = "三聯式發票";
                }              
                data.items.push(item.attributes);
            });

            $.extend(data, this.params);
            if(this.options.shipping_type == 5){
                data.washer = true;
            }
            $(parent_view.el).hide().html(Handlebars.templates.shipping_list(data)).fadeIn("slow");
            RT.update_table();
            RT.dialogs.loading('close');

	    $(".barinfo").html("有 "+data.items.length+" 件貨單");
                 if(this.options.shipping_type==-1)
                            $("#change_status").remove();
	    if(this.options.shipping_type==1)
		$("#change_status").text("轉待收貨");
		//$("#change_status").text("轉待收貨");
	    else if(this.options.shipping_type==2){
		$("#change_status").text("轉收貨中");
		$(".check_all").show();
	    }else if(this.options.shipping_type==3){
		$("#change_status").text("轉驗收中");
		$(".changeStatus").show();
	    }else if(this.options.shipping_type==4){
		$("#change_status").text("轉已送洗");
		$(".recount_price").show();
		$(".changeStatus").html('<i class="icon-edit icon-white"></i> 送洗');
		$(".changeStatus").show();
	    }else if(this.options.shipping_type==5){
		$("#change_status").css({"visibility":"hidden"});
		$("#th_name").text("送洗日期");
		$(".td_wash_date, .washer").show();
	    }
	
	    this.setClothingClassJSON();

            return this;
        }
    });
    return RT.ViewShipping;
});
