define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewShopping = View.extend({

        events: _.extend({
        }, View.prototype.events),

        params: {},
	clothingClass: {},

        initialize: function() {
            var parent_view = this;
            var model_name = parent_view.options.model_name || parent_view.model_name;

            if (this.model) {
                this.model.on("reset", this.render, this);
            }
            if (this.collection) {
                this.collection.on("reset", this.render, this);
            }
        },

        search: function(e) {
            (this.debug) && console.log('search');
            console.log('search');
            e.preventDefault();
            var parent_view = this,
                form_id = $(e.currentTarget).data('form');
            this.params = $(form_id).serializeObject();

            parent_view.options.model_name.set_params(this.params);
            parent_view.collection.fetch();
            e.stopImmediatePropagation();
            return false;
	},

        setShoppingType: function(id) {
	    switch (id) {
            case 'notpay':
		id="1";
		break; 
            case 'nottake':
		id="2";
		break; 
            case 'taking':
		id="3";
		break; 
            case 'checking':
		id="4";
		break; 
            case 'washed':
		id="5";
		break; 
	    }
	    this.options.shopping_type=id;
        },

	setClothingClassJSON: function(){
	    $.ajax({
       	        type: "GET",
		async: false,
       	        url: RT.API.getClothingSetting,
       	        dataType: "json",
        	success: function(data){
		    $('.barinfo').data("items_class",data);
		}
            });
	},

	setRedColor: function(){
	    var dt=new Date();
	    dt.setDate(dt.getDate()-5);
	    var mydate, datetext;
	    $('#shopping_list').find('tr').each(function(){
		$this=$(this);
		//console.log($this.find("td:eq(3)").text().split(" ")[0].replace(/-/g,"/"));
		datetext = $this.find("td:eq(3)").text().replace(/-/g,"/");
		mydate = new Date(datetext);
//console.log(dt.getTime()+">"+mydate.getTime());
		if(dt.getTime()>mydate.getTime())
		    $this.find('td').css("color","red");
	    });
	},

        // implement render function
        render: function() {
            var parent_view = this,
                data = this.options.data || {};
            $(parent_view.el).empty();
            data.items = [];
            this.collection.each(function(item) {
                data.items.push(item.attributes);
            });

            $.extend(data, this.params);
            $(parent_view.el).hide().html(Handlebars.templates.shopping_list(data)).fadeIn("slow");
            RT.update_table();
            RT.dialogs.loading('close');

	    $(".barinfo").html("共 "+data.items.length+" 件商品已送洗");
	    this.setRedColor();
	
	    //this.setClothingClassJSON();

            return this;
        }
    });
    return RT.ViewShopping;
});
