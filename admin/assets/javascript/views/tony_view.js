define([
    'jquery',
    'underscore',
    'backbone'
    ], function($, _, Backbone) {
    var view = Backbone.View.extend({
        // implement render function
        render: function(template_name, data, smooth) {
          
            // set smooth default value to true
            smooth = typeof smooth !== 'undefined' ? smooth : true;

            // set smooth default value to true
            data = typeof data !== 'undefined' ? data : {};
            
            this.$el.html('');
            if (smooth){
                this.$el.hide().html(Handlebars.templates[template_name](data)).fadeIn("slow");
            }
            else{
                this.$el.html(Handlebars.templates[template_name](data));
            }
            return this;
        }
    });

    return view;
});
