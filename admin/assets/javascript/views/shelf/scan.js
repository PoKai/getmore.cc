define([
    'jquery',
    'underscore',
    'backbone',
    'views/size_table_view'
    ], function($, _, Backbone, SizeTableView) {
    RT.ViewShelf = SizeTableView.extend({

        initialize: function() {
            if (this.model) {
                this.model.on("change", this.render, this);
                this.model.on("error", this.ajax_error, this);
            }
            this.debug = false;

            $.ajaxSetup({
              statusCode: {
                301: function(xhr){
                    var url = JSON.parse(xhr.responseText).toString();
                    location.replace(url);
                }
              }
            });
        },
	jsonData:{},

        events: _.extend({
            'keypress #wash_id': 'scan_barcode',
            'click .add_link': 'add_link',
            'click .btn_next': 'next_tab',
        }, SizeTableView.prototype.events),

	scan_barcode: function(e) {
	    if ( e.which == 13 ) {
        e.preventDefault();
        var parent_view = this;
        var $this = $(e.currentTarget);
        var washId=$this.val();
        var itemId;
        if(washId.length > 2){
	        $.ajax({
            type: "GET",
            url: RT.API.getShippingItems,
            data: "wash_id=" + washId,
            dataType: "json",
            async: false,
            success: function(data){
              if(data.items.length>0){
                //this.options.itemInfo=data;
                parent_view.jsonData=data;
                parent_view.pleace_item_info(data,"#tabmeasure");
                itemId=data.items[0].id;
              }else
                itemId="";
            }
          });//end ajax

          /*
          if(itemId != ""){
            $.ajax({
              type: "POST",
              url: RT.API.iteminfoEdit,
              data: {id:"shelf!" + itemId, value:"1"},
              dataType: "json",
              success: function(){
              }
            });
          } //if(itemId != "")
          */
        } //if(washId.length > 2)
	    } //if ( e.which == 13 )
	},

        setClothingClassJSON: function(){
            $.ajax({
                type: "GET",
                async: false,
                url: RT.API.getClothingSetting,
                dataType: "json",
                success: function(data){
                    $('.menu').data("items_class",data);
                }
            });
        },

        pleace_item_info: function(itemData, tab){
            var parent_view = this;
            var data = itemData.items[0];
	    $('.wash_id').text(data.shipping_wash_id);
	    $('.item_id').text(data.product_item_number);
	    //$('.item_desc').text(data.product_title);
	    $('.item_label').text(data.product_brand);
	    var product_type=data.product_type;
	    var itemClassText=parent_view.getClassName(product_type);
	    $('.item_class').text(itemClassText);
	    parent_view.setStyleCheckbox(product_type);
	    parent_view.setPatternRadio(product_type);
	    parent_view.setLongRadio(product_type);
	    parent_view.setInitImg(product_type);
	    /*
	    $('input[name="r_long"]').change(function(e){
	    	parent_view.setInitImg(product_type);
	    });
	    */

	    $('#has_tag').val((data.has_tag=='1')?'有':'無');
	    $('#has_accessory').val(data.has_accessory);


	    $('#item_comment').val(data.comments);
	    var iconName='/admin/assets/images/clothing_icon/'+product_type.substr(0,2)+'.png';
	    //$('#clothing_icon').attr('src',iconName);

	    $('.item_edit_desc').html("<input type='text' value='"+data.product_title+"'>");

	    $(tab).trigger('click');

            // Only show matched size selection table
            if (product_type.substr(0,1)=='1'){
                $('.men-size').hide();
                $('.women-size').show();
            }
            else if (product_type.substr(0,1)=='2'){
                $('.women-size').hide();
                $('.men-size').show();
            }
            else{
                $('.men-size').hide();
                $('.women-size').hide();
            }


        },

	getClassName: function(productType) {
	    var items_class=$('.menu').data("items_class");
	    var rs='';

	    rs=items_class.name[productType.substr(0,1)];

	    if(productType.length>1)
	        rs+=" / "+items_class.name[productType.substr(0,2)];
	    if(productType.length>2)
	        rs+=" / "+items_class.name[productType.substr(0,3)];

	    return rs;
	},

	setInitImg: function(productType) {
      console.log('set image');
	    var t = productType.substr(0,2);
	    var setting="/admin/assets/images/clothing_table/";
	    setting+=t;
	    if(!(t=="14" || t=="15" || t=="24" || t=="31" || t=="35")){
          if(!(t=="33" && $('input[name="r_pattern"]:checked').val()=="20"))
                  setting+=$('input[name="r_long"]:checked').val();
          else
                  setting+='skirt';
	    }

	    setting+=".png";
	    $("#measure_img").attr('src',setting);
	},

	setStyleCheckbox: function(productType) {
	    var t = productType.substr(0,1);
	    var setting;
	    if(t=='1'){  //女
          setting='<label style="float: left;"><input type="checkbox" value="7" name="r_style">條紋</label>';
          setting+='<label style="float: left;"><input type="checkbox" value="5" name="r_style"> 幾何 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="6" name="r_style"> 格子 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="4" name="r_style"> 小碎花 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="9" name="r_style"> 牛仔 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="2" name="r_style"> 復古 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="3" name="r_style"> 浪漫 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="1" name="r_style"> 商務 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="8" name="r_style"> 正式場合 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="10" name="r_style"> 運動風 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="11" name="r_style"> 休閒 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="12" name="r_style"> 學院 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="13" name="r_style"> 可愛 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="14" name="r_style"> 奢華 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="15" name="r_style"> 宴會 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="16" name="r_style"> 中國風 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="19" name="r_style"> 動物紋 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="20" name="r_style"> 印花 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="21" name="r_style"> 個性 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="22" name="r_style"> 民俗風 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="23" name="r_style"> 性感 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="29" name="r_style"> 婚禮 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="30" name="r_style"> 新娘 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="32" name="r_style"> Bling Bling </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="33" name="r_style"> 量身訂做 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="24" name="r_style"> 皮質 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="27" name="r_style"> 棉質 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="28" name="r_style"> 絲質 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="25" name="r_style"> 吊帶 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="26" name="r_style"> 孕婦 </label>';
	    }else if(t=='2'){  //男
          setting='<label style="float: left;"><input type="checkbox" value="7" name="r_style"> 條紋 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="6" name="r_style"> 格子 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="4" name="r_style"> 小碎花 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="9" name="r_style"> 牛仔 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="2" name="r_style"> 復古 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="1" name="r_style"> 商務 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="8" name="r_style"> 正式場合 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="10" name="r_style"> 運動風 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="11" name="r_style"> 休閒 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="12" name="r_style"> 學院 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="20" name="r_style"> 印花 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="21" name="r_style"> 個性 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="22" name="r_style"> 民俗風 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="29" name="r_style"> 婚禮 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="30" name="r_style"> 新郎 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="33" name="r_style"> 量身訂做 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="24" name="r_style"> 皮質 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="27" name="r_style"> 棉質 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="28" name="r_style"> 絲質 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="25" name="r_style"> 吊帶 </label>';
	    }else if(t=='3'){  //kids
          setting='<label style="float: left;"><input type="checkbox" value="7" name="r_style"> 條紋 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="5" name="r_style"> 幾何 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="6" name="r_style"> 格子 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="9" name="r_style"> 牛仔 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="12" name="r_style"> 學院 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="17" name="r_style"> 小紳士 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="18" name="r_style"> 小淑女 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="10" name="r_style"> 運動風 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="11" name="r_style"> 休閒 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="29" name="r_style"> 婚禮 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="31" name="r_style"> 小花童 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="33" name="r_style"> 量身訂做 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="24" name="r_style"> 皮質 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="27" name="r_style"> 棉質 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="28" name="r_style"> 絲質 </label>';
          setting+='<label style="float: left;"><input type="checkbox" value="25" name="r_style"> 吊帶 </label>';
	    }


	    $('.item_style').html(setting);
	},

	setLongRadio: function(productType) {
	    var t = productType.substr(0,2);
	    var setting, txt;
	    $('.group_long').show();
	    if(t=='11'){ //Women/上衣
		txt='袖長';
		setting='<input type="radio" value="ns" name="r_long" checked=""> 無袖 ';
		setting+='<input type="radio" value="ss" name="r_long"> 短袖 ';
		setting+='<input type="radio" value="ls" name="r_long"> 長袖 ';
	    }else if(t=='12'){//Women/洋裝
		txt='袖長';
		setting='<input type="radio" value="ns" name="r_long" checked=""> 無袖 ';
		setting+='<input type="radio" value="ss" name="r_long"> 短袖 ';
		setting+='<input type="radio" value="ls" name="r_long"> 長袖 ';
	    }else if(t=='13'){//Women/褲子
		txt='褲長';
		setting='<input type="radio" value="sp" name="r_long" checked=""> 短褲 ';
		setting+='<input type="radio" value="lp" name="r_long"> 長褲 ';
	    }else if(t=='14'){//Women/裙子
		$('.group_long').hide();
	    }else if(t=='15'){//Women/外套
		txt='長度';
		setting='<input type="radio" value="sb" name="r_long" checked=""> 短版 ';
		setting+='<input type="radio" value="lb" name="r_long"> 長版 ';
	    }else if(t=='21'){//Men/上衣
		txt='袖長';
		setting='<input type="radio" value="ns" name="r_long" checked=""> 無袖 ';
		setting+='<input type="radio" value="ss" name="r_long"> 短袖 ';
		setting+='<input type="radio" value="ls" name="r_long"> 長袖 ';
	    }else if(t=='22'){//Men/襯衫
		txt='袖長';
		setting='<input type="radio" value="ns" name="r_long" checked=""> 無袖 ';
		setting+='<input type="radio" value="ss" name="r_long"> 短袖 ';
		setting+='<input type="radio" value="ls" name="r_long"> 長袖 ';
	    }else if(t=='23'){//Men/褲子
		txt='褲長';
		setting='<input type="radio" value="sp" name="r_long" checked=""> 短褲 ';
		setting+='<input type="radio" value="lp" name="r_long"> 長褲 ';
	    }else if(t=='24'){//Men/外套
		txt='長度';
		setting='<input type="radio" value="sb" name="r_long" checked=""> 短版 ';
		setting+='<input type="radio" value="lb" name="r_long"> 長版 ';
	    }else if(t=='32'){//Kids/Baby/上衣
		txt='袖長';
		setting='<input type="radio" value="ns" name="r_long" checked=""> 無袖 ';
		setting+='<input type="radio" value="ss" name="r_long"> 短袖 ';
		setting+='<input type="radio" value="ls" name="r_long"> 長袖 ';
	    }else if(t=='33'){//Kids/Baby/下身
		txt='褲長';
		setting='<input type="radio" value="sp" name="r_long" checked=""> 短褲 ';
		setting+='<input type="radio" value="lp" name="r_long"> 長褲 ';
	    }else if(t=='34'){//Kids/Baby/洋裝
		txt='袖長';
		setting='<input type="radio" value="ns" name="r_long" checked=""> 無袖 ';
		setting+='<input type="radio" value="ss" name="r_long"> 短袖 ';
		setting+='<input type="radio" value="ls" name="r_long"> 長袖 ';
	    }else if(t=='35'){//Kids/Baby/外套
		$('.group_long').hide();
	    }else{// 31  Kids/Baby/連身衣
		$('.group_long').hide();
	    }

	    $('.text_long').text(txt);
	    $('.item_long').html(setting);
	},

	setPatternRadio: function(productType) {
	    var parent_view = this;
	    var t = productType.substr(0,2);
	    var setting;
	    if(t=='11'){ //Women/上衣
		setting='<label style="float: left;"><input type="radio" value="3" name="r_pattern"> T-Shirt </label>';
		setting+='<label style="float: left;"><input type="radio" value="23" name="r_pattern"> 襯衫 </label>';
		setting+='<label style="float: left;"><input type="radio" value="19" name="r_pattern"> 背心 </label>';
		setting+='<label style="float: left;"><input type="radio" value="35" name="r_pattern"> 休閒運動衫 </label>';
		setting+='<label style="float: left;"><input type="radio" value="2" name="r_pattern"> Polo衫 </label>';
		setting+='<label style="float: left;"><input type="radio" value="33" name="r_pattern"> 雪紡 </label>';
		setting+='<label style="float: left;"><input type="radio" value="28" name="r_pattern"> 針織/毛衣 </label>';
	    }else if(t=='12'){//Women/洋裝
		setting='<label style="float: left;"><input type="radio" value="31" name="r_pattern"> 長洋裝 </label>';
		setting+='<label style="float: left;"><input type="radio" value="15" name="r_pattern"> 短洋裝 </label>';
	    }else if(t=='13'){//Women/褲子
		setting='<label style="float: left;"><input type="radio" value="5" name="r_pattern"> 休閒褲 </label>';
		setting+='<label style="float: left;"><input type="radio" value="25" name="r_pattern"> 西裝褲 </label>';
		setting+='<label style="float: left;"><input type="radio" value="13" name="r_pattern"> 牛仔褲 </label>';
		setting+='<label style="float: left;"><input type="radio" value="26" name="r_pattern"> 連身褲 </label>';
		setting+='<label style="float: left;"><input type="radio" value="1" name="r_pattern"> Legging </label>';
		setting+='<label style="float: left;"><input type="radio" value="22" name="r_pattern">  寬褲/褲裙 </label>';
		setting+='<label style="float: left;"><input type="radio" value="40" name="r_pattern"> 運動褲 </label>';
	    }else if(t=='14'){//Women/裙子
		setting='<label style="float: left;"><input type="radio" value="36" name="r_pattern"> A Line/圓裙 </label>';
		setting+='<label style="float: left;"><input type="radio" value="37" name="r_pattern"> 窄裙/鉛筆裙 </label>';
	    }else if(t=='15'){//Women/外套
		setting='<label style="float: left;"><input type="radio" value="24" name="r_pattern"> 西裝外套 </label>';
		setting+='<label style="float: left;"><input type="radio" value="29" name="r_pattern"> 針織/毛衣外套 </label>';
		setting+='<label style="float: left;"><input type="radio" value="14" name="r_pattern"> 皮衣 </label>';
		setting+='<label style="float: left;"><input type="radio" value="12" name="r_pattern"> 休閒外套(棉質外套) </label>';
		setting+='<label style="float: left;"><input type="radio" value="18" name="r_pattern"> 羽絨衣 </label>';
		setting+='<label style="float: left;"><input type="radio" value="9" name="r_pattern"> 大衣 </label>';
		setting+='<label style="float: left;"><input type="radio" value="11" name="r_pattern"> 斗篷 </label>';
    setting+='<label style="float: left;"><input type="radio" value="34" name="r_pattern"> 背心外套 </label>';
		setting+='<label style="float: left;"><input type="radio" value="38" name="r_pattern"> 風衣 </label>';
	    }else if(t=='21'){//Men/上衣
		setting='<label style="float: left;"><input type="radio" value="3" name="r_pattern"> T-Shirt </label>';
		setting+='<label style="float: left;"><input type="radio" value="2" name="r_pattern"> Polo衫 </label>';
		setting+='<label style="float: left;"><input type="radio" value="28" name="r_pattern"> 針織/毛衣 </label>';
		setting+='<label style="float: left;"><input type="radio" value="19" name="r_pattern"> 背心 </label>';
    setting+='<label style="float: left;"><input type="radio" value="35" name="r_pattern"> 休閒運動衫 </label>';
	    }else if(t=='22'){//Men/襯衫
		setting='<label style="float: left;"><input type="radio" value="8" name="r_pattern"> 商務襯衫 </label>';
		setting+='<label style="float: left;"><input type="radio" value="6" name="r_pattern"> 休閒襯衫 </label>';
	    }else if(t=='23'){//Men/褲子
		setting='<label style="float: left;"><input type="radio" value="5" name="r_pattern"> 休閒褲 </label>';
		setting+='<label style="float: left;"><input type="radio" value="25" name="r_pattern"> 西裝褲 </label>';
		setting+='<label style="float: left;"><input type="radio" value="13" name="r_pattern"> 牛仔褲 </label>';
		setting+='<label style="float: left;"><input type="radio" value="39" name="r_pattern"> 海灘褲 </label>';
    setting+='<label style="float: left;"><input type="radio" value="40" name="r_pattern"> 運動褲 </label>';
	    }else if(t=='24'){//Men/外套
		setting='<label style="float: left;"><input type="radio" value="24" name="r_pattern"> 西裝外套 </label>';
		setting+='<label style="float: left;"><input type="radio" value="14" name="r_pattern"> 皮衣 </label>';
		setting+='<label style="float: left;"><input type="radio" value="17" name="r_pattern"> 羽絨衣 </label>';
		setting+='<label style="float: left;"><input type="radio" value="9" name="r_pattern"> 大衣 </label>';
		setting+='<label style="float: left;"><input type="radio" value="29" name="r_pattern"> 針織/毛衣外套 </label>';
    setting+='<label style="float: left;"><input type="radio" value="34" name="r_pattern"> 背心外套 </label>';
    setting+='<label style="float: left;"><input type="radio" value="12" name="r_pattern"> 休閒外套(棉質外套) </label>';
    setting+='<label style="float: left;"><input type="radio" value="38" name="r_pattern"> 風衣 </label>';
	    }else if(t=='32'){//Kids/Baby/上衣
		setting='<label style="float: left;"><input type="radio" value="3" name="r_pattern"> T-Shirt </label>';
		setting+='<label style="float: left;"><input type="radio" value="19" name="r_pattern"> 背心 </label>';
		setting+='<label style="float: left;"><input type="radio" value="28" name="r_pattern"> 針織/毛衣 </label>';
		setting+='<label style="float: left;"><input type="radio" value="23" name="r_pattern"> 襯衫 </label>';
    setting+='<label style="float: left;"><input type="radio" value="35" name="r_pattern"> 休閒運動衫 </label>';
    setting+='<label style="float: left;"><input type="radio" value="2" name="r_pattern"> Polo衫 </label>';
	    }else if(t=='33'){//Kids/Baby/下身
		setting='<label style="float: left;"><input type="radio" value="21" name="r_pattern"> 褲子 </label>';
		setting+='<label style="float: left;"><input type="radio" value="20" name="r_pattern"> 裙子 </label>';
		setting+='<label style="float: left;"><input type="radio" value="22" name="r_pattern"> 褲裙 </label>';
	    }else if(t=='34'){//Kids/Baby/洋裝
		setting='';
	    }else if(t=='35'){//Kids/Baby/外套
		setting='<label style="float: left;"><input type="radio" value="4" name="r_pattern"> 夾克(一般外套) </label>';
		setting+='<label style="float: left;"><input type="radio" value="18" name="r_pattern"> 羽絨衣 </label>';
    setting+='<label style="float: left;"><input type="radio" value="34" name="r_pattern"> 背心外套 </label>';
    setting+='<label style="float: left;"><input type="radio" value="28" name="r_pattern"> 西裝外套 </label>';
    setting+='<label style="float: left;"><input type="radio" value="38" name="r_pattern"> 風衣 </label>';
    setting+='<label style="float: left;"><input type="radio" value="29" name="r_pattern"> 針織/毛衣外套 </label>';
    setting+='<label style="float: left;"><input type="radio" value="14" name="r_pattern"> 皮衣 </label>';
	    }else{// 31  Kids/Baby/連身衣
		setting='';
	    }
	    $('.item_pattern').html(setting);

	    if(t=='33'){
	        $('input[name="r_pattern"]').change(function(){
		    if($(this).val()=='20'){
	    		$('.group_long').hide();
		    }else{
	    		$('.group_long').show();
		    }
		    //parent_view.setInitImg(productType);
		});
	    }
	},

        next_tab: function(e) {
          console.log("shelf/scan.js/next_tab");
          e.preventDefault();
          var parent_view = this;
          var tab = $(e.currentTarget).data('next');

          if(tab=="tabtake_picture"){
            var colorVal=$('input[name="r_color"]:checked').val();
            if(typeof(colorVal)=='undefined'){
              alert('請選擇顏色!');
              return false;
            }
            var seasonVal=$('input[name="r_season"]:checked').val();
            if(typeof(seasonVal)=='undefined'){
              alert('請選擇季節!');
              return false;
            }

            var $style=$('input[name="r_style"]:checked');
            var styleVal='';
            $style.each(function(){
                styleVal+=parent_view.classNameMap('style',$(this).val())+" ";
            });

            var patternVal=$('input[name="r_pattern"]:checked').val();
            if(typeof(patternVal)=='undefined' && $('input[name="r_pattern"]').length>0){
                alert('請選擇款式!');
                return false;
            }

            var longVal=$('input[name="r_long"]:checked').val();
            longVal=parent_view.classNameMap('long',longVal);

            seasonVal=parent_view.classNameMap('season',seasonVal);
            patternVal=parent_view.classNameMap('pattern',patternVal);

            $('.item_color_set').html('<img src="/admin/assets/images/colorbox/'+colorVal+'.png">');
            $('.item_season_set').text(seasonVal);
            $('.item_long_set').text(longVal);
            $('.item_style_set').text(styleVal);
            $('.item_pattern_set').text(patternVal);
            $('.item_desc').text(jQuery('.item_edit_desc').find('input').val());
            parent_view.setInitImg(parent_view.jsonData.items[0].product_type);
          }else if(tab=="tabdescript"){

          }else if(tab=="tabcheck"){
            if($('#item_size').val()==''){
              alert('請輸入尺寸標示!');
              return false;
            }
            if($('#item_status').val()==''){
                alert('請輸入狀態驗收!');
                return false;
            }
          var $preview=$('.preview');
          $preview.children().remove();
          $preview.append('<div class="control-group-left"></div> <div class="control-group-right"></div>');
          var p_o_price="參考原價:"+parent_view.jsonData.items[0].product_original_price;
          var p_imgs=jQuery('.thumbnails li img');
          $preview.find('.control-group-left').append(p_imgs.clone());

          var p_link=jQuery('.item_class:eq(0)').text();
          var p_title=jQuery('.item_edit_desc').find('input').val();
          var p_id="商品編號:"+jQuery('.item_id:eq(0)').text();
          var p_desc="品牌:"+jQuery('.item_label:eq(0)').text();
          p_desc+="<br>款式:"+jQuery('.item_pattern_set:eq(0)').text();
          p_desc+="<br>尺寸:" + jQuery('.size_type1').val() + ' / ' +
          jQuery('.size_number1').val() + ' / ' +
          jQuery('.size_standard1').val() + ' / ' +
          jQuery('.size_type2').val() + ' / ' +
          jQuery('.size_number2').val();

          var p_style="風格:"+jQuery('.item_style_set:eq(0)').text();
          var p_status="狀態驗收:"+jQuery('#item_status').val();
          var p_o_price="參考原價:"+parent_view.jsonData.items[0].product_original_price;
          var p_price="售價:"+parent_view.jsonData.items[0].product_sell_price;
	        $previewLayout = $preview.find('.control-group-right').append(p_link);
          $previewLayout.append('<br>').append(p_title);
          $previewLayout.append('<br>').append(p_desc);
          $previewLayout.append('<br>').append(p_status);

          var p_mater=$('#item_material').val();
          if(p_mater!='')
              $previewLayout.append('<br>材質:').append(p_mater);
          var p_location=$('#item_location').val();
          if(p_location!='')
              $previewLayout.append('<br> 產地:').append(p_location);
          var p_other='';
          jQuery('input[name="detail_name_list[]"]').each(function(i,item){
            p_other+=jQuery(this).val()+":";
            p_other+=jQuery('input[name="detail_value_list[]"]:eq('+i+')').val();
            p_other+='<br>';
          });
          $previewLayout.append('<br>').append(p_other);
          $previewLayout.append('<br>').append(p_style).append('<br>');
          $previewLayout.append('<br>').append(p_o_price);
          $previewLayout.append('<br>').append(p_price);
          $previewLayout.append('<br>');

          var meas1=(jQuery('#measure1').val()=='')?'&nbsp;':jQuery('#measure1').val()+' cm';
          var meas2=(jQuery('#measure2').val()=='')?'&nbsp;':jQuery('#measure2').val()+' cm';
          var meas3=(jQuery('#measure3').val()=='')?'&nbsp;':jQuery('#measure3').val()+' cm';
          var meas4=(jQuery('#measure4').val()=='')?'&nbsp;':jQuery('#measure4').val()+' cm';
          var meas5=(jQuery('#measure5').val()=='')?'&nbsp;':jQuery('#measure5').val()+' cm';
          var meas6=(jQuery('#measure6').val()=='')?'&nbsp;':jQuery('#measure6').val()+' cm';
          var meas7=(jQuery('#measure7').val()=='')?'&nbsp;':jQuery('#measure7').val()+' cm';
          var meas8=(jQuery('#measure8').val()=='')?'&nbsp;':jQuery('#measure8').val()+' cm';
          var meas9=(jQuery('#measure9').val()=='')?'&nbsp;':jQuery('#measure9').val()+' cm';
          var meas10=(jQuery('#measure10').val()=='')?'&nbsp;':jQuery('#measue10').val()+' cm';
          var p_measure='<table cellspacing="0" cellpadding="0" border="0" >';
          p_measure+='  <tbody><tr>';
          p_measure+='    <td align="right" rowspan="10"><img id="measure_img" src="'+jQuery('#measure_img').attr('src')+'"></td>';
          p_measure+='    <td><div style="height:30px;display: table-cell;vertical-align: middle;">'+meas1+'</div></td>';
          p_measure+='  </tr><tr>';
          p_measure+='    <td><div style="height:30px;display: table-cell;vertical-align: middle;">'+meas2+'</div></td>';
          p_measure+='  </tr><tr>';
          p_measure+='    <td><div style="height:30px;display: table-cell;vertical-align: middle;">'+meas3+'</div></td>';
          p_measure+='  </tr><tr>';
          p_measure+='    <td><div style="height:30px;display: table-cell;vertical-align: middle;">'+meas4+'</div></td>';
          p_measure+='  </tr><tr>';
          p_measure+='    <td><div style="height:30px;display: table-cell;vertical-align: middle;">'+meas5+'</div></td>';
          p_measure+='  </tr><tr>';
          p_measure+='    <td><div style="height:30px;display: table-cell;vertical-align: middle;">'+meas6+'</div></td>';
          p_measure+='  </tr><tr>';
          p_measure+='    <td><div style="height:30px;display: table-cell;vertical-align: middle;">'+meas7+'</div></td>';
          p_measure+='  </tr><tr>';
          p_measure+='    <td><div style="height:30px;display: table-cell;vertical-align: middle;">'+meas8+'</div></td>';
          p_measure+='  </tr><tr>';
          p_measure+='    <td><div style="height:30px;display: table-cell;vertical-align: middle;">'+meas9+'</div></td>';
          p_measure+='  </tr><tr>';
          p_measure+='    <td><div style="height:30px;display: table-cell;vertical-align: middle;">'+meas10+'</div></td>';
          p_measure+='  </tr></tbody></table>';
          $preview.append(p_measure);

        }else if(tab=="tabscan"){
          //save row
          //clear field
          parent_view.addClothing();
	    }

	    tab="#"+tab;
	    $(tab).trigger('click');
	},

	addClothing: function(){
    console.log('shelf/scan.js/addClothing');
    var parent_view = this;

    var jsonData=parent_view.jsonData.items[0];
    var $measure={
      1: jQuery('#measure1').val(),
      2: jQuery('#measure2').val(),
      3: jQuery('#measure3').val(),
      4: jQuery('#measure4').val(),
      5: jQuery('#measure5').val(),
      6: jQuery('#measure6').val(),
      7: jQuery('#measure7').val(),
      8: jQuery('#measure8').val(),
      9: jQuery('#measure9').val(),
      10:jQuery('#measure10').val()
    };

		var $detail={};
		var key;
		jQuery('input[name="detail_name_list[]"]').each(function(i,item){
      key=jQuery(this).val();
      $detail[key]=
      jQuery('input[name="detail_value_list[]"]:eq('+i+')').val();
		});


    console.log('join');


    var insertData={
      product_id: jsonData.id,
      product_item_number: jsonData.product_item_number,
      product_original_price: jsonData.product_original_price,
      product_sell_price: jsonData.product_sell_price,
      product_title: jQuery('.item_edit_desc').find('input').val(),//jsonData.product_title,
      product_brand: jsonData.product_brand,
      product_type: jsonData.product_type,
      product_comments: jsonData.comments,
      color: jQuery('input[name="r_color"]:checked').val(),
      season: jQuery('input[name="r_season"]:checked').val(),
      style: jQuery('input[name="r_style"]:checked').map(function() {return this.value;}).get().join(),
      pattern: jQuery('input[name="r_pattern"]:checked').val(),
      item_long: jQuery('input[name="r_long"]:checked').val(),
      files: jQuery('input[name="banner_list[]"]').map(function() {return this.value;}).get().join(),
      // size: jQuery('#item_size').val(),
      size_comment: jQuery('#size_comment').val(),
      item_status: jQuery('#item_status').val(),
      material: jQuery('#item_material').val(),
      location: jQuery('#item_location').val(),
      measure: JSON.stringify($measure),
      descript: JSON.stringify($detail),
      publication_time: $('#publication_time').val(),

      size_type_major: $('.size_type1').val(),
      size_type_minor: $('.size_type2').val(),
      size: $('.size_number1').val(),
      size_number_minor: $('.size_number2').val(),
      size_standard: $('.size_standard1').val(),
      admin_comment: $('#admin_comment').val(),
      admin_remark: $('#admin_remark').val(),
      draft: $('#draft').is(':checked'),
      order_id: jsonData.order_id
    };
    console.log(insertData);
    $.ajax({
      type: "POST",
      url: RT.API.addClothingitem,
      data: insertData,
      dataType: "json",
      success: function(data){
        console.log(data);
        parent_view.clearFormField();
      }
    });//end ajax
	},

	clearFormField: function(){
	    jQuery('#wash_id').val("");
	    jQuery('input[name="r_color"]:checked').attr("checked",false);
	    jQuery('input[name="r_season"]:checked').attr("checked",false);
	    jQuery('#measure_table input').val("");
	    jQuery('ul.thumbnails li').remove();
	    jQuery('ul#detail_list li').remove();
	    jQuery('#descript input').val("");
      $('.size_clear1').trigger('click');
      $('.size_clear2').trigger('click');
	},

	classNameMap: function(key, val){
	    if(key=="pattern"){
		if(val=="1") return 'Legging';
		else if(val=="2") return 'Polo衫';
		else if(val=="3") return 'T-Shirt';
		else if(val=="4") return '一般外套';
		else if(val=="5") return '休閒褲';
		else if(val=="6") return '休閒襯衫';
		else if(val=="7") return '其他';
		else if(val=="8") return '商務襯衫';
		else if(val=="9") return '大衣';
		else if(val=="10") return '娃娃裝';
		else if(val=="11") return '斗篷';
		else if(val=="12") return '棉質外套';
		else if(val=="13") return '牛仔褲';
		else if(val=="14") return '皮衣';
		else if(val=="15") return '短洋裝';
		else if(val=="16") return '短裙';
		else if(val=="17") return '羽絨衣';
		else if(val=="18") return '羽絨衣';
		else if(val=="19") return '背心';
		else if(val=="20") return '裙子';
		else if(val=="21") return '褲子';
		else if(val=="22") return '褲裙';
		else if(val=="23") return '襯衫';
		else if(val=="24") return '西裝外套';
		else if(val=="25") return '西裝褲';
		else if(val=="26") return '連身褲';
		else if(val=="27") return '運動外套';
		else if(val=="28") return '針織/毛衣';
		else if(val=="29") return '針織/毛衣外套';
		else if(val=="30") return '長上衣';
		else if(val=="31") return '長洋裝';
		else if(val=="32") return '長裙';
		else if(val=="33") return '雪紡';
                           else if(val=="34") return '背心外套';
	    }else if(key=="style"){
		if(val=="1") return '上班族';
		else if(val=="2") return '復古';
		else if(val=="3") return '浪漫';
		else if(val=="4") return '小碎花';
		else if(val=="5") return '幾何';
		else if(val=="6") return '格子';
		else if(val=="7") return '條紋';
		else if(val=="8") return '正式場合';
		else if(val=="9") return '牛仔';
		else if(val=="10") return '運動風';
		else if(val=="11") return '休閒';
		else if(val=="12") return '學院';
		else if(val=="13") return '可愛';
		else if(val=="14") return '奢華';
		else if(val=="15") return '宴會';
		else if(val=="16") return '中國風';
		else if(val=="17") return '小紳士';
		else if(val=="18") return '小淑女';
		else if(val=="19") return '動物紋';
		else if(val=="20") return '印花';
		else if(val=="21") return '個性';
		else if(val=="22") return '民俗風';
		else if(val=="23") return '性感';
	    }else if(key=="season"){
		if(val=="1") return '春夏';
		else if(val=="2") return '秋冬';
		else if(val=="3") return '無季節';
	    }else if(key=="long"){
		if(val=="ns") return '無袖';
		else if(val=="sb") return '短版';
		else if(val=="ss") return '短袖';
		else if(val=="sp") return '短褲';
		else if(val=="lb") return '長版';
		else if(val=="ls") return '長袖';
		else if(val=="lp") return '長褲';
	    }else
		return '';
	},

        add_link: function(e) {
            e.preventDefault();
            var parent_view = this;
            var data = {};
            var form_id = $(e.currentTarget).data('form');
            var form_info = $(form_id).serializeObject();

            data = {
                link: form_info.link,
                target: (form_info.target == "1") ? true : false,
                target_value: form_info.target
            };

            $("input[name=link]").val('');
            $("input[name=link]").focus();
            $("#link_list").append(Handlebars.templates.shelf_scan(data));

            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },
        render: function() {
            var parent_view = this;
            var data = this.options.data || {};
            $(parent_view.el).empty();

            Handlebars.partials = Handlebars.templates;

            $.extend(data, parent_view.model.attributes);
            $(parent_view.el).hide().html(Handlebars.templates.shelf_scan(data)).fadeIn("slow");
            RT.dialogs.loading('close');

	    this.setClothingClassJSON();

            return this;
        }
    });
    return RT.ViewShelf;
});
