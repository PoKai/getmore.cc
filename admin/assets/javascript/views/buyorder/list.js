define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewBuyorder = View.extend({

        events: _.extend({
            'click .show_order_detail': 'showOrderDetail',
            'click .status-button': 'moveToNextStatus'
        }, View.prototype.events),

        statusChangeLabel: {
            '1': { 'name': '轉待出貨', 'next': '2'},
            '1.5': { 'name': '轉已出貨', 'next': '3'},
            '2': { 'name': '轉已出貨', 'next': '3'},
            '3': { 'name': '轉已到貨', 'next': '4'},
            '4': { 'name': '轉已完成', 'next': '5'}
        },

        initialize: function() {
            var parent_view = this;
            if (this.model) {
                this.model.on("reset", this.render, this);
            }
            if (this.collection) {
                this.collection.on("reset", this.render, this);
            }            
            
        },

        // implement render function
        render: function() {
            var parent_view = this;
            $(parent_view.el).empty();

            var data = {};
            data.items = [];

            this.collection.each(function(item) {
                if (item.attributes.status==parent_view.status_id){
                  
                    // add some labels for displaying
                    if ((item.attributes.delivery_time)=='any'){
                        item.attributes.delivery_time_label = "不指定";
                    }
                    else if ((item.attributes.delivery_time)=='0-12'){
                        item.attributes.delivery_time_label = "中午前";
                    }
                    else if ((item.attributes.delivery_time)=='12-17'){
                        item.attributes.delivery_time_label = "12時～17時";
                    }
                    else if ((item.attributes.delivery_time)=='17-20'){
                        item.attributes.delivery_time_label = "17時～20時";
                    }
                    
                    if ((item.attributes.receipt_to)=='3'){
                        item.attributes.receipt_to_label = "捐贈創世基金會";
                    }
                    else if ((item.attributes.receipt_to)=='1'){
                        item.attributes.receipt_to_label = "寄給訂購人";
                    }
                    else if ((item.attributes.receipt_to)=='2'){
                        item.attributes.receipt_to_label = "寄給收件人";
                    }

                    if (item.attributes.logs != ''){
                        item.attributes.logs = JSON.parse(item.attributes.logs);                    
                    }else{
                        item.attributes.logs = [];
                    }
                    
                    data.items.push(item.attributes);
                }
            });
            $(parent_view.el).hide().html(Handlebars.templates.buyorder_list(data)).fadeIn("slow");            
            $(".barinfo").html("共 "+data.items.length+" 筆訂單");

            if (!(this.status_id in this.statusChangeLabel)){
                $('.statusColumn').hide();
            }
            else{
                $('.status-button').html(this.statusChangeLabel[this.status_id].name);
            }
            
            RT.dialogs.loading('close');
            return this;
        },
        
        setStatus: function(status_id){
            this.status_id = status_id;
        },
        
        moveToNextStatus: function(e){
            if (!confirm('確定轉訂單狀態？')) return false;
            e.preventDefault();
            var self = this;
            var data = {
                order_number: $(e.currentTarget).data('order-number'), 
                status: this.statusChangeLabel[this.status_id].next
            };
            var status_id = this.status_id;
            $.ajax({
                url: '/api/buyorder/updateOneByOrderNumber',
                type: 'POST',
                dataType: 'json',
                data: data,
                beforeSend: function(jqXHR, settings) {
                    RT.dialogs.loading('open');
                },
                success: function(response) {
                    if (response.error_text) {
                        alert('修改失敗');
                        RT.dialogs.loading('close');
                    }
                    if (response.success_text) {
                        self.collection.setStatus(status_id).fetch();
                        RT.dialogs.loading('close');
                    }
                },
                error: function(){
                    alert('修改失敗');
                    RT.dialogs.loading('close');
                }
            });                        
        },
        
        showOrderDetail: function(e){
            var parent_view = this;
          
            $nextTR=$(e.currentTarget).parent().next();
            if ($nextTR.hasClass("hidden"))
                $nextTR.removeClass("hidden");
            else
                $nextTR.addClass("hidden");
                
            var order_number = $(e.currentTarget).data("order_number");
            
            if($nextTR.find('div').size()==1){
                $.ajax({
                    url: "/api/buyorder/getItemsByOrderNumber" + '?order_number=' + order_number,
                    type: 'GET',
                    dataType: 'json',
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            alert('讀取items失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            $.each(response.items, function(index, item) {
                                console.log(item);
                                var plain_html =
                                "<div class='alert alert-info'>"+ 
                                  "<p>商品名稱：" + "<a style='color: red;' target='_blank' href='/clothing/view/" + item.id + "'>" + item.product_title + "</a></p>" +
                                  "<p>商品編號：" +  item.product_item_number + "</p>" +
                                  "<p>洗衣編號：" +  item.shipping_wash_id + "</p>" +
                                  "<p>商品金額：" +  item.product_sell_price + "</p>" +
                                  "<p>配件：" +  item.has_accessory + "</p>" +
                                  "<p>吊牌：";
                                plain_html += (item.has_tag!='0')?'有':'';

                                plain_html +=
                                  "</p><p><button data-number='" + item.product_item_number + "' class='btn btn-danger return_item'>退貨</button></p>" +
                                "</div>";
                                $nextTR.find('td:eq(1)').append(plain_html);
                            });        
                            $('.return_item').unbind('click').click(function(){
                                if (!confirm('確定退貨？')){
                                    return;
                                }
                                var self = this;
                                $(self).prop('disabled', true);
                                var item_number = $(this).data('number');
                                $.post('/api/clothing/returnItem', {item_number: item_number}, function(res){
                                    $(self).parent().parent().hide();
                                    // don't want to implement ajax refresh, I think it's unnecessary  
                                    window.location.reload();
                                }, 'json');
                            });
                            $('.recalculate_order').unbind('click').click(function(){
                                if (!confirm('確定重新計算？')){
                                    return;
                                }
                                var self = this;
                                $(self).prop('disabled', true);
                                var order_id = $(this).data('id');
                                $.post('/api/buyorder/recalculate', {order_id: order_id}, function(res){
                                    // don't want to implement ajax refresh, I think it's unnecessary  
                                    window.location.reload();
                                }, 'json');
                            });
                            
                            // hide thre return_item buttons if in these status
                            if ( ['1','5'].indexOf(parent_view.status_id) != -1 ){
                                $('.return_item').hide();
                                $('.recalculate_order').hide();
                            }
                
                            RT.dialogs.loading('close');
                        }
                    },
                    error: function(){
                        alert('讀取items失敗');
                        RT.dialogs.loading('close');
                    }
                });// end ajax
            }// end if

        }
    });
    return RT.ViewBuyorder;
});
