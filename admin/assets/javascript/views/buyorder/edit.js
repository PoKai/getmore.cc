define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewBuyorderEdit = View.extend({

        events: _.extend({
          "click #edit_buyorder": 'editBuyorder'
        }, View.prototype.events),

        initialize: function() {
            var parent_view = this;
            //this.render();
            
            if (this.model) {
                // this.model.on("reset", this.render, this);
                //this.model.on("change", this.render, this);
                //this.model.on("error", this.render, this);
                this.model.on("sync", this.render, this);
            }
        },
        
        editBuyorder: function(){
                var data = {order_number: $("input[name='order_number']").val(), 
                    shipping_number: $("input[name='shipping_number']").val(), 
                    note: $("input[name='note']").val(), 
                    status: $("select[name='status']").val()
                };
                $.ajax({
                    url: '/api/buyorder/updateOneByOrderNumber',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    beforeSend: function(jqXHR, settings) {
                        RT.dialogs.loading('open');
                    },
                    success: function(response) {
                        if (response.error_text) {
                            alert('修改失敗');
                            RT.dialogs.loading('close');
                        }
                        if (response.success_text) {
                            alert('修改成功');
                            RT.dialogs.loading('close');
                        }
                    },
                    error: function(){
                        alert('修改失敗');
                        RT.dialogs.loading('close');
                    }
                });
        },

        // implement render function
        render: function() {
            var parent_view = this;
            $(parent_view.el).empty();
            
            var data = {};
            data.item = {};
            
            console.log(parent_view.model.attributes);
            $.extend(data.item, parent_view.model.attributes);
                  
            // add some labels for displaying
            
            if ((data.item.delivery_time)=='any'){
                data.item.delivery_time_label = "不指定";
            }
            else if ((data.item.delivery_time)=='0-12'){
                data.item.delivery_time_label = "中午前";
            }
            else if ((data.item.delivery_time)=='12-17'){
                data.item.delivery_time_label = "12時～17時";
            }
            else if ((data.item.delivery_time)=='17-20'){
                data.item.delivery_time_label = "17時～20時";
            }
            
            if ((data.item.receipt_to)=='3'){
                data.item.receipt_to_label = "捐贈創世基金會";
            }
            else if ((data.item.receipt_to)=='1'){
                data.item.receipt_to_label = "寄給訂購人";
            }
            else if ((data.item.receipt_to)=='2'){
                data.item.receipt_to_label = "寄給收件人";
            }
                        
            $(parent_view.el).hide().html(Handlebars.templates.buyorder_edit(data)).fadeIn("slow", function(){
                  $("select[name='status']").val(data.item.status);
            });
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewBuyorderEdit;
});
