define([
    'jquery',
    'underscore',
    'backbone',
    'views/view',
    'models/remark',
    'libs/jquery/jquery.tagsinput.min',
    'libs/jquery/jquery.twzipcode-1.4.1'
], function($, _, Backbone, View, ModelRemark){
    RT.ViewUser = View.extend({

        render: function() {
            var parent_view = this;
            var data = this.options.data || {};
            var user_id = parent_view.model.attributes.id;
            var default_address = {
                'county': '',
                'district': '',
                'zipcode': '',
                'address': ''
            };
            $(parent_view.el).empty();

            parent_view.model.attributes.sex = +parent_view.model.attributes.sex;
            parent_view.model.attributes.active = +parent_view.model.attributes.active;
            parent_view.model.attributes.p_address_json = (parent_view.model.attributes.p_address !== '') ? JSON.parse(parent_view.model.attributes.p_address) : default_address;
            parent_view.model.attributes.s_address_json = (parent_view.model.attributes.s_address !== '') ? JSON.parse(parent_view.model.attributes.s_address) : default_address;

            $.extend(data, parent_view.model.attributes);
            $(parent_view.el).hide().html(Handlebars.templates.user_edit(data)).fadeIn("slow");

            // user remarks
            parent_view.remark = new ModelRemark({ type: 'user', r_id: user_id });
            parent_view.remarks = parent_view.remark.lists;
            parent_view.remarks.fetch();
            new View({
                is_table: true,
                template_name: 'remark_table',
                el: '#user_remarks > tbody',
                collection: parent_view.remarks
            });
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewUser;
});
