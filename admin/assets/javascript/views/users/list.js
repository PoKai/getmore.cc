define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewUsers = View.extend({

        events: _.extend({
            'click .search_user': 'search'
        }, View.prototype.events),

        params: {},

        search: function(e) {
            (this.debug) && console.log('search');
            e.preventDefault();
            var parent_view = this,
                form_id = $(e.currentTarget).data('form');
            this.params = $(form_id).serializeObject();

            parent_view.options.model_name.set_params(this.params);
            parent_view.collection.fetch();
            e.stopImmediatePropagation();
            return false;
        },

        // implement render function
        render: function() {
            var parent_view = this,
                template = this.options.template_name || this.template_name,
                data = this.options.data || {},
                sex;
            data.items = [];
            $(parent_view.el).empty();

            this.collection.each(function(item) {
                sex = +item.get('sex');
                item.set('sex', sex, {
                    silent: true
                });
                data.items.push(item.attributes);
            });

            $.extend(data, this.params);
            $(parent_view.el).hide().html(Handlebars.templates.user_list(data)).fadeIn("slow");
            RT.update_table();
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewUsers;
});