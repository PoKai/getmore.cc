define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewConfig = View.extend({

        initialize: function() {
            if (this.model) {
                this.model.on("change", this.render, this);
                this.model.on("error", this.ajax_error, this);
            }
            this.debug = false;
        },

        events: _.extend({
            'click .add_link': 'add_link',
            'click .delete_file': 'delete_file'
        }, View.prototype.events),

        add_link: function(e) {
            e.preventDefault();
            var parent_view = this;
            var data = {};
            var form_id = $(e.currentTarget).data('form');
            var form_info = $(form_id).serializeObject();

            data = {
                link: form_info.link,
                target: (form_info.target == "1") ? true : false,
                target_value: form_info.target
            };

            $("input[name=link]").val('');
            $("input[name=link]").focus();
            $("#link_list").append(Handlebars.templates.config_link(data));

            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },

        delete_file: function(e) {
            console.log('delete_link');
            e.preventDefault();
            if (confirm("確定刪除資料?")) {
                $(e.currentTarget).parent().fadeOut("slow", function() {
                    $(this).remove();
                });
            }
            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },

        render: function() {
            var parent_view = this;
            var data = this.options.data || {};
            $(parent_view.el).empty();

            $.extend(data, parent_view.model.attributes);
            $(parent_view.el).hide().html(Handlebars.templates.config_edit(data)).fadeIn("slow");
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewConfig;
});