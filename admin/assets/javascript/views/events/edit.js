define([
    'jquery',
    'underscore',
    'backbone',
    'views/view',
    'models/remark',
    'models/product'
    ], function($, _, Backbone, View, ModelRemark, ModelProduct) {
    RT.ViewEvent = View.extend({

        events: _.extend({
            'click .add_product': 'add_product',
            'click .delete_product': 'delete_product'
        }, View.prototype.events),

        initialize: function() {
            if (this.model) {
                this.model.on("change", this.render, this);
            }
            if (this.options.category_collection) {
                this.options.category_collection.on("reset", this.insert_option, this);
            }
        },

        add_product: function(e) {
            e.preventDefault();
            var id = $("input[name=product_id]").val();
            this.product = new ModelProduct({
                id: id
            });
            this.product.fetch();
            new View({
                is_append: true,
                template_name: 'event_product',
                el: '#product_list',
                model: this.product
            });
            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },

        delete_product: function(e) {
            console.log('delete_product');
            e.preventDefault();
            if (confirm("確定刪除資料?")) {
                $(e.currentTarget).parent().fadeOut("slow", function() {
                    $(this).remove();
                });
            }
            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },

        insert_option: function(e) {
            var parent_view = this;
            var category_id = parent_view.model.attributes.category_id;
            var category_option = '';
            this.options.category_collection.each(function(item) {
                var key = item.get('id');
                var value = item.get('tw_name');
                var selected = (key == category_id) ? 'selected' : '';
                category_option += '<option value="' + key + '" ' + selected + '>' + value + '</optiion>';
            });
            $("#category_id").html('');
            $("#category_id").append(category_option);
        },

        render: function(e) {
            var parent_view = this;
            //var template = this.options.template_name || this.template_name;
            var category_collection = this.options.category_collection || this.category_collection;
            var data = this.options.data || {};
            var event_id = parent_view.model.attributes.id;
            $(parent_view.el).empty();

            // calculate price
            var price = parent_view.model.attributes.price;
            var peoples = parent_view.model.attributes.peoples;
            var participants = parent_view.model.attributes.participants;
            var costs = parent_view.model.attributes.costs;

            var number_1 = Math.floor(peoples * price) || 0;
            var number_2 = Math.floor((number_1 - costs) / costs * 100) || 0;
            var number_3 = Math.floor(participants * price) || 0;
            var number_4 = Math.floor((number_3 - costs) / costs * 100) || 0;
            $.extend(data, {
                number_1: number_1,
                number_2: number_2,
                number_3: number_3,
                number_4: number_4
            });

            parent_view.model.attributes.is_index = +parent_view.model.attributes.is_index;
            parent_view.model.attributes.is_enabled = +parent_view.model.attributes.is_enabled;
            $.extend(parent_view.model.attributes, data);
            $(parent_view.el).hide().html(Handlebars.templates.event_edit(parent_view.model.attributes)).fadeIn("slow");
            // get category
            category_collection.fetch();
            // product remarks
            parent_view.remark = new ModelRemark({
                type: 'event',
                r_id: event_id
            });
            parent_view.remarks = parent_view.remark.lists;
            parent_view.remarks.fetch();
            new View({
                is_table: true,
                template_name: 'remark_table',
                el: '#event_remarks > tbody',
                collection: parent_view.remarks
            });
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewEvent;
});