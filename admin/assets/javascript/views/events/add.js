define([
    'jquery',
    'underscore',
    'backbone',
    'views/view',
    'models/product'
    ], function($, _, Backbone, View, ModelProduct) {
    RT.ViewEventAdd = View.extend({

        initialize: function() {
            if (this.options.category_collection) {
                this.options.category_collection.on("reset", this.insert_option, this);
            }
        },

        events: _.extend({
            'click .add_product': 'add_product',
            'click .delete_product': 'delete_product'
        }, View.prototype.events),

        insert_option: function() {
            var parent_view = this;
            var category_collection = this.options.category_collection || this.category_collection;
            var category_option = '';
            category_collection.each(function(item) {
                var key = item.get('id');
                var value = item.get('tw_name');
                category_option += '<option value="' + key + '">' + value + '</optiion>';
            });

            $("#category_id").html('');
            $("#category_id").append(category_option);
        },

        add_product: function(e) {
            console.log('add_product');
            e.preventDefault();
            var id = $("input[name=product_id]").val();
            this.product = new ModelProduct({
                id: id
            });
            this.product.fetch();
            new View({
                is_append: true,
                template_name: 'event_product',
                el: '#product_list',
                model: this.product
            });
            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },

        delete_product: function(e) {
            console.log('delete_product');
            e.preventDefault();
            if (confirm("確定刪除資料?")) {
                $(e.currentTarget).parent().fadeOut("slow", function() {
                    $(this).remove();
                });
            }
            // call return false or e.stopPropagation() or e.stopImmediatePropagation();
            e.stopImmediatePropagation();
            return false;
        },

        render: function(e) {
            var parent_view = this,
                data = this.options.data || {};
            $(parent_view.el).empty();

            $.extend(data, {
                is_add: true
            });
            $(parent_view.el).hide().html(Handlebars.templates.event_edit(data)).fadeIn("slow");

            // get category options
            parent_view.options.category_collection.fetch();
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewEventAdd;
});