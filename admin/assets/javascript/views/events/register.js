define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewEventRegister = View.extend({

        // implement render function
        render: function() {
            var parent_view = this;
            var data = this.options.data || {};
            data.items = [];
            $(parent_view.el).empty();

            $.each(parent_view.model.attributes.items, function(key, item) {
                $.extend(item, {
                    id: key + 1
                });

                data.items.push(item);
            });

            $.extend(data, parent_view.model.attributes);
            $(parent_view.el).hide().html(Handlebars.templates.event_register(data)).fadeIn("slow");
            RT.update_table();
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewEventRegister;
});