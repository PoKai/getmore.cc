define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
    ], function($, _, Backbone, View) {
    RT.ViewCategories = View.extend({

        params: {},

        initialize: function() {
            if (this.options.collection) {
                this.options.collection.on("reset", this.insert_data, this);
            }
        },

        events: _.extend({
            'click .search_category': 'search'
        }, View.prototype.events),

        insert_data: function() {
            var parent_view = this,
                boxes = [],
                temp;

            parent_view.collection.each(function(item) {
                boxes.push(Handlebars.templates.category_list_table(item.attributes));
            });

            // get search bar;
            temp = $('#category_list tbody tr').eq(0).clone();
            // clear table
            $('#category_list tbody').html('');
            temp.appendTo('#category_list tbody');
            $('#category_list tbody').append(boxes.join(''));
        },

        search: function(e) {
            (this.debug) && console.log('search');
            e.preventDefault();
            var parent_view = this;
            var form_id = $(e.currentTarget).data('form');
            var form_info = $(form_id).serializeObject();
            var model = $(e.currentTarget).data('model');
            var model_name = parent_view.options.model_name || parent_view.model_name;

            this.params = {
                type: this.options.data.type,
                en_name: form_info.en_name,
                tw_name: form_info.tw_name
            };

            parent_view.options.model_name.set_params(this.params);
            parent_view.collection.fetch();
            e.stopImmediatePropagation();
            return false;
        },

        // implement render function
        render: function() {
            var parent_view = this;
            var data = this.options.data || {};
            $(parent_view.el).empty();
            $.extend(data, this.params);
            $(parent_view.el).hide().html(Handlebars.templates.category_list(data)).fadeIn("slow");

            // get row data
            this.collection.fetch();
            RT.update_table();
            RT.dialogs.loading('close');
            return this;
        }
    });
    return RT.ViewCategories;
});
