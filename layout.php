<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--><html class="no-js" lang="<?php echo $lang; ?>"> <!--<![endif]-->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $meta_charset; ?>"/>
		<title><?php echo $site_title; ?></title>
        <meta name="description" content="<?php echo $site_description; ?>" />
        <meta name="keywords" content="<?php echo $site_keywords; ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?php echo $meta_tag; ?>
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="stylesheet" href="/assets/css/bootstrap.css"/>
		<link rel="stylesheet" href="/assets/css/getmore_bootstrap.css"/>
        <?php if (isset($is_home)): ?>
		<link rel="stylesheet" href="/assets/css/home.css"/>
        <?php else:?>
        <link rel="stylesheet" href="/assets/css/apply.css"/>
        <?php endif;?>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="/assets/vendor/jquery/jquery.min.js"></script>
		<script type="text/javascript">
        var getmore = getmore || {}; // Backbone namespace
        var root_path = '.';
        </script>
	<script src="/assets/javascript/bootstrap.js"></script>
	<script src='/assets/javascript/home.js'></script>
	<script data-main="/assets/javascript/main-built" src="/assets/vendor/requirejs/require.js"></script>
        <script type="text/javascript">
        
          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-26930222-1']);
          _gaq.push(['_setDomainName', 'getmore.cc']);
          _gaq.push(['_trackPageview']);
        
          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
        
        </script>
	</head>

	<body>
		<div class='container'>
			<div class='header'>
				<nav class="navbar-fixed-top navbar-inverse" role="navigation">
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<ul class="nav navbar-nav navbar-left">
						<li><a data-toggle="modal" href="/"><img src="/assets/images/home.png" alt="" id='log_in'/></a></li>
						<li><a data-toggle="modal" href="#logInModal">GetMore Blog</a></li>
					</ul>
                    <?php if(!$logined): ?>
					<ul class="nav navbar-nav navbar-right">
						<li><a data-toggle="modal" href="#signUpModal"><img src="/assets/images/login.png" alt="" id='log_in'/> Log In</a></li>
					</ul>
                    <?php else: ?>
					<ul class="nav navbar-nav navbar-right">
						<li><a data-toggle="modal" href="/auth/logout?redirect_url=<?php echo urlencode(current_url()); ?>"><img src="/assets/images/home/logout.png" alt="" id='log_in'/> Logout</a></li>
					</ul>
                    <?php endif; ?>
				</div><!-- /.navbar-collapse -->
				</nav>
				<div class="modal fade" id='signUpModal'>
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">註冊會員</h4>
							</div>
							<div class="modal-body">
								<div class="fb_login">
									<h4>GetMore 是亞洲第一個二手衣時尚購物網站</h4>
									<a class='fb_login_btn btn btn-primary' href="<?php echo site_url('/auth/facebook?redirect_url=' . urlencode(current_url()))?>">用facebook登入</a> 快使用facebook快速登入!
									<hr class="dashed_hr"/>
								</div>
								<div id="getmore_login">
									<div class="row">
										<div class="col-md-4">
											<h4>註冊會員</h4> 
										</div>
										<div class="col-md-8 pull-right announcement getmore_instruction">
											還不是GetMore的會員嗎？ 免費加入！
										</div>
									</div>
									<form role="form">
										<div class="form-group">
											<label for="sign_up_email">信箱</label><div class='col-md-5 pull-right description'>此為您的會員登入帳號</div>
											<input type="email" class="form-control" id="sign_up_email" placeholder="email">
										</div>
										<div class="form-group">
											<label for="nickname">暱稱</label> <div class='col-md-5 pull-right description'>14字元以內</div>
											<input type="text" class="form-control" id="nickname" placeholder="暱稱">
										</div>
										<div class="form-group">
											<label for="password">密碼</label>
											<input type="password" class="form-control" id="password" placeholder="Password">
										</div>
										<a id='start_to_getmore' href="#" class='btn btn-primary'>開始GetMore</a>
                                        <div id='registration' class="col-md-4 pull-right">
											已是會員？ <a id='logIn' href="#logInModal" data-toggle="modal">由此登入！</a>
										</div>
									</form>
								</div>
							</div>
							<div class="modal-footer">
								<p class='announcement'>已成為「會員」或是點擊「用facebook 登入」按鈕的同時,表示您已詳讀並同意GetMore的<a href="#">使用者條款</a>。</p>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				<div class="modal fade" id='logInModal'>
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">會員入口</h4>
							</div>
							<div class="modal-body">
								<div class="fb_login">
									<h4>GetMore 是亞洲第一個二手衣時尚購物網站</h4>
									<a class='fb_login_btn btn btn-primary' href="<?php echo site_url('auth/facebook?redirect_url=' . urlencode(current_url()))?>">用facebook登入</a> 快使用facebook快速登入!
									<hr class="dashed_hr"/>
								</div>
								<div id="getmore_login">
									<div class="row">
										<div class="col-md-4">
											<h4>會員登入</h4> 
										</div>
										<div class="getmore_instruction col-md-8 pull-right announcement">
											若已是GetMore的會員,請在此登入!
										</div>
									</div>
									<form name="login-form" action="/auth/login" method="POST">
										<div class="form-group">
											<label for="email">信箱</label>
											<input type="email" class="form-control" id="email" name="identity" placeholder="email">
										</div>
										<div class="form-group">
											<label for="password">密碼</label> <a id='forget_password' class="pull-right" href="#">忘記密碼？</a>
											<input type="password" name="password" class="form-control" id="password" placeholder="Password">
										</div>
										<a id='start_to_getmore' href="javascript:document.forms['login-form'].submit();" class='btn btn-primary'>開始GetMore</a>
										<input type="hidden" name="redirect_url" value="<?php echo urlencode(current_url());?>" />
                                        <div id='registration' class="col-md-4 pull-right">
											還不是會員? <a id='register' href='#'>來註冊!</a>
										</div>
									</form>
								</div>
							</div>
							<div class="modal-footer">
								<p class='announcement'>已成為「會員」或是點擊「用facebook 登入」按鈕的同時,表示您已詳讀並同意GetMore的<a href="#">使用者條款</a>。</p>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
				<div class="modal fade" id='videomodel'>
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">GetMore 理念</h4>
							</div>
							<div class="modal-body">
								<iframe src="//player.vimeo.com/video/74211715" width="500" height="364" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> 
							</div>
							<div class="modal-footer">
								<p class='announcement'>已成為「會員」或是點擊「用facebook 登入」按鈕的同時,表示您已詳讀並同意GetMore的<a href="#">使用者條款</a>。</p>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
				<div id='header' class='row'>
					<div id='logo' class='col-md-12'>
						<a href='home.html'><img src='/assets/images/getmore.png' /></a>
					</div>
					<div class='col-md-7'>
						<ul class='nav navbar-nav tags'>
							<li>NEW</li>
							<li>WOMEN</li>
							<li>MEN</li>
							<li>KIDS/BABY</li>
						</ul>
					</div>
					<div id='header_right_section' class='col-md-5'>
						<ul class='nav navbar-nav tags'>
							<li>新手上路</li>
							<li onclick="location.href='/sellorder/steps/1';">賣衣服</li>
							<li>About</li>
						</ul>
					</div>
				</div>
			</div><!-- header -->
			<div class='main'>
            	<?php if (isset($is_home)): ?>
				<div id="main-carousel" class="carousel slide">
					<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<div class="item active">
							<img src="/assets/images/banner/banner-02.jpg">
						</div>
						<div class="item">
							<img src="/assets/images/banner/banner-03.jpg">
						</div>
						<div class="item">
							<img src="/assets/images/banner/banner-05.jpg">
						</div>
						<div class="item">
							<img src="/assets/images/banner/banner-06.jpg">
						</div>
						<div class="item">
							<img src="/assets/images/banner/banner-07.jpg">
						</div>
						<div class="item">
							<img src="/assets/images/banner/banner-08	.jpg">
						</div>
					</div>

					<!-- Controls -->
					<a class="left carousel-control" href="#main-carousel" data-slide="prev">
						<span class="icon-prev"></span>
					</a>
					<a class="right carousel-control" href="#main-carousel" data-slide="next">
						<span class="icon-next"></span>
					</a>
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#main-carousel" data-slide-to="0" class="active"></li>
						<li data-target="#main-carousel" data-slide-to="1"></li>
						<li data-target="#main-carousel" data-slide-to="2"></li>
						<li data-target="#main-carousel" data-slide-to="3"></li>
						<li data-target="#main-carousel" data-slide-to="4"></li>
						<li data-target="#main-carousel" data-slide-to="5"></li>
						<li data-target="#main-carousel" data-slide-to="6"></li>
						<li data-target="#main-carousel" data-slide-to="7"></li>
					</ol>
				</div>
                <?php endif;?>

			<div class="homead">
                <?php if (isset($is_home)): ?>
				<div class="panel-body">
					<div class="homead-box">
					<div class="col-md-4">
            		 <a data-toggle="modal" href="#videomodel"><img src="/assets/images/ad/ad-01.png" alt=""></a>
          			</div>	
					
					<div class="col-md-4">
            		 <img src="/assets/images/ad/ad-02.png" alt=""> 
          			</div>		
					
					<div class="col-md-4">
            		 <img src="/assets/images/ad/ad-03.png" alt=""> 
          			</div>	
					</div>
				</div>
                <?php endif;?>
			</div>
            <?php if (isset($is_home)===false) echo $content; ?>
            <?php if (isset($is_home)): ?>
			<div class="panel">
				<div class="panel-body">
					<div class="panel-box">
					<div class="col-md-2">
						<h5>關於GetMore</h5>
						<div class="panel-link">
							<a href="http://blog.getmore.cc/?page_id=119">
							<h6>GetMore簡介</h6>
							</a>
							<a href="http://blog.getmore.cc/?page_id=119">
							<h6>GetMore Energy</h6>
							</a>
							<a href="http://blog.getmore.cc/?page_id=119">
							<h6>GetMore理念</h6>
							</a>
							<a href="http://blog.getmore.cc/?page_id=119">
							<h6>合作夥伴</h6>
							</a>
						</div>
					</div>
					<div class='col-md-2'>
						<h5>開始GetMore</h5>
						<div class="panel-link">
							<a href="http://blog.getmore.cc/?page_id=119">
							<h6>我要賣衣服</h6>
							</a>
							<a href="http://blog.getmore.cc/?page_id=119">
							<h6>我要洗衣服</h6>
							</a>
							<a href="http://blog.getmore.cc/?page_id=119">
							<h6>我要買衣服</h6>
							</a>
							<a href="http://blog.getmore.cc/?page_id=119">
							<h6>使用條款</h6>
							</a>
						</div>
					</div>
					<div class='col-md-2'>
						<h5>購買說明</h5>
						<div class="panel-link">
							<a href="http://blog.getmore.cc/?page_id=119">
							<h6>付款與運費</h6>
							</a>
							<a href="http://blog.getmore.cc/?page_id=119">
							<h6>配送方式</h6>
							</a>
							<a href="http://blog.getmore.cc/?page_id=119">
							<h6>關於退貨</h6>
							</a>
						</div>
					</div>
					<div class='col-md-2'>
						<h5>公益服務</h5>
						<div class="panel-link">
							<a href="http://blog.getmore.cc/?page_id=119">
							<h6>GetMore Love</h6>
							</a>
						</div>
					</div>
					<div class='col-md-2'>
						<h5>客服中心</h5>
						<div class="panel-link">
							<a href="http://blog.getmore.cc/?page_id=119">
							<h6> Q&A </h6>
							</a>
							<h4> Contact us <br> service@getmore.cc </h4>
							<img src="/assets/images/fb.png" alt="" />
						</div>
					</div>
					</div>
				</div>
				<div class="panel-footer">
					<p>咩斯特數位整合有限公司 Copyright © 2013 GetMore.cc INC. All rights reserved.</p>
				</div>
			</div>
            <?php endif;?>
		</div>
        <?php echo $scripts_footer; ?>
		<?php (isset($debug)) && _log($_SESSION);?>
		</body>
	</html>

