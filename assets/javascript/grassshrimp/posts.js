$(function(){
    var root = $('[getmore-post]');
    var message_template = _.template('<div class="reply clr"><img class="avatar" src=""><div class="subject"><p class="text magictext"><%=content%></p><div class="msgfrom">來自你</div><div class="created"><%=created_time%></div></div></div>');
    var message_template_with_photo = _.template('<div class="reply clr"><img class="avatar" src=""><div class="subject"><p/><p/><img src= http://getmore.s3.amazonaws.com/messages/<%=file%>.jpg ><p class="text magictext"><%=content%></p><div class="created"><%=created_time%></div></div></div>');
    var post_template = _.template('<div class="row clr " id="<%=id%>"><span class="replied"></span><div class="from"><a href="/closet/view/<%=parseInt(user_id)+9881%>" class="avatar" target="_blank"><img src="<%=profile_pic_url%>"></a><a href="/closet/view/<%=parseInt(user_id)+9881%>" target="_blank"><%=user_name%></a></div><div class="subject"><div class="line"><b><%=title%></b></div><p class="description magictext"><%=content%></p><div class="created"><%=created_time%></div><a class="delete" mid="<%=id%>" role="receiver">×</a></div></div>');
    root
        .delegate('div.wrap form.message', 'submit', function(e){
            e.preventDefault();

            var formData = new FormData(document.getElementById($(this).attr('id')));
            var message_content = ($(this).find('textarea[name="content"]').val()).replace(/\s/gm,'');
            var alert_message = "禁止私下交易";
            var re = /[0-9]{6,}|電話|私下交易|面交|手續費|[零一二三四五六七八九壹貳參肆伍陸柒捌玖]{6,}|直接跟你買|直接聯絡|傳真|e[-]{0,1}mail|E[-]{0,1}mail|Line|line|一手交錢一手交貨|歡迎來店選購/;
            if (re.test(message_content)){
             alert( alert_message );
             return;
            }
            re =/^([a-z0-9_\.-]+\@[\da-z\.-]+\.[a-z\.]{2,6})$/gm; // mail
            if (re.test(message_content)){
             alert( alert_message );
             return;
            }
            var form = this;
            $.ajax({
                processData: false,
                contentType: false,
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: formData,
                dataType: 'json'
            }).done(function(message){
                form.reset();
            Number.prototype.padLeft = function(base,chr){
               var  len = (String(base || 10).length - String(this).length)+1;
               return len > 0? new Array(len).join(chr || '0')+this : this;
            }
            var d = new Date( message['created_time'].toString() );
            dformat = [ d.getFullYear(),
                        (d.getMonth()+1).padLeft(),
                        d.getDate().padLeft()
                        ].join('-')+
                        ' ' +
                      [ d.getHours().padLeft(),
                        d.getMinutes().padLeft(),
                        d.getSeconds().padLeft()].join(':');
            message['created_time'] = dformat;

                if( message['file'] == null )
                    var html = message_template(message);
                else
                    var html = message_template_with_photo(message);
                if($('div.reply.clr', $(form).parents('div.hidable')).length){
                    $('div.reply.clr:last').after(html);
                }
                else {
                    $(form).parent('div.wrap').before($('<div>').addClass('messages').append(html));
                }
            }).error(function(response){
                console.log(response);
            });
        })
        .delegate('.readmore_messages', 'click', function(e){
            $.ajax({
                type: 'GET',
                url: '/api/posts/get_all_message_of_post',
                data: {'post_id': $(this).data('post_id')},
                dataType: 'json'
            }).done(function(messages){
                var html = '';
                _.each(messages, function(message){
                    html += message_template(message);
                });
                $(e.target).toggleClass('readmore_messages readless_messages').html('讀較少留言').next('div.messages').html(html);
            }).error(function(response){
                console.log(response);
            });
        })
        .delegate('.readless_messages', 'click', function(e){
            $('div.reply.clr:not(:last)', $(e.target).parent()).remove();
            $(e.target).toggleClass('readmore_messages readless_messages').html('看其他留言')
        })
        .delegate('form.post', 'submit', function(e){
            e.preventDefault();
            var form = this;
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: 'json'
            }).done(function(post){
                form.reset();
                var html = post_template(post);
                $(form).before(html);
            }).error(function(response){
                console.log(response);
            });
        });
})