/*
Handlebars.registerHelper('is_index_text', function() {
    return new Handlebars.SafeString(
    (this.options & RT.Flags._IS_INDEX) ? '<span style="color:blue">是</span>' : '<span style="color:red">否</span>');
});

Handlebars.registerHelper('is_enabled_text', function() {
    return new Handlebars.SafeString(
    (this.options & RT.Flags._IS_ENABLED) ? '<span style="color:blue">是</span>' : '<span style="color:red">否</span>');
});

Handlebars.registerHelper('is_index_radio', function() {
    return new Handlebars.SafeString(
    (this.options & RT.Flags._IS_INDEX) ? 'checked' : '');
});

Handlebars.registerHelper('is_enabled_radio', function() {
    return new Handlebars.SafeString(
    (this.options & RT.Flags._IS_ENABLED) ? 'checked' : '');
});
*/
Handlebars.registerHelper('date_time', function(time) {
    return moment.unix(time).format('YYYY-MM-DD HH:mm:ss');
});

Handlebars.registerHelper('checkbox', function(value) {
    return (value == '1') ? 'checked' : '';
});
