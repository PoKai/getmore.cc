 
    $('body').delegate('*', 'change', function(e){
        $(window).bind('beforeunload', function(e){
            return "確定離開嗎？離開的話 剛才所輸入的資訊都會消失喔！";
        });

        $('body').undelegate('*', 'change');
    });

    (function(){
        var file_count = 5;

        $('div.clothing_view_list').delegate('a.del_file', 'click', function(e){
            if(file_count === 5){
                return false;
            }
            var target = e.target;
            $.ajax({
                url: '/restful/marketplace/files/'+$(target).data('file_id'),
                type: 'DELETE'
            }).done(function(){
                $(target).parents('div.clothing_view').remove();
                file_count++;
            });
        });

        $("div.clothing_view_list").sortable();
        $("div.clothing_view_list").disableSelection();

        $('#fileupload').fileupload({
            dataType: 'json',
            acceptFileTypes: /\.(gif|jpe?g|png)$/i,
            maxFileSize: 5000000,
            autoUpload: false,
            done: function (e, data) {
                var img = $('<img class="clothing_view_img">').attr('src', 'http://s3.getmore.cc/clothing/'+data.result[0]+'/medium.jpg');
                var del_btn = $('<a class="btn btn-danger del_file">').data('file_id', data.result[0]).html('刪除');
                var input = $('<input type="hidden" name="files[]">').attr('value', data.result[0]);
                $('i.clothing_loading_icon:first').after(input).after(del_btn).replaceWith(img);
            },
            add: function (e, data) {
                if(file_count > 0){
                    file_count--;
                    data.submit();
                }
                else {
                    $('#fileupload').hide();
                }
            }
        }).bind('fileuploadsubmit', function(){
            var div = $('<div class="col-xs-6 col-md-3 clothing_view"><a class="thumbnail text-center"><i class="fa fa-spinner fa-spin fa-5x clothing_loading_icon"></i></a></div>');
            
            $('div.clothing_view_list').append(div);
        });
    }());

    $(function() {

        if(location.hash == "#success") {
            alert("上架成功！");
            location.hash = "";
        }

        //tag input
        var exist_style = {{ styles | json_encode() }};
        
        $('#free_style').tokenfield({
            autocomplete: {
                source: exist_style,
                delay: 100,
                minLength: 1
            },
            showAutocompleteOnFocus: true
        }).on('tokenfield:createtoken', function (event) {
            var existingTokens = $(this).tokenfield('getTokens');
            $.each(existingTokens, function(index, token) {
                if (token.value === event.attrs.value)
                    event.preventDefault();
            });
        });
        //$('#free_style').tokenfield('setTokens', 'blue,red,white');
        $('#free_style').parent().keypress(function(e){
            if(e.keyCode === 13) {
                e.preventDefault();
            }
        })

        $('.numbersOnly').keyup(function () { 
            this.value = this.value.replace(/[^0-9\.]/g,'');
        });

        $("#launchForm").submit(function(e){
            console.log(e);
            var have_error = false;
            var error_input = ["請完成以下必填欄位："];
            $('.has-error').removeClass('has-error');
            $('.error-div').removeClass('error-div');

            if ($("input[name='color']:checked").length == 0){
                $(".selectcolor").addClass('error-div');
                have_error = true;
                error_input.push("顏色");
            }
            if ($("input[name='product_title']").val() == ""){
                $("input[name='product_title']").parent('div').addClass('has-error');
                have_error = true;
                error_input.push("商品名稱");             
            }
            if ($("input[name='product_brand']").val() == ""){
                $("input[name='product_brand']").parent('div').addClass('has-error');
                have_error = true;
                error_input.push("品牌");
            }
            if ($("select[name='item_status']").val() == 0){
                $("select[name='item_status']").parent('div').addClass('error-div');
                have_error = true;
                error_input.push("商品狀況");             
            }
            if ($("textarea[name='product_comments']").val() == ""){
                $("textarea[name='product_comments']").parent('div').addClass('has-error');
                have_error = true;
                error_input.push("商品敘述");                
            }
            if ($('img.clothing_view_img').length == 0){
                have_error = true;
                error_input.push("上傳照片");                
            }
            if ($("select[name='size_type_major']").val() == 0){
                $("select[name='size_type_major']").parent('div').addClass('error-div');
                have_error = true;
                error_input.push("尺寸");                
            }
            if ($("select[name='product_original_price']").val() == 0){
                $("select[name='product_original_price']").parent('div').addClass('error-div');
                have_error = true;
                error_input.push("商品原始價格");             
            }
            if ($("input[name='product_sell_price']").val() == ""){
                $("input[name='product_sell_price']").parent('div').addClass('has-error');
                have_error = true;
                error_input.push("商品售價");
            }

            console.log(typeof $("select[name='size_type_major']").val());
            console.log(typeof $("select[name='size']").val());
            if (have_error == true) {
                error_input = error_input.join('\n');
                alert(error_input);
                return false;
            }
        });
    });

    app
        .controller('launch', ['$scope', '$element', '$http', function ($scope, $element, $http) {
            
            $scope.category = 1;
            $scope.category2 = 11;

            $scope.size_type_major = '0';

            $scope.measure = {
                1: '',
                2: '',
                3: '',
                4: '',
                5: '',
                6: '',
                7: '',
                8: '',
                9: '',
                10: ''
            };
            //default values
            $scope.product_original_price = "0";
            $scope.item_status = "0";

            $scope.$watch('category', function(newVal, oldVal){
                $scope.category2 = newVal + '1';
            });

            $scope.$watch('product_original_price', function(newVal, oldVal){
                if ($scope.product_original_price == "0"){
                    $scope.suggest_price = "";
                } else if ($scope.product_original_price == "1000"){
                    $scope.suggest_price = 150;
                } else if ($scope.product_original_price == "2000"){
                    $scope.suggest_price = 300;
                } else if ($scope.product_original_price == "3000"){
                    $scope.suggest_price = 500;
                } else if ($scope.product_original_price == "4000"){
                    $scope.suggest_price = 700;
                } else if ($scope.product_original_price == "5000"){
                    $scope.suggest_price = 900;
                } else if ($scope.product_original_price == "6000"){
                    $scope.suggest_price = 1100;
                } else if ($scope.product_original_price == "7000"){
                    $scope.suggest_price = 1300;
                } else if ($scope.product_original_price == "8000"){
                    $scope.suggest_price = 1500;
                } else if ($scope.product_original_price == "9000"){
                    $scope.suggest_price = 1700;
                } else if ($scope.product_original_price == "10000"){
                    $scope.suggest_price = 1900;
                } else if ($scope.product_original_price == "15000"){
                    $scope.suggest_price = 2500;
                } else if ($scope.product_original_price == "20000"){
                    $scope.suggest_price = 3500
                } else if ($scope.product_original_price == "25000"){
                    $scope.suggest_price = 4500;
                } else if ($scope.product_original_price == "30000"){
                    $scope.suggest_price = 5500;
                } else if ($scope.product_original_price == "35000"){
                    $scope.suggest_price = 6500;
                } else if ($scope.product_original_price == "40000"){
                    $scope.suggest_price = 7500;
                } else if ($scope.product_original_price == "45000"){
                    $scope.suggest_price = 8500;
                } else if ($scope.product_original_price == "50000"){
                    $scope.suggest_price = 9500;
                } else if ($scope.product_original_price == "60000"){
                    $scope.suggest_price = 11000;
                } else if ($scope.product_original_price == "70000"){
                    $scope.suggest_price = 13000;
                } else if ($scope.product_original_price == "80000"){
                    $scope.suggest_price = 15000;
                } else if ($scope.product_original_price == "90000"){
                    $scope.suggest_price = 17000;
                } else if ($scope.product_original_price == "100000"){
                    $scope.suggest_price = 19000;
                } else if ($scope.product_original_price == "999999"){
                    $scope.suggest_price = 20000;
                }
            });

            $scope.$watch('category2', function(newVal, oldVal){
                // 上衣款式預設值
                if(_.contains(['11', '21', '32'], newVal)){
                    $scope.r_pattern = '3';
                }
                // 襯衫款式預設值
                else if(_.contains(['22'], newVal)){
                    $scope.r_pattern = '8';
                }
                // 洋裝款式預設值
                else if(_.contains(['12'], newVal)){
                    $scope.r_pattern = '31';
                }
                // 成人褲子款式預設值
                else if(_.contains(['13', '23'], newVal)){
                    $scope.r_pattern = '5';
                }
                // 小孩褲子款式預設值
                else if(_.contains(['33'], newVal)){
                    $scope.r_pattern = '21';
                }
                // 裙子款式預設值
                else  if(_.contains(['14'], newVal)){
                    $scope.r_pattern = '16';
                }
                // 成人外套款式預設值
                else if(_.contains(['15', '24'], newVal)){
                    $scope.r_pattern = '24';
                }
                // 小孩外套款式預設值
                else if(_.contains(['35'], newVal)){
                    $scope.r_pattern = '4';
                }


                if(_.contains(['11', '12', '21', '22', '32', '34'], newVal)){
                    $scope.r_long = 'ns';
                }
                else if(_.contains(['15', '24'], newVal)){
                    $scope.r_long = 'sb';
                }
                else if(_.contains(['13', '23', '33'], newVal)){
                    $scope.r_long = 'sp';
                }
            });

            $scope.$watch('size_type_major', function(newVal, oldVal){
                switch(newVal){
                    case 'US':
                    case 'JP':
                        $scope.$parent.size = 0;
                        break;
                    case 'UK':
                        $scope.$parent.size = 4;
                        break;
                    case 'EU':
                        $scope.$parent.size = 32;
                        break;
                    case 'IT':
                        $scope.$parent.size = 34;
                        break;
                    case '牛仔褲':
                        $scope.$parent.size = 27;
                        break;
                }
            });

            $scope.$watch('product_comments', function(newVal, oldVal){
                if(!newVal) {
                    $scope.remain_count = 100;
                }
                else {
                    $scope.remain_count = 100 - newVal.length;
                }
            });

            $scope.submit = function(e){
                $(window).unbind('beforeunload');
            }

            // autocomplete product_brand
            var availableTags = {{ brands | json_encode() }};
            $("#product_brand").autocomplete({
                source: function(req, responseFn) {
                    var re = $.ui.autocomplete.escapeRegex(req.term);
                    var matcher = new RegExp( "^" + re, "i" );
                    var a = $.grep( availableTags, function(item,index){
                        return matcher.test(item);
                    });
                    responseFn( a );
                }                 
            }).bind('keypress blur', function(e){
                $scope.product_brand = $(this).val();
                if(e.type === 'keypress' && e.keyCode !== 13) return;
                var reg = new RegExp('^'+$(this).val()+'$', "i");
                var brand = _.find(availableTags, function(tag){
                  return reg.test(tag);
                });
                if(e.keyCode == 13){
                    e.preventDefault();
                    var inputs = $(this).closest('form').find(':input:visible');
                    inputs.eq( inputs.index(this)+ 1 ).focus();
                };

                if(brand){
                  $(this).val(brand);
                }
            })
        }]);