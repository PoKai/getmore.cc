/*
 * Bacbkbone View for the shopping cart in selling procedure.
 */
// the namespace for the whole selling procedure
// as a OOP method
var SellApp = SellApp || {};

SellApp.Helper = {
    getTypeName: function(typeNumber){
        var mappingJSON = {"1":"Women","11":"\u4e0a\u8863","12":"\u6d0b\u88dd","13":"\u8932\u5b50","14":"\u88d9\u5b50","15":"\u5916\u5957","151":"\u4e00\u822c\u5916\u5957","152":"\u7fbd\u7d68\u8863", "153": "\u771f\u76ae\u5916\u5957","2":"Men","21":"\u4e0a\u8863","22":"\u896f\u886b","23":"\u8932\u5b50","24":"\u5916\u5957","241":"\u4e00\u822c\u5916\u5957","242":"\u7fbd\u7d68\u8863", "243": "\u771f\u76ae\u5916\u5957","3":"Kids \/ Baby","31":"\u9023\u8eab\u8863","32":"\u4e0a\u8863","33":"\u4e0b\u8eab","34":"\u6d0b\u88dd","35":"\u5916\u5957","351":"\u4e00\u822c\u5916\u5957","352":"\u7fbd\u7d68\u8863\/\u80cc\u5fc3"};
        return mappingJSON[typeNumber];
    },

    
    getReturnName: function(donative){
        if (donative == '1'){
            return '捐贈';
        }
        else{
            return '退回';
        }
    },

    getPriceLabel: function(priceValueInDatabase){

        var priceLabel = '';
        
        if (priceValueInDatabase == '1000')
            priceLabel = '500~1,000';
        else if (priceValueInDatabase == '2000')
            priceLabel = '1,001~2,000';
        else if (priceValueInDatabase == '3000')
            priceLabel = '2,001~3,000';
        else if (priceValueInDatabase == '4000')
            priceLabel = '3,001~4,000';
        else if (priceValueInDatabase == '5000')
            priceLabel = '4,001~5,000';
        else if (priceValueInDatabase == '6000')
            priceLabel = '5,001~6,000';
        else if (priceValueInDatabase == '7000')
            priceLabel = '6,001~7,000';
        else if (priceValueInDatabase == '8000')
            priceLabel = '8,001~9,000';
        else if (priceValueInDatabase == '9000')
            priceLabel = '8,001~9,000';
        else if (priceValueInDatabase == '10000')
            priceLabel = '9,001~10,000';
        else if (priceValueInDatabase == '15000')
            priceLabel = '10,001~15,000';
        else if (priceValueInDatabase == '20000')
            priceLabel = '15,001~20,000';
        else if (priceValueInDatabase == '25000')
            priceLabel = '20,001~25,000';
        else if (priceValueInDatabase == '30000')
            priceLabel = '25,001~30,000';
        else if (priceValueInDatabase == '35000')
            priceLabel = '30,001~35,000';
        else if (priceValueInDatabase == '40000')
            priceLabel = '35,001~40,000';
        else if (priceValueInDatabase == '45000')
            priceLabel = '40,001~45,000';
        else if (priceValueInDatabase == '50000')
            priceLabel = '45,001~50,000';
        else if (priceValueInDatabase == '60000')
            priceLabel = '50,001~60,000';
        else if (priceValueInDatabase == '70000')
            priceLabel = '60,001~70,000';
        else if (priceValueInDatabase == '80000')
            priceLabel = '70,001~80,000';
        else if (priceValueInDatabase == '90000')
            priceLabel = '80,001~90,000';
        else if (priceValueInDatabase == '100000')
            priceLabel = '90,001~100,000';
        else if (priceValueInDatabase == '999999')
            priceLabel = '超過100,000';
        
        return priceLabel;

    }
    
};

SellApp.Config = {
    typeNames: null,
    typePrices: null,
    
    importTypeName: function(typeNames){
        this.typeNames = typeNames;
    },

    importTypePrice: function(typePrices){
        this.typePrices = typePrices;
    },
    
    getTypeName: function(typeNumber){
        return this.typeNames[typeNumber];
    },

    getTypePrice: function(typeNumber){        
        var result = null;
        if(typeNumber.length==2)
          result = this.typePrices[typeNumber.substr(0,1)][typeNumber.substr(0,2)];
        else if(typeNumber.length==3)
          result = this.typePrices[typeNumber.substr(0,1)][typeNumber.substr(0,2)][typeNumber];
        
        return result;
    }

};

SellApp.CartView = Backbone.View.extend({

    el: "body",

    _isDev: true,
    typeNames: null,
    typePrices: null,

    // Delegated events for creating new items, and clearing completed ones.
    events: {
        "click #product-view-buy": "addToCart",
        "click #clear_cart": "clearCart",
        "click .delete_item": "deleteItem",
        "change .item_class": 'changeTypeName',
        "click .add-sellcart": 'addToCart',
        "click .delete-sellcart": 'deleteItem',
        "click .edit-sellcart": 'editItem',
        "click .save-sellcart": 'updateItem',
        "click .go-second": 'goSecond'        
    },

    initialize: function() {
        if (this._isDev){ console.log('cart view initialized.'); }
        // binding the collection with events
        this.collection.bind('change', this.render, this);

        // fetch!
        this.collection.fetch();
    },
    
    firstStep: function(){
        // type names are sent by PHP.
        // it's hidden in html input.
        var typeNames = JSON.parse($('#type_name').val());
        
        // import and render the product type radio buttons.
        this.importTypeName(typeNames);
        this.renderTypeName('1');
        
        // similar to typeNames
        this.typePrices = JSON.parse($('#type_price').val());

        // Set configs
        SellApp.Config.importTypeName(typeNames);
        SellApp.Config.importTypePrice(this.typePrices);
        
        if (this._isDev){
            // console.log('current product type selected: ' + this.getCurrentTypeNumber());
        }      
        this.collection.trigger('change');
    },
    
    thirdStep: function(){
      
        // Prevent users from weird behaviors
        if ( !this.collection.length ){
            window.location.href = '/sell/first';
        }
        
        // similar to typeNames
        this.typePrices = JSON.parse($('#type_price').val());
        
        if (this._isDev){
            console.log('execute third step.');
        }
        this.renderInfo();      
    },
    
    goSecond: function(e){
        if ( !this.collection.length ){
            alert('請填入衣服資訊。');
            return false;
        }
    },
    
    importTypeName: function(typeNames){
        if (this._isDev){
            console.log('cart import type name.'); 
            // console.log('this is type_names fetched:');
            // console.log(typeNames);
        }
        this.typeNames = typeNames;
        var $level_1 = $('.level_1');
        var $level_2 = $('.level_2');
        var $level_3 = $('.level_3');
        for(typeNumber in typeNames){
            if (typeNumber.length === 1){
                var option = '<div class="radio"><label><input type="radio" value="'+typeNumber+'" name="item_class1" class="item_class1 item_class">'+typeNames[typeNumber]+'</label></div>';
                $level_1.append(option);
            }
            else if (typeNumber.length === 2){
                var option = '<div class="radio"><label><input type="radio" value="'+typeNumber+'" name="item_class2" class="item_class2 item_class">'+typeNames[typeNumber]+'</label></div>';
                $level_2.append(option);
            }
            else if (typeNumber.length === 3){
                var option = '<div class="radio"><label><input type="radio" value="'+typeNumber+'" name="item_class3" class="item_class3 item_class">'+typeNames[typeNumber]+'</label></div>';
                $level_3.append(option);
            }
        }
    },

    changeTypeName: function(e){
        var typeNumber = $(e.currentTarget).val();
        this.renderTypeName(typeNumber);        
    },

    // get current type number according to the radio buttons
    getCurrentTypeNumber: function(){
        return $('.item_class:visible:checked').last().val();
    },

    renderTypeName: function(typeNumber){
        if (this._isDev){
            console.log('render type name for:' + typeNumber);
        }
        
        // it's the level 1 radio button
        if (typeNumber.length === 1){
            // hide all level 3 buttons
            $('.item_class3').prop('checked', false);
            $('.level_3').hide();
            
            // the parent of parent structure here is quite ugly.
            // but it's compatible with the html code.
            // so fine for now.
            $('.item_class2').parent().parent().hide();
            $('.item_class2').prop('checked', false);
            $('.item_class2').each(function(){
                var targetTypeNumber = $(this).val();
                if (targetTypeNumber.substr(0,1)===typeNumber){
                    $(this).parent().parent().show();
                }
            });
          //  $('.item_class2:visible').first().prop('checked', true);
        }
        // it's the level 2 radio button
        else if (typeNumber.length === 2){
            $('.item_class[value="' + typeNumber.substr(0,1) + '"]').trigger('click');

            // hide all level 3 buttons
            $('.item_class3').prop('checked', false);
            $('.level_3').hide();
            
            // the parent of parent structure here is quite ugly.
            // but it's compatible with the html code.
            // so fine for now.
            $('.item_class3').parent().parent().hide();
            $('.item_class3').prop('checked', false);
            var show_level3 = false;
            
            $('.item_class3').each(function(){
                var targetTypeNumber = $(this).val();
                if (targetTypeNumber.substr(0,2)===typeNumber){
                    show_level3 = true;
                    $(this).parent().parent().show();
                }
            });
                      
          
            // only show level3 when necessary
            if (show_level3){
                $('.level_3').show();            
        //        $('.item_class3:visible').first().prop('checked', true);
            }
        }
        else{
            $('.item_class[value="' + typeNumber.substr(0,2) + '"]').trigger('click');          
        }
        
        // in case it's not triggered by the click event.
        $('.item_class[value="' + typeNumber + '"]').prop('checked', true);

        if (this._isDev){
            // console.log('current product type selected: ' + this.getCurrentTypeNumber());
        }

    },

    clearFields: function(){
        $('#product_desc').val('');
        this.renderTypeName('1');
        $('#product_brand').val('');
        $('#original_price').val('1000');
        $('#sell_price').val('');
        $('.donative').last().prop('checked', true),
        $('#product_note').val('')
    },

    addToCart: function(e) {
        var item = {
            product_title: $('#product_desc').val(),
            product_type: this.getCurrentTypeNumber(),
            product_brand: $('#product_brand').val(),
            product_original_price: $('#original_price').val(),
            product_sell_price: $('#sell_price').val(),
            donative: $('.donative:checked').val(),
            comment: $('#product_note').val()     
        };
        
        // call the collection insert method
        try{
            this.collection.insertItem( item );
            this.clearFields();
        }
        catch(e){
            alert(e);
        }
    },

    renderList: function(){
        if(this._isDev){
            console.log('refresh the list. after refreshing, now it contains:');
            console.log(this.collection.toJSON());
        }

        var template = _.template($('#items_list_template').html(), {items: this.collection.models});
        $("#items_list").html(template);
        
    },
    
    deleteItem: function(e) {
        if (!confirm('確定刪除？')){
            return false;
        }
        var id = $(e.currentTarget).data("id");        
        var item = this.collection.get(id);
        item.destroy();
        this.render();
        
        // why this failed? I can't figure it out -,.-
        // this.collection.remove(item);

        if (this._isDev){
            console.log('delete item.');          
        }
    },


    
    editItem: function(e) {

        var id = $(e.currentTarget).data("id");        
        var item = this.collection.get(id);

        $('#myModal').modal('show');
        $('#edit_product_id').val(item.get('id'));
        this.renderTypeName(item.get('product_type'));
        $('#edit_product_desc').val(item.get('product_title'));
        $('#edit_product_brand').val(item.get('product_brand'));
        $('#edit_original_price').val(item.get('product_original_price'));        
        $('#edit_sell_price').val(item.get('product_sell_price'));
        
        if( item.get('donative') == "1" )
          $('input[name="edit_donative"]:eq(0)').trigger('click');
        else if( item.get('donative') == "0" )
          $('input[name="edit_donative"]:eq(1)').trigger('click');
        
        $('#edit_product_note').val(item.get('comment'));
                
        if (this._isDev){
            console.log('edit item.');          
        }
    },
    
    updateItem:function(){
        var id = $('#edit_product_id').val();
        var item = this.collection.get(id);
        
        try{
            item.updateItem({
                product_title: $('#edit_product_desc').val(),
                product_type: this.getCurrentTypeNumber(),
                product_brand: $('#edit_product_brand').val(),
                product_original_price: $('#edit_original_price').val(),       
                product_sell_price: $('#edit_sell_price').val(),
                donative: $('.edit_donative:checked').val(),
                comment: $('#edit_product_note').val()
            });
            $('#myModal').modal('hide');                 
        }
        catch(e){
            alert(e);
        }

    },
        
    render: function(){
        this.renderList();
        this.renderInfo();
    },
    
    getTypePrice: function(typeNumber){
        if (this._isDev){
            // console.log('type prices are:');
            // console.log(this.typePrices);
        }
        var result = null;
        if(typeNumber.length==2)
          result = this.typePrices[typeNumber.substr(0,1)][typeNumber.substr(0,2)];
        else if(typeNumber.length==3)
          result = this.typePrices[typeNumber.substr(0,1)][typeNumber.substr(0,2)][typeNumber];
        else
          throw new Error('Type price is not defined.');
        
        return result;
    },
    
    renderInfo: function(){
        var numbers = {};
        var washingFee = 0;
        var shippingFee = 0;
        var totalFee = 0;

        numbers['all'] = this.collection.length;

        var self = this;
        this.collection.each(function(item){
            var price = self.getTypePrice(item.get('product_type'));
            if (price in numbers){
                numbers[price] ++;
            }
            else{
                numbers[price] = 1;
            }
            washingFee += price;
        });

        // if(washingFee < 400)
        //     shippingFee = 180;
        // else if(washingFee < 600)
        //     shippingFee = 80;
        // else
        //     shippingFee=0;
        shippingFee=120;
            
        totalFee = washingFee + shippingFee;
        
        $('#all_num_value').html(numbers['all']);
        /*
        $('#small_num_value').html(numbers['75'] || 0);
        $('#big_num_value').html(numbers['120'] || 0);
        $('#fly_num_value').html(numbers['150'] || 0);
        */

        $('#washing_fee').html(washingFee);
        $('#transport_fee').html(shippingFee);
        $('#total_fee').html(totalFee);
        $('#new_sell_product').attr("disabled", ( numbers['all'] >= 30 ) ? true : false );
        $('#over20clothing_message').html( ( numbers['all'] >= 30 ) ? '每張代售申請以30件為限，超過請另外申請一張單。' : '' );
    },

});
