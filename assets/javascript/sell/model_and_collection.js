/*
 * Backbone Models and Collections used in selling procedure.
 */

// the namespace for the whole selling procedure
var SellApp = SellApp || {};

SellApp.ItemModel = Backbone.Model.extend({

    updateItem: function(attrs){

        if (!attrs.product_title){
            throw '請輸入商品敘述';
        }
        
        if ( !_.isNumber(SellApp.Config.getTypePrice(attrs.product_type)) ){
            throw '請選擇商品種類';
        }
                
        if (!attrs.product_brand){
            throw '請輸入品牌';
        }
        if (!attrs.product_sell_price){
            throw '請輸入二手販售價格';
        }

        this.set(attrs);
        this.save();
    }

    /*
    validate: function(attrs, options) {
        if (!attrs.product_title){
            throw '請輸入商品敘述';
        }
        
        if ( !_.isNumber(SellApp.Config.getTypePrice(attrs.product_type)) ){
            throw '請選擇商品種類';
        }
                
        if (!attrs.product_brand){
            throw '請輸入品牌';
        }
        if (!attrs.product_sell_price){
            throw '請輸入二手販售價格';
        }
    }
    */
});

SellApp.ItemCollection = Backbone.Collection.extend({
    localStorage: new Backbone.LocalStorage("SellItemCollection"),
    // Reference to this collection's model.
    model: SellApp.ItemModel,
    
    insertItem: function( item ){
      
        if (!item.product_title){
            throw '請輸入商品敘述';
        }
        
        if ( !_.isNumber(SellApp.Config.getTypePrice(item.product_type)) ){
            throw '請選擇商品種類';
        }
                
        if (!item.product_brand){
            throw '請輸入品牌';
        }
        if (!item.product_sell_price){
            throw '請輸入二手販售價格';
        }
        this.create( item );
    }
    
});

SellApp.OrderModel = Backbone.Model.extend({
    localStorage: new Backbone.LocalStorage('SellOrderModel'),
    
    defaults:{
        seller: {
            name: '',
            phone: '',
            location: {
                county: '',
                district: '',
                zipcode: '106',
                address: ''
            }
        },
        receiver: {
            name: '',
            phone: '',
            location: {
                county: '',
                district: '',
                zipcode: '106',
                address: ''
            }
        },
        invoice: {
            'type': '0',
            'number': '',
            'title': ''
        },
        other: {
            'payment_method': '',
            'last_five_number': '',
            'estimated_remit_datetime': ''
        },
        no_return: '0',
        deduction_amount: '0',
        coupon_code: ''
        
        /*,
        receipt_to: '',
        uniform_invoice_type: '',
        uniform_invoice_title: '',
        uniform_invoice_number: '',            
        delivery_time: 'any'
        */
    },
    
    saveOrder: function(order){
        SellApp.Validator.isNotEmpty(order.seller.name, '請輸入您的姓名。');
        SellApp.Validator.isNotEmpty(order.seller.phone, '請輸入您的電話。');
        SellApp.Validator.isNotEmpty(order.seller.location.address, '請輸入您的地址。');

        if ( order.no_return !== '1' ){
            SellApp.Validator.isNotEmpty(order.receiver.name, '請輸入送回資訊。');
            SellApp.Validator.isNotEmpty(order.receiver.phone, '請輸入送回資訊。');
            SellApp.Validator.isNotEmpty(order.receiver.location.address, '請輸入送回資訊。');
        }
        
        // ok, fine. don't clear fields here.
        // mask the fields when sending form data.
        if ( order.invoice.type === '0' ){
            // order.invoice.number = '';
            // order.invoice.title = '';
        }
        else if ( order.invoice.type === '1' ){
            // order.invoice.title= '';            
        }
        else if ( order.invoice.type === '2' ){
            SellApp.Validator.isNotEmpty(order.invoice.number, '請輸入統一編號。');
            SellApp.Validator.isNotEmpty(order.invoice.title, '請輸入發票抬頭。');
        }
      
        this.set(order);
        this.save();
    },

    savePaymentMethod: function(order){
        console.log(order);
        SellApp.Validator.isDefined(order.other.payment_method, '請選擇付款方式。');      

        if ( order.other.payment_method === 'atm' ){
            SellApp.Validator.isNotEmpty(order.other.last_five_number, '請輸入匯款帳號後五碼。');
            SellApp.Validator.isNotEmpty(order.other.estimated_remit_datetime, '請輸入預計匯款時間。');
        }

        //this.set(order);
        this.save(order);
    }    
    
});
