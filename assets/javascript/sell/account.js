/*
 * The account module for checking amount in account and decuction with coupon.
 */

function Account(newBalance) {
    
    var initBalance = parseInt(newBalance);
    var deduction = 0;
    var balance = parseInt(newBalance);
    
    // the total price of the order
    var initTotalPrice = 0;
    var totalPrice = 0;
    
    var couponAmount = 0;
    
    var setTotalPrice = function(p){
        initTotalPrice = p;
        totalPrice = p;
    }
    
    var getBalance = function(){
        return balance;
    }
    var getTotalPrice = function(){
        return totalPrice;
    }
    
    var getInitTotalPrice = function(){
        return initTotalPrice;
    }

    var isPositiveInteger = function(n) {
        return n >>> 0 === parseFloat(n);
    }
    
    var getDeduction = function(){
        return deduction;
    }
        
    var setDeduction = function(newDeduction){

        /* START checking the deduction */
        if ( isNaN(newDeduction) ){
            throw '請輸入數字。';
        }

        deduction = parseInt(newDeduction);

        if ( deduction < 0 ){
            throw '請輸入正整數。';
        }
        if ( !isPositiveInteger(deduction) ){
            throw '請輸入正整數。';
        }
        /* END */

        /* START checking the balance */
        if ( deduction > initBalance ){
            throw '您的帳戶餘額不足。';
        }
        /* END */
        
        if ( deduction > initTotalPrice ){
            throw '應付餘額不可小於零';
        }    
        
        balance = initBalance - deduction;
        totalPrice = initTotalPrice - deduction - couponAmount;
    }

    var setCouponAmount = function(newCouponAmount){
      
        couponAmount = parseInt(newCouponAmount);
        
        totalPrice = initTotalPrice - deduction - couponAmount;
    }

    var getCouponAmount = function(){
        return couponAmount;
    }



    return {
        getBalance: getBalance,
        setDeduction: setDeduction,
        setTotalPrice: setTotalPrice,
        getTotalPrice: getTotalPrice,
        getInitTotalPrice: getInitTotalPrice,
        getDeduction: getDeduction,
        setCouponAmount: setCouponAmount,
        getCouponAmount: getCouponAmount
    };
  
}
