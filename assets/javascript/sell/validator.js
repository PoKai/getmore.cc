/*
 * Helper functions for validating user input in selling procedure.
 * 
 * @author howtomakeaturn
 */

// the namespace for the whole selling procedure
var SellApp = SellApp || {};

SellApp.Validator = {};
SellApp.Validator.isNotEmpty = function (value, errMessage){
    if (value===''){
        throw errMessage;
    }  
}

SellApp.Validator.isDefined = function (value, errMessage){
    if ( typeof value == "undefined"){
        throw errMessage;
    }  
}
