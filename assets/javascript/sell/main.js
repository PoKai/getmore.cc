/*
 * The main process coordinating every part in selling procedure.
 */
// the namespace for the whole selling procedure
var SellApp = SellApp || {};

/*
 * This is the module FRAMEWORK.
 */
SellApp.app = (function(){
    if (!window.jQuery) { throw new Error("SellApp requires jQuery") }

    var $ = window.jQuery;
    
    // this decide to hide all the console messages or not.
    var _is_dev = true;

    var _firstStep = function(){
        if (!SellApp.ItemModel) { throw new Error("enableCart require the SellApp ItemModel") }
        if (!SellApp.ItemCollection) { throw new Error("enableCart require the SellApp ItemCollection") }        
        if (!SellApp.CartView) { throw new Error("enableCart require the SellApp CartView") }        

        var items = new SellApp.ItemCollection;
        var cart = new SellApp.CartView({collection: items});
        cart.firstStep();
        
    };

    var _secondStep = function(){
        if (!SellApp.OrderModel) { throw new Error("enableOrder require the SellApp OrderModel") }
        if (!SellApp.OrderView) { throw new Error("enableOrder require the SellApp OrderView") }
        if(_is_dev){ console.log('initialize order succeed.'); }

        var orderModel = new SellApp.OrderModel({ id: 1 });                
        var order = new SellApp.OrderView({ model: orderModel});

        order.secondStep();     
    };

    var _thirdStep = function(){
        if (!SellApp.ItemModel) { throw new Error("enableCart require the SellApp ItemModel") }
        if (!SellApp.ItemCollection) { throw new Error("enableCart require the SellApp ItemCollection") }        
        if (!SellApp.CartView) { throw new Error("enableCart require the SellApp CartView") }        
        if(_is_dev){ console.log('initialize cart succeed.'); }

        var items = new SellApp.ItemCollection;
        var cart = new SellApp.CartView({collection: items});
        cart.thirdStep();

        var orderModel = new SellApp.OrderModel({ id: 1 });                
        var order = new SellApp.OrderView({ model: orderModel});
        order.thirdStep();
        
        // It should not contain any business logic in this file... orz
        // But I'm not sure how to structure them for now.
        // So, forgive me ;)
        $('#confirm-button').click(function(){
            if ( order.savePaymentMethod() ){
                $('#order').val(JSON.stringify(orderModel));
                $('#items').val(JSON.stringify(items));
                console.log(orderModel);
                $('#final-order').submit();
            }
        });
        
    };

    var _complete = function(){
        // I think it's kind of business logic to clear the localStorage.
        // So I'm not sure if it's proper to do this here. Fine :D
      
        var items = new SellApp.ItemCollection;
        items.fetch();

        _(items.length).times(function(n){ 
            var model = items.models[0]
            model.destroy();
        });
        
        console.log('now the cart is cleared.');        
    };

   

    // the public API interface
    return {
        firstStep: function(){            
            _firstStep();
        },
        secondStep: function(){
            _secondStep();
        },
        thirdStep: function(){            
            _thirdStep();
        },
        complete: function(){            
            _complete();
        },
        
    };
  
}());



window.onload = function(){
   /*
    * This is the module ROUTER.
    */
    var Workspace = Backbone.Router.extend({

        routes: {
          'sell/first': 'sellFirst',
          'sell/second': 'sellSecond',
          'sell/third': 'sellThird',
          'sell/fourth': 'sellComplete',
          'sell/receive': 'sellComplete',
          'sell/webatm_done': 'sellComplete'
        },
        
        sellFirst: function() {
            SellApp.app.firstStep();        
        },
        sellSecond: function() {
            SellApp.app.secondStep();        
        },
        sellThird: function() {
            SellApp.app.thirdStep();
        },
        sellComplete: function() {
            SellApp.app.complete();
        }
        
    });    
    
    workspace = new Workspace;
    Backbone.history.start({pushState: true});
};
