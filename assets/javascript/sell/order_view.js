/*
 * Backbone View for order module in selling procedure.
 * 
 * @author howtomakeaturn
 */


// the namespace for the whole selling procedure
var SellApp = SellApp || {};

SellApp.OrderView = Backbone.View.extend({

    _isDev: true,

    el: "body",

    // Delegated events for creating new items, and clearing completed ones.
    events: {
        "click #prev": "prev",
        "click #next": "next",
        "change #sel_invoice_type": "changeInvoiceType",
        "change input[name='no_return']": "changeReturnType",
        "change #same_as": "changeSameAs",
        "change .payment_method": "changePaymentMethod",
//        "change .deduction_amount": "changeDeduction",
        "click .sure-btn": "changeDeduction",
        "change .coupon_code": "changeCoupon"
    },


    initialize: function() {
        this.model.fetch();
        this._isDev && console.log("order initialized");
    },
    
    prev: function(){
        // this.saveOrder();
        window.location = '/sell/first';
    },
    
    next: function(){
        if (this.saveOrder()){      
            window.location = '/sell/third';
        }
    },
    
    saveOrder: function(){
        this._isDev && console.log("save order.");      
        
        var order = {
            seller: {
                name: $('#seller_name').val(),
                phone: $('#seller_phone').val(),
                location: {
                    'county': $('select[name="seller_county"]').val(),
                    'district': $('select[name="seller_district"]').val(),
                    'zipcode': $('input[name="seller_zipcode"]').val(),
                    'address': $('input[name="seller_address"]').val()
                }
            },
            receiver: {
                name: $('#receiver_name').val(),
                phone: $('#receiver_phone').val(),
                location: {
                    'county': $('select[name="receiver_county"]').val(),
                    'district': $('select[name="receiver_district"]').val(),
                    'zipcode': $('input[name="receiver_zipcode"]').val(),
                    'address': $('input[name="receiver_address"]').val()
                }
            },
            invoice: {
                'type': $('#sel_invoice_type').val(),
                'number': $('#uniform_invoice_number').val(),
                'title': $('#uniform_invoice_title').val(),
            },
            no_return: $('input[name="no_return"]:checked').val()
        };
        
        try{
            this.model.saveOrder( order );
            return true;
        }
        catch(e){
            alert(e);
        }
        
        
        this._isDev && console.log(JSON.stringify(order));

        /*
        try{
            this.collection.insertItem( item );
            this.clearFields();
        }
        catch(e){
            alert(e);
        }
        */
    },

    savePaymentMethod: function(){
        this._isDev && console.log("save payment method.");      
        
        var order = {
            other: {
                'payment_method': $('input[name="payment_method"]:checked').val(),
                'last_five_number': $('#last_five_code').val(),
                'estimated_remit_datetime': $('#datetimepicker').val()
            }
        };
        
        try{
            this.model.savePaymentMethod( order );
            return true;
        }
        catch(e){
            alert(e);
            return false;
        }
    },


    changeInvoiceType: function(e){
        this._isDev && console.log('change invoice type.');
        $this = $(e.currentTarget);
        $('#uniform_invoice_number').hide();
        $('#uniform_invoice_title').hide();
        if($this.val()==1){
            $('#uniform_invoice_number').show();
        }else if($this.val()==2){
            $('#uniform_invoice_number').show();
            $('#uniform_invoice_title').show();
        }
    },

    changeReturnType: function(e){
        this._isDev && console.log('change return type: ' + $('input[name="no_return"]:checked').val());
        var returnType = $('input[name="no_return"]:checked').val();
        if (returnType == '0'){
            $('#same_as, #receiver_name, #receiver_phone, ' + 
                'input[name="receiver_address"]'
            ).prop('disabled', false);          
            
            $('select[name="receiver_county"], select[name="receiver_district"]').attr('disabled', false);            
        }
        else if (returnType == '1'){
            $('#same_as, #receiver_name, #receiver_phone, ' + 
                'input[name="receiver_address"]'
            ).prop('disabled', true);          
            
            $('select[name="receiver_county"], select[name="receiver_district"]').attr('disabled', 'disabled');            
        }
    },
    
    changeSameAs: function(e){
        this._isDev && console.log('change same as checkbox.');
        
        if($(e.currentTarget).prop( "checked" )){
            $('#receiver_name').val($('#seller_name').val());
            $('#receiver_phone').val($('#seller_phone').val())

            var zipcode=$('input[name="seller_zipcode"]', '#seller_location').val();
            var address=$('input[name="seller_address"]', '#seller_location').val();

            $('[name="receiver_zipcode"]', '#receiver_location').val(zipcode).blur();
            $("input[name=receiver_address]", '#receiver_location').val(address);

        }
    },
    
    changePaymentMethod: function(e){
        if ($(e.currentTarget).val()=='atm'){
            $('#atm_fields').show();
        }
        else{
            $('#atm_fields').hide();
        }      
    },
    
    setDeduction: function(deduction_amount){
        try{
            // improve the User Experience. Helping users.
            if (deduction_amount > (this.account.getInitTotalPrice() - this.account.getCouponAmount())){
                deduction_amount = this.account.getInitTotalPrice() - this.account.getCouponAmount();
            }
            this.account.setDeduction(deduction_amount);

            // Update the html
            $('#account_balance').html(this.account.getBalance());
            $('#total_fee').html(this.account.getTotalPrice());
            $('.deduction_amount').val(deduction_amount);
            
            // Change the payment method if needed.
            if (this.account.getTotalPrice()==0){
                $('.payment_method').prop('checked', false);                
                $("#deduction").prop('checked', true);
                $(".is-deduction").show();
                $('.is-not-deduction').hide();
            }
            else{
                $('.payment_method').prop('checked', false);                
                $(".is-deduction").hide();
                $('.is-not-deduction').show();              
            }
            
            // Sotre into the Backbone model.
            var order = {
                deduction_amount: deduction_amount
            };
            this.model.save( order );
            this._isDev && console.log(this.model.attributes);
        }
        catch(e){
            alert(e);
        }
        this._isDev && console.log(this.account.getBalance());      
    },
    
    changeDeduction: function(e){
        var deduction_amount = $('.deduction_amount').val();
        this.setDeduction(deduction_amount);
    },

    changeCoupon: function(e){
        var self = this;
        var coupon_code = $('.coupon_code').val();
        var items_price = this.account.getInitTotalPrice();
        if (!coupon_code){
            var order = {
                coupon_code: ''
            };
            self.model.save( order );
            self.account.setCouponAmount(0);

            // Update the html
            $('#total_fee').html(self.account.getTotalPrice());          
            return false;
        }
        var self = this;
        $.ajax({
            type: 'POST',
            url: "/sell/coupon_code",
            data: { coupon_code: coupon_code, items_price: items_price },
            dataType: 'json',
            success: function(res){
                if (res.status == 'error'){
                    alert(res.message);
                    $('.coupon_code').val('');
                    var order = {
                        coupon_code: ''
                    };
                    self.model.save( order );
                    self.account.setCouponAmount(0);

                    // Update the html
                    $('#total_fee').html(self.account.getTotalPrice());
                }
                else if (res.status == 'success'){
                    // Adjust the deduction amount if needed.
                    var remainPrice = self.account.getInitTotalPrice() - res.coupon.amount;
                    if (self.account.getDeduction() > remainPrice){
                        self.account.setDeduction(remainPrice);
                        $('.deduction_amount').val(remainPrice);
                        var order = {
                            deduction_amount: remainPrice
                        };
                        self.model.save( order );
                    }

                    var order = {
                        coupon_code: $('.coupon_code').val()
                    };
                    self.model.save( order );
                    self.account.setCouponAmount(res.coupon.amount);

                    // Update the html
                    $('#account_balance').html(self.account.getBalance());
                    $('#total_fee').html(self.account.getTotalPrice());
                        
                    // Change the payment method if needed.
                    if (self.account.getTotalPrice()==0){
                        $('.payment_method').prop('checked', false);                
                        $("#deduction").prop('checked', true);
                        $(".is-deduction").show();
                        $('.is-not-deduction').hide();
                    }
                    else{
                        $('.payment_method').prop('checked', false);                
                        $(".is-deduction").hide();
                        $('.is-not-deduction').show();              
                    }
                                            
                    alert('禮券折抵' + res.coupon.amount + '元');
                }
            },
            error: function(res){
                alert("failed, please try again");
            },
            complete: function(res){
                // console.log(res);
                // console.log(res.responseJSON);
            }
        });
    },
    
    secondStep: function(){
        this._isDev && console.log("second step.");      
        
        var order = this.model.attributes;
        
        $('#seller_name').val(order.seller.name);
        $('#seller_phone').val(order.seller.phone)

        $('#receiver_name').val(order.receiver.name);
        $('#receiver_phone').val(order.receiver.phone)

        // initialize the zipcode plugin
        $('#seller_location').twzipcode();
        $('[name="seller_zipcode"]', '#seller_location').val(order.seller.location.zipcode).blur();
        $("input[name=seller_address]", '#seller_location').val(order.seller.location.address);        

        // initialize the zipcode plugin
        $('#receiver_location').twzipcode();
        $('[name="receiver_zipcode"]', '#receiver_location').val(order.receiver.location.zipcode).blur();
        $("input[name=receiver_address]", '#receiver_location').val(order.receiver.location.address);        

        // initialize the uniform invoice
        $('#sel_invoice_type').val(order.invoice.type),
        $('#uniform_invoice_number').val(order.invoice.number),
        $('#uniform_invoice_title').val(order.invoice.title),

        $('input[name="no_return"][value="' + order.other.no_return + '"]').prop('checked', true);
        
        $('#sel_invoice_type').trigger('change');
        $('input[name="no_return"]').trigger('change');
    },

    thirdStep: function(){
        this._isDev && console.log("third step.");      
        
        this.account = new Account($('#account_balance').html());
        this.account.setTotalPrice(parseInt($('#total_fee').html()));

        var order = this.model.attributes;

        this.model.save( {
            coupon_code: '',
            deduction_amount: 0
        } );
        
        $('#seller_name').html(order.seller.name);
        $('#seller_phone').html(order.seller.phone)

        var seller_location = order.seller.location;
        $('#seller_location').html(seller_location.zipcode + ' ' + 
            seller_location.county + seller_location.district + ' ' + 
            seller_location.address
        );
        
        $('#receiver_name').html(order.receiver.name);
        $('#receiver_phone').html(order.receiver.phone)

        var receiver_location = order.receiver.location;
        $('#receiver_location').html(receiver_location.zipcode + ' ' + 
            receiver_location.county + receiver_location.district + ' ' + 
            receiver_location.address
        );

        

        $('.is_not_donative').hide();
        $('.is_donative').hide();          
        
        if (order.no_return == '1'){
            $('.is_donative').show();
        }
        else if (order.no_return == '0'){
            $('.is_not_donative').show();          
        }



        $('#uniform_invoice_number').html(order.invoice.number);
        $('#uniform_invoice_title').html(order.invoice.title);

        $('.invoice').hide();
        
        if (order.invoice.type == '0'){
            $('.invoice_0').show();
        }
        else if (order.invoice.type == '1'){
            $('.invoice_1').show();
        }
        else if (order.invoice.type == '2'){
            $('.invoice_2').show();
        }

        
        /*
        $('#receiver_name').val(order.receiver.name);
        $('#receiver_phone').val(order.receiver.phone)

        var seller_location = order.seller.location;
        $('#seller_location').html(seller_location.zipcode + ' ' + 
            seller_location.county + seller_location.district + ' ' + 
            seller_location.address
        );
        */

        // initialize the uniform invoice
        $('#sel_invoice_type').val(order.invoice.type),
        $('#uniform_invoice_number').val(order.invoice.number),
        $('#uniform_invoice_title').val(order.invoice.title),

        $('input[name="no_return"][value="' + order.no_return + '"]').prop('checked', true);
        
        $('#sel_invoice_type').trigger('change');

        $('#datetimepicker').datetimepicker();
        
        $('#atm_fields').hide();
        $('.is-deduction').hide();
    }

});
