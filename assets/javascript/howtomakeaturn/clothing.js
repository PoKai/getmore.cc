/*
 * Common functions that clothing pages need
 * 
 * @author howtomakeaturn
 */

var LikeButtonModule;

LikeButtonModule = (function(){

    if (!window.jQuery) { throw new Error("LikeButtonModule requires jQuery") }

    // this line declares the module dependency and
    // gives the global variable a local reference.
    var $ = window.jQuery;
    
    var _is_logined = false;
  
    var _setupToLikeButton = function(){
        $('.to_like').hide();
        $('.fresh-product-view').hover(
            function(){ // hover in
                $(this).find('.to_like').show();
            },
            function(){ // hover out
                $(this).find('.to_like').hide();
            }
        ); // end hover function
        
        $('.to_like').hover(
            function(){ // hover in
                $(this).find('img').attr('src', '/assets/likeicon/unlike.png');
            },
            function(){ // hover out
                $(this).find('img').attr('src', '/assets/likeicon/to_like.png');
            }
        ); // end hover function          

        $('.to_like').click(function(e){

            if (!_is_logined){
                $('#logInModal').modal('show');
                return false;
            }
            
            e.preventDefault();          
            $(this).find('img').attr('src', '/assets/likeicon/liked.png');
            $(this).removeClass('to_like');
            $(this).addClass('liked');
            var clothing_id = $(this).data('id');
            $.post('/clothing/toggle_like_clothing', { clothing_id: clothing_id });
            _refreshLikeButtonEvent();
        });
    }// end _setupToLikeButton

    var _setupLikedButton = function(){
        $('.liked').hover(
            function(){ // hover in
                $(this).find('img').attr('src', '/assets/likeicon/unlike.png');
            },
            function(){ // hover out
                $(this).find('img').attr('src', '/assets/likeicon/liked.png');
            }
        ); // end hover function      
        
        $('.liked').click(function(e){

            if (!_is_logined){
                $('#logInModal').modal('show');
                return false;
            }

            e.preventDefault();
            $(this).find('img').attr('src', '/assets/likeicon/to_like.png');
            $(this).removeClass('liked');
            $(this).addClass('to_like');
            var clothing_id = $(this).data('id');
            $.post('/clothing/toggle_like_clothing', { clothing_id: clothing_id });
            _refreshLikeButtonEvent();
        });
    }// end _setupLikedButton

    var _refreshLikeButtonEvent = function(){
        // remove all the events and register them again
        $('.fresh-product-view').unbind();        
        $('.to_like').unbind();        
        $('.liked').unbind();        
        _setupToLikeButton();
        _setupLikedButton();
    }// end _refreshLikeButtonEvent

    // this is for the link in clothing view page
    var _setupToLikeLink = function(){
        $('.to_like').click(function(e){
            e.preventDefault();          
            
            if (!_is_logined){
                $('#logInModal').modal('show');
                return false;
            }            
            
            $(this).html('已加入欲望清單！');
            $(this).removeClass('to_like');
            $(this).addClass('liked');
            var clothing_id = $(this).data('id');
            $.post('/clothing/toggle_like_clothing', { clothing_id: clothing_id });
            _refreshLikeLink();
            
        });
    }// end _setupToLikeButton

    var _setupLikedLink = function(){
        $('.liked').hover(
            function(){ // hover in
                $(this).html('從欲望清單移除');
            },
            function(){ // hover out
                $(this).html('已加入欲望清單！');
            }
        ); // end hover function            
      
        $('.liked').click(function(e){
            e.preventDefault();          
                        
            $(this).html('加入我的欲望清單！');
            $(this).removeClass('liked');
            $(this).addClass('to_like');
            var clothing_id = $(this).data('id');
            $.post('/clothing/toggle_like_clothing', { clothing_id: clothing_id });
            _refreshLikeLink();
            
        });
    }// end _setupToLikeButton

    var _refreshLikeLink = function(){
        // remove all the events and register them again
        $('.to_like').unbind();        
        $('.liked').unbind();        
        _setupToLikeLink();
        _setupLikedLink();
    }

  
    // the public API interface
    return {
        initialize: function(){
            // initialization
            _setupToLikeButton();
            _setupLikedButton();

        },
        setIsLogined: function(result){
            _is_logined = result;
        },
        setupLink: function(){
            _setupToLikeLink();
            _setupLikedLink();
        }
    };
  
}());


