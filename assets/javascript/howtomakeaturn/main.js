/*
 * Backbone application used for shopping.
 * They are shopping cart and order module.
 * 
 * @author howtomakeaturn
 */

$(function(){

    var Item = Backbone.Model.extend({
    });
    
    var ItemList = Backbone.Collection.extend({
        localStorage: new Backbone.LocalStorage("ItemCollection"),
        // Reference to this collection's model.
        model: Item,
    });

    var Cart = Backbone.View.extend({

        el: "body",
        free_shipping: false,
        
        // Delegated events for creating new items, and clearing completed ones.
        events: {
            "click #product-view-buy": "addToCart",
            "click #clear_cart": "clearCart",
            "click .delete_item": "deleteItem",
            "click .checkout-button": 'checkoutButton',
            "click .buy-next-time": "buyNextTime",
            "click .deduct_bonus": "deductBonus",
            "click button.deduction_amount": "deductionAmount",
            "click button.coupon-btn": "getCoupon",
            "change select.number": "changeNumber"
        },

        initialize: function() {
            console.log('cart initialized');
            var self = this;

            this.free_shipping = getmore.free_shipping;
            this.bonus_points = 0;
            this.account_balance = 0;
            this.coupon = {};
            
            this.collection.fetch().done(function(){
              self.refresh_dropdown_list();

              if(/checkout\/first/.test(location.pathname)){
                self.check_stock();
              }
            });
            
            this.collection.bind('change', this.refresh_dropdown_list, this);
            this.collection.bind('change', this.check_stock, this);
            this.collection.bind('remove', this.refresh_dropdown_list, this);
            this.collection.bind('remove', this.refreshCheckoutCart, this);
            // this.listenTo(this.model, 'change', this.render);
        },
        changeNumber: function(e){
            var clothing = this.collection.findWhere($(e.target).data());
            clothing.set('number', parseInt($(e.target).val(), 10));
        },
        check_stock: function(e){
            var product_clothings = this.collection.where({launch_way: 2}),
                  marketplace_clothings = this.collection.where({launch_way: 1}),
                  designer_clothings = this.collection.where({launch_way: 3}),
                  buyer_clothings = this.collection.where({launch_way: 4}),
                  product_clothing_deferrd = $.Deferred(),
                  marketplace_deferrd = $.Deferred(),
                  designer_deferrd = $.Deferred(),
                  buyer_deferrd = $.Deferred();

            $.when(product_clothing_deferrd, marketplace_deferrd, designer_deferrd, buyer_deferrd).then(function(){
                this.refreshCheckoutCart();
            }.bind(this));

            if(!_.isEmpty(product_clothings)){
                var product_clothings_query = _.reduce(product_clothings, function(result, product_clothing){
                        result['product_id'].push(product_clothing.get('product_id'));
                        result['color'].push(product_clothing.get('color'));
                        result['size'].push(product_clothing.get('size'));
                        return result;
                      }, {product_id: [], color: [], size: [], type: 2});

                $.ajax({
                    url: '/restful/stock',
                    method: 'get',
                    data: $.param(product_clothings_query)
                }).done(function(data, status, xhr){
                    if(_.isEmpty(data)) {
                        return product_clothing_deferrd.resolve();
                    }
                    _.each(product_clothings, function(product_clothing){
                        var stock = _.findWhere(data, _.pick(product_clothing.toJSON(), 'product_id', 'color', 'size'));
                        if(_.isEmpty(stock)){
                            return false;
                        }
                        product_clothing.set('Stock', stock);
                        product_clothing.set('stock_id', stock.id);
                    });
                    product_clothing_deferrd.resolve();
                }.bind(this));
            } else {
                product_clothing_deferrd.resolve();
            }
            
            if(!_.isEmpty(marketplace_clothings)){
                var marketplace_query = _.reduce(marketplace_clothings, function(result, marketplace_clothing){
                        result['product_id'].push(marketplace_clothing.get('product_id'));
                        result['color'].push(marketplace_clothing.get('color'));
                        result['size'].push(marketplace_clothing.get('size'));
                        return result;
                      }, {product_id: [], color: [], size: [], type: 1});

                $.ajax({
                    url: '/restful/stock',
                    method: 'get',
                    data: $.param(marketplace_query)
                }).done(function(data, status, xhr){
                    if(_.isEmpty(data)) {
                        return marketplace_deferrd.resolve();
                    }
                    _.each(marketplace_clothings, function(marketplace){
                        var stock = _.findWhere(data, _.pick(marketplace.toJSON(), 'product_id', 'color', 'size'));
                        marketplace.set('Stock', stock);
                        marketplace.set('stock_id', stock.id);
                    });
                    
                    marketplace_deferrd.resolve();
                }.bind(this));
            } else {
                marketplace_deferrd.resolve();
            }
            
            if(!_.isEmpty(designer_clothings)){
                var designer_query = _.reduce(designer_clothings, function(result, designer_clothing){
                        result['product_id'].push(designer_clothing.get('product_id'));
                        result['color'].push(designer_clothing.get('color'));
                        result['size'].push(designer_clothing.get('size'));
                        return result;
                      }, {product_id: [], color: [], size: [], type: 3});
                
                $.ajax({
                    url: '/restful/stock',
                    method: 'get',
                    data: $.param(designer_query)
                }).done(function(data, status, xhr){

                    if(_.isEmpty(data)) {
                        return designer_deferrd.resolve();
                    }
                    _.each(designer_clothings, function(designer){
                        var stock = _.findWhere(data, _.pick(designer.toJSON(), 'product_id', 'color', 'size'));
                        designer.set('Stock', stock);
                        designer.set('stock_id', stock.id);
                    });
                    
                    designer_deferrd.resolve();
                }.bind(this));
            } else {
                designer_deferrd.resolve();
            }

            if(!_.isEmpty(buyer_clothings)){
                var buyer_query = _.reduce(buyer_clothings, function(result, buyer_clothing){
                        result['product_id'].push(buyer_clothing.get('product_id'));
                        result['color'].push(buyer_clothing.get('color'));
                        result['size'].push(buyer_clothing.get('size'));
                        return result;
                      }, {product_id: [], color: [], size: [], type: 4});
                
                $.ajax({
                    url: '/restful/stock',
                    method: 'get',
                    data: $.param(buyer_query)
                }).done(function(data, status, xhr){

                    if(_.isEmpty(data)) {
                        return buyer_deferrd.resolve();
                    }
                    _.each(buyer_clothings, function(buyer){
                        var stock = _.findWhere(data, _.pick(buyer.toJSON(), 'product_id', 'color', 'size'));
                        buyer.set('Stock', stock);
                        buyer.set('stock_id', stock.id);
                    });
                    
                    buyer_deferrd.resolve();
                }.bind(this));
            } else {
                buyer_deferrd.resolve();
            }
        },

        getCoupon: function(e){
            var code = $('input.coupon_code').val(),
                  self = this;

            $.get('/restful/coupon/'+code, {totalPrice: this.itemsPrice+this.shippingPrice})
                .success(function(data){
                    self.coupon = data;
                    self.refreshCheckoutCart();
                })
                .fail(function(data){
                    switch(data.status){
                        case 410:
                            alert('此優惠卷已過期');
                            break;
                        case 411:
                            alert('此優惠卷已過完');
                            break;
                        case 404:
                            alert('沒有此優惠卷');
                            break;
                        case 412:
                            alert('此優惠卷需要消費滿$'+data.responseJSON+'元才能使用');
                            break;
                    }
                });
        },

        deductionAmount: function(e){
            var account_balance = parseInt($('input.deduction_amount').val() || 0, 10);

            if(account_balance > this.totalPrice){
                account_balance = parseInt(this.totalPrice, 10);
                
                if(account_balance > $('span.account_balance').data('value')){
                    account_balance = parseInt($('span.account_balance').data('value'), 10);
                }
            } 

            this.account_balance = account_balance;

            this.refreshCheckoutCart();
        },

        deductBonus: function(e){
            if(this.bonus_points){
                bonus_amount = 0;
            }
            else {
                var bonus_amount = parseInt($('span.bonus_points').data('value') / 100, 10);

                if(this.totalPrice < bonus_amount) {
                    bonus_amount = this.totalPrice;
                }
            }
            
            this.bonus_points = bonus_amount*100;

            this.refreshCheckoutCart();
        },

        checkoutButton: function(e){
            console.log(e);
            $('#checkout-modal').modal('show');          
            return false;
        },

        addToCart: function(e) {
            var clothing = JSON.parse($("#clothing_json_data").html()),
                  select_number = parseInt($('.select_number').val(), 10) || 1,
                  select_color = $('.dsner-color').val() || clothing.color,
                  product_id = _.isEmpty(clothing.Stocks)?clothing.product_id:clothing.Stocks[0].product_id,
                  select_size = $('input[type="radio"][name="size"]:checked').val() || clothing.size;
            
            clothing = _.chain(clothing).omit('Stocks').extend({number: select_number, color: select_color, size: select_size, product_id: product_id}).value();
            /* i think this is the same as create(), but it failed. i dont know why */
            // this.todos.add(clothing);
            // this.todos.sync();
            clothing.id = clothing.id+'-'+clothing.launch_way;
            this.collection.create(clothing);
            this.refresh_dropdown_list();// i bind that to 'change' event already! but it failed i didn't assign null to model id. dont know why!
            
            e.stopImmediatePropagation();
            $("#shopping-cart-menu").dropdown('toggle');
            $('#product-view-buy').html('已加入購物車');
            $('#product-view-buy').prop('disabled', true);
            $('#cart_badge_Headerbadge').html(this.collection.length);
            
            /* old codes, old effect
            alert('成功加入購物車！');
            $('#product-view-buy').html('已加入購物車');
            $('#product-view-buy').prop('disabled', true);
            console.log('addToCart. now ' + this.collection.length + " models in collection");
            */
        },
        
        clearCart: function() {
            console.log('claer cart');
            // this.collection.reset();

            var model;
            while (model = this.collection.first()) {
              model.destroy();
              // this.collection.remove(model);
            }
        },
        
        deleteItem: function(e) {
            e.stopImmediatePropagation();            
            console.log('delete');
            var id = $(e.currentTarget).data("id");
            console.log(id);
            var item = this.collection.get(id);
            item.destroy();
            $('#cart_badge_Headerbadge').html(this.collection.length == 0 ? '' : this.collection.length);
        },
        
        buyNextTime: function(e){
            e.preventDefault();
            e.stopImmediatePropagation();            
                        
            var id = $(e.currentTarget).data("id"),
                   clothing_id = id.match(/^\d+/)[0],
                   type = $(e.currentTarget).data("type");

            $.ajax({
                url: '/restful/clothing_like/'+clothing_id,
                type: 'PUT',
                data: {
                    type: type
                }
            }).done(function(data, textStatus, jqXHR){
                if(jqXHR.status == 250){
                    return false;
                }
                var item = this.collection.get(id);
                item.destroy();
                
                alert('已加入欲望清單！');
            }.bind(this));
        },
                
        test: function() {
            console.log('test ' + this.collection.length + " models in collection");          
   //         $("#product-view-buy").html(this.collection.length + " models in collection");
        },
                
        refresh_dropdown_list: function() {
            
            $("#shopping-cart-menu").empty();

            if (this.collection.length==0){
                $('#cart_badge_Headerbadge').html('');
                $('.cart-count').html('');
              
                var dropdownHeader = ''+
                                '<li>'+
                                  '<span>SHOPPING CART</span>'+
                                '</li>'+
                                '<li role="presentation" class="divider"></li>' ;
                $("#shopping-cart-menu").append(dropdownHeader);
                var dropdownFooter = '' + 
                    '<li><img src="/assets/images/cartempty.png" /></li>';
                
                $("#shopping-cart-menu").append(dropdownFooter);
                $("#shopping-cart-menu").css('max-height', '400px');
                $("#shopping-cart-menu").css('min-width', '280px');
                $("#shopping-cart-menu").css('overflow', 'auto');                            
                
            }// end if
            else {
                $('#cart_badge_Headerbadge').html(this.collection.length);
                $('.cart-count').html( '（' + this.collection.length + '）' );

                var newButton = $('<button/>',
                {
                    text: 'CLEAR',
                    id: 'clear_cart'
                });
                  
                var dropdownHeader = ''+
                                '<li>'+
                                  '<span>SHOPPING CART</span>'+
                                  '<span class="pull-right checkout-button"><a class="poaki-checkout-link" href="/checkout/first"><img src="/assets/images/checkout/paybillbtn.png"></a></span>'+
                                '</li>'+
                                '<li role="presentation" class="divider"></li>' ;
                
                $("#shopping-cart-menu").append(dropdownHeader);

                var itemsPrice = total_stock = 0; // total sell price in the sell cart

                this.collection.each(function(model) {
                    var model_attrs = model.attributes;
                    var newLi = $('<li/>')
                    var mainDiv = $('<div class="cart-block">');                    
                    var img = $('<img>'); //Equivalent: $(document.createElement('img'))
                    img.attr('src', 'http://s3.getmore.cc/clothing/'+model_attrs.files[0]+'/medium.jpg');
                    img.css('width', '50px');
                    var newDiv = $('<div class="cart-img">');
                    img.appendTo(newDiv);
                    newDiv.appendTo(mainDiv);
                    mainDiv.append("<div class='cart-title'>" + model_attrs.product_title + "</div>");
                    mainDiv.append("<div class='cart-price'>$" + model_attrs.product_sell_price + "</div>");
                    
                    var newButton = $('<button type="button" class="close">&times;</button>');
                    newButton.attr("data-id", model.id);
                    newButton.addClass('delete_item');
                    var newDiv2 = $("<div class='cart-close'>")
                    newDiv2.append(newButton);
                    mainDiv.append(newDiv2);
                    mainDiv.append('<div style="clear: both;"></div>');      
                    newLi.append(mainDiv);
                     
                    $("#shopping-cart-menu").append(newLi);                              
                       
                    total_stock += model.get('number');
                    itemsPrice += parseInt(model_attrs.product_sell_price) * model.get('number');
                });//end each
                
                var dropdownFooter = '' + 
                    '<li role="presentation" class="divider"></li>' +
                    '<li class="pull-right">共<span class="cart-number">' + total_stock + '</span>件 單品</li>' +
                    "<li style='clear: both;'></li>" +
                    '<li class="pull-right">總價 $<span class="cart-number">' + itemsPrice  + '</span></li>';
                
                $("#shopping-cart-menu").append(dropdownFooter);
                
                $("#shopping-cart-menu").css('max-height', '400px');
                $("#shopping-cart-menu").css('min-width', '280px');
                $("#shopping-cart-menu").css('overflow', 'auto');

                $("#shopping-cart-menu").delegate('.checkout-button', 'click', this.checkoutButton);
                $("#shopping-cart-menu").delegate('.delete_item', 'click', this.deleteItem.bind(this));
            }// end else
        },
        refreshCheckoutCart: function() {
            if(!/checkout\/first/.test(location.pathname)){
                return false;
            }
            $(".pagecontent").empty();
            var  item_numbers = [],
                   itemsPrice = 0,
                   virginCheckoutPrice = 250,
                   closets = _.reduce(this.collection.models, function(result, item){
                       var number = item.get('Stock') && item.get('Stock').number <= item.get('number')?item.get('Stock').number:item.get('number');

                       if(!_.isNull(soldout_clothings) && _.findWhere(soldout_clothings, {product_item_number: item.get('product_item_number')})){
                            item.set('lock_status', 2);
                            console.log(item);
                       }

                       switch(item.get('launch_way')){
                            case 1: 
                                var other = _.findWhere(result.others, {'closet_id': item.get('closet_id')}),
                                      total_price = parseInt(item.get('product_sell_price'), 10) * number
                                
                                if(_.isEmpty(other)){
                                    result.others.push({
                                        'closet_id': item.get('closet_id'),
                                        'closet_title': item.get('closet_title'),
                                        'closet_url_code': item.get('closet_url_code'),
                                        'items': [item],
                                        total_price: total_price,
                                        trade_rule: item.get('trade_rule'),
                                        count: number
                                    });
                                }
                                else {
                                    other.items.push(item);
                                    other.total_price += total_price;
                                    other.count += number;
                                }
                            break;
                            case 2:
                                var total_price = parseInt(item.get('product_sell_price'), 10) * number;

                                if(_.isEmpty(result.getmore)){
                                    result.getmore = {
                                        title: 'getmore',
                                        items: [item],
                                        total_price: total_price,
                                        trade_rule: 'GetMore上的每一件衣服都是獨一無二的，若您收到的衣服尺寸不合或不如預期，很抱歉我們也無法提供相同款式的衣服供您更換。',
                                        count: number,
                                        shipping_price: (total_price < 1000)?80:0
                                    }
                                } else {
                                    result.getmore.items.push(item);
                                    result.getmore.total_price += total_price;
                                    result.getmore.count += number;

                                    if(result.getmore.total_price < 1000){
                                        result.getmore.shipping_price = 80;
                                    } else {
                                        result.getmore.shipping_price = 0;
                                    }
                                }
                            break;
                            case 3:
                                var designer = _.findWhere(result.designers, {'user_id': item.get('user_id')}),
                                       total_price = parseInt(item.get('product_sell_price'), 10) * number;
                                
                                if(_.isEmpty(designer)){
                                    result.designers.push({
                                        url_code: item.get('Designer').url_code,
                                        title: item.get('Designer').title,
                                        user_id: item.get('user_id'),
                                        items: [item],
                                        total_price: total_price,
                                        trade_rule: item.get('trade_rule'),
                                        count: number,
                                        shipping_price: (total_price < item.get('Designer').free_shipping)?item.get('Designer').shipping_price: 0
                                    });
                                }
                                else {
                                    designer.items.push(item);
                                    designer.total_price += total_price;
                                    designer.count += number;

                                    if(designer.total_price < item.get('Designer').free_shipping){
                                        designer.shipping_price = item.get('Designer').shipping_price
                                    } else {
                                        designer.shipping_price = 0;
                                    }
                                }
                            break;
                            case 4: 
                                var buyer = _.findWhere(result.buyers, {'user_id': item.get('user_id')}),
                                       total_price = parseInt(item.get('product_sell_price'), 10) * number;
                                
                                if(_.isEmpty(buyer)){
                                    result.buyers.push({
                                        url_code: item.get('Buyer').url_code,
                                        title: item.get('Buyer').title,
                                        user_id: item.get('user_id'),
                                        items: [item],
                                        total_price: total_price,
                                        trade_rule: item.get('trade_rule'),
                                        count: number,
                                        shipping_price: (total_price < item.get('Buyer').free_shipping)?item.get('Buyer').shipping_price: 0
                                    });
                                }
                                else {
                                    buyer.items.push(item);
                                    buyer.total_price += total_price;
                                    buyer.count += number;

                                    if(buyer.total_price < item.get('Buyer').free_shipping){
                                        buyer.shipping_price = item.get('Buyer').shipping_price
                                    } else {
                                        buyer.shipping_price = 0;
                                    }
                                }
                            break;
                       }
                       itemsPrice += total_price;
                       item_numbers.push(item.get('product_item_number'));
                       return result;
                   }.bind(this), {getmore: {}, others: [], designers: [], buyers: []});
            
            var getmore = closets.getmore,
                  designers = closets.designers,
                  buyers = closets.buyers,
                  others = closets.others,
                  total_price = 0,
                  shippingPrice = (_.isEmpty(getmore)?0:getmore.shipping_price) + _.chain(designers).pluck('shipping_price').reduce(function(result, value){
                    return result + value;
                  }, 0).value() + _.chain(buyers).pluck('shipping_price').reduce(function(result, value){
                    return result + value;
                  }, 0).value(),
                  coupon_amount = 0;
                  
                  (function(){
                    var marketplace_total_price = _.chain(others).pluck('total_price').reduce(function(result, value){
                            return result + value;
                          }, 0).value(),
                          designers_total_price = _.chain(designers).pluck('total_price').reduce(function(result, value){
                            return result + value;
                          }, 0).value(),
                          designers_shipping_price = _.chain(designers).pluck('shipping_price').reduce(function(result, value){
                            return result + value;
                          }, 0).value(),
                          buyers_total_price = _.chain(buyers).pluck('total_price').reduce(function(result, value){
                            return result + value;
                          }, 0).value(),
                          buyers_shipping_price = _.chain(buyers).pluck('shipping_price').reduce(function(result, value){
                            return result + value;
                          }, 0).value(),
                          getmore_total_price = getmore.total_price,
                          getmore_shipping_price = getmore.shipping_price;

                    if(this.coupon.union){
                        if(/marketplace/.test(this.coupon.limit) && !_.isEmpty(others) || /product_clothing/.test(this.coupon.limit) && !_.isEmpty(getmore) || /designer/.test(this.coupon.limit) && !_.isEmpty(designers) || /buyer/.test(this.coupon.limit) && !_.isEmpty(buyers)){
                            if(this.coupon.discount){
                                coupon_amount = parseInt((itemsPrice + shippingPrice) * (1 - this.coupon.discount/100));
                            } else {
                                coupon_amount = this.coupon.amount;
                            }
                        }
                    } else {
                        if(/marketplace/.test(this.coupon.limit) && !_.isEmpty(others)){
                            if(this.coupon.discount){
                                coupon_amount = parseInt(marketplace_total_price * (1 - this.coupon.discount/100));
                            } else {
                                coupon_amount = this.coupon.amount;
                            }
                        }

                        if(/designer/.test(this.coupon.limit) && !_.isEmpty(designers)){
                            if(this.coupon.discount){
                                coupon_amount = parseInt((designers_total_price + designers_shipping_price) * (1 - this.coupon.discount/100));
                            } else {
                                coupon_amount = this.coupon.amount;
                            }
                        }

                        if(/buyer/.test(this.coupon.limit) && !_.isEmpty(buyers)){
                            if(this.coupon.discount){
                                coupon_amount = parseInt((buyers_total_price + buyers_shipping_price) * (1 - this.coupon.discount/100));
                            } else {
                                coupon_amount = this.coupon.amount;
                            }
                        }

                        if(/product_clothing/.test(this.coupon.limit) && !_.isEmpty(getmore)){
                            if(this.coupon.discount){
                                coupon_amount = parseInt((getmore_total_price + getmore_shipping_price) * (1 - this.coupon.discount/100));
                            } else {
                                coupon_amount = this.coupon.amount;
                            }
                        }
                    }

                    if(!this.coupon.union && !this.coupon.limit){
                        if(this.coupon.discount){
                            coupon_amount = parseInt((itemsPrice + shippingPrice) * (1 - this.coupon.discount/100));
                        } else {
                            coupon_amount = this.coupon.amount;
                        }
                    }
                }.bind(this)());
            
            virginCheckoutPrice = Math.floor(virgin_checkout? ( itemsPrice * 0.05 >= virginCheckoutPrice ? virginCheckoutPrice : itemsPrice * 0.05 ) : 0);
            console.log(virginCheckoutPrice);
            this.totalPrice = itemsPrice + shippingPrice - (coupon_amount || 0) - (this.account_balance || 0) - (parseInt(this.bonus_points/100, 10) || 0) - virginCheckoutPrice;
            this.virginCheckoutPrice = virginCheckoutPrice;
            this.itemsPrice = itemsPrice;
            this.shippingPrice = shippingPrice;
            this.coupon.amount = coupon_amount;
            var template = _.template($('#artist-list-template').html(), {getmore: getmore, others: others, designers: designers, buyers: buyers, itemsPrice: itemsPrice, shippingPrice: shippingPrice, virginCheckoutPrice: this.virginCheckoutPrice, totalPrice: this.totalPrice, account_balance: this.account_balance, bonus_points: this.bonus_points, coupon: this.coupon, clothings: _.chain(this.collection.toJSON()).map(function(item){  return [item.product_item_number, _.pick(item, 'number', 'stock_id')]; }).object().value()});
            $(".pagecontent").html(template);
        }
    });

    var OrderModel = Backbone.Model.extend({
        localStorage: new Backbone.LocalStorage('OrderModel99'),

        defaults:{
            buyer: {
                name: '',
                gender: '1',
                contact_phone_number: '',
                location: {
                    county: '',
                    district: '',
                    zipcode: '200',
                    address: ''
                }
            },
            receiver: {
                name: '',
                gender: '1',
                contact_phone_number: '',
                location: {
                    county: '',
                    district: '',
                    zipcode: '200',
                    address: ''
                }
            },
            receipt_to: '',
            uniform_invoice_type: '',
            uniform_invoice_title: '',
            uniform_invoice_number: '',            
            delivery_time: 'any'
        }
    });

    var Order = Backbone.View.extend({

        el: "body",
        
        // Delegated events for creating new items, and clearing completed ones.
        events: {
            "click #fill_receiver_from_buyer": "fillReceiverFromBuyer",
            // "click #go_second_step": "goSecondStep",
            "click #go_third_step": "goThirdStep",
            "click #test": "test",
            "change input[name='receipt_to']": "refreshInvoiceFields",
            "change select[name='uniform_invoice_type']": "refreshInvoiceFields",
        },
        refreshInvoiceFields: function(){
            $('#uniform_invoice_type').hide();
            $('#uniform_invoice_title').hide();
            $('#uniform_invoice_number').hide();
            
            $('input[name="uniform_invoice_title"]').val('');
            $('input[name="uniform_invoice_number"]').val('');
            
            if ($('input[name="receipt_to"]:checked').val()=='3'){
              
            }
            else if ($('input[name="receipt_to"]:checked').val()=='1' || $('input[name="receipt_to"]:checked').val()=='2'){
                $('#uniform_invoice_type').show();
                if ($('select[name="uniform_invoice_type"]').val()=='two'){
                    $('#uniform_invoice_title').show();                                    
                }
                else if ($('select[name="uniform_invoice_type"]').val()=='three'){
                    $('#uniform_invoice_title').show();                  
                    $('#uniform_invoice_number').show();
                }
            }
        },



        test: function(){
            if($("#fill_receiver_from_buyer").prop('checked'))
                alert('aloha');
            // $('input[name="buyer_gender"][value="0"]').prop('checked',true);
        },

        initialize: function() {
            this.model.fetch();
            console.log("order initialized")
        },

        fillReceiverFromBuyer: function(){
            if($("#fill_receiver_from_buyer").prop('checked')){
                $("#receiver_name").val($("#buyer_name").val());
                $("#receiver_contact_phone_number").val($("#buyer_contact_phone_number").val());
                
                if ($('input[name="buyer_gender"]:checked').val()=='1'){
                    $('input[name="receiver_gender"][value="1"]').prop('checked',true);
                }
                else if ($('input[name="buyer_gender"]:checked').val()=='0'){
                    $('input[name="receiver_gender"][value="0"]').prop('checked',true);
                }

                // seems no method to set twzipcode values after it initialized. so i intialized it again.
                $('#twzipcode2').twzipcode({
                  countyName: 'receiver_county',
                  districtName: 'receiver_district',
                  zipcodeName: 'receiver_zipcode',
                  addressName: 'receiver_address',
                  zipcodeSel: $('input[name="buyer_zipcode"]').val(),
                  addressSel: $('input[name="buyer_address"]').val()
                });

                $("input[name=receiver_address]").addClass('long_address');
                
              /*
                var data = this.model.attributes;
                $("#receiver_name").val(data.buyer.name);
                $("#receiver_contact_phone_number").val(data.buyer.contact_phone_number);
                
                if (data.buyer.gender=='1'){
                    $('input[name="receiver_gender"][value="1"]').prop('checked',true);
                }
                else if (data.buyer.gender=='0'){
                    $('input[name="receiver_gender"][value="0"]').prop('checked',true);
                }

                // seems no method to set twzipcode values after it initialized. so i intialized it again.
                $('#twzipcode2').twzipcode({
                  countyName: 'receiver_county',
                  districtName: 'receiver_district',
                  zipcodeName: 'receiver_zipcode',
                  addressName: 'receiver_address',
                  zipcodeSel: data.buyer.location.zipcode,
                  addressSel: data.buyer.location.address
                  
                });
                
              */  
            }//end if
        },
        
        saveOrder: function(){
            var data = this.model.attributes;
            data.buyer.name = $("#buyer_name").val();
            data.buyer.contact_phone_number = $("#buyer_contact_phone_number").val();
            data.buyer.gender = $("input[type='radio'][name='buyer_gender']:checked").val();
            
            data.buyer.location.county = $("select[name='buyer_county']").val();
            data.buyer.location.district = $("select[name='buyer_district']").val();
            data.buyer.location.zipcode = $("input[name='buyer_zipcode']").val();            
            data.buyer.location.address = $("input[name='buyer_address']").val();
            
            data.receiver.name = $("#receiver_name").val();
            data.receiver.contact_phone_number = $("#receiver_contact_phone_number").val();
            data.receiver.gender = $("input[type='radio'][name='receiver_gender']:checked").val();
            
            data.receiver.location.county = $("select[name='receiver_county']").val();
            data.receiver.location.district = $("select[name='receiver_district']").val();
            data.receiver.location.zipcode = $("input[name='receiver_zipcode']").val();            
            data.receiver.location.address = $("input[name='receiver_address']").val();
            
            data.receipt_to = $("input[type='radio'][name='receipt_to']:checked").val();
            
            if ($("input[type='radio'][name='receipt_to']:checked").val()!='3'){
                data.uniform_invoice_type = $('select[name="uniform_invoice_type"]').val();
                data.uniform_invoice_title = $('input[name="uniform_invoice_title"]').val();
                data.uniform_invoice_number = $('input[name="uniform_invoice_number"]').val();
            }
            else{
                data.uniform_invoice_type = '';
                data.uniform_invoice_title = '';
                data.uniform_invoice_number = '';
            }

            data.delivery_time = $("select[name='delivery_time']").val();
            
            this.model.save(data);
            console.log('save order');
        },
        
        goSecondStep: function(){
            this.saveOrder();

            if (!$("#buyer_name").val() || !$("#buyer_contact_phone_number").val() || 
                 !$("input[name='buyer_address']").val() ||
                 !$("#receiver_name").val() || !$("#receiver_contact_phone_number").val() || 
                 !$("input[name='receiver_address']").val())
            {
                alert("請填妥資訊！");
                return false;
            }

            if ($("input[type='radio'][name='receipt_to']:checked").val()!='3'){
                if ($('select[name="uniform_invoice_type"]').val()=='two'){
                    if (!$('input[name="uniform_invoice_title"]').val()){
                        alert("請填妥發票資訊！");
                        return false;                      
                    }
                }
                else if ($('select[name="uniform_invoice_type"]').val()=='three'){
                    if (!$('input[name="uniform_invoice_title"]').val() || 
                         !$('input[name="uniform_invoice_number"]').val())
                     {
                        alert("請填妥發票資訊！");
                        return false;                      
                    }
                }                
            }

            // fetch ids in the cart
            // this is a little ugly, because i think this is kind of crossing fetch models(fetch from Order and Cart )

            var clothingIds = [];
            var items = new ItemList;
            var cart = new Cart({collection: items});
            cart.collection.each(function(model) {
                clothingIds.push(model.id);
            })
            
            var json_model_data = JSON.stringify(this.model.attributes);
            // send a HTTP POST with form data dynamically            
            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", "/checkout/second");

            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", "json_model_data");
            hiddenField.setAttribute("value", json_model_data);

            var hiddenField2 = document.createElement("input");
            hiddenField2.setAttribute("type", "hidden");
            hiddenField2.setAttribute("name", "json_clothing_ids");
            hiddenField2.setAttribute("value", JSON.stringify(clothingIds));

            form.appendChild(hiddenField);
            form.appendChild(hiddenField2);

            document.body.appendChild(form);
            form.submit();            
        },

        goThirdStep: function(){
            if ($("input[type='radio'][name='payment_method']:checked").val()=='pay-at-home'){
                if (!confirm('訂單即將送出！您選擇「貨到付款」方式，請記得保持手機暢通，宅配人員將與您聯繫！ ')){
                    return false;
                }              
            }
          
          
            var deduction_amount = $('.deduction_amount').val();
            var coupon_amount = $('.coupon_amount').val();
            var account_balance = $('.account_balance').html();
            var total_price = $('.cart_1_sum').html();
            var final_price = total_price - deduction_amount - coupon_amount;

            // quick and dirty fix for the negative condition
            if (parseInt(account_balance)<0){
                account_balance = 0;
            }

            if  (!(validateDeductionAmount( deduction_amount ) && 
                validateAccountBalance( account_balance, deduction_amount) &&
                validateFinalPrice( final_price )))
            {
                return false;
            }

                        
            // this function doesn't use any data from model, it just sends payment method
            // send a HTTP POST with form data dynamically            
            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", "/checkout/third");

            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", "payment_method");
            hiddenField.setAttribute("value", $("input[type='radio'][name='payment_method']:checked").val());

            var hiddenField2 = document.createElement("input");
            hiddenField2.setAttribute("type", "hidden");
            hiddenField2.setAttribute("name", "deduction_amount");
            hiddenField2.setAttribute("value", $(".deduction_amount").val());

            var hiddenField3 = document.createElement("input");
            hiddenField3.setAttribute("type", "hidden");
            hiddenField3.setAttribute("name", "coupon_code");
            hiddenField3.setAttribute("value", $(".coupon_code").val());

 
            form.appendChild(hiddenField);
            form.appendChild(hiddenField2);
            form.appendChild(hiddenField3);

            document.body.appendChild(form);
            form.submit();            
            $("#go_third_step").prop('disabled', true);            
        },
        
        showFirstOrder: function(data){
            // for buyer
            $("#buyer_name").val(data.buyer.name);
            $("#buyer_contact_phone_number").val(data.buyer.contact_phone_number);
            
            if (data.buyer.gender=='1'){
                $('input[name="buyer_gender"][value="1"]').prop('checked',true);
            }
            else if (data.buyer.gender=='0'){
                $('input[name="buyer_gender"][value="0"]').prop('checked',true);
            }
            //for receiver
            $("#receiver_name").val(data.receiver.name);
            $("#receiver_contact_phone_number").val(data.receiver.contact_phone_number);
            
            if (data.receiver.gender=='1'){
                $('input[name="receiver_gender"][value="1"]').prop('checked',true);
            }
            else if (data.receiver.gender=='0'){
                $('input[name="receiver_gender"][value="0"]').prop('checked',true);
            }
            
            // other information
            $('select[name="delivery_time"]').val(data.delivery_time);                
                
            /*
            if (data.receipt_to == '1')
                $('input[name="receipt_to"][value="1"]').prop('checked',true);                
            else if (data.receipt_to == '2')
                $('input[name="receipt_to"][value="2"]').prop('checked',true);                   
            else if (data.receipt_to == '3')
                $('input[name="receipt_to"][value="3"]').prop('checked',true);                        
            */
            this.refreshInvoiceFields();
        },
        
        showSecondOrder: function(data){
            // for buyer
            $("#buyer_name").append(data.buyer.name);     
            if (data.buyer.gender == '1'){
                $("#buyer_name").append(" " + "先生");
            }
            else if(data.buyer.gender == '0'){
                $("#buyer_name").append(" " + "小姐");              
            }
            var buyer_location = data.buyer.location;
            $("#buyer_full_location").append(buyer_location.zipcode + buyer_location.county + buyer_location.district + buyer_location.address);
            $("#buyer_contact_phone_number").append(data.buyer.contact_phone_number);

            // for receiver
            $("#receiver_name").append(data.receiver.name);     
            if (data.receiver.gender == '1'){
                $("#receiver_name").append(" " + "先生");
            }
            else if(data.receiver.gender == '0'){
                $("#receiver_name").append(" " + "小姐");              
            }            
            var receiver_location = data.receiver.location;            
            $("#receiver_full_location").append(receiver_location.zipcode + receiver_location.county + receiver_location.district + receiver_location.address);            
            $("#receiver_contact_phone_number").append(data.receiver.contact_phone_number);
            
            // other information
            if (data.delivery_time == 'any')
                $("#delivery_time").append("不指定");
            else if (data.delivery_time == '0-12')
                $("#delivery_time").append("中午前");              
            else if (data.delivery_time == '12-17')
                $("#delivery_time").append("12時～17時");              
            else if (data.delivery_time == '17-20')
                $("#delivery_time").append("17時～20時");
                
            if (data.receipt_to == '1')
                $("#receipt_to").append("寄給訂購人");
            else if (data.receipt_to == '2')
                $("#receipt_to").append("寄給收件人");              
            else if (data.receipt_to == '3')
                $("#receipt_to").append("捐贈創世基金會");              
        },
    });
    
    var Workspace = Backbone.Router.extend({

      routes: {
        '(/)': 'shopping',        
        'clothing(/)': 'shopping',
        'clothing/category(/)': 'shopping',
        'clothing/category/(:page)(/)*hash': 'shopping',
        'clothing/brandsonly/:id': 'clothingView',
        'clothing/marketplace/:id': 'clothingView',
        'clothing/designer/:id': 'clothingView',
        'clothing/special(/)': 'shopping',
        'checkout/first': 'checkoutFirst',
        'checkout/second': 'checkoutSecond',
        "#test_router": "testRouter",
        'member/*path': 'shopping',
        'clothing/getmorex/*path': 'shopping',
        'closet/*path': 'shopping',
        'clothing/reduction(/)': 'shopping',
        'clothing/earthhour(/)': 'shopping',
        'clothing/suggested(/)': 'shopping',
        'clothing/search/*path': 'shopping',
        'clothing/mkplace(/)': 'shopping',
        'clothing/dsner(/)': 'shopping',
        'info/*path': 'shopping',
        'verify/*path': 'shopping',
        'marketplace/*path': 'shopping',
        'message/*path': 'shopping',
        'marketplace(/)': 'shopping',
        'clothing/*path': 'shopping'
      },
      
      testRouter: function() {
          alert('test router');
      },

      shopping: function() {
            var items = new ItemList;
            var cart = new Cart({collection: items});
      },

      clothingView: function() {
            var items = new ItemList;
            var cart = new Cart({collection: items});
            var clothing = JSON.parse($("#clothing_json_data").html());
            console.log(clothing);
            console.log(cart.collection);
            
            // if the item is already in the cart, disable the 'add' button
            cart.collection.each(function(model) {
                var model_attrs = model.attributes;
                if (model_attrs.id == clothing.id+'-'+clothing.launch_way){
                    $('#product-view-buy').html('已加入購物車');
                    $('#product-view-buy').prop('disabled', true);
                }
            });
      },

      checkoutFirst: function() {
            var items = new ItemList;
            var cart = new Cart({collection: items});
            console.log('in checkout workspace');
            
            
            var orderModel = new OrderModel({ id: 1 });
            var order = new Order({model: orderModel});

            $('#twzipcode1').twzipcode({
              countyName: 'buyer_county',
              districtName: 'buyer_district',
              zipcodeName: 'buyer_zipcode',
              addressName: 'buyer_address',
              zipcodeSel: orderModel.attributes.buyer.location.zipcode,
              addressSel: orderModel.attributes.buyer.location.address
            });

            $('#twzipcode2').twzipcode({
              countyName: 'receiver_county',
              districtName: 'receiver_district',
              zipcodeName: 'receiver_zipcode',
              addressName: 'receiver_address',
              zipcodeSel: orderModel.attributes.receiver.location.zipcode,
              addressSel: orderModel.attributes.receiver.location.address
            });

          $("input[name=buyer_address]").addClass('long_address');
          $("input[name=receiver_address]").addClass('long_address');

            order.showFirstOrder(orderModel.attributes);

      },
      
      checkoutSecond: function() {
            // this is just for the next button
            var orderModel = new OrderModel({ id: 1 });
            var order = new Order({model: orderModel});
            var self = this;
            var account_balance = $('.account_balance').html();
            if (parseInt(account_balance) <= 0){
                $('.deduction_related').hide();
            }

            $('.deduction_amount').change(self.checkDeductionAndCoupon);
            $('.coupon_code').change(function(){
                $('.coupon_amount').html(0);
                self.checkDeductionAndCoupon();              
                var coupon_code = $(this).val();
                var items_price = $('.cart_1_price').html();
                if (!coupon_code){
                    return false;
                }
                $.ajax({
                    type: 'POST',
                    url: "/checkout/coupon_code",
                    data: { coupon_code: coupon_code, items_price: items_price },
                    dataType: 'json',
                    success: function(res){
                        if (res.status == 'error'){
                            alert(res.message);
                            $('.coupon_code').val('');
                        }
                        else if (res.status == 'success'){
                            $('.coupon_amount').html(res.coupon.amount);
                            self.checkDeductionAndCoupon();
                            alert('禮券折抵' + res.coupon.amount + '元');
                        }
                    },
                    error: function(res){
                        alert("failed, please try again");
                    },
                    complete: function(res){
                        console.log(res);
                        console.log(res.responseJSON);
                    }
                });
            });
            
            /* due to security issue, checkout/second page show data from server rather than localStorage
             * so function showSecondOrder is no longer used :)
             * besides, we dont use showCheckoutCart here. 
             * although the page looks totally the same(except for the delete button) as first step :)
             */        
            
            /* var items = new ItemList;
            var cart = new Cart({collection: items});
            console.log('in checkout workspace');
            cart.showCheckoutCart();
            */

            // var orderModel = new OrderModel({ id: 1 });
            // var order = new Order({model: orderModel});
            // order.showSecondOrder(orderModel.attributes);            
      },
      
      checkDeductionAndCoupon: function(){
          var total_price = $('.cart_1_sum').html();
          var account_balance = $('.account_balance').html();
          var deduction_amount = $('.deduction_amount').val();
          var coupon_amount = $('.coupon_amount').html();
          
          // quick and dirty fix for the negative condition
          if (parseInt(account_balance)<0){
              account_balance = 0;
          }
          
          if (validateDeductionAmount(deduction_amount)){
              var final_price = total_price - deduction_amount - coupon_amount;
              if( validateAccountBalance(account_balance, deduction_amount) )
              {
                  // just for improving UX
                  if ( !validateFinalPrice(final_price) ){
                      $('.deduction_amount').val(total_price - coupon_amount);
                      $('.deduction_amount').trigger('change');
                      return;
                  }
                
                  $('.final_price').html( final_price );
                  if (final_price == 0){
                      $("#deduction").prop('checked', true);     
                      $('.cart_3_choose_pay').hide();
                  }else{
                      // $("#credit-cart").prop('checked', true);     
                      $('.cart_3_choose_pay').show();
                  }
              }
          }        
      }
      
    });    
    
    workspace = new Workspace;
    Backbone.history.start({pushState: true, hashChange: false, root: '/'});
});

function isPositiveInteger(n) {
    return n >>> 0 === parseFloat(n);
}

function validateDeductionAmount(deduction_amount){
    console.log('validateDeductionAmount: ' + deduction_amount);
    if ( isNaN(deduction_amount) ){
        alert('請輸入數字');
        return false;
    }

    if ( deduction_amount < 0 ){
        alert('請輸入正整數');
        return false;
    }
    if ( !isPositiveInteger(deduction_amount) ){
        alert('請輸入正整數。');
        return false;              
    }
    
    return true;
}

function validateAccountBalance(account_balance, deduction_amount){
    account_balance = parseInt(account_balance);
    deduction_amount = parseInt(deduction_amount);
    console.log('validateAccountBalance: ' + account_balance + ', ' + deduction_amount);
    if ( deduction_amount > account_balance ){
        alert('您的帳戶餘額不足。');
        return false;              
    }
    
    return true;
  
}

function validateFinalPrice(final_price){
    console.log('validateFinalPrice: ' + final_price);
    if ( isNaN(final_price) ){
        throw new Error('final_price should be number');
    }
    /*
    if ( !isPositiveInteger(deduction_amount) ){
        throw new Error('final_price should be number');         
    }
    */
    if ( final_price < 0 ){
        // alert('應付餘額不可小於零');
        return false;
    }
    /*
    if ( final_price == 0 ){
        alert('應付餘額不可為零');
        return false;
    }
    */
    return true;
}


// slide down alert by stanley
function slideDownAlert(alertText) {
    $("body").prepend('<div id="slideDownAlert" style="opacity: 0.9; z-index: 200;width: 100%;background-color:#BDE1D6;height:30px;text-align:center;padding-top:5px;color:#fff;position:fixed; display: none">'+ alertText +'</div>').slideDown();
      $( "#slideDownAlert" ).slideDown( "slow", function() {
        setTimeout(function(){ 
          $( "#slideDownAlert" ).slideUp("normal", function() {
            $(this).remove(); 
          });
        }, 3000);
    });
}
