/*
 * Enables the search functionality.
 * 
 * @author howtomakeaturn
 */

var SearchModule;

SearchModule = (function(){

    var $ = window.jQuery;
      
    var _search = function(keyword){
        // trim the space from both sides
        keyword = keyword.replace(/^\s+|\s+$/gm,'');
        if (keyword != ''){
            var location = '/clothing/search/' + keyword;
            window.location = location;
//            window.open(location, '_blank');            
        }
    }; //
  
    // the public API interface
    return {
        search: function(keyword){
            _search(keyword);
        }
    };
  
}());


$(document).ready(function(){

    $('.head-search-btn').click(function(){        
        SearchModule.search($(".head-search").val());
    });
    
    $(".head-search").keypress(function (e) {
        if (e.keyCode == 13) {
            SearchModule.search($(".head-search").val());            
        }
    });   
  
});
