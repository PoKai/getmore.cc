# Filename: main.js
require.config
    paths:
        jquery: "../vendor/jquery/jquery"
        underscore: "../vendor/underscore-amd/underscore"
        backbone: "../vendor/backbone-amd/backbone"
        handlebars: "../vendor/handlebars/handlebars.runtime"
        templates: "../templates"
    shim:
        "../vendor/jquery.lazyload/jquery.lazyload.min": ['jquery']
        "../vendor/jquery.slideShow/jquery.slideshow.min": ['jquery']
        "../vendor/jquery-serialize-object/jquery.serialize-object": ['jquery']
        "libs/jquery/jquery.twzipcode-1.4.1": ['jquery']
        "libs/jquery/jqzoom/js/jquery.jqzoom-core-pack": ['jquery']
        "libs/handlebars_helper": ['handlebars']
        "../templates/template": ['handlebars']

require ["app"], (App) ->
    App.initialize()
