// edited by popchieh, will edit coffeescript later
// use r.js to compile this file
// cmd: ~$node r.js -o app.build.js 
(function() {
  var removeKey;

  removeKey = function(arrayName, key) {
    var tmpArray, x;
    x = void 0;
    tmpArray = [];
    for (x in arrayName) {
      x = parseInt(x);
      if (x !== key) {
        tmpArray.push(arrayName[x]);
      }
    }
    return tmpArray;
  };

  getmore.API = {
    getCarts: "/API/Cart/getCarts",
    getSellcarts: "/API/Sellcart/getCarts",
    getConfigs: "/API/Config/getConfigs",
    getSubjectList: "/API/Subject/getSubjectList",
    getEventList: "/API/Event/getEventList",
    me: "/API/Users/me"
  };

  define(["jquery", "underscore", "backbone", "models/config", "models/me", "handlebars", "../vendor/jquery-migrate/jquery-migrate", "../vendor/jquery.lazyload/jquery.lazyload.min", "../vendor/jquery.slideShow/jquery.slideshow.min", "../vendor/jquery-serialize-object/jquery.serialize-object", "libs/jquery/jquery.twzipcode-1.4.1", "libs/jquery/jqzoom/js/jquery.jqzoom-core-pack", "libs/handlebars_helper", "../templates/template"], function($, _, Backbone, ModelConfig, ModelMe) {
    var AppView, Cart, Order, Sellcart, ajaxSettings, api_req, debug, initialize, is_ajax_load, local_storage;
    debug = false;
    is_ajax_load = false;
    ajaxSettings = {
      dataType: "json",
      crossDomain: true
    };
    api_req = function(name, callback, settings) {
      settings = (!settings ? {} : settings);
      return $.ajax($.extend({}, ajaxSettings, {
        url: name,
        type: (settings.data ? "POST" : "GET"),
        success: callback,
        error: function(xhr, status, errorThrown) {
          var message;
          message = "Unknown error. Please try again later.";
          switch (status) {
            case "timeout":
              return message = "Server timeout. Please try again later.";
            case "error":
            case "parsererror":
              return message = "Server experienced some difficulty. Please try again later.";
            case "abort":
              return message = "Aborted.";
          }
        }
      }, settings));
    };
    getmore.api_req = {
      GET: function(path, data, callback, settings) {
        settings = settings || {};
        data = data || {};
        return api_req(path, callback, $.extend({
          type: "GET",
          data: data
        }, settings));
      },
      POST: function(path, data, callback, settings) {
        settings = settings || {};
        data = data || {};
        return api_req(path, callback, $.extend({
          type: "POST",
          data: data
        }, settings));
      },
      PUT: function(path, data, callback, settings) {
        settings = settings || {};
        data = data || {};
        return api_req(path, callback, $.extend({
          type: "PUT",
          data: data
        }, settings));
      },
      DELETE: function(path, data, callback, settings) {
        settings = settings || {};
        data = data || {};
        return api_req(path, callback, $.extend({
          type: "DELETE",
          data: data
        }, settings));
      }
    };
    local_storage = function() {
      this.add_item = function(item, value) {
        if (item === null || item === "") {
          return;
        }
        return localStorage.setItem(item, JSON.stringify(value));
      };
      this.get_item = function(item) {
        var value;
        value = void 0;
        if (item === null || item === "") {
          return;
        }
        value = localStorage.getItem(item);
        if (value) {
          return JSON.parse(value);
        } else {
          return "";
        }
      };
      this.delete_item = function(item) {
        if (item === null || item === "") {
          return;
        }
        return localStorage.removeItem(item);
      };
      this.get_carts = function(item) {
        var carts;
        carts = this.get_item("carts");
        if (carts !== "" && _.isArray(carts)) {
          return carts;
        } else {
          return [];
        }
      };
      this.get_sellcarts = function(item) {
        var sellcarts;
        sellcarts = this.get_item("sellcarts");
        if (sellcarts !== "" && _.isArray(sellcarts)) {
          return sellcarts;
        } else {
          return [];
        }
      };
      return null;
    };
    Cart = Backbone.View.extend({
      el: "body",
      events: {
        "click .add-cart": "add_shopping_cart",
        "click .delete-cart": "delete_shopping_cart",
        "click .update-cart": "update_shopping_cart"
      },
      initial_cart: function() {
        var carts, length, _results;
        carts = getmore.storage.get_carts("carts");
        length = void 0;
        length = carts.length;
        if (length > 0) {
          $("#shopping-empty").hide();
          $("#shopping-region").show();
          $("#shopping-car").html("");
          length;
          _results = [];
          while (length--) {
            _results.push($("#shopping-car").append(Handlebars.templates["shopping_cart"](carts[length])));
          }
          return _results;
        }
      },
      insert_cart: function(carts, data) {
        data = data || {};
        $("#shopping-car").append(Handlebars.templates["shopping_cart"](data));
        $("#shopping-cart-area").trigger("click");
        setTimeout((function(e) {
          return $("#shopping-cart-area").trigger("click");
        }), 2000);
        carts.push(data);
        $(".cart-count").text(carts.length);
        getmore.storage.add_item("carts", carts);
        $.post(getmore.API.getCarts, {
          carts: JSON.stringify(carts),
          force: true
        });
        return debug && console.log(carts);
      },
      add_shopping_cart: function(e) {
        var carts, data, i, insert, total;
        e.stopImmediatePropagation();
        e.preventDefault();
        total = parseInt($(".cart-count").text());
        data = {
          id: $(e.currentTarget).data("id"),
          item: ($(e.currentTarget).data("type") === "product" ? "商品" : "活動"),
          type: $(e.currentTarget).data("type"),
          count: $("select[name=\"order_count\"]").val(),
          title: ($(e.currentTarget).data("type") === "product" ? $(".product_title .title").text() : $(".event_title .title").text()),
          price: $(e.currentTarget).data("price"),
          total: parseInt($(e.currentTarget).data("price")) * parseInt($("select[name=\"order_count\"]").val())
        };
        carts = getmore.storage.get_carts("carts");
        insert = true;
        i = 0;
        while (i < carts.length) {
          if (carts[i].type === data.type && carts[i].id === data.id) {
            insert = false;
            alert("此商品已存在於您的購物車中，可在結帳前修改數量，謝謝！");
            break;
          }
          i++;
        }
        if (insert) {
          return this.insert_cart(carts, data);
        }
      },
      update_total_value: function() {
        var all_total_price, is_transportation_charge, mini_price, total_d_price, total_event_price, total_o_price, total_product_price, transportation_charge;
        total_o_price = total_d_price = total_product_price = total_event_price = all_total_price = 0;
        is_transportation_charge = false;
        mini_price = +getmore.Config.attributes.min_price;
        transportation_charge = +getmore.Config.attributes.transportation_charge;
        $(".product_body tr").each(function(i) {
          var self, unit;
          self = $(this);
          unit = $(this).find("select").val() || 0;
          total_o_price += unit * $(this).find(".original_price").text();
          total_product_price += unit * $(this).find(".sell_price").text();
          return total_d_price = total_o_price - total_product_price;
        });
        all_total_price += total_product_price;
        $("#product_original_price").text(total_o_price);
        $("#product_discount_price").text(total_d_price);
        $(".product_total_price").text(total_product_price);
        total_o_price = total_d_price = 0;
        $(".event_body tr").each(function(i) {
          var self, unit;
          self = $(this);
          unit = $(this).find("select").val() || 0;
          return total_event_price += unit * $(this).find(".unit_price").text();
        });
        all_total_price += total_event_price;
        $(".event_total_price").text(total_event_price);
        if (total_product_price !== 0 && all_total_price < mini_price) {
          $(".transportation_charge").text(transportation_charge);
          all_total_price += transportation_charge;
        } else {
          $(".transportation_charge").text("0");
        }
        return $("#total_price").text(all_total_price);
      },
      update_shopping_cart: function(e) {
        var carts, data, i, price, self, top, total_price, type, unit;
        e.stopImmediatePropagation();
        e.preventDefault();
        debug && console.log("update cart");
        type = $(e.currentTarget).data("type");
        top = $(e.currentTarget).parent().parent();
        carts = getmore.storage.get_carts("carts");
        total_price = 0;
        unit = void 0;
        price = void 0;
        data = void 0;
        self = this;
        unit = top.find("select").val();
        $(e.currentTarget).data("unit", unit);
        total_price = unit * $(e.currentTarget).data("price");
        top.find(".total_price").text(total_price);
        self.update_total_value();
        data = {
          id: $(e.currentTarget).data("id"),
          type: $(e.currentTarget).data("type"),
          total: total_price,
          count: unit
        };
        i = 0;
        while (i < carts.length) {
          if (carts[i].type === data.type && carts[i].id === data.id) {
            carts[i].count = data.count;
            carts[i].total = data.total;
            break;
          }
          i++;
        }
        getmore.storage.add_item("carts", carts);
        $.post(getmore.API.getCarts, {
          carts: JSON.stringify(carts),
          force: true
        });
        return this.initial_cart();
      },
      delete_shopping_cart: function(e) {
        var carts, data, i, message, self, update;
        e.stopImmediatePropagation();
        e.preventDefault();
        self = this;
        message = ($(e.currentTarget).data("type") === "product" ? "確定刪除商品?" : "確定取消報名?");
        update = $(e.currentTarget).data("update") || 0;
        carts = getmore.storage.get_carts("carts");
        data = {
          id: $(e.currentTarget).data("id"),
          type: $(e.currentTarget).data("type")
        };
        if (!window.confirm(message)) {
          return false;
        }
        i = 0;
        while (i < carts.length) {
          if (carts[i].type === data.type && carts[i].id === data.id) {
            carts = removeKey(carts, i);
            break;
          }
          i++;
        }
        $(e.currentTarget).parent().parent().hide("slow", function() {
          $(this).remove();
          if (update === "1") {
            return self.update_total_value();
          }
        });
        if (carts.length > 0) {
          getmore.storage.add_item("carts", carts);
        } else {
          getmore.storage.delete_item("carts");
          carts = [];
          $("#shopping-empty").show();
          $("#shopping-region").hide();
        }
        $(".cart-count").text(carts.length);
        $.post(getmore.API.getCarts, {
          carts: JSON.stringify(carts),
          force: true
        });
        return debug && console.log(carts);
      },
      initialize: function() {
        var carts;
        carts = getmore.storage.get_carts("carts");
        debug && console.log(carts);
        $.ajax({
          url: getmore.API.getCarts,
          dataType: "json",
          type: "POST",
          data: {
            carts: JSON.stringify(carts)
          },
          success: function(response) {
            var i;
            debug && console.log(response);
            if (response.success_text && (response.carts != null) && _.isArray(response.carts) && response.carts.length > 0) {
              getmore.storage.add_item("carts", response.carts);
              carts = response.carts;
            }
            if (_.isArray(carts) && carts.length > 0) {
              i = 0;
              while (i < carts.length) {
                if (carts[i].id) {
                  $("#shopping-car").append(Handlebars.templates["shopping_cart"](carts[i]));
                }
                i++;
              }
            }
            return $(".cart-count").text(carts.length);
          }
        });
        getmore.Config = new ModelConfig();
        return getmore.Config.fetch();
      }
    });
    Order = Backbone.View.extend({
      el: "body",
      events: {
        "click .payment": "payment",
        "click #step2": "step2",
        "click #step3": "step3"
      },
      payment: function(e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        $(e.currentTarget).parent().find("li").removeClass("active");
        $(e.currentTarget).addClass("active");
        return $("input[name=payment_method]").val($(e.currentTarget).data("type"));
      },
      step2: function(e) {
        var form_id;
        e.stopImmediatePropagation();
        e.preventDefault();
        form_id = $(e.currentTarget).data("form");
        if (window.confirm("商品活動數量確定，進入下一步填寫資料?")) {
          return $("#" + form_id).submit();
        }
      },
      step3: function(e) {
        var error, form_id, form_info, name;
        e.stopImmediatePropagation();
        e.preventDefault();
        error = false;
        form_id = $(e.currentTarget).data("form");
        form_info = $("#" + form_id).serializeObject();
        for (name in form_info) {
          $("input[name=" + name + "]").removeClass("form_error");
          if ($.trim(form_info[name]) === "" && name !== "uniform_invoice_title" && name !== "uniform_invoice_number") {
            $("input[name=" + name + "]").addClass("form_error");
            error = true;
          }
        }
        if (error) {
          alert("紅色欄位務必填寫");
          e.stopImmediatePropagation();
          return false;
        }
        if (window.confirm("資料填寫完成，確定完成此購物?")) {
          getmore.storage.delete_item("carts");
          return $("#" + form_id).submit();
        }
      },
      initialize: function() {
        return $("input[name=uniform_invoice_type]").click(function() {
          var type;
          type = $("input[name=uniform_invoice_type]:checked").val();
          if (type === "1" || type === "2") {
            return $(".show_uniform_invoice").show();
          } else {
            return $(".show_uniform_invoice").hide();
          }
        });
      }
    });
    Sellcart = Backbone.View.extend({
      el: "body",
      events: {
        "click .add-sellcart": "add_shopping_cart",
        "click .delete-sellcart": "delete_shopping_cart",
        "click .update-sellcart": "update_shopping_cart"
      },
      initial_cart: function() {
        var carts, length, _results;
        carts = getmore.storage.get_sellcarts("sellcarts");
        length = void 0;
        length = carts.length;
        if (length > 0) {
          $("#shopping-empty").hide();
          $("#shopping-region").show();
          $("#shopping-car").html("");
          length;
          _results = [];
          while (length--) {
            _results.push($("#shopping-car").append(Handlebars.templates["shopping_cart"](carts[length])));
          }
          return _results;
        }
      },
      insert_cart: function(carts, data) {
        data = data || {};
        $("#shopping-car").append(Handlebars.templates["shopping_cart"](data));
        carts.push(data);
        $(".cart-count").text(carts.length);
        getmore.storage.add_item("sellcarts", carts);
        $.post(getmore.API.getSellcarts, {
          carts: JSON.stringify(carts),
          force: true
        });
        return debug && console.log(carts);
      },
      add_shopping_cart: function(e) {
        console.log("fs");
        var carts, data, insert, total;
        e.stopImmediatePropagation();
        e.preventDefault();
        total = parseInt($(".cart-count").text());
        data = {
          title: $(".product_desc").text(),
          product_type: $(".product_type").text(),
          product_brand: $(".product_brand").text(),
          original_price: $(".original_price").text(),
          sell_price: $(".sell_price").text(),
          shipping_size: $(".shipping_size").text()
        };
        carts = getmore.storage.get_sellcarts("sellcarts");
        insert = true;
        if (insert) {
          return this.insert_cart(carts, data);
        }
      },
      update_total_value: function() {
        var all_total_price, is_transportation_charge, mini_price, total_d_price, total_event_price, total_o_price, total_product_price, transportation_charge;
        total_o_price = total_d_price = total_product_price = total_event_price = all_total_price = 0;
        is_transportation_charge = false;
        mini_price = +getmore.Config.attributes.min_price;
        transportation_charge = +getmore.Config.attributes.transportation_charge;
        $(".product_body tr").each(function(i) {
          var self, unit;
          self = $(this);
          unit = $(this).find("select").val() || 0;
          total_o_price += unit * $(this).find(".original_price").text();
          total_product_price += unit * $(this).find(".sell_price").text();
          return total_d_price = total_o_price - total_product_price;
        });
        all_total_price += total_product_price;
        $("#product_original_price").text(total_o_price);
        $("#product_discount_price").text(total_d_price);
        $(".product_total_price").text(total_product_price);
        total_o_price = total_d_price = 0;
        $(".event_body tr").each(function(i) {
          var self, unit;
          self = $(this);
          unit = $(this).find("select").val() || 0;
          return total_event_price += unit * $(this).find(".unit_price").text();
        });
        all_total_price += total_event_price;
        $(".event_total_price").text(total_event_price);
        if (total_product_price !== 0 && all_total_price < mini_price) {
          $(".transportation_charge").text(transportation_charge);
          all_total_price += transportation_charge;
        } else {
          $(".transportation_charge").text("0");
        }
        return $("#total_price").text(all_total_price);
      },
      update_shopping_cart: function(e) {
        var carts, data, i, price, self, top, total_price, type, unit;
        e.stopImmediatePropagation();
        e.preventDefault();
        debug && console.log("update sellcart");
        type = $(e.currentTarget).data("type");
        top = $(e.currentTarget).parent().parent();
        carts = getmore.storage.get_sellcarts("sellcarts");
        total_price = 0;
        unit = void 0;
        price = void 0;
        data = void 0;
        self = this;
        unit = top.find("select").val();
        $(e.currentTarget).data("unit", unit);
        total_price = unit * $(e.currentTarget).data("price");
        top.find(".total_price").text(total_price);
        self.update_total_value();
        data = {
          id: $(e.currentTarget).data("id"),
          type: $(e.currentTarget).data("type"),
          total: total_price,
          count: unit
        };
        i = 0;
        while (i < carts.length) {
          if (carts[i].type === data.type && carts[i].id === data.id) {
            carts[i].count = data.count;
            carts[i].total = data.total;
            break;
          }
          i++;
        }
        getmore.storage.add_item("sellcarts", carts);
        $.post(getmore.API.getSellcfarts, {
          carts: JSON.stringify(carts),
          force: true
        });
        return this.initial_cart();
      },
      delete_shopping_cart: function(e) {
        var carts, data, i, message, self, update;
        e.stopImmediatePropagation();
        e.preventDefault();
        self = this;
        message = ($(e.currentTarget).data("type") === "product" ? "確定刪除商品?" : "確定取消報名?");
        update = $(e.currentTarget).data("update") || 0;
        carts = getmore.storage.get_sellcarts("sellcarts");
        data = {
          id: $(e.currentTarget).data("id"),
          type: $(e.currentTarget).data("type")
        };
        if (!window.confirm(message)) {
          return false;
        }
        i = 0;
        while (i < carts.length) {
          if (carts[i].type === data.type && carts[i].id === data.id) {
            carts = removeKey(carts, i);
            break;
          }
          i++;
        }
        $(e.currentTarget).parent().parent().hide("slow", function() {
          $(this).remove();
          if (update === "1") {
            return self.update_total_value();
          }
        });
        if (carts.length > 0) {
          getmore.storage.add_item("sellcarts", carts);
        } else {
          getmore.storage.delete_item("sellcarts");
          carts = [];
          $("#shopping-empty").show();
          $("#shopping-region").hide();
        }
        $(".cart-count").text(carts.length);
        $.post(getmore.API.getSellcarts, {
          carts: JSON.stringify(carts),
          force: true
        });
        return debug && console.log(carts);
      },
      initialize: function() {
        var carts;
        carts = getmore.storage.get_sellcarts("sellcarts");
        debug && console.log(carts);
        $.ajax({
          url: getmore.API.getSellcarts,
          dataType: "json",
          type: "POST",
          data: {
            carts: JSON.stringify(carts)
          },
          success: function(response) {
            var i;
            debug && console.log(response);
            if (response.success_text && (response.carts != null) && _.isArray(response.carts) && response.carts.length > 0) {
              getmore.storage.add_item("sellcarts", response.carts);
              carts = response.carts;
            }
            if (_.isArray(carts) && carts.length > 0) {
              i = 0;
              while (i < carts.length) {
                if (carts[i].id) {
                  $("#shopping-car").append(Handlebars.templates["shopping_cart"](carts[i]));
                }
                i++;
              }
            }
            return $(".cart-count").text(carts.length);
          }
        });
        getmore.Config = new ModelConfig();
        return getmore.Config.fetch();
      }
    });
    Order = Backbone.View.extend({
      el: "body",
      events: {
        "click .payment": "payment",
        "click #step2": "step2",
        "click #step3": "step3"
      },
      payment: function(e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        $(e.currentTarget).parent().find("li").removeClass("active");
        $(e.currentTarget).addClass("active");
        return $("input[name=payment_method]").val($(e.currentTarget).data("type"));
      },
      step2: function(e) {
        var form_id;
        e.stopImmediatePropagation();
        e.preventDefault();
        form_id = $(e.currentTarget).data("form");
        if (window.confirm("商品活動數量確定，進入下一步填寫資料?")) {
          return $("#" + form_id).submit();
        }
      },
      step3: function(e) {
        var error, form_id, form_info, name;
        e.stopImmediatePropagation();
        e.preventDefault();
        error = false;
        form_id = $(e.currentTarget).data("form");
        form_info = $("#" + form_id).serializeObject();
        for (name in form_info) {
          $("input[name=" + name + "]").removeClass("form_error");
          if ($.trim(form_info[name]) === "" && name !== "uniform_invoice_title" && name !== "uniform_invoice_number") {
            $("input[name=" + name + "]").addClass("form_error");
            error = true;
          }
        }
        if (error) {
          alert("紅色欄位務必填寫");
          e.stopImmediatePropagation();
          return false;
        }
        if (window.confirm("資料填寫完成，確定完成此購物?")) {
          getmore.storage.delete_item("carts");
          return $("#" + form_id).submit();
        }
      },
      initialize: function() {
        return $("input[name=uniform_invoice_type]").click(function() {
          var type;
          type = $("input[name=uniform_invoice_type]:checked").val();
          if (type === "1" || type === "2") {
            return $(".show_uniform_invoice").show();
          } else {
            return $(".show_uniform_invoice").hide();
          }
        });
      }
    });
    AppView = Backbone.View.extend({
      el: "body",
      events: {
        click: "close_all_menu",
        "click .close": "close_message",
        "click #login-area": "login_area",
        "click #shopping-cart-area": "shopping_area",
        "click .check-payment": "check_payment",
        "click #add_to_delivery": "add_to_delivery"
      },
      add_to_delivery: function(e) {
        $("input[name=delivery_name]").val($("input[name=user_name]").val());
        if ($("input[name=user_sex]:checked").val() === "1") {
          $(".delivery_sex_man").prop("checked", true);
        } else {
          $(".delivery_sex_woman").prop("checked", true);
        }
        $("input[name=delivery_mobile_phone]").val($("input[name=user_mobile_phone]").val());
        $("input[name=delivery_telephone]").val($("input[name=user_telephone]").val());
        $("select[name=delivery_county]").val($("select[name=user_county]").val()).trigger("change");
        $("select[name=delivery_district]").val($("select[name=user_district]").val()).trigger("change");
        $("input[name=delivery_zipcode]").val($("input[name=user_zipcode]").val());
        return $("input[name=delivery_address]").val($("input[name=user_address]").val());
      },
      check_payment: function(e) {
        var $target;
        e.preventDefault();
        $target = $(e.currentTarget);
        if (window.confirm("線上付款總金額:" + $target.data("price"))) {
          window.location.href = "/order/pay/" + $target.data("id");
        }
        return false;
      },
      close_all_menu: function(e) {
        var $target;
        return $target = $(e.target);
      },
      close_message: function(e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        return $(e.currentTarget).parent().fadeOut("slow");
      },
      login_area: function(e) {
        var $target;
        $target = $(e.target);
        if ($target.is(".login-area *")) {
          return;
        }
        $(e.currentTarget).toggleClass("active");
        return $(e.currentTarget).find(".login-area").toggleClass("show");
      },
      shopping_area: function(e) {
        var $target;
        $target = $(e.target);
        if ($target.is(".shopping-cart-area *")) {
          return;
        }
        $(e.currentTarget).toggleClass("active");
        $(e.currentTarget).find(".shopping-cart-area").toggleClass("show");
        return $(".shopping-cart").slideDown();
      },
      update_user: function(e) {
        var obj;
        if (this.me.get("p_address") && $("#p_address").length > 0) {
          obj = $.parseJSON(this.me.get("p_address"));
          $("#p_address").twzipcode({
            zipcodeSel: obj.zipcode,
            addressSel: obj.address,
            css: ["input-small", "input-small", "input-small zipcode"]
          });
        }
        if (this.me.get("p_address") && $("#user_address").length > 0) {
          obj = $.parseJSON(this.me.get("p_address"));
          $("#user_address").twzipcode({
            zipcodeSel: obj.zipcode,
            addressSel: obj.address,
            countyName: "user_county",
            districtName: "user_district",
            zipcodeName: "user_zipcode",
            addressName: "user_address",
            css: ["input-small", "input-small", "input-small zipcode", "input-small address"]
          });
        }
        if (this.me.get("p_address") && $("#delivery_address").length > 0) {
          obj = $.parseJSON(this.me.get("s_address"));
          return $("#delivery_address").twzipcode({
            zipcodeSel: obj.zipcode,
            addressSel: obj.address,
            countyName: "delivery_county",
            districtName: "delivery_district",
            zipcodeName: "delivery_zipcode",
            addressName: "delivery_address",
            css: ["input-small", "input-small", "input-small zipcode", "input-small address"]
          });
        }
      },
      add_placeholder: function(id, placeholder) {
        var el;
        if ($("#" + id).length <= 0) {
          return;
        }
        el = document.getElementById(id);
        el.placeholder = placeholder;
        el.onfocus = function() {
          if (this.value === this.placeholder) {
            this.value = "";
            return el.style.cssText = "";
          }
        };
        el.onblur = function() {
          if (this.value.length === 0) {
            this.value = this.placeholder;
            return el.style.cssText = "color:#A9A9A9;";
          }
        };
        return el.onblur();
      },
      initialize: function() {
        var is_login_pannel, is_shopping_pannel, is_show_pannel;
        is_show_pannel = 0;
        is_login_pannel = 0;
        is_shopping_pannel = 0;
        $("img.lazy").lazyload({
          effect: "fadeIn"
        }).removeClass("lazy");
        $(document).ajaxStop(function() {
          return $("img.lazy").lazyload({
            effect: "fadeIn"
          }).removeClass("lazy");
        });
        $(".slideShow").slideShow({
          interval: 10,
          transition: {
            mode: "slideshow",
            speed: 800
          }
        });
        $("#shopping-cart-area").mouseenter(function(e) {
          $(this).addClass("active");
          $(this).find(".shopping-cart-area").addClass("show");
          return $(".shopping-cart").slideDown();
        }).mouseleave(function() {
          $(this).removeClass("active");
          $(this).find(".shopping-cart-area").removeClass("show");
          return $(".shopping-cart").slideUp();
        });
        $("#login-area, .login").mouseenter(function(e) {
          $(this).addClass("active");
          return $(this).find(".login-area").addClass("show");
        }).mouseleave(function() {
          $(this).removeClass("active");
          return $(this).find(".login-area").removeClass("show");
        });
        $("#main_pic").jqzoom({
          zoomType: "reverse",
          zoomWidth: 500,
          zoomHeight: 350,
          xOffset: 40,
          showEffect: "fadein"
        });
        $("input[name='identity']").focus();
        $(window).scroll(function() {
          var api_path, id, limit, module, offset, template;
          if ($(window).scrollTop() === $(document).height() - $(window).height()) {
            api_path = void 0;
            template = void 0;
            module = $("#content-row-3").data("module");
            limit = $("#content-row-3").data("limit");
            id = $("#content-row-3").data("id") || 0;
            offset = $("#content-row-3 .small-article").length || 0;
            if (!module || !limit) {
              return;
            }
            switch (module) {
              case "subject":
                api_path = getmore.API.getSubjectList;
                template = "single_subject";
                break;
              case "event":
                api_path = getmore.API.getEventList;
                template = "single_event";
            }
            if (module && is_ajax_load === false) {
              is_ajax_load = true;
              return getmore.api_req.GET(api_path, {
                limit: limit,
                offset: offset,
                id: id
              }, function(data) {
                var i;
                i = 0;
                while (i < data.items.length) {
                  $("#row-3").append(Handlebars.templates[template](data.items[i]));
                  i++;
                }
                return is_ajax_load = false;
              });
            }
          }
        });
        if (/msie/.test(navigator.userAgent.toLowerCase())) {
          this.add_placeholder("identity", "請輸入電子郵件");
          this.add_placeholder("password", "請輸入密碼");
          this.add_placeholder("user_name", "請輸入全名");
          this.add_placeholder("email", "請輸入電子郵件");
          this.add_placeholder("password_confirm", "請再次確認密碼");
          this.add_placeholder("new_password", "請輸入密碼");
        }
        this.me = new ModelMe();
        this.me.on("change", this.update_user, this);
        return this.me.fetch();
      }
    });
    initialize = function() {
      getmore.storage = new local_storage;
      getmore.View = new AppView;
      getmore.Cart = new Cart;
      getmore.Sellcart = new Sellcart;
      return getmore.Order = new Order;
    };
    return {
      initialize: initialize
    };
  });

}).call(this);
