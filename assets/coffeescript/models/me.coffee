#
# * The currently logged in user
#
define ["jquery", "underscore", "backbone"], ($, _, Backbone) ->
    ModelMe = Backbone.Model.extend(
        url: ->
            getmore.API.me

        parse: (response) ->
            response.item
    )
    ModelMe
