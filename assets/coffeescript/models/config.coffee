#
# * Config Model
#
define ["jquery", "underscore", "backbone"], ($, _, Backbone) ->
    ModelConfig = Backbone.Model.extend(
        initialize: ->

        url: ->
            getmore.API.getConfigs

        parse: (response) ->
            response.item
    )
    ModelConfig
