removeKey = (arrayName, key) ->
    x = undefined
    tmpArray = []
    for x of arrayName
        x = parseInt(x)
        tmpArray.push arrayName[x]  if x != key
    tmpArray

#
# * API List
#
# author: popchieh
getmore.API =
    getCarts: "/API/Cart/getCarts"
    getSellcarts: "/API/Sellcart/getCarts"
    getConfigs: "/API/Config/getConfigs"
    getSubjectList: "/API/Subject/getSubjectList"
    getEventList: "/API/Event/getEventList"
    me: "/API/Users/me"

define [
    "jquery",
    "underscore",
    "backbone",
    "models/config",
    "models/me",
    "handlebars",
    "../vendor/jquery-migrate/jquery-migrate",
    "../vendor/jquery.lazyload/jquery.lazyload.min",
    "../vendor/jquery.slideShow/jquery.slideshow.min",
    "../vendor/jquery-serialize-object/jquery.serialize-object",
    "libs/jquery/jquery.twzipcode-1.4.1",
    "libs/jquery/jqzoom/js/jquery.jqzoom-core-pack",
    "libs/handlebars_helper",
    "../templates/template"], ($, _, Backbone, ModelConfig, ModelMe) ->
    debug = false
    is_ajax_load = false
    ajaxSettings =
        dataType: "json"
        crossDomain: true

    api_req = (name, callback, settings) ->
        settings = (if (not settings) then {} else settings)
        $.ajax $.extend({}, ajaxSettings,
            url: name
            type: (if (settings.data) then "POST" else "GET")
            success: callback
            error: (xhr, status, errorThrown) ->
                message = "Unknown error. Please try again later."
                switch status
                  when "timeout"
                        message = "Server timeout. Please try again later."
                  when "error", "parsererror"
                        message = "Server experienced some difficulty. Please try again later."
                  when "abort"
                        message = "Aborted."
        , settings)

    getmore.api_req =
        GET: (path, data, callback, settings) ->
            settings = settings or {}
            data = data or {}
            api_req path, callback, $.extend(
                type: "GET"
                data: data
            , settings)

        POST: (path, data, callback, settings) ->
            settings = settings or {}
            data = data or {}
            api_req path, callback, $.extend(
                type: "POST"
                data: data
            , settings)

        PUT: (path, data, callback, settings) ->
            settings = settings or {}
            data = data or {}
            api_req path, callback, $.extend(
                type: "PUT"
                data: data
            , settings)

        DELETE: (path, data, callback, settings) ->
            settings = settings or {}
            data = data or {}
            api_req path, callback, $.extend(
                type: "DELETE"
                data: data
            , settings)

    local_storage = ->
        @add_item = (item, value) ->
            return  if item is null or item is ""
            localStorage.setItem item, JSON.stringify(value)

        @get_item = (item) ->
            value = undefined
            return  if item is null or item is ""
            value = localStorage.getItem(item)
            (if (value) then JSON.parse(value) else "")

        @delete_item = (item) ->
            return  if item is null or item is ""
            localStorage.removeItem item

        @get_carts = (item) ->
            carts = @get_item("carts")
            if (carts isnt "" and _.isArray(carts)) then carts else []

        @get_sellcarts = (item) ->
            sellcarts = @get_item("sellcarts")
            if (sellcarts isnt "" and _.isArray(sellcarts)) then sellcarts else []
        null

    Cart = Backbone.View.extend(
        el: "body"
        events:
            "click .add-cart": "add_shopping_cart"
            "click .delete-cart": "delete_shopping_cart"
            "click .update-cart": "update_shopping_cart"
            
        initial_cart: ->
            carts = getmore.storage.get_carts("carts")
            length = undefined
            length = carts.length
            if length > 0
                $("#shopping-empty").hide()
                $("#shopping-region").show()
                $("#shopping-car").html ""
                length
                while length--
                    $("#shopping-car").append Handlebars.templates["shopping_cart"](carts[length])

        insert_cart: (carts, data) ->
            data = data or {}

            # update shopping cart table
            $("#shopping-car").append Handlebars.templates["shopping_cart"](data)

            # show shopping cart 2 sec
            $("#shopping-cart-area").trigger "click"
            setTimeout ((e) ->

                # close shopping cart
                $("#shopping-cart-area").trigger "click"
            ), 2000
            carts.push data
            $(".cart-count").text carts.length
            getmore.storage.add_item "carts", carts

            # update cats data to session or database when add product into carts
            $.post getmore.API.getCarts,
                carts: JSON.stringify(carts)
                force: true

            (debug) and console.log(carts)


        # add event or product into cart
        add_shopping_cart: (e) ->
            e.stopImmediatePropagation()
            e.preventDefault()
            total = parseInt($(".cart-count").text())
            data =
                id: $(e.currentTarget).data("id")
                item: (if ($(e.currentTarget).data("type") is "product") then "商品" else "活動")
                type: $(e.currentTarget).data("type")
                count: $("select[name=\"order_count\"]").val()
                title: (if ($(e.currentTarget).data("type") is "product") then $(".product_title .title").text() else $(".event_title .title").text())
                price: $(e.currentTarget).data("price")
                total: parseInt($(e.currentTarget).data("price")) * parseInt($("select[name=\"order_count\"]").val())

            carts = getmore.storage.get_carts("carts")
            insert = true

            # check product exist on cart
            i = 0

            while i < carts.length
                if carts[i].type is data.type and carts[i].id is data.id
                    insert = false
                    alert "此商品已存在於您的購物車中，可在結帳前修改數量，謝謝！"
                    break
                i++

            # insert data to cart
            @insert_cart carts, data  if insert

        update_total_value: ->
            total_o_price = total_d_price = total_product_price = total_event_price = all_total_price = 0
            is_transportation_charge = false
            mini_price = +getmore.Config.attributes.min_price
            transportation_charge = +getmore.Config.attributes.transportation_charge
            $(".product_body tr").each (i) ->
                self = $(this)
                unit = $(this).find("select").val() or 0

                # update product total price
                total_o_price += unit * $(this).find(".original_price").text()
                total_product_price += unit * $(this).find(".sell_price").text()
                total_d_price = total_o_price - total_product_price


            # update cart total price
            all_total_price += total_product_price
            $("#product_original_price").text total_o_price
            $("#product_discount_price").text total_d_price
            $(".product_total_price").text total_product_price

            # initial event value
            total_o_price = total_d_price = 0
            $(".event_body tr").each (i) ->
                self = $(this)
                unit = $(this).find("select").val() or 0

                # update event total price
                total_event_price += unit * $(this).find(".unit_price").text()


            # update cart total price
            all_total_price += total_event_price
            $(".event_total_price").text total_event_price

            # add transportation price
            if total_product_price isnt 0 and all_total_price < mini_price
                $(".transportation_charge").text transportation_charge

                # update cart total price
                all_total_price += transportation_charge
            else
                $(".transportation_charge").text "0"

            # update all total price
            $("#total_price").text all_total_price

        update_shopping_cart: (e) ->
            e.stopImmediatePropagation()
            e.preventDefault()
            (debug) and console.log("update cart")
            type = $(e.currentTarget).data("type")
            top = $(e.currentTarget).parent().parent()
            carts = getmore.storage.get_carts("carts")
            total_price = 0
            unit = undefined
            price = undefined
            data = undefined
            self = this
            unit = top.find("select").val()
            $(e.currentTarget).data "unit", unit
            total_price = unit * $(e.currentTarget).data("price")
            top.find(".total_price").text total_price
            self.update_total_value()

            # update cart value
            data =
                id: $(e.currentTarget).data("id")
                type: $(e.currentTarget).data("type")
                total: total_price
                count: unit

            i = 0

            while i < carts.length
                if carts[i].type is data.type and carts[i].id is data.id
                    carts[i].count = data.count
                    carts[i].total = data.total
                    break
                i++
            getmore.storage.add_item "carts", carts

            # update cats data to session or database when add product into carts
            $.post getmore.API.getCarts,
                carts: JSON.stringify(carts)
                force: true


            # update shopping cart on tool bar
            @initial_cart()

        delete_shopping_cart: (e) ->
            e.stopImmediatePropagation()
            e.preventDefault()
            self = this
            message = (if ($(e.currentTarget).data("type") is "product") then "確定刪除商品?" else "確定取消報名?")
            update = $(e.currentTarget).data("update") or 0
            carts = getmore.storage.get_carts("carts")
            data =
                id: $(e.currentTarget).data("id")
                type: $(e.currentTarget).data("type")

            # confirm remove product
            return false  unless window.confirm(message)

            # delete product on cart
            i = 0

            while i < carts.length
                if carts[i].type is data.type and carts[i].id is data.id
                    carts = removeKey(carts, i)
                    break
                i++

            # remove table tr
            $(e.currentTarget).parent().parent().hide "slow", ->
                $(this).remove()

                # update steps 1 all price
                self.update_total_value()  if update is "1"

            if carts.length > 0

                # if carts is not empty, update local storage
                getmore.storage.add_item "carts", carts
            else

                # if carts is empty, delete local storage and create new Array
                getmore.storage.delete_item "carts"
                carts = []
                $("#shopping-empty").show()
                $("#shopping-region").hide()
            $(".cart-count").text carts.length

            # update cats data to session or database when remove product
            $.post getmore.API.getCarts,
                carts: JSON.stringify(carts)
                force: true

            (debug) and console.log(carts)

        initialize: ->
            carts = getmore.storage.get_carts("carts")
            (debug) and console.log(carts)
            $.ajax
                url: getmore.API.getCarts
                dataType: "json"
                type: "POST"
                data:
                    carts: JSON.stringify(carts)

                success: (response) ->
                    (debug) and console.log(response)
                    if response.success_text and response.carts? and _.isArray(response.carts) and response.carts.length > 0
                        getmore.storage.add_item "carts", response.carts
                        carts = response.carts
                    if _.isArray(carts) and carts.length > 0
                        i = 0

                        while i < carts.length

                            # check carts is null
                            $("#shopping-car").append Handlebars.templates["shopping_cart"](carts[i])  if carts[i].id
                            i++
                    $(".cart-count").text carts.length


            # get site config all item
            getmore.Config = new ModelConfig()
            getmore.Config.fetch()
    )
    Order = Backbone.View.extend(
        el: "body"
        events:
            "click .payment": "payment"
            "click #step2": "step2"
            "click #step3": "step3"

        payment: (e) ->
            e.stopImmediatePropagation()
            e.preventDefault()
            $(e.currentTarget).parent().find("li").removeClass "active"
            $(e.currentTarget).addClass "active"
            $("input[name=payment_method]").val $(e.currentTarget).data("type")

        step2: (e) ->
            e.stopImmediatePropagation()
            e.preventDefault()
            form_id = $(e.currentTarget).data("form")

            # redirect to complete page
            $("#" + form_id).submit()  if window.confirm("商品活動數量確定，進入下一步填寫資料?")

        step3: (e) ->
            e.stopImmediatePropagation()
            e.preventDefault()
            error = false
            form_id = $(e.currentTarget).data("form")
            form_info = $("#" + form_id).serializeObject()
            for name of form_info
                $("input[name=" + name + "]").removeClass "form_error"
                if $.trim(form_info[name]) is "" and name isnt "uniform_invoice_title" and name isnt "uniform_invoice_number"
                    $("input[name=" + name + "]").addClass "form_error"
                    error = true
            if error
                alert "紅色欄位務必填寫"
                e.stopImmediatePropagation()
                return false
            if window.confirm("資料填寫完成，確定完成此購物?")

                # delete local storage carts
                getmore.storage.delete_item "carts"

                # redirect to complete page
                $("#" + form_id).submit()

        initialize: ->

            # check uniform invoice type
            $("input[name=uniform_invoice_type]").click ->
                type = $("input[name=uniform_invoice_type]:checked").val()
                if type is "1" or type is "2"
                    $(".show_uniform_invoice").show()
                else
                    $(".show_uniform_invoice").hide()

    )

    # author: popchieh
    Sellcart = Backbone.View.extend(
        el: "body"
        events:
            "click .add-sellcart": "add_shopping_sellcart"
            "click .delete-sellcart": "delete_shopping_cart"
            "click .update-sellcart": "update_shopping_cart"

        initial_cart: ->
            carts = getmore.storage.get_sellcarts("sellcarts")
            length = undefined
            length = carts.length
            if length > 0
                $("#shopping-empty").hide()
                $("#shopping-region").show()
                $("#shopping-car").html ""
                length
                while length--
                    $("#shopping-car").append Handlebars.templates["shopping_cart"](carts[length])

        insert_cart: (carts, data) ->
            data = data or {}

            # update shopping cart table
            $("#shopping-car").append Handlebars.templates["shopping_cart"](data)

            carts.push data
            $(".cart-count").text carts.length
            getmore.storage.add_item "sellcarts", carts

            # update carts data to session or database when add product into carts
            $.post getmore.API.getSellcarts,
                carts: JSON.stringify(carts)
                force: true

            (debug) and console.log(carts)


        # add event or product into sellcart
        add_shopping_sellcart: (e) ->
            e.stopImmediatePropagation()
            e.preventDefault()
            total = parseInt($(".cart-count").text())
            data =
                title: $(".product_desc").text()
                product_type: $(".product_type").text()
                product_brand: $(".product_brand").text()
                original_price: $(".original_price").text()
                sell_price: $(".sell_price").text()
                shipping_size: $(".shipping_size").text()

            carts = getmore.storage.get_sellcarts("sellcarts")
            insert = true

            # insert data to cart
            @insert_cart carts, data  if insert

        update_total_value: ->
            total_o_price = total_d_price = total_product_price = total_event_price = all_total_price = 0
            is_transportation_charge = false
            mini_price = +getmore.Config.attributes.min_price
            transportation_charge = +getmore.Config.attributes.transportation_charge
            $(".product_body tr").each (i) ->
                self = $(this)
                unit = $(this).find("select").val() or 0

                # update product total price
                total_o_price += unit * $(this).find(".original_price").text()
                total_product_price += unit * $(this).find(".sell_price").text()
                total_d_price = total_o_price - total_product_price


            # update cart total price
            all_total_price += total_product_price
            $("#product_original_price").text total_o_price
            $("#product_discount_price").text total_d_price
            $(".product_total_price").text total_product_price

            # initial event value
            total_o_price = total_d_price = 0
            $(".event_body tr").each (i) ->
                self = $(this)
                unit = $(this).find("select").val() or 0

                # update event total price
                total_event_price += unit * $(this).find(".unit_price").text()


            # update cart total price
            all_total_price += total_event_price
            $(".event_total_price").text total_event_price

            # add transportation price
            if total_product_price isnt 0 and all_total_price < mini_price
                $(".transportation_charge").text transportation_charge

                # update cart total price
                all_total_price += transportation_charge
            else
                $(".transportation_charge").text "0"

            # update all total price
            $("#total_price").text all_total_price

        update_shopping_cart: (e) ->
            e.stopImmediatePropagation()
            e.preventDefault()
            (debug) and console.log("update sellcart")
            type = $(e.currentTarget).data("type")
            top = $(e.currentTarget).parent().parent()
            carts = getmore.storage.get_sellcarts("sellcarts")
            total_price = 0
            unit = undefined
            price = undefined
            data = undefined
            self = this
            unit = top.find("select").val()
            $(e.currentTarget).data "unit", unit
            total_price = unit * $(e.currentTarget).data("price")
            top.find(".total_price").text total_price
            self.update_total_value()

            # update cart value
            data =
                id: $(e.currentTarget).data("id")
                type: $(e.currentTarget).data("type")
                total: total_price
                count: unit

            i = 0

            while i < carts.length
                if carts[i].type is data.type and carts[i].id is data.id
                    carts[i].count = data.count
                    carts[i].total = data.total
                    break
                i++
            getmore.storage.add_item "sellcarts", carts

            # update cats data to session or database when add product into carts
            $.post getmore.API.getSellcfarts,
                carts: JSON.stringify(carts)
                force: true


            # update shopping cart on tool bar
            @initial_cart()

        delete_shopping_cart: (e) ->
            e.stopImmediatePropagation()
            e.preventDefault()
            self = this
            message = (if ($(e.currentTarget).data("type") is "product") then "確定刪除商品?" else "確定取消報名?")
            update = $(e.currentTarget).data("update") or 0
            carts = getmore.storage.get_sellcarts("sellcarts")
            data =
                id: $(e.currentTarget).data("id")
                type: $(e.currentTarget).data("type")

            # confirm remove product
            return false  unless window.confirm(message)

            # delete product on cart
            i = 0

            while i < carts.length
                if carts[i].type is data.type and carts[i].id is data.id
                    carts = removeKey(carts, i)
                    break
                i++

            # remove table tr
            $(e.currentTarget).parent().parent().hide "slow", ->
                $(this).remove()

                # update steps 1 all price
                self.update_total_value()  if update is "1"

            if carts.length > 0

                # if carts is not empty, update local storage
                getmore.storage.add_item "sellcarts", carts
            else

                # if carts is empty, delete local storage and create new Array
                getmore.storage.delete_item "sellcarts"
                carts = []
                $("#shopping-empty").show()
                $("#shopping-region").hide()
            $(".cart-count").text carts.length

            # update cats data to session or database when remove product
            $.post getmore.API.getSellcarts,
                carts: JSON.stringify(carts)
                force: true

            (debug) and console.log(carts)

        initialize: ->
            carts = getmore.storage.get_sellcarts("sellcarts")
            (debug) and console.log(carts)
            $.ajax
                url: getmore.API.getSellcarts
                dataType: "json"
                type: "POST"
                data:
                    carts: JSON.stringify(carts)

                success: (response) ->
                    (debug) and console.log(response)
                    if response.success_text and response.carts? and _.isArray(response.carts) and response.carts.length > 0
                        getmore.storage.add_item "sellcarts", response.carts
                        carts = response.carts
                    if _.isArray(carts) and carts.length > 0
                        i = 0

                        while i < carts.length

                            # check carts is null
                            $("#shopping-car").append Handlebars.templates["shopping_cart"](carts[i])  if carts[i].id
                            i++
                    $(".cart-count").text carts.length


            # get site config all item
            getmore.Config = new ModelConfig()
            getmore.Config.fetch()
    )
    # notyet
    Order = Backbone.View.extend(
        el: "body"
        events:
            "click .payment": "payment"
            "click #step2": "step2"
            "click #step3": "step3"

        payment: (e) ->
            e.stopImmediatePropagation()
            e.preventDefault()
            $(e.currentTarget).parent().find("li").removeClass "active"
            $(e.currentTarget).addClass "active"
            $("input[name=payment_method]").val $(e.currentTarget).data("type")

        step2: (e) ->
            e.stopImmediatePropagation()
            e.preventDefault()
            form_id = $(e.currentTarget).data("form")

            # redirect to complete page
            $("#" + form_id).submit()  if window.confirm("商品活動數量確定，進入下一步填寫資料?")

        step3: (e) ->
            e.stopImmediatePropagation()
            e.preventDefault()
            error = false
            form_id = $(e.currentTarget).data("form")
            form_info = $("#" + form_id).serializeObject()
            for name of form_info
                $("input[name=" + name + "]").removeClass "form_error"
                if $.trim(form_info[name]) is "" and name isnt "uniform_invoice_title" and name isnt "uniform_invoice_number"
                    $("input[name=" + name + "]").addClass "form_error"
                    error = true
            if error
                alert "紅色欄位務必填寫"
                e.stopImmediatePropagation()
                return false
            if window.confirm("資料填寫完成，確定完成此購物?")

                # delete local storage carts
                getmore.storage.delete_item "carts"

                # redirect to complete page
                $("#" + form_id).submit()

        initialize: ->

            # check uniform invoice type
            $("input[name=uniform_invoice_type]").click ->
                type = $("input[name=uniform_invoice_type]:checked").val()
                if type is "1" or type is "2"
                    $(".show_uniform_invoice").show()
                else
                    $(".show_uniform_invoice").hide()

    )

    AppView = Backbone.View.extend(
        el: "body"
        events:
            click: "close_all_menu"
            "click .close": "close_message"
            "click #login-area": "login_area"
            "click #shopping-cart-area": "shopping_area"
            "click .check-payment": "check_payment"
            "click #add_to_delivery": "add_to_delivery"


        # copy data to delivery area
        add_to_delivery: (e) ->
            $("input[name=delivery_name]").val $("input[name=user_name]").val()
            if $("input[name=user_sex]:checked").val() is "1"
                $(".delivery_sex_man").prop "checked", true
            else
                $(".delivery_sex_woman").prop "checked", true
            $("input[name=delivery_mobile_phone]").val $("input[name=user_mobile_phone]").val()
            $("input[name=delivery_telephone]").val $("input[name=user_telephone]").val()
            $("select[name=delivery_county]").val($("select[name=user_county]").val()).trigger "change"
            $("select[name=delivery_district]").val($("select[name=user_district]").val()).trigger "change"
            $("input[name=delivery_zipcode]").val $("input[name=user_zipcode]").val()
            $("input[name=delivery_address]").val $("input[name=user_address]").val()


        # online pay for user order list
        check_payment: (e) ->
            e.preventDefault()
            $target = $(e.currentTarget)
            window.location.href = "/order/pay/" + $target.data("id")  if window.confirm("線上付款總金額:" + $target.data("price"))
            false


        # close menu when click body area
        close_all_menu: (e) ->
            $target = $(e.target)


        #
        #            if (!$target.is('#login-area *')) {
        #                $('#login-area').removeClass('active');
        #                $('#login-area').find('.login-area').removeClass('show');
        #            }
        #
        #            if (!$target.is('#shopping-cart-area *')) {
        #                $('#shopping-cart-area').removeClass('active');
        #                $('#shopping-cart-area').find('.shopping-cart-area').removeClass('show');
        #            }
        #

        # close alert message
        close_message: (e) ->
            e.stopImmediatePropagation()
            e.preventDefault()
            $(e.currentTarget).parent().fadeOut "slow"


        # click event for login area
        login_area: (e) ->
            $target = $(e.target) # click target
            return  if $target.is(".login-area *")
            $(e.currentTarget).toggleClass "active"
            $(e.currentTarget).find(".login-area").toggleClass "show"


        # click event for shopping cart area
        shopping_area: (e) ->
            $target = $(e.target) # click target
            return  if $target.is(".shopping-cart-area *")
            $(e.currentTarget).toggleClass "active"
            $(e.currentTarget).find(".shopping-cart-area").toggleClass "show"
            $(".shopping-cart").slideDown()

        update_user: (e) ->
            if @me.get("p_address") and $("#p_address").length > 0
                obj = $.parseJSON(@me.get("p_address"))
                $("#p_address").twzipcode
                    zipcodeSel: obj.zipcode
                    addressSel: obj.address
                    css: ["input-small", "input-small", "input-small zipcode"]

            if @me.get("p_address") and $("#user_address").length > 0
                obj = $.parseJSON(@me.get("p_address"))
                $("#user_address").twzipcode
                    zipcodeSel: obj.zipcode
                    addressSel: obj.address
                    countyName: "user_county"
                    districtName: "user_district"
                    zipcodeName: "user_zipcode"
                    addressName: "user_address"
                    css: ["input-small", "input-small", "input-small zipcode", "input-small address"]

            if @me.get("p_address") and $("#delivery_address").length > 0
                obj = $.parseJSON(@me.get("s_address"))
                $("#delivery_address").twzipcode
                    zipcodeSel: obj.zipcode
                    addressSel: obj.address
                    countyName: "delivery_county"
                    districtName: "delivery_district"
                    zipcodeName: "delivery_zipcode"
                    addressName: "delivery_address"
                    css: ["input-small", "input-small", "input-small zipcode", "input-small address"]


        add_placeholder: (id, placeholder) ->

            # check id exist.
            return  if $("#" + id).length <= 0
            el = document.getElementById(id)
            el.placeholder = placeholder
            el.onfocus = ->
                if @value is @placeholder
                    @value = ""
                    el.style.cssText = ""

            el.onblur = ->
                if @value.length is 0
                    @value = @placeholder
                    el.style.cssText = "color:#A9A9A9;"

            el.onblur()

        initialize: ->
            is_show_pannel = 0
            is_login_pannel = 0
            is_shopping_pannel = 0

            # image lazy load
            $("img.lazy").lazyload(effect: "fadeIn").removeClass "lazy"
            $(document).ajaxStop ->
                $("img.lazy").lazyload(effect: "fadeIn").removeClass "lazy"


            # index slide show
            $(".slideShow").slideShow
                interval: 10
                transition:
                    mode: "slideshow"
                    speed: 800

            $("#shopping-cart-area").mouseenter((e) ->
                $(this).addClass "active"
                $(this).find(".shopping-cart-area").addClass "show"
                $(".shopping-cart").slideDown()
            ).mouseleave ->
                $(this).removeClass "active"
                $(this).find(".shopping-cart-area").removeClass "show"
                $(".shopping-cart").slideUp()

            $("#login-area, .login").mouseenter((e) ->
                $(this).addClass "active"
                $(this).find(".login-area").addClass "show"
            ).mouseleave ->
                $(this).removeClass "active"
                $(this).find(".login-area").removeClass "show"


            # product Zoom
            $("#main_pic").jqzoom
                zoomType: "reverse"
                zoomWidth: 500
                zoomHeight: 350
                xOffset: 40
                showEffect: "fadein"


            # default focus input for login page
            $("input[name='identity']").focus()
            $(window).scroll ->
                if $(window).scrollTop() is $(document).height() - $(window).height()
                    api_path = undefined
                    template = undefined
                    module = $("#content-row-3").data("module")
                    limit = $("#content-row-3").data("limit")
                    id = $("#content-row-3").data("id") or 0
                    offset = $("#content-row-3 .small-article").length or 0

                    # check ajax module exist.
                    return  if not module or not limit
                    switch module
                      when "subject"
                            api_path = getmore.API.getSubjectList
                            template = "single_subject"
                      when "event"
                            api_path = getmore.API.getEventList
                            template = "single_event"
                    if module and is_ajax_load is false
                        is_ajax_load = true
                        getmore.api_req.GET api_path,
                            limit: limit
                            offset: offset
                            id: id
                        , (data) ->
                            i = 0

                            while i < data.items.length
                                $("#row-3").append Handlebars.templates[template](data.items[i])
                                i++
                            is_ajax_load = false

            # add placeholder on register page
            if /msie/.test(navigator.userAgent.toLowerCase())
                @add_placeholder "identity", "請輸入電子郵件"
                @add_placeholder "password", "請輸入密碼"
                @add_placeholder "user_name", "請輸入全名"
                @add_placeholder "email", "請輸入電子郵件"
                @add_placeholder "password_confirm", "請再次確認密碼"
                @add_placeholder "new_password", "請輸入密碼"

            # get current user data
            @me = new ModelMe()
            @me.on "change", @update_user, this
            @me.fetch()
    )
    initialize = ->
        getmore.storage = new local_storage
        getmore.View = new AppView

        # cart initial
        getmore.Cart = new Cart

        # sellcart initial
        getmore.Sellcart = new Sellcart

        # Order steps initial
        getmore.Order = new Order

    initialize: initialize

