<?php
class test_article_library extends CodeIgniterUnitTestCase
{

    public function __construct(){
        parent::__construct('Article Library');
        $this->load->library('Getmore/Article/ArticleDataMapper', '', 'article_dm');
        $this->load->library('Getmore/Article/ArticleRepository', '', 'article_rp');
    }
    
    function test_class_exists(){
        $this->assertTrue(class_exists('ArticleDataMapper'));
        $this->assertTrue(class_exists('Article'));
    }

    /*
     * Test Data Mapper
     */
    
    function test_save_and_remove(){
        $row_num = $this->db->count_all('articles');
        $article = new Article();
        $id = $this->article_rp->save($article);
        $this->assertEqual($row_num + 1, $this->db->count_all('articles'));        
        $victim = $this->article_dm->get($id);
        $this->article_dm->remove($victim);
        $this->assertEqual($row_num, $this->db->count_all('articles'));        
    }
    
    function test_get(){
        $article = new Article();
        $title = 'This is Title';
        $content = 'This is Content';
        $feature_pic_id = 999;
        $article->title = $title;
        $article->content = $content;
        $article->feature_pic_id = $feature_pic_id;
        $id = $this->article_rp->save($article);
        
        $victim = $this->article_dm->get($id);
        $this->assertEqual($victim->title, $title);
        $this->assertEqual($victim->content, $content);
        $this->assertEqual($victim->feature_pic_id, $feature_pic_id);
    }
    
    /*
     * Test Domain Object
     */

}
