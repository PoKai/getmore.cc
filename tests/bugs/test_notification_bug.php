<?php
class test_notification_bug extends CodeIgniterUnitTestCase{

    public function __construct(){
        parent::__construct('Bug 123 Fix');
    }
    
    public function test_bug(){
        $this->pass();
    }
     
    public function test_notification(){
        $this->load->model('Product_clothing_model');
                
        try{
            $this->Product_clothing_model->notify_the_clothing_is_soldout('T1404170001002');
        }
        catch(Exception $e){
            $this->dump($e->getMessage());
        }
          
        $this->pass();
    }
    
}

/* End of file test_123_bug.php */
/* Location: ./tests/bugs/test_123_bug.php */
