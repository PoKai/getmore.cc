<?php
class test_clothing_model extends CodeIgniterUnitTestCase
{
    protected $valid_clothing_id;

    public function __construct()
    {
        parent::__construct('Clothing Model');

        $this->load->model('Product_clothing_model');

        $this->valid_clothing_id = $this->db->limit(1)->get('product_clothing')->result()[0]->id;
    }
    
    function test_get_entries_with_like_feature(){
        // let's like ten items
        $clothing = $this->Product_clothing_model->get_entries(10);
        foreach($clothing as $cloth){
            $this->Product_clothing_model->like_clothing(89, $cloth['id']);
        }
        $clothing = $this->Product_clothing_model->get_entries(20);
        $clothing = $this->Product_clothing_model->append_my_like_id($clothing, 89);
#        $this->dump($clothing);

        // is number correct?
        $this->assertEqual(count($clothing), 20);
        
        $my_likes = 0;
        foreach($clothing as $cloth){
            if (!empty($cloth['my_like_id'])){
                $my_likes = $my_likes + 1;
            }
        }
        
        $this->assertEqual($my_likes, 10);        
    }
    
    function test_like_clothing(){
        $result = $this->Product_clothing_model->like_clothing(89, $this->valid_clothing_id);
        $this->assertTrue($result);
    }
    
    function test_unlike_clothing(){
        $result = $this->Product_clothing_model->unlike_clothing(89, $this->valid_clothing_id);
        $this->assertTrue($result);
    }
    
    function test_toggle_for_like(){
        $result = $this->Product_clothing_model->toggle_like_clothing(89, $this->valid_clothing_id);
        $this->assertTrue($result);
    }
    function test_toggle_for_unlike(){
        $result = $this->Product_clothing_model->toggle_like_clothing(89, $this->valid_clothing_id);
        $this->assertTrue($result);
    }

    public function test_included()
    {
      $this->assertTrue(class_exists('Product_clothing_model'));
    }
    /*
    function test_update_clothing_price(){
        $result = $this->Product_clothing_model->update_price(46, 2355, 89);
        $this->assertTrue($result);
    } 
    */    
    function test_if_id_is_invalid(){
        $this->expectException();
        $result = $this->Product_clothing_model->update_price(9999, 2355, 89);
    }    
    function test_if_price_is_invalid(){
        $this->expectException();
        $result = $this->Product_clothing_model->update_price(46, -100, 89);
    }    
    
    function test_notify_the_clothing_is_reduction_again(){
        $this->assertTrue($this->Product_clothing_model->notify_the_clothing_is_reduction_again(652), true);
    }

}
