<?php
class test_user_model extends CodeIgniterUnitTestCase{
    
    protected $data = array();
    protected $user_data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model', 'user');
        $this->user_data = array(
            "user_name"                => 'dumb',
            "sex"                              => '1',
            "mobile_phone"          => '0988123456',
            "telephone"                 => 'dumb',
            "birthday"                    => '1900-01-01',
            "p_address"                 => '{ "address": "dumb" }',
            "s_address"                 => '{ "address": "dumb" }',
            "identification_card_number"  => 'A100000001'
        );        
    }
    
    public function test_included(){
        $this->assertTrue(class_exists('User_model'));
    }
    
    public function test_get_effective_email(){
        $user_without_active_email = $this->db->from('users')
            ->where('active_email', '')
            ->get()->row_array();

        $user_with_active_email = $this->db->from('users')
            ->order_by('active_email', 'desc')
            ->get()->row_array();
        
        $this->assertEqual($user_without_active_email['email'], 
            $this->user->get_effective_email($user_without_active_email['id']));

        $this->assertEqual($user_with_active_email['active_email'], 
            $this->user->get_effective_email($user_with_active_email['id']));
    }    
    
    function setUp(){
        $user_data = array();
        $fields = $this->db->list_fields('users');
        foreach ($fields as $field){
           $user_data[$field] = 'dumb';
        }
        unset($user_data['id']);
        $this->db->insert('users', $user_data);
        $this->data['user_id'] = $this->db->insert_id();
    }
    
    function tearDown(){
        $this->db->where('id', $this->data['user_id'])->delete('users');
    }
    
    /*
     * Validate the method with some missing arguments
     */    
    function test_save_personal_information_robustness(){
        $user_data = $this->user_data;
        try{
            $invalid_data = $user_data;
            $invalid_data['user_name'] = '';
            $this->user->save_personal_information($this->data['user_id'], $invalid_data);
            $this->fail();
        }
        catch(Exception $e){
            $this->pass();
        }
        try{
            $invalid_data = $user_data;
            $invalid_data['mobile_phone'] = '';
            $this->user->save_personal_information($this->data['user_id'], $invalid_data);
            $this->fail();
        }
        catch(Exception $e){
            $this->pass();
        }
        try{
            $invalid_data = $user_data;
            $invalid_data['telephone'] = '';
            $this->user->save_personal_information($this->data['user_id'], $invalid_data);
            $this->fail();
        }
        catch(Exception $e){
            $this->pass();
        }
        try{
            $invalid_data = $user_data;
            $invalid_data['birthday'] = '';
            $this->user->save_personal_information($this->data['user_id'], $invalid_data);
            $this->fail();
        }
        catch(Exception $e){
            $this->pass();
        }
        try{
            $invalid_data = $user_data;
            $invalid_data['p_address'] = '';
            $this->user->save_personal_information($this->data['user_id'], $invalid_data);
            $this->fail();
        }
        catch(Exception $e){
            $this->pass();
        }
        try{
            $invalid_data = $user_data;
            $invalid_data['s_address'] = '';
            $this->user->save_personal_information($this->data['user_id'], $invalid_data);
            $this->fail();
        }
        catch(Exception $e){
            $this->pass();
        }
        try{
            $invalid_data = $user_data;
            $invalid_data['identification_card_number'] = '';
            $this->user->save_personal_information($this->data['user_id'], $invalid_data);
            $this->fail();
        }
        catch(Exception $e){
            $this->pass();
        }
        $success_case = $this->user->save_personal_information($this->data['user_id'], $user_data);
        $this->assertTrue($success_case);
    }
        
    function test_validate_id_number(){
        $valid_id = array( // plus 1
            'A228748089',  
            'R223046948',
            'F228114186'
        );
            
        foreach($valid_id as $v_id){
            try{
                $invalid_data = $this->user_data;
                $invalid_data['identification_card_number'] = $v_id;
                $this->user->save_personal_information($this->data['user_id'], $invalid_data);
                $this->fail();
            }
            catch(Exception $e){
                $this->pass();
            }          
        }

        $valid_data = $this->user_data;
        $valid_data['identification_card_number'] = lcfirst($valid_data['identification_card_number']);
        $success_case = $this->user->save_personal_information($this->data['user_id'], $valid_data);
        $this->assertTrue($success_case);      
    }

    function test_validate_mobile_phone_number(){
        $valid_number = array(
            '0987654321'
        );
        
        foreach($valid_number as $v_id){
            if ($this->user->validate_mobile_phone_number($v_id)){
                $this->pass();
            }
            else{
                $this->fail();
            }
        }
        foreach($invalid_number as $v_id){
            if ($this->user->validate_mobile_phone_number($v_id)){
                $this->fail();
            }
            else{
                $this->pass();
            }
        }
    }


    /*
     * Validate the method with some missing arguments
     */    
    function test_is_filled_information(){
        $user_data = $this->user_data;
        $this->user->save_personal_information($this->data['user_id'], $user_data);
        $result = $this->user->is_filled_information($this->data['user_id']);
        $this->assertTrue($result);
    }


}
