<?php
class test_sell_and_product_model extends CodeIgniterUnitTestCase{
    
    protected $order_data;
    protected $item_data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Order_model');
        $this->load->model('Product_model');
        $this->load->model('Sellcoupon_model');
        
        $this->order_data = array(
            'user_id' => '89',

            'user_name' => '尤川豪',
            'user_telephone' => '0989623038',
            'user_mobile_phone' => '0989623038',
            'user_address' => '235 新北市中和區 圓通路335巷37弄47-3號4樓',

            'delivery_name' => '尤川豪',
            'delivery_telephone' => '0989623038',
            'delivery_mobile_phone' => '0989623038',
            'delivery_address' => '235 新北市中和區 圓通路335巷37弄47-3號4樓',
            
            'order_number' => $this->Order_model->generate_order_number(),
            'payment_method' => 'test',
            
            'deduction_amount' => '-100',
            
            'uniform_invoice_type' => '1',
            'uniform_invoice_title' => '',
            'uniform_invoice_title' => '',
            
            'no_return' => '1',
            
            'date_purchased' => date('c'),
            'status_id' => '1'
        );        
        
        $this->item_data = array(
            'product_title' => '1000',
            'product_original_price' => '1000',
            'product_sell_price' => '1000',
            'product_type' => '21',
            'donative' => '1'
        );
        
    }
    
    public function test_included(){
        $this->assertTrue(class_exists('Order_model'));
        $this->assertTrue(class_exists('Product_model'));
    }
    
    public function test_get_type_washing_fee(){
        $types = array('11', '12', 11, 12, '241', '242');
        foreach($types as $type){
            # $this->dump($this->Product_model->get_type_washing_fee($type));
        }
        $this->pass();
    }
    public function test_get_price_transportation_charge(){
        $prices = array(0, '0', '100', 100, 500, 700);
        foreach($prices as $price){
           # $this->dump($this->Order_model->get_price_transportation_charge($price));
        }
        $this->pass();
    }
    /*
    public function test_create(){
        $items_data = array();
        for($i=0; $i<5; $i++){
            array_push($items_data, $this->item_data);
        }
        $order_id = $this->Order_model->create($this->order_data, $items_data);
        $this->assertTrue($order_id);
    }

    
    public function test_create_robustness(){
        $items_data = array();
        for($i=0; $i<5; $i++){
            array_push($items_data, $this->item_data);
        }
        $order_data = $this->order_data;
        $order_data['payment_method'] = 'deduction';
        $order_data['order_number'] = $this->Order_model->generate_order_number();
        // Is the code protect from evil payment method?
        try{
            $order_id = $this->Order_model->create($order_data, $items_data);
            $this->fail();
        }
        catch(Exception $e){
            $this->dump($e->getMessage());
            $this->pass();
        }
        
        $order_data2 = $this->order_data;
        $order_data2['deduction_amount'] = '-90000';
        $order_data2['order_number'] = $this->Order_model->generate_order_number();

        // Is the code protect from evil deduction_amount?
        try{
            $order_id = $this->Order_model->create($order_data2, $items_data);
            $this->fail();
        }
        catch(Exception $e){
            $this->dump($e->getMessage());
            $this->pass();
        }
    }
    */
    /*
    public function test_complete(){
        $items_data = array();
        for($i=0; $i<5; $i++){
            array_push($items_data, $this->item_data);
        }
        $order_data = $this->order_data;
        $order_data['order_number'] = $this->Order_model->generate_order_number();

        $order_id = $this->Order_model->create($order_data, $items_data);
        
        $this->Order_model->complete($order_id);
        $order = $this->Order_model->get($order_id);
        $this->dump($order['date_finished']);
        $this->assertEqual($order['status_id'], ORDER_COMPLETE);
    }
    */
    /*
    public function test_activate_coupon_robustness(){
        $items_data = array();
        for($i=0; $i<5; $i++){
            array_push($items_data, $this->item_data);
        }
        $order_data = $this->order_data;
        $order_data['order_number'] = $this->Order_model->generate_order_number();
        
        $coupon_data = array(
            'amount' => 100,
            'code' => rand(),
            'times_available' => 5,
            'times_used' => 5,
            'expired_at' => '2015-01-01'
        );
        $this->db->insert('sellcoupons', $coupon_data);
        $id = $this->db->insert_id();
        $order_data['coupon_code'] = $coupon_data['code'];
        
        try{ // test run-out coupon
            $order_id = $this->Order_model->create($order_data, $items_data);        
            $this->fail();
        }
        catch(Exception $e){
            $this->dump($e->getMessage());
            $this->pass();
        }
          
        $order_data['order_number'] = $this->Order_model->generate_order_number();        
        $coupon_data2 = array(
            'amount' => 100,
            'code' => rand(),
            'times_available' => 5,
            'times_used' => 0,
            'expired_at' => '2015-01-01',
            'minimum_charge' => '20000'
        );
        $this->db->insert('sellcoupons', $coupon_data2);
        $order_data['coupon_code'] = $coupon_data2['code'];
        
        try{ // test coupon with strict minimum_charge
            $order_id = $this->Order_model->create($order_data, $items_data);        
            $this->fail();
        }
        catch(Exception $e){
            $this->dump($e->getMessage());
            $this->pass();
        }
        
        $order_data['order_number'] = $this->Order_model->generate_order_number();        
        $coupon_data3 = array(
            'amount' => 100,
            'code' => rand(),
            'times_available' => 5,
            'times_used' => 0,
            'expired_at' => '2014-01-01'
        );
        $this->db->insert('sellcoupons', $coupon_data3);
        $order_data['coupon_code'] = $coupon_data3['code'];
        
        try{ // test coupon with strict minimum_charge
            $order_id = $this->Order_model->create($order_data, $items_data);        
            $this->fail();
        }
        catch(Exception $e){
            $this->dump($e->getMessage());
            $this->pass();
        }        
        
    }
    */
    public function test_activate_coupon(){
        $items_data = array();
        for($i=0; $i<5; $i++){
            array_push($items_data, $this->item_data);
        }
        $order_data = $this->order_data;
        $order_data['order_number'] = $this->Order_model->generate_order_number();
        
        $coupon_data = array(
            'amount' => 100,
            'code' => rand(),
            'times_available' => 5,
            'times_used' => 0,
            'expired_at' => '2015-01-01',
            'dedicated_user_id' => '89'
        );
        $this->db->insert('sellcoupons', $coupon_data);
        $id = $this->db->insert_id();
        $order_data['coupon_code'] = $coupon_data['code'];
        
        $order_id = $this->Order_model->create($order_data, $items_data, '89');        
        $this->assertTrue($order_id);
    }

    
    /*
    public function test_add_empty_order(){
        $id = $this->Order_model->add_empty_order($this->order_data);
        $this->assertTrue($id);      
    }
    */

    /*
    public function test_add_empty_order_robustness(){
      
        try{
            $invalid_data = $this->order_data;
            $invalid_data['user_id'] = '';
            $id = $this->Order_model->add_empty_order($invalid_data);
            $this->fail();
        }
        catch(Exception $e){
            $this->dump($e->getMessage());
            $this->pass();
        }
        try{
            $invalid_data = $this->order_data;
            $invalid_data['order_number'] = '';
            $id = $this->Order_model->add_empty_order($invalid_data);
            $this->fail();
        }
        catch(Exception $e){
            $this->dump($e->getMessage());
            $this->pass();
        }
        try{
            $invalid_data = $this->order_data;
            $invalid_data['payment_method'] = '';
            $id = $this->Order_model->add_empty_order($invalid_data);
            $this->fail();
        }
        catch(Exception $e){
            $this->dump($e->getMessage());
            $this->pass();
        }
      
    }
    */
    
    /*
    public function test_add_to_order(){
        $order_id = $this->Order_model->add_empty_order($this->order_data);
        $this->assertTrue($order_id);
#        $this->dump($this->item_data);

        try{
            for($i=0; $i<5; $i++){
                $item_id = $this->Product_model->add_to_order($order_id, $this->item_data);
            }
        }
        catch(Exception $e){
            $this->dump($e->getMessage());
            $this->dump($this->item_data);
        }
        $this->assertTrue($item_id);
    }
    */
    
}
  
