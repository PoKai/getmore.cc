<?php
class test_system_configuration_model extends CodeIgniterUnitTestCase
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('System_configuration_model', 'sys_config');
        $this->db->truncate('banners');
    }
    
    public function test_included(){
        $this->assertTrue(class_exists('System_configuration_model'));
    }
    
    public function test_get_free_shipping(){
        $result = $this->sys_config->get_free_shipping();
        $this->assertNotNull($result);
    }

    public function test_set_free_shipping(){
        $result = $this->sys_config->set_free_shipping(TRUE);
        $this->assertNotNull($result);
    }
    
    public function test_add_banner(){
        $href = 'http://example.com/foo/foo.jpg';
        
        $result = $this->sys_config->add_banner(2073, $href, TRUE);
        $result = $this->sys_config->add_banner(2071, $href, TRUE);
        $result = $this->sys_config->add_banner(2070, $href, TRUE);
        $result = $this->sys_config->add_banner(2072, $href, TRUE);
        $this->assertTrue($result);
    }
    
    public function test_set_banner_order(){
        $ordered_banner_ids = array(3, 2, 4, 1);
        $result = $this->sys_config->set_banner_order($ordered_banner_ids);
        $this->assertNotNull($result);
    }
    
    public function test_get_banners_with_image_url_and_href(){
        $result = $this->sys_config->get_banners_with_image_url_and_href();
        $this->assertNotNull($result);
        $this->dump($result);
    }
    
    public function test_delete_banner(){
        $result = $this->sys_config->delete_banner(4);
        $this->assertTrue($result);
    }
}
