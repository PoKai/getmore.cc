<?php
class test_account_model extends CodeIgniterUnitTestCase
{
    protected $valid_clothing_id;
    
    protected $account_id = 89;
    protected $account_amount;
    
    protected $test_trans_id;

    public function __construct()
    {
        parent::__construct('Account Model');

        $this->load->model('Transaction_model', 'transaction');
        $this->load->model('Account_model', 'account');
    }

    public function setUp(){
        $this->account_amount = $this->account->get($this->account_id)->balance;
        $this->account->update($this->account_id, array('balance'=>0));
    }

    public function tearDown(){
        $this->account->update($this->account_id, array('balance'=>$this->account_amount));
    }


    function test_adding() {
        $account_before = $this->account->get('89');
        $this->transaction->insert_to_user('89', 'Testing trans', 'mocking order number', 150, 'created from unit test');
        $account_after = $this->account->get('89');
        $this->assertEqual( $account_after->balance, $account_before->balance + 150, 'Simple Adding test' );
    }
    function test_deduting() {
        $account_before = $this->account->get('89');
        $this->transaction->insert_to_user('89', 'Testing trans', 'mocking order number', -150, 'created from unit test');
        $account_after = $this->account->get('89');
        $this->assertEqual( $account_after->balance, $account_before->balance - 150, 'Simple Adding test' );
    }
    function test_if_amount_is_zero() {
        $this->expectException();
        $account_before = $this->account->get('89');
        $this->transaction->insert_to_user('89', 'Testing trans', 'mocking order number', 0, 'created from unit test');
        $account_after = $this->account->get('89');
    }
    function test_if_user_id_not_exist() {
        $this->expectException();
        $this->account->get_by_user_id(-99);
    }    
    function test_if_wrong_account() {
        $this->expectException();
        $this->transaction->insert_to_user(-99, 'Testing trans', 'mocking order number', 0, 'created from unit test');
    } 

}

/* End of file test_users_model.php */
/* Location: ./tests/models/test_users_model.php */
