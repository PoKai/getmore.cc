<?php
class test_coupon_model extends CodeIgniterUnitTestCase
{
    protected $coupon_id;

    public function __construct()
    {
        parent::__construct('Coupon Model');

        $this->load->model('Coupon_model', 'coupon');
       # $this->db->truncate('coupons');
    }
    
    function test_included(){
        $this->assertTrue(class_exists('Coupon_model'));
    }
   
    function test_create(){
        $data = array(
            'amount' => rand(200, 500),
            'code'       => $this->generateRandomString(),
            'times_available' => rand(200,500),
            'expired_at'          => date("Y-m-d H:i:s"),            
            'created_at'          => date("Y-m-d H:i:s"),            
            'name'          => 'coupon test'            
        );        
        $id = $this->coupon->create($data);
        $this->assertNotNull($id);
        $this->coupon_id = $id;
    }

    function test_create_with_invalid_params(){
        $this->expectException();
        $data = array(
            'amount' => 0,
            'code'       => $this->generateRandomString(),
            'times_available' => rand(200,500),
            'expired_at'          => date("Y-m-d H:i:s")            
        );        
        $id = $this->coupon->create($data);
    }
    
    function test_consume(){
        $coupon = $this->coupon->get($this->coupon_id);
        $result = $this->coupon->consume($coupon->code);
        $this->assertTrue($result);
    }
    function test_consume_with_invalid_code(){
        $this->expectException();
        $result = $this->coupon->consume($this->generateRandomString());
    }    

    function test_remove(){
        $result = $this->coupon->remove($this->coupon_id);
        $this->assertTrue($result);
    }
    
    function test_create_with_invalid_user_id(){
        $data = array(
            'amount' => rand(200, 500),
            'code'       => $this->generateRandomString(),
            'times_available' => rand(200,500),
            'expired_at'          => date("Y-m-d H:i:s"),            
            'created_at'          => date("Y-m-d H:i:s"),            
            'name'          => 'coupon test',
            'dedicated_user_id' => '-1'
        );        
        $this->expectException();
        $id = $this->coupon->create($data);
    }    

    function test_create_with_valid_user_id(){
        $data = array(
            'amount' => rand(200, 500),
            'code'       => $this->generateRandomString(),
            'times_available' => rand(200,500),
            'expired_at'          => date("Y-m-d H:i:s"),            
            'created_at'          => date("Y-m-d H:i:s"),            
            'name'          => 'coupon test',
            'dedicated_user_id' => 89
        );        

        $id = $this->coupon->create($data);
        $this->assertTrue($id);
    }    

    function test_consume_with_invalid_dedicated_user_id(){
        $data = array(
            'amount' => rand(200, 500),
            'code'       => $this->generateRandomString(),
            'times_available' => rand(200,500),
            'expired_at'          => date("Y-m-d H:i:s"),            
            'created_at'          => date("Y-m-d H:i:s"),            
            'name'          => 'coupon test',
            'dedicated_user_id' => 89
        );        

        $this->coupon->create($data);

        $this->expectException();
        
        $this->coupon->consume($data['code']);
    }    
    
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
}

/* End of file test_users_model.php */
/* Location: ./tests/models/test_users_model.php */
