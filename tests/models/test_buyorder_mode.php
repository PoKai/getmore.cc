<?php
class test_buyorder_model extends CodeIgniterUnitTestCase
{
    protected $valid_user_id;

    public function __construct()
    {
        exit('This is bad and dangerous test. Fix it.');
        parent::__construct('Buyorder Model');

        $this->load->model('Buy_order_model');
        $this->load->model('Transaction_model', 'transaction');
        $this->load->model('Account_model', 'account');      
        $this->valid_user_id = 89;
        
        // this is for the weird bug that tearDown don't work for
        // test expiration and used up cases
        $this->delete_testing_tuple(9999);
        $this->delete_testing_tuple(9998);
        $this->delete_testing_tuple(9997);        
   }
    
    private function add_testing_tuple($forced_id){
        $data = array(
          'id'                      => $forced_id,
          'product_item_number' => $forced_id . '_mock_number',
          'product_title' => 'STUPID_ROW',
         'product_id' => '9999' ,
         'product_original_price' => '200' ,
         'product_sell_price' => '100',
         'add_time' => time(),
         'edit_time' => time()
        );

        $this->db->insert('product_clothing', $data); 
        return TRUE;
    }
    
    private function delete_testing_tuple($forced_id){
        $this->db->where('id', $forced_id);
        $this->db->delete('product_clothing'); 
        return TRUE;
    }

    public function setUp(){
        $this->account->update($this->valid_user_id, array('balance'=>0));            
        $this->db->truncate('transactions'); 
        $this->db->truncate('buy_order'); 
        
      
        $this->add_testing_tuple(9999);
        $this->add_testing_tuple(9998);
        $this->add_testing_tuple(9997);      
        $this->transaction->insert_to_user('89', 'Trans for testing buyorder', 'mocking order number', 150, 'created from unit test');
    }

    public function tearDown(){
        $this->delete_testing_tuple(9999);
        $this->delete_testing_tuple(9998);
        $this->delete_testing_tuple(9997);
    }

    public function test_included(){
      $this->assertTrue(class_exists('Buy_order_model'));
    }
    /*
    function test_add_testing_tuple(){
        $result = $this->add_testing_tuple(9999);
        $this->assertTrue($result);
    }
    function test_delete_testing_tuple(){
        $result = $this->delete_testing_tuple(9999);
        $this->assertTrue($result);
    }
    */

    public function test_create_one_without_enough_money_in_account(){
        $this->expectException(new Exception('no enought money in account'));
        $payment_method = 'pseudo_payment_method';
        $ids = array(9999,9998,9997);
        $order = $this->mocking_order();
        $deduction_amount = -200;
        $this->Buy_order_model->create_one($order, $ids, $this->valid_user_id, $payment_method, $deduction_amount);  
    }
    
    
    public function test_create_one(){
        $num_of_orders_before_creation = $this->Buy_order_model->count_all();        
        $payment_method = 'pseudo_payment_method';
        $ids = array(9999,9998,9997);
        $order = $this->mocking_order();
        $deduction_amount = -75;        
        $this->Buy_order_model->create_one($order, $ids, $this->valid_user_id, $payment_method, $deduction_amount);  
        $this->assertEqual($this->Buy_order_model->count_all(), $num_of_orders_before_creation+1);
    }
    
    public function test_create_one_with_invalid_coupon_code(){
        $this->expectException(new Exception('invalid coupon code'));
        $payment_method = 'pseudo_payment_method';
        $ids = array(9999,9998,9997);
        $order = $this->mocking_order();
        $deduction_amount = -75;        
        $coupon_code = $this->generateRandomString();
        $this->Buy_order_model->create_one($order, $ids, $this->valid_user_id, $payment_method, $deduction_amount, $coupon_code);  
    }

    public function test_create_one_with_coupon_and_deduction(){
        $this->load->model('Coupon_model', 'coupon');
        $data = array(
            'amount' => 225,
            'code'       => $this->generateRandomString(),
            'times_available' => rand(200,500),
            'expired_at'          => date("Y-m-d H:i:s")            
        );
        $id = $this->coupon->create($data);
    
        $payment_method = 'deduction';
        $ids = array(9999,9998,9997);
        $order = $this->mocking_order();
        $deduction_amount = -75;        
        $coupon_code = $data['code'];
        $result = $this->Buy_order_model->create_one($order, $ids, $this->valid_user_id, $payment_method, $deduction_amount, $coupon_code);  
        $this->assertTrue($result);
    }

    public function test_create_one_with_too_big_coupon(){
        $this->load->model('Coupon_model', 'coupon');
        $data = array(
            'amount' => 230,
            'code'       => $this->generateRandomString(),
            'times_available' => rand(200,500),
            'expired_at'          => date("Y-m-d H:i:s")            
        );
        $id = $this->coupon->create($data);
    
        $this->expectException(new Exception("invalid final price by this coupon_amount. it makes final price negative"));
        $payment_method = 'pseudo_payment_method';
        $ids = array(9999,9998,9997);
        $order = $this->mocking_order();
        $deduction_amount = -75;        
        $coupon_code = $data['code'];
        $this->Buy_order_model->create_one($order, $ids, $this->valid_user_id, $payment_method, $deduction_amount, $coupon_code);  
    }

    public function test_create_one_with_expired_coupon(){
#        $this->expectException();
        
        
        $this->load->model('Coupon_model', 'coupon');
        $expired_at = (new Datetime())->modify("-1 hours")->format("Y-m-d H:i:s");
        $data = array(
            'amount' => 30,
            'code'       => $this->generateRandomString(),
            'times_available' => rand(200,500),
            'expired_at'          => $expired_at
        );
        $id = $this->db->insert('coupons', $data);
    
        $this->expectException(new Exception('this coupon is expired'));
        $payment_method = 'pseudo_payment_method';
        $ids = array(9999,9998,9997);
        $order = $this->mocking_order();
        $deduction_amount = -75;        
        $coupon_code = $data['code'];
        $this->Buy_order_model->create_one($order, $ids, $this->valid_user_id, $payment_method, $deduction_amount, $coupon_code);  
        
    }

    public function test_create_one_with_used_up_coupon(){
        $this->load->model('Coupon_model', 'coupon');
        $expired_at = (new Datetime())->format("Y-m-d H:i:s");
        $rand = rand(200,500);
        $data = array(
            'amount' => 30,
            'code'       => $this->generateRandomString(),
            'times_available' => $rand,
            'times_used' => $rand,
            'expired_at'          => $expired_at
        );
        $id = $this->db->insert('coupons', $data);
    
        $this->expectException(new Exception('this coupon has run out'));
        $payment_method = 'pseudo_payment_method';
        $ids = array(9999,9998,9997);
        $order = $this->mocking_order();
        $deduction_amount = -75;        
        $coupon_code = $data['code'];
        $this->Buy_order_model->create_one($order, $ids, $this->valid_user_id, $payment_method, $deduction_amount, $coupon_code);  
    }

    public function test_create_one_without_satisfying_coupon_minimun_charge(){
        $this->load->model('Coupon_model', 'coupon');
        $expired_at = (new Datetime())->format("Y-m-d H:i:s");
        $rand = rand(200,500);
        $data = array(
            'amount' => 30,
            'code'       => $this->generateRandomString(),
            'times_available' => $rand,
            'times_used' => 0,
            'minimum_charge' => 301,
            'expired_at'          => $expired_at
        );
        $this->db->insert('coupons', $data);
        $id = $this->db->insert_id();

        $this->expectException(new Exception('not satisfy minimum charge'));
        $payment_method = 'pseudo_payment_method';
        $ids = array(9999,9998,9997);
        $order = $this->mocking_order();
        $deduction_amount = -75;        
        $coupon_code = $data['code'];
        $this->Buy_order_model->create_one($order, $ids, $this->valid_user_id, $payment_method, $deduction_amount, $coupon_code);  
    }

    
    public function test_payment_method_is_deduction_with_none_zero_final_price(){
      
        $this->expectException();
        $num_of_orders_before_creation = $this->Buy_order_model->count_all();
        
        $payment_method = 'deduction';

        $ids = array(999,998,997);
        $order = $this->mocking_order();
        $deduction_amount = -50;
        
        $this->Buy_order_model->create_one($order, $ids, $this->valid_user_id, $payment_method, $deduction_amount);        
    }    
    
    public function test_total_price_is_zero_with_valid_payment_method(){
        $this->transaction->insert_to_user('89', 'Trans for testing buyorder', 'mocking order number', 150, 'created from unit test');
      
        $num_of_orders_before_creation = $this->Buy_order_model->count_all();
        
        $payment_method = 'deduction';

        $ids = array(9999,9998,9997);
        $order = $this->mocking_order();
        $deduction_amount = -300;
        
        $result = $this->Buy_order_model->create_one($order, $ids, $this->valid_user_id, $payment_method, $deduction_amount);        
        $this->assertNotNull($result);
    }
    

    public function test_user_id_not_exist(){
        $this->expectException();
        $num_of_orders_before_creation = $this->Buy_order_model->count_all();
        
        $payment_method = 'deduction';

        $ids = array(999,998,997);
        $order = $this->mocking_order();
        $deduction_amount = -80;
        
        $result = $this->Buy_order_model->create_one($order, $ids, -1, $payment_method, $deduction_amount);        
    }
    
    public function test_total_price_is_negative(){
        $this->expectException();
        
        $payment_method = 'credit-cart';

        $ids = array(999,998,997);
        $order = $this->mocking_order();
        $deduction_amount = -80000;
        
        $result = $this->Buy_order_model->create_one($order, $ids, $this->valid_user_id, $payment_method, $deduction_amount);        
    }    
    

    function test_refresh_items_amount_and_price(){
        $result = $this->Buy_order_model->refresh_items_amount_and_price('217');
        $this->assertTrue($result);        
    }    

    protected function mocking_order(){
        $order = new stdClass();
        $order->buyer = new stdClass();
        $order->buyer->location= new stdClass(); 
        $order->buyer->name = 'mocking_name';
        $order->buyer->gender = '1';
        $order->buyer->contact_phone_number = '3345678';        
        $order->buyer->location->zipcode = '888';
        $order->buyer->location->county = 'mocking_taipei';
        $order->buyer->location->district = 'mocking_street';
        $order->buyer->location->address = 'mocking_somewhere';
        
        $order->receiver = new stdClass();
        $order->receiver->location= new stdClass(); 
        $order->receiver->name = 'mocking_name';
        $order->receiver->gender = '1';
        $order->receiver->contact_phone_number = '3345678';
        $order->receiver->location->zipcode = '888';
        $order->receiver->location->county = 'mocking_taipei';
        $order->receiver->location->district = 'mocking_street';
        $order->receiver->location->address = 'mocking_somewhere';
    
        $order->delivery_time = 'any';
        $order->receipt_to = '1';

        $order->uniform_invoice_type = 'two';
        $order->uniform_invoice_title = 'mocking title';
        $order->uniform_invoice_number = 'mocking number';
        
        return $order;
    }        
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
}

/* End of file test_users_model.php */
/* Location: ./tests/models/test_users_model.php */
